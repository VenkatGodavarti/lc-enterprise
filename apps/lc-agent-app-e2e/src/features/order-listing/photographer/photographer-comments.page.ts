import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const photographerCommentsElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Lockbox Available', field: "#lockbox-chkbox"}),
    new Element({ name: 'Code', field: "#lockbox-code"}),
    new Element({ name: 'Location', field: "#lockbox-location"}),
    new Element({ name: 'Comments', field: "#comments-input"}),
    new Element({ name: 'Next', field: "#nextBtn" }),
  ]
);


export class PhotographerCommentsPage extends LCPage {
  constructor() {
    super('Photographer Comments', [photographerCommentsElements]);
  }
}
