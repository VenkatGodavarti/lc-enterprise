import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AllOrders } from '../models';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

export enum OrderFilterTypes {
  mine = 'mine',
  notMine = 'notMine',
  completed = 'completd'
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) { }

  getOrders(requestType: OrderFilterTypes, params?: { search?: string, sortColumn?: string, sortDir?: string, page?: number, pageSize?: number}): Observable<{orders: AllOrders[], totalCount: number}> {
    const allParams: any = params || {};
    allParams.requestType = requestType;
    const httpParams = new HttpParams({fromObject: allParams});
    httpParams.append('requestType', requestType.toString());
    return this.httpClient.get<AllOrders[]>(`${environment.apiBaseURL}marketing-orders/all`, { params: httpParams, observe: 'response'})
      .pipe(map(response => {
        const orders = response.body.map(order => new AllOrders(order));
        const totalCount = response.headers.get('total-count');
        return { orders, totalCount: +totalCount}
      }));
  }
}
