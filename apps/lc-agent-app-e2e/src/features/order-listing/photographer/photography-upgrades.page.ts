import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const photographyUpgradesElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Upgrades Available', field: "#upgrades-label"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Showcase Upgrade', field: "#card-option-showcase" }),
    new Element({ name: 'Twilight Upgrade', field: "#card-option-twilight" }),
    new Element({ name: 'Next', field: "#nextBtn" }),
  ]
);


export class PhotographyUpgradesPage extends LCPage {
  constructor() {
    super('Photography Upgrades', [photographyUpgradesElements]);
  }
}
