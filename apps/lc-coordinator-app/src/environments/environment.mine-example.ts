export const environment = {
  // App configuration
  production: false,
  envName: 'mine',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'http://localhost:4600/',
  apiBaseURL: 'http://localhost:4100/api/v1/',
  photosBaseURL: 'http://localhost:4300/api/v1/',
  notifyBaseURL: 'http://localhost:4800/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajznyw1hcD45l890h7',
  oktaRedirectUri: 'http://localhost:4600/implicit/callback',
};
