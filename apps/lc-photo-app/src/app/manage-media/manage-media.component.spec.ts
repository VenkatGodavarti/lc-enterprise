import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMediaComponent } from './manage-media.component';
import { TestingModule } from '@lc/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TabSetComponent } from '../../../../../libs/shared-components/src/lib/tabset/tab-set.component';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;

describe('ManageMediaComponent', () => {
  let component: ManageMediaComponent;
  let fixture: ComponentFixture<ManageMediaComponent>;
  let store: SpyObj<Store<any>>;


  beforeEach(async(() => {
    store = jasmine.createSpyObj('Store', ['pipe', 'select', 'dispatch']);
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule,
        SharedComponentsUiModule,
      ],
      declarations: [ ManageMediaComponent ],
      providers: [
        TabSetComponent,
        { provide: ActivatedRoute, useValue: {
            snapshot: {
              data: {
                navLinks: [],
                activeId: null
              },
              paramMap: convertToParamMap({
                photoOrderId: '123'
              }),
              queryParamMap: convertToParamMap({
                photographerName: 'My photo shop name'
              })
            }
          }
        },
        { provide: Store, useValue: store }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
