import { Element, ElementMap, LCPage } from '../../../../../e2e/support';

export class MyTeamContactPreferencesPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Photo', field: '#team-photo-input'}),
      new Element({ name: 'Name', field: '#team-name-input'}),
      new Element({ name: 'Phone', field: '#team-phone-input'}),
      new Element({ name: 'Email', field: '#team-email-input'}),
      new Element({ name: 'Website', field: '#team-website-input'}),
      new Element({ name: 'License', field: '#team-license-input'}),
      new Element({ name: 'Logo', field: '#team-logo-input'}),
    ]
  );


  constructor() {
    super('Team Contact Preferences', [MyTeamContactPreferencesPage.elements]);
  }

}
