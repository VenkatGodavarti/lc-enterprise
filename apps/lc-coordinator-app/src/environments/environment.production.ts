export const environment = {
  // App confiuration
  production: true,
  envName: 'production',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'http://1.2.3.4:4200/',
  apiBaseURL: 'http://1.2.3.4:4100/api/v1/',
  photosBaseURL: 'http://1.2.3.4:4300/api/v1/',
  notifyBaseURL: 'http://100.25.65.5:4800/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajcqv31m3erdpr00h7',
  oktaRedirectUri: 'http://1.2.3.4:4200/implicit/callback',
};
