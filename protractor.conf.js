// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { join } = require('path');
const { SpecReporter } = require('jasmine-spec-reporter');

const summaryJsonPath = join(__dirname, './/reports/e2e/summary.json');

exports.config = {
  allScriptsTimeout: 11000,

  capabilities: {
    'browserName': 'chrome',
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',

  // Use a custom framework adapter and set its relative path
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),

  // require feature files
  specs: [
    'apps/lc-agent-app-e2e/src/features/**/*.feature' // accepts a glob
  ],

  SELENIUM_PROMISE_MANAGER: false,

  cucumberOpts: {
    // require step definitions
    require: [
      'apps/lc-agent-app-e2e/src/**/*.ts',
    ],
    // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    tags: [],
    // <boolean> fail if there are any undefined or pending steps
    strict: true,
    // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    format: [
      // 'pretty',
      // 'pretty:reports/e2e/summary.txt',
      'json:'+summaryJsonPath
    ],
    // <boolean> invoke formatters without executing steps
    dryRun: false,
    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    compiler: []
  },

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },

  // Enable TypeScript for the tests
  onPrepare() {
    require('ts-node').register({
      project: 'apps/lc-agent-app-e2e/tsconfig.e2e.json'
    });
  }
};
