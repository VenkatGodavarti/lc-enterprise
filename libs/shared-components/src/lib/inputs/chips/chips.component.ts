import { Component, forwardRef, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { InputField } from '../input-field';
import { Observable, isObservable, of } from 'rxjs';

@Component({
  selector: 'lc-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChipsComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ChipsComponent),
      multi: true,
    }
  ]
})
export class ChipsComponent extends InputField implements OnChanges {
  @Input()
  label: string;

  @Input()
  placeholder = '';

  @Input()
  emptyPlaceholder = '';

  @Input()
  allowCustomInput = false;

  @Input()
  displayPath: string;

  @Input()
  valuePath: string;

  @Input()
  options: any | Observable<any[]>;

  @Output()
  public readonly blur = new EventEmitter<void>();

  @Output()
  public readonly textChange = new EventEmitter<string>();

  @Output()
  public readonly selectedChanged = new EventEmitter<any[]>();

  @Input()
  selectedValues: any[];

  @Input()
  max: number;

  public options$: any | Observable<any[]>;

  constructor(sanitizer: DomSanitizer) {
    super(sanitizer);
  }

  public ngOnChanges(changes: SimpleChanges) {
    if(changes.options) {
      if(isObservable(this.options)) {
        this.options$ = this.options;
      } else {
        this.options$ = of(this.options);
      }
    }
  }

  public onBlur() {
    this.blur.emit();
    this.executeOnTouched();
  }

  public onItemAdded(event: {display: any, value: any}) {
    this.updateSelection();
  }

  public onItemRemoved(event: {display: any, value: any}) {
    this.updateSelection();
  }

  public onSelected(event: {display: any, value: any}){
    this.updateSelection();
  }

  public onTextChange(text) {
    this.textChange.emit(text);
  }

  private updateSelection() {
    this.formControl.setValue(this.selectedValues);
    this.selectedChanged.emit(this.selectedValues);
  }
}
