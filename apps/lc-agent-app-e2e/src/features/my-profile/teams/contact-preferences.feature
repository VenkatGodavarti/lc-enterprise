@login-before-all
Feature: I want to edit my team contact preferences

  Background:
    Given the following team profile exists
      | Name                 | coldwell                    |
      | Photo URL            | http://localhost/team/photo |
      | Photo Metadata       | {"ratio": "2"}              |
    When I go to the Team contact Preferences page in My Profile
    Then the Team Contact Preferences page from My Profile is loaded

  @teams
  @team-profile
  @teams-contact-preferences
  Scenario: Verify Contact Preference Options
    #Validate default options
    Then the "Photo" text is "Team"
    And the "Name" text is "Team"
    And the "Phone" text is "Team"
    And the "Email" text is "Team"
    And the "Website" text is "Team"
    And the "License" text is "Primary Agent"
    And the "Logo" text is "Team"

    # Validate availableOptions
    # Note that Photo options are now being validated as which options are selectable in the dropdown rather than just present
    Then the selectable "Photo" options are "Team, First Two Members, None"
    And the selectable "Name" options are "Team, First Two Members, Team with Primary, Team with First Two Members, None"
    And the selectable "Phone" options are "Team, First Two Members, Team with Primary, Team with First Two Members, Office, None"
    And the selectable "Email" options are "Team, First Two Members, Team with Primary, None"
    And the selectable "Website" options are "Team, First Two Members, Team with Primary, None"
    And the selectable "License" options are "Primary Agent, First Two Members, Team with Primary, None"
    And the selectable "Logo" options are "Team, Primary Agent, None"
