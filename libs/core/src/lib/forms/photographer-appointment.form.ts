import { FormControl  } from '@angular/forms';
import { BaseForm } from './base.form';
import { LockboxForm } from './lockbox.form';
import { PhotographerAppointment, Lockbox } from '../models/photographer-appointment.model';
import { PhotographerForm } from './photographer.form';

export class PhotographerAppointmentForm extends BaseForm {
  // accessors are not required, but reduce code and risk of not retrieving the proper control
  public get comments(): string { return this.get('comments').value; }
  public get lockboxAvailable(): boolean { return this.get('lockboxAvailable').value; }
  public get lockbox(): Lockbox { return this.get('lockbox').value; }

  constructor(value?: PhotographerAppointment){
    super({
      lockboxAvailable: new FormControl(false),
      comments: new FormControl(null),
      lockbox: new LockboxForm(value ? value.lockbox : null),
      photographer: new PhotographerForm(value ? value.photographer : null)
    });

    if(value) {
      const model = Object.assign({}, value);
      delete model.lockbox;
      this.patchValue(model);
    }
  }
}
