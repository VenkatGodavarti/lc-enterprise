import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import {
  ActionTypes,
  PackageInstance,
  PackageService,
  WorkflowAction,
  ProductCategory,
  OrderListingFlowStateService,
  SELECTEDORDER,
  MarketingOrder,
  OrderActionTypes,
  UpdateOrder,
  UpdateOrderComplete, UIConstants
} from '@lc/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { ActionsSubject, Store } from '@ngrx/store';
import { tap, map, filter } from 'rxjs/operators';
import { PromptDialogService, DialogAction } from '@lc/shared-components-ui';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'lc-select-marketing-package',
  templateUrl: './select-marketing-package.component.html',
  styleUrls: ['./select-marketing-package.component.scss']
})
export class SelectMarketingPackageComponent implements OnInit, OnDestroy {
  availableCategories = [
    ProductCategory.MULTIMEDIA,
    ProductCategory.PRINT,
    ProductCategory.DIGITAL,
    ProductCategory.OTHER
  ];

  marketingOrder$: Observable<MarketingOrder>;
  marketingOrder: MarketingOrder;
  subjectSub: Subscription;

  // Flag to prevent "flicker" of selection change on button press
  justSelected: boolean;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private dialogService: PromptDialogService,
              private packageService: PackageService,
              private orderListingFlowService: OrderListingFlowStateService,
              private actionSubject: ActionsSubject,
              private store: Store<any>) {
    // setting state on constructor will handle any browser navigation
    const marketingOrderStore$ = store.select<MarketingOrder>(SELECTEDORDER);

    // select the packages form the server
    const packages$ = this.packageService.getAllAvailablePackages().pipe(
      map(packages => packages.sort((pkgA, pkgB) => pkgA.position - pkgB.position))
    );

    // create an observable to get the listing and the packages
    this.marketingOrder$ = combineLatest(marketingOrderStore$, packages$).pipe(
      tap(([order, packages]) => this.updateListingPackages(order, packages)),
      map(([order, packages]) => {
        this.marketingOrder = order;
        return order;
      })
    );

    this.justSelected = false;
  }

  ngOnInit() {
    this.subjectSub = this.actionSubject.pipe(
      ofType<UpdateOrderComplete>(OrderActionTypes.UpdateOrderComplete),
      filter(data => this.marketingOrder && this.marketingOrder._id === data.payload._id) // Only trigger after marketing order has been set and the ids match
    ).subscribe((data: UpdateOrderComplete) => {

      // created listing or updated with no package selected
      if (!data.payload.getOrderState()) {
        const pageRoute = `/order-listing/${this.marketingOrder._id}/${data.payload.selectedPackageCode}`;
        this.router.navigate([pageRoute]);
      } else {
        // TODO - create better workflow action to implement this code
        const newState = this.orderListingFlowService.createNewFlowState(data.payload);
        this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionInitialize,
          { state: newState },
          convertToParamMap({ id: data.payload._id, pid: data.payload.selectedPackageCode })));
        }
      });
  }

  ngOnDestroy(): void {
    if (this.subjectSub) {
      this.subjectSub.unsubscribe();
    }
  }

  marketingPackageSelected(marketingOrder: MarketingOrder, selectedPkg: PackageInstance) {
    this.justSelected = true;
    const id = marketingOrder ? marketingOrder._id : this.activatedRoute.snapshot.params['id'];
    const listing = marketingOrder.listing;

    // save selected package
    const packageCode = selectedPkg ? selectedPkg.code : null;
    if(marketingOrder.selectedPackageCode !== packageCode) {

      if(marketingOrder.selectedPackageCode) {
        // If package code was previously set, prompt user
        const message = 'You will lose all progress on your order, if you continue. Do you want to continue?';
        const okAction = new DialogAction(UIConstants.YES, () => this.changePackage(marketingOrder, packageCode))
        this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)])
      } else {
        // Otherwise just change package
        this.changePackage(marketingOrder, packageCode);
      }
    } else {
      //Order has not been updated but still redirect like previous code was doing
      this.store.dispatch(new UpdateOrder(marketingOrder));
    }
  }

  private updateListingPackages(order: MarketingOrder, packages: PackageInstance[]) {
    // if the listing is new and has no packages already assigned, assign the packages
    if(order && order.listing && (order.availablePackages ||[]).length === 0) {
      order.setAvailablePackages(packages);
    }
  }

  private changePackage(marketingOrder: MarketingOrder, packageCode: string) {
    // Package changed. Clear order state and product specific information.
    marketingOrder.setOrderState(null);

    // Clear out old package related information
    marketingOrder.clearPackageSpecificData();

    // Don't change selectedPackageCode until we clear out previous package information
    marketingOrder.selectedPackageCode = packageCode;
    this.store.dispatch(new UpdateOrder(marketingOrder));
  }
}
