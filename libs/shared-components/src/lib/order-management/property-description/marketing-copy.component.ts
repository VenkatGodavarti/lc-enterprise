import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MarketingOrder,
  ProductInstance,
  SELECTEDORDER,
  UpdateOrderProductDescription,
  UpdateOrder,
  TemplateService, TemplateInstanceMetaData
} from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import {  Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDescription } from '@lc/core';

@Component({
  selector: 'lc-marketing-copy',
  templateUrl: './marketing-copy.component.html',
  styleUrls: ['./marketing-copy.component.scss']
})
export class MarketingCopyComponent implements OnInit, OnDestroy {

  marketingCopyForm: FormGroup;

  marketingOrderSub: Subscription;
  marketingOrder$: Observable<MarketingOrder>;
  marketingOrder: MarketingOrder;

  product: ProductInstance;
  templateMetaDataSub: Subscription;
  templateMetaData: TemplateInstanceMetaData;

  orderId: string;
  packageCode: string;
  productCode: string;

  headlinePlaceHolder = '';
  bodyPlaceHolder = '';

  // used for the lc-text-checker component
  headlineCheckerOptions: any;
  bodyCheckerOptions: any;

  constructor(private store: Store<any>,
              private templateInfoService: TemplateService,
              private readonly router: Router,
              private readonly activatedRoute: ActivatedRoute) {
    // example of using observable from store
    this.marketingOrder$ = this.store.select(SELECTEDORDER);
    this.productCode = this.activatedRoute.parent.snapshot.paramMap.get('product_code');
  }

  ngOnInit() {
    this.initForm();
    const self = this;

    this.marketingOrderSub = this.marketingOrder$.subscribe( marketingOrder => {
      if (marketingOrder) {
        this.marketingOrder = marketingOrder;
        if (self.productCode) {
          this.product = marketingOrder.getSelectedPackage().getProduct(self.productCode);
        }
  
        self.orderId = marketingOrder._id;
        self.packageCode = marketingOrder.selectedPackageCode;
        self.populateForm();
      }
    });

    if(self.productCode) {
      this.templateMetaDataSub = this.templateInfoService.getTemplateInfo(self.productCode).subscribe(metaData => {
        this.templateMetaData = metaData;
        this.createTextChecking(metaData);
      });
    }
  }

  ngOnDestroy(): void {
    if(this.marketingOrderSub) {
      this.marketingOrderSub.unsubscribe();
    }
    if(this.templateMetaDataSub) {
      this.templateMetaDataSub.unsubscribe();
    }
  }

  initForm() {
    let fg: FormGroup;
    if (this.productCode) {
      fg = new FormGroup({
        headline: new FormControl(''),
        body: new FormControl('')
      });
    }
    else {
      fg = new FormGroup({
        headline: new FormControl('', [Validators.required]),
        body: new FormControl('', [Validators.required])
      });
    }
    this.marketingCopyForm = fg;
  }

  private populateForm() {
    this.marketingCopyForm.setValue(this.buildFormValues());
  }

  private buildFormValues() {
    let values = {
      headline: this.marketingOrder.marketingCopyHeadline[0] ? this.marketingOrder.marketingCopyHeadline[0] : '',
      body: this.marketingOrder.marketingCopyBody[0] ? this.marketingOrder.marketingCopyBody[0] : ''
    };
    if (this.productCode) {

      this.headlinePlaceHolder = values.headline;
      this.bodyPlaceHolder = values.body;

      const productHeadline = this.product.marketingCopyHeadline[0];
      if(productHeadline && productHeadline.length > 0) {
        values.headline = productHeadline;
      }
      const productBody = this.product.marketingCopyBody[0];
      if(productBody && productBody.length > 0) {
        values.body = productBody;
      }
    }
    return values;
  }

  // Creates the configuration options for use with the lc-text-checker component
  private createTextChecking(metaData) {
    if(metaData) {
      // for headline copy
      const headlineMetaData = metaData.headlineInfo[0];
      this.headlineCheckerOptions = this.buildCheckerOptions(headlineMetaData, 'headline-id');

      // for body copy
      const bodyMetaData = metaData.bodyInfo[0];
      this.bodyCheckerOptions = this.buildCheckerOptions(bodyMetaData, 'body-id');
    }
  }

  private buildCheckerOptions(metaData, id) {
    const options = {};
    options['id'] = id;
    options['height'] = metaData.height;
    options['width'] = metaData.width;
    options['fontPointSize'] = parseInt(metaData.fontPointSize);
    options['fontFamily'] = metaData.fontFamily;
    options['fontStyle'] = metaData.fontStyle;

    return options;
  }

  onSubmit(marketingOrder: MarketingOrder) {

    if (!this.marketingCopyForm.valid) {
      return;
    }
    if (this.productCode) {
      this.updateProductInstance(this.orderId, this.packageCode, this.productCode);
    } else {
      this.updateOrder(marketingOrder);
    }

    this.navigateToSummary();
  }

  private updateOrder(marketingOrder: MarketingOrder) {
    marketingOrder.marketingCopyHeadline = [this.marketingCopyForm.value.headline];
    marketingOrder.marketingCopyBody = [this.marketingCopyForm.value.body];

    this.store.dispatch(new UpdateOrder(marketingOrder));
  }

  private updateProductInstance(orderId, packageCode, productCode) {

    let headline = this.marketingCopyForm.value.headline;
    if (headline === this.marketingOrder.marketingCopyHeadline[0]) {
      headline = null;
    }
    let body = this.marketingCopyForm.value.body;
    if (body === this.marketingOrder.marketingCopyBody[0]) {
      body = null;
    }
    this.product.marketingCopyHeadline = headline ? [headline] : [];
    this.product.marketingCopyBody = body ? [body] : [];

    const description = new ProductDescription({
      orderId: orderId,
      packageCode: packageCode,
      productCode: productCode,
      marketingCopyHeadline: headline ? [headline] : [],
      marketingCopyBody: body ? [body] : []
    });

    this.store.dispatch(new UpdateOrderProductDescription(description));
  }

  onCancel(){
    this.navigateToSummary();
  }

  private navigateToSummary() {
    if (!this.productCode) {
      this.router.navigate(['../summary'], { relativeTo: this.activatedRoute });
    }
  }
}
