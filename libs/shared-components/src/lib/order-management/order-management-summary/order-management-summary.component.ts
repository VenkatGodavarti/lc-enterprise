import { Component, OnInit } from '@angular/core';
import { MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'lc-order-management-summary',
  templateUrl: './order-management-summary.component.html',
  styleUrls: ['./order-management-summary.component.scss']
})
export class OrderManagementSummaryComponent implements OnInit {

  marketingOrder$: Observable<MarketingOrder>;

  constructor(private store: Store<any>) {
    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));
  }

  ngOnInit() {
  }

}
