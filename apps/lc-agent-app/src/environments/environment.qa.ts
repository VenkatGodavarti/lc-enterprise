export const environment = {
  production: false,
  envName: 'qa',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'https://lc.qa.listing-concierge.realogy.com/',
  apiBaseURL: 'https://lc.qa.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.qa.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.qa.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajgzl61lniml6gm0h7',
  oktaRedirectUri: 'https://lc.qa.listing-concierge.realogy.com/implicit/callback',
};
