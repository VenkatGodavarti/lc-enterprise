import { Given, Then, When } from 'cucumber';
import { Configuration as config, Context, ContextKeys, LCPage } from '../../../../../../../e2e';
import {
  by,
  ElementFinder,
  element,
  WebElement,
  $$,
  ElementArrayFinder,
  browser,
  ExpectedConditions as EC
} from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

export enum PhotoKeys {
  favoritePhotoCount = 'favoritePhotoCount',
  originalSrc = 'originalSrc',
  destinationSrc = 'destinationSrc'
}

Given(/^The "(.*)" is greater than "(\d*)"$/,  { timeout: config.timeout },  async (elementName: string, count: number) => {
  const page = Context.get<LCPage>(ContextKeys.page);

  const element: ElementFinder = page.finder(elementName);
  const domElement = await element.getWebElement();
  const value = parseInt(await domElement.getText());

  Context.set(PhotoKeys.favoritePhotoCount, value);

  if(count > value) {
    // we need to favorite some photos before we continue
  }
});

Then(/^I should see only my favorite photos$/,  { timeout: config.timeout },  async () => {
  const favorites = Context.get(PhotoKeys.favoritePhotoCount);
  const allImages = element.all(by.css('.card-img img'));

  const total = await allImages.count();
  const hidden = await findHiddenPhotos().count();

  expect(favorites + hidden).to.be.equal(total);
});

Then(/^I should see all photos$/,  { timeout: config.timeout },  async () => {
  const allImages = element.all(by.css('.card-img img'));

  const total = await allImages.count();
  const hidden = await findHiddenPhotos().count();

  expect(total - hidden).to.be.equal(total);
});

When(/^I move photo from position "(\d*)" to "(\d*)"$/,  { timeout: config.timeout },  async (orig: number, dest: number) => {
  browser.wait(EC.visibilityOf(browser.element(by.id(`#photo-${orig}`))), 2000);

  // find the src of the photo in the orig and dest positions to retain src urls
  const originalImg = await getImgUrl(orig);
  expect(originalImg.length > 0).to.be.true;
  const destinationImg = await getImgUrl(dest);
  expect(destinationImg.length > 0).to.be.true;

  Context.set(PhotoKeys.originalSrc, originalImg);
  Context.set(PhotoKeys.destinationSrc, destinationImg);

  // drag-n-drop
  await simulateDragAndDrop(`#photo-${orig}`, `#photo-${dest}`);
});

Then(/^I see the "(.*)" photo in the "([1-9]*|previous|next)" place$/,  { timeout: 5000 },  async (orig, dest) => {
  // check to see that the destination image moved to the previous/next position
  let finalUrl: string;
  if(dest === 'previous' || dest === 'next') {
    const step = dest === 'next' ? parseInt(orig) + 1 : parseInt(orig) - 1;
    finalUrl = await getImgUrl(step);
    expect(finalUrl).to.be.equal(Context.get(PhotoKeys.destinationSrc));
  } else {
    finalUrl = await getImgUrl(dest);
    expect(finalUrl).to.be.equal(Context.get(PhotoKeys.originalSrc));
  }
});

When(/^I should see all favorite photos selected$/, { timeout: config.timeout }, async () => {
  const elements = $$('.select-img').filter(async (checkbox) => {
    return await checkbox.isSelected();
  });
  const selected = await elements.count();

  expect(selected).to.be.equal(Context.get(PhotoKeys.favoritePhotoCount));

});

When(/^I should see all photos selected$/, { timeout: config.timeout }, async () => {
  const allCheckboxes = await $$('.select-img').count();
  const checked = $$('.select-img').filter(async (checkbox) => {
    return await checkbox.isSelected();
  });
  const selected = await checked.count();

  expect(selected).to.be.equal(allCheckboxes);
});

Then(/^There should be "(\d*)" photos selected$/, { timeout: config.timeout }, async (expected) => {
  const checked = $$('.select-img').filter(async (checkbox) => {
    return await checkbox.isSelected();
  });
  const selected = await checked.count();

  expect(selected).to.be.equal(parseInt(expected));
});

function findHiddenPhotos(): ElementArrayFinder {
  return $$('.photo-container')
    .$$('.d-none')
    .filter(async (hiddenDiv) => {
      const element: WebElement = await hiddenDiv.getWebElement();
      const id: string = await element.getAttribute('id');
      const match = id.match(/^photo\-\d*$/);

      return match && id === match[0];
    });
}

async function getImgUrl(imageNumber) {
  const element = browser.element(by.id(`img-${imageNumber}`));
  let url = await element.getAttribute('src');

  if(!url || url.length === 0) {
    url = await getImgUrl(imageNumber);
  }

  return url;
}

/*
* @param target css selector of element to drag & drop
* @param destination css selector of destination element
*
* from: https://stackoverflow.com/questions/44205616/cannot-drag-and-drop-with-protractor-in-website-using-ng2-dragula/50854035
*/
async function simulateDragAndDrop(target: string, destination: string): Promise<void> {
  await browser.executeScript((target, destination) => {
    let getEventOptions = (el, relatedElement) => {
      //coordinates from destination element
      const coords = el.getBoundingClientRect();
      const x = coords.x || coords.left;
      const y = coords.y || coords.top;
      return {
        x: x,
        y: y,
        clientX: x,
        clientY: y,
        screenX: x,
        screenY: y,
        //target reference
        relatedTarget: relatedElement
      };
    };

    let raise = (el, type, options?) => {
      const o = options || { which: 1 };
      const e = document.createEvent('Event');
      e.initEvent(type, true, true);
      Object.keys(o).forEach(apply);
      el.dispatchEvent(e);
      function apply(key) {
        e[key] = o[key];
      }
    };

    let targetEl = document.querySelector(target);
    let destinationEl = document.querySelector(destination);
    let options = getEventOptions(destinationEl, targetEl);

    //start drag
    raise(targetEl, 'mousedown');
    raise(targetEl, 'mousemove');
    //set event on location
    raise(destinationEl, 'mousemove', options);
    //drop
    raise(destinationEl, 'mouseup', options);

  }, target, destination);
}
