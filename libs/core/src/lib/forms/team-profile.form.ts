import { FormControl, FormArray, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { PhoneNumberForm } from './phone-number.form';
import { TeamContactPreferencesForm } from './team-contact-preferences.form';
import { TeamMemberForm } from './team-member.form';
import { urlValidator } from '../validators/url.validator';
import { TeamMember } from '../models';
import { PhotoInformationForm } from './photo-information.form';

// export type MemberFormArray = Array<MemberForm>;
export class TeamProfileForm extends BaseForm {
  get teamLogoURL() { return this.get('teamLogoURL').value; }

  constructor(value?: any){
    super({
      _id: new FormControl(),
      name: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
      slogan: new FormControl(null, [Validators.maxLength(30)]),
      phone: new PhoneNumberForm(),
      email: new FormControl(null, [Validators.required, Validators.email]),
      websiteURL: new FormControl(null, [Validators.required, urlValidator()]),
      ownerId: new FormControl(),
      teamPhoto: new PhotoInformationForm(value ? value.teamPhoto : null),
      teamLogoURL: new FormControl(),
      members: new FormArray([]),
      contactPreferences: new TeamContactPreferencesForm(),
    });

    if(value) {
      this.patchValue(value);
      const members = value.members as Array<TeamMember> || [];
      members.forEach(member => (this.get('members') as FormArray).push(new TeamMemberForm(member)));
    }
  }
}
