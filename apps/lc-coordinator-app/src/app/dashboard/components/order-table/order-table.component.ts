import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter, ContentChild, TemplateRef } from '@angular/core';
import { Observable, isObservable, of } from 'rxjs';
import { SortEvent } from '../../directives/sortable.directive';
import { MarketingOrder } from '@lc/core';

// These are the available columns names for the table.
export type OrderTableColumns = 'status' | 'address' | 'area' | 'agent' | 'date' | 'package' | 'copy' | 'photos' | 'coordinator' | 'actions';

@Component({
  selector: 'lc-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss']
})
export class OrderTableComponent implements OnInit, OnChanges {
  @ContentChild('actions')
  actionTemplate: TemplateRef<any>;

  @Input()
  orders: any[] | Observable<any[]>;

  // Columns to display on the table. If specified, will display. Otherwise will not render.
  @Input()
  columns: OrderTableColumns[] = ['status', 'address', 'area', 'agent', 'date', 'package', 'copy', 'photos', 'coordinator', 'actions'];

  @Input()
  showRequestButtons: boolean;

  orders$: Observable<any>;

  @Output()
  readonly sort = new EventEmitter<SortEvent>();

  constructor() {

  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.orders) {
      // We are using observables for the table. If we pass in a plain array, wrap in observable
      if(isObservable(this.orders)) {
        this.orders$ = this.orders;
      } else {
        this.orders$ = of(this.orders);
      }
    }
  }

  onSort(sortEvent: SortEvent) {
    // When grid is sorted, emit an event
    this.sort.emit(sortEvent);
  }

  onRequestCopy(order: MarketingOrder) {
    console.log(`Request copy placeholder ${order._id}`);
  }

  onRequestPhotos(order: MarketingOrder) {
    console.log(`Request photos placeholder ${order._id}`);
  }
}
