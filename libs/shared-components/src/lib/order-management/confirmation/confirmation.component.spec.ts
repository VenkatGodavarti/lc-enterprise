import { ComponentFixture, TestBed, tick, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfirmationComponent } from './confirmation.component';
import {
  ApiService,
  LoggerFactory,
  Listing,
  MarketingOrder,
  SELECTEDORDER
} from '@lc/core';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreMock, TestingModule } from '@lc/testing';
import { Store } from '@ngrx/store';

describe('ConfirmationComponent', () => {
  let component: ConfirmationComponent;
  let fixture: ComponentFixture<ConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TestingModule
      ],
      declarations: [ConfirmationComponent],
      providers: [
        ApiService,
        LoggerFactory
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(SELECTEDORDER, new MarketingOrder({_id: '5c0ab26a1e045b7673b8c577'})); // set default state
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call #manageOrder',() => {
    spyOn(component.router, 'navigate').and.returnValue(true);
    const mOrder = createMarketingOrder();
    component.manageOrder(mOrder);

    const expectedUrl = `/manage-order/${mOrder._id}`;
    expect(component.router.navigate).toHaveBeenCalledWith([expectedUrl]);
  });

  function createMarketingOrder() {
    const marketingOrder:any  = new MarketingOrder();
    marketingOrder._id = "5c0ab26a1e045b7673b8c577";
    marketingOrder.id = "5c0ab26a1e045b7673b8c577";
    marketingOrder.listing = new Listing();
    return marketingOrder;
  }

  function createListing() {
    const listing: any = new Listing();
    listing._id = "5c0aa2247a931a709f8b15ea";
    listing.id = "5c0aa2247a931a709f8b15ea";
    listing.marketingOrder = createMarketingOrder();
    return listing;
  }
});
