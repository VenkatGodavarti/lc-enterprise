import { http } from './http';
import { Configuration as config } from '../configuration';

export class PackagesClient {
  static async getMyPackages() {
    return http.get(`${config.apiBaseURL}/package-instances/my-packages`).then(response => response.data);
  }
}
