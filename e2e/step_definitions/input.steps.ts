
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { browser, by, ExpectedConditions as EC, protractor } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

When(/^I enter "(.*)" into the "(.*)"$/, {timeout: config.timeout }, async (value, fieldName) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.element(fieldName);
  await element.setValue(value);
});


Then(/^the "(.*)" value is "(.*)"$/, {timeout: config.timeout }, async (fieldName, value) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const field = page.finder(fieldName);

  await browser.wait(EC.elementToBeClickable(field), 10000);
  const fieldValue = await field.getAttribute('value');
  expect(fieldValue).to.be.equal(value);
});


Then(/^the "(.*)" has an error containing "(.*)"$/, {timeout: config.timeout }, async (fieldName, expectedText) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const field = page.finder(fieldName);

  // Using xpath to grab a hold of the next sibling that is a small, error
  const errorElement = field.element(by.xpath(`following-sibling::small[contains(@class,'invalid-feedback')]`));
  const errorText = await errorElement.getAttribute('textContent');

  expect(errorText).to.have.string(expectedText);
});


When(/^I set the (.*) to "(.*)"$/, {timeout: config.timeout }, async (fieldName, value) => {

  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.element(fieldName);
  await element.clear();
  await element.setValue(value);
});


Then(/^the (.*) value is set to "(.*)"$/, {timeout: config.timeout }, async (fieldName, value) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.element(fieldName);
  const matches = await element.hasValue(value);

  expect(matches).to.be.equal(true);
});
