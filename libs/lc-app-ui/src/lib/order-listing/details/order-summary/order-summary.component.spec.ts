import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { OrderSummaryComponent } from './order-summary.component';
import { MarketingOrder, SELECTEDORDER } from '@lc/core';
import { StoreMock, TestingModule } from '@lc/testing';
import { Store } from '@ngrx/store';

describe('OrderSummaryComponent', () => {
  let component: OrderSummaryComponent;
  let fixture: ComponentFixture<OrderSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule
      ],
      declarations: [ OrderSummaryComponent ],
      providers: [
      ]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(SELECTEDORDER, new MarketingOrder()); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
