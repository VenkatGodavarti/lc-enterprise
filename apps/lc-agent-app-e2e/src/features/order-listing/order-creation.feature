@login-before-all
Feature: I want to create and edit orders

Background:
  Given I am logged in to Listing Concierge

@cleanup-order
Scenario: Create a new listing with the Gold Package
  When I go to the Main page in Listing Dashboard
  And I wait for the Add Listing button to load
  Then the Add Listing button is present
  When I click on the Add Listing button

  Then the Add New Listing page from Listing Dashboard is loaded

  #Validate no errors are shown when form is untouched
  Then the "Address Error" text is empty
  Then the "City Error" text is empty
  Then the "State Error" text is empty
  Then the "Zip Code Error" text is empty

  # Validate Submit button is enabled and that clicking on it displays the validation errors
  When I click on the Continue button

  # Then the Add New Listing page from Listing Dashboard is still loaded
  Then the "Address Error" contains "required"
  Then the "City Error" contains "required"
  Then the "State Error" contains "required"
  Then the "Zip Code Error" contains "required"

  # Enter in the form validation
  When I enter data into the page form
  | Address         | 139 Park Drive  |
  | Unit Number     |                 |
  | City            | San Antonio     |
  | State           | TX              |
  | Zip Code        | 78212           |

  # Validate Navigation to the Select Marketing Packages Page
  And I click on the Continue button
  Then the Select Marketing Package page from Order Listing is loaded

  # Validate different Flyer quantities on the Select Marketing Packages Page
  Then I wait for the Gold Flyer Qty element to load
  And the "Silver Flyer Qty" text is "50"
  And the "Gold Flyer Qty" text is "100"
  And the "Platinum Flyer Qty" text is "150"

  #Validate Postcard qty
  And the "Silver Postcard Qty" text is "50"
  And the "Gold Postcard Qty" text is "50"
  And the "Platinum Postcard Qty" text is "50"

  # Validating that navigate home and back into order picks up at proper step
  When I click on the Home button
  Then the Main page from Listing Dashboard is loaded
  When I click the Complete Order button on the last order
  Then the Select Marketing Package page from Order Listing is loaded

  # Validate Select Marketing Package navigates to listing details
  And I wait for the Order Gold button to load
  When I click on the Order Gold button
  Then the Order Listing Details page from Order Listing is loaded

  # Validating that navigate home and back into order picks up at proper step
  When I click on the Home button
  Then the Main page from Listing Dashboard is loaded
  When I click the Complete Order button on the last order
  Then the Order Listing Details page from Order Listing is loaded

  # Validate Listing Details navigates to the Order Details Page and Continue is enabled on proper step
  And I wait for the Listing Details Continue button to load
  When I click on the Listing Details Continue button
  Then the Order Listing Property page from Order Listing is loaded

  # Validating that navigate home and back into order picks up at proper step
  When I click on the Home button
  Then the Main page from Listing Dashboard is loaded
  When I click the Complete Order button on the last order
  Then the Order Listing Details page from Order Listing is loaded

  # Validate it Navigates back into Property Details and fill out form
  And I wait for the Listing Details Continue button to load
  When I click on the Listing Details Continue button
  Then the Order Listing Property page from Order Listing is loaded

  # Fill out Property Detail Form
  When I wait for the Next button to load
  And I enter data into the page form
  | Property Type   | Single Family   |
  | Listing Status  | Pending         |
  | List Price      | 1199000         |
  | Square Feet     | 3304            |
  | Lot Size        | .25             |
  | List Date       | 04/22/2019      |
  | Close Date      | 04/22/2020      |

  When I click the Bedroom Increment button 3 times
  Then the "Bedroom Count" text is "3"
  When I click the Bathroom Increment button 10 times
  Then the "Bathroom Count" text is "2.5"

  # Validate the Property Details page navigates to the Agent page
  When I click on the Next button
  Then the Order Listing Agent page from Order Listing is loaded

  # Validate the agent page navigates to the order details page
  When I wait for the Next button to load
  When I click on the Single Agent button
  And I click on the Next button
  Then the Overview Photography page from Order Listing is loaded

  # Validating that navigate home and back into order picks up at proper step
  When I click on the Home button
  Then the Main page from Listing Dashboard is loaded
  When I click the Complete Order button on the last order
  Then the Overview Photography page from Order Listing is loaded

  # Validate it navigates back into Listing Details with the Professional Photography button
  When I wait for the Professional Photography Continue button to load
  And I click on the Professional Photography Continue button

  # Select Photographer Page Steps
  Then the Select Photographer page from Order Listing is loaded
  When I wait for the Cutting Edge button to load
  Then the "Cutting Edge" button exists
  And the "Fabulous Photo" button exists
  And the "Quick Photo" button exists
  And the "Next" button is disabled

  When I click on the Cutting Edge button
  Then the "Next" button is enabled

  #Navigate to the Photography Upgrades page
  When I click on the Next button
  Then the Select Photography Upgrades page from Order Listing is loaded
  When I wait for the Next button to load

  # Validate you can click next without selecting upgrades
  Then the "Next" button is enabled

  # Then the Schedule Photographer screen displays
  When I click on the Next button
  Then the Schedule Photographer page from Order Listing is loaded
  Then the "Next" button is disabled

  # Click the first available appointment date
  When I click the first available appointment date
  Then the "Next" button is enabled

  # Then the Schedule Photographer screen displays
  When I click on the Next button
  Then the Photographer Comments page from Order Listing is loaded
  Then the "Next" button is enabled

  # Validate next is disabled if the lockbox info is not filled out yet
  When I check the "Lockbox Available" checkbox

  # Fill out form
  When I enter "123" into the "Code"
  And I enter "123" into the "Location"
  When I enter "Please wipe feet before entering" into the "Comments"
  Then the "Next" button is enabled

  # Click Next to navigate to order details overview
  When I click on the Next button
  Then the Overview Package Options page from Order Listing is loaded

  # Validate it navigates back into Listing Details with the Package Options button
  When I wait for the Package Options Continue button to load
  And I click on the Package Options Continue button

  # Order Listing Details Page Steps
  Then the Overview Checkout page from Order Listing is loaded
  When I wait for the Checkout Continue button to load
  When I click on the Checkout Continue button

  #Shipping Page Steps
  Then the Shipping page from Order Listing is loaded
  When I wait for the Next button to load
  And I enter data into the page form
  | Full Name       | 300 Alamo Plaza             |
  | Address 1       | 508 E Dove Ave              |
  | Address 2       | Suite 401 Shipping          |
  | City            | McAllen                     |
  | State           | TX                          |
  | Zip Code        | 78504                       |
  | Company         | Coldwell Banker LA Mansion  |
  | Phone           | 5551234567                  |
  | Email           | test-shipping@realogy.com  |
  And I click on the Next button

  Then the Billing page from Order Listing is loaded
  When I wait for the Place Order button to load
  And I enter data into the page form
  | Full Name       | 300 Alamo Plaza             |
  | Address 1       | 508 E Dove Ave              |
  | Address 2       | Suite 401 Billing           |
  | City            | McAllen                     |
  | State           | TX                          |
  | Zip Code        | 78504                       |
  | Company         | Coldwell Banker LA Mansion  |
  | Phone           | 5551234567                  |
  | Email           | test-billing@realogy.com   |
  When I click on the Place Order button

  Then the Confirmation page from Order Management is loaded

@billing-shipping-changes
@empty-profile
Scenario: Create a new listing with default shipping and billing addresses
  Given the user profile addresses contain Contact Info
  And the following orders exist
  | Name             | Alamo    |
  | Shipping Address |          |
  | Billing Address  |          |
  | Submitted        | false    |
  | Status           | OPEN     |
  And the order is on the Shipping step

  When I go to the Main page in Listing Dashboard
  Then the Main page from Listing Dashboard is loaded

  # Order Listing Details Page Steps
  When I click the Complete Order button on the last order
  Then the Overview Checkout page from Order Listing is loaded

  When I wait for the Checkout Continue button to load
  And I click on the Checkout Continue button

  # Shipping Page Steps
  Then the Shipping page from Order Listing is loaded

  # Shipping details pre-populated with address from profile
  When I wait for the Next button to load
  Then the "Full Name" value is "John Jones"
  And the "Address 1" value is "1234 Realty Circle NW"
  And the "Address 2" value is "Unit shipping #1"
  And the "City" value is "Santa Clara"
  And the "State" value is "CA"
  And the "Zip Code" value is "98765"
  And the "Company" value is "CB Realty"
  And the "Phone" value is "714.555.1111"
  And the "Email" value is "john.jones@realogy.com"

  # Change the shipping info
  And I enter data into the page form
    | Full Name       | Jonathan Jones              |
    | Address 1       | 508 E Dove Ave              |
    | Address 2       | Suite 401 Shipping          |
    | City            | McAllen                     |
    | State           | TX                          |
    | Zip Code        | 78504                       |
    | Company         | Coldwell Banker LA Mansion  |
    | Phone           | 5551234567                  |
    | Email           | test-shipping@realogy.com   |

  # Load Billing page
  When I click on the Next button
  Then the Billing page from Order Listing is loaded

  # Billing details pre-populated with address from profile
  When I wait for the Place Order button to load
  Then the "Full Name" value is "John Jones"
  And the "Address 1" value is "1234 Realty Circle NW"
  And the "Address 2" value is "Unit billing #2"
  And the "City" value is "Santa Clara"
  And the "State" value is "CA"
  And the "Zip Code" value is "98765"
  And the "Company" value is "CB Realty"
  And the "Phone" value is "714.555.1111"
  And the "Email" value is "john.jones@realogy.com"

  # Billing Page loads
  When I click on the Back button
  Then the Shipping page from Order Listing is loaded

  # Shipping details are populated with last entered data
  When I wait for the Next button to load
  Then the "Full Name" value is "Jonathan Jones"
  And the "Address 1" value is "508 E Dove Ave"
  And the "Address 2" value is "Suite 401 Shipping"
  And the "City" value is "McAllen"
  And the "State" value is "TX"
  And the "Zip Code" value is "78504"
  And the "Company" value is "Coldwell Banker LA Mansion"
  And the "Phone" value is "555.123.4567"
  And the "Email" value is "test-shipping@realogy.com"

  # Billing Page loads
  When I click on the Next button
  Then the Billing page from Order Listing is loaded

  # Billing details have same data as before -- from profile
  When I wait for the Place Order button to load
  Then the "Full Name" value is "John Jones"
  And the "Address 1" value is "1234 Realty Circle NW"
  And the "Address 2" value is "Unit billing #2"
  And the "City" value is "Santa Clara"
  And the "State" value is "CA"
  And the "Zip Code" value is "98765"
  And the "Company" value is "CB Realty"
  And the "Phone" value is "714.555.1111"
  And the "Email" value is "john.jones@realogy.com"

  # Click 'same as shipping address' checkbox to populate billing address with shipping info
  When I check the "Same As Shipping Address" checkbox
  Then the "Full Name" value is "Jonathan Jones"
  And the "Address 1" value is "508 E Dove Ave"
  And the "Address 2" value is "Suite 401 Shipping"
  And the "City" value is "McAllen"
  And the "State" value is "TX"
  And the "Zip Code" value is "78504"
  And the "Company" value is "Coldwell Banker LA Mansion"
  And the "Phone" value is "555.123.4567"
  And the "Email" value is "test-shipping@realogy.com"

  # Order Confirmation page loads
  When I click on the Place Order button
  Then the Confirmation page from Order Management is loaded


## TODO:
## 1.) Form Validation Errors
## 4.) Validate data saved
