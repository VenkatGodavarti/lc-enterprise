import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementSummaryComponent } from './order-management-summary.component';
import { Store } from '@ngrx/store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('OrderManagementSummaryComponent', () => {
  let component: OrderManagementSummaryComponent;
  let fixture: ComponentFixture<OrderManagementSummaryComponent>;
  let store: SpyObj<Store<any>>;

  beforeEach(async(() => {

    store = jasmine.createSpyObj('Store', ['pipe']);

    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        RouterTestingModule
      ],
      declarations: [ OrderManagementSummaryComponent ],
      providers: [
        { provide: Store, useValue: store }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
