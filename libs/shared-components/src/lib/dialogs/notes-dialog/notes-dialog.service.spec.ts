import { TestBed } from '@angular/core/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NotesDialogService } from './notes-dialog.service';

describe('NotesDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ NgbModule ],
    providers: [ NotesDialogService ]
  }));
  it('should be created', () => {
    const service: NotesDialogService = TestBed.get(NotesDialogService);
    expect(service).toBeTruthy();
  });
});
