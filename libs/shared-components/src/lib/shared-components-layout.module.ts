import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CoreModule } from "@lc/core";
import { LayoutWithoutSidenavComponent } from './layout/layout-without-sidenav/layout-without-sidenav.component';
import { LayoutNavbarComponent } from './layout/layout-navbar/layout-navbar.component';
import { LayoutFooterComponent } from './layout/layout-footer/layout-footer.component';
import { LayoutService } from './layout/layout.service';
import { AppService } from "./layout/app.service";
import { NotificationsMenuComponent } from './layout/notifications-menu/notifications-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    CoreModule
  ],
  declarations: [
    LayoutWithoutSidenavComponent,
    LayoutNavbarComponent,
    LayoutFooterComponent,
    NotificationsMenuComponent,
  ],
  exports: [
    NotificationsMenuComponent
  ],
  providers: [
    AppService,
    LayoutService
  ]
})
export class SharedComponentsLayoutModule {}
