import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MarketingOrder, SELECTEDORDER, UpdateOrderListing, AddressForm, Listing } from '@lc/core';
import { PropertyInfoForm } from '@lc/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lc-listing-details',
  templateUrl: './listing-details.component.html',
  styleUrls: ['./listing-details.component.scss']
})
export class ListingDetailsComponent implements OnInit {
  marketingOrder$: Observable<MarketingOrder>;
  marketingOrder: MarketingOrder;
  productCode: string;

  detailsForm: FormGroup;
  get addressForm(): AddressForm { return this.detailsForm ? this.detailsForm.get('address') as AddressForm : null; }
  get propertyForm(): PropertyInfoForm { return this.detailsForm ? this.detailsForm.get('property') as PropertyInfoForm : null; }

  submitted = false;

  constructor(private readonly router: Router,
              private readonly activatedRoute: ActivatedRoute,
              private store: Store<any>) {
    this.marketingOrder$ = this.store.select(SELECTEDORDER).pipe(
      tap(marketingOrder => {
        this.marketingOrder = marketingOrder;
        this.setForms(marketingOrder);
      })
    );
    this.productCode = this.activatedRoute.parent.snapshot.paramMap.get('product_code');
  }

  ngOnInit() {
  }

  setForms(order: MarketingOrder) {
    const listing = order ? order.listing : null;
    const address = listing ?  listing.address : null;
    this.detailsForm = new FormGroup({
      address: new AddressForm(address),
      property: new PropertyInfoForm(listing)
    })
  }

  onSubmit(listing) {
    if (!this.detailsForm.valid) {
      return;
    }

    const updatedListing = Object.assign({}, listing, ...this.propertyForm.getDirty());
    updatedListing.address = Object.assign({}, updatedListing.address, ...this.addressForm.getDirty());
    this.marketingOrder.listing = updatedListing;
    this.store.dispatch(new UpdateOrderListing(this.marketingOrder));

    this.submitted = true;
    this.navigateToSummary();
  }

  onCancel(){
    this.navigateToSummary();
  }
  private navigateToSummary() {
    if (!this.productCode) {
      this.router.navigate(['../summary'], { relativeTo: this.activatedRoute });
    }
  }
}
