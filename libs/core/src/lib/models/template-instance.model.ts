import { Deserializable } from './deserializable.model';
import { PageSize } from './page-size';
import { OutputMimeType } from './output-mime-type';
import { TemplateInstanceMetaData } from './template-meta-data.model';

export class TemplateInstance implements Deserializable<TemplateInstance> {

  title: string;
  description: string;
  size = PageSize.Letter;
  sides = 1;
  numberOfPhotos: number;
  thumbnailURL: string;
  templateURL: string;
  mimeType: OutputMimeType;
  metaData: TemplateInstanceMetaData;

  constructor(data?: any) {
    this.deserialize(data || {});
  }

  deserialize(input: any): TemplateInstance {
    Object.assign(this, input);
    return this;
  }
}
