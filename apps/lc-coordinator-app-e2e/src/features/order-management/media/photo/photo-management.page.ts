import { LCPage, ElementMap, Element } from '../../../../../../../e2e/support';

export class PhotoManagementPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Select All', field: '#select-all'}),
      new Element( { name: 'Hi Res', field: '#hi-res-download'}),
      new Element( { name: 'Low Res', field: '#low-res-download'}),
      new Element( { name: 'Delete Selected', field: '#delete-selected'}),
      new Element( { name: 'Favorites', field: '#favorites-filter'}),
      new Element( { name: 'Number of Favorites', field: '#favorites-count'})
    ]
  );

  static imgElements = new ElementMap( 'images',
    [
      new Element( { name: 'Image Order', field: 'order'}),
      new Element( { name: 'Image Order Input', field: 'order-input'}),
      new Element( { name: 'Image Selected', field: '.select-img'})
    ]
  );

  constructor() {
    super('Photos', [PhotoManagementPage.elements, PhotoManagementPage.imgElements]);
  }
}
