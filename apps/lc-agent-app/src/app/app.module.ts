import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

import {AppComponent} from './app.component';
import {NxModule} from '@nrwl/nx';
import {RouterModule} from '@angular/router';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { CoreModule, StateModule} from '@lc/core';
import { UserIdleModule } from 'angular-user-idle';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    RouterModule.forRoot(
      [{ path: '', loadChildren: '@lc/lc-app-ui#LcAppUiModule' }],
      { initialNavigation: 'enabled' , enableTracing: false }
    ),
    NgbModule.forRoot(),
    CoreModule.forRoot(),
    StateModule,

    // User activity timeouts
    // Ping value is not used
    UserIdleModule.forRoot({idle: 5 * 60, timeout: 30, ping: 10000})
  ],
  providers: [
      {
          provide: APP_BASE_HREF,
          useValue: '/'
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
