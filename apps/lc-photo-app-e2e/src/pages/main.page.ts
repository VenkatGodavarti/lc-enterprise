import { ElementMap, Element, LCPage } from '../../../../e2e';

export class MainPage extends LCPage {
  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Header', field: 'h1'}),
      new Element( { name: 'My Orders Tab', field: '#my-orders'}),
      new Element( { name: 'All Orders Tab', field: '#all-orders'}),
      new Element( { name: 'Completed Orders Tab', field: '#completed-orders'}),
    ]
  );

  constructor() {
    super('Main', [MainPage.elements]);
  }
}
