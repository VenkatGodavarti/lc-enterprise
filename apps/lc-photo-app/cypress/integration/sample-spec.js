describe('Example Test', function() {

  it('should contain three photographers', function() {
    cy.server();
    cy.route('GET', 'api/v1/photographers', 'fixture:photographers');
    cy.visit('http://localhost:4700');

    cy.get('#photographer-cards>div')
      .should("have.length", 3);
  });
});
