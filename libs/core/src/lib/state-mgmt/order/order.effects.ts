import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, filter, map, switchMap, withLatestFrom, tap } from 'rxjs/operators';
import { MarketingOrderService } from '../../services/marketing-order.service';

import {
  OrderActionTypes,
  UpdateOrderListing,
  UpdateOrder,
  UpdateOrderPartial,
  UpdateOrderPhotos,
  UpdateOrderPhotosComplete,
  UploadOrderPhotos,
  UpdateOrderProductDescription,
  LoadOrder,
  AddOrderComplete,
  GetOrderComplete,
  GetOrder,
  UpdateOrderComplete,
  SubmitOrder,
  SubmitOrderComplete,
  SELECTEDORDER,
  UpdateOrderPartialComplete,
  UpdateOrderListingComplete, UpdateOrderState, UpdateOrderPhoto,
  AddMarketingOrderMultimediaLink,
  AddMarketingOrderMultimediaLinkComplete,
  LoadOrderPhotos, DeleteOrder, DeleteOrderComplete
} from './order.actions';
import { ErrorData } from '../../errors/error-data';
import { NotificationEvent, NotificationEventService } from '../../notifications';
import { Listing, ListingPhoto, MarketingOrder, MultiMediaItem } from '../../models';
import { select, Store } from '@ngrx/store';
import { GlobalErrorHandler } from '../../errors/global-error-handler';
import { BaseEffects } from '../base.effects';

/**
 * The order effects are post state change actions that occur
 */
@Injectable()
export class OrderEffects extends BaseEffects {

  constructor(private actions$: Actions,
              private marketingOrderService: MarketingOrderService,
              private eventService: NotificationEventService,
              private store: Store<any>,
              errorHandler: GlobalErrorHandler) {
    super(errorHandler)

  }

  /**
   * Take action on AddOrder actions
   *  1. Update the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  addOrder: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<UpdateOrder>(OrderActionTypes.AddOrder),
    switchMap((action) => {
      return this.marketingOrderService.saveOrder(action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.AddOrder, action.payload, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      this.store.dispatch(new LoadOrder(order));
      const event = new NotificationEvent(OrderActionTypes.AddOrderComplete, OrderActionTypes.AddOrderComplete, order);
      this.eventService.getEventEmitter().emit(event);
      this.store.dispatch(new AddOrderComplete(order));
      return order;
    })
  );

  /**
   * Take action on GetOrder actions
   *  1. Get the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  getOrder: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<GetOrder>(OrderActionTypes.GetOrder),
    withLatestFrom(this.store.pipe(select<MarketingOrder>(SELECTEDORDER))),
    switchMap(([action, order]) => {
      if (order && order._id === action.payload._id) {
        return of(order);
      } else {
        return this.marketingOrderService.getOrder(action.payload._id).pipe(
          catchError(err => this.processCatchError(OrderActionTypes.GetOrder, action.payload, err))
        )
      }
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      const event = new NotificationEvent(OrderActionTypes.GetOrderComplete, OrderActionTypes.GetOrderComplete, order);
      this.eventService.getEventEmitter().emit(event);
      this.store.dispatch(new GetOrderComplete(order));
      return order;
    })
  );

  /**
   * Take action on UpdateOrderListing actions
   *
   *  1. Update the listing
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of listing
   *
   */
  @Effect({ dispatch: false })
  updateListing: Observable<Listing> = this.actions$.pipe(
    ofType<UpdateOrderListing>(OrderActionTypes.UpdateOrderListing),
    switchMap((action) => {
      if (!action.persist) {
        return of(action.payload);
      }
      return this.marketingOrderService.updateListing(action.payload._id, action.payload.listing).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderListing, action.payload, err))
      )
    }),
    filter(listing => !(listing instanceof ErrorData)),
    map((listing:Listing) => {
      const event = new NotificationEvent(OrderActionTypes.UpdateOrderListingComplete, OrderActionTypes.UpdateOrderListingComplete, listing);
      this.eventService.getEventEmitter().emit(event);
      this.store.dispatch(new UpdateOrderListingComplete(listing));
      return listing;
    })
  );

  /**
   * Take action on UpdateOrder actions
   *  1. Update the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  updateOrder: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<UpdateOrder>(OrderActionTypes.UpdateOrder),
    switchMap((action) => {
      return this.marketingOrderService.saveOrder(action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrder, action.payload, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      const event = new NotificationEvent(OrderActionTypes.UpdateOrderComplete, OrderActionTypes.UpdateOrderComplete);
      this.eventService.getEventEmitter().emit(event);
      this.store.dispatch(new UpdateOrderComplete(order));
      return order;
    })
  );

  /**
   * Take action on SubmitOrder actions
   *  1. Update the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  submitOrder: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<SubmitOrder>(OrderActionTypes.SubmitOrder),
    switchMap((action) => {
      return this.marketingOrderService.submitOrder(action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.SubmitOrder, action.payload, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      const event = new NotificationEvent(OrderActionTypes.SubmitOrderComplete, OrderActionTypes.SubmitOrderComplete);
      this.eventService.getEventEmitter().emit(event);
      this.store.dispatch(new SubmitOrderComplete(order));
      return order;
    })
  );

  /**
   * Take action on DeleteOrder actions
   *
   *  1. Delete the MarketingOrder
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Dispatch delete complete state
   *
   */
  @Effect({ dispatch: false })
  deleteOrder: Observable<any> = this.actions$.pipe(
    ofType<DeleteOrder>(OrderActionTypes.DeleteOrder),
    switchMap((action) => {
      return this.marketingOrderService.deleteOrder(action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.DeleteOrder, action.payload, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map(() => { this.store.dispatch(new DeleteOrderComplete()); })
  );

  /**
   * Take action on UpdateOrderPartial actions
   *  1. Update the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  updateOrderPartial: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<UpdateOrderPartial>(OrderActionTypes.UpdateOrderPartial),
    switchMap(({payload, fields}) => {
      return this.marketingOrderService.updateOrderPartial(payload, fields).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderPartial, {payload: payload, fields: fields}, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      this.store.dispatch(new UpdateOrderPartialComplete(order));

      const event = new NotificationEvent(OrderActionTypes.UpdateOrderPartialComplete);
      this.eventService.getEventEmitter().emit(event);
      return order;
    })
  );

  /**
   * Take action on LoadOrderPhotos actions
   *  1. Query for the photos attached to the marketing order
   *  2. If error, dispatch to error handler
   *  3. Map to data structure to preserve action data passed
   *  4. filter out chained error observables
   *  5. Send out success notification and return observable of order
   */
  @Effect({ dispatch: false })
  loadOrderPhotos: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<LoadOrderPhotos>(OrderActionTypes.LoadOrderPhotos),
    withLatestFrom(this.store.select<MarketingOrder>(SELECTEDORDER)),
    filter(([action, order]) => order != null),
    switchMap(([action, order]) => {
      return this.marketingOrderService.getOrderPhotos(order._id, null, action.productCode).pipe(
        map(photos => ({ photos: photos, action: action, order: order})),
        catchError(err => this.processCatchError(OrderActionTypes.LoadOrderPhotos, order, err))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map((result: {order: MarketingOrder, action: LoadOrderPhotos, photos: ListingPhoto[]}) => this.emitUpdateOrderPhotosComplete(result.order, result.photos, result.action.productCode))
  );


    /**
   * Take action on UpdateOrderPhoto actions
   *  1. Updates a single listing photos call to API
   *  2. If error, dispatch to error handler
   *  3. Map to data structure to preserve action data passed
   *  4. filter out chained error observables
   *  5. Send out success notification and return observable of order
   */
  @Effect({ dispatch: false })
  updateOrderPhoto: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<UpdateOrderPhoto>(OrderActionTypes.UpdateOrderPhoto),
    switchMap((action) => {
      return this.marketingOrderService.updatePhoto(action.payload._id, action.photo._id, action.photo).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderPhoto, action.payload, err)),
        map((photo: ListingPhoto) => ({ photo: photo, action: action}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    tap(result => {
      //Find the photo in the listing and update it.
      const photos = result.action.payload.getPhotosFor(result.action.productCode);
      const indexOfPhoto = photos.findIndex(photo => photo._id === result.photo._id);
      if(indexOfPhoto > -1){
        photos.splice(indexOfPhoto, 1, result.photo);
      }
    }),
    map(result => this.emitUpdateOrderPhotosComplete(result.action.payload, result.action.payload.photos, result.action.productCode))
  );

  /**
   * Take action on UpdateOrderPhotos actions
   *  1. Update the listing photos call to API
   *  2. If error, dispatch to error handler
   *  3. Map to data structure to preserve action data passed
   *  4. filter out chained error observables
   *  5. Send out success notification and return observable of order
   */
  @Effect({ dispatch: false })
  updateOrderPhotos: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<UpdateOrderPhotos>(OrderActionTypes.UpdateOrderPhotos),
    switchMap((action) => {
      action.payload.setPhotos(action.photos, action.productCode);
      return this.marketingOrderService.setPhotos(action.payload, action.productCode).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderPhotos, action.payload, err)),
        map(photos => ({ photos: photos, action: action}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map(result => this.emitUpdateOrderPhotosComplete(result.action.payload, result.photos, result.action.productCode))
  );

  // TODO - look at consolidating this with updateOrderPhotos above
  /**
   * Take action on UploadOrderPhotos actions
   *  1. Update the listing photos call to API
   *  2. If error, dispatch to error handler
   *  3. Map to data structure to preserve action data passed
   *  4. filter out chained error observables
   *  5. Send out success notification and return observable of order
   */
  @Effect({ dispatch: false })
  uploadOrderPhotos: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<UploadOrderPhotos>(OrderActionTypes.UploadOrderPhotos),
    switchMap((action) => {
      return this.marketingOrderService.addPhotoFromUploadAndUpdatePhotos(action.payload, action.photos,
        action.photographerId, action.productCode).pipe(
          catchError(err => this.processCatchError(OrderActionTypes.UploadOrderPhotos, action.payload, err)),
        map(photos => ({ photos: photos, action: action}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map(result => this.emitUpdateOrderPhotosComplete(result.action.payload, result.photos, result.action.productCode))
  );


  @Effect({ dispatch: false })
  updateOrderProductDescription: Observable<MarketingOrder> = this.actions$.pipe(
    ofType<UpdateOrderProductDescription>(OrderActionTypes.UpdateOrderProductDescription),
    switchMap((action) => this.marketingOrderService.saveOrderProductDescription(action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderProductDescription, action.payload, err)),
      )
    ),
    filter(order => !(order instanceof ErrorData)),
    map((order:MarketingOrder) => {
      const event = new NotificationEvent(OrderActionTypes.UpdateOrder, OrderActionTypes.UpdateOrder);
      this.eventService.getEventEmitter().emit(event);
      return order;
    })
  );

  /**
   * Add a multimedia link to the photoorder. This will call the photo-order API
   */
  @Effect({ dispatch: false })
  addMultimediaLink: Observable<any> = this.actions$.pipe(
    ofType<AddMarketingOrderMultimediaLink>(OrderActionTypes.AddMarketingOrderMultimediaLink),
    withLatestFrom(this.store.pipe(select<MarketingOrder>(SELECTEDORDER))),
    switchMap(([action, order]) => {
      return this.marketingOrderService.addMultimediaLink(order._id, action.payload).pipe(
        map(link => ({link, order})),
        catchError(err => this.processCatchError(OrderActionTypes.AddMarketingOrderMultimediaLink, {payload: action.payload}, err)),
      )
    }),
    filter(response => !(response instanceof ErrorData)),
    map((response: {link: MultiMediaItem, order: MarketingOrder}) => {
      this.store.dispatch(new AddMarketingOrderMultimediaLinkComplete(response.link));

      const event = new NotificationEvent(OrderActionTypes.AddMarketingOrderMultimediaLinkComplete);
      this.eventService.getEventEmitter().emit(event);
      return response.link;
    })
  );

  /**
   * Emits an event when the marketing order photos have been updated/loaded
   * @param order The marketing order the photos have been added to
   * @param photos The photos that have been updated/loaded
   * @param productCode Optional: The productCode the photos are attached to
   */
  private emitUpdateOrderPhotosComplete(order: MarketingOrder, photos: ListingPhoto[], productCode?: string): ListingPhoto[]{
    const event = new NotificationEvent(OrderActionTypes.UpdateOrderPhotosComplete, OrderActionTypes.UpdateOrderPhotosComplete);
    this.eventService.getEventEmitter().emit(event);
    this.store.dispatch(new UpdateOrderPhotosComplete(order, photos, productCode));
    return photos;
  }
}

/**
 * The order state effects are post state change actions that occur.
 */
@Injectable()
export class OrderStateEffects extends BaseEffects {

  constructor(private actions$: Actions,
              private marketingOrderService: MarketingOrderService,
              errorHandler: GlobalErrorHandler) {
    super(errorHandler)

  }

  /**
   * Take action on UpdateOrderState actions
   *
   *  1. Update the listing
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of listing
   *
   */
  @Effect({ dispatch: false })
  updateOrderState: Observable<any> = this.actions$.pipe(
    ofType<UpdateOrderState>(OrderActionTypes.UpdateOrderState),
    switchMap((action) => {
      return this.marketingOrderService.updateOrderState(action.marketingOrder._id, action.payload).pipe(
        catchError(err => this.processCatchError(OrderActionTypes.UpdateOrderState, action.payload, err)),
      )
    }),
    filter(orderState => !(orderState instanceof ErrorData)),
    map((result:any) => {
      return result;
    })
  );
}
