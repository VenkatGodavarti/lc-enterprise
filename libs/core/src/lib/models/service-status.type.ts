/**
 * For photo orders
 *
 * PENDING => order placed in system. When order is submitted with photo order, this is the state set
 * TODO => order received by photographer (maybe logged into system to see orders)
 * CREATING => order accepted and started
 * PROOFING => At least one photo is uploaded
 * DELIVERY => order "complete"
 * DONE => Completed workflow
 *
 */
export enum ServiceStatusType {
  PENDING = 'PENDING',
  TODO = 'TO_DO',
  CREATING = 'CREATING',
  PROOFING = 'PROOFING',
  DELIVERY = 'DELIVERY',
  DONE = 'DONE'
}
