import { ModelMap, FunctionModelProperty } from '../support/model.map';
import { Context, ContextKeys } from '../support';

/*
Used to set properties of an orde rusng user-friendlyt names:

      | Package              | Gold                  |
      | Street               | 123 Main St           |
      | City                 | Madison               |
      | State                | NJ                    |
      | Zip Code             | 07040                 |
      | Property Information | default               |
      | Order Date           | 04/01/2019 at 1:00 am |
      | Photos               | none                  |
 */
export class MarketingOrderProps extends ModelMap {

  private static props: FunctionModelProperty[] = [
    new FunctionModelProperty('Package', setSelectedPackage),
    new FunctionModelProperty('Street', setStreet),
    new FunctionModelProperty('Unit Number', setUnitNumber),
    new FunctionModelProperty('City', setCity),
    new FunctionModelProperty('State', setState),
    new FunctionModelProperty('Zip Code', setZipCode),
    new FunctionModelProperty('Property Information', setPropertyInformation),
    new FunctionModelProperty('List Date', setListDate),
    new FunctionModelProperty('Closed Date', setClosedDate),
    new FunctionModelProperty('Photos', setPhotos),
    new FunctionModelProperty('Status', setStatus),
    new FunctionModelProperty('MLS ID', setMlsId),
    new FunctionModelProperty('Shipping Address', setShippingAddress),
    new FunctionModelProperty('Billing Address', setBillingAddress),
    new FunctionModelProperty( 'Agent Type', setAgentType),
    new FunctionModelProperty( 'Agent', setAgent),
    new FunctionModelProperty( 'Coordinator', setCoordinator),
    new FunctionModelProperty( 'Submitted', setSubmitted)

  ];

  constructor() {
    super('marketing-order', MarketingOrderProps.props);
  }
}

function setPropertyInformation(order: any, value: any): void {

  if (value === 'default') {
    setDefaultPropertyInformation(order);
  }
}

function setDefaultPropertyInformation(order: any) {

  const listing = order.listing;
  listing.agentIsPrimary = false;
  listing.agentType = 'Single Agent';
  listing.bathrooms = 0;
  listing.bedrooms = 0;
  listing.displayAsPriceOnRequest = false;
  listing.globalLuxuryListing = false;
  listing.hideAddress = false;
  listing.listPrice = 0;
  listing.lotSqFt = 0;
  listing.propertyType = 'Single family';
  listing.sqFt = 0;
  listing.mlsId = null;
}

function setSelectedPackage(order: any, value: any): void {

  const pkg = order.availablePackages.find( p => p.title === value);
  if (!pkg) {
    throw new Error('no package found with title: ' + value);
  }

  order.selectedPackageCode = pkg.code;

}

function setPhotos(order: any, value: any): void {

  if (value === 0 || value === 'none') {
    order.photos = [];
  }
  //TODO allow adding a number of photos to the order
}

function setShippingAddress(order: any, value: any) {
  if(!value || value === 'null') {
    delete order.shippingInformation;
  }
}

function setBillingAddress(order: any, value: any) {
  if(!value || value === 'null') {
    delete order.billingInformation;
  }
}

function setMlsId(order: any, value: any): void {

  order.listing.mlsId = value;
}

function setStatus(order: any, value: any): void {

  order.status = value;
}
function setStreet(order: any, value: any): void {

  setAddressProperty(order, value, 'streetAddress1');
}

function setUnitNumber(order: any, value: any): void {

  setAddressProperty(order, value, 'unitNumber');
}

function setCity(order: any, value: any): void {

  setAddressProperty(order, value, 'city');
}

function setState(order: any, value: any): void {

  setAddressProperty(order, value, 'state');
}

function setListDate(order: any, value: any): void {

  order.listing.listDate = value;
}

function setClosedDate(order: any, value: any): void {

  order.listing.closedDate = value;
}

function setZipCode(order: any, value: any): void {

  setAddressProperty(order, value, 'postalCode');
}

function setAddressProperty(order: any, value: any, name: string): void {

  const address = order.listing.address;
  address[name] = value;
}

function setAgentType(order: any, value: any): void {
  order.listing.agentType = value;
}

function setAgent(order: any, value: any): void {

  setIdKeyValue(order, 'agentId', value);
}


function setCoordinator(order: any, value: any): void {

  setIdKeyValue(order, 'coordinatorId', value);
}

function setSubmitted(order: any, value: any): void {
  order.submitted = value;
}

function setIdKeyValue(order: any, idKey: string, value: string) {

  let userId = Context.get(ContextKeys.userId);
  if (value.toLowerCase() !== 'me') {
    userId = Context.getUser(value)._id;
  }
  order[idKey] = userId;
}
