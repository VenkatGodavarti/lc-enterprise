import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { TemplateInstanceMetaData } from '../models/template-meta-data.model';
import { Observable } from 'rxjs';

@Injectable()
export class TemplateService {
  resource = 'template-info';

  constructor(private apiService: ApiService) { }

  getTemplateInfo(id: string): Observable<TemplateInstanceMetaData> {
    return this.apiService.get(`${this.resource}/${id}`);
  }
}
