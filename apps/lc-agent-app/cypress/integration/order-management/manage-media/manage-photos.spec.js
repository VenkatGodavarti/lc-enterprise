
const order = require('../../../fixtures/marketing-order-with-photos.json');

describe('Manage Photos', function() {

  it('should display all photos in the order"', function() {
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/'+order._id, order).as('order');
    cy.route('OPTIONS', '/api/v1/marketing-orders/'+order._id);

    let presignCallCount = 0;

    // Handler for the presign url
    //
    // TODO:
    // Note that cypress will only call this once no matter how many times the browser makes a call to the route it is
    // assigned to.  We can get cypress to call the handler for each call if we re-assign the route with every wait()
    // call. However cypress ignores the returned result on subsequent calls.
    //
    // https://stackoverflow.com/questions/53654626/cypress-stub-response-for-same-route-with-three-different-responses
    // https://github.com/cypress-io/cypress/issues/521
    //
    const presign = function (param) {
      console.log(`PARAM ${JSON.stringify(param, null, 1)}` );

      // This is meant to give a different response with each call, but cypress ignores the result after the first call.
      const result = [
        `uri-a-${presignCallCount}`,
        `uri-b-${presignCallCount}`,
        `uri-c-${presignCallCount}`];
      ++presignCallCount;
      console.log(`PRESIGN COUNT ${presignCallCount} : ${JSON.stringify(result, null, 1)}` );
      return result;
    };

    cy.route('POST', `${Cypress.config("image_apiBase")}/presign`, presign).as('presign');

    cy.visit('/manage-order/'+order._id+'/tabset/photos');

    for (let i = 0; i < order.photos.length; i++) {
      cy.wait('@presign');

      // This will re-assign the route so that the handler is called again with the next call.
      cy.route('POST', `${Cypress.config("image_apiBase")}/presign`, presign).as('presign');
    }

    cy.get('img.photo-grid-img')
      .should("have.length", order.photos.length);

    // TODO: Here we should check the url of the image src tags to ensure the proper order, but because of the issues
    // TODO: described above, there's no way to get a different URL for each image using cypress stubs.
  });


  it('should be able to select all photos in the order"', function() {
    cy.server();
    cy.route('GET', `${Cypress.config("persist_apiBase")}/marketing-orders/${order._id}`, order).as('order');
    cy.route('OPTIONS', `${Cypress.config("persist_apiBase")}/marketing-orders/${order._id}`);

    cy.visit('/manage-order/'+order._id+'/tabset/photos');

    cy.get('#selectAll').click({force: true});

    cy.get('input[name="select_photos"]:checked')
      .should("have.length", order.photos.length);
  });


});

