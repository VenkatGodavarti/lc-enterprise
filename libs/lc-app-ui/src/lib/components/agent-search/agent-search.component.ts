import { Component, OnInit, Attribute, Input, Output, EventEmitter, OnChanges, SimpleChanges, forwardRef } from '@angular/core';
import { FormControl, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AgentInfo, TeamMemberService } from '@lc/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'lc-agent-search',
  templateUrl: './agent-search.component.html',
  styleUrls: ['./agent-search.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AgentSearchComponent),
      multi: true
    }
  ]
})
export class AgentSearchComponent implements  ControlValueAccessor, OnInit, OnChanges {

  memberSearch$: Observable<AgentInfo[]>;
  searchControl: FormControl;

  @Input()
  formControl: FormControl;

  @Input()
  selected: AgentInfo[] = [];

  @Input()
  selectedIds: string[] = [];


  @Output()
  readonly selectedChanged = new EventEmitter<AgentInfo[]>();

  readonly validators: Validators;

  constructor(
    @Attribute('max') public max: number,
    @Attribute('label') public label: string = 'Members',
    @Attribute('placeholder') public placeholder='Add member',
    @Attribute('emptyPlaceholder') public emptyPlaceholder='Add new members',
    private teamMemberService: TeamMemberService) {
      this.max = +this.max;
    }

  ngOnInit() {
    this.initializeSearch();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.selectedIds && (this.selectedIds || []).length > 0) {
      // We need to find these agent/names to select them again.
      this.teamMemberService.searchAgentsById(this.selectedIds).subscribe(agents => {
        this.updateAgents(agents);
      })
    }
  }

  public updateAgents(agents: AgentInfo[]) {
    this.selected.splice(0, this.selected.length + 1, ...agents);
    this.selectedChanged.emit(this.selected);
    this.formControl.setValue(this.selected);
  }


  writeValue(value: any) {
    if (value !== undefined) {
      this.selected = value;
    }
  }
  registerOnChange(fn) {
  }

  registerOnTouched() {}

  private initializeSearch() {
    this.searchControl = new FormControl();
    this.memberSearch$ = this.searchControl.valueChanges.pipe(
      // debounce(() => timer(1000)), // NOTE: The debounce time on the chips input is already 250 ms
      switchMap(searchTerm => this.teamMemberService.searchAgents(searchTerm)),
    );
  }
}
