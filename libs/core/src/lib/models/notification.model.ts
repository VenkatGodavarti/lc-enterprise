import { Deserializable } from './deserializable.model';
import { Audit } from './audit.model';

export enum NotificationStatus {
  ACTIVE = 'ACTIVE',
  SNOOZED = 'SNOOZED',
  COMPLETED = 'COMPLETED'
}

export enum NotificationType {
  ASSIGNED_TO_ORDER = 'ASSIGNED_TO_ORDER',
  ASSIGNED_TO_TEAM = 'ASSIGNED_TO_TEAM',
  REQUESTED_MARKETING_COPY = 'REQUESTED_MARKETING_COPY',
  REQUESTED_PHOTOS = 'REQUESTED_PHOTOS',
  ORDER_CONFIRMATION = 'ORDER_CONFIRMATION',
  DEADLINE_EXPIRED = 'DEADLINE_EXPIRED',
  READY_FOR_PROOFING = 'READY_FOR_PROOFING',
  NOTIFICATION = 'NOTIFICATION',
  PRO_PHOTOS_RECEIVED = 'PRO_PHOTOS_RECEIVED'
}

export class Notification  implements Deserializable<Notification> {

  _id?: string;

  /** User this notification pertains to */
  userId?: string;

  /** Marketing order this notification pertains to */
  marketingOrderId?: string;

  /** Title of the notification */
  subject: string;

  /** Description of the notification to be displayed to the user */
  text: string;

  /** Signifies the type that the notification is. Used for routing to the proper screen */
  notificationType: NotificationType;

  /** Status of the notification: active, snoozed, completed */
  status: NotificationStatus = NotificationStatus.ACTIVE;

  /** Date when the notification should be displayed again */
  nextNotificationAt?: Date;

  /** Date the notification was generated. Used as the secondary sort order of the notifications in the UI */
  audit?: Audit;

  /** Data used to reconstruct route and any additional data required for this notification (i.e. - orderId, packageCode) */
  otherData?: any; // {orderId, packageCode, etc...}

    // NOTE: The following properties are not yet on the API
  /** Used as the primary sort order of the notifications in the UI */
  priority?: number;

  /** Signifies if notification has been read */
  isRead?: boolean;

  constructor(input?: any) {
    this.deserialize(input);
  }

  deserialize(input: any): Notification {
    Object.assign(this, input);
    if(input && input.audit) {
      this.audit = new Audit(input.audit);
    }

    return this;
  }
}
