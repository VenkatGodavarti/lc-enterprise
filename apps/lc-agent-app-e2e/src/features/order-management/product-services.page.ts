import { LCPage, ElementMap, Element } from '../../../../../e2e/support';

export class ProductServicesPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Flyer', field: '#100'}),
      new Element({ name: 'Web Site', field: '#102'}),
      new Element({ name: 'Postcard', field: '#103'}),
      new Element({ name: 'Brochure', field: '#104'}),
    ]
  );
  constructor() {
    super('Product Services', [ProductServicesPage.elements]);
  }

}
