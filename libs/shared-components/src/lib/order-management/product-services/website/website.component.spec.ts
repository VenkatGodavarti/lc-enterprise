import { async, ComponentFixture, inject, TestBed, fakeAsync, flush } from '@angular/core/testing';

import { WebsiteComponent } from './website.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ProductPreviewComponent } from '../product-preview/product-preview.component';
import { PrintPreviewComponent } from '../print-preview/print-preview.component';
import { Store } from '@ngrx/store';
import { StoreMock, TestingModule } from '@lc/testing';
import { FlowState, MarketingOrder, ORDERSTATE, SELECTEDORDER } from '@lc/core';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../../core/src/lib/factories';

describe('WebsiteComponent', () => {
  let component: WebsiteComponent;
  let fixture: ComponentFixture<WebsiteComponent>;
  let store: StoreMock<any>;
  let marketingOrder: MarketingOrder;



  beforeEach((() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    const packages = PackageInstanceFactory.createAll();
    marketingOrder.availablePackages = packages;
    marketingOrder.selectedPackageCode = packages[0].code

    TestBed.configureTestingModule({
      imports: [
        TestingModule
      ],
      declarations: [ WebsiteComponent, ProductPreviewComponent, PrintPreviewComponent],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));


  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, marketingOrder); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(WebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
