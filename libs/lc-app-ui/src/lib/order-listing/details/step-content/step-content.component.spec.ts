import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepContentComponent } from './step-content.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('StepContentComponent', () => {
  let component: StepContentComponent;
  let fixture: ComponentFixture<StepContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepContentComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
