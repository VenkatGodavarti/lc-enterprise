import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from "./api.service";
import { PackageInstance } from '../models/package-instance.model';

@Injectable()
export class PackageService {

  resource = 'packages';

  constructor(private apiService: ApiService) {
  }

  getAllAvailablePackages(): Observable<PackageInstance[]> {
    return this.apiService.get('package-instances/my-packages');
  }
}
