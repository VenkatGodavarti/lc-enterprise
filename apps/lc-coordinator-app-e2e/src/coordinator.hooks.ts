import { BeforeAll } from 'cucumber';
import { Configuration } from '../../../e2e';
import { pages as dashboardPages } from './features/dashboard/dashboard.page';
import { pages as myProfilePages } from './features/my-profile/navigation';
import { orderManagementPages as orderMgmtPages } from './features/order-management/order-management.navigation';

/**
 *  Execute functions scheduled to be executed after a scenario completes
 */
BeforeAll( { timeout: Configuration.timeout }, () => {
  const environment = readConfiguration();

  // Register Configuration
  Configuration.appURL = environment.baseURL;
  Configuration.appLogoutURL = environment.baseURL + '/logout';
  Configuration.apiBaseURL = environment.apiBaseURL;
  Configuration.apiBaseURL = environment.apiBaseURL;
  Configuration.appTitle = 'LC Coordinator';

  // Add all pages to the Navigation Map
  Configuration.addPageNavigation(myProfilePages);
  Configuration.addPageNavigation(dashboardPages);
  Configuration.addPageNavigation(orderMgmtPages);
});


/**
 * Conditionally read configuration files. The CI/CD pipeline does not have an environment.mine file
 */
function readConfiguration() {
  const env = process.env.ENVIRONMENT || 'local';
  switch(env.toLowerCase()) {
    case 'development':
      const devEnvironment = require('../../lc-coordinator-app/src/environments/environment.development');
      return devEnvironment.environment;
    case 'qa':
      const qaEnvironment = require('../../lc-coordinator-app/src/environments/environment.qa');
      return qaEnvironment.environment;
    case 'ops':
      const opsEnvironment = require('../../lc-coordinator-app/src/environments/environment.ops');
      return opsEnvironment.environment;
    default:
      const localEnvironment  = require('../../lc-coordinator-app/src/environments/environment.mine');
      return localEnvironment.environment;
  }
}
