import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ActionTypes, Listing, WorkflowAction, User,
  SELECTEDORDER, MarketingOrder, AgentTypes, LOGGEDINUSER, TeamProfile, SELECTEDTEAM, AgentInfo, UpdateOrder, BaseForm
} from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { tap, map, filter } from 'rxjs/operators';
import { FormControl, Validators, FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { CardOption } from '@lc/shared-components-ui';

class AgentViewModel extends CardOption<AgentTypes> {
  constructor(public agentType: AgentTypes, public isEnabled: boolean = false) {
    super(agentType, agentType.toString(), false)
    const imagePrefix = AgentViewModel.getImagePrefix(agentType);
    this.imageSource = `/assets/images/${imagePrefix}off.svg`;
    this.selectedImageSource = `/assets/images/${imagePrefix}on.svg`;
  }

  public static getImagePrefix(agentType: AgentTypes) {
    switch(agentType) {
      case AgentTypes.singleAgent: return 'singleagent';
      case AgentTypes.twoAgents: return 'colisting';
      case AgentTypes.team: return 'team';
    }
  }

  /**
   * Returns the appropriate description for the agent type
   * @param agentType The agentType to get the description for
   * @param hasTeam Whether the agent has a team or not
   */
  public getInfoText(hasTeam: boolean) {
    switch(this.agentType) {
      case AgentTypes.singleAgent: return 'Select this option if you are listing this property by yourself';
      case AgentTypes.twoAgents: return 'Select this option if you are listing this property with another agent and tell us their name';
      case AgentTypes.team:
        return hasTeam
          ? 'Select this option if you are listing this property as a Team'
          : `Looks like you're not part a team yet. To select this option, please check your profile to create your team, or contact your team owner(s)`;
    }
  }
}

class AgentForm extends BaseForm {
  get agentType(): AbstractControl { return this.get('agentType'); }
  get coListingAgent(): AbstractControl { return this.get('coListingAgent'); }

  constructor(order?: MarketingOrder){
    super({
      agentType: new FormControl(order.listing.agentType, [Validators.required]),
      coListingAgent: new FormControl(),
    });

    // Set the validators on the coListing to only be required if the agentType is Two Agents
    this.coListingAgent.setValidators([this.coListRequired(this)]);
    this.agentType.valueChanges.subscribe(() => this.coListingAgent.updateValueAndValidity());
  }


  private coListRequired(group: FormGroup): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const selectedAgents = (control.value || []);
      const agentType = group.get('agentType').value;
      return agentType  === AgentTypes.twoAgents && selectedAgents.length === 0 ? {required: true} : null;
    };
  }
}

@Component({
  selector: 'lc-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss']
})
export class AgentComponent implements OnDestroy, OnInit {
  currentListing$: Observable<Listing>;
  agentName$: Observable<string>;
  marketingOrder: MarketingOrder;

  coListers: AgentInfo[] = [];

  readonly agentDisplays: AgentViewModel[] = [
    new AgentViewModel(AgentTypes.singleAgent, true),
    new AgentViewModel(AgentTypes.twoAgents, true),
    new AgentViewModel(AgentTypes.team),
  ];

  params: ParamMap;
  infoText: string;
  team: TeamProfile;
  teamSub: Subscription;
  selectedCoListerIds: string[] = null;
  twoAgents = AgentTypes.twoAgents;

  form: AgentForm;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.currentListing$ = this.store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => (order != null && order.listing != null)),
      tap(order => this.form = new AgentForm(order)),
      tap(order => this.marketingOrder = order),
      tap(order => this.setCoListerId(order)),
      map(order => order.listing),
      tap(listing => this.setAgentType(listing.agentType))
    );

    this.agentName$ = this.store.select<User>(LOGGEDINUSER).pipe(
      map(user => user ? user.firstName + ' ' + user.lastName : '')
    );

    this.teamSub = this.store.pipe(select<TeamProfile>(SELECTEDTEAM), filter(profile => profile != null)).subscribe(team => {
      this.team = team;
      const teamDisplay = this.agentDisplays.find(agent => agent.agentType === AgentTypes.team);
      teamDisplay.isEnabled = true;
    });
    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  ngOnDestroy(){
    if(this.teamSub) { this.teamSub.unsubscribe(); }
  }

  setAgentType(agentType) {
    this.agentDisplays.forEach(display => display.isSelected = (agentType === display.agentType));
    this.form.agentType.setValue(agentType);
  }

  setCoListerId(order: MarketingOrder) {
    if(order && order.coListAgentId) {
      // We need to set the selected array so that the agents will reload within the <lc-agent-search>
      this.selectedCoListerIds = [order.coListAgentId];
    }
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  next(listing: Listing) {
    this.form.markAllAsDirty();
    if(!this.form.valid) { return; }


    listing.agentType = this.form.agentType.value;
    if(listing.agentType === AgentTypes.twoAgents){
      const coListingAgents: AgentInfo[] = this.form.coListingAgent.value;
      this.marketingOrder.coListAgentId = coListingAgents[0].agentId;
    } else {
      this.marketingOrder.coListAgentId = null;
    }

    this.store.dispatch(new UpdateOrder(this.marketingOrder));
    // this.store.dispatch(new UpdateOrderListing(this.marketingOrder));

    // TODO - verify this is correct or should we wait for successful
    // Request navigate to next page, we are not going to wait for successful on previous calls
    const params = this.activatedRoute.parent.snapshot.paramMap;
    const data = { stepCompleted: true, parentCompleted: true };
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, data, params));
  }

  updateInfoText(option?: AgentViewModel) {
    this.infoText = option ? option.getInfoText(this.team != null) : null;
  }
}
