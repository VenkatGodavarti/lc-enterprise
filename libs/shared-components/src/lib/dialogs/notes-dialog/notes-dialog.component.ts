import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { NotesService, NoteForm, Note } from '@lc/core';

@Component({
  selector: 'lc-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.scss']
})
export class NotesDialogComponent implements OnInit {
  public title: string;
  public subTitle: string;
  public notes$: Observable<Note[]>;

  public readonly close = new Subject();
  public readonly form: NoteForm = new NoteForm()

  constructor(private notesService: NotesService) { }

  ngOnInit() {
  }

  /**
   * Initializes the properties on the dialog before displaying
   * @param ownerId The ownerId in which the notes are being looked up on
   * @param title The title of the dialog
   * @param subTitle The subtitle of the dialog
   */
  initialize(ownerId: string, title: string, subTitle: string) {
    this.title = title;
    this.subTitle = subTitle;
    this.form.getControl('ownerId').setValue(ownerId);
    this.notes$ = this.notesService.getNotesFor(ownerId);
  }

  /**
   * Marks all fields as dirty, validates the form and then submits the form to the API
   */
  onSave() {
    this.form.markAllAsDirty();
    if(!this.form.valid) {
      return;
    }

    this.notesService.create(this.form).toPromise()
      .then(_note => this.closeDialog());
  }

  /**
   * Cancels any form edits and closes the dialog
   */
  onCancel() {
    this.closeDialog();
  }

  /**
   * Closes the dialog by emitting the close event
   */
  private closeDialog() {
    this.close.next();
  }
}
