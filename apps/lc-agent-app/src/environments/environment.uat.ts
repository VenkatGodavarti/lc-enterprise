export const environment = {
  production: false,
  envName: 'uat',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'https://lc.uat.listing-concierge.realogy.com/',
  apiBaseURL: 'https://lc.uat.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.uat.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.uat.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajgzl5zcy9OtrY20h7',
  oktaRedirectUri: 'https://lc.uat.listing-concierge.realogy.com/implicit/callback',
};
