import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PhotosComponent } from './photos.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, ViewChild } from '@angular/core';
import { Listing, ListingPhoto, MarketingOrder, PackageInstance, ProductInstance, ThumbnailSizes } from '@lc/core';

describe('PhotosComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        RouterTestingModule
      ],
      declarations: [ TestHostComponent, PhotosComponent ]
    })
      .compileComponents();
  });

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;

    const marketingOrder = component.createMarketingOrder();
    marketingOrder.listing = component.createListing();
    component.photosComponent.marketingOrder = marketingOrder;

    fixture.detectChanges();
    tick(1000);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create and handle null MarketingOrder', fakeAsync(() => {
    component.photosComponent.marketingOrder = null;
    fixture.detectChanges();
    tick(1000);

    expect(component).toBeTruthy();
    expect(component.listing).toBeFalsy();
  }));
});


/**
 * A simple component used to host the photos component during tests so that web can set its Input listing value
 * properly.
 *
 * TestHostComponent has some test methods for creating  a bare-bones listing with package, product and photos for testing.
 * the listing object will not be saved to mongo or sent to a rest api call with validation so they are minimal listings
 *
 * We are not testing image retrieval so image urls are not real image urls.
 *
 */
@Component({
  selector: 'lc-test-host',
  template: '<lc-photos></lc-photos>'
})
class TestHostComponent {
  @ViewChild(PhotosComponent)
  public photosComponent: PhotosComponent;

  public listing: Listing;

  createListing() {
    const listing = new Listing();
    return listing;
  }

  createPhoto(id, order, favorite, uri) {
    const photo = new ListingPhoto();
    photo._id = id;
    photo.order = order;
    photo.favorite = favorite;
    photo.uri = uri;
    photo.thumbnails = {};
    photo.thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH] = {
      uri: '/test/image-' + order + '.jpg'
    };

    return photo;
  }

  createMarketingOrder() {
    const order = new MarketingOrder();
    order.availablePackages = [this.createPackage()];
    order.selectedPackageCode = order.availablePackages[0].code;

    const photos = [this.createPhoto('id-1', 1, true, '/test/image.jpg')];
    order.setPhotos(photos);
    return order;
  }

  createPackage() {
    const packAge = new PackageInstance({});
    packAge.title = "Silver";
    packAge.price = 453;
    packAge.description = "string";
    packAge.position = 0;
    packAge.code = '1234';
    packAge.products = [this.createProduct()];
    return packAge;
  }

  createProduct() {
    const product = new ProductInstance();
    product.title = 'Professional Photography';

    return product;
  }
}
