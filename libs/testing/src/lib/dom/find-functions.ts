import { By } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';


export function findById(fixture: ComponentFixture<any>, id: string) : DebugElement {

  return find(fixture, '#'+id);
}

export function find(fixture: ComponentFixture<any>, cssSelector: string) : DebugElement {
  return fixture.debugElement.query(By.css(cssSelector));
}

export function findAll(fixture: ComponentFixture<any>, cssSelector: string) : DebugElement[] {
  return fixture.debugElement.queryAll(By.css(cssSelector));
}

export function count(fixture, cssSelector: string) : number {
  return findAll(fixture, cssSelector).length;
}
