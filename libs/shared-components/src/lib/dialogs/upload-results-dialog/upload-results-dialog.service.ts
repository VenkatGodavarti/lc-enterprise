import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadResultsDialogComponent } from './upload-results-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class UploadResultsDialogService {

  constructor(private modal: NgbModal) { }

  openResults(failedMessages: string[]): Promise<any> {
    const dialogRef = this.modal.open(UploadResultsDialogComponent);
    const component: UploadResultsDialogComponent = dialogRef.componentInstance;
    component.initialize(failedMessages);
    component.close.subscribe(() => dialogRef.close());
    return dialogRef.result.then(() => {}, () => {}); // Handle dismiss and close
  }
}
