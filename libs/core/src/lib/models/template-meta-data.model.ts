import { Deserializable } from './deserializable.model';

export class ComponentMetaData implements Deserializable<ComponentMetaData> {
  tagname: string;
  width: number;
  height: number;
  fontFamily: string;
  fontStyle: string;
  fontPointSize: number;
  pageNumber: number;

  constructor(data?: any) {
    this.deserialize(data || {});
  }

  deserialize(input: any): ComponentMetaData {
    Object.assign(this, input);
    return this;
  }
}

export class TemplateInstanceMetaData implements Deserializable<TemplateInstanceMetaData> {

  templateURL: string;
  headlineCount: number;
  bodyCount: number;
  photoCount: number;
  headlineInfo: ComponentMetaData[];
  bodyInfo: ComponentMetaData[];
  photoInfo: ComponentMetaData[];
  pageCount: number;
  pageWidth: number;
  pageHeight: number;

  constructor(data?: any) {
    this.deserialize(data || {});
  }

  deserialize(input: any): TemplateInstanceMetaData {
    Object.assign(this, input);
    return this;
  }
}
