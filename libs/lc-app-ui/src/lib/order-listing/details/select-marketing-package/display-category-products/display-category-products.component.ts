import { Component, Input, OnInit } from '@angular/core';
import { PackageInstance, ProductCategory, ProductInstance } from '@lc/core';

@Component({
  selector: 'lc-display-category-products',
  templateUrl: './display-category-products.component.html',
  styleUrls: ['./display-category-products.component.scss']
})
export class DisplayCategoryProductsComponent implements OnInit {

  @Input()
  category: string;

  @Input()
  packages: PackageInstance[];

  @Input()
  rowStyle = '';

  // for now, only opt out if the category is multimedia (i.e. - Photography)
  public get canOptOut() { return this.category && this.category === ProductCategory.MULTIMEDIA; }

  constructor() { }

  ngOnInit() {
  }

  hasProductsForCategory() {
    // All we need is one package that has a product in this category.
    return this.packages.some(pkg => pkg.products.some(p => p.category === this.category));
  }

  availableProducts() {
    const availableProducts = [];
    const products = {};
    this.packages.forEach( pkg => {
      pkg.products.forEach( pr => {
        if (!products[pr.code]) {
          products[pr.code] = pr
          availableProducts.push(pr);
        }
      });
    });

    const ap = availableProducts.filter( p => p.category === this.category).sort( (p1, p2) => p1.position - p2.position );
    return ap;
  }

  productQuantity(pkg, productCode) {
    const product = pkg.products.find(p => p.code === productCode);
    if( product && (product.unitQuantity * product.unitMultiple > 1) ) {
      return `<b>${product.unitQuantity * product.unitMultiple}</b> ${product.description}`;
    }
    return null;
  }

  packageHasProduct(product, pkg) {
    return pkg.products.some(pr => pr.code === product.code);
  }

  toggleOptOut(product: ProductInstance) {
    // when user opts out of a product, we need to update the product in all packges
    const products = this.packages.map(pkg => pkg.products.find(prod => prod.code === product.code)).filter(prod => prod != null);
    products.forEach(prod => prod.optedOut = product.optedOut);
  }

  getElementId(text){
    return text.replace(/ /g, '-').toLowerCase();
  }
}
