import { Component, Input, OnInit } from '@angular/core';
import { FlowStep } from '@lc/core';

@Component({
  selector: 'lc-detail-step-template',
  templateUrl: './detail-step-template.component.html',
  styleUrls: ['./detail-step-template.component.scss']
})
export class DetailStepTemplateComponent implements OnInit {

  @Input() index: number;
  @Input() currentStep: FlowStep;
  @Input() step: FlowStep;

  constructor() {
  }

  /* istanbul ignore next */
  ngOnInit() {
  }

}
