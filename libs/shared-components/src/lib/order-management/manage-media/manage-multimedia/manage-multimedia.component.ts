import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {
  LOGGEDINUSER,
  MarketingOrder,
  Media,
  MultiMediaItem,
  SELECTEDORDER, UIConstants,
  UpdateOrderPartial,
  User,
  AddMarketingOrderMultimediaLink
} from '@lc/core';
import { Store } from '@ngrx/store'
import { MultiMediaItemForm } from '../../../../../../shared-components/src/lib/media/multi-media-item-form/multi-media-item.form';
import { PromptDialogService } from '@lc/shared-components-ui';
import { FormGroupDirective } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { MultiMediaItemRequest } from '../../../../../../../libs/core/src/lib/models/multi-media-item.model';

@Component({
  selector: 'lc-manage-multimedia',
  templateUrl: './manage-multimedia.component.html',
  styleUrls: ['./manage-multimedia.component.scss']
})
export class ManageMultimediaComponent implements OnInit, OnDestroy {

  addFormGroup = new MultiMediaItemForm();

  // Angular FormDirective needs to be reset after submission so that the error validations are removed.
  // See: https://github.com/angular/components/issues/4190
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;

  subscriptions: Subscription[] = [];

  marketingOrder$: Observable<MarketingOrder>;
  marketingOrder: MarketingOrder;

  userId = null;
  userFullName = 'First Last';

  constructor(private store: Store<any>,
              private dialogService: PromptDialogService) {
    this.marketingOrder$ = this.store.select<MarketingOrder>(SELECTEDORDER).pipe(
      tap(order => {
        this.marketingOrder = order;
        if (order && !order.media) {
          order.media = new Media();
        }
      })
    );
  }

  ngOnInit() {
    this.subscriptions.push(this.store.select<User>(LOGGEDINUSER).subscribe(user => {
      this.userId = user._id;
      this.userFullName = (user.profile.preferredFirstName && user.profile.preferredLastName)
        ? `${user.profile.preferredFirstName}  ${user.profile.preferredLastName}`
        : `${user.firstName}  ${user.lastName}`;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.subscriptions = [];
  }

  addLink() {

    // when dispatching the request the effects will throw another event
    // when the transaction is complete and update the selected order with
    // the new media link. @see photo-order.effects.ts and photo-order.reducer.ts
    const request = new MultiMediaItemRequest(this.addFormGroup.value);
    this.dispatchUpdateLink(request);
    this.addFormGroup.reset();
    this.formDirective.resetForm();
  }

  updateItem(item: MultiMediaItem) {
    const request = new MultiMediaItemRequest(item);
    delete request.audit;
    this.dispatchUpdateLink(request);
  }

  dispatchUpdateLink(item) {
    this.store.dispatch(new AddMarketingOrderMultimediaLink(item));
  }

  dispatchUpdate(marketingOrder) {
    this.store.dispatch(new UpdateOrderPartial(marketingOrder, ['media']));
  }

  deleteSelected(selectedItems: MultiMediaItem[]) {
    this.dialogService.openPrompt(UIConstants.CONFIRM, 'Are you sure you want to delete this multimedia link?', UIConstants.YES, [UIConstants.NO])
      .then(response => {
        if(response && response.text === UIConstants.YES){
          const items =  this.marketingOrder.media.multimedia.filter(item => selectedItems.indexOf(item) < 0);
          this.marketingOrder.media.multimedia = items;
        }
        this.dispatchUpdate(this.marketingOrder);
      });
    }
}

