import { Given, Then, When } from 'cucumber';
import {
  Configuration,
  MarketingOrdersClient,
  Context,
  ContextKeys,
  Scheduler,
  Schedule,
  Configuration as config, LCPage
} from '../../../../../e2e';
import { MarketingOrder, Listing, Address } from '../../../../../libs/core/src/lib/models';
import { $$, browser, by, ElementArrayFinder, ElementFinder, ExpectedConditions as EC } from 'protractor';
import { tableToRecords } from '../../../../../e2e/step_definitions';
import { ListingDashboardPage } from './listing-dashboard.page';
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Given(/^the following marketing order data exists$/,  { timeout: Configuration.timeout },  async (dataTable: any) => {
  const orders = tableToMarketingOrders(dataTable);

  const orderIds = [];
  orders.forEach(async (order) => {
    const response = await MarketingOrdersClient.post(order);
    orderIds.push(response.data._id);
  });

  Scheduler.schedule(Schedule.afterScenario, async () => {
    //console.log('Restoring profile addresses to initial state');
    orderIds.forEach(async (orderId) => {
      await MarketingOrdersClient.delete(orderId);
    });
  })
});

Given(/^I should see (.*) orders$/,  { timeout: Configuration.timeout },  async (count: number) => {
  const cards = $$('.listing-card');
  await browser.wait(
    () => cards.count().then(cardCount => cardCount === +count)
    , 5000,
    `The number of cards expected ${count} did not load in the proper time`);
});

When(/^I click the (hide|show) order button on order (.*)$/,  { timeout: Configuration.timeout },  async (hideOrShow: string, cardNumber: number) => {
  const cardToggle = $$('.archive-toggle');
  const toggle = await cardToggle.get(cardNumber - 1);
  /*
   Using WebElements and browser actions instead of ElementFinder because protractor/selenium have known bugs
   with clickable elements wrapped in other elements (div/span/label/etc)
   */
  const webEl = toggle.getWebElement();
  await browser.actions().mouseMove(webEl).perform();
  await browser.actions().click().perform();
});

When(/^I click the (Complete|Manage) Order button on order (.*)$/,  { timeout: Configuration.timeout },  async (completeOrManage: string, cardNumber: number) => {
  const cardToggle = $$('.archive-toggle');
  const toggle = await cardToggle.get(cardNumber - 1);
  await toggle.click();
});

When(/^I click the (Complete|Manage) Order button on the last order$/,  { timeout: Configuration.timeout },  async (completeOrManage: string) => {
  const buttons = $$(completeOrManage === 'Complete' ? '.complete-order' : '.manage-order');
  const count = await buttons.count();
  const button = await buttons.get(count - 1);
  await button.click();
});

When(/^I click the (.*) of order (\d)$/,  { timeout: Configuration.timeout },  async (elementName: string, orderNumber: number) => {
  const element: ElementFinder = await findChildOfListingCard(elementName, +orderNumber);
  await element.click();
});

When(/^I click the (Complete|Manage) Order button for the (.*) order$/,  { timeout: Configuration.timeout },  async (completeOrManage: string, orderName: string) => {
  const order = Context.get(orderName+'-'+ContextKeys.order);
  let elementId = completeOrManage === 'Complete' ? '#complete-order-' : '#manage-order-';
  elementId += order._id;

  const button = $$(elementId);
  await button.click();
});

When( /^I hover over the (.*) of order (\d*)$/, {timeout: config.timeout},async (elementName: string, orderNumber: number)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element: ElementFinder = await findChildOfListingCard(elementName, +orderNumber);
  await page.mouseHover(element);
});

Then(/^the (.*) element of order (\d*) is (not present|present)$/, {timeout: config.timeout},async (elementName: string, orderNumber: number, present: string) => {
  if(elementName.length > 0) {
    const element = await findChildOfListingCard(elementName, +orderNumber);

    const checkPresent = present === 'present';

    let isPresent = false
    try {
      isPresent =  await element.isPresent(); // TODO: Should this be isDisplayed()?
    } catch(e) {}
    expect(isPresent).to.be.equal(checkPresent);
  }
});

Then(/^the (.*) element of order (\d*) contains (.*)$/, {timeout: config.timeout},async (elementName: string, orderNumber: number, value: string) => {
  const element = await findChildOfListingCard(elementName, orderNumber);
  const text = await element.getText();

  expect(text).to.include(value);
});

async function findListingCard(orderNumber: number): Promise<ElementFinder> {
  const listingCards: ElementArrayFinder = $$('.listing-card');
  const listingCard = listingCards.get(+orderNumber - 1);
  await browser.wait(EC.elementToBeClickable(listingCard));
  return listingCard;
}

async function findChildOfListingCard(elementName: string, orderNumber: number = 1): Promise<ElementFinder> {
  const parent = await findListingCard(orderNumber);

  const source = ListingDashboardPage.elements.elements().find(element => { return element.name === elementName; });
  if(!source) {
    expect(true, `${elementName} could not be found`).to.be.equal(false);
  }
  const selector = source.field;

  return parent.$$(selector).get(0);
}

function tableToMarketingOrders(dataTable: any): MarketingOrder[] {
  const records = tableToRecords(dataTable);

  const orders = records.map(record => {
    const order = new MarketingOrder();
    delete order.summary;
    order.agentId = Context.get(ContextKeys.apiUserId);
    order.listing = new Listing();
    order.listing.address = new Address(record);
    return order;
  });
  return orders;
}
