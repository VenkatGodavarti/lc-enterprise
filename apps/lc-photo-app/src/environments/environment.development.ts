export const environment = {
  // App configuration
  production: false,
  envName: 'development',
  applicationName: 'Listing Concierge Photo App Dev',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://lc.dev.lc-photo.realogy.com/',
  apiBaseURL: 'https://lc.dev.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.dev.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.dev.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajm3d7aipPzzMKR0h7',
  oktaRedirectUri: 'https://lc.dev.lc-photo.realogy.com/implicit/callback',
};
