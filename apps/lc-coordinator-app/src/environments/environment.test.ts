export const environment = {
  // App confiuration
  production: false,
  envName: 'development',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'https://lc.dev.lc-coordinator.realogy.com/',
  apiBaseURL: 'https://lc.dev.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.dev.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.dev.lc-notify.realogy.com/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzo5kchwnqP49t0h7',
  oktaRedirectUri: 'https://lc.dev.lc-coordinator.realogy.com/implicit/callback',
};
