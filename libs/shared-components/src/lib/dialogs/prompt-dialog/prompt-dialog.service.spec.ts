import { TestBed } from '@angular/core/testing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PromptDialogService } from './prompt-dialog.service';

describe('PromptDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ NgbModule ],
    providers: [ PromptDialogService ]
  }));

  it('should be created', () => {
    const service: PromptDialogService = TestBed.get(PromptDialogService);
    expect(service).toBeTruthy();
  });
});
