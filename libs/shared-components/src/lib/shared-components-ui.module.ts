// Angular Modules
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
const angularModules = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule];

// Third Party Modules
import { NgbTabsetModule, NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { TagInputModule } from 'ngx-chips';
import { FileDropModule } from 'ngx-file-drop';
import { ImageCropperModule } from 'ngx-image-cropper';
const thirdPartyModules = [FileDropModule, NgbTabsetModule, NgbModule, NgxMaskModule.forRoot(), TagInputModule, ImageCropperModule];

// Internal Services
import { PromptDialogService } from './dialogs/prompt-dialog/prompt-dialog.service';
import { NotesDialogService } from './dialogs/notes-dialog/notes-dialog.service';
import { UploadResultsDialogService } from './dialogs/upload-results-dialog/upload-results-dialog.service';
import { ImageCropperDialogService } from './dialogs/image-cropper-dialog/image-cropper-dialog.service';
const services = [ImageCropperDialogService, NotesDialogService, PromptDialogService, UploadResultsDialogService];

// Internal Components
import { AutocompleteComponent } from './inputs/autocomplete/autocomplete.component';
import { CheckboxComponent } from './inputs/checkbox/checkbox.component';
import { ChipsComponent } from './inputs/chips/chips.component';
import { DropdownComponent } from './inputs/dropdown/dropdown.component';
import { TextboxComponent } from './inputs/textbox/textbox.component';
import { TextareaComponent } from './inputs/textarea/textarea.component';
import { FileUploadComponent } from './inputs/file-upload/file-upload.component';
import { CardOptionsComponent } from './inputs/card-options/card-options.component';
import { PromptDialogComponent } from './dialogs/prompt-dialog/prompt-dialog.component';
import { NotesDialogComponent } from './dialogs/notes-dialog/notes-dialog.component';
import { UploadResultsDialogComponent } from './dialogs/upload-results-dialog/upload-results-dialog.component';
import { TextCheckerComponent } from './inputs/text-checker/text-checker.component';
import { ImageCropperDialogComponent } from './dialogs/image-cropper-dialog/image-cropper-dialog.component';
import { AvatarImageComponent } from './images/avatar-image/avatar-image.component';
import { TeamImageComponent } from './images/team-image/team-image.component';
import { TabSetComponent } from './tabset/tab-set.component';
import { DatepickerComponent } from './inputs/date-picker/datepicker.component';
const dialogComponents = [ PromptDialogComponent, NotesDialogComponent, UploadResultsDialogComponent, ImageCropperDialogComponent];
const inputComponents = [ AutocompleteComponent, CheckboxComponent, ChipsComponent, CardOptionsComponent, DropdownComponent, TextareaComponent, TextboxComponent, TextCheckerComponent, AvatarImageComponent, TeamImageComponent, DatepickerComponent];
const internalComponents = [ ...dialogComponents, ...inputComponents, FileUploadComponent, TabSetComponent ];

// WARNING: DO NOT IMPORT CORE MODULE HERE
@NgModule({
  imports: [
    ...angularModules,
    ...thirdPartyModules
  ],
  declarations: [
    ...internalComponents
  ],
  providers: [
    ...services
  ],
  entryComponents:[
    ...dialogComponents
  ],
  exports: [
    ...internalComponents
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedComponentsUiModule { }
