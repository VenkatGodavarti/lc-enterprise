import { Deserializable } from './deserializable.model';
import { Profile } from './profile.model';

class PreferenceCategory {

  category: string;
  preferences: any;

  constructor(preferenceCategory) {
   if (preferenceCategory) {
      this.category = preferenceCategory.category;
      this.preferences = preferenceCategory.preferences;
   }
  }
}

export class User implements Deserializable<User> {

  // Used to filter older versions that may be in localStorage
  // Should be kept in sync with server model
  public readonly compatibleSchemaVersions: Array<number>;
  schemaVersion: number;

  _id: string;
  oktaId: string;
  username: string;
  email: string;
  dateJoined: string;
  firstName: string;
  lastName: string;
  profile: Profile;
  preferences: PreferenceCategory[];
  userPermissions: string[];

  constructor(input?: any) {
    if(input){
      this.deserialize(input);
    }
    this.compatibleSchemaVersions = [2];
  }

  deserialize(input: any): User {
    Object.assign(this, input);

    if (input['profile']) {
      this.profile = new Profile().deserialize(input['profile']);
    }

    this.preferences = [];
    if (input.preferences) {
      input.preferences.forEach(category => {
        this.preferences.push(new PreferenceCategory(category));
      });
    }

    return this;
  }

  getFirstName(): string {
    if (this.profile && this.profile.preferredFirstName) {
      return this.profile.preferredFirstName;
    }

    return this.firstName;
  }

  getLastName(): string {
    if (this.profile && this.profile.preferredLastName) {
      return this.profile.preferredLastName;
    }
    return this.lastName;
  }

  getPhotoUrl(): string {
    if (this.profile && this.profile.photoUrl) {
      return this.profile.photoUrl;
    }

    return '';
  }

}
