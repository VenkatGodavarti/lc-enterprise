// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const { join } = require('path');
const { constants } = require('karma');

module.exports = () => {
  return {
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
        require('karma-jasmine'),
        require('karma-chrome-launcher'),
        require('karma-coverage'),
        require('karma-jasmine-html-reporter'),
        require('karma-junit-reporter'),
        require('karma-coverage-istanbul-reporter'),
        require('karma-remap-coverage'),
        require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    angularCli: {
      sourceMap: true,
    },
    reporters: ['progress', 'junit', 'coverage', 'remap-coverage', 'kjhtml', 'coverage-istanbul'],
    customLaunchers: {
      ChromeWithDevTools: {
          base: 'Chrome',
          flags: [
              '--auto-open-devtools-for-tabs'
          ]
      },
      ChromeHeadless: {
          base: 'Chrome',
          flags: [
              '--disable-gpu',
              '--headless',
              '--no-sandbox',
              '--remote-debugging-port=9222'
          ]
      },
      ChromeHeadlessCI: {
          base: 'ChromeHeadless',
          flags: ['--no-sandbox']
      }
    },
    coverageReporter: {
      //type: 'in-memory'//, 'cobertura',
      type: 'cobertura',
      dir: join(__dirname, './reports/coverage')
    },
    coverageIstanbulReporter: {
      //dir: require('path').join(__dirname, '../reports/coverage/html'),
      dir: join(__dirname, './reports/coverage'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    remapCoverageReporter: {
      html: join(__dirname, './reports/coverage/html'),
      cobertura: join(__dirname, './reports/coverage/cobertura.xml')
    },
    junitReporter: {
      outputDir: join(__dirname, './reports/unit-tests'),
      outputFile: undefined, // if included, results will be saved as $outputDir/$browserName/$outputFile
      suite: '', // suite will become the package name attribute in xml testsuite element
      useBrowserName: true, // add browser name to report and classes names
      nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
      classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
      properties: {} // key value pair of properties to add to the <properties> section of the report
    },
    port: 9876,
    colors: true,
    logLevel: constants.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeHeadless'],
    singleRun: true
  };
};
