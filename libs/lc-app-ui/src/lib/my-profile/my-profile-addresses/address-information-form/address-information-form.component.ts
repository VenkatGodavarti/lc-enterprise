import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { STATES_BY_ABBREVIATION, USStates } from '@lc/core';
import { Masks } from '@lc/shared-components-ui';

@Component({
  selector: 'lc-address-information-form',
  templateUrl: './address-information-form.component.html',
  styleUrls: ['./address-information-form.component.scss']
})
export class AddressInformationFormComponent implements OnInit {
  readonly phoneMask = Masks.phoneNumber;
  readonly states: USStates[] = STATES_BY_ABBREVIATION;

  @Input()
  formGroup: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
