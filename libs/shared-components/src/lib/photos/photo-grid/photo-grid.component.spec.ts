import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import {PhotoGridComponent} from './photo-grid.component';
import {DragulaModule, DragulaService} from 'ng2-dragula';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  LoggerFactory,
  PhotoService,
  MarketingOrder, SELECTEDORDER
} from '@lc/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { StoreMock, TestingModule } from '@lc/testing';
import { MarketingOrderFactory } from '../../../../../core/src/lib/factories';
import { Store } from '@ngrx/store';
import { Component, ViewChild } from '@angular/core';


describe('PhotoGridComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  /**
   * PhotoGridComponent has an Input of type Listing. We use the TestHostComponent to host the PhotoGrid so that we can
   * assign a listing to the input for testing.
   */
  let modalServiceSpy;
  let photoServiceSpy;
  let marketingOrder: MarketingOrder;
  let store: StoreMock<any>;

  beforeEach(fakeAsync(() => {
    modalServiceSpy = jasmine.createSpyObj('NgbModal', ['open']);
    photoServiceSpy = jasmine.createSpyObj('PhotoService', ['getImage', 'presign']);
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    store = new StoreMock();
    store.setState(SELECTEDORDER, marketingOrder);

    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        DragulaModule,
        FormsModule,
        ReactiveFormsModule,
        TestingModule
      ],
      declarations: [
        PhotoGridComponent
      ],
      providers: [
        {provide: NgbModal, useValue: modalServiceSpy},
        {provide: PhotoService, useValue: photoServiceSpy},
        {provide: Store, useValue: store},
        DragulaService,
        LoggerFactory
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.photoGrid.marketingOrder = component.marketingOrder;
    fixture.detectChanges();
    tick(1000);
  }));
});

@Component({
  selector: 'lc-test-host',
  template: '<lc-photo-grid></lc-photo-grid>'
})
class TestHostComponent {
  @ViewChild(PhotoGridComponent)
  public photoGrid: PhotoGridComponent;
  public marketingOrder: MarketingOrder = MarketingOrderFactory.createMarketingOrder('123456789', null, null, true);
}
