export const environment = {
  production: false,
  envName: 'ops',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'https://lc-coordinator.ops.concierge.realogydev.com/',
  apiBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-persist/api/v1/',
  photosBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-image/api/v1/',
  notifyBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-notify/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzuptmwgyfDgjQ0h7',
  oktaRedirectUri: 'https://lc-coordinator.ops.concierge.realogydev.com/implicit/callback',
};
