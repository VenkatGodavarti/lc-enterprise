import { Component, OnInit } from '@angular/core';
import { TemplateInstance, OutputMimeType, ProductInstance, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'lc-product-designs',
  templateUrl: './product-designs.component.html',
  styleUrls: ['./product-designs.component.scss']
})
export class ProductDesignsComponent implements OnInit {

  templates = [
    new TemplateInstance({
      title: 'Flyer 1',
      sides: 2,
      numberOfPhotos: 5,
      thumbnailURL: '/assets/images/flyer01@3x.png',
      mimeType: OutputMimeType.pdf
    }),

    new TemplateInstance({
      title: 'Flyer 2',
      sides: 2,
      numberOfPhotos: 5,
      thumbnailURL: '/assets/images/flyer02@3x.png',
      mimeType: OutputMimeType.pdf
    }),

    new TemplateInstance({
      title: 'Flyer 3',
      sides: 2,
      numberOfPhotos: 5,
      thumbnailURL: '/assets/images/flyer03@3x.png',
      mimeType: OutputMimeType.pdf
    })
  ];

  product$: Observable<ProductInstance>;

  constructor(private store: Store<any>,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const productCode = this.activatedRoute.snapshot.paramMap.get('product_code');
    this.product$ = this.store.pipe(select<MarketingOrder>(SELECTEDORDER)).pipe(
      filter((marketingOrder: MarketingOrder) => marketingOrder != null),
      map((marketingOrder) => marketingOrder.getSelectedPackage().products.find(pr => pr.code === productCode))
    );
  }

  selectTemplate(product: ProductInstance, template: TemplateInstance) {
    product.selectedTemplate = template;
    this.router.navigate(['preview'], {relativeTo: this.activatedRoute});
  }
}
