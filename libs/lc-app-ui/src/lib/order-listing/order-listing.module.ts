import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthGuard, TeamResolve } from '@lc/core';

import { OrderFormsModule } from '@lc/shared-components-order-forms';
import { OrderListingResolve } from './order-listing.resolve';

import { OrderListingComponent } from './order-listing.component';
import { OrderStepsComponent } from './steps/order-steps/order-steps.component';
import { DetailStepsComponent } from './details/detail-steps/detail-steps.component';
import { AgentComponent } from './details/agent/agent.component';
import { PropertyComponent } from './details/property/property.component';
import { ShippingComponent } from './details/shipping/shipping.component';
import { OrderSummaryComponent } from './details/order-summary/order-summary.component';
import { EditToolbarComponent } from './edit-toolbar/edit-toolbar.component';
import { BillingComponent } from './details/billing/billing.component';
import { DetailStepTemplateComponent } from './details/detail-steps/detail-step-template/detail-step-template.component';
import { OrderStepTemplateComponent } from './steps/order-steps/order-step-template/order-step-template.component';
import { OrderStepOverviewComponent } from './steps/order-step-overview/order-step-overview.component';
import { SelectPhotographerComponent } from './details/photography/select-photographer/select-photographer.component';
import { SchedulePhotographerComponent } from './details/photography/schedule-photographer/schedule-photographer.component';
import { PhotographerCommentsComponent } from './details/photography/photographer-comments/photographer-comments.component';
import { SelectMarketingPackageComponent } from './details/select-marketing-package/select-marketing-package.component';
import { DisplayCategoryProductsComponent } from './details/select-marketing-package/display-category-products/display-category-products.component';
import { StepContentComponent } from './details/step-content/step-content.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { PhotographyUpgradesComponent } from './details/photography/photography-upgrades/photography-upgrades.component';
import { ComponentsModule } from '../components/components.module';
import { BrochureOptionsComponent } from './details/brochure-options/brochure-options.component';
import { PaymentsComponent } from './details/payments/payments.component';
import { NgxStripeModule } from 'ngx-stripe';


@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    OrderFormsModule,
    NgxStripeModule.forRoot('pk_test_GzgXKTGsDAOYaX6jHsDzEFQ800jzeOk9BL'),
    SharedComponentsUiModule,
    RouterModule.forChild([
      { path: 'select-marketing-package',
        component: SelectMarketingPackageComponent,
        resolve: {
          listing: OrderListingResolve
        },
        canActivate: [AuthGuard]
      },
      {
        path: ':pid',
        component: OrderListingComponent,
        resolve: {
          listing: OrderListingResolve
        },
        children: [
          { path: '', redirectTo: 'details', pathMatch: 'full' },
          { path: 'details', component: OrderStepOverviewComponent, pathMatch: 'full',
            data: {id: '/order-listing/details'}},
          { path: 'property', component: PropertyComponent,
            data: {id: '/order-listing/details/property'}},
          { path: 'agent', component: AgentComponent,
            data: {id: '/order-listing/details/agent' }, resolve: { team: TeamResolve }},
          { path: 'photography', component: OrderStepOverviewComponent,
            data: {id: '/order-listing/photography'}},
          { path: 'photography-upgrades', component: PhotographyUpgradesComponent,
            data: {id: '/order-listing/photography/photography-upgrades'}},
          { path: 'select-photographer', component: SelectPhotographerComponent,
            data: {id: '/order-listing/photography/select-photographer'}},
          { path: 'schedule-photographer', component: SchedulePhotographerComponent,
            data: {id: '/order-listing/photography/schedule-photographer'}},
          { path: 'photographer-comments', component: PhotographerCommentsComponent,
            data: {id: '/order-listing/photography/photographer-comments'}},
          { path: 'package-options', component: OrderStepOverviewComponent,
            data: {id: '/order-listing/package-options'}},
          { path: 'brochure-options', component: BrochureOptionsComponent,
            data: {id: '/order-listing/brochure-options'}},
          { path: 'checkout', component: OrderStepOverviewComponent,
            data: {id: '/order-listing/checkout'}},
          { path: 'shipping', component: ShippingComponent,
            data: {id: '/order-listing/checkout/shipping'}},
          { path: 'billing', component: BillingComponent,
            data: {id: '/order-listing/checkout/billing'}},
            { path: 'payments', component: PaymentsComponent,
            data: {id: '/order-listing/checkout/payments'}}

        ]
      }
    ])
  ],
  declarations: [
    SelectMarketingPackageComponent,
    DisplayCategoryProductsComponent,
    OrderListingComponent,
    OrderStepsComponent,
    DetailStepsComponent,
    AgentComponent,
    PropertyComponent,
    ShippingComponent,
    OrderSummaryComponent,
    EditToolbarComponent,
    BillingComponent,
    SelectPhotographerComponent,
    SchedulePhotographerComponent,
    PhotographerCommentsComponent,
    DetailStepTemplateComponent,
    OrderStepTemplateComponent,
    OrderStepOverviewComponent,
    StepContentComponent,
    PhotographyUpgradesComponent,
    BrochureOptionsComponent,
    PaymentsComponent
  ],
  providers: [
    OrderListingResolve,
  ]
})
export class OrderListingModule {
}
