export class Mask {
  constructor(public mask: string, public pattern: { [character: string]: { pattern: RegExp, optional?: boolean} }) {
  }
}

export class Masks {
  public static phoneNumber: Mask = new Mask('#00.000.0000',  {'#': { pattern: new RegExp('\[2-9A-Z\]')}, '0': { pattern: new RegExp('\[0-9A-Z\]')}});
  public static wholeNumber: Mask = new Mask('#0*',  {'#': { pattern: new RegExp('\[1-9\]')}, '0': { pattern: new RegExp('\[0-9\]')}});
}
