import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardOptionsComponent, CardOption } from './card-options.component';
import { PackageInstanceFactory } from 'libs/core/src/lib/factories/package-instance.factory';
import { PackageInstance, ProductOption } from '@lc/core';

describe('CardOptionsComponent', () => {
  let component: CardOptionsComponent;
  let fixture: ComponentFixture<CardOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('toggles and limits number of selections', () => {
    const goldPackage: PackageInstance = PackageInstanceFactory.gold;
    const photoProduct = goldPackage.getPhotographyProduct();
    const options = photoProduct.options;
    const cardOptions: CardOption<ProductOption>[] = options.map(option => new CardOption(option, option.title, false));

    component.options = cardOptions;
    component.autoDeselect = false;
    component.maxSelect = 2;

    component.selectOption(cardOptions, cardOptions[0]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(1);
    component.selectOption(cardOptions, cardOptions[0]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(0);

    component.selectOption(cardOptions, cardOptions[0]);
    component.selectOption(cardOptions, cardOptions[1]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(2);
    component.selectOption(cardOptions, cardOptions[2]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(2);
    component.selectOption(cardOptions, cardOptions[1]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(1);
    component.selectOption(cardOptions, cardOptions[2]);
    expect(component.options.filter(option => option.isSelected).length).toEqual(2);
  });
});
