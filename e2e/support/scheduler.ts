/**
 * Scheduler allows functions to be scheduled for execution after the scenario completes,
 * Used in connjuction with Scheduler hooks.
 *
 * Example uses: deleting test data created during a scenario.
 */

export enum Schedule {
  afterScenario = 'afterEach',
  afterAll = 'afterAll'
}

type AfterFunction =  () => void;


let scheduled = [];

export class Scheduler {

 static schedule(when: Schedule, what: AfterFunction) {
   scheduled.push( { type: when, callback: what });
 }

 static async execute(type: Schedule) {
   const candidates = scheduled.filter( s => s.type === type);
   for (let i = 0; i < candidates.length; i++) {
     await candidates[i].callback();
   }
   scheduled = scheduled.filter( s => s.type !== type);
 }

 static executeAll() {
   scheduled.forEach(async (s) => { await s.callback(); });
   scheduled = [];
 }


}
