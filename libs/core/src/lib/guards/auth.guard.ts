import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Store } from '@ngrx/store';
import { LoadNotifications } from '../state-mgmt';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private store: Store<any>) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isAuthenticated()) {
      // TODO: Remove this line (only for demo purposes)
      this.store.dispatch(new LoadNotifications());

      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page
    const postLoginRedirectTo = state.url;
    localStorage.removeItem('post-login-redirect');
    if (postLoginRedirectTo) {
      localStorage.setItem('post-login-redirect', state.url);
    }

    this.authService.redirectToLogin();
    return false;
  }
}
