import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MarketingOrder, ProductInstance, SELECTEDORDER } from '@lc/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'lc-product-preview-wrapper',
  templateUrl: './product-preview-wrapper.component.html',
  styleUrls: ['./product-preview-wrapper.component.scss']
})
export class ProductPreviewWrapperComponent implements OnInit {

  product$: Observable<ProductInstance>;

  constructor(private store: Store<any>, private activatedRoute: ActivatedRoute) {

    this.product$ = this.store.select(SELECTEDORDER).pipe(
      map((order: MarketingOrder) => {
        const selectedPackage = order.getSelectedPackage();
        console.log('selectedPackage: '+selectedPackage.title);
        if (!selectedPackage || !selectedPackage.products) { return null; }
        const productCode = this.activatedRoute.snapshot.paramMap.get('product_code');
        console.log('productCode: '+ productCode);
        return selectedPackage.products.find((pr) => pr.code === productCode)
      }));
  }
  ngOnInit() {
  }

}
