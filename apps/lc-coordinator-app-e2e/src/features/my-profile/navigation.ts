import { ProfilePage } from './profile/profile.page';

/***
 * Page mapping for the My Profile section of the app
 */
export const pages = {
  'my profile': {
    'my profile': {
      url: '/my-profile',
      page: new ProfilePage()
    },
  }
};
