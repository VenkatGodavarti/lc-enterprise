import { async, ComponentFixture, fakeAsync, flush, inject, TestBed } from '@angular/core/testing';

import { PrintPreviewComponent } from './print-preview.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../../core/src/lib/factories';
import { Store } from '@ngrx/store';
import {
  OutputMimeType,
  ProductGenerationService,
  ProductInstance,
  SELECTEDORDER
} from '@lc/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { By, DomSanitizer } from '@angular/platform-browser';
import { mockedStoreProvider, StoreMock, TestingModule, find, findAll, findById } from '@lc/testing';

describe('PrintPreviewComponent PDF', () => {

  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let store: StoreMock<any>;
  let generationService;

  let marketingOrder;
  let packages;

  const mockSanitizer = {
    sanitize: (input) => input,
    bypassSecurityTrustHtml: (html) => html,
    bypassSecurityTrustResourceUrl: (url) => url
  }

  beforeEach(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    packages = PackageInstanceFactory.createAll();
    marketingOrder.availablePackages = packages;
    marketingOrder.selectedPackageCode = packages[0].code;

    generationService = jasmine.createSpyObj('ProductGenerationService', ['generate', 'download', 'isDownloadable'])
    generationService.generate.and.callFake( (o, p) => {
      return of('some text')
    });
    generationService.isDownloadable.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TestingModule
      ],
      declarations: [ TestHostComponent, PrintPreviewComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ],
      providers: [
        { provide: ProductGenerationService, useValue: generationService},
        { provide: DomSanitizer, useValue: mockSanitizer }
      ]
    })
    .compileComponents();

  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;

    store.setState(SELECTEDORDER, marketingOrder); // set default state
  }));


  beforeEach(fakeAsync(() => {

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.printPreview.product = new ProductInstance({
      title: 'Flyer',
      category: 'print',
      kind: 'product',
      selectedTemplate: {
        mimeType: OutputMimeType.pdf
      }
    });
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('displays print content', () => {
    const content = findById(fixture, 'print-preview-content');

    expect(content).toBeTruthy();
    expect(content.nativeElement.src).toBeTruthy();
  })

  it('should call download when clicked', () => {
    let downloadOccurred = false;
    generationService.download.and.callFake( (o, p) => {
      downloadOccurred = true;
    });

    const download = findById(fixture, 'print-preview-download');
    expect(download).toBeTruthy();

    download.nativeElement.click();
    fixture.detectChanges();

    expect(downloadOccurred).toBeTruthy();
  });


  it('should generate when \'Apply Changes\' is clicked', () => {

    let changesApplied = false;
    generationService.generate.and.callFake( (o, p) => {
      changesApplied = true;

      return of('some different text');
    });

    const apply = findById(fixture, 'print-preview-apply-changes');
    expect(apply).toBeTruthy();

    apply.nativeElement.click();
    fixture.detectChanges();

    expect(changesApplied).toBeTruthy();

  });

  it('should call download when clicked', () => {
    let downloadOccurred = false;
    generationService.download.and.callFake( (o, p) => {
      downloadOccurred = true;
    });

    const download = findById(fixture, 'print-preview-download');
    expect(download).toBeTruthy();

    download.nativeElement.click();
    fixture.detectChanges();

    expect(downloadOccurred).toBeTruthy();
  });
});

/*
Commenting out until I can resolcve the Observable of an Observable issue I have in this component

describe('PrintPreviewComponent HTML', () => {

  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let store: StoreMock<any>;
  let generationService;

  let marketingOrder;
  let packages;

  const mockSanitizer = {
    sanitize: (input) => input,
    bypassSecurityTrustHtml: (html) => html,
    bypassSecurityTrustResourceUrl: (url) => url
  }

  beforeEach(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    packages = PackageInstanceFactory.createAll();
    marketingOrder.availablePackages = packages;
    marketingOrder.selectedPackageCode = packages[0].code;

    generationService = jasmine.createSpyObj('ProductGenerationService', ['generate', 'download', 'isDownloadable'])
    generationService.generate.and.callFake( (o, p) => {
      return of('some text')
    });
    generationService.isDownloadable.and.returnValue(false);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TestingModule
      ],
      declarations: [ TestHostComponent, PrintPreviewComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ],
      providers: [
        { provide: ProductGenerationService, useValue: generationService},
        { provide: DomSanitizer, useValue: mockSanitizer }
      ]
    })
      .compileComponents();

  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;

    store.setState(SELECTEDORDER, marketingOrder); // set default state
  }));


  beforeEach(fakeAsync(() => {

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.printPreview.product = new ProductInstance({
      title: 'Website',
      category: 'digital',
      kind: 'product',
      selectedTemplate: {
        mimeType: OutputMimeType.html
      }
    });
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('displays print content', () => {
    const content = findById(fixture, 'print-preview-content');

    expect(content).toBeTruthy();
    expect(content.nativeElement.srcdoc).toBeTruthy();
  })

  it('download not available', () => {

    const download = findById(fixture, 'print-preview-download');
    expect(download).toBeFalsy();
  });


  it('should generate when \'Apply Changes\' is clicked', () => {

    let changesApplied = false;
    generationService.generate.and.callFake( (o, p) => {
      changesApplied = true;

      return of('some different text');
    });

    const apply = findById(fixture, 'print-preview-apply-changes');
    expect(apply).toBeTruthy();

    apply.nativeElement.click();
    fixture.detectChanges();

    expect(changesApplied).toBeTruthy();

  });

});
*/

@Component({
  selector: 'lc-test-host',
  template: '<lc-print-preview></lc-print-preview>'
})
class TestHostComponent {
  @ViewChild(PrintPreviewComponent)
  public printPreview: PrintPreviewComponent;

}
