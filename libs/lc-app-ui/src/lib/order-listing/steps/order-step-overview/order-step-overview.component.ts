import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FlowState, ORDERSTATE } from '@lc/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-order-step-overview',
  templateUrl: './order-step-overview.component.html',
  styleUrls: ['./order-step-overview.component.scss']
})
export class OrderStepOverviewComponent implements OnInit, OnDestroy {

  // TODO - is this needed?
  @HostBinding('class') private hostClasses = 'w-100';

  orderState$: Observable<FlowState>;

  constructor(private store: Store<any>) {
    // setting state on constructor will handle any browser navigation if user hits refresh
    this.orderState$ = store.select(ORDERSTATE);
  }
  ngOnInit() {
  }

  ngOnDestroy() {
  }

}
