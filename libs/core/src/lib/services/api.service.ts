import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }

  get<TModel = any>(path: string, params: any = {}): Observable<TModel> {
    const httpParams = new HttpParams({fromObject: params});
    return this.http.get<TModel>(`${environment.apiBaseURL}${path}`, { params: httpParams })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    const url = `${environment.apiBaseURL}${path}`;
    return this.http.put(url, body)
      .pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(`${environment.apiBaseURL}${path}`, body)
      .pipe(catchError(this.formatErrors));
  }

  delete(path): Observable<any> {
    return this.http.delete(`${environment.apiBaseURL}${path}`)
      .pipe(catchError(this.formatErrors));
  }
}
