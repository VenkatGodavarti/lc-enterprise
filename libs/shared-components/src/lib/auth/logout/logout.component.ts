import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '@lc/core';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'lc-app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  @Input() title: string;
  readonly appName: string = environment.applicationName;

  constructor(
    public authService: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.authService.logout();
  }

  login() {
    this.authService.redirectToLogin();
  }
}
