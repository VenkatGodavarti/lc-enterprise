@property-description
@order-management
@login-before-all
Feature: Order Management

  Background:
    Given I am logged in to Listing Concierge
    And the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Street               | 123 Main St           |
      | Unit Number          | 1                     |
      | City                 | Madison               |
      | State                | NJ                    |
      | Zip Code             | 07040                 |
      | Property Information | default               |
      | Photos               | none                  |
      | Status               | PENDING               |
  When I select the Alamo order
  And I go to the Property Description page in Order Management
  Then the Property Description page from Order Management is loaded

  Scenario: Verify Required Fields
    When the Headline value is set to "Headline 1"
    And the Body value is set to "Body 1"
    When I set the Headline to ""
    And I set the Body to ""
    Then the Headline value is set to ""
    And the Body value is set to ""
    Then the "Headline" has an error containing "required"
    And the "Body" has an error containing "required"
