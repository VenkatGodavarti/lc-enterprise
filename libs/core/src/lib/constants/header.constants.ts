
export enum HeaderConstants {
  CORRELATION_ID = 'x-lc-cid',
  APPNAME = 'x-lc-appName'
}
