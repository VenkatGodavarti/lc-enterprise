import {Deserializable} from './deserializable.model';
import { PhotographerAppointment } from './photographer-appointment.model';
import { TemplateInstance } from './template-instance.model';
import { ListingPhoto } from './listing-photo.model';
import { ServiceStatusType } from '@lc/core';
import { StateTransition } from './status-history';

export class ProductOption  implements Deserializable<ProductOption> {

  title: string;
  selected: boolean;
  thumbnailImageId: string;
  description: string;
  assets: any;

  deserialize(input: any): ProductOption {
    Object.assign(this, input);

    return this;
  }
}

export class ProductInstance implements Deserializable<ProductInstance> {

  title: string;
  description: string;
  code: string;
  kind: string;
  approvedByAgent: boolean;
  category: string;
  thumbnailUri: string;
  marketingCopyHeadline: string[];
  marketingCopyBody: string[];
  optional: boolean;
  optedOut: boolean;
  photos: ListingPhoto[];
  unitQuantity: number;
  unitPrice: number;
  unitValue: number;
  unitMultiple: number;
  price: number;
  position: number;
  options: ProductOption[];
  serviceAppointment: PhotographerAppointment;
  selectedTemplate: TemplateInstance;
  status: ServiceStatusType;
  maxOptionsSelectable;
  pageSizeChoice: string;
  paperTypeChoice: string;

  statusHistory: StateTransition[];

  constructor(data?: any) {
    this.deserialize(data || {});
  }

  deserialize(input: any): ProductInstance {
    Object.assign(this, input);

    if (this.serviceAppointment) {
      this.serviceAppointment = new PhotographerAppointment(this.serviceAppointment);
    }
    if (this.selectedTemplate) {
      this.selectedTemplate = new TemplateInstance(this.selectedTemplate);
    }
    return this;
  }
}
