import { Component, HostBinding, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  ActionTypes,
  ORDERSTATE,
  WorkflowAction,
  OrderListingFlowStateService,
  FlowState,
  SELECTEDORDER,
  MarketingOrder,
  UpdateOrderState,
  HeaderConstants,
  AddKeys, KEYS
} from '@lc/core';
import { ActivationEnd, convertToParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import * as uuid from 'uuid';

@Component({
  selector: 'lc-order-listing',
  templateUrl: './order-listing.component.html',
  styleUrls: ['./order-listing.component.scss']
})
/**
 * Just a placeholder component used to attach resolvers and children components
 * that rely on those resolvers
 */
export class OrderListingComponent implements OnDestroy {

  @HostBinding('class') private hostClasses = 'd-flex flex-grow-1 align-items-stretch h-100';

  routeSub: Subscription;
  stateSub: Subscription;
  orderSub: Subscription;

  // TODO - remove with 'withLatestFrom' operation
  currentOrder: MarketingOrder;
  currentState: FlowState;

  order$: Observable<MarketingOrder>;
  orderState$: Observable<FlowState>;
  keys$: Observable<any>;

  constructor(private router: Router,
              private orderListingFlowService: OrderListingFlowStateService,
              private store: Store<any>) {

    this.order$ = this.store.select<MarketingOrder>(SELECTEDORDER);
    this.orderState$ = this.store.select<FlowState>(ORDERSTATE);
    this.keys$ = this.store.select<any>(KEYS);

    // TODO - I am not real happy with this implementation. Would prefer to coordinate the two observables
    this.orderSub = this.order$.subscribe(order => {
      this.currentOrder = order;
      // Handle case when the listing is loaded and there is now state
      if (this.currentOrder && !this.currentState && this.currentOrder._id) {

        // TODO - make a new action and move this down to the workflow reducer
        const newState = orderListingFlowService.createNewFlowState(this.currentOrder);
        store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionInitialize, {state: newState},
          convertToParamMap({id: order._id, pid: this.currentOrder.selectedPackageCode})));
      }
    });

    // this pipe will combine the orderstate with the latest from the header keys
    const combined = this.orderState$.pipe(
      withLatestFrom(this.keys$),
      map(([orderState, keys]) => {
        return { orderState: orderState, keys: keys };
      })
    );

    this.stateSub = combined.subscribe(value => {
        if (!this.currentOrder) {
          return;
        }
        this.currentState = value.orderState;
        if (!this.currentState && this.currentOrder._id) {
          // handle case of a refresh
          // console.log('initialize state');
          // const newState = orderListingFlowService.createNewFlowState(this.currentListing);
          // store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionInitialize, {state: newState},
          //   convertToParamMap({lid: this.currentListing._id, pid: this.currentListing.selectedPackage.code})));
        } else {
          // use the correlationId if it exists already
          let cid = value.keys[HeaderConstants.CORRELATION_ID];
          if (!cid) {
            // no cid exists, so use the order state id or generate one if there is none
            cid = (this.currentOrder.getOrderState() && this.currentOrder.getOrderState().id) ?
              this.currentOrder.getOrderState().id :
              uuid.v4();
            // populate the id into state for the header keys
            const newKeys = {};
            newKeys[HeaderConstants.CORRELATION_ID] = cid;
            this.store.dispatch(new AddKeys(newKeys));
          }
          // save the current state of the order with the correlation id
          this.currentOrder.setOrderState(this.currentState ? {id: cid, state: this.currentState.serializeState()} : null);
          if (this.currentOrder._id) {
            store.dispatch(new UpdateOrderState(this.currentOrder, this.currentOrder.getOrderState()));
          }
        }
      });

    // look for events that have an workflow id assigned which would require setting the current step
    this.routeSub = this.router.events.pipe(
      filter(event => event instanceof ActivationEnd && event.snapshot.data && event.snapshot.data['id'])
    ).subscribe((event: ActivationEnd) => {
      this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionSetStep, {id: event.snapshot.data['id']}));
    });
  }

  ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.orderSub) {
      this.orderSub.unsubscribe();
    }
    if (this.stateSub) {
      this.stateSub.unsubscribe();
    }
  }
}
