import { Component, Input, OnInit } from '@angular/core';
import { PropertyStatuses, PropertyTypes, PropertyInfoForm } from '@lc/core';
import { Masks } from '@lc/shared-components-ui';

@Component({
  selector: 'lc-property-info-form',
  templateUrl: './property-info-form.component.html',
  styleUrls: ['./property-info-form.component.scss']
})
export class PropertyInfoFormComponent implements OnInit {
  readonly wholeNumberMask = Masks.wholeNumber;

  @Input()
  formGroup: PropertyInfoForm;

  propertytypes: string[] = PropertyTypes.all;
  propertystatus:string[] = PropertyStatuses.all;

  constructor() { }

  ngOnInit() {
  }

}
