export const environment = {
  // App configuration
  production: false,
  envName: 'development',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'https://lc.dev.listing-concierge.realogy.com/',
  apiBaseURL: 'https://lc.dev.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.dev.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.dev.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajcqv31m3erdpr00h7',
  oktaRedirectUri: 'https://lc.dev.listing-concierge.realogy.com/implicit/callback',

  //Stripe Key 

  stripePublishKey:'pk_test_GzgXKTGsDAOYaX6jHsDzEFQ800jzeOk9BL',
};

