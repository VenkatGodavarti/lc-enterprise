import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyInfoFormComponent } from './property-info-form.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { PropertyInfoForm } from '@lc/core';
import { ListingFactory } from '../../../../../core/src/lib/factories';

describe('PropertyInfoFormComponent', () => {
  const listingFactory = new ListingFactory();
  let listing;

  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, PropertyInfoFormComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    listing = listingFactory.createDefaultListing();
    component.propertyInfoComponent.formGroup = new PropertyInfoForm(listing);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('has info values', () => {

    const controls = component.propertyInfoComponent.formGroup.controls;
    expect(controls).toBeTruthy();
    expect(controls.listPrice.value).toEqual(listing.listPrice)
    expect(controls.bedrooms.value).toEqual(listing.bedrooms);
    expect(controls.bathrooms.value).toEqual(listing.bathrooms);

  });

});


/**
 * A simple component used to host the component during tests so that we can set its Input value
 * properly.
 *
 */
@Component({
  selector: 'lc-test',
  template: '<lc-property-info-form></lc-property-info-form>'
})
class TestComponent {
  @ViewChild(PropertyInfoFormComponent)
  public propertyInfoComponent: PropertyInfoFormComponent;

}
