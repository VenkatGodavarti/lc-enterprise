import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';

// Providers
import { mockedUserServiceProvider } from './mocks/user-service.mock';
import { mockedStoreProvider } from './mocks/store.mock';

@NgModule({
  imports: [
    StoreModule.forRoot({}),
  ],
  providers: [
    mockedUserServiceProvider,
    mockedStoreProvider
  ],
  exports: []
})
export class TestingModule {}
