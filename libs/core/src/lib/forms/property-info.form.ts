import { FormControl  } from '@angular/forms';
import { Listing } from '@lc/core';
import { BaseForm } from './base.form';
import { wholeNumberValidator } from '../validators/whole-number.validator';

export class PropertyInfoForm extends BaseForm {
  private BATHROOM_INCREMENT = 0.25;
  private BEDROOM_INCREMENT = 1;

  public get bedrooms(): number { return this.get('bedrooms').value; }
  public get bathrooms(): number { return this.get('bathrooms').value; }

  constructor(listing?: Listing) {
    super({
      propertyType: new FormControl(null),
      status: new FormControl(null),
      listPrice: new FormControl(null, [wholeNumberValidator()]),
      displayAsPriceOnRequest: new FormControl(false),
      bedrooms: new FormControl(0),
      bathrooms: new FormControl(0),
      sqFt: new FormControl(0),
      lotSqFt: new FormControl(0),
      listDate: new FormControl(null),
      closedDate: new FormControl(null),
      mlsId: new FormControl(null),
      globalLuxuryListing: new FormControl(null)
    });

    if (listing) {
      this.patchValue(listing);
    }
  }

  patchValue(value) {
    return super.patchValue(value);
  }

  incrementBedrooms() {
    const current = this.bedrooms || 0;
    this.setControlValue(this.controls.bedrooms,current + this.BEDROOM_INCREMENT);
  }

  decrementBedrooms() {
    const current = this.bedrooms || 0;
    if (current === 0) { return; }

    this.setControlValue(this.controls.bedrooms,current - this.BEDROOM_INCREMENT);
  }

  incrementBathrooms() {
    const current = this.bathrooms || 0;
    this.setControlValue(this.controls.bathrooms,current + this.BATHROOM_INCREMENT);
  }

  decrementBathrooms() {
    const current = this.bathrooms || 0;
    if (current === 0) { return; }
    this.setControlValue(this.controls.bathrooms,current - this.BATHROOM_INCREMENT);
  }

  setControlValue(control, value) {
    control.setValue(value);
    control.markAsDirty();
  }
}
