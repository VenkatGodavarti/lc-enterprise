
export enum OutputMimeType {
  pdf = 'application/pdf',
  html = 'text/html'
}
