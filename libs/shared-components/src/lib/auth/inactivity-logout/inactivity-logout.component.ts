import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '@lc/core';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'lc-inactivity-logout',
  templateUrl: './inactivity-logout.component.html',
  styleUrls: ['./inactivity-logout.component.scss']
})
export class InactivityLogoutComponent implements OnInit {
  @Input() title: string;

  readonly appName: string = environment.applicationName;

  constructor(
    public authService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.authService.logout();
  }

  login() {
    this.authService.redirectToLogin();
  }
}

