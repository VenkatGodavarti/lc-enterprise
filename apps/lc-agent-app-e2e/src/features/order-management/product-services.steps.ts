import { Then } from 'cucumber';
import { Configuration as config, Context, ContextKeys, LCPage } from '../../../../../e2e';
import { browser, ElementFinder, ExpectedConditions as EC } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const assert = chai.assert;

Then(/^The (.*) Content Preview is present$/,  { timeout: config.timeout },  async (product: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element: ElementFinder = page.finder('Content Preview');
  await browser.wait(EC.visibilityOf(element));

  let content: string;

  if(product === 'Flyer') {
    content = await element.getWebElement().getAttribute('src');
    expect(content.startsWith('blob\:')).to.be.true;
  } else if(product === 'Website') {
    // Using 'while' because it takes longer for the website iframe to load data.
    // Test will fail if it takes longer than config.timeout to
    while (!content) {
      content = await element.getWebElement().getAttribute('srcdoc');
    }
    const contentBegin: string = content.substr(0, 20);
    expect(contentBegin.startsWith('\<\!DOCTYPE html\>')).to.be.true;
  } else {
    assert.fail('Product string used is not recognized');
  }
});
