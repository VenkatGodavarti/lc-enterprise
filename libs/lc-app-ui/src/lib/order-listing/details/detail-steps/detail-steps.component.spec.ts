import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { DetailStepsComponent } from './detail-steps.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FlowState, ORDERSTATE } from '@lc/core';
import { TestingModule, StoreMock } from '@lc/testing';
import { Store } from '@ngrx/store';

describe('DetailStepsComponent', () => {
  let component: DetailStepsComponent;
  let fixture: ComponentFixture<DetailStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailStepsComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [
        TestingModule
      ]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
