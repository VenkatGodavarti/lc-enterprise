import {LogEntry} from './logentry';
import {LoggerFactory} from './logger-factory';
import { ConsoleAppender } from './console-appender';

describe('Logger', () => {

  const loggerFactory = new LoggerFactory();

  let loggedMessages: LogEntry[] = [];
  const testAppender = {
    append: function(entry: LogEntry) {
      loggedMessages.push(entry);
    }
  };

  beforeEach(() => {
  });


  afterEach(() => {
    loggedMessages = [];
  });

  it('should be created', () => {

    const logger = loggerFactory.getLogger('test-category-1');

    expect(logger).toBeTruthy();
  });



  it('should log a debug message', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('DEBUG');
    logger.removeAllAppenders();
    logger.addAppender(testAppender);

    logger.debug('There is no argument here, Michael Henderson');
    expect(loggedMessages.length).toEqual(1);
    const message = loggedMessages[0].message;
    expect(message).toBeTruthy();
    expect(message.indexOf('Michael')).toBeTruthy();
  });


  it('should not log a debug message', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('INFO');
    logger.removeAllAppenders();
    logger.addAppender(testAppender);

    logger.debug('There is an argument here Michael Henderson');
    expect(loggedMessages.length).toEqual(0);
  });


  it('levels are enabled', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('TRACE');

    expect(logger.isTraceEnabled()).toBeTruthy();
    expect(logger.isDebugEnabled()).toBeTruthy();
    expect(logger.isInfoEnabled()).toBeTruthy();
    expect(logger.isWarnEnabled()).toBeTruthy();
    expect(logger.isErrorEnabled()).toBeTruthy();

  });


  it('some levels are enabled', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('INFO');

    expect(logger.isTraceEnabled()).toBeFalsy();
    expect(logger.isDebugEnabled()).toBeFalsy();
    expect(logger.isInfoEnabled()).toBeTruthy();
    expect(logger.isWarnEnabled()).toBeTruthy();
    expect(logger.isErrorEnabled()).toBeTruthy();
  });

  it('levels are not enabled', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('ERROR');

    expect(logger.isTraceEnabled()).toBeFalsy();
    expect(logger.isDebugEnabled()).toBeFalsy();
    expect(logger.isInfoEnabled()).toBeFalsy();
    expect(logger.isWarnEnabled()).toBeFalsy();
    expect(logger.isErrorEnabled()).toBeTruthy();
  });

  it('logging is completely disabled', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('NONE');

    expect(logger.isTraceEnabled()).toBeFalsy();
    expect(logger.isDebugEnabled()).toBeFalsy();
    expect(logger.isInfoEnabled()).toBeFalsy();
    expect(logger.isWarnEnabled()).toBeFalsy();
    expect(logger.isErrorEnabled()).toBeFalsy();
  });

  it('logging is completely enabled for all levels', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('ALL');

    expect(logger.isTraceEnabled()).toBeTruthy();
    expect(logger.isDebugEnabled()).toBeTruthy();
    expect(logger.isInfoEnabled()).toBeTruthy();
    expect(logger.isWarnEnabled()).toBeTruthy();
    expect(logger.isErrorEnabled()).toBeTruthy();
  });

  it('messages logged, only when level is enabled', () => {

    const logger = loggerFactory.getLogger('test-category-1');
    logger.setLevel('INFO');
    logger.removeAllAppenders();
    logger.addAppender(testAppender);


    logger.trace("a trace message");
    logger.debug("a debug message");
    logger.info("an info message");
    logger.warn("a warn message");
    logger.error("an error message");

    expect(loggedMessages.length).toEqual(3);
    expect(loggedMessages.filter( (e) => e.category === 'default').length).toEqual(0);
    expect(loggedMessages.filter( (e) => e.category === 'test-category-1').length).toEqual(3);
    expect(loggedMessages.filter( (e) => e.level === 'TRACE').length).toEqual(0);
    expect(loggedMessages.filter( (e) => e.level === 'DEBUG').length).toEqual(0);
    expect(loggedMessages.filter( (e) => e.level === 'INFO').length).toEqual(1);
    expect(loggedMessages.filter( (e) => e.level === 'WARN').length).toEqual(1);
    expect(loggedMessages.filter( (e) => e.level === 'ERROR').length).toEqual(1);
  });

  it('test dynamic configuration', () => {
    const lfConfig = {
      'myDebugLoggerConfig': {
        level: 'DEBUG',
        appenders: [
          new ConsoleAppender()
        ]
      }
    };
    const customLoggerFactory = new LoggerFactory();
    customLoggerFactory.setConfiguration(lfConfig);
    const logger = customLoggerFactory.getLogger('myDebugLoggerConfig');
    logger.removeAllAppenders();
    logger.addAppender(testAppender);

    expect(logger.isDebugEnabled()).toBeTruthy();
    logger.debug("a debug message");
    expect(loggedMessages.filter( (e) => e.category === 'myDebugLoggerConfig').length).toEqual(1);
  });

});
