import { TestBed } from '@angular/core/testing';

import { PresignService } from './presign.service';
import { ListingPhotoFactory } from '../factories';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('PresignService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: PresignService = TestBed.get(PresignService);
    expect(service).toBeTruthy();
  });

  it('should assign the presigned urls appropriately', async() => {
    const service: PresignService = TestBed.get(PresignService);

    const mockedSuffix = '/presigned'

    // Only mock out the presignedUrls method so we can unit test the presignPhoto method
    spyOn(service, 'presignUrls').and.callFake((urls: string[]) => of(urls.map(url => url + mockedSuffix)));

    // Call presignPhoto with the photo to presign the url on
    const photo = ListingPhotoFactory.photoOne;
    service.presignPhoto(photo).subscribe(result => {

      //Validate that the main photo url received the proper presigning
      expect(result.presignedUrl).toBe(photo.uri + mockedSuffix);

      //Validate that each of the thumbnail urls received the proper presigning
      result.thumbnails.forEach(thumbnail => {
        expect(thumbnail.presignedUrl).toBe(thumbnail.originalUrl + mockedSuffix);
      })
    });
  });
});
