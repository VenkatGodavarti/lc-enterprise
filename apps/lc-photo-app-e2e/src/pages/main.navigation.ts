import { SelectPhotographerPage } from './select-photographer.page';
import { AppointmentListPage } from './appointment-list.page';
import { MainPage } from './main.page';

export const mainPages = {
  'main': {
    'home' : {
      url: '',
      page: new MainPage()

    },
    'select photographer' : {
      url: '',
      page: new SelectPhotographerPage()
    },
    'appointment list' : {
      url: '/appointment-list/:photographerId',
      page: new AppointmentListPage()
    },
  },
};
