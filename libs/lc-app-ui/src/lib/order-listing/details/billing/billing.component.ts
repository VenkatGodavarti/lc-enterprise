import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionsSubject, Store } from '@ngrx/store';
import {
  ActionTypes, AddressInformation,
  AddressInformationForm,
  AddressType,
  MarketingOrder,
  OrderActionTypes,
  PhoneType,
  SELECTEDORDER,
  ServiceStatusType,
  STATES_BY_ABBREVIATION,
  SubmitOrder,
  SubmitOrderComplete,
  UpdateOrder,
  UserService,
  USStates,
  WorkflowAction
} from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { ofType } from '@ngrx/effects';
import { filter, tap } from 'rxjs/operators';
import { Masks } from '@lc/shared-components-ui';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'lc-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit, OnDestroy {
  readonly states: USStates[] = STATES_BY_ABBREVIATION;
  readonly phoneMask = Masks.phoneNumber;
  currentOrder$: Observable<MarketingOrder>;

  /**
   * Making exception here because we need to initialize it for UI to work
   */
  currentOrder: MarketingOrder;

  // billing address form
  billingForm: AddressInformationForm;
  params: any;

  subjectSub: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private actionSubject: ActionsSubject,
              private userService: UserService,
              private store: Store<any>) {


    // wait for an action change that matches complete
    this.subjectSub = this.actionSubject.pipe(
      ofType<SubmitOrderComplete>(OrderActionTypes.SubmitOrderComplete)
    ).subscribe((data: SubmitOrderComplete) => {
      // created listing or updated with no package selected 61

      // onsbmit Billig (Place order)
      const pageRoute = '/manage-order/' + data.payload._id + '/confirmation/';
      // this.router.navigate([pageRoute]);
    });

    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  ngOnInit() {
    this.billingForm = new AddressInformationForm();
    this.billingForm.get('fullName').setValidators([Validators.required]);

    // initialize the form if the order is not null. The subscription is on html page
    this.currentOrder$ = this.store.select(SELECTEDORDER).pipe(
      filter(order => order),
      tap(order => this.patchForm(order.billingInformation))
    );

    // populate address type if it's not already
    if(!this.billingForm.get('type').value) {
      this.billingForm.controls['type'].setValue(AddressType.BILLING);
    }

    // populate required phoneNumber fields if necessary
    const phoneNumber = this.billingForm.get('phoneNumber') as FormGroup;
    if(!phoneNumber.get('type').value) {
      phoneNumber.get('type').setValue(PhoneType.direct);
    }
  }

  ngOnDestroy(): void {
    if (this.subjectSub) {
      this.subjectSub.unsubscribe();
    }
  }

  patchForm(addressInfo) {
    if (addressInfo) {
      this.billingForm.patchValue(addressInfo);
      this.billingForm.get('type').setValue(AddressType.BILLING);
    } else {
      this.billingForm.patchValue(this.getBillingContactInfo());
    }
  }

  getShippingDetails(event, order) {
    if (event.target.checked && order) {
      this.patchForm(order.shippingInformation);
    } else {
      this.patchForm(null);
    }
    event.stopPropagation();
  }

  getBillingContactInfo() {
    let address = this.userService.getAddress(AddressType.BILLING);
    if (!address) {
      address = new AddressInformation();
    }
    return address;
  }

  goBack(order) {
    // dispatch and save the data state - Just a hack for LC-604
    if(this.billingForm.valid) {
      order.billingInformation = this.billingForm.value;
      this.store.dispatch(new UpdateOrder(order));
    }
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(order: MarketingOrder) {
    this.billingForm.markAllAsDirty();
    if(!this.billingForm.valid) { return; }

    // remove any keys with null values before populating the model
    const formData = this.billingForm.value;
    Object.keys(formData).forEach((key) => (formData[key] == null) && delete formData[key]);
    Object.keys(formData.phoneNumber).forEach((key) => (formData.phoneNumber[key] == null) && delete formData.phoneNumber[key]);

    order.billingInformation = formData;

    // This is the last step of order entry, so we are complete
    //order.submitted = true;

    // submit order with or without a photoorder
    const product = order.getSelectedPackage().getPhotographyProduct();
    if(product) {
      // TODO - we need to set the state of the product instance in the API on submit order api
      product.status = ServiceStatusType.PENDING;
      //TODO from LC-416: replace currentUser with store.select(LOGGEDINUSER)
      order.photoOrder = order.createPhotoOrder(this.userService.getCurrentUser());
    }
    //this.store.dispatch(new SubmitOrder(order));

    this.store.dispatch(new UpdateOrder(order));
    // next step
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, {stepCompleted: true}, this.params));

  }
}
