import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ImageUploaderService,
  PhoneNumber,
  PhoneType,
  PhotoService,
  Profile,
  ProfileService,
  SELECTEDPROFILE, ThumbnailSizes,
  UpdateProfile,
  UploadStatus
} from '@lc/core';
import { Observable, combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { ProfileForm } from '@lc/core';
import { FormControl } from '@angular/forms';
import {
  ImageCropperDialogService,
  CropSize,
  ImageCropperResponse,
  UploadResultsDialogService,
  Masks
} from '@lc/shared-components-ui';

@Component({
  selector: 'lc-app-my-profile',
  templateUrl: './my-profile-main.component.html',
  styleUrls: ['./my-profile-main.component.scss'],
})
export class MyProfileMainComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('content') content: any;

  profile$: Observable<Profile>;
  private profile: Profile;

  readonly phoneMask = Masks.phoneNumber;

  // edit profile
  readonly myProfileForm: ProfileForm = new ProfileForm(null, {validateType: true, phoneNotRequired: true });

  phoneTypes = Object.keys(PhoneType).filter((type) => isNaN(<any>type) && type !== 'values');
  priorityDisplayMap = { 0: 'Primary', 1: 'Secondary', 2: 'Tertiary'};

  logoStatus: UploadStatus; // Image upload status updated by the ImageUploaderService
  editLogo: boolean; // helps template determine whether or not to display the edit icon
  allowedExtensions = []; // image file extensions allowed (.png, .jpg, etc)

  constructor(private store: Store<any>,
              private photoService: PhotoService,
              private uploadResultsDialog: UploadResultsDialogService,
              private imageUploaderService: ImageUploaderService,
              private dialogService: ImageCropperDialogService,
  ) {
    this.profile$ = store.select(SELECTEDPROFILE).pipe(
      tap(profile => this.profile = profile),
      tap(profile => this.initMyProfileForm(profile))
    );

    this.allowedExtensions = imageUploaderService.allowedExtensions.map((e) => ('.' + e));
  }

  ngOnInit() { }

  initMyProfileForm(userProfile: Profile) {
    // loop through the enum of phone number priorities and add up to 3 if not already three
    const phoneNumbers = userProfile.phoneNumbers.slice();
    while(phoneNumbers.length < 3) {
      const nextAvailablePriority = [0, 1, 2].filter(number => !phoneNumbers.some(phone => phone.priority === number))[0];
      phoneNumbers.splice(nextAvailablePriority, 0, new PhoneNumber({priority: nextAvailablePriority}));
    }

    // initialize the UI form
    const customValues = Object.assign({}, userProfile, { phoneNumbers: phoneNumbers });
    this.myProfileForm.patchValue(customValues);
    this.myProfileForm.markAsPristine();
  }

  // update profile details
  onSubmit() {
    this.myProfileForm.markAllAsDirty();
    if(!this.myProfileForm.valid) { return; }

    const formData: Profile = this.myProfileForm.value;
    formData._id = this.profile._id;

    formData.phoneNumbers = formData.phoneNumbers.filter(number => (number.number || '').trim() !== '');

    // dispatch profile updates to the user store
    this.store.dispatch( new UpdateProfile(formData,
      ['preferredFirstName', 'preferredLastName', 'preferredEmail', 'website', 'phoneNumbers', 'photoUrl', 'originalPhotoUrl', 'logoUrl']));
  }

  resetProfileForm() {
    this.initMyProfileForm(this.profile);
    this.clearEditStatus();
  }

  onEditPhoto() {
    if(this.myProfileForm.get('originalPhotoUrl').value){
      this.photoService.getImage(this.myProfileForm.get('originalPhotoUrl').value)
        .subscribe(res => this.cropImage(res[0].body));
    } else {
      this.cropImage(this.profile);
    }
  }

  private cropImage(file?: any){
    this.dialogService.openImageCropper(file, [new CropSize('Avatar', 80, 80, true),
      new CropSize('Marketing Products', 120,  80)], null)
      .then(response => {
        if(!response) { return; }
        if(response.isDelete) {
          this.deleteImage();
        } else if(response.croppedFile) {
          this.uploadImage(response);
        }
      });
  }

  private deleteImage() {
    this.myProfileForm.patchValue({photoUrl: null, originalPhotoUrl: null});
  }

  private uploadImage(response: ImageCropperResponse) {
    if(!response){ return; }

    (<any>response.croppedFile).name = "profilePhoto.png";

    // Upload images synchronously and then update the profile with both images
    combineLatest(this.photoService.upload(response.originalFile), this.photoService.upload(response.croppedFile))
      .subscribe(([originalPhotos, croppedPhotos]) => {
        const original: any = originalPhotos.find((photo: any) => photo.original);
        const cropped: any = croppedPhotos.find((photo: any) => photo.original);

        // Treat the photo upload as an change in a form field
        // Update the form value, but since the form is not submitted yet, do not update the profile
        this.myProfileForm.patchValue({photoUrl: cropped.url, originalPhotoUrl: original.url});
        this.myProfileForm.markAllAsDirty();
      });
  }

  /**
   * Removes the logo url from the profile and updates to save the change
   */
  private deleteLogo() {
    this.editLogo = true;
    this.myProfileForm.get('logoUrl').setValue(null);
  }

  /**
   * Removes the logo url from the profile form only
   */
  private replaceLogo() {
    this.editLogo = true;
    this.myProfileForm.get('logoUrl').setValue(null);
  }

  /**
   * Fired when the user uploads a file through lc-file-upload InputField
   * @param files the array of files uploaded through lc-file-upload
   */
  private uploadLogo(files: File[]) {
    const self = this;
    // lc-file-upload allows for an array of files but we only want one
    this.logoStatus = new UploadStatus(files[0]);

    this.imageUploaderService.upload(this.logoStatus, ThumbnailSizes.STANDARD_THUMBNAIL_SPECS, true).subscribe((status) => {
      // TODO we don't yet have any requirements for error handling.  Opening a modal for now.
      if(status && status.error) {
        self.uploadResultsDialog.openResults([status.error])
          .then(() => this.clearEditStatus());
      } else {
        const url = self.logoStatus.photos[0].url;

        // Like the profile photo, the upload is not submitted so update the form and mark as dirty but do not change profile
        self.myProfileForm.get('logoUrl').setValue(url);
        this.myProfileForm.markAllAsDirty();
        this.clearEditStatus();
      }
    });
  }

  /**
   * Resets file upload related statuses
   */
  private clearEditStatus() {
    this.editLogo = false;
    this.logoStatus = null;
  }

}

