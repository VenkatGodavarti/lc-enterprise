import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'lc-package-option-details',
  templateUrl: './package-options-details.component.html',
  styleUrls: ['./package-options-details.component.scss']
})
export class PackageOptionsDetailsComponent implements OnInit {

  detailsForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.detailsForm = new FormGroup({});
  }

  onSubmit() {
    if (!this.detailsForm.valid) {
      return;
    }
    this.router.navigate(['../checkout'], {relativeTo: this.activatedRoute});
  }
}
