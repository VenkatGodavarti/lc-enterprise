import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lc-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product;

  constructor() {
  }

  ngOnInit() {
  }
}
