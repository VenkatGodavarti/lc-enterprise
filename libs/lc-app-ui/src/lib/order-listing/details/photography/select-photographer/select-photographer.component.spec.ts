import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { SelectPhotographerComponent } from './select-photographer.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import {
  AuthenticationService,
  FlowState,
  Listing,
  Photographer,
  PhotographerAppointment,
  PhotographerService,
  ORDERSTATE, MarketingOrder, SELECTEDORDER
} from '@lc/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderStepsComponent } from '../../../steps/order-steps/order-steps.component';
import { OrderStepTemplateComponent } from '../../../steps/order-steps/order-step-template/order-step-template.component';
import { DetailStepsComponent } from '../../detail-steps/detail-steps.component';
import { DetailStepTemplateComponent } from '../../detail-steps/detail-step-template/detail-step-template.component';
import { TestingModule, StoreMock } from '@lc/testing';
import { StepContentComponent } from '../../step-content/step-content.component';
import { OrderSummaryComponent } from '../../order-summary/order-summary.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

describe('SelectPhotographerComponent', () => {
  let component: SelectPhotographerComponent;
  let fixture: ComponentFixture<SelectPhotographerComponent>;
  let photographerService: SpyObj<PhotographerService>;
  let authenticationService: SpyObj<AuthenticationService>;
  let order: MarketingOrder;
  let store: StoreMock<any>;
  const photographers: Photographer[] = [];

  beforeEach(async(() => {

    photographerService = jasmine.createSpyObj('PhotographerService', ['retrievePhotographers']);
    authenticationService = jasmine.createSpyObj('AuthenticationService', ['getToken']);

    photographerService.retrievePhotographers.and.returnValue(of(photographers));

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule,
        FormsModule, ReactiveFormsModule,
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [
        SelectPhotographerComponent,
        OrderStepsComponent,
        OrderStepTemplateComponent,
        DetailStepsComponent,
        DetailStepTemplateComponent,
        StepContentComponent,
        OrderSummaryComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
        { provide: PhotographerService, useValue: photographerService },
        { provide: AuthenticationService, useValue: authenticationService }
      ],
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    order = createOrder(false);
    store.setState(SELECTEDORDER, order); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  // Create the component
  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPhotographerComponent);
    component = fixture.componentInstance;

    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  function createOrder(hasAppt: boolean) {
    const newListing = new Listing();
    newListing._id = '123456789ABCDEF';
    newListing.listPrice = 400000;
    newListing.sqFt = 4000;
    newListing.lotSqFt = 14000;
    newListing.listDate = new Date();

    const newAppt = new PhotographerAppointment();

    const newOrder : MarketingOrder = new MarketingOrder();
    newOrder.listing = newListing;

    return newOrder;
  }
});
