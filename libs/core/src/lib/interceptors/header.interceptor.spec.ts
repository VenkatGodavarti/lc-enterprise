import { TestBed } from '@angular/core/testing';
import { AuthenticationService, PhotoService } from '@lc/core';
import { TokenInterceptor } from './token.interceptor';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import SpyObj = jasmine.SpyObj;
import { ProfileService } from '../services/profile.service';

describe('TokenInterceptor', () => {

  let httpMock: HttpTestingController;
  let authServiceSpy: SpyObj<AuthenticationService>;
  let profileServiceSpy: SpyObj<ProfileService>;
  let service: PhotoService;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['isAuthenticated', 'getHeaderOptions']);
    profileServiceSpy = jasmine.createSpyObj('ProfileService', ['']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PhotoService,
        {provide: AuthenticationService, useValue: authServiceSpy},
        {provide: ProfileService, useValue: profileServiceSpy},
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        }
      ]
    });

    httpMock = TestBed.get(HttpTestingController);
    authServiceSpy = TestBed.get(AuthenticationService);
    service = TestBed.get(PhotoService);
  });

  describe('#intercept', () => {

    it('should authenticate and clone request of a non-AWS url', () => {
      const imgUrl = 'unit.test.local/url';

      authServiceSpy.isAuthenticated.and.returnValue(true);

      service.getImage(imgUrl).subscribe((result) => { });

      const imgReq = httpMock.expectOne(imgUrl);
      expect(imgReq.request.method).toBe('GET');
      imgReq.flush(null);
    });
  })


});
