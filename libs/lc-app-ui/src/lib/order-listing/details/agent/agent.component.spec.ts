import { async, ComponentFixture, TestBed, inject, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AgentComponent } from './agent.component';
import { DetailStepsComponent } from '../detail-steps/detail-steps.component';
import { FlowState, Listing, ORDERSTATE, MarketingOrder, SELECTEDORDER, AgentTypes, LOGGEDINUSER, User } from '@lc/core';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { TestingModule, UserServiceMock } from '@lc/testing';
import { StoreMock } from 'libs/testing/src/lib/mocks/store.mock';
import { OrderListingModule } from '../../order-listing.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('AgentComponent', () => {
  let component: AgentComponent;
  let fixture: ComponentFixture<AgentComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule,
        OrderListingModule,
        TestingModule,
      ],
      declarations: [
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
    const order : MarketingOrder = new MarketingOrder();
    order.listing = new Listing();
    store.setState(SELECTEDORDER, order);
    store.setState(LOGGEDINUSER, UserServiceMock.mockedUser);
  }));

  // Create the component
  beforeEach(async(() => {
    fixture = TestBed.createComponent(AgentComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', async() => {
    expect(component).toBeTruthy();

    component.agentName$.subscribe(agentName => {
      expect(agentName).toEqual(UserServiceMock.mockedUser.firstName + ' ' + UserServiceMock.mockedUser.lastName);
    })
  });

  describe('#setAgentType', () => {
    it('none should be selected when created', () => {
      const singleAgentImage = fixture.debugElement.query(By.css('a#card-option-single-agent img'));
      expect(singleAgentImage.properties.src).toEqual('/assets/images/singleagentoff.svg');

      const colistingImage = fixture.debugElement.query(By.css('a#card-option-co-listing img'));
      expect(colistingImage.properties.src).toEqual('/assets/images/colistingoff.svg');

      const teamImage = fixture.debugElement.query(By.css('a#card-option-team img'));
      expect(teamImage.properties.src).toEqual('/assets/images/teamoff.svg');

    });

    it('should be single agent listing type selected when selected', () => {

      component.setAgentType(AgentTypes.singleAgent);
      fixture.detectChanges();

      const singleAgentImage = fixture.debugElement.query(By.css('a#card-option-single-agent img'));
      expect(singleAgentImage.properties.src).toEqual('/assets/images/singleagenton.svg');

      const colistingImage = fixture.debugElement.query(By.css('a#card-option-co-listing img'));
      expect(colistingImage.properties.src).toEqual('/assets/images/colistingoff.svg');

      const teamImage = fixture.debugElement.query(By.css('a#card-option-team img'));
      expect(teamImage.properties.src).toEqual('/assets/images/teamoff.svg');

    });

    it('should be colisting listing type selected when selected', () => {

      component.setAgentType(AgentTypes.twoAgents);
      fixture.detectChanges();

      const singleAgentImage = fixture.debugElement.query(By.css('a#card-option-single-agent img'));
      expect(singleAgentImage.properties.src).toEqual('/assets/images/singleagentoff.svg');

      const colistingImage = fixture.debugElement.query(By.css('a#card-option-co-listing img'));
      expect(colistingImage.properties.src).toEqual('/assets/images/colistingon.svg');

      const teamImage = fixture.debugElement.query(By.css('a#card-option-team img'));
      expect(teamImage.properties.src).toEqual('/assets/images/teamoff.svg');

    });

    it('should be team listing type selected when selected', () => {

      component.setAgentType(AgentTypes.team);
      fixture.detectChanges();

      const singleAgentImage = fixture.debugElement.query(By.css('a#card-option-single-agent img'));
      expect(singleAgentImage.properties.src).toEqual('/assets/images/singleagentoff.svg');

      const colistingImage = fixture.debugElement.query(By.css('a#card-option-co-listing img'));
      expect(colistingImage.properties.src).toEqual('/assets/images/colistingoff.svg');

      const teamImage = fixture.debugElement.query(By.css('a#card-option-team img'));
      expect(teamImage.properties.src).toEqual('/assets/images/teamon.svg');

    });
  });
});
