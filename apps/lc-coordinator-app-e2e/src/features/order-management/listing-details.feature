@listing-details
@order-management
@login-before-all
Feature: Order Management

  Background:
    Given I am logged in to Listing Concierge
    Given the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Street               | 123 Main St           |
      | Unit Number          | 1                     |
      | City                 | Madison               |
      | State                | NJ                    |
      | Zip Code             | 07040                 |
      | Property Information | default               |
      | Photos               | none                  |
      | Status               | PENDING               |
  When I select the Alamo order
  And I go to the Listing Details page in Order Management
  Then the Listing Details page from Order Management is loaded

  Scenario Outline: Enter Invalid List Price
    And I set the List Price to "<input>"
    Then the List Price value is set to "<output>"
    Examples:
      | input   | output  |
      | 0       |         |
      | abc     |         |
      | 123.123 | 123123  |
      | 123     | 123     |
      | 123,1   | 1231    |
      | 123  1  | 1231    |
      | 123$1   | 1231    |

  @bathrooms
  Scenario: Verify Bathroom Count Input
    And the "Bathroom Count" text is "0"
    When I click on the Bathroom Increment button to 4 times
    Then the "Bathroom Count" text is "1"
    When I click on the Bathroom Decrement button to 4 times
    Then the "Bathroom Count" text is "0"
    When I click on the Bathroom Decrement button
    Then the "Bathroom Count" text is "0"
    And I click on the Bathroom Decrement button
    Then the "Bathroom Count" text is "0"

  @bedrooms
  Scenario: Verify Bedroom Count Input
    And the "Bedroom Count" text is "0"
    When I click on the Bedroom Increment button to 4 times
    Then the "Bedroom Count" text is "4"
    When I click on the Bedroom Decrement button to 2 times
    Then the "Bedroom Count" text is "2"
    When I click on the Bedroom Decrement button to 2 times
    Then the "Bedroom Count" text is "0"
    When I click on the Bedroom Decrement button
    Then the "Bedroom Count" text is "0"
    And I click on the Bedroom Decrement button
    Then the "Bedroom Count" text is "0"

  @sqft
  Scenario Outline: Verify SQFT input
    And I set the SQFT to "<input>"
    Then the SQFT value is set to "<output>"
    Examples:
      | input   | output  |
      | 0       | 0       |
      | abc     |         |
      | 123.123 | 123.123 |
      | 123     | 123     |
      | 123,1   | 1231    |
      | 123  1  | 1231    |
      | 123$1   | 1231    |

  @lot-size
  Scenario Outline: Verify Lot SqFT input
    And I set the Lot Size to "<input>"
    Then the Lot Size value is set to "<output>"
    Examples:
      | input   | output  |
      | 0       | 0       |
      | abc     |         |
      | 123.123 | 123.123 |
      | 123     | 123     |
      | 123,1   | 1231    |
      | 123  1  | 1231    |
      | 123$1   | 1231    |

  @list-date
  Scenario Outline: Verify List Date input
    And I set the List Date to "<input>"
    Then the List Date value is set to "<output>"
    Examples:
      | input        | output       |
      | 01/01/0001   | 01/01/0001   |
      | 12/31/0000   | 12/31/0001   |
      | 01/01/2000   | 01/01/2000   |
      | 13/32/2019   | 12/31/2019   |
      | 12/31/275760 |              |
      | 01/01/275761 | 01/01/275760 |

  @closed-date
  Scenario Outline: Verify Closed Date input
    And I set the Closed Date to "<input>"
    Then the Closed Date value is set to "<output>"
    Examples:
      | input        | output       |
      | 01/01/0001   | 01/01/0001   |
      | 12/31/0000   | 12/31/0001   |
      | 01/01/2000   | 01/01/2000   |
      | 13/32/2019   | 12/31/2019   |
      | 12/31/275760 |              |
      | 01/01/275761 | 01/01/275760 |

  @display-listing-details
  Scenario: Display Listing Details
    Then the "Street" value is "123 Main St"
    And the "Unit Number" value is "1"
    And the "City" value is "Madison"
    And the "State" value is "NJ"
    And the "Zip Code" value is "07040"
    And the "Property Type" value is "Single family"
    And the "Listing Status" value is "Active"
    And the "List Price" value is ""
    And the "Bedroom Count" text is "0"
    And the "Bathroom Count" text is "0"
    And the "SQFT" value is "0"
    And the "Lot Size" value is "0"
    And the "List Date" value is ""
    And the "Closed Date" value is ""

  @update-listing-details-and-cancel
   Scenario Outline: Update Listing Details text-fields and Cancel

    And I set the <field> to "<new-value>"
    Then the <field> value is set to "<new-value>"
    When I click on the Cancel button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the <field> value is set to "<old-value>"
    Examples:
      | field                | old-value             | new-value        |
      | Street               | 123 Main St           | 1117 Jackson Ave |
      | Unit Number          | 1                     | 23               |
      | City                 | Madison               | Morristown       |
      | Zip Code             | 07040                 | 10045            |
      | MLS ID               |                       | 123456           |
      | List Price           |                       | 800000           |
      | SQFT                 | 0                     | 2400             |
      | Lot Size             | 0                     | 8000             |
      | Property Type        | Single family         | Residential      |
      | Listing Status       | Active                | Pending          |

  @update-listing-details-and-cancel
  Scenario: Update Listing Details bedrooms and Cancel
    When I click on the Bedroom Increment button to 4 times
    Then the "Bedroom Count" text is "4"
    When I click on the Cancel button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the "Bedroom Count" text is "0"

  @update-listing-details-and-cancel
  Scenario: Update Listing Details bathrooms and Cancel
    When I click on the Bathroom Increment button to 8 times
    Then the "Bathroom Count" text is "2"
    When I click on the Cancel button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the "Bathroom Count" text is "0"

  @update-listing-details-and-save
  Scenario Outline: Update Listing Details text-fields and Save
    And I set the <field> to "<new-value>"
    Then the <field> value is set to "<new-value>"
    When I click on the Save button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the <field> value is set to "<new-value>"
    Examples:
      | field                | new-value        |
      | Street               | 1117 Jackson Ave |
      | Unit Number          | 23               |
      | City                 | Morristown       |
      | State                | NY               |
      | Zip Code             | 10045            |
      | MLS ID               | 123456           |
      | List Price           | 800000           |
      | SQFT                 | 2400             |
      | Lot Size             | 8000             |
      | Property Type        | Residential      |
      | Listing Status       | Pending          |

  @update-listing-details-and-save
  Scenario: Update Listing Details bedrooms and Save
    When I click on the Bedroom Increment button to 4 times
    Then the "Bedroom Count" text is "4"
    When I click on the Save button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the "Bedroom Count" text is "4"

  @update-listing-details-and-save
  Scenario: Update Listing Details list data and Save
    When I click on the Bathroom Increment button to 8 times
    Then the "Bathroom Count" text is "2"
    When I click on the Save button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the "Bathroom Count" text is "2"

  @update-listing-details-and-save
  @update-list-date-and-save
  Scenario: Update Listing Details list date and Save
    And I set the List Date to "07/21/2019"
    When I click on the Save button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the List Date" value is set to "07/21/2019"

  @update-listing-details-and-save
  @update-list-date-and-save
  Scenario: Update Listing Details closed date and Save
    And I set the Closed Date to "07/21/2019"
    When I click on the Save button
    Then the Summary page from Order Management is displayed
    When I go to the Listing Details page in Order Management
    Then the Closed Date value is set to "07/21/2019"
