import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const elements = new ElementMap( 'page',  [
    new Element({ name: '8x8', field: "#card-option-8-x-8-in" }),
    new Element({ name: '85x11', field: "#card-option-85-x-11-in" }),
    new Element({ name: '11x17', field: "#card-option-11-x-17-in" }),
    new Element({ name: 'Other', field: "#card-option-other" }),
    new Element({ name: 'Glossy', field: "#card-option-glossy" }),
    new Element({ name: 'Matte', field: "#card-option-matte" }),
    new Element({ name: 'Soft touch', field: "#card-option-soft-touch" }),
    new Element({ name: 'Paper Size Error', field: "#paper-size-error" }),
    new Element({ name: 'Paper Type Error', field: "#paper-type-error" }),
    new Element({ name: 'Back', field: "#backBtn" }),
    new Element({ name: 'Next', field: "#nextBtn" })
  ]
);


export class BrochureOptionsPage extends LCPage {
  constructor() {
    super('Brochure Options', [elements]);
  }
}
