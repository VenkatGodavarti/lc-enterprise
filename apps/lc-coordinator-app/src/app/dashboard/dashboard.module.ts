import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const angularModules = [ CommonModule, FormsModule, ReactiveFormsModule ];

import { NgbTabsetModule, NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
const thirdPartyModules = [NgbTabsetModule, NgbPaginationModule, NgbModule];

import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { DashboardRoutingModule } from './dashboard-routing.module';
const internalModules = [ SharedComponentsUiModule, DashboardRoutingModule ];

import { DashboardComponent } from './dashboard.component';
import { AllOrdersComponent } from './views/all-orders/all-orders.component';
import { OrderTableComponent } from './components/order-table/order-table.component';
import { SortableDirective } from './directives/sortable.directive';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { CoreModule } from '@lc/core';
const internalComponents = [ DashboardComponent, AllOrdersComponent, OrderTableComponent ];

@NgModule({
  declarations: [...internalComponents, SortableDirective, PaginatorComponent],
  imports: [
    ...angularModules,
    ...thirdPartyModules,
    ...internalModules,
    CoreModule
  ]
})
export class DashboardModule { }
