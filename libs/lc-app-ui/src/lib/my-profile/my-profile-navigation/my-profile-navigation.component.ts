import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'lc-my-profile-navigation',
  templateUrl: './my-profile-navigation.component.html',
  styleUrls: ['./my-profile-navigation.component.scss']
})
export class MyProfileNavigationComponent implements OnInit {

  public isExpanded = true;

  readonly tabs = [
    {isSelected: true, label: 'MY PROFILE', path: 'main', svgPath: 'assets/images/profile/my_profile-icon.svg'},
    {isSelected: false, label: 'MY TEAM', path: 'team', svgPath: 'assets/images/profile/teams-icon.svg'},
    {isSelected: false, label: 'ADDRESSES', path: 'addresses', svgPath: 'assets/images/profile/addresses-icon.svg'},
    {isSelected: false, label: 'NOTIFICATIONS', path: 'notifications', svgPath: 'assets/images/profile/notifications-icon.svg'},
  ];

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {

    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        const path = this.activatedRoute.snapshot.firstChild.url[0].path;
        this.tabs.forEach(tab => tab.isSelected = tab.path === path);
      });
  }

  ngOnInit() {
  }
}
