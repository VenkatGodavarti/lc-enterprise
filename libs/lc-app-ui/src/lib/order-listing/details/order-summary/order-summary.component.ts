import { Component } from '@angular/core';
import { SELECTEDORDER, MarketingOrder } from '@lc/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent {

  readonly currentOrder$: Observable<MarketingOrder>;

  constructor(private store: Store<any>) {
    this.currentOrder$ = store.select(SELECTEDORDER);
  }

}
