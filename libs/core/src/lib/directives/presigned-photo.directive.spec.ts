import { PresignedPhotoDirective } from './presigned-photo.directive';
import { PresignService, PresignedPhoto } from '../services';
import { Component, ViewChild } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ListingPhoto } from '../models';
import { ListingPhotoFactory } from '../factories';
import { PresignServiceMocks } from '../mocks';
import { of } from 'rxjs';

describe('PresignedPhotoDirective', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let directive: PresignedPhotoDirective;
  let presignedService: PresignService;

  // Restore Mocks
  beforeEach(() => {
    presignedService = PresignServiceMocks.buildPresignedService();
  });

  // Configure testing module
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        PresignedPhotoDirective,
        TestHostComponent // <--- Using a test host for these tests
      ],
      providers: [
        { provide: PresignService, useValue: presignedService }
      ]
    }).compileComponents();
  });

  // Create the component and access the directive
  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    directive = component.directive;
  })

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call presignPhoto when photo changes', () => {
    expect(directive).toBeTruthy();

    component.photo = ListingPhotoFactory.photoOne;
    fixture.detectChanges();

    expect(presignedService.presignPhoto).toHaveBeenCalled();
  });
});

@Component({
  selector: 'lc-test-host',
  template: '<img [lcPresignedPhoto]="photo">'
})
class TestHostComponent {
  @ViewChild(PresignedPhotoDirective)
  public directive: PresignedPhotoDirective;

  public photo: ListingPhoto;
}
