import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AuthGuard, CoreModule, OrderListingFlowStateService } from '@lc/core';

import { ListingDashboardComponent } from './listing-dashboard/listing-dashboard.component';
import { OrderListingResolve } from './order-listing/order-listing.resolve';
import { ProductDashboardComponent } from './product-dashboard/product-dashboard.component';
import { ProductCardComponent } from './product-dashboard/product-card/product-card.component';
import { ProductCardGridComponent } from './product-dashboard/product-card-grid/product-card-grid.component';
import { ListingCardContainerComponent } from './listing-dashboard/listing-card-container/listing-card-container.component';
import { ListingCardComponent } from './listing-dashboard/listing-card/listing-card.component';
import { ListingModule } from './listing/listing.module';
import {
  InactivityLogoutComponent,
  LogoutComponent, OktaCallbackComponent,
  OktaLoginComponent,
  SharedComponentsAuthModule
} from '@lc/shared-components-auth';
import { LayoutWithoutSidenavComponent, SharedComponentsLayoutModule } from '@lc/shared-components-layout';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

import { OrderFormsModule } from '@lc/shared-components-order-forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ImageCropperModule,
    SharedComponentsAuthModule,
    ListingModule,
    RouterModule.forChild([
      {
        path: '',
        component: LayoutWithoutSidenavComponent,
        children: [
          { path: '', component: ListingDashboardComponent, canActivate: [AuthGuard] },
          { path: 'listing', loadChildren: './listing/listing.module#ListingModule', canActivate: [AuthGuard] },

          { path: 'product-dashboard', component: ProductDashboardComponent, canActivate: [AuthGuard] },
          { path: 'logout', component: LogoutComponent },
          { path: 'inactivity-logout', component: InactivityLogoutComponent },

          { path: 'okta/login', component: OktaLoginComponent },
          { path: 'implicit/callback', component: OktaCallbackComponent },

        ]
      },
      {
        path: '',
        component: LayoutWithoutSidenavComponent,
        children: [
          { path: 'my-profile', loadChildren: './my-profile/my-profile.module#MyProfileModule', canActivate: [AuthGuard] },

          { path: 'order-listing/:id', loadChildren: './order-listing/order-listing.module#OrderListingModule', canActivate: [AuthGuard] },
          { path: 'manage-order/:id', loadChildren: '@lc/shared-components-order-mgmt#OrderManagementModule', canActivate: [AuthGuard] },
        ]
      }
    ]),

    CoreModule,
    SharedComponentsLayoutModule,
    SharedComponentsUiModule,
    OrderFormsModule
  ],
  declarations: [
    ProductCardComponent,
    ProductCardGridComponent,
    ProductDashboardComponent,
    ListingDashboardComponent,
    ListingCardComponent,
    ListingCardContainerComponent,
  ],
  providers: [
    OrderListingResolve,
    OrderListingFlowStateService
  ]
})
export class LcAppUiModule {}
