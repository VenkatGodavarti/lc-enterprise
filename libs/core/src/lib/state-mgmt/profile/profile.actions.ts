import { Action } from '@ngrx/store';
import { Profile } from '../../models/profile.model';

export enum ProfileActionTypes {
  LoadProfile = '[Profile] Load User Profile',
  UpdateProfile = '[Profile] Update User Profile',
  UpdateProfileComplete = '[Profile] Update User Profile Complete',
}

/**
 * Create ease of consumption wrapper actions
 * Eg.
 *
 * new LoadLoggedInUser();
 */
export class LoadProfile implements Action {
  readonly type = ProfileActionTypes.LoadProfile;
  constructor(public payload: Profile) {}
}

export class UpdateProfile implements Action {
  readonly type = ProfileActionTypes.UpdateProfile;
  constructor(public payload: Profile,  public fields?: string[]) {}
}

export class UpdateProfileComplete implements Action {
  readonly type = ProfileActionTypes.UpdateProfileComplete;
  constructor(public payload: Profile) {}
}


export type ProfileActions = LoadProfile | UpdateProfile | UpdateProfileComplete;

export const SELECTEDPROFILE = 'selectedProfile';
