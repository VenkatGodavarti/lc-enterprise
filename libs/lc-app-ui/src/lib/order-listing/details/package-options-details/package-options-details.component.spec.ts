import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { PackageOptionsDetailsComponent } from './package-options-details.component';
import { DetailStepsComponent } from '../detail-steps/detail-steps.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { FlowState, ORDERSTATE } from '@lc/core';
import { Observable, of } from 'rxjs';
import { StoreMock, TestingModule } from '@lc/testing';

describe('PackageOptionsDetailsComponent', () => {
  let component: PackageOptionsDetailsComponent;
  let fixture: ComponentFixture<PackageOptionsDetailsComponent>;
  let activatedRoute: ActivatedRoute;

  beforeEach(async(() => {
    activatedRoute = new ActivatedRoute();
    activatedRoute.data = of({});

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        TestingModule
      ],
      declarations: [
        DetailStepsComponent,
        PackageOptionsDetailsComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageOptionsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
