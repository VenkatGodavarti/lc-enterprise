import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  PhotoOrder,
  ServiceStatusType,
  UpdatePhotoOrder,
  selectAllOrders, UIConstants
} from '@lc/core';
import { DialogAction, PromptDialogService } from '@lc/shared-components-ui';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss']
})
export class AppointmentListComponent implements OnInit {

  photographerName: string;
  photographerId: string;
  photoOrders$: Observable<PhotoOrder[]>;
  orderStatus = ServiceStatusType;

  constructor(private router: Router,
      private route: ActivatedRoute,
      private dialogService: PromptDialogService,
      private store: Store<any>) {

  }

  ngOnInit() {
    // TODO - name on the URL?
    this.photographerName =  this.route.snapshot.queryParamMap.get('photographerName');
    this.photographerId = this.route.snapshot.paramMap.get('photographerId');
    this.photoOrders$ = this.store.pipe(select(selectAllOrders));
  }

  toPhotographers() {
    this.router.navigate(['/']);
  }

  notStarted(photoOrder: PhotoOrder) {
    // if the service order is CREATING or above then started
    return photoOrder.status === this.orderStatus.PENDING || photoOrder.status === this.orderStatus.TODO;
  }


  isStarted(photoOrder: PhotoOrder) {
    return photoOrder.status === this.orderStatus.CREATING ||
      photoOrder.status === this.orderStatus.PROOFING ||
      photoOrder.status === this.orderStatus.DELIVERY;
  }

  isDone(photoOrder: PhotoOrder) {
    return photoOrder.status === this.orderStatus.DONE;
  }


  start(photoOrder: PhotoOrder) {
    const message = 'This confirms you are accepting this order and will confirm the preferred date/time with the Agent. Do you want to continue?';
    const okAction = new DialogAction(UIConstants.YES, () => {
      let aOrder = new PhotoOrder({_id: photoOrder._id, status: this.orderStatus.CREATING, photos: photoOrder.photos});
      delete aOrder.media;
      this.store.dispatch(new UpdatePhotoOrder(aOrder));
      this.router.navigate(['/manage-media', this.photographerId, photoOrder._id])
    });
    this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)])
  }

  complete(photoOrder: PhotoOrder) {
    const message = 'This confirms you are completing this order. Do you want to continue?';
    const okAction = new DialogAction('OK', () => {
      let aOrder = new PhotoOrder({_id: photoOrder._id, status: this.orderStatus.DONE});
      delete aOrder.media;
      this.store.dispatch(new UpdatePhotoOrder(aOrder));
    });
    this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)])
  }
}
