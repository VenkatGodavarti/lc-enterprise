import { $, $$, browser, by, ExpectedConditions as EC, protractor, ElementFinder, ElementArrayFinder } from 'protractor';

/**
 * Element maps a user-friendly name to a css-selector so steps can use the user-friendly name in tests to find the
 * element.
 *
 * An optional model attribute allows steps to map between modwl properties and UI elements.
 */
export class Element {

  name: string;
  model: string;
  field: string;
  isDate: boolean;

  constructor(data: any) {
    this.name = data.name;
    this.model = data.model;
    this.field = data.field;

    this.isDate = this.name.toLowerCase().indexOf('date') >= 0;
  }

  finder(index?: number) {

    const finderPattern = (index >= 0) ? this.field.replace('{index}', '' + index) : this.field;
    return $(finderPattern);
  }

  arrayFinder(): ElementArrayFinder {
    return $$(this.field);
  }

  async getValue(index?: number) {

    const finder = this.finder(index);

    await browser.wait(EC.elementToBeClickable(finder), 10000);

    return finder.getAttribute('value');
  }

  async hasValue(value: string, index?: number) {

    let fieldValue = await this.getValue(index);
    if (this.isDate) {
      fieldValue = this.parseDateValue(fieldValue);
    }

    return new Promise(resolve => {
        resolve(fieldValue === value);
      });
  }

  async hasClass(className: string) {
    return this.finder().getAttribute('class').then(function(classes) {
      return { result: classes.split(' ').indexOf(className) !== -1 };
    });
  }

  async setValue(value: string, index?: number) {

    const finder = this.finder(index);

    await browser.wait(EC.elementToBeClickable(finder), 10000);

    await this._clear(finder, this.field);

    await finder.sendKeys(value);
    if (this.isDate) {
      await finder.sendKeys(protractor.Key.TAB);
    }
  }

  async clear(index?: number) {
    return this._clear(this.finder(index), this.field);
  }

  private async _clear(finder: ElementFinder, field) {
    const type = await finder.getTagName();
    if (type === 'input') {
      // Selenium has a feature that allows for sending key chords.  We tried using
      // await finder.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
      // await finder.sendKeys(protractor.Key.BACK_SPACE);
      // to select all and hit back space once but it doesn't work and
      // Selenium marked the bug as won't fix.  So we're getting th current value and
      // hitting backspace that many times.
      const value = await this.getValue();
      const length: number = value.length;
      let i: number = 0;
      for(i; i < length; i++) {
        await finder.sendKeys(protractor.Key.BACK_SPACE);
      }
    }
    else if (type === 'select' || field.startsWith('select')) {
      await finder.all(by.tagName('option')).get(0).click();
    } else {
      await finder.clear();
    }
  }

  private parseDateValue(value: string): string {
    if (value.indexOf('-') > 0) {
      const parts = value.trim().split('-');
      if (parts.length == 3) {
        return `${parts[1]}/${parts[2]}/${parts[0]}`;
      }
    }
    return value;
  }
}

/**
 * A map of Elements, keyed by the element name for lookup during steps
 */
export class ElementMap {

  name: string;

  readonly  _elements: Element[];

  private elementsByName = {};

  constructor(name: string, elements: Element[]) {
    this.name = name;
    this._elements = elements;
    for (let i = 0; i < elements.length; i++) {
      this.elementsByName[elements[i].name] = elements[i];
    }
  }

  elements(): Element[] {
    return this._elements;
  }

  element(name: string): Element {
    return this.elementsByName[name];
  }
}
