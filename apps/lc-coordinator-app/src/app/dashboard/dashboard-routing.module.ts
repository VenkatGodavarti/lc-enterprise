import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AllOrdersComponent } from './views/all-orders/all-orders.component';
import { OrderFilterTypes } from '@lc/core';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'orders', pathMatch: 'full'},
      { path: 'orders', component: AllOrdersComponent, data: {type: OrderFilterTypes.notMine } },
      { path: 'orders/mine', component: AllOrdersComponent, data: {type: OrderFilterTypes.mine } },
      { path: 'orders/completed', component: AllOrdersComponent, data: {type: OrderFilterTypes.completed } },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
