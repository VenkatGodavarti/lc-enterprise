
import { Then, When } from 'cucumber';
import { Context, ContextKeys, LCPage, Configuration } from '../../../../../../e2e';
import { browser, by, ExpectedConditions as EC } from 'protractor';

//TODO retire this and use the one in form.steps.ts
When( /^I click the (.*) button (.*) times$/, {timeout: Configuration.timeout},async (fieldName: string, numberOfClicks: number)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const button = page.finder('page', fieldName);

  await browser.wait(EC.elementToBeClickable(button), 10000);

  for(var i = 0; i < numberOfClicks; i++) {
    await button.click();
  }
});
