import { TestBed } from '@angular/core/testing';
import { PhonePipe } from '@lc/core';

describe('PhonePipe', () => {

  let pipe: PhonePipe;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [PhonePipe]
    });

    pipe = TestBed.get(PhonePipe);
  });

  it('#transform converts a phone number with special characters to a GTM acceptable value', () => {
    const phoneNumber = '(123) 456-7890';
    const expected = '123.456.7890';

    const result = pipe.transform(phoneNumber, {});

    expect(result).toEqual(expected);
  });


});
