import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PromptDialogComponent, DialogAction } from './prompt-dialog.component';

@Injectable()
export class PromptDialogService {

  constructor(private modal: NgbModal) { }

  openPrompt(title: string, message: string, primaryAction: DialogAction | string, otherActions?: DialogAction[] | string[]): Promise<DialogAction> {
    const dialogRef = this.modal.open(PromptDialogComponent);
    const component: PromptDialogComponent = dialogRef.componentInstance;

    // Allow the actions to just be text, but convert them to dialogAction objects
    const primary = this.toAction(primaryAction);
    const others = (<any[]>otherActions).map((action: DialogAction | string) => this.toAction(action));

    // Initialize the dialog with the properties
    component.initialize(title, message, primary, others);
    component.close.subscribe(action => dialogRef.close(action));
    return dialogRef.result.then((action: DialogAction) => action, () => null); // Handle dismiss and close
  }

  private toAction(action: DialogAction | string) {
    // Convert string actions to DialogAction if necissary
    return action instanceof DialogAction ? action : new DialogAction(action);
  }
}
