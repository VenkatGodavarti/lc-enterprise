import { When } from "cucumber";
import { Configuration as config  } from '../../../../../../e2e';
import { $$ } from 'protractor';
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

When(/^I click the first available appointment date$/, {timeout: config.timeout }, async () => {
  const availableAppointments = $$(".ngb-dp-day").filter(async (element) => {
    const classString = await element.getAttribute('class');
    const classes = classString.split(' ');
    return classes.indexOf('disabled') < 0;
  })

  const appointment = await availableAppointments.get(0);
  await appointment.click();
});
