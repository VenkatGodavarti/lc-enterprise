import { of } from "rxjs";
import { PresignedPhoto, PresignService } from "@lc/core";

export class PresignServiceMocks {

  static buildPresignedService(): PresignService {
    const mockedSuffix = '/presigned'
    const service = jasmine.createSpyObj<PresignService>(['presignPhoto', 'presignUrls']);

    // Mock out presign Photo by just returning all the urls + a '/presigned' suffix
    service.presignPhoto.and.callFake(photo => {
      const presigned = new PresignedPhoto(photo);
      presigned.presignedUrl = presigned.originalUrl + mockedSuffix;
      presigned.thumbnails.forEach(thumb => thumb.presignedUrl = thumb.originalUrl + mockedSuffix);
      return of(presigned);
    });

    // Mock out the presignUrls method by just returning all requested urls + a '/presigned' suffix
    service.presignUrls.and.callFake((urls: string[]) => of(urls.map(url => url + mockedSuffix)));

    return service;
  };

}
