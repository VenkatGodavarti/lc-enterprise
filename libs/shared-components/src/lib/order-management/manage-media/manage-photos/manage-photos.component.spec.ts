import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ManagePhotosComponent } from './manage-photos.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { StoreMock, TestingModule } from '@lc/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { MarketingOrderFactory } from '../../../../../../core/src/lib/factories';

describe('ManagePhotosComponent', () => {
  let component: ManagePhotosComponent;
  let fixture: ComponentFixture<ManagePhotosComponent>;
  let marketingOrder: MarketingOrder;
  let store: StoreMock<any>;

  beforeEach(async(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);


    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        TestingModule,
      ],
      declarations: [ ManagePhotosComponent ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({ product_cod: '100' })
              }
            }
          }
        }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, marketingOrder); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
  //
  // it('#uploadResult should dispatch to store', fakeAsync(() => {
  //   component.uploadResult(null);
  //   expect(component).toBeTruthy();
  // }));
});
