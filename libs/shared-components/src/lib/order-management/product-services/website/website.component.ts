import { Component, OnInit } from '@angular/core';
import { TemplateInstance, MarketingOrder, SELECTEDORDER, ProductInstance, OutputMimeType, UpdateOrder } from '@lc/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'lc-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {

  template = new TemplateInstance({
      title: 'Website 1',
      numberOfPhotos: 5,
      thumbnailURL: '/assets/images/flyer01@3x.png',
      mimeType: OutputMimeType.html
    });

  product$: Observable<ProductInstance>;

  constructor(private store: Store<any>) {
    this.product$ = this.store.pipe(
      select<MarketingOrder>(SELECTEDORDER),
      filter(order => order != null),
      map(order => {
        const product = order.getSelectedPackage().products.find( (pr) => pr.title === 'Website')
        if(product != null) {
          product.selectedTemplate = this.template;
          this.store.dispatch(new UpdateOrder(order));
        }
        return product;
      })
    );
  }

  ngOnInit() {
  }
}
