import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentSearchComponent } from './agent-search/agent-search.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { ReactiveFormsModule } from '@angular/forms';

const components =  [AgentSearchComponent];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedComponentsUiModule
  ],
  exports: [...components]
})
export class ComponentsModule { }
