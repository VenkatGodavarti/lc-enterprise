import { Deserializable } from './deserializable.model';

import { AgentTypes } from './agent.type';
import { MarketingOrder } from './marketing-order.model';
import { PropertyStatus, PropertyType } from './property.type';
import { FlowStateData } from './flow-state.model';
import { Address } from './address.model';

export interface OrderState {
  id: string;
  state: FlowStateData;
}

export class Listing implements Deserializable<Listing> {
  _id: string;
  mlsId: string;
  propertyType: string = PropertyType.SINGLE_FAMILY;
  status:string = PropertyStatus.ACTIVE;
  listPrice:Number;
  displayAsPriceOnRequest:Boolean;
  bedrooms: Number;
  bathrooms:Number;
  sqFt:Number;
  lotSqFt:Number;
  listDate:Date;
  closedDate:Date;
  archived: Boolean;
  agentType: AgentTypes;
  useOwnPhotographer: Boolean;
  globalLuxuryListing = false;
  marketingOrder: MarketingOrder;
  address: Address;

  public static createListing(): Listing {
    const listing = new Listing();
    return listing;
  };

  /**
   * @param listing Listing to copy (optional)
   * @param photoPresign Function that handles url pre-sign of listing photos
   */
  constructor(listing?: Listing) {
    if (listing) {
      this.deserialize(listing);
    }
  }

  public clearAgentInfo() {
    this.agentType = AgentTypes.singleAgent;
  }

  public clearPropertyInfo() {
    // HACK: This should really be a child entity (i.e. Listing.PropertyInfo)
    // To Reset, we should really just be doing Listing.PropertInfo = new PropertyInfo();
    this.propertyType = PropertyType.SINGLE_FAMILY;
    this.status = PropertyStatus.ACTIVE;
    this.listPrice = null;
    this.displayAsPriceOnRequest = false;
    this.bedrooms = null;
    this.bathrooms = null;
    this.sqFt = null;
    this.lotSqFt = null;
    this.listDate = null;
    this.closedDate = null;
  }

  formatAddress(){
    const address = this.address;
    if(!address) { return null; }
    return `${address.streetAddress1}${address.unitNumber ? ' - ' + address.unitNumber : ''}, ${address.city}, ${address.state} ${address.postalCode}`;
  }

  deserialize(input: any): Listing {
    Object.assign(this, input);
    if(input.listDate) {
      this.listDate = new Date(input.listDate);
    }
    if(input.closedDate) {
      this.closedDate = new Date(input.closedDate);
    }
    return this;
  }
}
