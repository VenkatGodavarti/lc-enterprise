import { TestBed, inject } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import { TestingModule, StoreMock } from '@lc/testing';
import { Store } from '@ngrx/store';
import { NOTIFICATIONS, UpdateNotification } from '../state-mgmt';
import { NotificationFactory } from '../factories/notification.factory';
import { Notification } from '../models';

describe('NotificationService', () => {
  let store: StoreMock<any>;
  let expectedNotifications: Notification[];
  beforeEach(() => TestBed.configureTestingModule({
    imports: [TestingModule]
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    expectedNotifications = NotificationFactory.createAll();
    store.setState(NOTIFICATIONS, expectedNotifications); // set default state
  }));

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });

  it('should return notifications', async() => {
    const service: NotificationService = TestBed.get(NotificationService);
    service.get().subscribe(notifications => {
      expect(notifications).toBe(expectedNotifications);
    } );
  });

  it('markAsRead should dispatch updateNotification action ', async() => {
    const service: NotificationService = TestBed.get(NotificationService);

    let dispatchedAction: UpdateNotification;
    const dispatchMethod = spyOn(store, 'dispatch').and.callFake((action: UpdateNotification) => { dispatchedAction = action; })

    const notification = expectedNotifications[0];
    service.markAsRead(notification);

    expect(dispatchMethod).toHaveBeenCalled();
    expect(dispatchedAction.form._id).toEqual(notification._id);


  });
});
