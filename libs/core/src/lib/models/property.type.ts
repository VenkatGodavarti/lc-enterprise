// TOOD - convert to enum
export enum PropertyType {

    SINGLE_FAMILY = "Single family",
    CONDO = "Condominiums/ Townhouse",
    MULTI_FAMILY = "Multi Family/ Rental Income",
    LOTS = "Lots/Land",
    RENTAL = "Rental",
    RESIDENTIAL = "Residential",
    BUSINESSOP = "Business Opportunity",
    MOBILE_HOME = "Mobile Home",
    TIC = "TIC",
    COOP = "CO-OP",
    COMMERCIAL = "Commercial",
    VACANTLAND = "Vacant Land",
    FARMLAND = "Farm Land"
  }

  export abstract class PropertyTypes {
    public static all: string[] = [
      PropertyType.SINGLE_FAMILY,
      PropertyType.CONDO,
      PropertyType.MULTI_FAMILY,
      PropertyType.LOTS,
      PropertyType.RENTAL,
      PropertyType.RESIDENTIAL,
      PropertyType.BUSINESSOP,
      PropertyType.MOBILE_HOME,
      PropertyType.TIC,
      PropertyType.COOP,
      PropertyType.COMMERCIAL,
      PropertyType.VACANTLAND,
      PropertyType.FARMLAND,
    ]
  }

  export enum PropertyStatus {
    ACTIVE  = "Active",
    PENDING = "Pending",
    SOLD    = "Sold"
  }

  export abstract class PropertyStatuses {
    public static all: string[] = [
      PropertyStatus.ACTIVE,
      PropertyStatus.PENDING,
      PropertyStatus.SOLD,
    ]
  }
