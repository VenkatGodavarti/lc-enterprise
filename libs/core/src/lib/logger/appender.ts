
import {LogEntry} from './logentry';

/**
 * Represents a worker that appends log entries to some output
 */
export class Appender {

  constructor() {
  }

  append(entry: LogEntry) {

  }
}
