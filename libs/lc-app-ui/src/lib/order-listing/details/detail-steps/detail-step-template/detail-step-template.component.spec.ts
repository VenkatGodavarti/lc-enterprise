import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailStepTemplateComponent } from './detail-step-template.component';
import { FlowStep } from '@lc/core';

/*
 * There isn't anything in the component to test so for now we're commenting out for error purposes.
 */
describe('DetailStepTemplateComponent', () => {
  let component: DetailStepTemplateComponent;
  let fixture: ComponentFixture<DetailStepTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailStepTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailStepTemplateComponent);
    component = fixture.componentInstance;
    component.currentStep = new FlowStep();
    component.step = new FlowStep();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
