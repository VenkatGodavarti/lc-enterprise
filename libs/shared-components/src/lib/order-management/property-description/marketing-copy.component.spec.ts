import { async, ComponentFixture, fakeAsync, flush, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from "@angular/router/testing";
import { Observable, of } from "rxjs";

import { MarketingCopyComponent } from './marketing-copy.component';
import {
  FlowState,
  Listing,
  MarketingOrder,
  OrderListingFlowStateService,
  ORDERSTATE, PackageInstance,
  SELECTEDORDER, TemplateService
} from '@lc/core';
import { ActionsSubject, Store } from '@ngrx/store';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { StoreMock, TestingModule } from '@lc/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('MarketingCopyComponent', () => {
  let component: MarketingCopyComponent;
  let fixture: ComponentFixture<MarketingCopyComponent>;
  let router: Router;
  let activatedRoute: ActivatedRoute;
  let store: StoreMock<any>;
  let listing: Listing;
  let order: MarketingOrder;
  let packages: PackageInstance[] = [];
  let templateService: SpyObj<TemplateService>;
  let template: any = {};

  beforeEach(async(() => {

    listing = new Listing();
    packages = [
      new PackageInstance({
        title: 'silver',
        code: '001'}
      ),

      new PackageInstance({
        title: 'gold', code: '002',
        products: [{
          code: '001',
          marketingCopyHeadline: 'headline',
          marketingCopyBody: 'body'
        }]
      })
    ];

    order = new MarketingOrder();
    order.selectedPackageCode = packages[1].code;
    order.listing = listing;

    order.availablePackages = packages;
    order.selectedPackageCode = packages[1].code;

    activatedRoute = new ActivatedRoute();
    activatedRoute.data = of({});
    spyOnProperty(activatedRoute, 'parent', 'get').and.returnValue(activatedRoute);
    spyOnProperty(activatedRoute, 'paramMap', 'get').and.returnValue(of(convertToParamMap({})));

    templateService = jasmine.createSpyObj('TemplateService', ['getTemplateInfo']);
    templateService.getTemplateInfo.and.returnValue(of(null));

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        TestingModule
      ],
      declarations: [
        MarketingCopyComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  product_code: '001'
                })
              }
            }
          },
        },
        { provide: TemplateService, useValue: templateService },
        OrderListingFlowStateService,
        ActionsSubject
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, order); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  beforeEach(fakeAsync(() => {
    router = TestBed.get(Router);

    fixture = TestBed.createComponent(MarketingCopyComponent);
    component = fixture.componentInstance;

    component.ngOnInit();
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.marketingCopyForm).toBeTruthy();
  });

  describe('#onSubmit', () => {
    it('#onSubmit should dispatch to store', fakeAsync(() => {
      spyOn(store, 'dispatch').and.callFake(() => {
      });
      component.onSubmit(order);
      expect(store.dispatch).toHaveBeenCalled();
      fixture.detectChanges();
      flush();
    }));

  });
});
