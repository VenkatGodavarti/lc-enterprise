import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const schedulePhotographerElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Next', field: "#nextBtn" }),
  ]
);


export class SchedulePhotographerPage extends LCPage {
  constructor() {
    super('Schedule Photographer', [schedulePhotographerElements]);
  }
}
