
import { BeforeAll, AfterAll } from 'cucumber';
import { Context, ContextKeys } from '../support/context';
import { ProfileClient } from '../clients/profile.client';
import { Configuration as config } from '../configuration';

import { $, browser, ExpectedConditions as EC } from 'protractor';
import { LoginPage } from '../pages/login.page';
import { LogoutPage } from '../pages/logout.page';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

const loginPage: LoginPage = new LoginPage();
const logoutPage: LogoutPage = new LogoutPage();

/**
 * Hooks for login:
 *
 *  @login-before-all  login to the app once, before any scenarios are executed and logout after all scenarios are
 *                     completed
 */

BeforeAll({tags: "@login-before-all", timeout: config.timeout}, async () => {
  if (!Context.get<boolean>(ContextKeys.loggedIn)) {
    await login(config.username, config.password);
    const currentUser = await ProfileClient.getCurrentUser();
    Context.set(ContextKeys.user, currentUser);
    Context.set(ContextKeys.userId, currentUser._id);
  }
});

AfterAll({tags: "@login-before-all", timeout: config.timeout}, async () => {
  if (Context.get<boolean>(ContextKeys.loggedIn)) {
    await logout();
  }
});

/**
 * Visits the root app url, checks that the OKTA loginm page is rendered
 * and enters the username and password nad clicks the login button.
 *
 * The auth token used for api calls and the user's userId are extracted form the page returned after login.
 * These values are only rendered in the page for the mine and test environments.
 *
 * @param username
 * @param password
 */
export async function login(username: string, password: string) {
  await browser.waitForAngularEnabled(false);
  await browser.get(config.appURL);

  //TODO externalize these comparison strings somehow
  expect(await browser.getTitle()).to.equal('Realogy - Sign In');

  loginPage.finder('Username').sendKeys(username);
  loginPage.finder('Password').sendKeys(password);
  loginPage.finder('Sign In').click();

  console.log(config.appTitle);
  await browser.wait(EC.titleIs(config.appTitle), 30000);

  let token = await getText('#e2e-api-token');
  Context.set(ContextKeys.apiAuthToken, token);

  let userId = await getText('#e2e-api-user-id');
  Context.set(ContextKeys.apiUserId, userId);

  Context.set(ContextKeys.loggedIn, true);
}

async function getText(elementPath: string) {
  let element = $(elementPath);
  await browser.wait(EC.elementToBeClickable(element), 30000);
  return await element.getText();
}

/**
 * Logs out of the application
 */
export async function logout() {

  const url = config.appLogoutURL;

  await browser.get(url);
  await browser.wait(EC.elementToBeClickable(logoutPage.finder('Sign In')), 30000);
}
