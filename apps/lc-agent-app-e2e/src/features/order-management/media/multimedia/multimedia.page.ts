import { LCPage, ElementMap, Element } from '../../../../../../../e2e';

export class MultimediaPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Add Link', field: '#multimediaitem-add-link'}),
      new Element({ name: 'Delete Selected', field: '#delete-selected-multimedia-items'}),
      new Element({ name: 'Name', field: '#add-title-input'}),
      new Element({ name: 'Link', field: '#add-link-input'}),
      new Element({ name: 'Update Name', field: '#update-title-input'}),
      new Element({ name: 'Update Link', field: '#update-link-input'}),
      new Element({ name: 'Multimedia', field: '#multimedia-table'}),
      new Element({ name: 'Select', field: 'lc-checkbox label'}),
      new Element({ name: 'Update', field: '#multimedia-update-button'}),
      new Element({ name: 'Edit', field: '.edit-multimedia-item'}),
      new Element({ name: 'Edit Prompt', field: '#edit-dialog'}),
      new Element({ name: 'Prompt', field: '#prompt-dialog'}),
    ]
  );

  static dialogElement = new ElementMap( 'prompt dialog',
    [
      new Element( { name: 'Yes', field: '#yes-button'}),
      new Element( { name: 'No', field: '#no-button'}),
    ]
  );

  static editMultiMediaDialogElements = new ElementMap('edit dialog',
    [
      new Element( { name: 'Update', field: '#multimedia-update-button'}),
      new Element( { name: 'Cancel', field: '#multimedia-cancel-button'}),
    ]
  );

  constructor() {
    super('Multimedia', [MultimediaPage.elements, MultimediaPage.dialogElement, MultimediaPage.editMultiMediaDialogElements]);
  }

}
