export const environment = {
  production: false,
  envName: 'qa',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'https://lc.qa.lc-coordinator.realogy.com/',
  apiBaseURL: 'https://lc.qa.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.qa.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.qa.lc-notify.realogy.com/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzuiccmdJSOQ5q0h7',
  oktaRedirectUri: 'https://lc.qa.lc-coordinator.realogy.com/implicit/callback',
};
