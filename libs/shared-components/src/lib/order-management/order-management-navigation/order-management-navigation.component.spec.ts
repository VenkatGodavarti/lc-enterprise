import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderManagementNavigationComponent } from './order-management-navigation.component';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterTestingModule } from '@angular/router/testing';

describe('OrderManagementNavigationComponent', () => {
  let component: OrderManagementNavigationComponent;
  let fixture: ComponentFixture<OrderManagementNavigationComponent>;
  let store: SpyObj<Store<any>>;

  beforeEach(async(() => {

    store = jasmine.createSpyObj('Store', ['pipe', 'select']);

    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        RouterTestingModule,
      ],
      declarations: [ OrderManagementNavigationComponent ],
      providers: [
        { provide: Store, useValue: store }
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderManagementNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
