describe('Create Order', function() {

  const apiBase = Cypress.config("persist_apiBase");

  const validAddress = {
    streetAddress1: "1060 W Addison St",
    streetAddress2: null,
    unitNumber: "Left Field",
    city: "Chicago",
    state: "IL",
    postalCode: "60613",
  };

  const mlsId = "37W00D";

  it('should accept a valid address', function() {

    cy.server();
    cy.route('OPTIONS', `${apiBase}/marketing-orders`);
    cy.route('POST', `${apiBase}/marketing-orders`, {}).as('submitPost');
    cy.route('POST', `${apiBase}/package-instances/my-packages`, {});

    cy.visit('/listing/add-new-listing');
    cy.get('button#nextBtn').as('submitButton');

    cy.get('input#listing-address-street').type(validAddress.streetAddress1);
    cy.get('input#listing-address-unit-number').type(validAddress.unitNumber);
    cy.get('input#listing-address-city').type(validAddress.city);
    cy.get('select#listing-address-state').select(validAddress.state);
    cy.get('input#listing-address-postal-code').type(validAddress.postalCode);
    cy.get('input#listing-address-mls-id').type(mlsId);

    cy.get('@submitButton').should('not.be.disabled');

    cy.get('@submitButton').click();

    cy.wait('@submitPost').then((xhr) => {
      const request = xhr.request.body;
      expect(request.listing).to.be.ok;
      expect(request.listing.address).to.be.ok;
      expect(request.listing.address).to.eql(validAddress);
      expect(request.listing.mlsId).to.equal(mlsId);
    });
  });

});

