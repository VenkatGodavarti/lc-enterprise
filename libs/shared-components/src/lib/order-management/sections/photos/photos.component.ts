import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MarketingOrder, ServiceStatusType } from '@lc/core';

@Component({
  selector: 'lc-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit, OnChanges {

  @Input() marketingOrder: MarketingOrder;

  photoApptComplete: boolean;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  init() {
    if(!this.marketingOrder) {
      return;
    }

    // We may not need to check for selectedPackage but it's doing it for safety
    const selectedPackage = this.marketingOrder.getSelectedPackage();
    const photoProduct = selectedPackage ? selectedPackage.getPhotographyProduct() : null;
    this.photoApptComplete = photoProduct && photoProduct.status === ServiceStatusType.DONE;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.marketingOrder) {
      this.init();
    }
  }

}
