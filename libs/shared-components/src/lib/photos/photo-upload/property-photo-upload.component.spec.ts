import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { PropertyPhotoUploadComponent } from './property-photo-upload.component';
import { FileDropModule } from 'ngx-file-drop';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ViewChild } from "@angular/core";
import { Listing, ListingPhoto, PhotoService } from '@lc/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

describe('PhotoUploadComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let photoServiceSpy;
  let modalServiceSpy;

  beforeEach(fakeAsync(() => {
    photoServiceSpy = jasmine.createSpyObj('PhotoService', ['upload']);
    modalServiceSpy = jasmine.createSpyObj('NgbModal', ['open']);
    TestBed.configureTestingModule({
      imports: [
        FileDropModule
      ],
      declarations: [TestHostComponent, PropertyPhotoUploadComponent ],
      providers: [
        { provide: PhotoService, useValue: photoServiceSpy},
        { provide: NgbModal, useValue: modalServiceSpy},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
    tick(1000);
  }));
});

@Component({
  selector: 'lc-test-host',
  template: '<lc-property-photo-upload></lc-property-photo-upload>'
})
class TestHostComponent {
  @ViewChild(PropertyPhotoUploadComponent)
  public photoUpload: PropertyPhotoUploadComponent;
}

