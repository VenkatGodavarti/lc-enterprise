import { Component, OnDestroy, OnInit } from '@angular/core';

import {
  ActionTypes,
  UserService,
  WorkflowAction,
  InitializeOrder,
  MarketingOrderService,
  MarketingOrder,
  RemoveKeys,
  HeaderConstants,
  DeleteOrder,
  OrderActionTypes
} from '@lc/core';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Subscription } from 'rxjs';
@Component({
  selector: 'lc-listing-dashboard',
  templateUrl: './listing-dashboard.component.html',
  styleUrls: ['./listing-dashboard.component.scss']
})
export class ListingDashboardComponent implements OnInit, OnDestroy {

  deleteOrderSubscription: Subscription;
  username = '';
  orders: MarketingOrder[] = [];

  constructor(private marketingOrderService: MarketingOrderService,
              private userService: UserService,
              private actions: Actions,
              private store: Store<any>) {

  }

  ngOnInit() {
    this.username = this.userService.getUserFirstName();

    // clear the workflow state for orders
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionTerminate));
    // clear any listing or orders since none are selected
    this.store.dispatch(new InitializeOrder());
    // clear correlation id
    this.store.dispatch(new RemoveKeys([HeaderConstants.CORRELATION_ID]));

    this.retrieveOrders();

    // subscribe to events when marketing orders are deleted so remaining orders can be retrieved
    this.deleteOrderSubscription = this.actions.pipe(ofType<DeleteOrder>(OrderActionTypes.DeleteOrderComplete))
      .subscribe(() => this.retrieveOrders());
  }

  ngOnDestroy(): void {
    if(this.deleteOrderSubscription) {
      this.deleteOrderSubscription.unsubscribe();
    }
  }

  retrieveOrders() {
    // TODO - this also can be in the order state
    this.marketingOrderService.getAllMyOrders().subscribe(
      orders => {
        this.orders = orders;
      });
  }

}
