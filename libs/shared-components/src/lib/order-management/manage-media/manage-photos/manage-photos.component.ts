import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActionsSubject, select, Store } from '@ngrx/store';
import {
  Listing, ListingPhoto,
  MarketingOrder, OrderActionTypes,
  SELECTEDORDER, UpdateOrderPhoto,
  UpdateOrderPhotos, UpdateOrderPhotosComplete, UploadOrderPhotos, UploadPhoto, LoadOrderPhotos
} from '@lc/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ofType } from '@ngrx/effects';
import { map, filter, tap, merge } from 'rxjs/operators';

@Component({
  selector: 'lc-product-photos',
  templateUrl: './manage-photos.component.html',
  styleUrls: ['./manage-photos.component.scss']
})
export class ManagePhotosComponent implements OnInit, OnDestroy {
  marketingOrder: MarketingOrder;
  productCode: string;
  photos$: Observable<ListingPhoto[]>;

  constructor(private activatedRoute: ActivatedRoute,
              private actionSubject: ActionsSubject,
              private store: Store<any>) {
    this.productCode = this.activatedRoute.parent.snapshot.paramMap.get('product_code');

    // This is the observable to any photo updates. Is not being subscribed to here, but is being merged
    // in with the photo subscription below
    const updatePhotosComplete$ = this.actionSubject.pipe(
      ofType<UpdateOrderPhotosComplete>(OrderActionTypes.UpdateOrderPhotosComplete),
      map(action => action.payload),
    );

    // Subscribe to either the selected order change or the updatePhotosComplete$ changes
    // and update photos accordingly
    this.photos$ = this.store.select<MarketingOrder>(SELECTEDORDER).pipe(
      merge(updatePhotosComplete$),
      filter(order => order != null),
      tap(order => this.marketingOrder = order),
      map(order => order.getPhotosFor(this.productCode))
    )
  }

  ngOnInit() {
    this.store.dispatch(new LoadOrderPhotos(this.productCode));
  }

  ngOnDestroy(): void {
  }

  /**
   * Update the order with new photos
   * @param photos
   */
  uploadResult(photos: UploadPhoto[]) {
    this.store.dispatch(new UploadOrderPhotos(this.marketingOrder, photos, null, this.productCode));
  }

  /**
   * Event to update photos
   * @param photos array of listing photos from grid
   */
  updatePhotos(photos: ListingPhoto[]) {
    this.store.dispatch(new UpdateOrderPhotos(this.marketingOrder, photos, null, this.productCode));
  }

  /**
   * Update one photo attributes
   * @param photo
   */
  updatePhoto(photo: ListingPhoto) {
    this.store.dispatch(new UpdateOrderPhoto(this.marketingOrder, photo, this.productCode));
  }

  refreshPhotos() {
    // return full list of photos so we will update the photos
    const productPhotos = this.marketingOrder.copyPhotos(this.productCode);
    if (productPhotos && productPhotos.length > 0) {
      this.store.dispatch(new UpdateOrderPhotos(this.marketingOrder, productPhotos, null, this.productCode));
    }
  }
}
