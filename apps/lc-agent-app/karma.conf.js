// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const { join } = require('path');
const getBaseKarmaConfig = require('../../karma.conf');

module.exports = function(config) {
  const baseConfig = getBaseKarmaConfig();
  config.set({
    ...baseConfig,
    coverageReporter: {
      ...baseConfig.coverageReporter,
      dir: join(__dirname, '../../reports/coverage/apps/lc-agent-app')
    },
    coverageIstanbulReporter: {
      ...baseConfig.coverageIstanbulReporter,
      dir: join(__dirname, '../../reports/coverage/apps/lc-agent-app')
    },
    remapCoverageReporter: {
      ...baseConfig.remapCoverageReporter,
      html: join(__dirname, '../../reports/coverage/apps/lc-agent-app/html'),
      cobertura: join(__dirname, '../../reports/coverage/apps/lc-agent-app/cobertura.xml')
    },
    junitReporter: {
      ...baseConfig.junitReporter,
      outputDir: join(__dirname, '../../reports/unit-tests/apps/lc-agent-app'),
    }
  });
};
