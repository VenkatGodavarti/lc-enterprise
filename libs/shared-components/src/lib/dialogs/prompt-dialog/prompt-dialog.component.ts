import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

export class DialogAction {
  constructor(public text: string, public onClick?: () => any) {
  }
}

@Component({
  selector: 'lc-prompt-dialog',
  templateUrl: './prompt-dialog.component.html',
  styleUrls: ['./prompt-dialog.component.scss']
})
export class PromptDialogComponent implements OnInit {
  public title: string;
  public message: string;
  public primaryAction: DialogAction;
  public secondaryActions: DialogAction[];

  public readonly close = new Subject<DialogAction>();

  constructor() { }

  ngOnInit() {
  }

  initialize(title: string, message: string, primaryAction: DialogAction, secondaryActions: DialogAction[]) {
    this.title = title;
    this.message = message;
    this.primaryAction = primaryAction;
    this.secondaryActions = secondaryActions || [];
  }

  onActionClicked(action: DialogAction) {
    if(action.onClick) {
      action.onClick();
    }
    this.close.next(action);
  }
}
