import { LCPage, Element, ElementMap } from '../../../../e2e';

export class SelectPhotographerPage extends LCPage {
  static elements = new ElementMap( 'page',  [
      new Element({ name: 'Header', field: "#select-photographer-header"}),
      new Element({ name: 'Cutting Edge Photographer', field: "#card-option-0"}),
      new Element({ name: 'Fabulous Photos', field: "#card-option-1"}),
      new Element({ name: 'Quick Photos', field: "#card-option-2"}),
      new Element({ name: 'Next', field: "#next-button"}),
    ]
  );

  constructor() {
    super('Select Photographer', [SelectPhotographerPage.elements]);
  }
}
