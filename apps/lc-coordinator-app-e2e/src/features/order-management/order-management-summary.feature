@order-management-summary
@order-management
@login-before-all
Feature: Order Management Summary

  Background:
    Given I am logged in to Listing Concierge
    Given the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Street               | 123 Main St           |
      | Unit Number          | 1                     |
      | City                 | Madison               |
      | State                | NJ                    |
      | Zip Code             | 07040                 |
      | Property Information | default               |
      | Photos               | none                  |
      | Status               | PENDING               |

  Scenario: Display order Summary
    When I select the Alamo order
    And I go to the Summary page in Order Management
    Then the "Listing" tile exists
    And the "Property Description" tile exists
    And the "Notifications" tile exists
    And the "Products & Services" tile exists
    And the "Photos" tile exists

  Scenario: Verify Listing Details Tile
    When I select the Alamo order
    And I go to the Summary page in Order Management
    Then the "Listing" tile exists
#    And the "Default Listing Photo" element exists
    And the "Street Address" text is "123 Main St - 1"
    And the "City, State, Zip" text is "Madison, NJ 07040"
    And the "Listing Price" text is "Price Not Set"
    When I click on the "Manage Listing Details" button
    Then the Listing Details page from Order Management is displayed
    And the "Page Title" text is "Listing Details"

  Scenario: Edit Listing Details
    When I select the Alamo order
    And I go to the Listing Details page in Order Management
