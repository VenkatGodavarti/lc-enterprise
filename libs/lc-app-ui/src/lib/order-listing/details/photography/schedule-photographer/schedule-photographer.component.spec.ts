import { ComponentFixture, fakeAsync, TestBed, tick, inject } from '@angular/core/testing';
import { SchedulePhotographerComponent } from './schedule-photographer.component';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { DetailStepsComponent } from '../../detail-steps/detail-steps.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  FlowState,
  Listing,
  PhotographerAppointment,
  ORDERSTATE,
  MarketingOrder,
  SELECTEDORDER
} from '@lc/core';
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { OrderStepTemplateComponent } from '../../../steps/order-steps/order-step-template/order-step-template.component';
import { OrderStepsComponent } from '../../../steps/order-steps/order-steps.component';
import { DetailStepTemplateComponent } from '../../detail-steps/detail-step-template/detail-step-template.component';
import { StoreMock, TestingModule } from '@lc/testing';
import { PackageInstanceFactory} from '../../../../../../../core/src/lib/factories';
import { StepContentComponent } from '../../step-content/step-content.component';
import { OrderSummaryComponent } from '../../order-summary/order-summary.component';

describe('SchedulePhotographerComponent', () => {
  let component: SchedulePhotographerComponent;
  let fixture: ComponentFixture<SchedulePhotographerComponent>;
  let order: MarketingOrder;
  let store: StoreMock<any>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule,
        FormsModule,
        TestingModule
      ],
      declarations: [
        SchedulePhotographerComponent,
        OrderStepsComponent,
        OrderStepTemplateComponent,
        DetailStepsComponent,
        DetailStepTemplateComponent,
        StepContentComponent,
        OrderSummaryComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
      ],
    })
    .compileComponents();
  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    order = createOrder(false);
    store.setState(SELECTEDORDER, order); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  // Create the component
  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(SchedulePhotographerComponent);
    component = fixture.componentInstance;

    tick();
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Next button will not be enabled until a date has been chosen', () => {
    // Submit initially disabled
    const submitButton = fixture.debugElement.query(By.css('#nextBtn')).nativeElement;
    expect(submitButton.disabled).toBeTruthy();

    // Select a date
    component.onDateSelect(new NgbDate(2019, 1, 1));
    fixture.detectChanges();

    // Submit is now enabled
    expect(submitButton.disabled).toBeFalsy();
  });

  it('#onSubmit will convert and save the correct date format', async() => {
    // Select a date
    component.onDateSelect(new NgbDate(2019, 2, 1)); // using ISO 8601 date format
    component.selectedTime = {hour: 13, minute: 45};
    fixture.detectChanges();

    component.marketingOrder$.subscribe(order => {
      component.onSubmit(order);

      expect(order.getSelectedPackage().getPhotographyProduct().serviceAppointment.scheduledAt.getMonth()).toBe(1);
    });

  });

  function createOrder(hasAppt: boolean) {
    const newListing = new Listing();
    newListing._id = '123456789ABCDEF';
    newListing.listPrice = 400000;
    newListing.sqFt = 4000;
    newListing.lotSqFt = 14000;
    newListing.listDate = new Date();

    const newAppt = new PhotographerAppointment();


    const newOrder : MarketingOrder = new MarketingOrder();
    newOrder.listing = newListing;
    newOrder.availablePackages = PackageInstanceFactory.createAll();
    newOrder.selectedPackageCode = newOrder.availablePackages[1].code;
    newOrder.availablePackages[1].getPhotographyProduct().serviceAppointment = (hasAppt) ? newAppt : null;
    return newOrder;
  }
});
