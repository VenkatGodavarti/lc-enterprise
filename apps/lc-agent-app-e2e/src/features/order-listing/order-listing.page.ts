import { LCPage,Element, ElementMap } from '../../../../../e2e';
import { SelectMarketingPackagePage } from './select-marketing-package/select-marketing-package.page';
import { OrderListingDetailsPage } from './details/order-listing-details.page';
import { OrderListingPropertyPage } from './property/property.page';
import { OrderListingAgentPage } from './agent/agent.page';
import { ShippingPage } from './shipping/shipping.page';
import { BillingPage } from './billing/billing.page';
import { SelectPhotographerPage } from './photographer/select-photographer.page';
import { PhotographyUpgradesPage } from './photographer/photography-upgrades.page';
import { SchedulePhotographerPage } from './photographer/schedule-photographer.page';
import { PhotographerCommentsPage } from './photographer/photographer-comments.page';
import { BrochureOptionsPage } from './brochure/brochure-options.page';

const orderListingElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Address', field: "#address-input" }),
    new Element({ name: 'Unit Number', field: "#unit-number-input" }),
    new Element({ name: 'City', field: "#city-input" }),
    new Element({ name: 'State', field: "#state-input" }),
    new Element({ name: 'Zip Code', field: "#zip-input" }),
    new Element({ name: 'MLS ID', field: "#mls-id-input" }),
    new Element({ name: 'GlobalLuxuryListing', field: "input[formcontrolname='globalLuxuryListing']" })
  ]
);

export const orderListingPages = {
  'order listing': {
    'select marketing package': {
      url: '/order-listing/:orderId/select-marketing-package',
      page: new SelectMarketingPackagePage()
    },
    'order listing details': {
      url: '/order-listing/:orderId/:packageId/details',
      page: new OrderListingDetailsPage()
    },
    'order listing property': {
      url: '/order-listing/:orderId/:packageId/property',
      page: new OrderListingPropertyPage()
    },
    'overview photography': {
      url: '/order-listing/:orderId/:packageId/photography',
      page: new OrderListingDetailsPage()
    },
    'overview checkout': {
      url: '/order-listing/:orderId/:packageId/checkout',
      page: new OrderListingDetailsPage()
    },
    'overview package options': {
      url: '/order-listing/:orderId/:packageId/package-options',
      page: new OrderListingDetailsPage()
    },
    'select photographer': {
      url: '/order-listing/:orderId/:packageId/select-photographer',
      page: new SelectPhotographerPage()
    },
    'select photography upgrades': {
      url: '/order-listing/:orderId/:packageId/photography-upgrades',
      page: new PhotographyUpgradesPage()
    },
    'schedule photographer': {
      url: '/order-listing/:orderId/:packageId/schedule-photographer',
      page: new SchedulePhotographerPage()
    },
    'photographer comments': {
      url: '/order-listing/:orderId/:packageId/photographer-comments',
      page: new PhotographerCommentsPage()
    },
    'order listing agent': {
      url: '/order-listing/:orderId/:packageId/agent',
      page: new OrderListingAgentPage()
    },
    'shipping': {
      url: '/order-listing/:orderId/:packageId/shipping',
      page: new ShippingPage()
    },
    'billing': {
      url: '/order-listing/:orderId/:packageId/billing',
      page: new BillingPage()
    },
    'brochure options': {
      url: '/order-listing/:orderId/:packageId/brochure-options',
      page: new BrochureOptionsPage()

    }
  }
};


export class OrderListingPage extends LCPage {
  constructor() {
    super('Order Listing', [orderListingElements]);
  }
}
