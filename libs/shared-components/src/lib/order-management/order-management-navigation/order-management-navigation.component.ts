import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'lc-order-management-navigation',
  templateUrl: './order-management-navigation.component.html',
  styleUrls: ['./order-management-navigation.component.scss']
})
export class OrderManagementNavigationComponent implements OnInit {
  marketingOrder$: Observable<MarketingOrder>;
  public isExpanded = true;

  private DEFAULT_NAV_ROUTE: string = '/';
  private DEFAULT_NAV_LABEL: string = 'Go To My Listings';

  myListNav: string;
  myListLabel: string;

  readonly tabs = [
    {isSelected: true, label: 'ORDER MANAGEMENT', path: 'summary', svgPath: 'assets/images/ordermanagementicon.svg'},
    {isSelected: false, label: 'LISTING DETAILS', path: 'listing-details', svgPath: 'assets/images/listingdetailsicon.svg'},
    {isSelected: false, label: 'PROPERTY DESCRIPTION', path: 'property-description', svgPath: 'assets/images/productsicon.svg'},
    {isSelected: false, label: 'PRODUCTS & SERVICES', path: 'products-services', svgPath: 'assets/images/propdescriptionicon.svg'},
    {isSelected: false, label: 'PHOTOS & MULTIMEDIA', path: 'media', svgPath: 'assets/images/photosicon.svg'},
  ];

  constructor(private store: Store<any>, private activatedRoute: ActivatedRoute, private router: Router) {
    this.myListNav = this.activatedRoute.snapshot.data['myListNav'] || this.DEFAULT_NAV_ROUTE;
    this.myListLabel = this.activatedRoute.snapshot.data['myListLabel'] || this.DEFAULT_NAV_LABEL;

    this.marketingOrder$ = this.store.select(SELECTEDORDER);
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        const path = this.activatedRoute.snapshot.firstChild.url[0].path;
        this.tabs.forEach(tab => tab.isSelected = tab.path === path);
      });

  }

  ngOnInit() {
  }
}
