export enum AgentTypes {
  singleAgent = 'Single Agent',
  twoAgents = 'Co-Listing',
  team = 'Team',
}
