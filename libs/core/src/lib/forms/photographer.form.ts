import { FormControl, Validators  } from '@angular/forms';
import { BaseForm } from './base.form';
import { Photographer } from '../models/photographer.model';


export class PhotographerForm extends BaseForm {
  constructor(value?: Photographer){
    super({
      _id: new FormControl(),
      name: new FormControl(null, Validators.required),
      logoUri: new FormControl(null, Validators.required),
    });

    if(value) {
      this.patchValue(value);
    }
  }
}
