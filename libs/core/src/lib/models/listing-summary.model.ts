import { Deserializable } from './deserializable.model';

export class ListingSummary implements Deserializable<ListingSummary> {
  id: string;
  address: string;
  unitNumber: string;
  city: string;
  state: string;
  postalCode: string;
  bedrooms: number;
  bathrooms: number;

  deserialize(input: any): ListingSummary {
    Object.assign(this, input);

    return this;
  }
}
