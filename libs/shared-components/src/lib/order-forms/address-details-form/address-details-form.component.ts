import { Component, OnInit, Input } from '@angular/core';
import { AddressForm } from '@lc/core';

@Component({
  selector: 'lc-address-details-form',
  templateUrl: './address-details-form.component.html',
  styleUrls: ['./address-details-form.component.scss']
})
export class AddressDetailsFormComponent implements OnInit {

  @Input()
  formGroup: AddressForm;

  @Input()
  submitted = false;

  constructor() { }

  ngOnInit() {
  }

}
