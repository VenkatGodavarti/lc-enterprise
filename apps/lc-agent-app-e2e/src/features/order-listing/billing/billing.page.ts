import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const billingElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Same As Shipping Address', field: "#populateShipping"}),
    new Element({ name: 'Full Name', field: "#full-name-input" }),
    new Element({ name: 'Address 1', field: "#street-address-1-input" }),
    new Element({ name: 'Address 2', field: "#street-address-2-input" }),
    new Element({ name: 'City', field: "#city-input" }),
    new Element({ name: 'State', field: "#state-input" }),
    new Element({ name: 'Zip Code', field: "#zip-input" }),
    new Element({ name: 'Company', field: "#company-input" }),
    new Element({ name: 'Phone', field: "#phone-input" }),
    new Element({ name: 'Email', field: "#email-input" }),
    new Element({ name: 'Back', field: "#backBtn" }),
    new Element({ name: 'Place Order', field: "#nextBtn" }),
  ]
);


export class BillingPage extends LCPage {
  constructor() {
    super('Billing', [billingElements]);
  }
}
