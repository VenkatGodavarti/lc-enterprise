import { User, UserService } from "@lc/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Provider } from "@angular/core";

export class UserServiceMock {
  public static mockedUser: User = {
    firstName: 'Unit',
    lastName: 'Test'
  } as User;

  private currentUserSubject = new BehaviorSubject<User>(UserServiceMock.mockedUser);
  public currentUser: Observable<User> = this.currentUserSubject.asObservable();

  getCurrentUser(): User {
    return UserServiceMock.mockedUser;
  }

  public getUserId(): string {
    return UserServiceMock.mockedUser._id;
  }
  public getUserFirstName(): string {
    return UserServiceMock.mockedUser.firstName;
  }
  public setCurrentUser(user: User, loadedFromStorage?: boolean): void {
    UserServiceMock.mockedUser = user;
    this.currentUserSubject.next(user);
  }
  clearCurrentUser(): void {
    UserServiceMock.mockedUser = null;
    this.currentUserSubject.next(null);
  }

  setupDataLayer(): boolean {
    throw new Error('Method not implemented.');
  }

  updateUserProfile(): Observable<boolean> {
    throw new Error('Method not implemented.');
  }

  saveToLocalStorage(): void {
    throw new Error('Method not implemented.');
  }

  loadFromLocalStorage(): void {
    throw new Error('Method not implemented.');
  }
}

export const mockedUserServiceProvider: Provider = { provide: UserService, useClass: UserServiceMock }
