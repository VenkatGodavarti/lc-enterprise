
export enum AddressType {

  BILLING = 'billing',
  SHIPPING = 'shipping',
  MAILING = 'mailing'
}
