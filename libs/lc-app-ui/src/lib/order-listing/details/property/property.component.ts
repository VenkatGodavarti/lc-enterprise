import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import {
  ActionTypes,
  Listing,
  PropertyStatus,
  PropertyType,
  WorkflowAction,
  PropertyInfoForm,
  SELECTEDORDER,
  MarketingOrder,
  UpdateOrderListing,
  OrderActionTypes,
  UpdateOrderListingComplete
} from '@lc/core';
import { ActionsSubject, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lc-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class PropertyComponent implements OnDestroy {
  readonly currentListing$: Observable<Listing>;
  marketingOrder: MarketingOrder;
  infoForm: PropertyInfoForm;

  params: any;

  subjectSub: Subscription;

  constructor(public activatedRoute: ActivatedRoute,
              private actionSubject: ActionsSubject,
              public store: Store<any>) {
    this.currentListing$ = store.select(SELECTEDORDER).pipe(
      filter(order => order && order.listing),
      tap(order => this.marketingOrder = order),
      map(order => order.listing),
      tap(listing => listing.propertyType = listing.propertyType || PropertyType.SINGLE_FAMILY),
      tap(listing => listing.status = listing.status || PropertyStatus.ACTIVE),
      tap(listing => this.infoForm = new PropertyInfoForm(listing))
    );
    this.params = this.activatedRoute.parent.snapshot.paramMap;

    // only move to next step if successfully saved
    this.subjectSub = this.actionSubject.subscribe((data: UpdateOrderListingComplete) => {
      if (data.type === OrderActionTypes.UpdateOrderListingComplete) {
        this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true }, this.params));
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subjectSub) {
      this.subjectSub.unsubscribe();
    }
  }

  goBack() {
    this.store.dispatch(
      new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(listing: Listing) {
    if (!this.infoForm.valid) {
      return;
    }
    Object.assign(listing, this.infoForm.value);
    this.store.dispatch(new UpdateOrderListing(this.marketingOrder));
  }
}
