import { Component } from '@angular/core';

@Component({
  selector: 'lc-photo-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'lc-photo-app';
}
