@notes
@login-before-all
Feature: I want to verify the listing dashboard page

Background:
  Given I am logged in to Listing Concierge
  Given the following orders exist
  | Name                 | Alamo                 | White House           |
  | Package              | Gold                  | Platinum              |
  | Status               | PENDING               | PENDING               |

  # Navigate to the All Orders tab
  When I go to the Main page in Dashboard
  And I click on the All Orders Tab button
  Then the All Orders page from Dashboard is loaded

  When the "Orders" table records load
  Then the "Orders" table has the following data
  | Address                                         | Package  | Coordinator | Copy  | Photos |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum |             | NO    | NO     |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold     |             | Ready | 2      |


# Note: This test assumes there are not already any note added
Scenario: Open notes
    # Sort Package name ascending
  When I click on the "View Notes" button in row "2" of the "Orders" table

  Then the "Notes" dialog is displayed
  And the "Notes Title" text is "Add Note"
  And the "Notes Subtitle" text is "300 Alamo Plaza, San Antonio, TX 78205"
  And there are 0 "Note" elements
  # And the "note section" text is empty
  # And the "This order does not have a note yet" text is "visible"
  When I click on the "Cancel" button inside the "Notes Dialog"
  Then the "Note" dialog is not displayed

Scenario: Add notes
  # Open the Dialog and verify no notes exist
  When I click on the "View Notes" button in row "2" of the "Orders" table
  Then the "Notes" dialog is displayed
  And there are 0 "Note" elements

  # Add a new Note
  When I enter "The customer called and asked to hurry up!" into the "Add a Note"
  When I click on the "Save" button inside the "Notes Dialog"
  Then the "Note" dialog is not displayed

  # Open the Dialog and verify the new note exists
  When I click on the "View Notes" button in row "2" of the "Orders" table
  Then the "Notes" dialog is displayed
  And there are 1 "Note" elements

  Then the "Note Text" text on element 1 is "The customer called and asked to hurry up!"
  # Then the "Note Author" text on element 1 is "The customer called and asked to hurry up!"

Scenario: Cancel or close note
  # Open the Dialog and verify no notes exist
  When I click on the "View Notes" button in row "2" of the "Orders" table
  Then the "Notes" dialog is displayed
  And there are 0 "Note" elements

  # Add a new Note
  When I enter "The customer called and asked to hurry up!" into the "Add a Note"
  When I click on the "Cancel" button inside the "Notes Dialog"
  Then the "Note" dialog is not displayed

  # Open the Dialog and verify no notes still exist
  When I click on the "View Notes" button in row "2" of the "Orders" table
  Then the "Notes" dialog is displayed
  And there are 0 "Note" elements
