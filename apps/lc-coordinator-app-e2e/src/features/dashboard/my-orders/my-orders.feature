@login-before-all
@coordinator
@dashboard
@my-orders

# Note: This feature assumes there are no marketing orders in the database.

Feature: Listing Dashboard

  Background:
    Given I am logged in to Listing Concierge
    And the following users exist
      | Name          | Alice               | John             |
      | First Name    | Alicia              | Jonathon         |
      | Last Name     | Burgundy            | Maximus          |
      | Email         | aliciab@realogy.com | jonm@realogy.com |
      | Office        | Downtown Fresno     | Whittier         |
      | Primary Phone | 3135556666          | 9495551234       |
    And the following orders exist
      | Name                 | Alamo                 | White House           |
      | Package              | Gold                  | Platinum              |
      | Status               | PENDING               | PENDING               |
      | Coordinator          | Me                    | Me                    |
      | Agent                | Alice                 | John                  |


  # Navigate to the All Orders tab
  When I go to the Main page in Dashboard
  And I click on the My Orders Tab button
  Then the My Orders page from Dashboard is loaded
  When the "Orders" table records load
  Then the "Orders" table has the columns "Address, Agent, Date, Package"

# Note: This test assumes there are not already any marketing orders
  Scenario: My Orders

    Then the "Orders" table has the following data
      | Address                                         | Package         | Agent             |
      | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum        | Jonathon\nMaximus |
      | 300 Alamo Plaza\nSan Antonio, TX                | Gold            | Alicia\nBurgundy  |

# Note: This test assumes there are not already any marketing orders
  Scenario: My Orders Sort by Package ascending

    When I click on the Package Header button
    And the "Orders" table records load
    Then the "Orders" table has the following data
      | Address                                         | Package          | Agent             |
      | 300 Alamo Plaza\nSan Antonio, TX                | Gold             | Alicia\nBurgundy  |
      | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum         | Jonathon\nMaximus |

# Note: This test assumes there are not already any marketing orders
  Scenario: My Orders sort by Package descending

    When I click on the Package Header button
    Then I click on the Package Header button
    When the "Orders" table records load
    Then the "Orders" table has the following data
      | Address                                         | Package         | Agent             |
      | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum        | Jonathon\nMaximus |
      | 300 Alamo Plaza\nSan Antonio, TX                | Gold            | Alicia\nBurgundy  |


  @agent-hover
  Scenario: Hovering over Agent Avatar shows Agent Information

    When I hover over the Agent Avatar on row number 1
    Then the "Agent Phone" text is "949.555.1234"
    And the "Agent Email" text is "jonm@realogy.com"
    And the "Agent Office" text is "Whittier"

    When I hover over the Agent Avatar on row number 2
    Then the "Agent Phone" text is "313.555.6666"
    And the "Agent Email" text is "aliciab@realogy.com"
    And the "Agent Office" text is "Downtown Fresno"
