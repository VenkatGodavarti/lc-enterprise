import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { MarketingOrder, OutputMimeType, ProductInstance } from '../models';
import { AuthenticationService } from './authentication.service';
import { WindowService } from './window.service';
import { Observable, of } from "rxjs";
import 'rxjs/add/observable/forkJoin';
import { format } from 'util';
import { HttpClient } from '@angular/common/http';
import { ContentGenerator } from './content-generator';
import * as snakeCase from 'lodash.snakecase';

enum PrintQuality {
  proof = 'proof',
  print = 'print'
}

class PdfRenderRequest {

  orderId: string;
  agentId: string;
  productCode: string;
  templateId: string;
  quality: PrintQuality;
  fileName: string
}

@Injectable()
export class PdfGenerationService implements  ContentGenerator{

  public  mimeType = OutputMimeType.pdf;

  downloadable = true;

  constructor(private http: HttpClient,
              private authenticationService: AuthenticationService,
              private windowService: WindowService) {
  }

  download(order: MarketingOrder, product: ProductInstance): Observable<any> {
    this.windowService.open(environment.apiBaseURL+this.pdfUrl(order, product, true), '_blank');
    return of(null);
  }

  generate(order: MarketingOrder, product: ProductInstance): Observable<any> {

    return this.post(this.buildPdfRequest(order, product, PrintQuality.proof));
  }

  private pdfUrl(order: MarketingOrder, product: ProductInstance, addToken?: boolean): string  {
    const listing = order.listing;
    addToken = addToken || false;
    let filename = snakeCase(product.title.toLowerCase()) +'_'+ snakeCase(listing.address.streetAddress1) +'.pdf';
    filename = filename.toLowerCase();
    let url = format('/print-render/order/%s/%s/123/%s/%s', order._id, product.code, PrintQuality.print, filename);
    if (addToken) {
      url += '?x='+this.authenticationService.getToken();
    }
    return url;
  }


  private buildPdfRequest(order: MarketingOrder, product: ProductInstance, quality: PrintQuality): PdfRenderRequest  {

    const request = new PdfRenderRequest();
    request.orderId = order._id;
    request.productCode = product.code;
    request.quality = quality || PrintQuality.proof;
    const listing = order.listing;
    const filename = snakeCase(product.title.toLowerCase()) +'_'+ snakeCase(listing.address.streetAddress1) +'.pdf';
    request.fileName = filename.toLowerCase();

    return request;
  }

  private post(request: PdfRenderRequest): Observable<any> {

    console.log('post request: ', request);
    return this.http.post(environment.apiBaseURL+'print-render',
      request,
      { responseType: 'arraybuffer' }
    );
  }
}
