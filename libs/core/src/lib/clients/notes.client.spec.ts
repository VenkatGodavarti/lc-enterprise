import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiClient } from './api.client';
import { environment } from '../../../../environments/environment';
import { NotesClient } from './notes.client';
import { NoteForm } from '../forms';

describe('NotesClient', () => {
  let testController: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      HttpClientTestingModule
    ],
    providers: [
      ApiClient,
      NotesClient
    ]
  }));

  beforeEach(() => {
    testController = TestBed.get(HttpTestingController);
  })

  afterEach(() => {
    testController.verify();
  });

it('get should call GET method with proper url', () => {
    const expectedUrl = `${environment.apiBaseURL}notes?ownerId=123`;
    const client: NotesClient = TestBed.get(NotesClient);
    client.get({ownerId: '123'}).subscribe(() => {});

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('GET');
  });

  it('post should call POST method with proper url', () => {
    const expectedUrl = `${environment.apiBaseURL}notes`;

    const form = new NoteForm();
    form.getControl('ownerId').setValue('123');
    form.getControl('text').setValue('unit test');

    const client: NotesClient = TestBed.get(NotesClient);
    client.post(form).subscribe(() => {});

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('POST');
  });
});
