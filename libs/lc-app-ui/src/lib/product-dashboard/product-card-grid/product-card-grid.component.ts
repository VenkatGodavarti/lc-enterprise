import { Component, Input, OnInit } from '@angular/core';
import { ProductInstance } from '@lc/core';

@Component({
  selector: 'lc-product-card-grid',
  templateUrl: './product-card-grid.component.html',
  styleUrls: ['./product-card-grid.component.scss']
})
export class ProductCardGridComponent implements OnInit {

  @Input()
  products: ProductInstance[];

  constructor() { }

  ngOnInit(): void {
  }
}
