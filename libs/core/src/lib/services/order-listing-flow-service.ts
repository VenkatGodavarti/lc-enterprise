import { Injectable } from '@angular/core';
import { FlowState, MarketingOrder } from '../models';


export const orderListingFlowStatePrototype = {

  name: 'Order Listing',
  version: 1,
  steps: [
    {
      id: "/order-listing/details",
      displayName: 'Listing Details',
      description: 'Agent, property and seller info.',
      routerLinkUri: '/order-listing/:id/:pid/details',
      template: './steps/listing-details/listing.details.component.html',
      canSkipSteps: false,
      data: {
        header1: 'Good start! Let\'s add some additional listing details.',
        header2: '',
        backgroundImage: '/assets/images/order-step-listing.png',
      },
      steps: [
        {
          id: "/order-listing/details/property",
          displayName: 'Property',
          routerLinkUri: '/order-listing/:id/:pid/property'
        },
        {
          id: "/order-listing/details/agent",
          displayName: 'Agent',
          routerLinkUri: '/order-listing/:id/:pid/agent'
        }
      ]
    },
    {
      id: "/order-listing/photography",
      displayName: 'Professional Photography',
      description: 'Select your photographer and your preferred date and time.',
      routerLinkUri: '/order-listing/:id/:pid/photography',
      template: './steps/photography/photography.component.html',
      canSkipSteps: false,
      steps: [
        {
          id: "/order-listing/photography/select-photographer",
          displayName: 'Photographer',
          routerLinkUri: '/order-listing/:id/:pid/select-photographer'
        },
        {
          id: "/order-listing/photography/photography-upgrades",
          displayName: 'Upgrades',
          routerLinkUri: '/order-listing/:id/:pid/photography-upgrades'
        },
        {
          id: "/order-listing/photography/schedule-photographer",
          displayName: 'Date',
          routerLinkUri: '/order-listing/:id/:pid/schedule-photographer'
        },
        {
          id: "/order-listing/photography/photographer-comments",
          displayName: 'Comments',
          routerLinkUri: '/order-listing/:id/:pid/photographer-comments'
        }
      ],
      data: {
        header1: 'This is looking good',
        header2: 'Let\'s pick your photography preferences',
        backgroundImage: '/assets/images/order-step-photo.png',
      },
    },
    {
      id: "/order-listing/package-options",
      displayName: 'Package Options',
      description: 'Customize your package.',
      routerLinkUri: '/order-listing/:id/:pid/package-options',
      template: './steps/package-options/package-options.component.html',
      canSkipSteps: false,
      steps: [
        {
          id: "/order-listing/brochure-options",
          displayName: 'Brochure',
          routerLinkUri: '/order-listing/:id/:pid/brochure-options'
        }],
      data: {
        header1: 'Almost Done!',
        header2: 'Let\'s pick your package options',
        backgroundImage: '/assets/images/order-step-photo.png',
      },
    },
    {
      id: "/order-listing/checkout",
      displayName: 'Checkout',
      description: 'Complete your Listing Concierge order.',
      routerLinkUri: '/order-listing/:id/:pid/checkout',
      template: './steps/checkout/checkout.component.html',
      canSkipSteps: false,
      data: {
        header1: 'Final step!',
        header2: 'Just complete your order to start processing your products.',
        backgroundImage: '/assets/images/order-step-checkout.png',
      },
      steps: [
        {
          id: "/order-listing/checkout/shipping",
          displayName: 'Shipping',
          routerLinkUri: '/order-listing/:id/:pid/shipping'
        },
        {
          id: "/order-listing/checkout/billing",
          displayName: 'Billing',
          routerLinkUri: '/order-listing/:id/:pid/billing'
        },
        {
          id: "/order-listing/checkout/payments",
          displayName: 'Payments',
          routerLinkUri: '/order-listing/:id/:pid/payments'
        },

      ]
    }
  ]
};

@Injectable()
export class OrderListingFlowStateService {

  resource = 'marketing-orders';

  constructor() { }

  /**
   * Create a listing and optionally set step states based on a listing model
   *
   * @param order option listing param to inspect to set state on steps
   */
  createNewFlowState(marketingOrder?: MarketingOrder): FlowState {
    const orderListingFlows = Object.assign({}, orderListingFlowStatePrototype);

    if(marketingOrder)  {
      const photographyProduct = marketingOrder.getPhotographyProduct();

      if(photographyProduct == null || photographyProduct.optedOut) {
        const photoSteps = orderListingFlowStatePrototype.steps.filter(step => step.id !== '/order-listing/photography');
        orderListingFlows.steps = photoSteps;
      }
    }

    const flowState =  new FlowState(orderListingFlows);

    if (marketingOrder) {
      // If we have a listing, we should still deserialize state to initialize currentStep if it isn't already set
      flowState.deserializeState(marketingOrder.getOrderState() ? marketingOrder.getOrderState().state : null);
    }
    return flowState;
  }

}
