
import { Then } from 'cucumber';
import { LCPage, ElementMap, Context, ContextKeys, Configuration} from '../../../../../../../e2e';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^the multimedia links are all displayed in the (.*) table$/, {timeout: Configuration.timeout}, async (tableName: string) => {

  const order = Context.get(ContextKeys.order)
  const page = Context.get<LCPage>(ContextKeys.page);

  await page.waitOnReady();

  const links = order.media.multimedia.sort( (a, b) => {
    new Date(b.updatedAt).getTime() < new Date(a).getTime();
  })

  const map: ElementMap = page.findMap(tableName);

  for (let i = 0; i < links.length; i++) {
    const finder = map.element('Link').finder(i);
    expect(await finder.getText()).to.be.equal(links[i].uri);
  }

});

const indexMap = {
  first: 0,
  second: 1,
  third: 2,
  fourth: 3,
  fifth: 4
};
Then(/^I click the (.*) in the (first|second|third|fourth|fifth) row in the (.*) table$/, {timeout: Configuration.timeout}, async (elementName: string, row: string, tableName: string) => {

  const page = Context.get<LCPage>(ContextKeys.page);
  // await page.waitOnReady();

  const map: ElementMap = page.findMap(tableName);

  const finder = map.element(elementName).finder(indexMap[row]);

  finder.click();
});
