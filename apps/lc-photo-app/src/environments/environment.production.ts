export const environment = {
  // App configuration
  production: true,
  envName: 'production',
  applicationName: 'Listing Concierge Photo App',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://1.2.3.4:4700/',
  apiBaseURL: 'https://1.2.3.4.realogy.com/api/v1/',
  photosBaseURL: 'https://1.2.3.4.realogy.com/api/v1/',
  notifyBaseURL: 'http://1.2.3.4.4800/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.1.2.3.4.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajcwwe6zYnqyhEA0h7',
  oktaRedirectUri: 'https://1.2.3.4.realogy.com/implicit/callback',
};
