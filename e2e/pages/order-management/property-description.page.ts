import { LCPage, ElementMap, Element } from '../../support';

export class PropertyDescriptionPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Page Title', field: '#page-title'}),
      new Element({ name: 'Headline', field: 'textarea#headline'}),
      new Element({ name: 'Body', field: 'textarea#body'}),
      new Element({ name: 'Cancel', field: 'button#property-description-cancel'}),
      new Element({ name: 'Save', field: 'button#property-description-submit'})
    ]
  );
  constructor() {
    super('Property Description', [PropertyDescriptionPage.elements]);
  }

}
