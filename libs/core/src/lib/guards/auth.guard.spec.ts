import { TestBed } from '@angular/core/testing';
import { AuthenticationService, AuthGuard } from '@lc/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TestingModule } from '@lc/testing';

describe('AuthGuard', () => {

  let authGuard: AuthGuard;
  let authServiceSpy: jasmine.SpyObj<AuthenticationService>;
  let routerStateSpy: jasmine.SpyObj<RouterStateSnapshot>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRouteSnapshot>;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['isAuthenticated', 'redirectToLogin']);
    routerStateSpy = jasmine.createSpyObj('RouterStateSnapshot', ['toString']);
    activatedRouteSpy = jasmine.createSpyObj('ActivatedRouterSnapshot', ['toString']);

    TestBed.configureTestingModule({
      imports: [TestingModule],
      providers: [AuthGuard,
        {provide: AuthenticationService, useValue: authServiceSpy},
        {provide: RouterStateSnapshot, useValue: routerStateSpy},
        {provide: ActivatedRouteSnapshot, useValue: activatedRouteSpy}]
    });

    authGuard = TestBed.get(AuthGuard);
    authServiceSpy = TestBed.get(AuthenticationService);
    activatedRouteSpy = TestBed.get(ActivatedRouteSnapshot);
  });

  afterEach(() => {
  });

  describe('#canActivate', () => {
    it('returns true when user is authenticated', () => {
      const expected = true;

      authServiceSpy.isAuthenticated.and.returnValue(expected);

      const result = authGuard.canActivate(activatedRouteSpy, routerStateSpy);

      expect(result).toEqual(expected);
    });

    it('returns false when user is not authenticated', () => {
      const expected = false;

      authServiceSpy.isAuthenticated.and.returnValue(expected);

      const result = authGuard.canActivate(activatedRouteSpy, routerStateSpy);

      expect(result).toEqual(expected);
      expect(authServiceSpy.redirectToLogin).toHaveBeenCalled();
    });
  });

});
