import { http } from './http';
import { Configuration as config } from '../configuration';

export class PhotoOrdersClient {
  static async get() {
    const res = await http.get(`${config.apiBaseURL}/photo-orders`);
    const orders = res.data;
    return orders;
  }

  static async put(photoOrder: any) {
    return http.put(`${config.apiBaseURL}/photo-orders/${photoOrder._id}`, photoOrder);
  }

  static async delete(photoOrderId) {
    await http.delete(`${config.apiBaseURL}/photo-orders/${photoOrderId}`);
  }

  static async findByMarketingOrderId(marketingOrderId) {
    const photoOrders = await PhotoOrdersClient.get();
    const photoOrder = photoOrders.find(p => p.marketingOrderId === marketingOrderId);
    return photoOrder;
  }

  static async addPhotos(photoOrder: any, photos, push: boolean = true) {
    return http.put(`${config.apiBaseURL}/photo-orders/${photoOrder._id}/photos?push=${push}`, photos);
  }
}
