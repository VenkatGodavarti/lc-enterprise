import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

export class PageRequest {
  constructor(public pageNumber: number, public pageSize: number){

  }
}

@Component({
  selector: 'lc-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input()
  pageSize: number = 25;

  @Input()
  pageNumber: number = 1;

  @Input()
  totalCount: number;

  @Input()
  pageSizeOptions: number[] = [5, 25, 50, 100, 150];

  @Output()
  pageChange = new EventEmitter<PageRequest>();

  constructor() { }

  ngOnInit() {
  }

  onPagingChanged() {
    this.pageChange.emit(new PageRequest(this.pageNumber, this.pageSize));
  }
}
