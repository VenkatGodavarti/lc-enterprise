/**
 * Maps user friendly section and page names to urls and page objects
 * for navigation and testing of page elements.
 */
export class NavigationMap {

  private static mappings = {

  };

  static add(data: any) {
    for (const key in data) {
      //console.log('adding navigation: ', key);
      this.mappings[key] = data[key];
    }
  }

  static page(section: string, page: string, params?: any): any {

    section = section.toLowerCase();
    page = page.toLowerCase();


    params = params || {};

    const found = NavigationMap.lookup(section, page);

    return {
      url: NavigationMap.replace(found.url, params),
      page: found.page
    };
  }

  private static lookup(section: string, page: string) {

    const sectionMappings = NavigationMap.lookupSection(section);

    return NavigationMap.lookupPage(sectionMappings, page);
  }

  private static lookupSection(section: string): any {

    const sectionMappings = NavigationMap.mappings[section];

    if (!sectionMappings) {
      throw new Error(
        'No mappings found for section: '+section
      )
    }
    return sectionMappings;
  }

  private static lookupPage(sectionMappings: any, page: string): any {

    const found = sectionMappings[page];
    if (!found) {
      throw new Error(
        'Nothing found for page: '+page
      )
    }
    return found;
  }

  private static replace(path: string, params) {

    let replaced = path;
    if (params) {
      for (const key in params) {

        const value = params[key];

        if (replaced.includes(':' + key)) {
          replaced = replaced.replace(':' + key, value);
        }
      }
    }
    return replaced;
  }
}
