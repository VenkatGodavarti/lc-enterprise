
import { ElementMap, Element } from './element-map';
import { browser, ExpectedConditions as EC, ElementFinder, protractor, ElementArrayFinder } from 'protractor';

export class LCPage {
  private _maps = {};

  constructor(public name: string, maps: ElementMap[], public files?: {[key: string]: string}) {

    this.name = name;

    for (let i = 0; i < maps.length; i++) {
      this._maps[maps[i].name] = maps[i];
    }
  }

  findMap(name?: string): ElementMap {
    name = name || 'page';
    if (!Object.keys(this._maps).includes(name) ) {
      throw new Error(`Cannot find ElementMap with name ${name} in page ${this.name}`);
    }
    const map =  this._maps[name];

    return map;
  }

  element(mapName: string, elementName?: string): Element {

    mapName = mapName.split('"').join('');

    if (!elementName) {
      return this.anyElement(mapName);
    }
    elementName = elementName.split('"').join('');
    return this._maps[mapName.toLowerCase()].element(elementName);
  }


  anyElement(elementName) {

    for (const key in this._maps) {
      const element = this._maps[key].element(elementName);
      if (element) {
        return element;
      }
    }
    throw new Error(`${this.name} page has no element with name: ${elementName}`);
  }

  getElement(elementName: string, parentMap?: string): Element {
    if(parentMap) {
      return this._maps[parentMap.toLowerCase()].element(elementName);
    }
    return this.anyElement(elementName);
  }

  finder(mapName: string, elementName?: string): ElementFinder {
    return this.element(mapName, elementName).finder();
  }

  arrayFinder(mapName: string, elementName?: string): ElementArrayFinder {
    return this.element(mapName, elementName).arrayFinder();
  }
  async waitOnReady() {

    const pageMap = this.findMap();
    const firstElement = pageMap.elements()[0];

    await browser.wait(EC.presenceOf(firstElement.finder()));
  }

  async clickEnter() {
    await browser.actions().sendKeys(protractor.Key.ENTER).perform();
  }

  async mouseHover(element: ElementFinder) {
    await browser.actions().mouseMove(element).perform();
  }


  async waitFor(element) {


    await browser.wait(EC.presenceOf(element));
  }

  async waitForElement(element: Element) {
    await this.waitFor(element.finder());
  }
}
