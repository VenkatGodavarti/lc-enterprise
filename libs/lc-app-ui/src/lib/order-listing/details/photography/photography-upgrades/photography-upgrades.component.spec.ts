import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { PhotographyUpgradesComponent } from './photography-upgrades.component';
import { Store } from '@ngrx/store';
import { StoreMock, TestingModule } from '@lc/testing';
import { FlowState, MarketingOrder, ORDERSTATE, SELECTEDORDER } from '@lc/core';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../../../core/src/lib/factories';
import { StepContentComponent } from '../../step-content/step-content.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrderStepsComponent } from '../../../steps/order-steps/order-steps.component';
import { OrderStepTemplateComponent } from '../../../steps/order-steps/order-step-template/order-step-template.component';
import { DetailStepsComponent } from '../../detail-steps/detail-steps.component';
import { DetailStepTemplateComponent } from '../../detail-steps/detail-step-template/detail-step-template.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('PhotographyUpgradesComponent', () => {
  let component: PhotographyUpgradesComponent;
  let fixture: ComponentFixture<PhotographyUpgradesComponent>;

  let order: MarketingOrder;
  let store: StoreMock<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule,
        TestingModule
      ],
      declarations: [
        PhotographyUpgradesComponent,
        OrderStepsComponent,
        OrderStepTemplateComponent,
        DetailStepsComponent,
        DetailStepTemplateComponent,
        StepContentComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    order = createOrder();
    store.setState(SELECTEDORDER, order); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographyUpgradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('chooses the professional photo package', () => {
    expect(order.getPhotographyProduct()).toBeTruthy();
  });

  it('onSubmit should update option selections', () => {
    store.setState(SELECTEDORDER, createOrder());


    const options = component.availableOptions;
    options[0].isSelected = true;
    options[2].isSelected = true;
    component.updateSelection(options.filter(x => x.isSelected));

    component.onSubmit(order);

    component.marketingOrder$.subscribe(marketingOrder => {
      const product = marketingOrder.getPhotographyProduct();
      const photoOptions = product.options;
      expect(photoOptions[0].selected).toBeTruthy();
      expect(photoOptions[1].selected).toBeFalsy();
      expect(photoOptions[2].selected).toBeTruthy();
    });
  });

  function createOrder() {
    const newOrder : MarketingOrder = MarketingOrderFactory.createMarketingOrder();
    newOrder.availablePackages = PackageInstanceFactory.createAll();
    newOrder.selectedPackageCode = newOrder.availablePackages[1].code;
    return newOrder;
  }
});
