import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ApiService } from './api.service';
import { Profile } from '../models/profile.model';
import { PresignService } from './presign.service';
import { Observable } from 'rxjs';

/**
 * Client side service for interacting with the server to retrieve and update profiles.
 */
@Injectable()
export class ProfileService {
  resource = 'profiles';

  constructor(private apiService: ApiService) {
  }

  /**
   * Update an existing profile with
   *
   * @param id
   * @param profile
   * @return an Observable that results in a Profile
   */
  updateProfile(id: string, profile: Profile): Observable<Profile> {
    return this.apiService.put(`profiles/${id}`, profile).pipe(map( body => new Profile(body)));
  }

  updateProfilePartial(profile: Profile, fields?: string[]): Observable<Profile> {
    let payload: any;

    payload = profile;
    if (fields && fields.length) {

      payload = {};
      //order.orderState = order.listing.orderState;
      fields.forEach( field => {
        payload[field] = profile[field];
      });
    }
    return this.apiService.put(`${this.resource}/${profile._id}`, payload).pipe(map( body => new Profile(body)))
  }
}
