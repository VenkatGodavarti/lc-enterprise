import { Deserializable } from './deserializable.model';

import { Listing, OrderState } from '../models/listing.model';
import { PackageInstance } from '../models/package-instance.model';
import { AddressInformation } from '../models/address-information.model';
import { Media } from './media.model';
import { ListingPhoto } from '../models/listing-photo.model';
import { PhotoOrder } from '../models/photo-order.model';
import { User } from '../models/user.model';
import { ProductInstance } from './product-instance.model';
import { PhotographerAppointment } from './photographer-appointment.model';
import { ServiceStatusType } from './service-status.type';
import { StateTransition } from './status-history';
import { Audit } from '../models/audit.model';

export enum OrderStatus {
  PENDING = 'PENDING',
  OPEN = 'OPEN',
  CANCELLED = 'CANCELLED',
  PROCESSING = 'PROCESSING',
  CLOSED = 'CLOSED',
  STOPPED = 'STOPPED'
}

export interface OrderStateStore {
  getOrderState(): OrderState;
  setOrderState(state: OrderState): OrderState
}

export class MarketingOrder implements Deserializable<MarketingOrder>, OrderStateStore {

  _id: string;
  agentId: string;
  coListAgentId: string;
  teamId: string;
  coordinatorId: string;
  listing: Listing;
  media: Media;
  availablePackages: PackageInstance[];
  selectedPackageCode: string;
  shippingInformation: AddressInformation;
  billingInformation: AddressInformation;
  marketingCopyHeadline: string[] = [];
  marketingCopyBody: string[] = [];
  photoOrder: PhotoOrder;
  status: string = OrderStatus.OPEN;
  orderState: OrderState;
  summary = false;
  audit: Audit;
  photos: ListingPhoto[] = [];
  isArchived: boolean;

  // Indicates order entry process has completed
  submitted: boolean = false;

  readonly copyCount: number;
  readonly photoCount: number;

  statusHistory: StateTransition[];

  constructor(input?: any) {
    if (input) {
      this.deserialize(input);
    }
  }

  // TODO - when using new MarketingOrder(aMarketingOrder) not all elements are cloned. media.multimedia is one example
  deserialize(input: any): MarketingOrder {
    Object.assign(this, input);

    if (this.availablePackages) {
      // if raw json data, ensure this gets converted to array of PackageInstances
      this.setAvailablePackages(this.availablePackages);
    }
    if (this.listing) {
      this.listing = new Listing(this.listing);
    }
    if (this.media) {
      this.media = new Media(this.media);
    }
    this.photos = [];
    if (input.photos) {
      input.photos.forEach((photo) => {
        this.photos.push(new ListingPhoto(photo));
      });
    }
    return this;
  }

  public getSelectedPackage(): PackageInstance | undefined {
    if (this.selectedPackageCode && this.availablePackages) {
      return this.availablePackages.find(p => p.code === this.selectedPackageCode);
    } else {
      return undefined;
    }
  }

  /**
   * This is a hack to get the correct photos at the listing or the product instance level
   * @param productCode the optional parameter. When passed the photos will be returned from product instance
   */
  public getPhotosFor(productCode?: string): ListingPhoto[] {
    if (!productCode) {
      return this.photos;
    }
    const pkg = this.availablePackages.find(aPkg => aPkg.code === this.selectedPackageCode);
    const product = pkg.products.find(aProduct => aProduct.code === productCode);
    return product.photos;
  }

  /**
   * Sets photos list on listing or a product instance
   * @param photos
   * @param productCode
   */
  public setPhotos(photos: ListingPhoto[], productCode?: string): void {
    if (!productCode) {
      this.photos = photos;
    } else {
      const pkg = this.availablePackages.find(aPkg => aPkg.code === this.selectedPackageCode);
      const product = pkg.products.find(aProduct => aProduct.code === productCode);
      product.photos = photos;
    }
  }

    /**
   * Copy photos with different uris down to product instance level
   * If a photo is already on the product instance, the copy will not override the existing entry. It will determine this
   * strictly by uri
   *
   * @param productCode the product code to target. We assume the selectedPackageCode on listing will be used
   */
  public copyPhotos(productCode: string): ListingPhoto[] {
    if (this.photos) {
      const productPhotos = this.getPhotosFor(productCode);
      this.photos.forEach(lPhoto => {
        const matchingPhoto = productPhotos.find(pPhoto => pPhoto.uri === lPhoto.uri);
        if (!matchingPhoto) {
          const movedPhoto = new ListingPhoto(lPhoto);
          movedPhoto.order = productPhotos.length + 1;
          productPhotos.push(movedPhoto);
        }
      });
      this.setPhotos(productPhotos, productCode);
      return productPhotos;
    }
    return null;
  }

  public createPhotoOrder(user: User): PhotoOrder {
    const photographyProduct = this.getSelectedPackage().getPhotographyProduct();
    if (photographyProduct) {
      const appointment = photographyProduct.serviceAppointment;
      if (appointment) {
        const photoOrder = new PhotoOrder();
        photoOrder.appointment = appointment;
        photoOrder.propertyAddress = this.listing.address;
        photoOrder.photographerId = appointment.photographer._id;
        photoOrder.orderedAt = new Date();
        photoOrder.agentName = `${user.firstName} ${user.lastName}`;
        photoOrder.status = ServiceStatusType.PENDING;
        return photoOrder;
      }
    }
    return undefined;
  }

  public setAvailablePackages(data: PackageInstance[]): void {
    this.availablePackages = data.map(pkg => new PackageInstance(pkg));
  }

  public getPhotographyProduct(): ProductInstance {
      const pkg = this.getSelectedPackage();
      if (pkg) {
        return pkg.getPhotographyProduct();
    }
    return undefined;
  }


  public getBrochureProduct(): ProductInstance {
    const pkg = this.getSelectedPackage();
    if (pkg) {
      return pkg.getBrochureProduct();
    }
    return undefined;
  }

  clearPackageSpecificData(){
    // HACK: Would be better to maintain a snapshot of the unmodified available packages and just clone the new package
    // To clear package specific data, we should be using selectedPackage instead of selectedPackageCode. Then we can simply
    // reassign the selectedPackage from the availablePackages snapshot (which SHOULD ONLY have snapshot data)
    const currentPackage = this.getSelectedPackage();
    if(currentPackage) {
      const photographyProduct = currentPackage.getPhotographyProduct();
      if(photographyProduct) {
        photographyProduct.serviceAppointment = new PhotographerAppointment();
        photographyProduct.selectedTemplate = null;
      }
    }
  }

  getOrderState(): OrderState {
    return this.orderState;
  }

  setOrderState(state: OrderState): OrderState {
    this.orderState = state;
    return state;
  }
}
