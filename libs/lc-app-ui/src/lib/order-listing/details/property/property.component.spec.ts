import { async, ComponentFixture, fakeAsync, flush, TestBed, inject } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { PropertyComponent } from './property.component';
import { DetailStepsComponent } from '../detail-steps/detail-steps.component';
import {
  FlowState,
  Listing,
  PropertyStatus,
  PropertyType,
  PropertyInfoForm,
  ORDERSTATE,
  MarketingOrder, SELECTEDORDER
} from '@lc/core';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { TestingModule, StoreMock } from '@lc/testing';

describe('PropertyComponent', () => {
  let component: PropertyComponent;
  let fixture: ComponentFixture<PropertyComponent>;
  let store: StoreMock<any>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        TestingModule
      ],
      declarations: [
        DetailStepsComponent,
        PropertyComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, createOrder(true)); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));


  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(PropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    flush();
  }));

  it('should be created', () => {
    expect(component).toBeTruthy();
    expect(component.infoForm).toBeTruthy();

  });

  it('new listing should set propertyType single-family', async() => {
    expect(component).toBeTruthy();
    component.currentListing$.subscribe(listing => {
      expect(listing).toBeTruthy();
      expect(listing.propertyType).toEqual(PropertyType.SINGLE_FAMILY);
    });
  });


  it('new listing should set status active', () => {
    expect(component).toBeTruthy();
    component.currentListing$.subscribe(listing => {
      expect(listing).toBeTruthy();
      expect(listing.status).toEqual(PropertyStatus.ACTIVE);
    });
  });

  describe('#incrementBathroom', () => {
    it('#should increase the number of bathrooms', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const startingBathrooms = 0;
      const endingBathrooms = 0.25;

      expect(app.infoForm.controls['bathrooms'].value).toEqual(startingBathrooms);
      app.infoForm.incrementBathrooms();
      expect(app.infoForm.controls['bathrooms'].value).toEqual(endingBathrooms);
    });

    it('#should decrease the number of bathrooms', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const startingBathrooms = 4;
      const endingBathrooms = 3.75;

      app.infoForm.controls['bathrooms'].setValue(startingBathrooms);
      expect(app.infoForm.controls['bathrooms'].value).toEqual(startingBathrooms);

      app.infoForm.decrementBathrooms();
      expect(app.infoForm.controls['bathrooms'].value).toEqual(endingBathrooms);
    });

    it('#should never allow the value to be less than 0', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const startingBathrooms = 0;

      app.infoForm.controls['bathrooms'].setValue(startingBathrooms);
      expect(app.infoForm.controls['bathrooms'].value).toEqual(startingBathrooms);

      app.infoForm.decrementBathrooms();
      expect(app.infoForm.controls['bathrooms'].value).toEqual(startingBathrooms);
    });
  });

  describe('#incrementBedroom', () => {
    it('#should increase the number of bedrooms', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const starting = 0;
      const ending = 1;

      expect(app.infoForm.controls['bedrooms'].value).toEqual(starting);
      app.infoForm.incrementBedrooms();
      expect(app.infoForm.controls['bedrooms'].value).toEqual(ending);
    });

    it('#should decrease the number of bedrooms', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const starting = 4;
      const ending = 3;

      app.infoForm.controls['bedrooms'].setValue(starting);
      expect(app.infoForm.controls['bedrooms'].value).toEqual(starting);

      app.infoForm.decrementBedrooms();
      expect(app.infoForm.controls['bedrooms'].value).toEqual(ending);
    });

    it('#should never allow the value to be less than 0', () => {
      const app = <PropertyComponent>fixture.debugElement.componentInstance;
      const starting = 0;

      app.infoForm.controls['bedrooms'].setValue(starting);
      expect(app.infoForm.controls['bedrooms'].value).toEqual(starting);

      app.infoForm.decrementBedrooms();
      expect(app.infoForm.controls['bedrooms'].value).toEqual(starting);
    });
  });

  describe('#onSubmit', () => {
    // TODO - taking this test out since the form validation is not working and allowing a update
    // it('should not save if the form is not valid', () => {
    //   component.infoForm.reset();
    //
    //   spyOn(listingStoreService, 'updateCurrentListing').and.returnValue(listing);
    //
    //   component.onSubmit();
    //
    //   expect(listingStoreService.updateCurrentListing).not.toHaveBeenCalled();
    //   expect(component.store.dispatch).not.toHaveBeenCalled();
    //   flush();
    // });

    it('#onSubmit should update the listing and navigate to the agent', fakeAsync(() => {

      const dispatchMethod = spyOn(store, 'dispatch').and.callFake(() => {});
      component.currentListing$.subscribe(listing => {
        component.onSubmit(listing);
        expect(dispatchMethod).toHaveBeenCalled();
      });

      fixture.detectChanges();
      flush();
    }));

    it('#onSubmit should create the listing and navigate to the agent', fakeAsync(() => {

      const dispatchMethod = spyOn(store, 'dispatch').and.callFake(() => {});
      component.currentListing$.subscribe(listing => {
        component.onSubmit(listing);
        expect(dispatchMethod).toHaveBeenCalled();
      });

      fixture.detectChanges();
      flush();
    }));
  });

  function createOrder(hasId: boolean) {
    const newListing = new Listing();
    newListing._id = hasId ? '123456789ABCDEF' : null;
    newListing.listPrice = 400000;
    newListing.sqFt = 4000;
    newListing.lotSqFt = 14000;
    newListing.listDate = new Date();

    const newOrder : MarketingOrder = new MarketingOrder();
    newOrder.listing = newListing;

    return newOrder;
  }
});
