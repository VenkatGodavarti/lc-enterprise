
import { Before, After } from 'cucumber';
import { Context, ContextKeys, Scheduler, Schedule } from '../../support';
import * as emptyProfileData from './data/empty-profile.json';
import { http } from '../../clients/http';
import { Configuration as config } from '../../configuration';
import { TeamProfilesClient } from '../../clients';

const profileKey = 'profile';

Before({tags: "@empty-profile", timeout: config.timeout}, async () => {
  storeCurrentProfile();
  await setProfile(emptyProfileData);
});

After({tags: "@empty-profile", timeout: config.timeout}, async () => {
  const profile = Context.get(profileKey);
  await setProfile(profile);
});

Before({tags: "@empty-team-profile", timeout: config.timeout}, async () => {
  const originalTeams = await deleteAllCreatedTeams();

  // Schedule to restore teams to original state after the scenario completes
  Scheduler.schedule(Schedule.afterScenario, async () => {
    // Delete all teams that were created during the test run
    await deleteAllCreatedTeams();

    // Recreate the original teams
    originalTeams.forEach(async team => await TeamProfilesClient.post(team))
  })
});

function storeCurrentProfile(){
  const user = Context.get(ContextKeys.user);
  Context.set(profileKey, user.profile);
}

async function setProfile(profile) {
  const profileId = Context.get(ContextKeys.user).profile._id;
  const route = `${config.apiBaseURL}/profiles/${profileId}`;

  delete profile.commonProfile;
  delete profile.user;

  const json = JSON.stringify(profile);
  const response = await http.put(route, json);
  return response.data;
}

/**
 * Deletes all teams belonging to the logged in user and returns the teams that were deleted
 */
async function deleteAllCreatedTeams() {
  const teams = await TeamProfilesClient.get();
  teams.forEach(async team => await TeamProfilesClient.delete(team._id));
  return teams;
}

