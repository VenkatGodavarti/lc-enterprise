import { Component, Input, HostBinding, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService, UserService, User, ProfileService, PhotoService} from "@lc/core";
import { AppService } from '../../layout/app.service';
import { LayoutService } from '../../layout/layout.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {
  UserActivityService,
  ActivityTimeoutConfirmation,
  ContinueConfirmationHandler
} from '../../auth/user-activity/user-activity-service';

@Component({
  selector: 'lc-app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styles: [':host { display: block; }'],
  styleUrls: ['./layout-navbar.component.scss'],

})
export class LayoutNavbarComponent implements OnInit, OnDestroy, ActivityTimeoutConfirmation {

  user: User;
  username = '';
  currentUserSub: Subscription;
  isExpanded = false;
  isRTL: boolean;
  confirmActivityTimeoutModal: NgbModalRef;

  @Input() sidenavToggle = true;

  @HostBinding('class.layout-navbar') private hostClassMain = true;

  @ViewChild('logout.confirm') logoutConfirmTemplate: any;

  @ViewChild('inactivity.continue') inactivityContinueTemplate: any;

  constructor(private appService: AppService, private layoutService: LayoutService,
              private authService: AuthenticationService, private userService: UserService,
              private userActivityService: UserActivityService,
              private modal: NgbModal) {
    this.isRTL = appService.isRTL;
  }

  ngOnInit() {
    //TODO from LC-416: replace currentUser with store.select(LOGGEDINUSER)
    this.currentUserSub = this.userService.currentUser.subscribe((user) => {
      this.user = user;
      this.username = this.userService.getUserFirstName();
    });

    this.userActivityService.setActivityTimeoutConfirmation(this);
  }

  ngOnDestroy(): void {
    this.currentUserSub.unsubscribe();
  }

  currentBg() {
    return `bg-${this.appService.layoutNavbarBg}`;
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }

  isUserAuthenticated() {
    return this.authService.isAuthenticated();
  }

  // logout() is not a good candidate for unit testing because it merely opens a modal and the modal handles
  // the rest of the interaction.  Marking it as istanbul ignore.  Should definitely be tested via Cucumber though.
  /* istanbul ignore next */
  logout () {
    this.modal.open(this.logoutConfirmTemplate, {centered: true}).result
      .then(() => {
        setTimeout(() => this.authService.redirectToLogout(), 300);
      }, () => {
        // no-op
      });
  }

  confirmContinue(continueConfirmationHandler: ContinueConfirmationHandler): void {
    this.confirmActivityTimeoutModal = this.modal.open(this.inactivityContinueTemplate, {centered: true});
    this.confirmActivityTimeoutModal.result
      .then(() => {
        continueConfirmationHandler.confirmInactivityLogout();
      }, () => {
        continueConfirmationHandler.confirmInactivityContinue();
      });
  }

  clearRequest(): void {
    if (this.confirmActivityTimeoutModal) {
      this.confirmActivityTimeoutModal.close();
    }
  }

}
