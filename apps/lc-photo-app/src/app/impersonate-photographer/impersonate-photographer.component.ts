import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Photographer, AuthenticationService, LoadPhotoOrders } from '@lc/core';
import { Observable } from 'rxjs';
import { PhotographerService } from '@lc/core';
import { environment } from '../../../../../libs/environments/environment';
import { Store } from '@ngrx/store';
import { CardOption } from '@lc/shared-components-ui';
import { map } from 'rxjs/operators';

/**
 * the ImpersonatePhotographerComponent is used as a temporary component for impersonating
 * a professional photographer for the sole purpose of retrieving a set of
 * photographer appointment requests.
 */
@Component({
  selector: 'lc-app-impersonate-photographer',
  templateUrl: './impersonate-photographer.component.html',
  styleUrls: ['./impersonate-photographer.component.scss']
})
export class ImpersonatePhotographerComponent implements OnInit {
  selectedPhotographer: Photographer;
  photoUrl = environment.apiBaseURL;
  imageToken: string;

  params: ParamMap;

  cardOptions$: Observable<CardOption<Photographer>[]>;
  constructor(private router: Router,
              private photographerService: PhotographerService,
              private authService: AuthenticationService,
              private store: Store<any>) {

  }

  ngOnInit() {
    // retrieve the auth token for retrieving images from the server
    this.imageToken = this.authService.getToken();

    this.cardOptions$ = this.photographerService.retrievePhotographers().pipe(
      map(photographers => photographers.map(photographer => this.toDisplayOption(photographer)))
    );
  }

  onSelected(options: CardOption<Photographer>[]) {
    if(options.length > 0){
      this.selectedPhotographer = options[0].model;
    } else {
      this.selectedPhotographer = null;
    }
  }

  // Used to set CSS on a photographer card based on whether it is selected or not
  isSelected(photographer) {
    return this.selectedPhotographer && photographer.name === this.selectedPhotographer.name;
  }

  onSubmit() {
    // don't allow if user hasn't chosen a photographer
    if (!this.selectedPhotographer) {
      return;
    }

    this.store.dispatch(new LoadPhotoOrders(this.selectedPhotographer._id));

    // navigate to photography appointment requests
    this.router.navigate(['appointment-list', this.selectedPhotographer._id],
      { queryParams: { photographerName: this.selectedPhotographer.name}});
  }

  /**
   * Converts the Options to a CardOption to be used in the display
   * @param option The photograhper to convert
   */
  private toDisplayOption(photographer: Photographer): CardOption<Photographer> {
    const cardOption = new CardOption(photographer, null, false, photographer.logoUri, photographer.logoUri);
    return cardOption;
  }
}
