import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'lc-team-image',
  templateUrl: './team-image.component.html',
  styleUrls: ['./team-image.component.scss']
})
export class TeamImageComponent implements OnInit {

  @Input() src: string;

  @Input() canEdit: boolean = true;

  @Output()
  readonly edit = new EventEmitter<void>()
  constructor() { }

  ngOnInit() {
  }

  onEdit() {
    this.edit.emit();
  }

}
