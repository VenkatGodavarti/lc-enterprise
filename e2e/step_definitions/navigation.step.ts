
import { browser, ExpectedConditions as EC} from 'protractor';
import { When, Then } from 'cucumber';
import { Context, ContextKeys, NavigationMap } from '../support';
import { Configuration as config } from '../configuration';
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

async function goToPageInSection(page: string, section: string, navigate: boolean) {

  const pageData = NavigationMap.page(section, page, { orderId: Context.get(ContextKeys.orderId)});
  if (navigate) {
    await browser.get(pageData.url);
  }

  await browser.wait(EC.urlContains(pageData.url));

  await pageData.page.waitOnReady();

  Context.set(ContextKeys.page, pageData.page);
  Context.set(ContextKeys.url, pageData.url);

}

When(/^I go to the (.*) page in (.*)$/,  { timeout: config.timeout },  async (page, section) => {

  await goToPageInSection(page, section, true);
});


When(/^the (.*) page from (.*) is displayed$/,  { timeout: config.timeout },  async (page, section) => {

  await goToPageInSection(page, section, false);
});

let previousUrl: string = '';

Then(/^the (.*) page from (.*) is loaded$/,  { timeout: config.timeout },  async (page, section) => {
  const pageData = NavigationMap.page(section, page);

  let matchedUrl = await pageUrlMatches(pageData);
  if(matchedUrl == null) {
    // If not already on page, we will wait for it to load and assert again
    await browser.wait(waitUrlChange(previousUrl));
    matchedUrl = await pageUrlMatches(pageData, true);
  }

  previousUrl = matchedUrl;
  Context.set(ContextKeys.page, pageData.page);
  Context.set(ContextKeys.url, pageData.url);
});

async function pageUrlMatches(pageData: any, throwError = false) {
  let url = await browser.getCurrentUrl();

  // Remove parameters
  const indexOfStartOfParams = url.indexOf('?');
  if(indexOfStartOfParams > -1) {
    url = url.substr(0, indexOfStartOfParams);
  }

  const urlParts = url.split('/').reverse(); // Reverse as we will validate from the end of the route (backwards);

  const pageUrl = pageData.url;
  const pageParts = pageUrl.split('/');

  // Enumerate the url segments, starting with the end
  let matches = true;
  pageParts.reverse().forEach((part, index) => {
    const urlPart = urlParts[index];
    if(part.indexOf(':') === 0) {
      // This is a wild card. Store the value of the corresponding route.
      Context.set(part.replace(':', ''), urlPart);
    } else {
      if(part.trim() != '' && urlPart.toLowerCase() !== part.toLowerCase()){
        matches = false;
        if(throwError) {
          expect(url).to.be.equal(pageUrl);
        }
      }
    }
  });
  return matches ? url : null;

}

function waitUrlChange(previousUrl) {
  return function () {
      return browser.getCurrentUrl().then(function(url) {
          return previousUrl !== url;
      });
  }
}
