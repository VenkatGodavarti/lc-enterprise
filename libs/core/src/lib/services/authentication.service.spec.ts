import { AuthenticationService } from './authentication.service';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import SpyObj = jasmine.SpyObj;
import { OktaAuthenticationProvider, UserService } from '@lc/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let userService: SpyObj<UserService>;
  let authProvider: SpyObj<OktaAuthenticationProvider>;

  const queryParams = {id: 'someValue'};

  beforeEach(fakeAsync(() => {
    userService = jasmine.createSpyObj('UserService', ['getCurrentUser']);
    authProvider = jasmine.createSpyObj('OktaAuthenticationProvider',
      ['redirectToLogin', 'logout', 'getToken', 'handle401Response', 'getHeaderOptions', 'isAuthenticated']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthenticationService,
        {provide: OktaAuthenticationProvider, useValue: authProvider},
        {provide: UserService, useValue: userService},
        {provide: ActivatedRoute, useValue: {
            queryParams: of(queryParams)
        }}
      ]
    });

    userService = TestBed.get(UserService);
    authProvider = TestBed.get(OktaAuthenticationProvider);

    service = TestBed.get(AuthenticationService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.activeAuthService).toBeTruthy();
    expect(service.currentQueryParams).toEqual({id: 'someValue'});
  });

  describe('#redirectToLogin', () => {
    it('redirects to the login page', () => {
      service.redirectToLogin();
      expect(authProvider.redirectToLogin).toHaveBeenCalled();
    });
  });

  describe('#redirectPostLogin', () => {

    it('redirects to a locally stored URL', () => {
      const expectedUrl = '/local/stored/url';

      spyOn(service.router, 'navigateByUrl');
      spyOn(localStorage, 'getItem').and.callFake(function(item) {
        return expectedUrl;
      });
      spyOn(localStorage, 'removeItem');

      service.redirectPostLogin();

      expect(service.router.navigateByUrl).toHaveBeenCalledWith(expectedUrl);
      expect(localStorage.removeItem).toHaveBeenCalledWith('post-login-redirect');
    });

    it('redirects to default homepage', () => {
      spyOn(service.router, 'navigate');
      spyOn(localStorage, 'getItem').and.callFake(function(item) {
        return;
      });
      spyOn(localStorage, 'removeItem');

      service.redirectPostLogin();

      expect(service.router.navigate).toHaveBeenCalledWith(['']);
      expect(localStorage.removeItem).toHaveBeenCalled();
    });
  });

  describe('#redirectToLogout', () => {
    it('redirects to logout route', () => {
      spyOn(service.router, 'navigate');
      service.redirectToLogout();
      expect(service.router.navigate).toHaveBeenCalledWith(['logout']);
    });
  });

  describe('#logout', () => {
    it('logs out via the auth service and removes local storage items', () => {
      const localStorageItem = 'impersonatedUser';

      spyOn(localStorage, 'getItem').and.callFake(function(item) {
        return {};
      });
      spyOn(localStorage, 'removeItem');

      service.logout();

      expect(authProvider.logout).toHaveBeenCalled();
      expect(localStorage.getItem).toHaveBeenCalledWith(localStorageItem);
      expect(localStorage.removeItem).toHaveBeenCalledWith(localStorageItem)
    });
  });

  it('#isAuthenticated authenticates the user', () => {

    authProvider.isAuthenticated.and.returnValue(true);
    userService.getCurrentUser.and.returnValue({});

    const result = service.isAuthenticated();

    expect(result);
  });

});
