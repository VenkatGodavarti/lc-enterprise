/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.cbone_flag = {
		attach: function(context, settings) {
			//Cbone flag at multisection page
			jQuery(".cbone-multisection-flag").click(function(e) {
				e.preventDefault();
				var $this = jQuery(this);
				$this.after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
				var pnid = $this.attr('pnid');
				var cnid = $this.attr('cnid');
				var action = $this.attr('action');
				jQuery.post('/cbone-heart/nojs/'+action+'/'+pnid+'/'+cnid, {"action":action}, function(response) {
					if(response){
						if(action == 'flag') {
							$this.attr('action', 'unflag');
							$this.find('img').attr('src', 'http://cbone-staging.s3-us-west-2.amazonaws.com/s3fs-public/images/icon/flag/heart-icon-gray-x.png');
						}
						else {
							$this.attr('action', 'flag');
							$this.find('img').attr('src', 'http://cbone-staging.s3-us-west-2.amazonaws.com/s3fs-public/images/icon/flag/heart-icon-gray.png');
						}
						jQuery('div.ajax-progress-throbber').remove();
					}
				});
			});
		}
	};
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */
 
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.property_website = {
		attach: function(context, settings) {
      jQuery('div.property-website').on('click', ".web-more-photos-link a", function(e){
        e.preventDefault();
        var $this = jQuery(this);
        var image_count = jQuery('.web-more-photos-link').attr('image_count');
        image_count = parseInt(image_count);
        var nid = jQuery('.property-website').attr('nid');
        var brand = jQuery('.property-website').attr('brand');
        if($this.hasClass("more-photos")){
          var val = jQuery($this).attr('value');
          val = parseInt(val);
          var total = val * 8;
          var total_photos = (val + 1) * 8;
          if(image_count > total){
            if(jQuery("ul.photos-gallery li").length > total) {
              total = total + 8;
              val = val + 1;
              jQuery('ul.photos-gallery li').filter(':lt('+total+')').show();
              jQuery($this).attr('value', val);
              jQuery('.less-photos').attr('value', val);
            }
            else {
              jQuery.ajax({
                data: {"val": val, "type": "more-photos"},
                url: '/cbone/pw-more-photos/'+nid+'/'+brand,
                type: "POST",
                beforeSend: function() {
                  jQuery($this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
                },
                success: function(data) {
                  jQuery('div.ajax-progress-throbber').remove();
                  jQuery('ul.photos-gallery').append(data).find(".group").colorbox({rel:"group", transition:"fade", width:"85%", slideshow:true, slideshowSpeed:5000});
                  val = val + 1;
                  jQuery($this).attr('value', val);
                  jQuery('.less-photos').attr('value', val);
                  jQuery('.less-photos').show();
                }
              });
            }
            if(total_photos >= image_count){
              jQuery($this).hide();
            }
          }
          else {
            jQuery($this).hide();
          }
          jQuery('.less-photos').show();
        }
        else if($this.hasClass("less-photos")){
          jQuery('html, body').animate({
            scrollTop: jQuery( jQuery('#gallery') ).offset().top
          }, 900);
          var val = jQuery($this).attr('value');
          var total = 8;
          val = parseInt(val) - 1;				
          if(val == 0){
            total = 8;
            val = 1;
          }
          else {
            total = val * 8;
          }
          jQuery('ul.photos-gallery li').hide().filter(':lt('+total+')').show();
          if(val == 1) {
            jQuery('.less-photos').hide();					
          }
          if(jQuery("ul.photos-gallery li").length > total) {
            jQuery('.more-photos').show();
          }
          jQuery($this).attr('value', val);
          jQuery('.more-photos').attr('value', val);
        }
      });
		}
	};
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
/*
 * jQuery.appear
 * https://github.com/bas2k/jquery.appear/
 * http://code.google.com/p/jquery-appear/
 *
 * Copyright (c) 2009 Michael Hixson
 * Copyright (c) 2012 Alexander Brovikov
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 */
(function($) {
    $.fn.appear = function(fn, options) {

        var settings = $.extend({

            //arbitrary data to pass to fn
            data: undefined,

            //call fn only on the first appear?
            one: true,

            // X & Y accuracy
            accX: 0,
            accY: 0

        }, options);

        return this.each(function() {

            var t = $(this);

            //whether the element is currently visible
            t.appeared = false;

            if (!fn) {

                //trigger the custom event
                t.trigger('appear', settings.data);
                return;
            }

            var w = $(window);

            //fires the appear event when appropriate
            var check = function() {

                //is the element hidden?
                if (!t.is(':visible')) {

                    //it became hidden
                    t.appeared = false;
                    return;
                }

                //is the element inside the visible window?
                var a = w.scrollLeft();
                var b = w.scrollTop();
                var o = t.offset();
                var x = o.left;
                var y = o.top;

                var ax = settings.accX;
                var ay = settings.accY;
                var th = t.height();
                var wh = w.height();
                var tw = t.width();
                var ww = w.width();

                if (y + th + ay >= b &&
                    y <= b + wh + ay &&
                    x + tw + ax >= a &&
                    x <= a + ww + ax) {

                    //trigger the custom event
                    if (!t.appeared) t.trigger('appear', settings.data);

                } else {

                    //it scrolled out of view
                    t.appeared = false;
                }
            };

            //create a modified fn with some additional logic
            var modifiedFn = function() {

                //mark the element as visible
                t.appeared = true;

                //is this supposed to happen only once?
                if (settings.one) {

                    //remove the check
                    w.unbind('scroll', check);
                    var i = $.inArray(check, $.fn.appear.checks);
                    if (i >= 0) $.fn.appear.checks.splice(i, 1);
                }

                //trigger the original fn
                fn.apply(this, arguments);
            };

            //bind the modified fn to the element
            if (settings.one) t.one('appear', settings.data, modifiedFn);
            else t.bind('appear', settings.data, modifiedFn);

            //check whenever the window scrolls
            w.scroll(check);

            //check whenever the dom changes
            $.fn.appear.checks.push(check);

            //check now
            (check)();
        });
    };

    //keep a queue of appearance checks
    $.extend($.fn.appear, {

        checks: [],
        timeout: null,

        //process the queue
        checkAll: function() {
            var length = $.fn.appear.checks.length;
            if (length > 0) while (length--) ($.fn.appear.checks[length])();
        },

        //check the queue asynchronously
        run: function() {
            if ($.fn.appear.timeout) clearTimeout($.fn.appear.timeout);
            $.fn.appear.timeout = setTimeout($.fn.appear.checkAll, 20);
        }
    });

    //run checks when these methods are called
    $.each(['append', 'prepend', 'after', 'before', 'attr',
        'removeAttr', 'addClass', 'removeClass', 'toggleClass',
        'remove', 'css', 'show', 'hide'], function(i, n) {
        var old = $.fn[n];
        if (old) {
            $.fn[n] = function() {
                var r = old.apply(this, arguments);
                $.fn.appear.run();
                return r;
            }
        }
    });

})(jQuery);
;/*})'"*/
;/*})'"*/
jQuery(document).ready(function($){
  var ua = navigator.userAgent, isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);
  if(isMobileWebkit && Drupal.settings.dexp_animation.disable_mobile){
    $(".dexp-animate").addClass('animated');
  }else{
    $(".dexp-animate").each(function(){
      var $this = $(this);
      var animate_class = $this.data('animate');
      $this.appear(function(){
				setTimeout(function(){
					$this.addClass(animate_class);
					$this.addClass('animated');
				},300);
      },{
        accX: 0,
        accY: 0,
        one:false
      });	
    });
  }
})
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	Drupal.upload_mail_list_plupload = Drupal.upload_mail_list_plupload || {};
	//filesAddedCallback
	Drupal.upload_mail_list_plupload.ListfilesAddedCallback = function (up, files) {
		if(up.files.length == 1){
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').hide('slow');
		}
		else if(files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
		}
		else if(up.files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//FilesRemovedCallback
	Drupal.upload_mail_list_plupload.ListFilesRemovedCallback = function (up, files) {
		if(up.files.length == 0){
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//filesAddedCallback
	Drupal.upload_mail_list_plupload.SoldfilesAddedCallback = function (up, files) {
		if(up.files.length == 1){
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').hide('slow');
		}
		else if(files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
		}
		else if(up.files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//FilesRemovedCallback
	Drupal.upload_mail_list_plupload.SoldFilesRemovedCallback = function (up, files) {
		if(up.files.length == 0){
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.marketing_concierge = {
		attach: function(context, settings) {
			//show/hide the upload link at marketing-concierge-order/[order_id] page			
			//jQuery("#edit-direct-mail-postcards-upload-own-list").click(function(e){
			jQuery("#direct-mail-upload").click(function(e){
				jQuery('#edit-direct-mail-postcards-upload-own-list').prop('checked', true);
			});
			/*jQuery("input[name='direct_mail_postcards']").click(function(e){
				if(jQuery("#edit-direct-mail-postcards-upload-own-list").is(':checked')) {
					jQuery(".direct-mail-file").show();
				}
				else {
					jQuery(".direct-mail-file").hide();
				}
			});*/
			//jQuery('#marketing-concierge-additional-order-form').on('keyup keydown', 'input', function (e) {
			jQuery(".additional-postcards").keyup(function(e) {
				this.value = this.value.replace(/[^0-9\.]/g,'');
				var subtotal_class = jQuery(this).attr('subtotal-class');
				if(this.value == ''){
					jQuery(this).parent(".form-item").next(".additional-total").find(".listed-total").text("--");
					jQuery(".package."+subtotal_class+" .amount").text("--");
					update_subtotal_amount();
				}
				else {
					var additional = jQuery(this).val();
					if(jQuery.isNumeric(additional)){
						var rate = jQuery(this).parent(".form-item").next(".additional-total").find(".rate").text();
						var total = additional * rate;
						total = parseFloat(total).toFixed(2);
						//update listed-total
						jQuery(this).parent(".form-item").next(".additional-total").find(".listed-total").text("$"+total);
						//update subtotal
						jQuery(".package."+subtotal_class+" .amount").text("$"+total);
						update_subtotal_amount();
					}
				}
			});
			//show/hide the "Just Sold Mailing List" file field
			jQuery("#upload-direct-mail-list-form #edit-same-listed-list").click(function(e) {
				jQuery(".form-item-sold-mailing-list").toggle();
			});
			//close popup after click on cancel button
			jQuery("#upload-direct-mail-list-form #edit-cancel").click(function(e) {
				e.preventDefault();
				parent.closeBPopup();
			});
			/*$(window).load(function() {
				if(jQuery('.mc-form-submit .form-submit').hasClass('cbone-mc-free')) {
					jQuery('.mc-form-submit .form-submit').click();
				}
			});*/
		}
	};
})(jQuery, Drupal, this, this.document);

/**
 * Callback function update_subtotal_amount()
 * to update the total amount
 **/
function update_subtotal_amount(){
	var subtotal_amount = 00;
	jQuery(".subtotal-total .amount").text("$"+subtotal_amount);
	jQuery('.subtotal .package .amount').each(function() {
		var amount = jQuery(this).text();
		var amountArray = amount.split('$');
		if(jQuery.isNumeric(amountArray[1])) {
			subtotal_amount = subtotal_amount + parseFloat(amountArray[1]);
		}
	});
	subtotal_amount = parseFloat(subtotal_amount).toFixed(2);
	jQuery(".subtotal-total .amount").text("$"+subtotal_amount);
	subtotal_amount = 00;
}
/**
 * Callback function closeBPopup()
 * to close the bpopup after click
 * on cancel button
 **/
function closeBPopup() {
	jQuery("#popup").bPopup().close()
}jQuery(document).ready(function(){	jQuery('form#add-lc-lms-tool-matrix-form .form-submit').before('<div class="last_border"></div>');	});
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.lc_photos = {
		attach: function(context, settings) {
			if(jQuery('#lc-conc-photos-drag').length > 0){
				jQuery('#lc-conc-photos-drag').sortable({
					appendTo: "body",
					helper: "clone",
					scroll: true,
					cursorAt: {left: 0, top: 0},
				});
			}
			//delete the images from listing-photos/* page
			jQuery('#field-collection-item-form .field-name-field-lms-listing-photo .form-item .image-widget-data #edit-field-lms-listing-photo-und-0-remove-button').attr('value', 'Replace');
			jQuery('.lc-conc-upload-photo-edit #edit-field-lms-listing-photo-update-und-0-value-date').prop("readonly", true);
			jQuery('#delete-selected-lc-conc-photos').click(function(e) {
				e.preventDefault();				
				var data = jQuery(".lms-photo-delete:checked").map(function() {
					return this.value;
				}).get();
				if(data == ''){
					var msg = "Kindly select the images.";
					doDeleteLcConcConfirm(msg, function yes(){
						jQuery('#quicktabs-lc_photos .cdswheel').remove();	
						return false;
					}, function no(){
					});
				}
				else {
					var photo_nid  = jQuery("#listing-photos-draggable").attr("photo_nid");
					var msg = "<span style='text-align: left!important;display: block;margin-left: 7px;'>IMPORTANT:</span>Before deleting images be sure they have not been saved to any LC Products.<br/>Are you sure you wish to permanently delete the selected image(s)?  This action cannot be undone.";
					doDeleteLcConc2Confirm(msg, function yes(){
					jQuery.ajax({
						url: "/listing-photos/delete",
						data:{"data":data, "photo_nid":photo_nid},
						type: "POST",
						success:function(data){
							var msg = "Deleted the selected images.";
								doDeleteLcConcConfirm(msg, function yes(){
									location.reload();
								}, function no(){
								jQuery('div.ajax-progress-throbber').remove();
								return false;
							});
							
						}
					});
					}, function no(){
					jQuery('div.ajax-progress-throbber').remove();
					return false;
					});
				}
			});
			
      /*
      // Task #1872 - Browser Tab Closes When Saving Photo Captions
			jQuery('.edit-lc-upload_update').click(function(e) {
				e.preventDefault();			
				var geturl = jQuery(this).attr('id');	
				//window.open(geturl, "_blank", "scrollbars=1,height=600,width=800");
				window.open(geturl,"_self");
			});
			jQuery(document).on("click", '.lc-conc-upload-photo-edit .lc_conc_upload_edit_submit', function(event) {
				setTimeout(function() {
					var lnid = jQuery("input[name=saved_value_1]").val();
					window.opener.location = lnid;
					window.close();
				},2000);				
			});
      */
      
			jQuery('.update-lc-conc-list').click(function(e) {
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var neworder = new Array();
				if(jQuery('li.photos-draggable').length > 0) {
					jQuery('li.photos-draggable').each(function() {
						var data = {};
						data.id  = jQuery(this).attr("id");
						data.revision_id  = jQuery(this).attr("revision_id");
						neworder.push(data);
					});
					var photo_nid  = jQuery("#lc-conc-photos-drag").attr("photo_nid");
					jQuery.ajax({
						url: "/lc-photos/sortable",
						data:{"neworder":neworder, "photo_nid":photo_nid},
						type: "POST",
						success:function(data){
							
							var msg = "Saved the new listing photos order!";
								doDeleteLcConcConfirm(msg, function yes(){
									jQuery('div.ajax-progress-throbber').remove();
									location.reload();
								}, function no(){
								jQuery('div.ajax-progress-throbber').remove();
								return false;
							});
							
						}
					});
					
				}
				else {
					jQuery('div.ajax-progress-throbber').remove();
					alert("This listing currently has no photos!");
				}
			});
			jQuery('.delete-lms-conc-upload-link').click(function(e) {
					e.preventDefault();			
					var geturl = jQuery(this).attr('href');					
					var url = geturl.split('destination=');
					var url2 = url[0].split('/');
					var url_sec = url[1].split('/');
					var photo_nid  = jQuery("#listing-photos-draggable").attr("photo_nid");					
					var data  = jQuery.makeArray(url2[3]);
					
					var msg = "<span style='text-align: left!important;display: block;margin-left: 7px;'>IMPORTANT:</span>Before deleting images be sure they have not been saved to any LC Products.<br/>Are you sure you wish to permanently delete the selected image(s)?  This action cannot be undone.";
					doDeleteLcConc2Confirm(msg, function yes(){
					jQuery.ajax({
						url: "/listing-photos/delete",
						data:{"data":data, "photo_nid":photo_nid},
						type: "POST",
						success:function(data){
							var msg = "Image has been deleted.";
								doDeleteLcConcConfirm(msg, function yes(){
									location.reload();
								}, function no(){
								jQuery('div.ajax-progress-throbber').remove();
								return false;
							});
							
						}
					});
					}, function no(){
					jQuery('div.ajax-progress-throbber').remove();
					return false;
					});
			});
			jQuery(document).on("click", '.lc-conc-upload-return-link', function(event) {
				jQuery('#quicktabs-tab-lc_photos-1').trigger('click');
			});
			//jQuery('#lc-conc-photographer-import--2 .form-submit').css('display','none');
			
			// jQuery(document).ready(function () {
				// var ckbox = jQuery('#lc-conc-photographer-import--2 .form-checkbox');
				// jQuery('#lc-conc-photographer-import--2 input').on('click',function () {
					// var data = jQuery(":checkbox:checked").map(function() {
						// return this.id;
					// }).get();	
							// if(data.length > 0){
								// jQuery('#lc-conc-photographer-import--2 .form-submit').css('display','block');
							// }
							// else {
								// jQuery('#lc-conc-photographer-import--2 .form-submit').css('display','none');
							// }
				// });
			// });
			
			
			jQuery(".add-lms-conc-caption-title-value").click(function(e){
				e.preventDefault();
				var geturl = jQuery(this).attr('id');
				var url = geturl;
				jQuery('#add_lms_conc_caption_popup').bPopup({
					content: 'iframe', //'ajax', 'iframe' or 'image'
					contentContainer: '.add_lms_conc_caption_popup_area',
					loadUrl: url,
					follow: [false, false],
					position: [342, 119],
					positionStyle: 'fixed',
					iframeAttr: 'scrolling="yes"',
					onOpen: function() {
						jQuery('.b-close span').html("X");
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.add_lms_conc_caption_popup_area').empty();
						jQuery('.b-close span').empty();
						jQuery('.button').hide();
						//jQuery('#quicktabs-tab-lc_property_website-1').trigger('click');
					}
				}, function(){
					//jQuery('.loading-image').hide();
					jQuery("#add_lms_conc_caption_popup iframe").css('width', "679px");
					jQuery("#add_lms_conc_caption_popup iframe").css('height', "425px");
				});
			});
			
			// jQuery(".delete-lms-conc-upload-link").click(function(e){
				// e.preventDefault();
				// var geturl = jQuery(this).attr('href');
				// var url = geturl;
				// jQuery('#add_lms_conc_caption_popup').bPopup({
					// content: 'iframe', //'ajax', 'iframe' or 'image'
					// contentContainer: '.add_lms_conc_caption_popup_area',
					// loadUrl: url,
					// follow: [false, false],
					// position: [342, 119],
					// positionStyle: 'fixed',
					// iframeAttr: 'scrolling="yes"',
					// onOpen: function() {
						// jQuery('.b-close span').html("X");
						// jQuery('.button').show();
					// },
					// onClose: function() {
						// jQuery('.add_lms_conc_caption_popup_area').empty();
						// jQuery('.b-close span').empty();
						// jQuery('.button').hide();
						//jQuery('#quicktabs-tab-lc_property_website-1').trigger('click');
					// }
				// }, function(){
					//jQuery('.loading-image').hide();
					// jQuery("#add_lms_conc_caption_popup iframe").css('width', "679px");
					// jQuery("#add_lms_conc_caption_popup iframe").css('height', "425px");
				// });
			// });	

			jQuery(".edit-lms-conc-upload-link").click(function(e){
				e.preventDefault();
				var geturl = jQuery(this).attr('href');
				var url = geturl;
				jQuery('#add_lms_conc_caption_popup').bPopup({
					content: 'iframe', //'ajax', 'iframe' or 'image'
					contentContainer: '.add_lms_conc_caption_popup_area',
					loadUrl: url,
					follow: [false, false],
					position: [342, 119],
					positionStyle: 'fixed',
					iframeAttr: 'scrolling="yes"',
					onOpen: function() {
						jQuery('.b-close span').html("X");
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.add_lms_conc_caption_popup_area').empty();
						jQuery('.b-close span').empty();
						jQuery('.button').hide();
						//jQuery('#quicktabs-tab-lc_property_website-1').trigger('click');
					}
				}, function(){
					//jQuery('.loading-image').hide();
					jQuery("#add_lms_conc_caption_popup iframe").css('width', "679px");
					jQuery("#add_lms_conc_caption_popup iframe").css('height', "425px");
				});
			});	

			/*
      // Task #1872 - Browser Tab Closes When Saving Photo Captions
			jQuery('.lc-conc-upload-photo-edit .lc_conc_upload_edit_submit').click(function(e){
				//alert("hello");
				setTimeout(function() {
					var lnid = jQuery("input[name=saved_value_1]").val();
					window.opener.location = lnid;
					window.close();
				},4000);				
			});
      */
			
			jQuery('.lc-conc-upload-photo-edit #edit-cancel').click(function(e){
				parent.closelcconPopup();
			});
      
			jQuery('.lc-conc-upload-photo-edit #field-collection-item-delete-confirm #edit-submit').click(function(e){
				setTimeout(function() {
					parent.closelcconPopup();
				},3000);			
			});
      /*chnages on 15-11-2017*/
			jQuery('#lc-conc-photos-upload-form #edit-submit').click(function(e){
			  var numItems = $('#edit-lms-photos_filelist .plupload_droptext').length;
			  if(numItems == 0){
			    jQuery('#quicktabs-lc_photos').prepend('<div class="cdswheel"><div>Processing</div></div>');	
			  }
			});
			
      jQuery('#lc-conc-photographer-import #edit-submit').click(function(e){
        jQuery('#quicktabs-lc_photos').prepend('<div class="cdswheel"><div>Processing</div></div>');
			});
      
      /*chnages on 21-11-2017*/
      jQuery('#field-collection-item-form .lc_conc_upload_edit_submit #edit-submit').click(function(e){
        jQuery('#field-collection-item-form').prepend('<div class="cdswheel"><div>Processing</div></div>');
      });	
		}
	};
})(jQuery, Drupal, this, this.document);

function closelcconPopup() {
	jQuery("#add_lms_conc_caption_popup").bPopup().close();
	var lnid = jQuery('.saved_value_1').attr('saved_value_1');	
	window.location.href = lnid;
	window.location.reload(true);
}
function doDeleteLcConcConfirm(msg, yesFn, noFn) {
   var confirmBox = jQuery("#lcconcconfirmBox");
   var confirmBoxCover = jQuery(".lcconcconfirmBox-group .cover");
   confirmBox.find(".message").text(msg);
   confirmBox.find(".yes,.no").unbind().click(function()
   {
		confirmBox.hide();
		confirmBoxCover.hide();
    jQuery('#quicktabs-lc_photos').prepend('<div class="cdswheel"><div>Processing</div></div>');	
   });
   confirmBox.find(".yes").click(yesFn);
   confirmBox.find(".no").click(noFn);
   confirmBox.show();
   confirmBoxCover.show();
}
function doDeleteLcConc2Confirm(msg, yesFn, noFn) {
   var confirmBox = jQuery("#lcconcconfirmBox2");
   var confirmBoxCover = jQuery(".lcconcconfirmBox2-group .cover");
   confirmBox.find(".message").text('');
	  msg = jQuery.parseHTML(msg);
	 confirmBox.find(".message").append(msg);
   confirmBox.find(".yes,.no").unbind().click(function()
   {
		confirmBox.hide();
		confirmBoxCover.hide();
   });
   confirmBox.find(".yes").click(yesFn);
   confirmBox.find(".no").click(noFn);
   confirmBox.show();
   confirmBoxCover.show();
}

(function ($) {
	$('#edit-lms-photos').ready(function() {
	 var numItems = $('#edit-lms-photos_filelist .plupload_droptext').length;
		if(numItems > 0){
				var objUploader = $("#edit-lms-photos").pluploadQueue();
					objUploader.bind('FilesAdded', function(up, addedFiles) {
						// remove duplicates from uploader.files and addedFiles
						var i = 0;
						var j = 0;
						var dupfile = [];
						var nid  = $("input[name=listing_nid]").val();
						$.ajax({
							type: "GET",
							url: "/get-listing-photos/"+ nid,
							data: {lnid: nid},
							success:function(response){
		             //alert(response); 
  						    var filesArray = response;
									while (i < addedFiles.length) {
										var dupe = false;
										var ii = 0;
										addedFiles[i].name = addedFiles[i].name.replace(/\s/g,'');
										if (filesArray.indexOf(addedFiles[i].name) > -1) {
											dupe = true;
											j++;
											dupfile.push(addedFiles[i].name);
											up.removeFile(up.getFile(addedFiles[i].id));
											$('#' + addedFiles[i].id).remove();
											addedFiles.splice(i, 1);
										} else {
											while (ii < (up.files.length - addedFiles.length)) {
												if (addedFiles[i].name == up.files[ii].name) {
														dupe = true;
														j++;
														dupfile.push(addedFiles[i].name);
														up.removeFile(up.getFile(addedFiles[i].id));
														// added this line of code to remove the visual duplication from the queue
														$('#' + addedFiles[i].id).remove();
														addedFiles.splice(i, 1);
														break;
												}
												ii++;
											}
										}
										
										if (!dupe) { i++; }
								}
								if (j > 0) {
									 dupfile = dupfile.map(function(e){
											return JSON.stringify(e);
									 });
									 var dataString = dupfile.join(",");
									 var msg = 'A photo named '+ dataString +' already exists<br /> in the Photos Bin.<br /> <br />Please change the filename of the new<br /> photo and upload it again.';
										return popupConfirm(msg,function ok(){
											return false;
										});
								}
							}
						});

						
						// at this point the addedFiles array has only the non-duplicate files that
						// have been added to uploader.files, so proceed to do what we need
						// to with the new files (add them to the visual list, etc.)
				});
			}
	});
}(jQuery));

function popupConfirm(msg, ok) {
	 var confirmBox = jQuery("#confirmBox");
	 var confirmBoxCover = jQuery(".confirmBox-group .cover");
	 confirmBox.find(".message").html(msg);
	 confirmBox.find(".yes").unbind().click(function()
	 {
		confirmBox.hide();
		confirmBoxCover.hide();
	 });
	 confirmBox.find(".yes").click(ok);
	 confirmBox.show();
	 confirmBoxCover.show();
}
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */
 
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.lc_flag = {
		attach: function(context, settings) {
			//Cbone flag at multisection page
			jQuery(".lc-flag").click(function(e) {
				e.preventDefault();
				var $this = jQuery(this);
				$this.closest('span').after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
				var pnid = $this.attr('pnid');
				var cnid = $this.attr('cnid');
				var action = $this.attr('action');
				jQuery.post('/lc-heart/nojs/'+action+'/'+pnid+'/'+cnid, {"action":action}, function(response) {
					if(response){
						if(action == 'flag') {
							$this.attr('action', 'unflag');
							$this.css('action', 'unflag');
							$this.children('img').attr('src','/sites/all/modules/custom/listing_concierge_feature/images/circle-favorite-selected.png');							
							jQuery('.fa-heart').css('color', '#4bc1e4');
						}
						else {
							$this.attr('action', 'flag');
							//$this.attr('src', '/sites/all/modules/custom/listing_concierge_feature/images/circle-favorite-selected.png');
							$this.children('img').attr('src', '/sites/all/modules/custom/listing_concierge_feature/images/circle-favorite-not-selected.png');
							$this.css('action', 'unflag');
						}
						jQuery('div.ajax-progress-throbber').remove();
					}
				});
			});
		}
	};
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */
 
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.lc_website = {
		attach: function(context, settings) {
			if(jQuery("#cbone-lc-website-options-open-house-form").length){
				jQuery("#cbone-lc-website-options-open-house-form select").selectBox();
			}
			jQuery(window).bind("load", function() { 
				//jQuery(".options-data-map" ).addClass('options-data');
				var hash = window.location.hash;
				if(hash) {
					//var latestArray = hash.split('document');
					//if(latestArray[0] == '#'){
					if(hash == '#document'){
						//jQuery('#quicktabs-tab-lc_property_website-1').click();
						jQuery('.lc-website-content-tab .documents').click();
					}
					else if(hash == '#document-report'){
						jQuery('li[menu-section="documents-report"]').click();
						//jQuery('#quicktabs-tab-lc_property_website-1').click();
						jQuery('.lc-website-content-tab .documents').click();
					}
				}
				/*if(jQuery(".lc-webpage-map").length){
					var listing_address = jQuery(".lc-webpage-map").attr('listing_address');
					jQuery('#edit-field-lms-gps-coordinates-und-0-address-field').val(listing_address);
					jQuery('.geolocation-address-geocode').click();
				}*/
			});
			
			//var hasTimeEntry = jQuery('.hasTimeEntry.date-popup-init').next('.description').text();
			//console.log("hasTimeEntry = "+hasTimeEntry);
			//var hasDatepicker = jQuery('.hasDatepicker.date-popup-init').next('.description').text();
			//console.log("hasDatepicker = "+hasDatepicker);
			//var hasTimeEntry = jQuery('.hasTimeEntry.date-popup-init').text();
			//var hasDatepicker = jQuery('.hasDatepicker.date-popup-init').text();
			//Update Page title by tabs value
			jQuery(".quicktabs-tabs li a").click(function() {				
				if(jQuery(".lc-website-title").length > 0){
					var name = jQuery(this).text();
					if(name == "Options"){
						var name = "Website";
					}
					//jQuery(".lc-website-title").text(name);
					var main_height = jQuery("#block-system-main").height();
					var left_main_height = jQuery("#block-lc-navigation-lc-navigation-bar-left").height();
					if(main_height > left_main_height){
						jQuery("#block-lc-navigation-lc-navigation-bar-left").css('height', main_height);
					}
				}
			});
			//LC options tab left side menu
			jQuery(".options-title").unbind().click(function(){
      //jQuery(".lc-website-content-data").on('click', ".options-title", function(e){
        var $this = jQuery(this);
				if (jQuery($this).next('.options-data').css('display') == 'none') {
					jQuery('.options-data').hide();
					jQuery('.selectBox-arrow').css('transform', 'rotate(0deg)');
					jQuery(".options-title").removeClass('active');
					jQuery(".options-title").attr('active-section', '');
					jQuery($this).addClass('active');
					jQuery($this).next('.options-data').show();
					jQuery($this).find('.selectBox-arrow').css('transform', 'rotate(90deg)');
					jQuery($this).attr('active-section', jQuery($this).text().toLowerCase());
				}
				else {
					jQuery('.options-data').hide();
					jQuery('.selectBox-arrow').css('transform', 'rotate(0deg)');
					jQuery($this).removeClass('active');
					jQuery(".options-title").attr('active-section', '');
				}
			});
			//check any one design at web page
			jQuery('.design-names input').click(function(e){
				jQuery('.design-names input').prop('checked', false);
				jQuery(this).prop('checked', true);
			});
			//Apply sort functionality for bullect list
			if(jQuery('.lc-document-list').length > 0){
			  jQuery('.lc-document-list').sortable({
				  appendTo: "body",
				  helper: "clone",
				  scroll: false,
				  containment: ".lc-manage-listing-document",
				  cursorAt: {left: 0, top: 0},
			  });
			}
			jQuery('#lc-save-listing-document').click(function(e) {
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var neworder = new Array();
				if(jQuery('li.document-draggable').length > 0) {
					jQuery('li.document-draggable').each(function() {
						//get the id
						var data = {};
						data.id  = jQuery(this).attr("id");
						data.revision_id  = jQuery(this).attr("revision_id");
						//push the object into the array
						neworder.push(data);
					});
					var document_nid  = jQuery("#listing-document").attr("document_nid");
					var product_id  = jQuery("#listing-document").attr("product_id");
					var listing_nid  = jQuery("#listing-document").attr("listing_nid");
					// POST to server using $.post or $.ajax
					jQuery.post('/listing-photos/sortable', {"neworder":neworder, "document_nid":document_nid}, function(response) {
						if(response){
							jQuery('div.ajax-progress-throbber').remove();
							//alert("Saved the new listing document order!");
							window.location.href = '/lc/product/website/'+listing_nid+'/'+product_id+'#document';
							window.location.reload(true);
						}
					});
				}
				else {
					jQuery('div.ajax-progress-throbber').remove();
					alert("This listing currently has no document!");
				}
			});
			//make Photo dragabble at lc page
			/*jQuery('#lc-photos-drag-wrapper').sortable({
				appendTo: "body",
				helper: "clone",
				scroll: false,
				containment: ".lc-photos-body",
				cursorAt: {left: 0, top: 0},
			});
			
			jQuery('#lc-photos-drag-wrapper').disableSelection();*/
      
      var getUrlParameter = function getUrlParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
              sURLVariables = sPageURL.split('&'),
              sParameterName,
              i;

          for (i = 0; i < sURLVariables.length; i++) {
              sParameterName = sURLVariables[i].split('=');

              if (sParameterName[0] === sParam) {
                  return sParameterName[1] === undefined ? true : sParameterName[1];
              }
          }
      };
/* 			jQuery('#website-options-save').click(function(e) {
				e.preventDefault();
        var geturl = window.location.href;
        var get_design_id = geturl.split("=");
        var get_nid_id = geturl.split("/");
        
        var website_lnid = get_nid_id[6];
        var active_style_section = 'templates'; 
        
        var website_style = getUrlParameter('website_style');
        var cds_id = getUrlParameter('cds_id');        
        
        if(active_style_section == 'templates'){
          jQuery.post('/update-lc-website-settings', {"lnid":website_lnid, "design_id":website_style, "active_section":active_style_section}, function(response) {
            // if(response){
              // jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
            // }
          });
        }
          
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var active_section = jQuery('.options-title.active').attr('active-section');
				if(active_section != ''){
					if(active_section == 'photos'){
						var neworder = new Array();
						if(jQuery('li.lc-photos-draggable').length > 0) {
							jQuery('li.lc-photos-draggable').each(function() {
								//get the id
								var data = {};
								data.id  = jQuery(this).attr("id");
								data.revision_id  = jQuery(this).attr("revision_id");
								//push the object into the array
								neworder.push(data);
							});
							var photo_nid  = jQuery("#lc-photos-drag-wrapper").attr("photo_nid");
							// POST to server using $.post or $.ajax
							jQuery.post('/listing-photos/sortable', {"neworder":neworder, "photo_nid":photo_nid}, function(response) {
								if(response){
									jQuery('div.ajax-progress-throbber').remove();
									//alert("Saved the new listing photos order!");
									jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
								}
							});
						}
						else {
							jQuery('div.ajax-progress-throbber').remove();
							alert("This listing currently has no photos!");
						}
					}
					else if(active_section == 'music'){
						var mp3 = '';
						if(jQuery("#background_music").is(':checked')) {
							if($("#listing-musics input[type='radio']").is(":checked")) {
								mp3 = jQuery("#listing-musics input[type='radio']:checked").val();
							}
							else {
								alert('Please select any audio.');
								jQuery('div.ajax-progress-throbber').remove();
								return false;
							}
						}
						var lnid = jQuery('.web-page-design').attr('lnid');
						jQuery.post('/update-lc-website-settings', {"lnid":website_lnid, "mp3":mp3, "active_section":active_section}, function(response) {
							if(response){
								console.log(response);
								jQuery('div.ajax-progress-throbber').remove();
								//alert("Saved the new listing photos order!");
								jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
							}
						});
					}
					else if(active_section == 'map'){
						var lnid = jQuery('.web-page-design').attr('lnid');
						var lat = jQuery('.geolocation-lat input').val();
						var lng = jQuery('.geolocation-lng input').val();
						console.log("lnid = "+lnid+" lat = "+lat+" lng"+lng);
						jQuery.post('/update-lc-website-settings', {"lnid":website_lnid, "lat":lat, "lng":lng, "active_section":active_section}, function(response) {
							if(response){
								//console.log(response);
								jQuery('div.ajax-progress-throbber').remove();
								//alert("Saved the new listing photos order!");
								jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
							}
						});
					}
					else if(active_section == 'open-house'){
						//var lnid = jQuery('.lc-webpage-openhouse').attr('lnid');
             var geturl = window.location.href;
             var get_nid_id = geturl.split("/");
             var lnid = get_nid_id[6];
						alert(lnid);
					}
					else {
						jQuery('div.ajax-progress-throbber').remove();
					}
				}
			}); */
			//Update LC web page publish/un-publish
			jQuery("#publish-site").click(function() {
        jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
        var geturl = window.location.href;
        var get_nid_id = geturl.split("/");
        var lnid = get_nid_id[6];
				var website_style = getUrlParameter('website_style');
				var action = 'remove';
				if(jQuery(this).is(':checked')) {
					action = 'add';
				}
				jQuery.post('/update-lc-website-settings', {"lnid":lnid, "active_section":"publish_site", "action":action, "website_style" : website_style}, function(response) {
					if(response){
						//console.log('Photo hide from Gallery');
            jQuery('div.ajax-progress-throbber').remove();
						//jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
					}
				});
			});
			//Update LC Photos donot display
			jQuery(".donot-display").click(function() {
				var item_id = jQuery(this).attr('item_id');
        var geturl = window.location.href;
        var get_nid_id = geturl.split("/");
        var lnid = get_nid_id[6];
				var website_style = getUrlParameter('website_style');
				var action = 'remove';
				if(jQuery(this).is(':checked')) {
					action = 'add';
				}
				if(item_id){
					jQuery.post('/update-lc-website-settings', {"lnid":lnid, "active_section":"photo_hide", "item_id":item_id, "action":action, "website_style" : website_style}, function(response) {
						if(response){
							console.log('Photo hide from Gallery');
							//jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
						}
					});
				}
			});
			//Update LC Photos cation
			jQuery(".lc-photo-add-caption").blur(function() {
				var item_id = jQuery(this).attr('item_id');
				var revision_id = jQuery(this).attr('revision_id');
				var caption = jQuery(this).val();
				if(caption != ''){
					jQuery.post('/lc-photos-caption', {"item_id":item_id, "revision_id":revision_id, "caption":caption}, function(response) {
						if(response){
							console.log('Caption updated');
						}
					});
				}
			});
			jQuery('.update-lc-webpage-url').click(function(e){
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var urli = jQuery('#web_page_url').val();
				//url.replace(/\s+/g, '-').toLowerCase();
				var url = urli.replace(/[!@#$%^&*(<)>+_\s]/g, '-');
				if(url != '') {
        var geturl = window.location.href;
        var get_nid_id = geturl.split("/");
        var lnid = get_nid_id[6];
				var website_style = getUrlParameter('website_style');				
					jQuery.post('/update-lc-website-settings', {"lnid":lnid, "url":url, "active_section":"update_url", "website_style" : website_style}, function(response) {
						if(response){
							jQuery('.webpageurl').text(url)
							jQuery('.action-webpage-url').hide();
							jQuery('.edit-web-page-url').show();
							jQuery('.webpageurl').show();
							jQuery('div.ajax-progress-throbber').remove();
						}
					});
				}
				else {
					jQuery('div.ajax-progress-throbber').remove();
				}
			});
			var currentRequest = null;
			// custom Ajax for Document Save button
			jQuery("#lc-website-document-save").click(function(){ 
				var url = window.location.href;
				var stac = url.split('/');
				//console.log(stac[6]);
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				 currentRequest = jQuery.ajax({
				 url: "/lc-document-save",
				 type: "POST",
				 data:{ "id":stac[6]},
					 success:function(data){
						  jQuery('#block-lc-website-lc-website-documents .manage-listing-photos').remove();
						  jQuery('#block-lc-website-lc-website-documents .content').append(data);
						  jQuery('div.ajax-progress-throbber').remove();
						  jQuery('#quicktabs-tab-document_sub_tab-0').trigger('click');
					 }
				});

		   });
			function getSearchParams(k){
				 var p={};
				 location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
				 return k?p[k]:p;
			}
			jQuery(".lc-website-document-upload").click(function(e){
				var geturl = jQuery(this).attr('id');
				var url = geturl;
				var exp_data = geturl.split('/');
				jQuery('#block-lc-website-lc-website-documents .manage-listing-photos').css('display','none');
				 jQuery('#block-lc-website-lc-website-documents #website-doc-report').css('display','none');
				 jQuery('#block-lc-website-lc-website-documents #website-doc-upload').css('display','block');
				 //jQuery('.website-doc-upload').load('<iframe src= ' + geturl +' frameborder="0" scrolling="no" id="myFrameupload"></iframe>');
				 jQuery('#website-doc-upload').html('<iframe src= '+geturl+' frameborder="0" scrolling="no" id="myFrameupload" height="400px" width="100%"></iframe><div class="lc-website-button"><span class="lc-website-return-dashboard returntodoctab"><a style="margin-right: 6px;" href="javascript:void(0);" id="returntodoctab">Return to Document</a></span><span class="lc-website-return-dashboard"><a style="margin-right: 6px;"href="/lc/dashboard/'+exp_data[3]+'">Return to dashboard</a></span><span class="lc-website-save"><a href="javascript:void(0);" id="embed_code_downloaded">Save</a></span></div>' );	
			});
			jQuery(".lc-website-document-report").click(function(e){
				var geturl = jQuery(this).attr('id');
				var url = geturl;
				var exp_data = geturl.split('/');
				jQuery('#block-lc-website-lc-website-documents .manage-listing-photos').css('display','none');
				 jQuery('#block-lc-website-lc-website-documents #website-doc-upload').css('display','none');
				 jQuery('#block-lc-website-lc-website-documents #website-doc-report').css('display','block');
				 //jQuery('.website-doc-upload').load('<iframe src= ' + geturl +' frameborder="0" scrolling="no" id="myFrameupload"></iframe>');
				 jQuery('#website-doc-report').html('<iframe src= '+geturl+' frameborder="0" scrolling="no" id="myFramereport" height="400px" width="100%"></iframe><div class="lc-website-button"><span class="lc-website-return-dashboard returntodoctab"><a style="margin-right: 6px;" href="javascript:void(0);" id="returntodoctab">Return to Document</a></span><span class="lc-website-return-dashboard"><a style="margin-right: 6px;"href="/lc/dashboard/'+exp_data[3]+'">Return to dashboard</a></span><span class="lc-website-save"><a href="javascript:void(0);" id="embed_code_downloaded">Save</a></span></div>' );	
			});
			
			jQuery(document).on("click", '.returntodoctab', function(event) {
					var lnid = jQuery('.website-options').attr('lnid');
					window.location.href = '/lc/product/website/'+lnid+'#document';
					window.location.reload(true);
				//jQuery('#quicktabs-tab-document_sub_tab-0').trigger('click');
			});
			jQuery(document).on("click", '#quicktabs-tab-lc_property_website-1', function(event) {				
				jQuery('#quicktabs-tab-document_sub_tab-0').trigger('click');
			});
			
			
			/*jQuery('.web-edit-doc').click(function(e) {
				e.preventDefault();			
				var geturl = jQuery(this).attr('href');	
				//window.open(geturl, "_blank", "scrollbars=1,height=600,width=800");
				//window.open(geturl,"_self");
				window.location.href =geturl ;
				//window.location.reload(true);
			});
			*/
			
			
		/*		
			jQuery(".web-edit-doc").click(function(e){
				e.preventDefault();
				var geturl = jQuery(this).attr('href');
				var url = geturl;
				jQuery('#web_doc_image_popup').bPopup({
					content: 'iframe', //'ajax', 'iframe' or 'image'
					contentContainer: '.web_doc_image_popup_area',
					loadUrl: url,
					follow: [false, false],
					position: [442, 119],
					positionStyle: 'fixed',
					iframeAttr: 'scrolling="yes"',
					onOpen: function() {
						jQuery('.b-close span').html("X");
						//jQuery('.web_doc_image_popup_area').append('<div class="loading-image"><img src="/sites/all/modules/custom/modal_node_display/images/dim-loading.gif"/></div>').html();
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.web_doc_image_popup_area').empty();
						jQuery('.b-close span').empty();
						jQuery('.button').hide();
						jQuery('#quicktabs-tab-lc_property_website-1').trigger('click');
					}
				}, function(){
					//jQuery('.loading-image').hide();
					jQuery("#web_doc_image_popup iframe").css('width', "475px");
					jQuery("#web_doc_image_popup iframe").css('height', "425px");
				});
			}); */
			jQuery('.lc-document-web-edit #edit-cancel').click(function(e){
				parent.closeDBPopup();
				jQuery('#quicktabs-tab-document_sub_tab-1').trigger('click');
				
			});
			jQuery('.lc-document-web-edit #field-collection-item-delete-confirm #edit-submit').click(function(e){
				setTimeout(function() {
					parent.closeDBPopup();
				},3000);
					var url = "https://dev.cbone.me/lc/product/website/152247";
					//alert(url);
					window.location.href = url;				
			});
			//jQuery('.lc-document-web-edit .lc_product_web_doc_submit').click(function(e){
			jQuery('.lc-document-web-edit #field-collection-item-form #edit-submit').click(function(e){
				
				//e.preventDefault();		
					url =getUrlParameter('destination');
					window.location.href = '/'+url+'#document';			
			});
			
			
			/*jQuery('#lc-document-photos-upload-form #edit-submit--3').click(function(e){
				alert('clicked');
				var lnid = jQuery('.website-options').attr('lnid');
				window.location.href = '/lc/product/website/'+lnid+'#document';
				window.location.reload(true);				
				jQuery('#quicktabs-tab-lc_property_website-1').trigger('click');
			});*/
			//TO open Delete URL in iframe
			/*jQuery(".web-delete-upload-doc").click(function(e){
				e.preventDefault();
				var geturl = jQuery(this).attr('id');
				var url = geturl;
				jQuery('#web_doc_image_popup').bPopup({
					content: 'iframe', //'ajax', 'iframe' or 'image'
					contentContainer: '.web_doc_image_popup_area',
					loadUrl: url,
					follow: [false, false],
					position: [442, 119],
					positionStyle: 'fixed',
					iframeAttr: 'scrolling="yes"',
					onOpen: function() {
						jQuery('.b-close span').html("X");
						//jQuery('.web_doc_image_popup_area').append('<div class="loading-image"><img src="/sites/all/modules/custom/modal_node_display/images/dim-loading.gif"/></div>').html();
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.web_doc_image_popup_area').empty();
						jQuery('.b-close span').empty();
						jQuery('.button').hide();
					}
				}, function(){
					//jQuery('.loading-image').hide();
					jQuery("#web_doc_image_popup iframe").css('width', "475px");
					jQuery("#web_doc_image_popup iframe").css('height', "425px");
				});
			});*/
			//TO open Delete URL in iframe
			//jQuery("ul.lc-documents-menu li").click(function(e){
			jQuery(".lc-website-content-data").on('click', "ul.lc-documents-menu li", function(e){
				e.preventDefault();
				var menu_section = jQuery(this).attr('menu-section');
				jQuery("ul.lc-documents-menu li").removeClass('active');
				jQuery(this).addClass('active');
				jQuery(".lc-website-documents-body > div").hide();
				jQuery("."+menu_section).show();
			});
			jQuery("#lc-document-photos-upload-form .plupload_button.plupload_add").attr("title", "");
			
			//delete LC document
			jQuery('.web-delete-upload-doc').click(function(e) {
				e.preventDefault();			
				var geturl = jQuery(this).attr('id');					
				var url = geturl.split('destination=');
				var url2 = url[0].split('/');
				var url_sec = url[1].split('/');
				var document_nid  = jQuery(".document-draggable").attr("id");					
				var data  = jQuery.makeArray(url2[3]);
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var msg = "Are you sure you wish to permanently delete the selected document(s)?  This action cannot be undone.";
				doDeleteLcDocumentConfirm(msg, function yes(){
					jQuery.post('/lc-document/delete', {"data":data, "document_nid":document_nid}, function(response) {
						if(response == 'Done'){
							jQuery('div.ajax-progress-throbber').remove();
							jQuery('li#'+data).remove();
							alert("Document has been deleted successfully.");
							//location.reload();
						}
					});
					/*jQuery.ajax({
						url: "/lc-document/delete",
						data:{"data":data, "document_nid":document_nid},
						type: "POST",
						success:function(data){
							var msg = "Document has been deleted.";
								doDeleteLcConcConfirm(msg, function yes(){
									location.reload();
								}, function no(){
									jQuery('div.ajax-progress-throbber').remove();
								return false;
							});
							
						}
					});*/
				}, function no(){
					jQuery('div.ajax-progress-throbber').remove();
				return false;
				});
			});
      jQuery(".lc-website-content-tab ul li").click(function(e){
        e.preventDefault();
        //var prev_tab = jQuery(".lc-website-content-tab ul li.active").find("a").text().toLowerCase();
        jQuery(".lc-website-content-tab ul li").removeClass('active');
        jQuery(this).addClass('active');
        var tab_body = jQuery(this).find("a").text().toLowerCase();
        jQuery(".lc-website-content-data > div").hide();
        jQuery(".lc-website-content-data div.lc-website-content-"+tab_body).show();
        if(jQuery('div.lc-website-content-'+tab_body).is(':empty') ) {
          jQuery('div.lc-website-content-'+tab_body).html('<div class="ajax-loading"><img src="/sites/all/modules/custom/lcmc_dashboard/images/ajaxloading.gif" style="width: 170px;"></div>');
          lc_webiste_tab(tab_body);
        }
      });
      
      //single page scrolling for section and multi-section pages
      jQuery(".view-id-icon_grids.view-display-id-block_2 .views-field-title, #block-cbone-admin-multi-section-main-block .sub-section-icons li").click(function(event){
        var section = jQuery(this).find('a').attr('href');
        if(section.indexOf('#') == 0){
          event.preventDefault();
          jQuery('html, body').animate({
            scrollTop: jQuery(section).offset().top - 70});
        }  
      });
      //Initialize google map on website setting page.
      if(jQuery('#lcGoogleMap').length > 0) {
        google.maps.event.addDomListener(window, 'load', lc_map_initialize);
        jQuery("#apply_lat_lng").click(function(event){
          event.preventDefault();
          lc_map_initialize();
        });
      }
		}
	};
})(jQuery, Drupal, this, this.document);
function closeDBPopup() {
	jQuery("#web_doc_image_popup").bPopup().close();
	var lnid = jQuery('.website-options').attr('lnid');
	var product_id = jQuery('.website-options').attr('product_id');
	window.location.href = '/lc/product/website/'+lnid+'/'+product_id+'#document';
	window.location.reload(true);
}
/**
 * jQuery Call after ajax callback
 * from open house form submit.
 **/
/* (function ($, Drupal, window, document, undefined) {
	Drupal.ajax.prototype.commands.afterAjaxCallbackLCWebsite = function(ajax, response, status) {
		jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
	};
})(jQuery, Drupal, this, this.document); */
function doDeleteLcDocumentConfirm(msg, yesFn, noFn) {
   var confirmBox = jQuery("#lcconcconfirmBox2");
   var confirmBoxCover = jQuery(".lcconcconfirmBox2-group .cover");
   confirmBox.find(".message").text(msg);
   confirmBox.find(".yes,.no").unbind().click(function()
   {
		confirmBox.hide();
		confirmBoxCover.hide();
   });
   confirmBox.find(".yes").click(yesFn);
   confirmBox.find(".no").click(noFn);
   confirmBox.show();
   confirmBoxCover.show();
}

function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
//fuction to call on save ajax for Property website right site.
(function($) {
	$.fn.properyWebsiteLeftObtionSave = function(data_ajax) {
		var geturl = window.location.href;
		var get_design_id = geturl.split("=");
		var get_nid_id = geturl.split("/");
		
		var website_lnid = get_nid_id[6];
		var active_style_section = 'templates'; 
		
		var website_style = getUrlParameter('website_style');
		var cds_id = getUrlParameter('cds_id');        

		jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
		var active_section = jQuery('.options-title.active').attr('active-section');
		if(active_section != ''){
			if(active_section == 'photos'){
				var neworder = new Array();
				if(jQuery('li.lc-photos-draggable').length > 0) {
					jQuery('li.lc-photos-draggable').each(function() {
						//get the id
						var data = {};
						data.id  = jQuery(this).attr("id");
						//data.revision_id  = jQuery(this).attr("revision_id");
						//push the object into the array
						neworder.push(data);
					});
					var photo_nid  = jQuery("#lc-photos-drag-wrapper").attr("photo_nid");
					//console.log('neworder'+JSON.stringify(neworder));
					//console.log('photo_nid'+photo_nid);
					// POST to server using $.post or $.ajax
					jQuery.post('/listing-photos/sorting', {"lnid":website_lnid, "neworder":neworder, "photo_nid":photo_nid, "website_style" : website_style}, function(response) {
						if(response){
							//jQuery('div.ajax-progress-throbber').remove();
							//alert("Saved the new listing photos order!");
							//jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
							if(data_ajax != 'preview'){
								window.location.reload(true);
							}
						}
					});
				}
				else {
					//jQuery('div.ajax-progress-throbber').remove();
					alert("This listing currently has no photos!");
				}
			}
			else if(active_section == 'music'){
				var mp3 = '';
				if(jQuery("#background_music").is(':checked')) {
					if($("#listing-musics input[type='radio']").is(":checked")) {
						mp3 = jQuery("#listing-musics input[type='radio']:checked").val();
					}
					else {
						alert('Please select any audio.');
						//jQuery('div.ajax-progress-throbber').remove();
						return false;
					}
				}
				var lnid = jQuery('.web-page-design').attr('lnid');
				var website_style = getUrlParameter('website_style');
				jQuery.post('/update-lc-website-settings', {"lnid":website_lnid, "mp3":mp3, "active_section":active_section, "website_style" : website_style}, function(response) {
					if(response){
						//console.log(response);
						//jQuery('div.ajax-progress-throbber').remove();
						//alert("Saved the new listing photos order!");
						//jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
							if(data_ajax != 'preview'){
								window.location.reload(true);
							}
					}
				});
			}
			else if(active_section == 'map'){
				var lnid = jQuery('.web-page-design').attr('lnid');
				var lat = jQuery('.geolocation-lat input').val();
				var lng = jQuery('.geolocation-lng input').val();
				console.log("lnid = "+lnid+" lat = "+lat+" lng"+lng);
				jQuery.post('/update-lc-website-settings', {"lnid":website_lnid, "lat":lat, "lng":lng, "active_section":active_section}, function(response) {
					if(response){
						//console.log(response);
						//jQuery('div.ajax-progress-throbber').remove();
						//alert("Saved the new listing photos order!");
						//jQuery('iframe').attr( 'src', function ( i, val ) { return val; });
							if(data_ajax != 'preview'){
								window.location.reload(true);
							}
					}
				});
			}
		}
		if(data_ajax == 'preview'){
			var base_url = window.location.origin;
			window.open(base_url+'/node/'+website_lnid+'?website_style='+website_style, "_blank");
		}
	};
})(jQuery);

/**
 * Callback function lc_webiste_tab()
 * to displayed the website edit page tab data.
 **/
function lc_webiste_tab(tab) {
  var href = '/cbone/website/content/'+tab
  jQuery('div.lc-website-content-'+tab).load(href);
	/*jQuery.ajax({
		data: '',
		url: href,
		type: "POST",
		beforeSend: function() {
		},
		success: function(data) {
      jQuery('div.lc-website-content-'+tab).html(data);
			jQuery('div.ajax-progress-throbber').remove();
		}
	});*/
	return false;
}
/**
 * callback function lc_map_initialize
 * to set the google map at lc website setting page
 **/
function lc_map_initialize() {
  var lc_lat = jQuery('#lc_lat').val();
  var lc_lng = jQuery('#lc_lng').val();
  var myLatlng = new google.maps.LatLng(lc_lat, lc_lng);
  var mapProp = {
    center: myLatlng,
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP      
  };
  var map = new google.maps.Map(document.getElementById("lcGoogleMap"), mapProp);
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    //title: 'Hello World!',
    draggable:true  
  });
  // map click event
  google.maps.event.addListener(map, 'click', function(event) {
    document.getElementById('lc_lat').value = event.latLng.lat();
    jQuery('.lc-lat-key').text(event.latLng.lat());
    document.getElementById('lc_lng').value = event.latLng.lng();
    jQuery('.lc-lng-key').text(event.latLng.lng());
    marker.setPosition(event.latLng);
  });
  // marker drag event
  google.maps.event.addListener(marker, 'drag', function(event) {
    document.getElementById('lc_lat').value = event.latLng.lat();
    jQuery('.lc-lat-key').text(event.latLng.lat());
    document.getElementById('lc_lng').value = event.latLng.lng();
    jQuery('.lc-lng-key').text(event.latLng.lng());
  });

  //marker drag event end
  google.maps.event.addListener(marker,'dragend',function(event) {
    document.getElementById('lc_lat').value = event.latLng.lat();
    jQuery('.lc-lat-key').text(event.latLng.lat());
    document.getElementById('lc_lng').value = event.latLng.lng();
    jQuery('.lc-lng-key').text(event.latLng.lng());
  });
}

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the customization form template.
 *
 */
(function($, Drupal, window, document, undefined) {
  Drupal.behaviors.lc_agent_popup = {
    attach: function(context, settings) {
			jQuery('.lc-office-agent-popup').click(function(e){
				e.preventDefault();
				var url = jQuery(this).attr('href');
				var slide = 'slideBack';
				jQuery("#popup").css('width', "350px");
				jQuery("#popup").css('height', "350px");
				jQuery('#popup').bPopup({
					content:'iframe', //'ajax', 'iframe' or 'image'
					contentContainer:'.c_area',
					loadUrl:url, //Uses jQuery.load()
					speed: 650,
					amsl: 0,
					transition: slide,
					positionStyle: 'fixed',
					iframeAttr: ('scrolling="yes" frameborder="0"'),
					onOpen: function() {
						jQuery('#popup-load-img').show();
						jQuery('.c_area').append('<div class="loading-image"><img src="/sites/all/modules/custom/modal_node_display/images/dim-loading.gif"/></div>').html();
						jQuery('.b-close span').html("[Close]");
					},
					onClose: function() {
						jQuery('.c_area').empty();
						jQuery('.b-close span').empty();
					}
				}, function(){
					setTimeout( "jQuery('.loading-image').hide();", 2000);
					jQuery("#popup iframe").css('width', "340px");
					jQuery("#popup iframe").css('height', "290px");
				});
			});
			jQuery("#lc-office-dashboard-search-form #edit-user-office").change(function() {
				var id = jQuery(this).val();
				if(id != '_none'){
          var office1 = id.split('[nid:');
          var office2 = office1[1].split(']');
          var nid = office2[0];
					location.href = Drupal.settings.basePath + "lc/office-dashboard/"+nid;
				}
			});
    }
  };
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

function cbone_lc_send_to_print(lnid , design_id , product_id){ 
	jQuery.ajax({
	url: "/lc/send-to-print",
	data:{"lnid":lnid, "design_id":design_id, "product_id":product_id},
	type: "POST",
		beforeSend : function(){
			jQuery('.lc-concierge-container').parent().after('<div class="cdswheel"><div>Processing</div></div>');
		},
		success:function(response){
			jQuery('div.cdswheel').remove();
			alert(response);
		}
	});   
} 
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.listing_video = {
		attach: function(context, settings) {
			jQuery('.magnify').click(function(){
				var url = jQuery(this).attr('photo-url');
				jQuery('#image_popup').bPopup({
					content: 'image', //'ajax', 'iframe' or 'image'
					contentContainer: '.image_area',
					loadUrl: url,
					onOpen: function() {
						jQuery('.b-close').html('X');
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.image_area').empty();
						jQuery('.b-close').empty();
						jQuery('.button').hide();
					}
				});
			});
		}
	};
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.my_listing = {
		attach: function(context, settings) {
			jQuery('.pdf_brochure').on('change', function() { 
				if (this.checked) {
					//alert('checked');
				 }
				 else{
					var id = jQuery(this).attr('id');
					if(id.length > 0){
						//alert('hello');
					}
					else{
						var list_id = jQuery(this).attr('name');
						alert('This listing does not currently have a Print and Go brochure available for download. To prepare and assign a brochure return to the "Listing Tools" > "Print and Go" feature to create and/or select a brochure.');
						jQuery(this).attr('checked', true);
						//var base = Drupal.settings.base_url;
						//window.location.replace(base + "/print-and-go/"+ list_id);
					}
				 }
			});
			//Shared lisitng download link at shared page
			jQuery('.shared_listing_download').on('click', function() { 
				
					var id = jQuery(this).attr('id');
					if(id.length > 0){
						//alert('hello');
					}
					else{
						alert('This listing does not currently have a Print and Go brochure available for download. To prepare and assign a brochure return to the "Listing Tools" > "Print and Go" feature to create and/or select a brochure.');
						return false;
					}
			});
			//Shared lisitng link at manage listing page
			jQuery('.share_listing').on('click', function() { 
				
					var id = jQuery(this).attr('id');
					if(id.length > 0){
						//alert('hello');
					}
					else{
						alert('We\'re sorry. Your listing is not quite ready to be shared. Please be sure you have first activated the single property website (via the "website" tile). Then create and select a "Flyer" brochure to be shared.');
						return false;
					}
			});
			//Added by nitesh for share/% page.
			jQuery('.listing-pdf #agent_submit #edit-submit').on('click', function() { 
				
					var id = jQuery(this).attr('brochure_id');
					if(id.length > 0){
						//alert('hello');
					}
					else{
						alert('We\'re sorry. Your listing is not quite ready to be shared. Please be sure you have first activated the single property website (via the "website" tile). Then create and select a "Flyer" brochure to be shared.');
						return false;
					}
			});
			jQuery('.shared_pdf_generation').click(function(e){
				e.preventDefault();
				var user_id = jQuery(this).attr("id");
				var listing_nid = jQuery(this).attr("listing_nid");
				jQuery('#agent-list-remove-form input[name=nid]').val(listing_nid);
				jQuery('#agent-list-remove-form input[name=uid]').val(user_id);
				jQuery("#agent-list-remove-form").submit();
			});
			//Displayed active or Inactive list at my-listings page
			jQuery('.filter-my-list li').click(function(e){
				var className = jQuery(this).attr('status');
				if(className == 'yes') {
					jQuery("#edit-status").val(1).change();
				}
				else {
					jQuery("#edit-status").val(0).change();
				}
			});
			//Displayed edit field for node URL at web page
			jQuery('.edit-web-page-url').click(function(e){
				jQuery('.edit-web-page-url').hide();
				jQuery('.webpageurl').hide();
				jQuery('.action-webpage-url').show();
				jQuery("a.web-listing-link").attr("href", "javascript: void(0)");
			});
			jQuery('.update-web-page-url').click(function(e){
				//jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var urli = jQuery('#web_page_url').val();
				//url.replace(/\s+/g, '-').toLowerCase();
				var url = urli.replace(/[!@#$%^&*(<)>+_\s]/g, '-');
				if(url != '') {
					jQuery('.webpageurl').text(url)
					jQuery('.action-webpage-url').hide();
					jQuery('.edit-web-page-url').show();
					jQuery('.webpageurl').show();
					jQuery('div.ajax-progress-throbber').remove();
				}
				else {
					jQuery('div.ajax-progress-throbber').remove();
				}
			});
			//check any one design at web page
			jQuery('.design-name input').click(function(e){
				jQuery('.design-name input').attr('checked', false);
				jQuery(this).attr('checked', true);
			});
			jQuery('#update-listing-website-settings').click(function(e){
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var url = jQuery('#web_page_url').val();
				var nid = jQuery(this).attr("listing-nid");
				var data = jQuery(":checkbox:checked").map(function() {
					return this.id;
				}).get();
				var mp3 = '';
				if(jQuery("#background_music").is(':checked')) {
					if($("#listing-musics input[type='radio']").is(":checked")) {
						mp3 = jQuery("#listing-musics input[type='radio']:checked").val();
					}
					else {
						alert('Please select any audio.');
						jQuery('div.ajax-progress-throbber').remove();
						return false;
					}
				}
				jQuery.post('/website-settings', {"data":data, "url":url, "nid":nid, "mp3":mp3}, function(response) {
					if(response == 'done'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Your Property Web Page settings have been successfully updated.");
					}
					else if(response == 'not-done') {
						jQuery('div.ajax-progress-throbber').remove();
						alert("Your Property Web Page settings have not been updated.");
					}else if(response == 'alias exists') {
						jQuery('div.ajax-progress-throbber').remove();
						alert("The path you have entered is already in use. Please try another.");
					}
					/*if(response){
						alert(response);
						jQuery('div.ajax-progress-throbber').remove();
					}*/
				});
			});
			//Show hide music list.
			if(jQuery("#background_music").is(':checked')) {
				jQuery('#listing-musics').show();
			}
			else {
				jQuery('#listing-musics').hide();
			}
			jQuery('#background_music').click(function(e){
				jQuery("#listing-musics").toggle(this.checked);
			});
			//Playing audio in web setting page.
			var audioElement = document.createElement('audio');
			var fid = '';
			jQuery(".play").click(function (e) {
				var current_fid = jQuery(this).parent().attr('fid');
				e.preventDefault();
				var url = jQuery(this).attr('url');
				var seconds = audioElement.currentTime;			
				if(fid == '' || fid == current_fid) {
					if(seconds == 0 || audioElement.paused == true){
						fid = current_fid;
						audioElement.setAttribute('src', url);
						audioElement.load();
						audioElement.addEventListener("load", function() { 
						audioElement.play(); 
						}, true);
						jQuery('.playing').hide()
						jQuery(this).find('.playing').show();
						audioElement.play();
					}
					else {
						jQuery('.playing').hide()
						audioElement.pause();
					}
				}
				else if(fid != current_fid) {
					fid = current_fid;
					audioElement.setAttribute('src', url); 
					audioElement.load();
					audioElement.addEventListener("load", function() {
					audioElement.play(); 
					}, true);
					jQuery('.playing').hide()
					jQuery(this).find('.playing').show();
					audioElement.play();
				}
			});
			//Play audio at web listing page
			if(jQuery('a.listing-audio-play').length > 0) {
				var url = jQuery('a.listing-audio-play').attr('url');
        var url_split = url.split('?');
        jQuery('#audio').append(audioElement);
				audioElement.setAttribute('src', url_split[0]);
        audioElement.setAttribute('crossorigin', 'anonymous');
				audioElement.load();
				audioElement.addEventListener("load", function() { 
					audioElement.play(); 
				}, true);
				audioElement.play();
			}
      //Play and pause audio at web listing page
      jQuery('div.property-website').on('click', ".listing-audio-play", function(e){
        e.preventDefault();
        var seconds = audioElement.currentTime;
        if(seconds == 0 || audioElement.paused == true){
          var url = jQuery('a.listing-audio-play').attr('url');
          //var url_split = url.split('d293rdt04oz7au.cloudfront.net');
          var url_split = url.split('?');
          //audioElement.setAttribute('src', 'http://www.homecb.com'+url_split[1]);
          audioElement.setAttribute('src', url_split[0]);
          audioElement.setAttribute('crossorigin', 'anonymous');
          audioElement.load();
          audioElement.addEventListener("load", function() { 
            audioElement.play(); 
          }, true);
          audioElement.play();
        }
        else {
          audioElement.pause();
        }
      });
			//Open Embed code page in modal window
			jQuery("#listing-embed-link").click(function(e){
				e.preventDefault();
				jQuery('#listing-embed-code').bPopup();
			});
			//copy embed code in clipboard
			jQuery("#d_clip_button").click(function(e){
				e.preventDefault();
				var text = jQuery("textarea#web_widgets_0").val();
				window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
			});
			//download embed code as .txt file
			jQuery("#embed_code_download").click(function(e){
				e.preventDefault();
				var code = jQuery("textarea#web_widgets_0").val();
				var base = Drupal.settings.base_url;
				window.location.replace(base + "/embed-code/download?code="+ code);
			});
			jQuery("#listing-modal-back").click(function(e){
				e.preventDefault();
				var bPopup = jQuery("#image_popup").bPopup();
				bPopup.close();
				/*jQuery("#image_popup .b-close span").trigger("click");
				alert('click');*/
			});
			//open LMS Client Report in modal window
			if(jQuery('#client_report_preview').length > 0) {
				jQuery('#client_report_preview').hide();
				setTimeout(function() {
					jQuery('#client_report_preview').trigger('click');
				},10);				
			}
			jQuery("#client_report_preview").click(function(e){
				e.preventDefault();
				var nid = jQuery(this).attr('nid');
				var url = '/node/' + nid;
				jQuery('#image_popup').bPopup({
					content: 'iframe', //'ajax', 'iframe' or 'image'
					contentContainer: '.image_area',
					loadUrl: url,
					follow: [false, false],
					position: [250, 100],
					iframeAttr: 'scrolling="yes"',
					onOpen: function() {
						jQuery('.b-close span').html("X");
						jQuery('.button').show();
					},
					onClose: function() {
						jQuery('.image_area').empty();
						jQuery('.b-close span').empty();
						jQuery('.button').hide();
					}
				}, function(){
					jQuery("#image_popup iframe").css('width', "750px");
					jQuery("#image_popup iframe").css('height', "730px");
				});
			});
			jQuery('#cbone-listing-email-form .form-submit').click(function(e){
				var con = confirm("Are you sure you wish to send this message now?");
				if (con == true) {
					jQuery(this).attr("disabled", "disabled");
					jQuery('form#cbone-listing-email-form').submit();
				} else {
					return false;
				}
			});
			//Import listing photos from "lms_photo_import" table
			jQuery('#import-listing-photos').click(function(e){
				e.preventDefault();
				jQuery(this).after('&nbsp;&nbsp;&nbsp;<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Importing, please wait...</div></div>');
				var nrt_id = jQuery(this).attr("nrt_id");
				var listing_nid = jQuery(this).attr("listing_nid");
				var photo_nid = jQuery(this).attr("photo_nid");
				jQuery.post('/feeds/photo-feed', {"nrt_id":nrt_id, "listing_nid":listing_nid, "photo_nid":photo_nid}, function(response) {
					if(response == 'done'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Photos have imported successfully!");
						location.reload();
					}
				});
			});
			//MyListing Overlay at image
			/*jQuery(".my-listings-part-one").hover(
				function() {
					var mc_status = jQuery(this).find(".mc-order-status .mc-status").text();
					if(mc_status != 'N/A') {
						jQuery(this).find(".mc-order-status").show();
					}
				},
				function() {
					jQuery(this).find(".mc-order-status").hide();
				}
			);*/
			/* MyListing Overlay at image by nitesh */
			jQuery(".my-listings-part-one").mouseenter(function() {
				jQuery(this).find('.mc-order-status').animate({width: 0, marginLeft: 0, fontSize: 0}, {duration: 700});
			});
			jQuery(".my-listings-part-one").mouseleave(function() {
				jQuery(this).find('.mc-order-status').animate({width: 159, marginLeft: 0, fontSize: 14}, {duration: 700});
			});
			//Import listing photos from "lms_photo_import" table
			jQuery('.accept-and-confirm-order #mcc-order-confirm').click(function(e){
				e.preventDefault();
				jQuery('.accept-and-confirm-order .confirm-order').after('&nbsp;&nbsp;&nbsp;<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var listing_nid = jQuery(this).attr("listing-nid");
				var product = jQuery(this).attr("product");
				jQuery.post('/lms-listing/mcc-order-confirm', {"listing_nid":listing_nid, "product":product}, function(response) {
					if(response == 'done'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Order completed successfully!");
						location.reload();
					}
					else if(response == 'accepted'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Order accepted successfully!");
						location.reload();
					}
				});
			});
			//code for /node/add/client page to change state to uppercase
			jQuery("#edit-field-buyer-state-und-0-value, #edit-field-buyer-property-state-und-0-value").bind('keyup blur', function (e) {
				var node = jQuery(this);
				node.val(node.val().replace(/[^a-zA-Z]/g,'')); 
				node.val(node.val().toUpperCase())
			});
			
			//code added by Harinder Singh Maan on 16-09-2016
			jQuery('.marketing-concierge-design-checkbox').click(function(){
				jQuery('#custom-confirm-popup').css('display', 'block');
				jQuery('.manage-listing-photos').css('opacity', '0.1');
			});
			jQuery('.custom-confirm-popup-ok').click(function(){
				jQuery('#custom-confirm-popup').css('display', 'none');
				jQuery('.manage-listing-photos').css('opacity', '1');
				jQuery('.standard-web-page-design-checkbox').prop('checked', true);
				jQuery('.marketing-concierge-design-checkbox').prop('checked', false);
				 return false;
			});
			
			//Placed label in field placeholder in cb_firstlook_property_post_node_form
			jQuery('#cb-firstlook-property-post-node-form').find(".form-type-textfield").each(function(ev) {
				var label = jQuery(this).children("label").text();				
				if(!jQuery(this).find("input[type=text]").val()) {
					if(jQuery.trim(label) == 'URL') {
						label = 'Weblink';
					}
					jQuery(this).find("input[type=text]").attr("placeholder", label);
				}
			});
			//Popup functionality at cb_firstlook_property_post_node_form
			jQuery('#cb-firstlook-property-post-node-form #edit-submit').click(function(e){
				e.preventDefault();
				jQuery('#firstlook-requirements-popup').bPopup();
			});
			jQuery('.firstlook-yes-save').click(function(e){
				e.preventDefault();
				parent.closeBPopup();
				jQuery("#edit-field-first-look-confirmation-und-1").prop('checked', true);
				jQuery("#cb-firstlook-property-post-node-form").submit();
			});
			jQuery('.firstlook-yes-preview').click(function(e){
				e.preventDefault();
				parent.closeBPopup();
				jQuery("#edit-field-first-look-confirmation-und-1").prop('checked', true);
				jQuery("#cb-firstlook-property-post-node-form #edit-preview").click();
			});
			jQuery('.firstlook-no-cancel').click(function(e){
				e.preventDefault();
				parent.closeBPopup();
				jQuery("#edit-field-first-look-confirmation-und-0").prop('checked', true);
			});
			jQuery('#upgrade-marketing-concierge').click(function(e){
				if(!jQuery(this).hasClass('listing_user')){
					e.preventDefault();
					alert('Only the listing agent may order a Marketing Concierge package.');
					return false;
				}
				else {
					var msgalert = '';
					if(jQuery(this).hasClass('less_listing_data')){
						msgalert += 'Details > Marketing Copy is required.\n';
					}
					if(jQuery(this).hasClass('listing_marketing_headline')){
						msgalert += 'Details > Marketing Copy > Listing Marketing Headline is required.\n';
					}
					if(jQuery(this).hasClass('less_listing_photos')){
						msgalert += 'Photos > Minimum of four photos are required.\n';
					}
					if (msgalert != '') {
						var mainmsgalert = "Oops! We see you haven't completed all of the requirements needed to place a Marketing Concierge order. Please complete the following in order to proceed. \n\n"+msgalert
						e.preventDefault();
						alert(mainmsgalert);
						return false;
					}
				}
			});
			jQuery('#edit-field-aam-profile-override-und-1').click(function(e){
				if (this.checked) {
					jQuery('.editable-field').show();
					jQuery('.disabled-field').hide();
				}
				else {
					jQuery('.disabled-field').show();
					jQuery('.editable-field').hide();
				}
			});
		}
	};
})(jQuery, Drupal, this, this.document);



/**
 * Callback function closeBPopup()
 * to close the bpopup after click
 * on cancel button
 **/
function closeBPopup() {
	jQuery("#firstlook-requirements-popup").bPopup().close();
}

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.print_and_go = {
		attach: function(context, settings) {
			//Displayed active or Inactive list at my-listings page
			/*jQuery('input[name=print_and_go]').on('change', function(){
				 jQuery('input[name=print_and_go]').not(this).prop('checked', false);
			});*/
			jQuery('#update-listing-brochures-id').click(function(e) {
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				if(jQuery("input[name='online_marketing']").is(':checked')) { 
					var listing_nid  = jQuery("input[name='online_marketing']:checked").attr("listing_nid");
				}
				else{
					var listing_nid  = jQuery("input[name='online_marketing_shared_listing']:checked").attr("listing_nid");
				}
				
				var mcpdf_nid  = jQuery("input[name='online_marketing']:checked").val();
				var shared_mcpdf_nid  = jQuery("input[name='online_marketing_shared_listing']:checked").val();
				// POST to server using $.post or $.ajax
				jQuery.post('/print-and-go/marketing', {"mcpdf_nid":mcpdf_nid, "listing_nid":listing_nid, "shared_mcpdf_nid":shared_mcpdf_nid}, function(response) {
					if(response){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Saved successfully");
					}
				}); 
			});
		}
	};
})(jQuery, Drupal, this, this.document);


;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */
 
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.web_listing = {
		attach: function(context, settings) {
			  //custom select box for brochure_size field on order page.
      if (jQuery("body").hasClass("page-lc-order-package-options")){
        jQuery("#edit-lc-options-brochure-size, #edit-lc-options-publications, .form-item-lc-options-templates select, #edit-lc-options-paper-type").selectBox();
      }
      //check any one option for paper_type field on order page.
			jQuery('#edit-lc-options-paper-type input').click(function(){
				jQuery('#edit-lc-options-paper-type input').prop('checked', false);
				jQuery(this).prop('checked', true);
			});  
			
			jQuery('.responsive-menus .toggler').click(function(){
				jQuery( ".responsive-menus #rm-removed" ).css('display', 'block');
			});
			jQuery('#rm-removed li a').on('click', function(){
				//jQuery('.toggler').click();
				alert('hiii');
			});
			var numItemsmain = jQuery('.web-listing-contact .listing-third-image').length;
			if(numItemsmain==0){
				jQuery('.agent-detail').css('width', '100%');
			}
			var numItems = jQuery('.agents-detail').length;
			if(numItems==2){
				jQuery('.agents-detail').css('width', '50%');
			}
			if(numItems==1){
				jQuery('.agents-detail').css('width', '100%');
			}
			//Displayed edit field for node URL at web page
			jQuery('ul.photos-gallery li').hide().filter(':lt(8)').show();
			jQuery('.less-photos').hide();
			//jQuery('.more-photos').click(function(e){
      /*jQuery('div.property-website').on('click', ".more-photos", function(e){
				e.preventDefault();
				var val = jQuery(this).attr('value');
				val = parseInt(val) + 1;
				var total = val * 8;
				jQuery('ul.photos-gallery li').filter(':lt('+total+')').show();
				if(jQuery("ul.photos-gallery li").length > total) {
					jQuery(this).attr('value', val);
					jQuery('.less-photos').attr('value', val);
				}
				else {
					jQuery(this).hide();
				}
				if(val != 1) {
					jQuery('.less-photos').show();
				}
			});*/
			
			//jQuery('.less-photos').click(function(e){
      /*jQuery('div.property-website').on('click', ".less-photos", function(e){
				e.preventDefault();
				jQuery('html, body').animate({
					scrollTop: jQuery( jQuery('#gallery') ).offset().top
				}, 900);
				var val = jQuery(this).attr('value');
				var total = 8;
				val = parseInt(val) - 1;				
				if(val == 0){
					total = 8;
					val = 1;
				}
				else {
					total = val * 8;
				}
				jQuery('ul.photos-gallery li').hide().filter(':lt('+total+')').show();
				if(val == 1) {
					jQuery('.less-photos').hide();					
				}
				if(jQuery("ul.photos-gallery li").length > total) {
					jQuery('.more-photos').show();
				}
				jQuery(this).attr('value', val);
				jQuery('.more-photos').attr('value', val);
			});*/
      
      jQuery('div.property-website').on('click', ".more-photos-link a", function(e){
        e.preventDefault();
        var $this = jQuery(this);
        var image_count = jQuery('.more-photos-link').attr('image_count');
        image_count = parseInt(image_count);
        var nid = jQuery('.property-website').attr('nid');
        var brand = jQuery('.property-website').attr('brand');
        if($this.hasClass("more-photos")){
          var val = jQuery($this).attr('value');
          val = parseInt(val);
          var total = val * 8;
          var total_photos = (val + 1) * 8;
          if(image_count > total){
            if(jQuery("ul.photos-gallery li").length > total) {
              total = total + 8;
              val = val + 1;
              jQuery('ul.photos-gallery li').filter(':lt('+total+')').show();
              jQuery($this).attr('value', val);
              jQuery('.less-photos').attr('value', val);
            }
            else {
              jQuery.ajax({
                data: {"val": val, "type": "more-photos"},
                url: '/listing-node-detail/'+nid+'/'+brand,
                type: "POST",
                beforeSend: function() {
                  jQuery($this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
                },
                success: function(data) {
                  jQuery('div.ajax-progress-throbber').remove();
                  jQuery('ul.photos-gallery').append(data);
                  val = val + 1;
                  jQuery($this).attr('value', val);
                  jQuery('.less-photos').attr('value', val);
                  jQuery('.less-photos').show();
                  //jQuery(".group").colorbox({rel:"group", transition:"fade", width:"85%", slideshow:true, slideshowSpeed:5000});
                  //attach lightGallery with more images
                }
              });
            }
            if(total_photos >= image_count){
              jQuery($this).hide();
            }
          }
          else {
            jQuery($this).hide();
          }
          jQuery('.less-photos').show();
        }
        else if($this.hasClass("less-photos")){
          jQuery('html, body').animate({
            scrollTop: jQuery( jQuery('#gallery') ).offset().top
          }, 900);
          var val = jQuery($this).attr('value');
          var total = 8;
          val = parseInt(val) - 1;				
          if(val == 0){
            total = 8;
            val = 1;
          }
          else {
            total = val * 8;
          }
          jQuery('ul.photos-gallery li').hide().filter(':lt('+total+')').show();
          if(val == 1) {
            jQuery('.less-photos').hide();					
          }
          if(jQuery("ul.photos-gallery li").length > total) {
            jQuery('.more-photos').show();
          }
          jQuery($this).attr('value', val);
          jQuery('.more-photos').attr('value', val);
        }
      });
      //Dynamic Listing Photos Gallery
      jQuery('div.property-website').on('click', ".mobile-photo-bottom, .mobile-photo-top", function(e){
        jQuery('#mobile-photo-overlay').hide();
      });
      jQuery('div.property-website').on('click', "a.photo-gallery, .view-more-photo-gallery", function(e){
        e.preventDefault();
        var nid = jQuery('.property-website').attr('nid');
        var design = jQuery('.property-website').attr('design');
        var web_style = (design != '') ? "?website_style="+design : '';
        jQuery("div.property-website .lms-listing-menu").hide();
        jQuery("div.property-website .spw-agent-strip").css('z-index', "1");
        //jQuery("div.property-website .spw-agent-strip").hide();
        var photo_index = jQuery(this).attr('photo_index');
        photo_index = parseInt(photo_index);
        //JS for mobile overlay gallery
        if(jQuery('.photos-gallery .gallery-image-no4').css('display') == 'none') {
          if(jQuery('#mobile-photo-overlay .mobile-photo img').length > 0){
            jQuery('#mobile-photo-overlay').show();
          }
          else {
            jQuery.ajax({
              data: {"type": "photo-gallery"},
              url: '/listing-node-gallery/'+nid+web_style,
              type: "POST",
              beforeSend: function() {
                jQuery('#mobile-photo-overlay').append('<div class="cdswheel"><div>Processing</div></div>');
              },
              success: function(data) {
                jQuery('div.cdswheel').remove();
                var dataStringify = JSON.stringify(data);
                jQuery('#spw-photos-gallery').after('<div class="photo-gallery-images-data" style="display: none !important;">'+dataStringify+'</div>');
                jQuery('#mobile-photo-overlay .mobile-photo').append('<div class="mobile-photo-top"><span aria-hidden="true">&times;</span></div>');
                jQuery.each(data, function( index, value ) {
                  console.log( index + ": " + value.src );
                  jQuery('#mobile-photo-overlay .mobile-photo').append('<img src="'+value.src+'" />');
                });
                jQuery('#mobile-photo-overlay .mobile-photo').append('<div class="mobile-photo-bottom">Close</div>');
                jQuery('#mobile-photo-overlay').show();
              }
            });
          }
        }
        else {
          //JS for tablet/desktop Photo thumbnail gallery
          if(jQuery('.photo-gallery-images-data').length > 0){
            var images_data = jQuery('.photo-gallery-images-data').text();
            var dataStringify = JSON.parse(images_data);
            var $gallery = jQuery('#spw-photos-gallery').lightGallery({
              showThumbByDefault:true,
              addClass:'showThumbByDefault',
              download: false,
              share: false,
              autoplay: false,
              counter: false,
              preload: 2,
              index: photo_index,
              dynamic: true,
              dynamicEl: dataStringify,
              thumbMargin : 10,
              autoplayControls: false,
              fullScreen: false,
              zoom: false,
              actualSize: false,
            });
            $gallery.on('onBeforeOpen.lg',function(event, index, fromTouch, fromThumb){
              jQuery("div.property-website .lms-listing-menu").hide();
              jQuery("div.property-website .spw-agent-strip").css('z-index', "1");
              //jQuery("div.property-website .spw-agent-strip").hide();
            });
            $gallery.on('onAfterAppendSubHtml.lg',function(event, index, fromTouch, fromThumb){
            //$gallery.on('onSlideItemLoad.lg',function(event, index, fromTouch, fromThumb){
              var display_img = jQuery('.lg-img-wrap .lg-image');
              var img_width = display_img.width();
              if(img_width != 0){
                //var display_img_width = img_width - 30;
                jQuery('.lg-sub-html .photo_caption').css({'width':img_width+'px'});
              }
              if(jQuery('#display_img_width').length == 0){
                jQuery('#spw-photos-gallery').append('<input type="hidden" id="display_img_width" value="'+img_width+'">');
              }
              else {
                jQuery('#display_img_width').val(img_width);
              }
            });
            $gallery.on('onCloseAfter.lg',function(event, index, fromTouch, fromThumb){
              jQuery("div.property-website .lms-listing-menu").show();
              jQuery("div.property-website .spw-agent-strip").css('z-index', "9999");
              //jQuery("div.property-website .spw-agent-strip").show();
              //try{$gallery.data('lightGallery').destroy(true);}catch(ex){};
            });
          }
          else {
            jQuery.ajax({
              data: {"type": "photo-gallery"},
              url: '/listing-node-gallery/'+nid+web_style,
              type: "POST",
              beforeSend: function() {
                jQuery('.gallery-head').after('<div class="cdswheel"><div>Processing</div></div>');
              },
              success: function(data) {
                jQuery('div.cdswheel').remove();
                var dataStringify = JSON.stringify(data);
                jQuery('#spw-photos-gallery').after('<div class="photo-gallery-images-data" style="display: none !important;">'+dataStringify+'</div>');
                var $gallery = jQuery('#spw-photos-gallery').lightGallery({
                  showThumbByDefault:true,
                  addClass:'showThumbByDefault',
                  download: false,
                  share: false,
                  autoplay: false,
                  counter: false,
                  preload: 2,
                  index: photo_index,
                  dynamic: true,
                  dynamicEl: data,
                  thumbMargin : 10,
                  autoplayControls: false,
                  fullScreen: false,
                  zoom: false,
                  actualSize: false,
                });
                $gallery.on('onBeforeOpen.lg',function(event, index, fromTouch, fromThumb){
                  jQuery("div.property-website .lms-listing-menu").hide();
                  jQuery("div.property-website .spw-agent-strip").css('z-index', "1");
                  //jQuery("div.property-website .spw-agent-strip").hide();
                });
                $gallery.on('onAfterAppendSubHtml.lg',function(event, index, fromTouch, fromThumb){
                //$gallery.on('onSlideItemLoad.lg',function(event, index, fromTouch, fromThumb){
                  var display_img = jQuery('.lg-img-wrap .lg-image');
                  var img_width = display_img.width();
                  if(img_width != 0){
                    //var display_img_width = img_width - 30;
                    jQuery('.lg-sub-html .photo_caption').css({'width':img_width+'px'});
                  }
                  if(jQuery('#display_img_width').length == 0){
                    jQuery('#spw-photos-gallery').append('<input type="hidden" id="display_img_width" value="'+img_width+'">');
                  }
                  else {
                    jQuery('#display_img_width').val(img_width);
                  }
                });
                $gallery.on('onCloseAfter.lg',function(event, index, fromTouch, fromThumb){
                  jQuery("div.property-website .lms-listing-menu").show();
                  jQuery("div.property-website .spw-agent-strip").css('z-index', "9999");
                  //jQuery("div.property-website .spw-agent-strip").show();
                  //try{$gallery.data('lightGallery').destroy(true);}catch(ex){};
                });
              }
            });
          }
        }
      });
      jQuery( window ).resize(function() {
        if(jQuery('#display_img_width').length){
          var pre_img_width = parseInt(jQuery('#display_img_width').val());
          var display_img = jQuery('.lg-img-wrap .lg-image');
          var img_width = display_img.width();
          if(img_width != pre_img_width){
            //var display_img_width = img_width - 30;
            jQuery('.lg-sub-html .photo_caption').css({'width':img_width+'px'});
            jQuery('#display_img_width').val(img_width);
          }
        }
      });

			
			//show hide plus sign in image gallery at features page
			/*jQuery("ul.photos-gallery li").mouseenter(function(){
				jQuery(this).find('.plus-image').show();
			});
			jQuery("ul.photos-gallery li").mouseleave(function(){
				jQuery(this).find('.plus-image').hide();
			});*/
			//show hide the social plugin at image
			jQuery("#image_popup").mouseenter(function(){
				jQuery('.image-share-div.image').show();
			});
			jQuery("#image_popup").mouseleave(function(){
				jQuery('.image-share-div.image').hide();
			});
			//Open video in modal popup
			jQuery(".listing-video").click(function(e){
				e.preventDefault();
				jQuery('.listing-youtube').bPopup();
			});
			//open document download form
			//jQuery("#download-document, #contact-agent").click(function(e){
      jQuery('div.property-website').on('click', ".download-document", function(e){
        if(!jQuery(this).hasClass("property-flyer")){
          e.preventDefault();
          var url = jQuery(this).attr('href');
          //alert(url);
          jQuery("#image_popup").css('width', "800px");
          jQuery("#image_popup").css('height', "750px");
          jQuery('#image_popup').bPopup({
            content: 'iframe', //'ajax', 'iframe' or 'image'
            contentContainer: '.image_area',
            loadUrl: url,
            speed: 650,
            amsl: 0,
            //transition: 'slideDown',
            iframeAttr: ('scrolling="yes" frameborder="0"'),
            onOpen: function() {
              jQuery('.b-close span').html("&times;");
              jQuery(".button").css({'width': "100%", 'text-align': "right", 'color': "#fff", 'font-weight': "normal", 'font-size': "45px", 'border-radius': "0px", 'display': "inline"});
              jQuery('.button').show();
              jQuery("div.property-website .lms-listing-menu").hide();
              jQuery("div.property-website .spw-agent-strip").hide();
            },
            onClose: function() {
              jQuery('.image_area').empty();
              jQuery('.b-close span').empty();
              jQuery('.button').hide();
              jQuery("div.property-website .lms-listing-menu").show();
              jQuery("div.property-website .spw-agent-strip").show();
            }
          }, function(){
            jQuery("#image_popup iframe").css('width', "800px");
            jQuery("#image_popup iframe").css('height', "750px");
          });
        }
			});
			//Open video in modal popup
			//jQuery(".open-house").click(function(e){
			jQuery('div.property-website').on('click', '.open-house', function(e){
				e.preventDefault();
				//jQuery('.open-house-dates').bPopup();
        jQuery(".directions-links .blackoverlay").show();
        jQuery(".open-house-dates-modal").show();
			});
      jQuery('div.property-website').on('click', '.directions-links .blackoverlay', function(e){
        jQuery(".directions-links .blackoverlay").hide();
        jQuery(".open-house-dates-modal").hide();
			});
			jQuery('div.property-website').on('click', '.open-house-link', function(e){
				e.preventDefault();
        if(jQuery('.open-house-dates-modal').css('display') == 'none') {
          jQuery( ".open-house-link span").removeClass('open-arrow');
          jQuery( ".open-house-link span").addClass('close-arrow');
          jQuery(".open-house-dates-modal").show();
        }
        else {
          jQuery( ".open-house-link span").addClass('open-arrow');
          jQuery( ".open-house-link span").removeClass('close-arrow');
          jQuery(".open-house-dates-modal").hide();
        }
			});
			jQuery('div.property-website').on('click', '.house-address-direction a', function(e){
        if(jQuery('.open-house-dates-modal').css('display') != 'none') {
          jQuery( ".open-house-link span").addClass('open-arrow');
          jQuery( ".open-house-link span").removeClass('close-arrow');
          jQuery(".open-house-dates-modal").hide();
        }
			});
      //close contact agent modal
      jQuery('div.property-website').on('click', '#contact_agent_modal .blackoverlay', function(e){
        contact_agent_modal_hide();
			});
			//Change opacity of header menu
			jQuery(window).scroll(function() {
				if (jQuery(this).scrollTop() > 5) {
					//jQuery( ".lms-listing-menu" ).css('opacity', '1');
          jQuery( ".lms-listing-menu" ).addClass('scrolled');
					jQuery( ".lms-listing-menu" ).css('background', 'rgba(255, 255, 255, 255)');
					jQuery(".beforescroll" ).css('display', 'none');
					jQuery(".afterscroll" ).css('display', 'inline-block');
          jQuery( ".node-type-lms-listing .toggle-menu svg" ).css('fill', '#2A2F34');
				} else {
					//console.log('there');
					//jQuery( ".lms-listing-menu" ).css('opacity', '.8');
          jQuery( ".lms-listing-menu" ).removeClass('scrolled');
					jQuery( ".lms-listing-menu" ).css('background', 'rgba(0, 0, 0, 0)');
					//jQuery( ".lms-listing-menu" ).css('background', 'rgba(202, 202, 202, 0.35)');
					jQuery( ".lms-listing-menu.global_luxury" ).css('background', '#14171A');
					jQuery(".beforescroll" ).css('display', 'inline-block');
					jQuery(".afterscroll" ).css('display', 'none');
          jQuery( ".node-type-lms-listing .toggle-menu svg" ).css('fill', '#fff');
				}
			});
			//Scroll the page slow
			//jQuery('.listing-menu2 a').click(function(){
      jQuery('div.property-website').on('click', ".listing-menu2 a", function(e){
				jQuery('html, body').animate({
					scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
				}, 1500);				
				return false;
			});
			//Responsive menu for mobile device
			/*jQuery('.meanmenu-reveal').on('click', function(){
				//jQuery('.mean-nav').toggleClass('open');
				var txt = jQuery(this).text();
				if(txt == 'X'){
					jQuery('.mean-nav').addClass('open');
				}
				else {
					jQuery('.mean-nav').removeClass('open');
				}
			});*/
			jQuery('.mean-nav li a').on('click', function(){
				//jQuery('.meanmenu-reveal').click();
				jQuery('html, body').animate({
					scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
				}, 1500);				
				return false;
			});
      if(jQuery('.property-website-loader').length){
        if(jQuery(window).width() < 770){
          jQuery('.website-spinner').attr('src', '/sites/all/modules/custom/listing_marketing_system/images/spinnersm.gif');
        }
        var nid = jQuery('.property-website').attr('nid');
        var brand = jQuery('.property-website').attr('brand');
        var design = jQuery('.property-website').attr('design');
        var web_style = (design != '') ? "?website_style="+design : '';
        jQuery.ajax({
          data: '',
          url: '/listing-node-detail/'+nid+'/'+brand+web_style,
          type: "POST",
          beforeSend: function() {
          },
          success: function(data) {
            jQuery('div.property-website').html(data);
            jQuery('div.property-website-loader').remove();
            jQuery('ul.photos-gallery li').hide().filter(':lt(8)').show();
            jQuery('.less-photos').hide();
            //Initialize google map on property web page.
            if(jQuery('#lcGoogleMap').length > 0) {
              //google.maps.event.addDomListener(window, 'load', lc_map_initialize);
              lc_map_initialize();
            }
            if(jQuery(window).width() < 770){
              jQuery('.lazy').attr('src', '/sites/all/modules/custom/listing_marketing_system/images/spinnersm.gif');
            }
            //Play audio at web listing page
            /*if(jQuery('a.listing-audio-play').length > 0) {
              var audioElement = document.createElement('audio');
              var url = jQuery('a.listing-audio-play').attr('url');
              //var url_split = url.split('d293rdt04oz7au.cloudfront.net');
              var url_split = url.split('?');
              jQuery('#audio').append(audioElement);
              //console.log("URL = "+url);
              //audioElement.setAttribute('src', 'http://www.homecb.com'+url_split[1]);
              audioElement.setAttribute('src', url_split[0]);
              audioElement.setAttribute('crossorigin', 'anonymous');
              audioElement.load();
              audioElement.addEventListener("load", function() { 
                audioElement.play(); 
              }, true);
              audioElement.play();
            }*/
          }
        });
      }
      /*jQuery('div.property-website').on('click', ".play", function(e){
        e.preventDefault();
        var seconds = audioElement.currentTime;
        if(seconds == 0 || audioElement.paused == true){
          var url = jQuery('a.listing-audio-play').attr('url');
          var url_split = url.split('d293rdt04oz7au.cloudfront.net');
          audioElement.setAttribute('src', 'http://www.homecb.com'+url_split[1]);
          audioElement.setAttribute('crossorigin', 'anonymous');
          audioElement.load();
          audioElement.addEventListener("load", function() { 
            audioElement.play(); 
          }, true);
          audioElement.play();
        }
        else {
          audioElement.pause();
        }
      });*/
      //       
      //
      if(jQuery('#screen_width').length == 0){
        jQuery.post('/set-screen-size', { width: screen.width, height:screen.height }, function(json) {
          if(json.outcome == 'success') {
            jQuery('.property-website').after('<input type="hidden" id="screen_width" value="'+screen.width+'">');
            jQuery('.property-website').after('<input type="hidden" id="screen_height" value="'+screen.height+'">');
          }
          else {
            console.log('Unable to let PHP know what the screen resolution is!');
          }
        },'json');
      }
		}
	};
})(jQuery, Drupal, this, this.document);

function contact_agent_modal(){
  jQuery("#contact_agent_modal .blackoverlay").show();
  jQuery("#contact_agent_modal_content").show();
  jQuery("#contact_agent_modal_content").css({"bottom": "unset"});
  jQuery("#contact_agent_modal_content").animate({"margin-top": "0px"}, 500);
}

function contact_agent_modal_hide(){
  jQuery("#contact_agent_modal .blackoverlay").hide();
  jQuery("#contact_agent_modal_content").hide();
  jQuery("#contact_agent_modal_content").animate({bottom: "-3270px"}, 500);
}

/**
 * callback function lc_map_initialize
 * to set the google map at lc website setting page
 **/
function lc_map_initialize() {
  var lc_lat = jQuery('#lc_lat').val();
  var lc_lng = jQuery('#lc_lng').val();
  var lc_address = jQuery('#lc_address').val();
  var lc_price = jQuery('#lc_price').val();
  var data = '<div class="map-tooltip"><div class="lc-address">'+lc_address+'</div><div class="lc-price">'+lc_price+'</div></div>';
  var cbone_info = new google.maps.InfoWindow({
    content: data
  });
  var myLatlng = new google.maps.LatLng(lc_lat, lc_lng);
  var mapProp = {
    center: myLatlng,
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP      
  };
  var map = new google.maps.Map(document.getElementById("lcGoogleMap"), mapProp);
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    icon: './site_files/map-pin-black.png',
    title: lc_address,
    draggable: false  
  });
  /*google.maps.event.addListener(marker, 'click', function() {
    cbone_info.open(map, marker);
  });*/
  google.maps.event.addListener(cbone_info, 'domready', function() {
    var iwOuter = jQuery('.gm-style-iw');
    iwOuter.parent().css({'width': '350px','border-top': '5px solid #1f69ff','padding': '6px 10px 10px','background': '#fff','box-shadow': '0 1px 2px 0 rgba(50,50,50,0.3)','position': 'relative'});
    iwOuter.next('button').find('img').remove();
    iwOuter.next('button').html("<div class='close-window'>&times;</span>");
    iwOuter.next('button').find('div.close-window').css({'width': '30px', 'font-size': '25px', 'color': '#000'});
    //iwOuter.next('button').find('div.close-window').css({'width': '30px', 'background': '#1f69ff', 'font-size': '25px','color': '#fff', 'box-shadow': '0 1px 2px 0 rgba(50,50,50,0.3)', 'border-radius': '4px', 'transition': 'all .25s linear'});
  });
  google.maps.event.addListener(marker, 'click', function() {
    cbone_info.open(map, marker);
  });
}

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.listing_pro_brochures = {
		attach: function(context, settings) {
			jQuery("[id^='submit-approval-for-']").click(function(e){
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var nid = jQuery(this).attr("listing-nid");
				var pdf_section = jQuery(this).attr('pdf_section');
				var data = jQuery(":checkbox:checked").map(function() {
					return this.id;
				}).get();
				jQuery.post('/pdf-send-for-approval', {"data":data, "nid":nid, "pdf_section":pdf_section}, function(response) {
					if(response == 'done'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("The agent has been successfully notified.");
						location.reload();
					}
					else if(response == 'empty'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Select pdf for approval.");
					}
				});
			});
			//uncheck edit and approve check if reject check clicked
			jQuery("#reject-proofs").click(function(e){
				jQuery("[class^='approve-pdf-']").prop('checked', false);
				jQuery('.edit-pdf').prop('checked', false);
			});
			//uncheck reject check if approve check clicked
			jQuery("[class^='approve-pdf-']").click(function(e){
				jQuery('#reject-proofs').prop('checked', false);
				jQuery('.edit-pdf').prop('checked', false);
			});
			//uncheck reject check if edit check clicked
			jQuery(".edit-pdf").click(function(e){
				jQuery('.edit-pdf').prop('checked', false);
				jQuery(this).prop('checked', true);
				jQuery("[class^='approve-pdf-']").prop('checked', false);
				jQuery('#reject-proofs').prop('checked', false);
			});
			//Submit the brochures approval page for further functionality
			jQuery("#submit-approval-reject-link").click(function(e){
				var data = jQuery(":checkbox:checked").map(function() {
					return this.id;
				}).get();
				if(data != 'reject-proofs'){
					 if (!confirm('Are you sure you are ready to submit your order? Once you click "Submit to printer" your order will immediately be sent to the printer and no further edits or changes can be made. Click "Cancel" to close this window and return without submitting the order.')) {
					return false;
				  }
				}
				e.preventDefault();
				jQuery(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><div class="message">Please wait...</div></div>');
				var listing_nid = jQuery(this).attr("listing_nid");
				var pdf_section = jQuery(this).attr('pdf_section');
				var data = jQuery(":checkbox:checked").map(function() {
					return this.id;
				}).get();
				var approve = '';
				var mcpdfs = new Array();
				jQuery('.brochures-approval-body ul li').each(function() {
					//get the id
					var data = {};
					data.id  = jQuery(this).attr("id");
					data.note  = jQuery(this).find(".marketing-coordinator-note textarea").val();
					//push the object into the array
					mcpdfs.push(data);
				});
				var note = '';
				if(jQuery("[class^='approve-pdf-']").is(':checked')) {				
					approve = jQuery("input[name='approve']:checked").attr("id");
					note = jQuery(".marketing-coordinator-note textarea").val();
				}
				if(data.length === 0 && approve == ''){
					jQuery('div.ajax-progress-throbber').remove();
					alert("Kindly view PDF and select approve!");
					return false;
				}
				jQuery.post('/brochures-approval-update', {"data":data, "listing_nid":listing_nid, "pdf_section":pdf_section, "approve":approve, "note":note, "mcpdfs":mcpdfs}, function(response) {
					if(response == 'done'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Thank you. Your order has been sent to the printer. You will receive additional information via email.");
						var redirect_url = "/manage-listing/" + listing_nid;
						window.location.pathname = redirect_url;
						//location.reload();
					}
					else if(response == 'reject'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("Your request for new proofs has been sent to your Marketing Coordinator.");
						var redirect_url = "/manage-listing/" + listing_nid;
						window.location.pathname = redirect_url;
					}
					else if(response == 'error'){
						jQuery('div.ajax-progress-throbber').remove();
						alert("We're sorry but this order encountered a problem. We can not send this order to the print vendor.");
						var redirect_url = "/manage-listing/" + listing_nid;
						window.location.pathname = redirect_url;
					}
					else if(response == 'empty'){
						jQuery('div.ajax-progress-throbber').remove();
						var redirect_url = "/edit-proof/" + listing_nid + "/" + data;
						window.location.pathname = redirect_url;
						//alert("Edit has selected.");
					}
				});
			});
			//Update mcpdf node for pdf view by agent
			jQuery("[id^='view-brochures-pdf-']").click(function(e){
				var mcpdf_nid = jQuery(this).attr("mcpdf_nid");
				if(jQuery('.approve-pdf-' + mcpdf_nid).attr('disabled',true)){
					jQuery.post('/brochures-pdf-view', {"mcpdf_nid":mcpdf_nid}, function(response) {
						if(response == 'done'){
							jQuery('#reject-proofs').prop('checked', false);
							jQuery('.edit-pdf').prop('checked', false);
							jQuery('.approve-pdf-' + mcpdf_nid).attr('disabled', false);
							jQuery('.approve-pdf-' + mcpdf_nid).prop('checked', true);
						}
					});
				}
			});
			//Show/Hide the tab data at PDF proof edit page
			jQuery("li.text-tabs").click(function(e){
				jQuery(".pdf-edit-proof ul.tabs li").removeClass("active");
				jQuery('.edit-proof-data > div').hide();
				jQuery('.edit-proof-text').show();
				jQuery(this).addClass("active");
			});
			jQuery("li.photos-tabs").click(function(e){
				jQuery(".pdf-edit-proof ul.tabs li").removeClass("active");
				jQuery('.edit-proof-data > div').hide();
				jQuery('.edit-proof-photos').show();
				jQuery(this).addClass("active");
			});
			//Check if user has edit the mc_pdf but not saved at "/edit-proof/[nid]/nid" page
			jQuery('#pdf-edit-proof-form').on('change keyup keydown', 'input, textarea', function (e) {
				var keys = [13,37,38,39,40,27,17,18,9,16,20,91,93,48,36,35,45,46,33,34,144,145,19,112,113,114,115,116,117,118,119,120,121,122,123];
				if(jQuery.inArray(e.keyCode, keys) == -1) {
					var original_text = jQuery(this).attr("original-text");
					var current_text =  jQuery(this).val();
					//alert("original_text = "+original_text);
					//alert("current_text = "+current_text);
					if(original_text == current_text){
						jQuery(this).removeClass('changed-input');
					}
					else {
						jQuery(this).addClass('changed-input');
					}
					if(this.checked) {
						jQuery(this).addClass('changed-input');
					}
					//jQuery(this).addClass('changed-input');
				}
			});
			//redirect the user if click on approve tab
			jQuery("li.approve-tabs").click(function(e){
				jQuery(".pdf-edit-proof ul.tabs li").removeClass("active");
				jQuery(this).addClass("active");
				var pdf_section = jQuery(this).attr('pdf_section');
				if(pdf_section == 'pro-brochure') {
					var listing_nid = jQuery('.edit-proof-data').attr("listing_nid");
					var redirect_url = "/pro-brochures-approval/" + listing_nid;
				}
				else if(pdf_section == 'post-card'){
					var listing_nid = jQuery('.edit-proof-data').attr("listing_nid");
					var redirect_url = "/postcard-approval/" + listing_nid;
				}
				if (jQuery('.changed-input').length) {
					var msg = "You have changes that have not been saved.";
					doConfirm(msg, function yes(){
						window.location.pathname = redirect_url;
					}, function no(){
						return false;
					});
				}
				else {
					window.location.pathname = redirect_url;
				}
			});
			//IMPORT LISTING COPY in textarea field at /edit-proof/[nid]/[nid]
			jQuery("#edit-text-options-import-listing-copy").click(function(e){
				if(this.checked) {
					var marketing_headline = jQuery('#edit-marketing-headline').val();
					var details_value = jQuery('#edit-details-value').text();
					if(marketing_headline != '' && details_value != ''){
						var msg = "Are you sure you want to replace the existing Headline and Marketing Copy from the listing data?";
						var r = confirm(msg);
						if (r == true) {
							var listing_copy = jQuery('.listing-copy-value').html();
							var listing_headline = jQuery('.listing-headline-value').text();
							if(listing_copy != 'N/A' && listing_copy != ''){
								//jQuery('#edit-details-value').text(listing_copy);
								CKEDITOR.instances['edit-details-value'].setData(listing_copy);
								jQuery('#edit-marketing-headline').val(listing_headline);
							}
							else {
								alert("Empty listing copy.");
								jQuery(this).prop('checked', false);
							}
						}
						else {
							jQuery('#edit-text-options-import-listing-copy').prop('checked', false);
							return false;
						}
						/*doConfirm(msg, function yes(){
							var listing_copy = jQuery('.listing-copy-value').html();
							var listing_headline = jQuery('.listing-headline-value').text();
							if(listing_copy != 'N/A' && listing_copy != ''){
								//jQuery('#edit-details-value').text(listing_copy);
								CKEDITOR.instances['edit-details-value'].setData(listing_copy);
								jQuery('#edit-marketing-headline').val(listing_headline);
							}
							else {
								alert("Empty listing copy.");
								jQuery(this).prop('checked', false);
							}
						}, function no(){
							jQuery('#edit-text-options-import-listing-copy').prop('checked', false);
							return false;
						});*/
					}
					else {
						var listing_copy = jQuery('.listing-copy-value').html();
						var listing_headline = jQuery('.listing-headline-value').text();
						if(listing_copy != 'N/A' && listing_copy != ''){
							//jQuery('#edit-details-value').text(listing_copy);
							CKEDITOR.instances['edit-details-value'].setData(listing_copy);
							jQuery('#edit-marketing-headline').val(listing_headline);
						}
						else {
							alert("Empty listing copy.");
							jQuery(this).prop('checked', false);
						}
					}
				}
				else {
					var original_text = jQuery('#edit-details-value').attr("original-text");
					CKEDITOR.instances['edit-details-value'].setData(original_text);
					var original_headline = jQuery('#edit-marketing-headline').attr("original-text");
					jQuery('#edit-marketing-headline').val(original_headline);
					//alert(original_text);
				}
			});
			//Auto generate mcpdf
			jQuery("[id^='generate-mcpdf-']").click(function(e){
				e.preventDefault();				
				var listing_nid = jQuery(this).attr('listing_nid');
				var template = jQuery(this).attr('template');
				//parseInt($("#testid").val(), 10);
				var require_photo = parseInt(jQuery(this).attr('require-photo'), 10);
				var listing_photos = parseInt(jQuery('.listing-photos').attr('photos'), 10);
				if(require_photo > listing_photos){
					var less = require_photo - listing_photos;
					alert("This PDF Design requires "+require_photo+" photos but the listing only has "+listing_photos+" photos. Please upload "+less+" additional photos for this listing or select another design with fewer photos.")
					return false;
				}
				else {
					var url = Drupal.settings.basePath + "sites/all/modules/custom/listing_pdf/images/waitscreens.gif";
					jQuery('#image_popup').bPopup({
						content:'image', //'ajax', 'iframe' or 'image'
						contentContainer:'.image_area',
						loadUrl:url, //Uses jQuery.load()
						modalClose:false,
					});
					jQuery.post('/auto-generate-mcpdf', {"listing_nid":listing_nid, "template":template}, function(response) {
						if(response == 'done'){
							location.href = Drupal.settings.basePath + "generate-pdf/"+listing_nid+"/"+template;
						}
					});
				}
			});
		}
	};
})(jQuery, Drupal, this, this.document);

/**
 * Calback function doConfirm()
 * to displayed the custom confirm form
 **/
function doConfirm(msg, yesFn, noFn) {
    var confirmBox = jQuery("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".yes,.no").unbind().click(function()
    {
        confirmBox.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}

;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	Drupal.upload_mail_list_plupload = Drupal.upload_mail_list_plupload || {};
	//filesAddedCallback
	Drupal.upload_mail_list_plupload.ListfilesAddedCallback = function (up, files) {
		if(up.files.length == 1){
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').hide('slow');
		}
		else if(files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
		}
		else if(up.files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//FilesRemovedCallback
	Drupal.upload_mail_list_plupload.ListFilesRemovedCallback = function (up, files) {
		if(up.files.length == 0){
			jQuery('.form-item-listed-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//filesAddedCallback
	Drupal.upload_mail_list_plupload.SoldfilesAddedCallback = function (up, files) {
		if(up.files.length == 1){
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').hide('slow');
		}
		else if(files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
		}
		else if(up.files.length > 1){
			alert("Kindly select only one file.");
			up.splice();
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	//FilesRemovedCallback
	Drupal.upload_mail_list_plupload.SoldFilesRemovedCallback = function (up, files) {
		if(up.files.length == 0){
			jQuery('.form-item-sold-mailing-list a.plupload_button.plupload_add').show('slow');
		}
	};
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.marketing_concierge = {
		attach: function(context, settings) {
			//show/hide the upload link at marketing-concierge-order/[order_id] page			
			//jQuery("#edit-direct-mail-postcards-upload-own-list").click(function(e){
			jQuery("#direct-mail-upload").click(function(e){
				jQuery('#edit-direct-mail-postcards-upload-own-list').prop('checked', true);
			});
			/*jQuery("input[name='direct_mail_postcards']").click(function(e){
				if(jQuery("#edit-direct-mail-postcards-upload-own-list").is(':checked')) {
					jQuery(".direct-mail-file").show();
				}
				else {
					jQuery(".direct-mail-file").hide();
				}
			});*/
			//jQuery('#marketing-concierge-additional-order-form').on('keyup keydown', 'input', function (e) {
			jQuery(".additional-postcards").keyup(function(e) {
				this.value = this.value.replace(/[^0-9\.]/g,'');
				var subtotal_class = jQuery(this).attr('subtotal-class');
				if(this.value == ''){
					jQuery(this).parent(".form-item").next(".additional-total").find(".listed-total").text("--");
					jQuery(".package."+subtotal_class+" .amount").text("--");
					update_subtotal_amount();
				}
				else {
					var additional = jQuery(this).val();
					if(jQuery.isNumeric(additional)){
						var rate = jQuery(this).parent(".form-item").next(".additional-total").find(".rate").text();
						var total = additional * rate;
						total = parseFloat(total).toFixed(2);
						//update listed-total
						jQuery(this).parent(".form-item").next(".additional-total").find(".listed-total").text("$"+total);
						//update subtotal
						jQuery(".package."+subtotal_class+" .amount").text("$"+total);
						update_subtotal_amount();
					}
				}
			});
			//show/hide the "Just Sold Mailing List" file field
			jQuery("#upload-direct-mail-list-form #edit-same-listed-list").click(function(e) {
				jQuery(".form-item-sold-mailing-list").toggle();
			});
			//close popup after click on cancel button
			jQuery("#upload-direct-mail-list-form #edit-cancel").click(function(e) {
				e.preventDefault();
				parent.closeBPopup();
			});
			/*$(window).load(function() {
				if(jQuery('.mc-form-submit .form-submit').hasClass('cbone-mc-free')) {
					jQuery('.mc-form-submit .form-submit').click();
				}
			});*/
		}
	};
})(jQuery, Drupal, this, this.document);

/**
 * Callback function update_subtotal_amount()
 * to update the total amount
 **/
function update_subtotal_amount(){
	var subtotal_amount = 00;
	jQuery(".subtotal-total .amount").text("$"+subtotal_amount);
	jQuery('.subtotal .package .amount').each(function() {
		var amount = jQuery(this).text();
		var amountArray = amount.split('$');
		if(jQuery.isNumeric(amountArray[1])) {
			subtotal_amount = subtotal_amount + parseFloat(amountArray[1]);
		}
	});
	subtotal_amount = parseFloat(subtotal_amount).toFixed(2);
	jQuery(".subtotal-total .amount").text("$"+subtotal_amount);
	subtotal_amount = 00;
}
/**
 * Callback function closeBPopup()
 * to close the bpopup after click
 * on cancel button
 **/
function closeBPopup() {
	jQuery("#popup").bPopup().close()
}
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
/**
 * verify mobile popup
 **/
jQuery(document).ready(function() {
	var link = jQuery('a[rel="modal-node-popup"]');
	if(link) {
		jQuery('body').append('<div><div id="popup"><span class="button b-close"><span></span></span><div class="c_area"></div></div></div>').html();
	}
	jQuery('a[rel="modal-node-popup"]').click(function(e){
		e.preventDefault();
		var scroll = jQuery(document).scrollTop();
		jQuery("#popup-load-img").css("top", scroll+"px");
		jQuery("#popup").css("height", "");
		jQuery("#popup").css("width", "");
		var url = jQuery(this).attr('href');
		var slide = 'slideBack';

		var p = {},
			width = "950",
			height = "850",
			e,
			a = /\+/g,  // Regex for replacing addition symbol with a space
			r = /([^&=]+)=?([^&]*)/g,
			d = function (s) { return decodeURIComponent(s.replace(a, ' ')); },
			q = url.split('?');				
		while (e = r.exec(q[1])) {
			e[1] = d(e[1]);
			e[2] = d(e[2]);
			if(e[1] != 'slide'){
				if(e[1] == 'width') {
					width = e[2];
				}
				else if(e[1] == 'height') {
					height = e[2];
				}
			}
			else {
				if(e[2] == 'top') {
					slide = 'slideDown';
				}
				else if(e[2] == 'bottom') {
					slide = 'slideUp';
				}
				else if(e[2] == 'right') {
					slide = 'slideBack';
				}
				else if(e[2] == 'left') {
					slide = 'slideIn';
				}
			}
		}
		jQuery("#popup").css('width', width+"px");
		jQuery("#popup").css('height', height+"px");
		
		q = url.split('/node/');
		jQuery('.c_area').html('');
		jQuery('.b-close span').html('');
		jQuery('#modal-popup').show();

		jQuery('#popup').bPopup({
			content:'iframe', //'ajax', 'iframe' or 'image'
			contentContainer:'.c_area',
			loadUrl:url, //Uses jQuery.load()
			speed: 650,
			amsl: 0,
			transition: slide,
			positionStyle: 'fixed',
			iframeAttr: ('scrolling="yes" frameborder="0"'),
			onOpen: function() {
				jQuery('#popup-load-img').show();
				jQuery('.c_area').append('<div class="loading-image"><img src="/sites/all/modules/custom/modal_node_display/images/dim-loading.gif"/></div>').html();
				jQuery('.b-close span').html("[Close]");
			},
			onClose: function() {
				jQuery('.c_area').empty();
				jQuery('.b-close span').empty();
			}
		}, function(){
			setTimeout( "jQuery('.loading-image').hide();", 2000);
			width = width - 10;
			height = height - 60;
			jQuery("#popup iframe").css('width', width+"px");
			jQuery("#popup iframe").css('height', height+"px");
		});
	});
});

;/*})'"*/
;/*})'"*/
/*================================================================================
 * @name: bPopup - if you can't get it up, use bPopup
 * @author: (c)Bjoern Klinggaard (twitter@bklinggaard)
 * @demo: http://dinbror.dk/bpopup
 * @version: 0.10.0.min
 ================================================================================*/
 (function(b){b.fn.bPopup=function(z,F){function M(){a.contentContainer=b(a.contentContainer||c);switch(a.content){case "iframe":var d=b('<iframe class="b-iframe" '+a.iframeAttr+"></iframe>");d.appendTo(a.contentContainer);r=c.outerHeight(!0);s=c.outerWidth(!0);A();d.attr("src",a.loadUrl);k(a.loadCallback);break;case "image":A();b("<img />").load(function(){k(a.loadCallback);G(b(this))}).attr("src",a.loadUrl).hide().appendTo(a.contentContainer);break;default:A(),b('<div class="b-ajax-wrapper"></div>').load(a.loadUrl,a.loadData,function(c,d,e){k(a.loadCallback,d);G(b(this))}).hide().appendTo(a.contentContainer)}}function A(){a.modal&&b('<div class="b-modal '+e+'"></div>').css({backgroundColor:a.modalColor,position:"fixed",top:0,right:0,bottom:0,left:0,opacity:0,zIndex:a.zIndex+t}).appendTo(a.appendTo).fadeTo(a.speed,a.opacity);D();c.data("bPopup",a).data("id",e).css({left:"slideIn"==a.transition||"slideBack"==a.transition?"slideBack"==a.transition?f.scrollLeft()+u:-1*(v+s):l(!(!a.follow[0]&&m||g)),position:a.positionStyle||"absolute",top:"slideDown"==a.transition||"slideUp"==a.transition?"slideUp"==a.transition?f.scrollTop()+w:x+-1*r:n(!(!a.follow[1]&&p||g)),"z-index":a.zIndex+t+1}).each(function(){a.appending&&b(this).appendTo(a.appendTo)});H(!0)}function q(){a.modal&&b(".b-modal."+c.data("id")).fadeTo(a.speed,0,function(){b(this).remove()});a.scrollBar||b("html").css("overflow","auto");b(".b-modal."+e).unbind("click");f.unbind("keydown."+e);h.unbind("."+e).data("bPopup",0<h.data("bPopup")-1?h.data("bPopup")-1:null);c.undelegate(".bClose, ."+a.closeClass,"click."+e,q).data("bPopup",null);clearTimeout(I);H();return!1}function J(d){w=y.innerHeight||h.height();u=y.innerWidth||h.width();if(B=E())clearTimeout(K),K=setTimeout(function(){D();d=d||a.followSpeed;c.dequeue().each(function(){g?b(this).css({left:v,top:x}):b(this).animate({left:a.follow[0]?l(!0):"auto",top:a.follow[1]?n(!0):"auto"},d,a.followEasing)})},50)}function G(d){var b=d.width(),e=d.height(),f={};a.contentContainer.css({height:e,width:b});e>=c.height()&&(f.height=c.height());b>=c.width()&&(f.width=c.width());r=c.outerHeight(!0);s=c.outerWidth(!0);D();a.contentContainer.css({height:"auto",width:"auto"});f.left=l(!(!a.follow[0]&&m||g));f.top=n(!(!a.follow[1]&&p||g));c.animate(f,250,function(){d.show();B=E()})}function N(){h.data("bPopup",t);c.delegate(".bClose, ."+a.closeClass,"click."+e,q);a.modalClose&&b(".b-modal."+e).css("cursor","pointer").bind("click",q);O||!a.follow[0]&&!a.follow[1]||h.bind("scroll."+e,function(){B&&c.dequeue().animate({left:a.follow[0]?l(!g):"auto",top:a.follow[1]?n(!g):"auto"},a.followSpeed,a.followEasing)}).bind("resize."+e,function(){J()});a.escClose&&f.bind("keydown."+e,function(a){27==a.which&&q()})}function H(d){function b(e){c.css({display:"block",opacity:1}).animate(e,a.speed,a.easing,function(){L(d)})}switch(d?a.transition:a.transitionClose||a.transition){case "slideIn":b({left:d?l(!(!a.follow[0]&&m||g)):f.scrollLeft()-(s||c.outerWidth(!0))-C});break;case "slideBack":b({left:d?l(!(!a.follow[0]&&m||g)):f.scrollLeft()+u+C});break;case "slideDown":b({top:d?n(!(!a.follow[1]&&p||g)):f.scrollTop()-(r||c.outerHeight(!0))-C});break;case "slideUp":b({top:d?n(!(!a.follow[1]&&p||g)):f.scrollTop()+w+C});break;default:c.stop().fadeTo(a.speed,d?1:0,function(){L(d)})}}function L(b){b?(N(),k(F),a.autoClose&&(I=setTimeout(q,a.autoClose))):(c.hide(),k(a.onClose),a.loadUrl&&(a.contentContainer.empty(),c.css({height:"auto",width:"auto"})))}function l(a){return a?v+f.scrollLeft():v}function n(a){return a?x+f.scrollTop():x}function k(a,e){b.isFunction(a)&&a.call(c,e)}function D(){x=p?a.position[1]:Math.max(0,(w-c.outerHeight(!0))/2-a.amsl);v=m?a.position[0]:(u-c.outerWidth(!0))/2;B=E()}function E(){return w>c.outerHeight(!0)&&u>c.outerWidth(!0)}b.isFunction(z)&&(F=z,z=null);var a=b.extend({},b.fn.bPopup.defaults,z);a.scrollBar||b("html").css("overflow","hidden");var c=this,f=b(document),y=window,h=b(y),w=y.innerHeight||h.height(),u=y.innerWidth||h.width(),O=/OS 6(_\d)+/i.test(navigator.userAgent),C=200,t=0,e,B,p,m,g,x,v,r,s,K,I;c.close=function(){q()};c.reposition=function(a){J(a)};return c.each(function(){b(this).data("bPopup")||(k(a.onOpen),t=(h.data("bPopup")||0)+1,e="__b-popup"+t+"__",p="auto"!==a.position[1],m="auto"!==a.position[0],g="fixed"===a.positionStyle,r=c.outerHeight(!0),s=c.outerWidth(!0),a.loadUrl?M():A())})};b.fn.bPopup.defaults={amsl:50,appending:!0,appendTo:"body",autoClose:!1,closeClass:"b-close",content:"ajax",contentContainer:!1,easing:"swing",escClose:!0,follow:[!0,!0],followEasing:"swing",followSpeed:500,iframeAttr:'scrolling="no" frameborder="0"',loadCallback:!1,loadData:!1,loadUrl:!1,modal:!0,modalClose:!0,modalColor:"#000",onClose:!1,onOpen:!1,opacity:.7,position:["auto","auto"],positionStyle:"absolute",scrollBar:!0,speed:250,transition:"fadeIn",transitionClose:!1,zIndex:9997}})(jQuery);
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */

/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
/**
 * verify mobile popup
 **/
jQuery(document).ready(function() {
	jQuery('.show-hide-meter').click(function(){
		var uid = jQuery(this).attr('uid');
		jQuery('.user-section-meter-'+uid).toggle(400);
		jQuery('.percent-meter-'+uid).toggle(400);
		if(jQuery(this).text() == 'Show All Meter') {
			jQuery(this).text('Hide All Meter');
		}
		else {
			jQuery(this).text('Show All Meter');
		}
	});
});

;/*})'"*/
;/*})'"*/
