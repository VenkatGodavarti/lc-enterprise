import { Component } from '@angular/core';
import { Address, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'lc-edit-toolbar',
  templateUrl: './edit-toolbar.component.html',
  styleUrls: ['./edit-toolbar.component.scss']
})
export class EditToolbarComponent {
  currentOrder$: Observable<MarketingOrder>;
  address: Address;

  constructor(public store: Store<any>, private router: Router) {
    this.currentOrder$ = this.store.select(SELECTEDORDER).pipe(
      filter(order => order && order.listing),
      tap(order => this.address = order.listing.address)
    );
  }

  editListingAddress(order: MarketingOrder) {
    this.router.navigate([`/listing/${order._id}/address`]);
  }

  editPackage(order: MarketingOrder) {
    this.router.navigate([`/order-listing/${order._id}/select-marketing-package`]);
  }
}
