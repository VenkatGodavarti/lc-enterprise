import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Component, ViewChild } from '@angular/core';
import {
  ApiService,
  FlowState,
  Listing,
  MarketingOrder,
  MarketingOrderService,
  OrderListingFlowStateService,
  PhotoService,
  UserService
} from '@lc/core';

import { ListingCardComponent } from './listing-card.component';
import { FormsModule } from '@angular/forms';
import { Store, ActionsSubject } from '@ngrx/store';
import { of } from 'rxjs';
import SpyObj = jasmine.SpyObj;
import { MarketingOrderFactory } from '../../../../../core/src/lib/factories';
import { PromptDialogService } from '@lc/shared-components-ui';

let photoService: SpyObj<PhotoService>;

describe('ListingCardComponent', () => {

  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let store: SpyObj<Store<any>>;
  let state: SpyObj<FlowState>;

  beforeEach(fakeAsync(() => {
    state = jasmine.createSpyObj('FlowState', ['setStep']);

    store = jasmine.createSpyObj('Store', ['pipe', 'dispatch']);
    store.pipe.and.returnValue(of(state));

    photoService = jasmine.createSpyObj('photoService', ['presign']);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        FormsModule,
        RouterTestingModule,
        NgbModule,
      ],
      declarations: [ListingCardComponent, TestHostComponent],
      providers: [
        ApiService,
        MarketingOrderService,
        UserService,
        OrderListingFlowStateService,
        PromptDialogService,
        ActionsSubject,
        { provide: Store, useValue: store },
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.listingCard.order = component.marketingOrder;
    fixture.detectChanges();
    tick(1000);

  }));

  it('should create and set backgroundImage', () => {
    expect(component.listingCard).toBeTruthy();
    expect(component.listingCard.backgroundImage).toBe('/test/image-1.jpg');
  });

  it('should call the store dispatch when archiving listing', () => {
    component.listingCard.order = component.marketingOrder;

    // Assert test setup
    expect(component.listingCard.order.isArchived).toBeFalsy();

    // Call archive and assert
    component.listingCard.toggleArchiveOrder();
    expect(store.dispatch).toHaveBeenCalled();
    expect(component.listingCard.order.isArchived).toBeTruthy();
  });
});

@Component({
  selector: 'lc-test-host',
  template: '<lc-listing-card></lc-listing-card>'
})
class TestHostComponent {
  @ViewChild(ListingCardComponent)
  public listingCard: ListingCardComponent;
  public listings: Listing[] = [];
  public marketingOrder: MarketingOrder = MarketingOrderFactory.createMarketingOrder('123456789', null, null, true);
}

