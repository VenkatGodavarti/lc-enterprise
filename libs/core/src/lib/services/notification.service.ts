import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Notification } from '../models/notification.model';
import { NotificationForm } from '../forms/notification.form';
import { Store } from '@ngrx/store';
import { NOTIFICATIONS, UpdateNotification, LoadNotifications } from '../state-mgmt';
import { UserService } from './user.service';
import { switchMap, filter } from 'rxjs/operators';

/**
 * The notification service is used by the UI components to perform specific actions on a notification.
 * UI components should interact with this instead of the store or clients directly so that these implementations can
 * be reused by other components.
 */
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly notifications$: Observable<Notification[]>;

  constructor(private authService: UserService, private store: Store<any>) {
    this.store.dispatch(new LoadNotifications());

    // Only request notifications if user is logged in
    this.notifications$ = this.authService.currentUser.pipe(
      filter(user => user != null),
      switchMap(user => this.store.select<Notification[]>(NOTIFICATIONS))
    );
  }

  /**
   * Returns an observable of the notifications that are stored in the current state management.
   */
  get(): Observable<Notification[]> {
    return this.notifications$;
  }

  /**
   * Marks the notifications as read and dispatches and update event.
   */
  markAsRead(notification: Notification) {
    if(notification.isRead) { return; }

    notification.isRead = true;
    this.store.dispatch(new UpdateNotification(new NotificationForm(notification)));
  }
}
