import { http } from './http';
import { Configuration as config } from '../configuration';

export class ProfileClient {
  static baseUrl = config.apiBaseURL + '/profiles';

  static async getCurrentUser() {
    const res = await http.get(`${config.apiBaseURL}/user/current-user`);
    const user = res.data;
    return user;
  }

  static async update(profileId: string, data: any) {
    const res = await http.put(`${config.apiBaseURL}/profiles/${profileId}`, data);
    if(res.status >= 400) {
      throw new Error(`unable to update the user profile because ${res.statusText}`);
    }
    return res;
  }
}
