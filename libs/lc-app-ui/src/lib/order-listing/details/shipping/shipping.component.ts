import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  ActionTypes, AddressInformation,
  AddressInformationForm,
  AddressType,
  MarketingOrder,
  PhoneType,
  SELECTEDORDER,
  STATES_BY_ABBREVIATION,
  UpdateOrder,
  UserService,
  USStates,
  WorkflowAction
} from '@lc/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { Masks } from '@lc/shared-components-ui';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'lc-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {
  readonly states: USStates[] = STATES_BY_ABBREVIATION;
  readonly phoneMask = Masks.phoneNumber;

  currentOrder$: Observable<MarketingOrder>;

  // shipping address form
  shippingForm: AddressInformationForm;
  params: ParamMap;

  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private store: Store<any>) {

    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  ngOnInit() {
    this.currentOrder$ = this.store.select(SELECTEDORDER).pipe(
      filter(order => order),
      tap(order => this.initForm(order.shippingInformation))
    );
  }

  initForm(shippingInformation) {
    // populate form from existing order data or user's profile shipping address
    this.shippingForm = new AddressInformationForm();
    if(shippingInformation) {
      this.shippingForm.patchValue(shippingInformation);
    } else {
      this.shippingForm.patchValue(this.getShippingContactInfo());
    }

    this.shippingForm.get('fullName').setValidators([Validators.required]);

    // populate address type if it's not already
    if(!this.shippingForm.get('type').value) {
      this.shippingForm.controls['type'].setValue(AddressType.SHIPPING);
    }

    // populate required phoneNumber fields if necessary
    const phoneNumber = this.shippingForm.get('phoneNumber') as FormGroup;
    if(!phoneNumber.get('type').value) {
      phoneNumber.get('type').setValue(PhoneType.direct);
    }
  }

  getShippingContactInfo():AddressInformation {
    let address = this.userService.getAddress(AddressType.SHIPPING);
    if (!address) {
      address = new AddressInformation();
    }
    return address;
  }

  goBack(order: MarketingOrder) {
    order.shippingInformation = this.shippingForm.value;
    // dispatch to the store to handle event
    this.store.dispatch(new UpdateOrder(order));
    // previous step
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(order: MarketingOrder) {
    this.shippingForm.markAllAsDirty();
    if(!this.shippingForm.valid) { return; }

    // remove any keys with null values before populating the model
    const formData = this.shippingForm.value;
    Object.keys(formData).forEach((key) => (formData[key] == null) && delete formData[key]);
    Object.keys(formData.phoneNumber).forEach((key) => (formData.phoneNumber[key] == null) && delete formData.phoneNumber[key]);

    // populate the model
    order.shippingInformation = formData;

    // dispatch to the store to handle event
    this.store.dispatch(new UpdateOrder(order));
    // next step
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, {stepCompleted: true}, this.params));
  }
}
