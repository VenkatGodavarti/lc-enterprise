import { ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import { SelectProductComponent } from './select-product.component';
import {Component, ViewChild} from "@angular/core";
import { Listing, ListingPhoto, PhotoService, ProductType, Address, ThumbnailSizes } from '@lc/core';
import {RouterTestingModule} from "@angular/router/testing";
import {By} from "@angular/platform-browser";

describe('SelectProductComponent', () => {

  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let selectProduct;
  let listing;
  let products;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [

        RouterTestingModule
      ],
      declarations: [ TestHostComponent, SelectProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    selectProduct = component.selectProduct;
    listing = component.createDefaultListing();
    selectProduct.listing = listing;
    products = component.createDefaultProducts();
    selectProduct.products = products;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(selectProduct).toBeTruthy();
  });

  it('should display all products', () => {
    expect(selectProduct).toBeTruthy();

    const productCards = fixture.debugElement.queryAll(By.css('div.service-product-card'));
    expect(productCards.length).toEqual(products.length);
  });
});




/**
 * A simple component ysed to host the photo grid during tests so that web can set its Inpiut listing value
 * properly.
 *
 * TestHostComponent has some test methods for creating  a bare-bones listing with photos for testing.
 * the listing object will not be saved to mongo or sent to a rest api call with validation so they are minimal listings
 *
 * We ar enot testing image retrieval so image urls are not real image urls.
 *
 */
@Component({
  selector: 'lc-test-host',
  template: '<lc-select-product></lc-select-product>'
})
class TestHostComponent {
  @ViewChild(SelectProductComponent)
  public selectProduct: SelectProductComponent;


  createDefaultListing() {
    const photos : ListingPhoto[] = [];

    photos.push(this.createPhoto('id-1', 1, true, '/test/image.jpg'));
    photos.push(this.createPhoto('id-2', 2, false, '/test/image.jpg'));
    photos.push(this.createPhoto('id-3', 3, true, '/test/image.jpg'));
    photos.push(this.createPhoto('id-4', 4, false, '/test/image.jpg'));

    return this.createListing(photos);
  }

  createListing(photos: ListingPhoto[]) : Listing {
    const listing = new Listing();

    listing._id = ''+new Date().getTime();
    listing.address = {
      streetAddress1: '123 Somewhere St.'
    } as Address;

    // if (photos) {
    //   listing.setPhotos(photos);
    // }

    return listing;
  }

  createPhoto(id, order, favorite, uri) {
    const photo = new ListingPhoto();
    photo._id = id;
    photo.order = order;
    photo.favorite = favorite;
    photo.uri = uri;
    photo.thumbnails = {};
    photo.thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH] = {uri: '/test/image-'+order+'.jpg' };
    return photo;
  }

  createDefaultProducts() {
    const products = [];

    let product = new ProductType();
    product.deserialize({ title: 'product-1', code: '001', category: 'print', status: 'Submitted'});
    products.push(product);

    product = new ProductType();
    product.deserialize({ title: 'product-2', code: '002', category: 'print', status: 'Submitted'});
    products.push(product);

    product = new ProductType();
    product.deserialize({ title: 'product-3', code: '003', category: 'digital', status: 'Completed'});
    products.push(product);

    return products;

  }
}
