import { Component, OnInit, Input } from '@angular/core';
import { TeamProfile } from '@lc/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'lc-team-display',
  templateUrl: './team-display.component.html',
  styleUrls: ['./team-display.component.scss']
})
export class TeamDisplayComponent implements OnInit {
  @Input()
  team: TeamProfile;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  onEdit() {
    this.router.navigate(['edit'], {relativeTo: this.activatedRoute})
  }

  onEditContacts() {
    this.router.navigate(['contacts'], {relativeTo: this.activatedRoute})
  }
}
