import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  ViewChildren,
  QueryList,
  Output, EventEmitter, Attribute
} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  ListingPhoto,
  MarketingOrder,
  PhotoService, ThumbnailSizes, PresignedPhotoDirective, UIConstants
} from '@lc/core';
import { DragulaService } from 'ng2-dragula';
import { environment } from '../../../../../environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogAction, PromptDialogService } from '@lc/shared-components-ui';
import { NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap/carousel/carousel';

/**
 * The Photo Display Model is only used by this component to handle the coordination of events related to a specific photo
 * Each photo in the grid has many actions we need to keep track of. In order to do so, we need to directly correlate the
 * photo with isSelected and isEditNumber. This also will make it easier to maintain rather than having separate lookup
 * objects to maintain these values.
 */
export class PhotoDisplayModel {
  isSelected: boolean;
  isEditNumber: boolean;
  constructor(public photo: ListingPhoto){
  }
}
/**
 * Display photos uploaded for a listing in a responsive grid with options to:
 *
 * 1. Choose up to 10 favorite photographs
 * 2. Toggle display of favorites or all photographs
 */
@Component({
  selector: 'lc-photo-grid',
  templateUrl: './photo-grid.component.html',
  styleUrls: ['./photo-grid.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PhotoGridComponent implements OnInit, OnDestroy, OnChanges {

  /**
   * Optional param of the marketing order. Used to show the address on popup window for image
   * TODO - consider address passing into this component
   */
  @Input()
  marketingOrder: MarketingOrder;

  @Input()
  photos: ListingPhoto[];

  @Input()
  deleteMessage: string;

  @Output()
  updatePhotos = new EventEmitter<ListingPhoto[]>();

  @Output()
  updatePhoto = new EventEmitter<ListingPhoto>();

  @Output()
  actionOnSelected = new EventEmitter<ListingPhoto[]>();

  @ViewChildren(PresignedPhotoDirective)
  directives: QueryList<PresignedPhotoDirective>;

  readonly photoDisplays: PhotoDisplayModel[] = [];

  dragulaGroup = 'LISTING_PHOTOS';
  subscriptions = new Subscription();

  selectedPhotoOrder = 1;
  isAllSelected: boolean;

  numberOfFavorites = 0;  // keep a count of the number of photos selected as favorites (max 10 allowed)
  showFavorites = false; // toggle true to show only favorites, false to show all photos
  useFourWideGrid = true;

  mediumThumbnailSize = ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH;
  fallbackThumbnailSize = ThumbnailSizes.LARGE_THUMBNAIL_WIDTH;
  get thumbnailSize() { return this.useFourWideGrid ?  ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH : ThumbnailSizes.LARGE_THUMBNAIL_WIDTH; }

  public actionButtonLabel: string;
  public noPhotosText: string;
  public favoritesEnabled : boolean;
  public dragDropEnabled : boolean;
  public deleteEnabled : boolean;
  public downloadEnabled : boolean;
  public selectEnabled : boolean;
  public numberOrderingEnabled : boolean;
  public isDragging: boolean;

  constructor(
    @Attribute('actionButtonLabel') actionButtonLabel: string,
    @Attribute('noPhotosText') noPhotosText: string,
    @Attribute('favoritesEnabled') favoritesEnabled : string,
    @Attribute('dragDropEnabled') dragDropEnabled : string,
    @Attribute('deleteEnabled')  deleteEnabled : string,
    @Attribute('downloadEnabled') downloadEnabled : string,
    @Attribute('selectEnabled') selectEnabled : string,
    @Attribute('numberOrderingEnabled') numberOrderingEnabled: string,

    private modalService: NgbModal,
              private photoService: PhotoService,
              private dialogService: PromptDialogService,
              private dragulaService: DragulaService) {

    this.actionButtonLabel = actionButtonLabel;
    this.noPhotosText = noPhotosText || 'No photos uploaded yet';
    this.favoritesEnabled = PhotoGridComponent.enabledAttribute(favoritesEnabled);
    this.dragDropEnabled = PhotoGridComponent.enabledAttribute(dragDropEnabled);
    this.deleteEnabled = PhotoGridComponent.enabledAttribute(deleteEnabled);
    this.downloadEnabled = PhotoGridComponent.enabledAttribute(downloadEnabled);
    this.selectEnabled = PhotoGridComponent.enabledAttribute(selectEnabled);
    this.numberOrderingEnabled = PhotoGridComponent.enabledAttribute(numberOrderingEnabled);
  }

  /**
   * When changes occur to photo model this will load other models
   */
  private loadPhotos() {
    // Maintain originally selected photoIds:
    const selectedPhotoIds = this.photoDisplays.filter(display => display.isSelected).map(display => display.photo._id);

    // HACK for dragula. Dragula does not like an array being re-initialized
    this.photoDisplays.splice(0, this.photoDisplays.length, ...this.photos.map(photo => new PhotoDisplayModel(photo)));

    //Restore selected photos.
    this.photoDisplays.forEach(display => display.isSelected = (selectedPhotoIds.includes(display.photo._id)));
    this.updateSelectAll();

    this.numberOfFavorites = this.photos.filter(photo => photo.favorite).length;
    this.resetEditablePhotoList();
    this.resetOrder();
  }

  ngOnInit() {
    this.loadPhotos();
    // subscribe to drag and drop changes in photo grid
    if (this.dragDropEnabled) {

      // We need to monitor drag events. Dragula has a bug with firefox: https://github.com/bevacqua/dragula/issues/564
      this.subscriptions.add(this.dragulaService.drag(this.dragulaGroup).subscribe(() => this.setIsDragging(true)));
      this.subscriptions.add(this.dragulaService.dragend(this.dragulaGroup).subscribe(() => this.setIsDragging(false)));

      this.subscriptions.add(this.dragulaService.dropModel(this.dragulaGroup)
        .subscribe(({ name, el, target, source, sibling, sourceModel, targetModel, item }) => {
          // We are not using many of these input values but here is what they each are
          // name = dragula group name
          // el, target, source, sibling = HTML elements being dragged and shuffled around
          // sourceModel, targetModel = dragulaModel assignment from the view/template
          // item = the ListingPhoto object that was just drug to a new location

          const photoDisplays: PhotoDisplayModel[] = targetModel;
          const photos = photoDisplays.map(display => display.photo);

          // re-order the dragula model and dispatch an event to update the photos
          photos.forEach((listingPhoto, index) => listingPhoto.order = index + 1);
          // Emit photo order changed
          this.updatePhotos.emit(photos);
        })
      );
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.photos) {
      this.loadPhotos();
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  onEditPhotoNumber(photoDisplay: PhotoDisplayModel, event) {
    photoDisplay.isEditNumber = !photoDisplay.isEditNumber;
    event.stopPropagation();
  }

  /**
   * Invoked after the user moves focus from the editable sort oder input field (onblur or hitting enter).
   * This takes the value from the input and moves the photos around in the photo array and on the UI to reflect the desired change.
   * @param photo -- the ListingPhoto object that was edited
   * @param event -- the Angular event object
   */
  setPhotoOrder(photoDisplay: PhotoDisplayModel, event) {
    if (event instanceof KeyboardEvent && event.key !== 'Enter') {
      return;
    }

    const originalIndex = this.photoDisplays.findIndex(item => item.isEditNumber);

    // New Index must be between 0 and the number of elements - 1
    let newIndex = event.target.valueAsNumber - 1;
    newIndex = Math.max(newIndex, 0);
    newIndex = Math.min(newIndex, this.photoDisplays.length - 1);

    // re-order the array
    this.photos.splice(newIndex, 0, this.photos.splice(originalIndex, 1)[0]);
    this.photos.forEach((foto, index) => foto.order = index + 1);
    // Send changes to listeners
    this.updatePhotos.emit(this.photos);
    photoDisplay.isEditNumber = false;
    this.loadPhotos();
  }

  /**
   * switches the grid display style to 4 across where screen resolution will support it.
   */
  /* istanbul ignore next */
  set4x4() {
    this.useFourWideGrid = true;
  }

  /**
   * switches the grid display style to 2 across where screen resolution will support it.
   */
  /* istanbul ignore next */
  set2x2() {
    this.useFourWideGrid = false;
  }

  /**
   * Select a photo in the large image modal window
   *
   * @param index
   */
  select(index: number, target?: HTMLImageElement) {
    this.selectedPhotoOrder = index;

    setTimeout(() => {
      target = target || document.getElementById('photo-button-'+ index) as HTMLImageElement;
      if(target){
        target.scrollIntoView({inline: "center"});
      }
    }, 200);

  }

  /**
   * Call to update the isAllSelected checkbox appropriately anytime the photos get selected
   */
  updateSelectAll() {
    this.isAllSelected = this.photoDisplays.every(display => display.isSelected);
  }

  /**
   * Called when carousel slide changes.
   */
  slideChanged(event: NgbSlideEvent) {
    // Determine index of current photo and select it in the scrollbar
    if (event.current.startsWith('photo-')) {
      const index = Number(event.current.substr(6));
      if (!isNaN(index)) {
        this.select(index);
      }
    }
  }

  /**
   * @param photo
   * @return true if the photo argument is the currently selected photo
   */
  isSelectedPhoto(photo) {
    return photo.order === this.selectedPhotoOrder;
  }

  setIsDragging(isDragging: boolean) {
    // Need to set is dragging with a 200ms debounce as Firefox will still open the image dialog after drag n' drop
    setTimeout(() => this.isDragging = isDragging, 200);
  }
  /**
   * Open the modal window with the large image and mrk the photo with the given order as the current selected image
   *
   * @param modal
   * @param index
   * @param event
   */
  open(modal, index, event) {
    // Do not open modal dialog if the user is dragging an image
    if(this.isDragging) { return; }
    this.modalService.open(modal, { centered: true, size: 'lg', windowClass: 'photo-gallery' });
    this.select(index);
  }

  /**
   * Toggle the photo argument's favorite flag, sending the updated photo back to the server
   * so that the listing is updated.
   * @param photo
   * @param event
   */
  toggleFavorite(photo: ListingPhoto, event) {

    // change numberOfFavorites displayed as badge on the favorites heart icon
    this.numberOfFavorites += photo.favorite ? -1 : 1;

    photo.favorite = !photo.favorite;
    this.updatePhoto.emit(photo);

    // stop the layer underneath getting the event, we only want it be be handled here.
    if (event.stopPropagation) {
      event.stopPropagation();
    }
  }

  /**
   * The favorites taoggle button overlaying each photo in the grid should be hidden if there are already 10 photos
   * marked as favorites in the UI and the photo argument is not one of the favorites
   *
   * @param photo
   * @return true if the favorites button for the photo argument should be hidden.
   */
  hideFavoritesButton(photo) {
    return this.numberOfFavorites >= 10 && !photo.favorite
  }

  /**
   * Toggle the flag that causes only favorite photos to be displayed
   */
  /* istanbul ignore next */
  toggleShowFavorites() {
    this.showFavorites = !this.showFavorites;
    this.numberOrderingEnabled = !this.showFavorites; // turn off numbering while filtered on favorites

    if(this.photoDisplays) {
      // Reset any selected photos when we change favorites filters
      const element: HTMLElement = document.getElementById('select-all');
      if(element instanceof HTMLInputElement) {
        const inputElement: HTMLInputElement = <HTMLInputElement>element;
        inputElement.checked = false;
      }
      this.photoDisplays.forEach(p => p.isSelected = false );
    }
  }

  /**
   * Return true if the photo should not be displayed.
   *
   * @param photo
   */
  hidePhoto(photo) {
    return this.showFavorites && !photo.favorite;
  }

  /**
   * Download single image with original resolution
   * @param imageUrl -- Cloud storage URL of the image
   * @param event - the event that invoked the action. Used to stop propagation
   */
  downloadImage(imageUrl, event) {
    this.photoService.getImage(imageUrl).subscribe(
      (res) => {
        const a = document.createElement('a');
        a.href = URL.createObjectURL(res[0].body);
        a.download = res[1].body['filename'];
        document.body.appendChild(a);
        a.click();
      });
    event.stopPropagation();
  }

  /**
    * separate low and high res imageurl and call export function
    * @param resl -- value of highres or lowres button when clicked
    */
  highLowRes(resl) {
    // Find all selected photos and match them up with their corresponding presignedPhotoDirective
    const selectedPresignedPhotoUrls = this.photoDisplays.filter(display => display.isSelected)
      .map(display => this.directives.find(directive => directive.photo === display.photo))
      .filter(directive => directive != null)

      // For each directive corresponding to the photos, find the proper url
      .map(directive => {
        if(resl === 'highRes'){
          return directive.presignedPhoto.presignedUrl;// Use the main presigned image
        }

        // If low res, select the current thumbnail size. If not found, use the original presigned
        const thumbnail = directive.presignedPhoto.locateThumbnail(this.thumbnailSize, this.fallbackThumbnailSize)
        return thumbnail ? thumbnail.presignedUrl : directive.presignedPhoto.presignedUrl;
      });

    // Export all of the pre-signed Urls
    this.exportZip(selectedPresignedPhotoUrls);
  }

  // TODO - need to separate from API. Emit an event
  /**
   * Sends a request with array of imageUrls to the microservice for zipping the photos
   */
  exportZip(selectedPhotoUrls: string[]) {
    if (selectedPhotoUrls.length > 0) {
      // Initiate zipping
      this.postForm(environment.photosBaseURL+"exportImages", {imageArray: selectedPhotoUrls}, 'post');
    }
    else {
      // it should pop up warning message for user: future scope
      console.warn("Please select images to export");
    }
  }

  postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
  }

  deletePhoto(photo, event) {
    const message = this.deleteMessage ? this.deleteMessage : 'Are you sure you want to delete photo?';
    const okAction = new DialogAction(UIConstants.YES, () => {
      const photos = this.photos.filter(aPhoto => aPhoto._id !== photo._id);
      photos.forEach((foto, index) => foto.order = index + 1);
      // set the new photos
      this.updatePhotos.emit(photos);
      // the reset will be calling API
      this.resetOrder();
    });
    this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)]);
    event.stopPropagation();
  }

  deleteSelected() {
    const message = this.deleteMessage ? this.deleteMessage : 'Are you sure you want to delete selected photo(s)?';
    const okAction = new DialogAction(UIConstants.YES, () => {
      // set the new photos on the listing model
      const remainingPhotos = this.photoDisplays.filter(x => !x.isSelected).map(display => display.photo);
      this.updatePhotos.emit(remainingPhotos);

      // the reset will be calling API and propagating changes
      this.resetOrder();
    });
    this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)]);
  }

  selectAll(event) {
    if(this.showFavorites) {
      // if user has filtered the view to only favorites then only select those
      this.photoDisplays.filter(display => display.photo.favorite).forEach(display => {
        display.isSelected = event.target.checked;
      });
    } else {
      // select all photos available
      this.photoDisplays.forEach(display => {
        display.isSelected = event.target.checked;
      });
    }

    event.stopPropagation();
  }

  // Checks to see if the user has selected any photos on the UI.  This return value assists in enabling and disabling UI functions
  // for downloading or deleting photos
  hasSelectedPhotos() {
    return this.photoDisplays.some(display => display.isSelected);
  }

  // Action button handler (e.g. "submit photos")
  // Emits the list of selected photos to the actionOnSelected output
  action() {
    const selectedPhotos = this.photoDisplays.filter(display => display.isSelected).map(display => display.photo);
    this.actionOnSelected.emit(selectedPhotos);
  }

  private resetOrder() {
    // set the correct order property on each object in the array
    this.numberOfFavorites = this.photos.filter(x => x.favorite).length;
    this.photos.forEach((foto, index) => foto.order = index + 1);
  }

  // this.edit object keeps track of which photo on the UI is currently editable (has the number input field viewable).  This function resets
  // the object when new photos are added to or removed from the array.
  private resetEditablePhotoList(editedPhoto?: ListingPhoto) {
    this.photoDisplays.forEach(p => p.isEditNumber = editedPhoto && p.photo._id === editedPhoto._id);
  }

  // Default for enabled attributes is true, so return true if not set
  private static enabledAttribute(val: string) { return (val === null) || (val === "true"); }


}

