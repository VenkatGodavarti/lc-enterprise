import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, HostBinding } from '@angular/core';
import { MarketingOrder, OutputMimeType, ProductGenerationService, ProductInstance, SELECTEDORDER } from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'lc-print-preview',
  templateUrl: './print-preview.component.html',
  styleUrls: ['./print-preview.component.scss']
})
export class PrintPreviewComponent implements OnInit, OnDestroy, OnChanges {

  @HostBinding('class')
  readonly hostClasses = 'flex-grow-1 d-flex flex-column';

  marketingOrder$: Observable<MarketingOrder>;
  marketingOrderSub: Subscription;
  marketingOrder: MarketingOrder;

  @Input()
  product: ProductInstance;

  mimeType: OutputMimeType;
  previewUrl;
  objectURL;
  text;

  loaded = false;

  downloadable = false;

  constructor(private store: Store<any>,
              private productGenerationService: ProductGenerationService,
              private domSanitizer: DomSanitizer) {
    this.marketingOrder$ = this.store.select(SELECTEDORDER);
  }

  ngOnInit() {

    this.marketingOrderSub = this.marketingOrder$.subscribe((marketingOrder) => {

      this.marketingOrder = marketingOrder;

      this.generate();
    });
  }

  ngOnDestroy(): void {
    if (this.marketingOrderSub) {
      this.marketingOrderSub.unsubscribe();
    }
    this.cleanup();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.generate();
  }

  applyChanges(): void {
    this.generate();
  }


  download(): void {

    if (this.marketingOrder && this.downloadable) {
      this.productGenerationService.download(this.marketingOrder, this.product);
    }
  }

  private generate() {

    this.mimeType = this.product.selectedTemplate.mimeType;

    this.loaded = false;
    this.downloadable = this.productGenerationService.isDownloadable(this.product)
    this.text = this.domSanitizer.bypassSecurityTrustHtml('');

    if (this.marketingOrder) {
      this.productGenerationService.generate(this.marketingOrder, this.product).subscribe(data => {

        switch (this.mimeType) {

          case OutputMimeType.pdf:

            this.cleanup();
            const blob = new Blob([data], { type: this.product.selectedTemplate.mimeType });
            this.objectURL = URL.createObjectURL(blob);
            this.previewUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.objectURL);
            this.loaded = true;
            break;

          case OutputMimeType.html:

            this.cleanup();
            // need to fix this, the observabe contains another observable
            // will resolve before release
            data.subscribe( text => {
              this.text = this.domSanitizer.bypassSecurityTrustHtml(text.replace(/\.\/site_files\//g, environment.baseURL+'assets/listing-site/site_files/'));
              this.loaded = true;
            })

        }

      });

    }
  }

  private cleanup(): void {

    if (this.objectURL) {
      URL.revokeObjectURL(this.objectURL);
    }
  }
}
