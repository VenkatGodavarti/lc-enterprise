import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { phoneValidator } from '../validators/phone.validator';

export class PhoneNumberForm extends BaseForm {
  public get number(): string { return this.get('number').value; }

  constructor(value?: any, options?: { validateType?: boolean, phoneNotRequired?: boolean }){
    super({
      number: new FormControl(null, [Validators.required, phoneValidator()]),
      type: new FormControl(),
      priority: new FormControl(),
      prefix: new FormControl(),
      primary: new FormControl(false),
      extension: new FormControl()
    });

    if(options && options.validateType === true){
      this.get('type').setValidators([this.validateType(this)]);
      this.get('number').valueChanges.subscribe(() => this.get('type').updateValueAndValidity({emitEvent: true}));
    }

    if(options && options.phoneNotRequired){
      // Set validators to only validate the phone regex
      this.get('number').setValidators([phoneValidator()]);
    }

    if(value) {
      this.patchValue(value);
    }
  }

  validateType(group: FormGroup): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const phoneNumber = group.get('number').value;
      return phoneNumber && control.value == null ? {required: true} : null;
    };
  }
}
