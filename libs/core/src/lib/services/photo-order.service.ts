import { Injectable } from '@angular/core';
import { PhotoOrder } from '../models/photo-order.model';
import { UploadPhoto } from '../models/upload-photo.model';
import { ListingPhoto } from '../models/listing-photo.model';
import { MultiMediaItem, MultiMediaItemRequest } from '../models/multi-media-item.model';
import { Media } from '../models/media.model';

import { ApiService } from './api.service';
import { PhotoService } from '../services/photo.service';
import { UserService } from '../services/user.service';

import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';


@Injectable()
export class PhotoOrderService {

  private resource = 'photo-orders';
  private photoSubmitResource = 'photo-submit';

  constructor(private apiService: ApiService,
              private userService: UserService) { }

  /* istanbul ignore next */
  saveOrder(order: PhotoOrder) {
    if (order._id) {
      return this.updateOrder(order);
    } else {
      return this.createOrder(order);
    }
  }

  /* istanbul ignore next */
  createOrder(order: PhotoOrder) {
    return this.apiService.post(this.resource, order);
  }

  /* istanbul ignore next */
  updateOrder(order: PhotoOrder) {
    const id = order._id;
    // do not pass in _.id in the body structure
    delete order._id;
    return this.apiService.put(this.resource + '/' + id, order);
  }

  /* istanbul ignore next */
  updateOrderPartial(order: PhotoOrder, fields: string[]) {
    fields = fields || [];
    const partial = {};
    fields.forEach(field => {
      partial[field] = order[field];
    });
    return this.apiService.put(this.resource + '/' + order._id, partial);
  }

  /* istanbul ignore next */
  retrievePhotoOrders(photographerId) {
    return this.apiService.get(this.resource + '?photographerId=' + photographerId);
  }

  /* istanbul ignore next */
  retrievePhotoOrder(photoOrderId) {
    return this.apiService.get(this.resource + '/' + photoOrderId).pipe(
      map( body => new PhotoOrder(body))
    );
  }

  /**
   * Adds a photo to a PhotoOrder.
   *
   * @param photoOrder The PhotoOrder
   * @param listingPhotos: New photos to add
   * @return Observable<any> of the response
   * @param productCode is optional. When sent we are modifying the product instance photos
   */
  addPhoto(photoOrder: PhotoOrder, listingPhotos: ListingPhoto[]): Observable<any> {
    return this.apiService.put(`${this.resource}/${photoOrder._id}/photos?push=true`, listingPhotos);
  }

  /**
   * Sends an update of the PhotoOrder for photos only.
   */
  setPhotos(photoOrder: PhotoOrder): Observable<any> {
    let url = `${this.resource}/${photoOrder._id}/photos`;
    return this.apiService.put(url, photoOrder.photos);
  }

  addPhotoFromUploadAndUpdatePhotos(photoOrder: PhotoOrder, photos: UploadPhoto[]) : Observable<any> {
    const userId = this.userService.getUserId();
    const listingPhotos: ListingPhoto[] = PhotoService.createFromRawPhotos(photos, userId, photoOrder.photos.length, photoOrder.photographerId);

    return this.addPhoto(photoOrder, listingPhotos).pipe(
      tap(photos => {
        photoOrder.photos = photos;
      })
    )}

  submitPhotos(photoOrderId: string, photoUrls: string[]) : Observable<any> {
    return this.apiService.post(this.photoSubmitResource, {photoOrderId: photoOrderId, photoUrls: photoUrls});
  }

  addMultimediaLink(marektingOrderId: string, photoOrderId: string, linkRequest: MultiMediaItemRequest): Observable<MultiMediaItem> {
    return this.apiService.put(this.resource + '/' + photoOrderId + '/addlink/' + marektingOrderId , linkRequest);
  }

  /**
   * This will fetch a order with media only.
   *
   * TODO - the API should be renamed to media via links
   *
   * @param photoOrderId the photo order id for this order
   * @param orderId the marketing order id
   * @param photographerId the photographer id to get media off the marketing order
   */
  getMedia(photoOrderId, orderId, photographerId): Observable<PhotoOrder> {
    return this.apiService.get(this.resource + '/' + photoOrderId + '/links/' + orderId + '/' + photographerId).pipe(
      // By instantiating a photo order object the model will correctly handle the conversion of attributes and sub
      // objects (i.e. dates in the audit)
      map(order => new PhotoOrder(order))
    );
  }

}
