import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, OktaAuthenticationProvider } from '@lc/core';
import { UserActivityService } from '../user-activity/user-activity-service';


@Component({
  selector: 'lc-app-okta-callback',
  templateUrl: './okta-callback.component.html',
  styleUrls: ['./okta-callback.component.scss']
})
export class OktaCallbackComponent implements OnInit {
  public errorMsg: any;
  public hasError = false;
  public gatewayReturnLink: string;

  constructor(
    private auth: AuthenticationService,
    private okta: OktaAuthenticationProvider,
    private router: Router,
    private userActivityService: UserActivityService
  ) {}

  ngOnInit() {
    // Handles the response from Okta and parses tokens
    const currentObject = this;

    this.okta.handleAuthentication().then(() => {
      currentObject.auth.redirectPostLogin();

      // We have a user, start monitoring activity
      this.userActivityService.startWatching();

    }).catch(function(error) {
      currentObject.errorMsg = error.message;
      currentObject.hasError = true;
      return;
    });

  }

  retry() {
    this.okta.logout();
    this.okta.login();
  }
}
