import {LogEntry, LoggerFactory} from "@lc/core";
import {ConsoleAppender} from "./console-appender";


class TestConsoleAppender extends ConsoleAppender {

  public called = false;

  write(str: string) {
    this.called = true;
  }
}

describe('ConsoleAppender', () => {

  const consoleAppender = new TestConsoleAppender();

  beforeEach(() => {


  });


  afterEach(() => {

    consoleAppender.called = false;

  });

  it('Empty string should be appended', () => {

    const entry = new LogEntry('DEBUG', 'default', '');
    consoleAppender.append(entry);

    expect(consoleAppender.called).toBeTruthy();
  });

  it('null string should be appended as empty', () => {

    const entry = new LogEntry('DEBUG', 'default', null);
    consoleAppender.append(entry);

    expect(consoleAppender.called).toBeTruthy();
  });

});

