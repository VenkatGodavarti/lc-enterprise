import axios from 'axios';
import { Context, ContextKeys } from '../support';
import { Configuration as config } from '../configuration';

function headers() {
  const authToken = Context.get(ContextKeys.apiAuthToken);

  return {
    'Authorization': `Bearer ${authToken}`,
    'Content-Type' : 'application/json',
    'Origin' : config.appURL,
    'Referer' : config.appURL
  };
}

export class http {
  static async get(url: string) {
    return await  axios({
      method: 'GET',
      url: url,
      headers: headers()
    });
  }

  static async post(url: string, json: string) {
    return await  axios({
      method: 'POST',
      url: url,
      headers: headers(),
      data: json
    });
  }

  static async put(url: string, json: string) {
    return await  axios({
      method: 'PUT',
      url: url,
      headers: headers(),
      data: json});
  }

  static async delete(url: string) {

    await  axios({
      method: 'DELETE',
      url: url,
      headers: headers()});
  }
}
