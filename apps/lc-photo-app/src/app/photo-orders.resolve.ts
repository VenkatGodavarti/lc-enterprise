import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { PhotoOrder } from '@lc/core';
import { Subscription, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { LoadPhotoOrders } from '../../../../libs/core/src/lib/state-mgmt/photo-orders/photo-order.actions';
import { selectAllOrders } from '../../../../libs/core/src/lib/state-mgmt/photo-orders/photo-order.reducer';

/**
 * This is not currently used but may be in the future to populate state before
 * executing components.
 */
@Injectable()
export class PhotoOrdersResolve implements Resolve<PhotoOrder>, OnDestroy {

  storeSub: Subscription;
  photoOrders: PhotoOrder[];

  constructor(private store: Store<any>) {
    const photoOrders$ = this.store.pipe(select(selectAllOrders));
    this.storeSub = photoOrders$.subscribe(photoOrders => {
      this.photoOrders = photoOrders;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const photographerId = route.paramMap.get('photographerId');
    // TODO - throw error to be handled by global error handler
    if (!photographerId) {
      return of(null);
    }

    // only load the marketing order if not already in memory store.
    if (!this.photoOrders || this.photoOrders.length === 0) {
      this.store.dispatch(new LoadPhotoOrders(photographerId));
    }
    // we are not using resolve injection of objects so just return null
    return of(null);
  }

  ngOnDestroy(): void {
    if (this.storeSub) {
      this.storeSub.unsubscribe();
    }
  }
}
