import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export class AgentInfo {
  agentId: string;
  fullName: string;
  constructor(agentId, public firstName, public lastName) {
    this.agentId = agentId;
    this.fullName = `${firstName} ${lastName}`;
  }
}
const mockedAgents: AgentInfo[] = [
  new AgentInfo('5c67331bb80e04683289fc85', 'Vincenzo', 'Wansley'),
  new AgentInfo('5c67331bb80e04683289fc84', 'Inocencia', 'Crabb'),
  new AgentInfo('5c67331bb80e04683289fc83', 'Lavonda', 'Riel'),
  new AgentInfo('5c67331bb80e04683289fc82', 'Larraine', 'Earnest'),
  new AgentInfo('5c67331bb80e04683289fc81', 'Elmer', 'Hose'),
  new AgentInfo('5c67331bb80e04683289fc80', 'Toni', 'Cowell'),
  new AgentInfo('5c67331bb80e04683289fc79', 'Clemmie', 'Kirchner'),
  new AgentInfo('5c67331bb80e04683289fc78', 'Yoko', 'Holdsworth'),
  new AgentInfo('5c67331bb80e04683289fc77', 'Ivory', 'Markham'),
  new AgentInfo('5c67331bb80e04683289fc76', 'Tracy', 'Eagle'),
  new AgentInfo('5c67331bb80e04683289fc75', 'Omer', 'Tavernia'),
  new AgentInfo('5c67331bb80e04683289fc74', 'Domonique', 'Greeno'),
  new AgentInfo('5c67331bb80e04683289fc73', 'Mel', 'Brinson'),
  new AgentInfo('5c67331bb80e04683289fc72', 'Berneice', 'Premo'),
  new AgentInfo('5c67331bb80e04683289fc71', 'Margy', 'Doner'),
  new AgentInfo('5c67331bb80e04683289fc70', 'Clora', 'Wilcoxson'),
  new AgentInfo('5c67331bb80e04683289fc69', 'Shaquita', 'Posner'),
  new AgentInfo('5c67331bb80e04683289fc68', 'Apolonia', 'Lobb'),
  new AgentInfo('5c67331bb80e04683289fc67', 'Antoine', 'Shimmin'),
  new AgentInfo('5c67331bb80e04683289fc66', 'Cherri', 'Lyda'),
];

@Injectable({
  providedIn: 'root'
})
export class TeamMemberService {

  constructor() { }

  searchAgents(searchTerm: string): Observable<AgentInfo[]> {
    searchTerm = (searchTerm || '').trim().toLowerCase();
    if(searchTerm === '') {
      return of([]);
    }
    const agents = mockedAgents.filter(agent => agent.fullName.toLowerCase().indexOf(searchTerm) > -1);
    return of(agents);
  }

  searchAgentsById(agentIds: string[]) {
    const agents = mockedAgents.filter(agent => agentIds.findIndex(id => id === agent.agentId) > -1);
    return of(agents);
  }
}
