
import { Then } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { browser } from 'protractor';
import { el } from '@angular/platform-browser/testing/src/browser_util';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^the "(.*)" (text|button|icon|element|tile|card) (exists|does not exist)$/, {timeout: config.timeout }, async (elementName, elementType, existsOrNot) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  let isPresent = false;
  try {
    const element = page.finder(elementName);
    isPresent = await element.isDisplayed();
  } catch(e) {
  }

  const expected = existsOrNot === 'exists';
  expect(isPresent).to.be.equal(expected);
});


Then(/^the "(.*)" (button|field|element) is (enabled|disabled)$/, {timeout: config.timeout }, async (elementName, elementType, enabledOrDisabled) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.finder(elementName);

  const isEnabled = await element.isEnabled();
  const expected = enabledOrDisabled === 'enabled';
  expect(isEnabled).to.be.equal(expected);
});

Then(/^the "(.*)" (link|button|icon|field|element|dialog) is (displayed|not displayed)$/, {timeout: config.timeout},async (elementName: string, elementType: string, present: string)=> {
  const displayed = present === 'displayed';
  let isDisplayed = false;
  try {
    const element = finder(elementName);
    isDisplayed = await element.isDisplayed();
  } catch(e) {}

  expect(isDisplayed).to.be.equal(displayed);
});

Then(/^the (.*) (link|button|icon|image|element|dialog|field|option) is (not present|present)$/, {timeout: config.timeout},async (elementName: string, elementType: string, present: string)=> {
  const element = finder(elementName);

  const checkPresent = present === 'present';
  const isPresent = await element.isPresent(); // TODO: Should this be isDisplayed()?
  expect(isPresent).to.be.equal(checkPresent);
});

Then(/^I click "(.*)" on the "(.*)"$/, {timeout: config.timeout},async (elementName: string, dialogName: string)=> {
  const elmnt = finder(elementName, dialogName);
  expect(elmnt).not.to.be.null;
  await elmnt.click();
});


Then(/^the (.*) option is (not highlighted|highlighted)$/, {timeout: config.timeout},async (elementName: string, highlighted: string)=> {

  const elmnt = element(elementName);

  const checkHighlighted = highlighted === 'highlighted';

  const isHighlighted =  (await elmnt.hasClass('option-selected')).result;
  expect(isHighlighted).to.be.equal(isHighlighted);
});


Then(/^there (is|are) (.*) "(.*)" elements in the "(.*)"$/, {timeout: config.timeout},async (isOrAre, expectedCount: number, childElementName: string, parentElementName: string)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const parentElement = finder(parentElementName);
  const childElementMap = page.getElement(childElementName);

  const children = parentElement.$$(childElementMap.field);

  await browser.wait(
    () => children.count().then(rowCount => rowCount === +expectedCount),
    10000, 'Element count did not match within expected time frame');
});


function element(elementName: string, subMapName?: string){
  const page = Context.get<LCPage>(ContextKeys.page);
  if(subMapName){
    return page.element(subMapName, elementName);
  }
  return page.element(elementName);
}


function finder(elementName: string, subMapName?: string) {
  return element(elementName, subMapName).finder();
}
