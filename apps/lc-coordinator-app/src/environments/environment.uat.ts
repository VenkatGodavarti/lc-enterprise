export const environment = {
  production: false,
  envName: 'uat',
  applicationName: 'Listing Concierge Coordinator App',
  applicationKey: 'lc-coordinator-app',

  // API/UI Endpoints
  baseURL: 'https://lc.uat.lc-coordinator.realogy.com/',
  apiBaseURL: 'https://lc.uat.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.uat.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.uat.lc-notify.realogy.com/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzuhf7e5lMI8f80h7',
  oktaRedirectUri: 'https://lc.uat.lc-coordinator.realogy.com/implicit/callback',
};
