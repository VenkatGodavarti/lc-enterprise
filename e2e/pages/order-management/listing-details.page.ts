import { LCPage, ElementMap, Element } from '../../support';

export class ListingDetailsPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Page Title', field: '#page-title'}),
      new Element({ name: 'Street', field: 'input#address-input'}),
      new Element({ name: 'Unit Number', field: 'input#unit-number-input'}),
      new Element({ name: 'City', field: 'input#city-input'}),
      new Element({ name: 'State', field: 'select#state-input'}),
      new Element({ name: 'Zip Code', field: 'input#zip-input'}),
      new Element({ name: 'Property Type', field: 'select#property-type-input'}),
      new Element({ name: 'Listing Status', field: 'select#status-input'}),
      new Element({ name: 'List Price', field: 'input#list-price-input'}),
      new Element({ name: 'MLS ID', field: 'input#mls-id-input'}),
      new Element({ name: 'Global Luxury Listing', field: 'input#listing-address-global-luxury-listing'}),
      new Element({ name: 'Bedroom Decrement', field: '#bedroom-decrement'}),
      new Element({ name: 'Bedroom Increment', field: '#bedroom-increment'}),
      new Element({ name: 'Bedroom Count', field: '#bedroom-count'}),
      new Element({ name: 'Bathroom Decrement', field: '#bathroom-decrement'}),
      new Element({ name: 'Bathroom Increment', field: '#bathroom-increment'}),
      new Element({ name: 'Bathroom Count', field: '#bathroom-count'}),
      new Element({ name: 'SQFT', model: 'listing.sqFt', field: 'input#sqFt'}),
      new Element({ name: 'Lot Size', model: 'listing.lotSqFt', field: 'input#lotSqFt'}),
      new Element({ name: 'List Date', field: 'input#listDate'}),
      new Element({ name: 'Closed Date', field: 'input#closedDate'}),
      new Element({ name: 'Cancel', field: 'button#listing-details-cancel'}),
      new Element({ name: 'Save', field: 'button#listing-details-submit'}),
      new Element({ name: 'Content Preview', field: 'iframe#print-preview-content'})
    ]
  );
  constructor() {
    super('Listing Details', [ListingDetailsPage.elements]);
  }

}
