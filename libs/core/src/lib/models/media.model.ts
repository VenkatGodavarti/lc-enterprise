import { Deserializable } from './deserializable.model';
import { MultiMediaItem} from './multi-media-item.model';

export class Media implements Deserializable<Media> {

  multimedia : MultiMediaItem[] = [];


  constructor( input?: any) {

    this.deserialize( input || {});

  }

  deserialize(input: any): Media {

    Object.assign(this, input);

    if (this.multimedia.length) {
      const items = [];
      this.multimedia.forEach( mm => {
        items.push(new MultiMediaItem(mm));
      });
      this.multimedia = items;
    }

    return this;
  }
}
