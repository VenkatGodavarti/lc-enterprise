import { Component, OnInit } from '@angular/core';
import { MarketingOrderService, ProductInstance, Address } from '@lc/core';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-product-dashboard',
  templateUrl: './product-dashboard.component.html',
  styleUrls: ['./product-dashboard.component.scss']
})
export class ProductDashboardComponent implements OnInit {
  address: Address;
  products$: Observable<ProductInstance[]>;

  constructor(private marketingOrderService: MarketingOrderService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const marketingOrderId = this.route.snapshot.queryParamMap.get('marketingOrderId');
    this.products$ = this.marketingOrderService.getOrder(marketingOrderId).pipe(
      tap(order => this.address = order.listing.address),
      map(order => order.getSelectedPackage().products)
    );
  }
}
