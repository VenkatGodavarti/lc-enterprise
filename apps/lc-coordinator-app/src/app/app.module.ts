import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { RouterModule } from '@angular/router';
import { LayoutWithoutSidenavComponent, SharedComponentsLayoutModule } from '@lc/shared-components-layout';
import { SharedComponentsAuthModule, OktaLoginComponent, OktaCallbackComponent, LogoutComponent } from '@lc/shared-components-auth';
import { CoreModule, StateModule, AuthGuard } from '@lc/core';
import { UserIdleModule } from 'angular-user-idle';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule.forRoot(),
    StateModule,
    SharedComponentsAuthModule,
    SharedComponentsLayoutModule,
    NxModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        component: LayoutWithoutSidenavComponent,
        canActivate: [AuthGuard],
        children: [
          { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard] },
          { path: 'manage-order/:id',
            loadChildren: '@lc/shared-components-order-mgmt#OrderManagementModule',
            data: {
              myListNav: '/orders/mine',
              myListLabel: 'Go To My Orders'
            },
            canActivate: [AuthGuard] }
        ]
      },{
        path: '',
        component: LayoutWithoutSidenavComponent,
        children: [
          { path: 'okta/login', component: OktaLoginComponent },
          { path: 'implicit/callback', component: OktaCallbackComponent },
          { path: 'logout', component: LogoutComponent },
          { path: 'my-profile', loadChildren: './my-profile/my-profile.module#MyProfileModule', canActivate: [AuthGuard] },
        ]
      },
    ]),

    // User activity timeouts (in seconds)
    // Ping value is not used
    UserIdleModule.forRoot({idle: 5 * 60, timeout: 30, ping: 10000})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
