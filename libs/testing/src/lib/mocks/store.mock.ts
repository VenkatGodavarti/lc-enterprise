import { BehaviorSubject, Observable, of } from 'rxjs';
import { Provider } from "@angular/core";
import { Store } from "@ngrx/store";
import { OperatorFunction } from 'rxjs/src/internal/types';

export class StoreMock<T> {
  private readonly states: { [selector: string]: BehaviorSubject<T>; }  = {};

  setState(selector: string, data: T) {
    const state = this.states[selector];
    if (!state) {
      this.states[selector] = new BehaviorSubject(data);
      return;
    }
    state.next(data);
  }

  select(selector?: any): Observable<T> {
    return this.states[selector].asObservable();
  }

  dispatch(action: any) {}

  pipe<A>(op1: OperatorFunction<T, A>): Observable<A> {
    return of(null);
  }
}

export const mockedStoreProvider: Provider = { provide: Store, useClass: StoreMock }
