import { LCPage, Element, ElementMap } from '../../../../e2e';

export class AppointmentListPage extends LCPage {
  static elements = new ElementMap( 'page',  [
      new Element({ name: 'Header', field: "h1"}),
      new Element({ name: 'Back to Photographers', field: "#back-button"}),
      new Element({ name: 'Appointments', field: "#appointments-table"}),
      new Element({ name: 'Start', field: '.start-button' }),
      new Element( { name: 'Prompt', field: '#prompt-dialog'}),
    ]
  );

  static dialogElements = new ElementMap( 'prompt dialog', [
      new Element( { name: 'Yes', field: '#yes-button'}),
      new Element( { name: 'No', field: '#no-button'}),
    ]
  );
  constructor() {
    super('Appointment List', [AppointmentListPage.elements, AppointmentListPage.dialogElements]);
  }
}
