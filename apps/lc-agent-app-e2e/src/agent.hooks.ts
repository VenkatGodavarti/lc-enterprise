import { BeforeAll } from 'cucumber';
import { Configuration } from '../../../e2e';
import { orderManagementPages } from './features/order-management/order-management.navigation';
import { pages as myProfile } from './step_definitions/my-profile/navigation';
import { listingDashboardPages } from './features/listing-dashboard/listing-dashboard.page';
import { orderListingPages } from './features/order-listing/order-listing.page';

/**
 *  Execute functions scheduled to be executed before a scenario runs
 */
BeforeAll( { timeout: Configuration.timeout }, () => {
  const environment = readConfiguration();

  // Register Configuration
  Configuration.appURL = environment.baseURL;
  Configuration.appLogoutURL = environment.baseURL + '/logout';
  Configuration.apiBaseURL = environment.apiBaseURL;
  Configuration.apiBaseURL = environment.apiBaseURL;
  Configuration.fileStorage = '../../apps/lc-agent-app-e2e/src/step_definitions/support/data';
  Configuration.appTitle = 'Listing Concierge';

  // Add all pages to the Navigation Map
  Configuration.addPageNavigation(myProfile);
  Configuration.addPageNavigation(orderManagementPages);
  Configuration.addPageNavigation(listingDashboardPages);
  Configuration.addPageNavigation(orderListingPages);
});

/**
 * Conditionally read configuration files. The CI/CD pipeline does not have an environment.mine file
 */
function readConfiguration() {
  const env = process.env.ENVIRONMENT || 'local';
  switch(env) {
    case 'development':
      const devEnvironment = require('../../lc-agent-app/src/environments/environment.development');
      return devEnvironment.environment;
    case 'qa':
      const qaEnvironment = require('../../lc-agent-app/src/environments/environment.qa');
      return qaEnvironment.environment;
    case 'ops':
      const opsEnvironment = require('../../lc-agent-app/src/environments/environment.ops');
      return opsEnvironment.environment;
    default:
      const localEnvironment  = require('../../lc-agent-app/src/environments/environment.mine');
      return localEnvironment.environment;
  }
}
