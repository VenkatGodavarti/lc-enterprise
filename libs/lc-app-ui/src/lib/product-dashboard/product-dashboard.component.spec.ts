import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ProductDashboardComponent } from './product-dashboard.component';
import {ProductCardGridComponent} from "./product-card-grid/product-card-grid.component";
import {ProductCardComponent} from "./product-card/product-card.component";
import { Listing, MarketingOrder, MarketingOrderService, PackageInstance } from '@lc/core';
import {RouterTestingModule} from "@angular/router/testing";
import {Observable, of} from "rxjs";

describe('ProductDashboardComponent', () => {
  let component: ProductDashboardComponent;
  let fixture: ComponentFixture<ProductDashboardComponent>;
  let marketingOrderServiceSpy;


  beforeEach(fakeAsync( () => {
    marketingOrderServiceSpy = jasmine.createSpyObj('MarketingOrderService', ['getOrder']);

    const packAge = new PackageInstance();
    packAge.title = "Silver";
    packAge.price = 453;
    packAge.description = "string";
    packAge.position = 0;
    packAge.code = '1234';
    packAge.products = [];

    const listing = new Listing();
    listing.marketingOrder = {
      listing: listing,
      selectedPackageCode: '1234',
    } as MarketingOrder;


    const mo = new MarketingOrder({
      listing: listing,
      selectedPackageCode: '1234',
      availablePackages: [packAge],
    });

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ ProductDashboardComponent, ProductCardGridComponent, ProductCardComponent],
      providers: [
        { provide: MarketingOrderService, useValue: marketingOrderServiceSpy }
      ]
    })
    .compileComponents();
    marketingOrderServiceSpy.getOrder.and.returnValue(of(mo));

    fixture = TestBed.createComponent(ProductDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tick(1000);

  }));

  it('should create', fakeAsync( () => {
    expect(component).toBeTruthy();
  }));
});
