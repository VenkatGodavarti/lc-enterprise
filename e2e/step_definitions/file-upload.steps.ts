import { Then, When } from 'cucumber';
import { config, Configuration } from '../configuration';
import { Context, ContextKeys, Element, LCPage } from '../support';
import * as path from "path";
import { browser, by, ExpectedConditions as EC } from 'protractor';
import { MyProfileMainPage } from '../../apps/lc-agent-app-e2e/src/step_definitions/my-profile/my-profile-main.page';
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

When(/^I upload a "(.*)" image$/, {timeout: config.timeout}, async (type: string, table) => {

  const page = Context.get<LCPage>(ContextKeys.page);

  // input data from feature file
  const row = table.raw();
  const formName = row[0][0];
  const inputName = row[0][1];
  const value = row[0][2];
  const absolutePath = path.resolve(__dirname, `${Configuration.fileStorage}/${value}`);

  // populate the file input with new data
  const form = page.findMap(formName);
  const fileElement: Element = form.element(inputName);
  const fileFinder = await fileElement.finder();
  await fileFinder.sendKeys(absolutePath);
  await page.clickEnter();
});

Then(/^I see the (.*) dialog/, {timeout: config.timeout}, async (element) => {

  const locator = MyProfileMainPage.elements._elements.find(el => {
    return el.name === element;
  });

  await browser.waitForAngular();
  const button = await browser.findElement(by.css(locator.field));
  const buttonText = await button.getText();
  expect(buttonText).to.be.equal('OK');
});
