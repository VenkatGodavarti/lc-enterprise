import { Deserializable } from './deserializable.model';
import { Audit } from '../models/audit.model';

export class MultiMediaItem implements Deserializable<MultiMediaItem> {

  _id: string;
  title: string;
  uri: string;
  // createdById is the photographerId
  createdById: string;
  audit: Audit;

  constructor( input?: any) {

    this.deserialize( input || {});
  }

  deserialize(input: any): MultiMediaItem {
    Object.assign(this, input);
    this.audit = new Audit(input.audit);
    return this;
  }
}

export class MultiMediaItemRequest extends MultiMediaItem {
  photoOrderId: string;
  orderId: string;

  constructor( input?: any) {
    super();
    this.deserialize( input || {});
  }

  deserialize(input: any): MultiMediaItem {
    Object.assign(this, input);
    if (input.audit) {
      this.audit = new Audit(input.audit);
    }
    return this;
  }
}
