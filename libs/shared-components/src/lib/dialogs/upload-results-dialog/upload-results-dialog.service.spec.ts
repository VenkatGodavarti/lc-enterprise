import { TestBed } from '@angular/core/testing';

import { UploadResultsDialogService } from './upload-results-dialog.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('UploadResultsDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ NgbModule ],
    providers: [ UploadResultsDialogService ]
  }));

  it('should be created', () => {
    const service: UploadResultsDialogService = TestBed.get(UploadResultsDialogService);
    expect(service).toBeTruthy();
  });
});
