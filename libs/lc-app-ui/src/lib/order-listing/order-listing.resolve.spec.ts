// import { TestBed } from '@angular/core/testing';
// import { RouterTestingModule } from '@angular/router/testing';
// import { Listing, UserService } from '@lc/core';
// import { OrderListingResolve } from './order-listing.resolve';
// import SpyObj = jasmine.SpyObj;
// import { Router } from '@angular/router';
// import { Store } from '@ngrx/store';
// import { StoreMock, TestingModule, UserServiceMock } from '@lc/testing';
//
// describe('OrderListingResolve', () => {
//   let userService: SpyObj<UserService>;
//   let router: Router;
//   let resolve: OrderListingResolve;
//
//   let store: StoreMock<any>;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         TestingModule
//       ],
//       providers: [
//         OrderListingResolve,
//       ]
//     }).compileComponents();
//
//     store = TestBed.get(Store);
//     userService = TestBed.get(UserService);
//     router = TestBed.get(Router);
//     resolve = TestBed.get(OrderListingResolve);
//   });
//
//   it('should be created', () => {
//     expect(resolve).toBeTruthy();
//   });
//
//   describe('#resolve', () => {
//     it('resolves by creating a new Listing', () => {
//       UserServiceMock.mockedUser._id = 'ABCD-EFGH-IJKL'
//       const route = new ActivatedRouteSnapshot();
//       route.params = {};
//
//       resolve.resolve(route, router.routerState.snapshot).subscribe((result: Listing) => {
//         expect(result).toBe(null);
//       });
//     });
//
//     it('resolves by retrieving an existing Listing', () => {
//       const listingId = 'ABCDEFGHIJKLMN';
//       const listing = new Listing();
//       listing._id = listingId;
//
//       spyOn(store, 'dispatch');
//       const route = new ActivatedRouteSnapshot();
//       route.params = {lid: listingId};
//       resolve.resolve(route, router.routerState.snapshot).subscribe((result: Listing) => {
//         expect(result._id).toBe(listingId);
//       });
//       expect(store.dispatch).toHaveBeenCalled();
//     });
//   });
// });
