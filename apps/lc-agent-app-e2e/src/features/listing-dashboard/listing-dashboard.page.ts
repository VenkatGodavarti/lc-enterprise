import { LCPage, ElementMap, Element } from '../../../../../e2e';
import { ListingAddressPage } from '../listing/address/listing-address.page';

export class ListingDashboardPage extends LCPage {
  static elements = new ElementMap( 'page',    [
    new Element({ name: 'Add Listing', field: '#add-new-listing-link'}),
    new Element({ name: 'Show Archived', field: '#show-archived-button'}),
    new Element({ name: 'Show Non Archived', field: '#show-non-archived-button'}),
    new Element({ name: 'Complete Order', field: '#complete-order-{orderId}'}),
    new Element({ name: 'Manage Order', field: '#manage-order-{orderId}'}),
    new Element({ name: 'Marketed As', field: '#marketedAs-input'}),
    new Element({ name: 'Listing Card', field: '.listing-card'}),
    new Element({ name: 'Listing Image', field: '.card-img-top'}),
    new Element({ name: 'Archive Order', field: 'button.top-left'}),
    new Element({ name: 'Agent Type', field: 'div.bottom-left'}),
    new Element({ name: 'Co-Listing Info', field: '#co-list-popover'}),
    new Element({ name: 'Team Info', field: '#team-popover'}),
    new Element({ name: 'Delete Order Button', field: '#delete-order-button'}),
    new Element({ name: 'Prompt', field: '#prompt-dialog'})
  ]);

  static promptDialogElement = new ElementMap( 'prompt dialog',
    [
      new Element( { name: 'Yes', field: '#yes-button'}),
      new Element( { name: 'Cancel', field: '#cancel-button'}),
    ]
  );

  constructor() {
    super('Listing Dashboard', [ListingDashboardPage.elements, ListingDashboardPage.promptDialogElement]);
  }
}

export const listingDashboardPages = {
  'listing dashboard': {
    'main': {
      url: '',
      page: new ListingDashboardPage()
    },
    'add new listing': {
      url: '/listing/add-new-listing',
      page: new ListingAddressPage()
    },
  }
};
