@products-services
@order-management
@login-before-all
Feature: Order Management

  Background:
    Given I am logged in to Listing Concierge
    Given the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Street               | 123 Main St           |
      | Unit Number          | 1                     |
      | City                 | Madison               |
      | State                | NJ                    |
      | Zip Code             | 07040                 |
      | Property Information | default               |
      | Photos               | none                  |
      | Status               | PENDING               |

    When I select the Alamo order
    And I go to the Summary page in Order Management
    Then the Summary page from Order Management is loaded

  @flyer-preview
  Scenario: The flyer preview displays
    When I go to the Flyer page in Order Management
    Then the Flyer page from Order Management is loaded
    And The Flyer Content Preview is present

  @website-preview
  Scenario: The website preview displays
    When I go to the Website page in Order Management
    Then the Website page from Order Management is loaded
    And The Website Content Preview is present
