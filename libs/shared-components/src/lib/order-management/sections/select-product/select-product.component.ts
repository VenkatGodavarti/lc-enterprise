import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Listing, ProductInstance } from '@lc/core';
import * as kebabCase from 'lodash.kebabcase';

@Component({
  selector: 'lc-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss']
})
export class SelectProductComponent implements OnInit, OnChanges {

  @Input() listing: Listing;

  @Input() products: ProductInstance[];

  mainProducts: object[] = [];

  ngOnInit() {

   this.init(this.products);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.products) {
      this.init(this.products);
    }
  }

  private init(products) {

    const extractedProducts = (products.length <= 3) ? [...products] : this.extractProducts(this.sortByCategory(products));
    this.mainProducts = extractedProducts.map( (pr) => {return {
      title: pr.title,
      status: pr.status,
      routerLink: '../products-services/' + kebabCase(pr.title) + '/' + pr.code,
      thumbnailUri: pr.thumbnailUri
    }});
  }

  /**
   *  Produce a map of products, organized by category.
   */
  private sortByCategory(products: ProductInstance[]): object  {
    const sorted = {};
    products.forEach(pr => {
      if (!sorted[pr.category]) {
        sorted[pr.category] = [];
      }
      sorted[pr.category].push(pr);
    });
    return sorted;
  }

  /**
   * pull out 3 different products from as many categories as possible for the card
   * - it should be the ones they use most, or something but we don't know yet.
   *
   * @param productsByCategory
   */
  private extractProducts(productsByCategory: object): ProductInstance[] {

    const products: ProductInstance[] = [];
    const indexByCategory = {};
    Object.keys(productsByCategory).forEach( category => indexByCategory[category] = 0);

    do {
      // TODO - 3 product limit? constant?
      Object.keys(indexByCategory).forEach(category => {
        if (products.length < 3 && indexByCategory[category] < productsByCategory[category].length) {
          products.push(productsByCategory[category][indexByCategory[category]]);
          indexByCategory[category]++;
        }
      });

    } while (products.length < 3);

    return products;
  }
}
