import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  TeamProfile,
  TeamProfileForm,
  SELECTEDTEAM,
  UpdateTeam,
  ImageUploaderService,
  UploadStatus,
  ThumbnailSizes,
  AgentInfo,
  TeamMemberService,
  TeamMemberForm,
  TeamMemberRole,
  UpdateTeamComplete,
  TeamActionTypes,
  TeamMember,
  PhotoService,
  UploadPhoto
} from '@lc/core';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { map, filter, switchMap, tap } from 'rxjs/operators';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { Masks, UploadResultsDialogService, ImageCropperDialogService, CropSize, ImageCropperResponse } from '@lc/shared-components-ui';
import { FormArray, FormControl } from '@angular/forms';
import { DragulaService } from 'ng2-dragula';
import { ActivatedRoute, Router } from '@angular/router';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'lc-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.scss']
})
export class TeamEditComponent implements OnInit, OnDestroy {

  @ViewChild('content')
  content: any;

  public readonly phoneMask = Masks.phoneNumber; // Input mask for phone number field

  form$: Observable<TeamProfileForm>; // TeamProfileForm retrieved from the state-mgmt
  private form: TeamProfileForm;
  private profile: TeamProfile;

  // dragula* fields are for re-ordering the team members
  dragulaSub: Subscription;
  dragulaGroup = 'TEAM_MEMBERS';
  dragulaModel: TeamMember[];

  memberSearch$: Observable<AgentInfo[]>; // suggestive search response
  searchControl: FormControl;             // suggestive search input
  newMembers: AgentInfo[] = [];           // array of agents chosen in selective search

  allowedExtensions = []; // image file extensions allowed (.png, .jpg, etc)

  photoStatus: UploadStatus; // Image upload status updated by the ImageUploaderService
  logoStatus: UploadStatus;  // Image upload status updated by the ImageUploaderService

  editPhoto: boolean; // helps template determine whether or not to display the edit icon on the team photo
  editLogo: boolean;  // helps template determine whether or not to display the edit icon on the team logo

  constructor(private store: Store<any>,
              private uploadResultsDialog: UploadResultsDialogService,
              private teamMemberService: TeamMemberService,
              private imageUploaderService: ImageUploaderService,
              private dragulaService: DragulaService,
              private readonly activatedRoute: ActivatedRoute,
              private readonly router: Router,
              private actionSubject: ActionsSubject,
              private photoService: PhotoService,
              private dialogService: ImageCropperDialogService) {
    this.form$ = store.pipe(
      select<TeamProfile>(SELECTEDTEAM),
      filter(model => model != null),
      map( model => this.profile = model),
      map(model => this.form = new TeamProfileForm(model)),
      tap(model => this.dragulaModel = this.form.get('members').value)
    );
    this.actionSubject.pipe(ofType<UpdateTeamComplete>(TeamActionTypes.UpdateTeamComplete))
      .subscribe(profileComplete => this.routeToTeamDisplay());

    this.initializeSearch();
    this.allowedExtensions = imageUploaderService.allowedExtensions.map((e) => ('.' + e));
  }

  // sets up dragula service events and handles results
  ngOnInit() {
    // subscribe to drag and drop changes in photo grid
    this.dragulaSub = this.dragulaService.dropModel(this.dragulaGroup)
      .subscribe(({ name, el, target, source, sibling, sourceModel, targetModel, item }) => {
        // We are not using many of these input values but here is what they each are
        // name = dragula group name
        // el, target, source, sibling = HTML elements being dragged and shuffled around
        // sourceModel, targetModel = dragulaModel assignment from the view/template
        // item = the TeamMemberForm object that was just drug to a new location

        // re-order the dragula model
        targetModel.forEach((member, index) => {
          member.position = index;
        });
        this.form.get('members').setValue(targetModel);
      });
  }

  // removes subscriptions before component is destroyed
  ngOnDestroy(): void {
    if(this.dragulaSub) {
      this.dragulaSub.unsubscribe();
    }
  }

  /**
   * Initializes the suggestive search form for finding agents
   */
  private initializeSearch() {
    this.searchControl = new FormControl();
    this.memberSearch$ = this.searchControl.valueChanges.pipe(
      switchMap(searchTerm => this.teamMemberService.searchAgents(searchTerm)),
    );
  }

  /**
   * Fired when the user clicks the 'Add' button for adding new members
   */
  addMembers() {
    for(const agent of this.newMembers) {
      const teamMember = new TeamMemberForm({
        agentId: agent.agentId,
        teamRole: TeamMemberRole.salesAgent,
        teamAdmin: false,
        position: this.form.get('members').value.length,
      });
      (this.form.get('members') as FormArray).push(teamMember);
    }
    this.newMembers = [];
    this.dragulaModel = this.form.get('members').value;
    this.initializeSearch();
  }

  /**
   * Removes the logo url from the profile and updates the profile because there is no another way to delete the logo
   */
  private deleteLogo() {
    this.editLogo = true;

    this.form.get('teamLogoURL').setValue(null);
    this.store.dispatch(new UpdateTeam(this.form.value));
  }

  onEditPhoto() {
    const photoUrl = this.form.get('teamPhoto.originalPhotoUrl').value;

    if(photoUrl) {
      // If there is an image, we need to load that as a default
      const metaData = this.form.get('teamPhoto.metaData').value;
      const aspectRatio = metaData && metaData.ratio ? metaData.ratio : "2";
      this.photoService.getImage(photoUrl)
        .subscribe(res => this.cropImage(res[0].body, aspectRatio));
    } else {
      // Otherwise, open up the cropper without an image
      this.cropImage();
    }
  }

  private cropImage(file?: any, aspectRatio = "2"){
    // Open up the image dialog with the crop size we want to preview it as
    this.dialogService.openImageCropper(file, [new CropSize('Avatar', 120, 120, false)], aspectRatio)
      .then(response => {
        if(!response) { return; }

        // If the user did an action, update appropriately
        if(response.isDelete) {
          this.deleteImage();
        } else if(response.croppedFile) {
          this.uploadImage(response);
        }
      });
  }

  private deleteImage() {
    this.updateImages(null, null, null);
  }

  private uploadImage(response: ImageCropperResponse) {
    if(!response){ return; }

    // Set the name on the cropped Blob or File
    (<any>response.croppedFile).name = "profilePhoto.png";

    // Upload images synchronously and then update the profile with both images
    combineLatest(this.photoService.upload(response.originalFile), this.photoService.upload(response.croppedFile))
      .subscribe(([originalPhotos, croppedPhotos]) => {
        // Find the original images from the response and then call update
        const original: UploadPhoto = originalPhotos.find((photo: any) => photo.original);
        const cropped: UploadPhoto = croppedPhotos.find((photo: any) => photo.original);
        this.updateImages(original.url, cropped.url, response.aspectRatio);
      });
  }

  private updateImages(originalUrl: string, croppedUrl: string, aspectRatio: string) {
    const metaData = aspectRatio ? { ratio: aspectRatio} : null;

    this.form.get('teamPhoto.originalPhotoUrl').setValue(originalUrl);
    this.form.get('teamPhoto.photoUrl').setValue(croppedUrl);
    this.form.get('teamPhoto.metaData').setValue(metaData);

    this.profile.teamPhoto.originalPhotoUrl = originalUrl;
    this.profile.teamPhoto.photoUrl = croppedUrl;
    this.profile.teamPhoto.metaData = metaData;

    // We need to first put it into a Profile Form,  which will only emit the values
    // that are available for the POST/PUT operations. Do not use the existing form on the component
    // as it could have pending changes.
    this.store.dispatch(new UpdateTeam(new TeamProfileForm(this.profile).value));
  }

  /**
   * Remove logo url from form only
   */
  replaceLogo() {
    this.editLogo = true;
    this.form.get('teamLogoURL').setValue(null);
  }

  /**
   * Fired when the user uploads a file through lc-file-upload InputField for the team logo
   * @param files the array of files uploaded through lc-file-upload
   */
  uploadTeamLogo(files: File[]) {
    // lc-file-upload allows for an array of files but we only want one
    this.logoStatus = new UploadStatus(files[0]);

    this.imageUploaderService.upload(this.logoStatus, ThumbnailSizes.STANDARD_THUMBNAIL_SPECS, true).subscribe((status) => {
      // TODO we don't yet have any requirements for error handling.  Opening a modal for now.
      if(status && status.error) {
        this.uploadResultsDialog.openResults([status.error])
          .then(() => this.clearUploadStatuses('teamLogo'));
      } else {
        const url = this.logoStatus.photos[0].url;
        this.form.get('teamLogoURL').setValue(url);
        this.profile.teamLogoURL = url;

        this.store.dispatch(new UpdateTeam(new TeamProfileForm(this.profile).value));

        this.clearUploadStatuses('teamLogo');
      }
    });
  }

  /**
   * Resets photo upload related statuses
   */
  private clearUploadStatuses(type: string) {
    switch (type) {

      case 'teamPhoto':
        this.editPhoto = false;
        this.photoStatus = null;
        break;

      case 'teamLogo':
        this.editLogo = false;
        this.logoStatus = null;
        break;

      default:
        break;
    }
  }

  /**
   * Routes to the team information page
   */
  private routeToTeamDisplay() {
    this.router.navigate(['..'], {relativeTo: this.activatedRoute});
  }

  /**
   * Fired when the cancel button is clicked
   */
  onCancel() {
    this.routeToTeamDisplay();
  }

  /**
   * Fired when the 'Save Changes' button is clicked
   * @param form the ReactiveForm that was edited by the user
   */
  onSubmit(form: TeamProfileForm) {
    this.form.markAllAsDirty();
    if(!this.form.valid) { return; }

    this.store.dispatch(new UpdateTeam(form.value));
    this.store.dispatch(new UpdateTeamComplete(form.value));
  }
}
