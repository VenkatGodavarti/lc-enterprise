import { ErrorHandler, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Angulartics2Module } from 'angulartics2';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthGuard } from './guards/auth.guard';

import { PhonePipe } from './pipes/phone.pipe';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

import { TokenInterceptor } from './interceptors/token.interceptor';

import { UserService } from './services/user.service';
import { PackageService } from './services/package.service';
import { MarketingOrderService } from './services/marketing-order.service';
import { ApiService } from './services/api.service';

import { PhotoService } from './services/photo.service';
import { LoggerFactory } from './logger/logger-factory';
import { ProfileService } from './services/profile.service';
import { PhotographerService } from './services/photographer.service';
import { AuthenticationService } from './services/authentication.service';
import { OktaAuthenticationProvider } from './services/auth-providers/okta-authentication.provider';
import { ErrorService, GlobalErrorHandler } from './errors';
import { NotificationEventService } from './notifications';
import { PhotoOrderService } from './services/photo-order.service';
import { OrderListingFlowStateService } from './services/order-listing-flow-service';
import { WindowService } from './services/window.service';
import { ProductGenerationService } from './services/product-generation.service';
import { PdfGenerationService} from './services/pdf-generation.service';
import { WebsiteGenerationService } from './services/website-generation.service';
import { PresignedPhotoDirective } from './directives/presigned-photo.directive';
import { TemplateService } from './services/template.service';
import { PhotoOrderStatusPipe } from './pipes/photo-order-status.pipe';
import { HeaderInterceptor } from './interceptors/header.interceptor';
import { OptionsService } from './services/options.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Angulartics2Module,
    NgbModule
  ],
  declarations: [
    PhonePipe,
    SafeHtmlPipe,
    PresignedPhotoDirective,
    PhotoOrderStatusPipe
  ],
  exports: [
    PhonePipe,
    SafeHtmlPipe,
    PresignedPhotoDirective,
    PhotoOrderStatusPipe
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthGuard,
        OktaAuthenticationProvider,
        AuthenticationService,
        PhotoService,
        UserService,
        PackageService,
        MarketingOrderService,
        ApiService,
        ProfileService,
        PhotographerService,
        ErrorService,
        NotificationEventService,
        GlobalErrorHandler,
        PhotoOrderService,
        OrderListingFlowStateService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HeaderInterceptor,
          multi: true
        },
        LoggerFactory,
        {
          provide: ErrorHandler,
          useClass: GlobalErrorHandler
        },
        WindowService,
        PdfGenerationService,
        WebsiteGenerationService,
        ProductGenerationService,
        TemplateService,
        OptionsService
      ]};
  }
}
