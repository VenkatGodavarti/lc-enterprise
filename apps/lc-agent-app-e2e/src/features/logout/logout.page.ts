import { LCPage, ElementMap, Element } from '../../../../../e2e';

export class LogoutPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Sign In', field: 'button#signin-button'}),
    ]
  );

  constructor() {
    super('Logout', [LogoutPage.elements]);
  }
}
