import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, filter, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { PhotoOrderService } from '../../services/photo-order.service';
import { ErrorData, GlobalErrorHandler } from '../../errors';
import { NotificationEvent, NotificationEventService } from '../../notifications';
import { ListingPhoto } from '../../models/listing-photo.model';
import { PhotoOrder } from '../../models/photo-order.model';
import { MultiMediaItem } from '../../models/multi-media-item.model';
import { select, Store } from '@ngrx/store';
import {
  AddPhotoOrderMultimediaLink,
  LoadPhotoOrderMedia,
  LoadPhotoOrderMediaComplete,
  LoadPhotoOrders,
  LoadPhotoOrdersComplete,
  PhotoOrderSelected,
  UpdatePhotoOrderPartial,
  PhotoOrderActionTypes,
  SubmitPhotoOrderPhotos,
  SubmitPhotoOrderPhotosComplete,
  UpdatePhotoOrder,
  UpdatePhotoOrderComplete,
  UpdatePhotoOrderPhotos,
  UpdatePhotoOrderPhotosComplete,
  UploadPhotoOrderPhotos,
  AddPhotoOrderMultimediaLinkComplete
} from './photo-order.actions';
import { selectCurrentOrder } from './photo-order.reducer';
import { BaseEffects } from '../base.effects';

/**
 * The order effects are post state change actions that occur
 */
@Injectable()
export class PhotoOrderEffects extends BaseEffects {

  constructor(private actions$: Actions,
              private photoOrderService: PhotoOrderService,
              private notificationService: NotificationEventService,
              private store: Store<any>,
              errorHandler: GlobalErrorHandler) {
    super(errorHandler)
  }

  /**
   * Take action on GetPhotoOrder actions
   *  1. Get the order
   *  2. If error, dispatch to error handler
   *  3. filter out chained error observables
   *  4. Send out success notification and return observable of order
   *
   */
  @Effect({ dispatch: false })
  getPhotoOrders: Observable<PhotoOrder[]> = this.actions$.pipe(
    ofType<LoadPhotoOrders>(PhotoOrderActionTypes.LoadPhotoOrders),
    switchMap((action) => {
      return this.photoOrderService.retrievePhotoOrders(action.payload).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.LoadPhotoOrders, action.payload, err)),
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((orders:PhotoOrder[]) => {
      const event = new NotificationEvent(PhotoOrderActionTypes.LoadPhotoOrdersComplete, PhotoOrderActionTypes.LoadPhotoOrdersComplete, orders);
      this.notificationService.getEventEmitter().emit(event);
      this.store.dispatch(new LoadPhotoOrdersComplete(orders));
      return orders;
    })
  );

  /**
   * Update the photo order
   */
  @Effect({ dispatch: false })
  updatePhotoOrder: Observable<PhotoOrder> = this.actions$.pipe(
    ofType<UpdatePhotoOrder>(PhotoOrderActionTypes.UpdatePhotoOrder),
    switchMap((action) => {
      return this.photoOrderService.updateOrder(action.payload).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.UpdatePhotoOrder, action.payload, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:PhotoOrder) => {
      const event = new NotificationEvent(PhotoOrderActionTypes.UpdatePhotoOrderComplete, PhotoOrderActionTypes.UpdatePhotoOrderComplete, order);
      this.notificationService.getEventEmitter().emit(event);
      this.store.dispatch(new UpdatePhotoOrderComplete(order));
      return order;
    })
  );

  @Effect({ dispatch: false })
  updatePhotoOrderPhotos: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<UpdatePhotoOrderPhotos>(PhotoOrderActionTypes.UpdatePhotoOrderPhotos),
    switchMap((action) => {
      action.payload.photos = action.photos;
      return this.photoOrderService.setPhotos(action.payload).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.UpdatePhotoOrderPhotos, action.payload, err)),
        map(photos => ({ photos: photos, action: action}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map((result) => {
      const event = new NotificationEvent(PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete, PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete);
      this.notificationService.getEventEmitter().emit(event);
      this.store.dispatch(new UpdatePhotoOrderPhotosComplete(result.action.payload, result.photos));
      return result.photos;
    })
  );

  @Effect({ dispatch: false })
  uploadPhotoOrderPhotos: Observable<ListingPhoto[]> = this.actions$.pipe(
    ofType<UploadPhotoOrderPhotos>(PhotoOrderActionTypes.UploadPhotoOrderPhotos),
    switchMap((action) => {
      return this.photoOrderService.addPhotoFromUploadAndUpdatePhotos(action.payload, action.newPhotos).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.UploadPhotoOrderPhotos, action.payload, err)),
        map(photos => ({ photos: photos, action: action}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map((result) => {
      const event = new NotificationEvent(PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete, PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete);
      this.notificationService.getEventEmitter().emit(event);
      this.store.dispatch(new UpdatePhotoOrderPhotosComplete(result.action.payload, result.photos));
      return result.photos;
    })
  );


  @Effect({ dispatch: false })
  submitPhotoOrderPhotos: Observable<string[]> = this.actions$.pipe(
    ofType<SubmitPhotoOrderPhotos>(PhotoOrderActionTypes.SubmitPhotoOrderPhotos),
    switchMap((action) => {
      return this.photoOrderService.submitPhotos(action.payload, action.photoUrls).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.SubmitPhotoOrderPhotos, action.payload, err)),
        map(urls => ({ photoUrls: urls}))
      )
    }),
    filter(listingPhotos => !(listingPhotos instanceof ErrorData)),
    map((result) => {
      const event = new NotificationEvent(PhotoOrderActionTypes.SubmitPhotoOrderPhotosComplete, PhotoOrderActionTypes.SubmitPhotoOrderPhotosComplete);
      this.notificationService.getEventEmitter().emit(event);
      this.store.dispatch(new SubmitPhotoOrderPhotosComplete(result.photoUrls));
      return result.photoUrls;
    })
  );

  /**
   * Partially update a photo order
   */
  @Effect({ dispatch: false })
  updateOrderPartial: Observable<PhotoOrder> = this.actions$.pipe(
    ofType<UpdatePhotoOrderPartial>(PhotoOrderActionTypes.UpdatePhotoOrderPartial),
    switchMap(({payload, fields}) => {
      return this.photoOrderService.updateOrderPartial(payload, fields).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.UpdatePhotoOrderPartial, {payload: payload, fields: fields}, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:PhotoOrder) => {
      // TODO - messages should be in constants file
      this.store.dispatch(new UpdatePhotoOrderComplete(order));

      const event = new NotificationEvent(PhotoOrderActionTypes.UpdatePhotoOrderComplete);
      this.notificationService.getEventEmitter().emit(event);
      return order;
    })
  );

  /**
   * When a photo order is selected, the change will invoke and if the order is not null
   * it will trigger a load of media
   */
  @Effect({ dispatch: false })
  selectPhotoOrder: Observable<any> = this.actions$.pipe(
    ofType<PhotoOrderSelected>(PhotoOrderActionTypes.PhotoOrderSelected),
    withLatestFrom(this.store.select(selectCurrentOrder), (action, order) => {
      return {
        action: action,
        order: order
      };
    }),
    map((payload: any) => {
      if (payload.order) {
        console.log('PhotoOrder selected, loading media');
        this.store.dispatch(new LoadPhotoOrderMedia(payload.order));
      }
      return payload;
    })
  );

  /**
   * Add a multimedia link to the photoorder. This will call the photo-order API
   */
  @Effect({ dispatch: false })
  addMultimediaLink: Observable<MultiMediaItem> = this.actions$.pipe(
    ofType<AddPhotoOrderMultimediaLink>(PhotoOrderActionTypes.AddPhotoOrderMultimediaLink),
    withLatestFrom(this.store.pipe(select<PhotoOrder>(selectCurrentOrder))),
    switchMap(([action, order]) => {
      return this.photoOrderService.addMultimediaLink(order.marketingOrderId, order._id, action.payload).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.AddPhotoOrderMultimediaLink, {payload: action.payload}, err))
      )
    }),
    filter(link => !(link instanceof ErrorData)),
    map((link:any) => new MultiMediaItem(link)),
    map((link:MultiMediaItem) => {
      this.store.dispatch(new AddPhotoOrderMultimediaLinkComplete(link));

      const event = new NotificationEvent(PhotoOrderActionTypes.AddPhotoOrderMultimediaLinkComplete);
      this.notificationService.getEventEmitter().emit(event);
      return link;
    })
  );

  /**
   * Add a multimedia link to the photoorder. This will call the photo-order API
   */
  @Effect({ dispatch: false })
  loadMedia: Observable<any> = this.actions$.pipe(
    ofType<LoadPhotoOrderMedia>(PhotoOrderActionTypes.LoadPhotoOrderMedia),
    switchMap(({payload}) => {
      return this.photoOrderService.getMedia(payload._id, payload.marketingOrderId, payload.photographerId).pipe(
        catchError(err => this.processCatchError(PhotoOrderActionTypes.AddPhotoOrderMultimediaLink, {payload: payload}, err))
      )
    }),
    filter(order => !(order instanceof ErrorData)),
    map((order:PhotoOrder) => {
      this.store.dispatch(new LoadPhotoOrderMediaComplete(order));
      const event = new NotificationEvent(PhotoOrderActionTypes.LoadPhotoOrderMediaComplete);
      this.notificationService.getEventEmitter().emit(event);
      return order;
    })
  );
}
