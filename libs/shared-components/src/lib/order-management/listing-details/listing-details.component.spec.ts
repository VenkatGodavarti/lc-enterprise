import { async, ComponentFixture, TestBed, inject, fakeAsync, flush } from '@angular/core/testing';

import { ListingDetailsComponent } from './listing-details.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MarketingOrderFactory } from '../../../../../core/src/lib/factories';
import { By } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { ORDERSTATE, FlowState, UpdateOrderListing, SELECTEDORDER, MarketingOrder } from '@lc/core';
import { StoreMock, TestingModule } from '@lc/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';

describe('ListingDetailsComponent', () => {

  let component: ListingDetailsComponent;
  let fixture: ComponentFixture<ListingDetailsComponent>;

  let marketingOrder: MarketingOrder;
  let updatedOrder: MarketingOrder;
  let store: StoreMock<any>;

  beforeEach(async(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    updatedOrder = null;

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule
      ],
      declarations: [ ListingDetailsComponent ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({ })
              }
            }
          }
        }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, marketingOrder); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));


  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(ListingDetailsComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should update on submit', async() => {
    const button = fixture.debugElement.query(By.css('#listing-details-submit'));
    spyOn(store, 'dispatch').and.callFake((updateOrderListing: UpdateOrderListing) => {
      updatedOrder = updateOrderListing.payload;
    });
    const router = TestBed.get(Router)
    spyOn(router, 'navigate').and.callFake(() => {});

    expect(component.detailsForm.valid).toBe(true);
    expect(button).toBeTruthy();
    button.triggerEventHandler('click', { button: 0 });

    expect(store.dispatch).toHaveBeenCalled();
    expect(updatedOrder.listing.address.streetAddress1).toEqual(marketingOrder.listing.address.streetAddress1);
  });
});
