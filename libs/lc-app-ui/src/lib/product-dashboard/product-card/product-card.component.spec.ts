import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {ProductCardComponent} from './product-card.component';
import {MarketingOrderService, ProductInstance} from '@lc/core';
import {Component, ViewChild} from "@angular/core";

describe('ProductCardComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let marketingOrderServiceSpy;

  beforeEach(fakeAsync(() => {
    marketingOrderServiceSpy = jasmine.createSpyObj('MarketingOrderService', ['getOrder']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        { provide: MarketingOrderService, useValue: marketingOrderServiceSpy }
      ],
      declarations: [ TestHostComponent, ProductCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.productCard.product = component.createProduct();
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


@Component({
  selector: 'lc-test-host',
  template: '<lc-product-card></lc-product-card>'
})
class TestHostComponent {
  @ViewChild(ProductCardComponent)
  public productCard: ProductCardComponent;

  createProduct() {
    const prd = new ProductInstance();

    prd.title = 'Silver';

    prd.price = 999.00;
    prd.unitQuantity = 25;

    return prd;
  }
}
