
import {Given, Then} from 'cucumber';
import { Context, ContextKeys, Scheduler, Schedule, ProfileClient, Configuration } from '../../../../../../e2e';
import { BaseData } from '../../../../../../e2e/data/base-data';

Then(/^I have no profile addresses$/, {timeout: Configuration.timeout}, async () => {

  let  user = Context.get(ContextKeys.user);

  const profile = user.profile;

  // After the scenario, restore the profile to its original set of addresses
  //
  Scheduler.schedule(Schedule.afterScenario, async () => {
    //console.log('Restoring profile addresses to initial state');
    await ProfileClient.update(profile._id, { contactInfo: profile.contactInfo })
  });

  await ProfileClient.update(profile._id, { contactInfo:  profile.contactInfo.filter( c => c.type !== 'card') });

  user = await ProfileClient.getCurrentUser();

  Context.set(ContextKeys.user, user);
});

Given(/^the user profile addresses contain (.*)$/,  { timeout: Configuration.timeout },  async (data: string) => {
  let newProfileData = readProfileDataFromFile(data);

  let  user = Context.get(ContextKeys.user);

  const profile = user.profile;
  profile.contactInfo = newProfileData;

  await ProfileClient.update(profile._id, { contactInfo: newProfileData });

  user = await ProfileClient.getCurrentUser();
  Context.set(ContextKeys.user, user);
});

function readProfileDataFromFile(profile) {
  if(!profile) {
    throw Error('orderName must be specified');
  }
  const fileName = profile.replace(' ', '-').toLowerCase();
  const profileData = BaseData.readFileToObject(`profile/data/${fileName}.json`);
  return profileData;
}
