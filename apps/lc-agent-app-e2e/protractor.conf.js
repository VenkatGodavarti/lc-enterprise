// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const config = require('../../protractor.conf').config;


config.baseUrl = process.env.BASE_URL || 'http://localhost:4200/',

// Specs here are the cucumber feature files
config.specs = [
  'src/**/*.feature'
];

config.cucumberOpts = {
  // require step definition files before executing features
  require: [
    'src/**/*.ts',
    '../../e2e/**/*.ts'
  ],
  // <string[]> (expression) only execute the features or scenarios with tags matching the expression
  tags: [],
  // <boolean> fail if there are any undefined or pending steps
  strict: true,
  // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
  format: [
    'json:../../reports/e2e/apps/lc-agent-app/summary.json'
  ],
  // <boolean> invoke formatters without executing steps
  dryRun: true,
  // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  compiler: []
};

// Enable TypeScript for the tests
config.onPrepare = function() {
  require('ts-node').register({
    project: require('path').join(__dirname, './tsconfig.e2e.json')
  });
};

exports.config = config;
