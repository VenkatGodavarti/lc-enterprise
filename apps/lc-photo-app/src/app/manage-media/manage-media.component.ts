import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotoOrder, PhotoOrderSelected, selectCurrentOrder } from '@lc/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-manage-media',
  templateUrl: './manage-media.component.html'
})
export class ManageMediaComponent implements OnInit {

  private navLinks: any[];
  private activeId: string;

  private photographerName: string;

  photoOrder$: Observable<PhotoOrder>;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {

    if (this.activatedRoute.snapshot.data && this.activatedRoute.snapshot.data['navLinks']) {
      this.navLinks = this.activatedRoute.snapshot.data['navLinks'];
      this.activeId = this.activatedRoute.snapshot.data['activeId'];
    }
  }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('photoOrderId');
    this.store.dispatch(new PhotoOrderSelected(id));
    this.photographerName = this.activatedRoute.snapshot.queryParamMap.get('photographerName');
    this.photoOrder$ = this.store.select(selectCurrentOrder);
  }

}
