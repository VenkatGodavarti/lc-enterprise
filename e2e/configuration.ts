import { NavigationMap } from './support/navigation-map';

export const config = {
  timeout: 25000,
  username: process.env.LC_USERNAME,
  password: process.env.LC_PASSWORD,
};

export class Configuration {
  static timeout = config.timeout;
  static username = config.username;
  static password = config.password;
  static fileStorage = './support/data';
  static appURL: string;

  static appTitle: string;
  static appLogoutURL: string;
  static apiBaseURL: string;

  static addPageNavigation(navigation) {
    NavigationMap.add(navigation);
  }
}
