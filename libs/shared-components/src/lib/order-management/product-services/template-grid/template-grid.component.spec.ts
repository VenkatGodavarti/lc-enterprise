import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateGridComponent } from './template-grid.component';
import {By} from "@angular/platform-browser";
import { Component, ViewChild } from '@angular/core';
import { TemplateInstance } from '@lc/core';

describe('TemplateGridComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let templates;
  let selectedTemplate;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestHostComponent, TemplateGridComponent ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;

    templates = TestHostComponent.createDefaultTemplates();
    selectedTemplate = null;
    component.templateGrid.templates = templates;

    fixture.detectChanges();
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('has cards for each template', () => {
    const templateCards = fixture.debugElement.queryAll(By.css('div.template-card'));
    expect(templateCards).toBeTruthy();
    expect(templateCards.length).toEqual(templates.length);
  });


  it('has thumbnails for each template', () => {
    const templateImgs = fixture.debugElement.queryAll(By.css('img'));
    expect(templateImgs).toBeTruthy();
    expect(templateImgs.length).toEqual(templates.length);
    for (let i = 0; i < templates.length; i++) {
      expect(templateImgs[i].nativeElement.src.endsWith(templates[i].thumbnailURL)).toBeTruthy();
    }
  });

});

@Component({
  selector: 'lc-test-host',
  template: '<lc-template-grid></lc-template-grid>'
})

class TestHostComponent {
  @ViewChild(TemplateGridComponent)
  public templateGrid: TemplateGridComponent;

  static createDefaultTemplates() {

    return [
      new TemplateInstance({
        title: 'Flyer 1',
        sides: 2,
        numberOfPhotos: 5,
        thumbnailURL: '/assets/images/flyer01@3x.png'
      }),

      new TemplateInstance({
        title: 'Flyer 2',
        sides: 2,
        numberOfPhotos: 5,
        thumbnailURL: '/assets/images/flyer02@3x.png'
      }),

      new TemplateInstance({
        title: 'Flyer 3',
        sides: 2,
        numberOfPhotos: 5,
        thumbnailURL: '/assets/images/flyer03@3x.png'
      })
    ];
  }
}
