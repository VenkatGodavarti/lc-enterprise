import { Notification, NotificationType } from '../models';

export class NotificationFactory {
  private static id = 1;
  public static get assignedToOrder() { return NotificationFactory.createNotification(NotificationType.ASSIGNED_TO_ORDER, 'New Listing', 'John added you as a Co-Lister on an order', '5cdec676914ebd45004dedb2'); }
  public static get photosRequested() { return NotificationFactory.createNotification(NotificationType.REQUESTED_PHOTOS, 'Photos Requested', 'Alice has requested photos', '5ce3ee8bd3478e3954321407'); }
  public static get marketingCopyRequested() { return NotificationFactory.createNotification(NotificationType.REQUESTED_MARKETING_COPY, 'Marketing Copy Requested', 'Alice has requested a marketing copy', '5ce3ee8bd3478e3954321407'); }
  public static get assignedToTeam() { return NotificationFactory.createNotification(NotificationType.ASSIGNED_TO_TEAM, `You're on the team!`, 'Michael has added you to his team'); }


  public static createAll(): Notification[] {

    const notifications = [];
    notifications.push(NotificationFactory.assignedToOrder);
    notifications.push(NotificationFactory.photosRequested);
    notifications.push(NotificationFactory.marketingCopyRequested);
    notifications.push(NotificationFactory.assignedToTeam);

    return notifications;
  }

  public static createNotification(type: NotificationType, subject: string, text: string, marketingOrderId?: string) {
    const notification: Notification = new Notification({
      _id: (NotificationFactory.id++).toString(),
      subject: subject,
      text: text,
      notificationType: type,
    });
    return notification;
  }
}
