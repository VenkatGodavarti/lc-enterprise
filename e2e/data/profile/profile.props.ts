import { ModelMap, ModelProperty, KeyModelProperty, FunctionModelProperty } from '../../support/model.map';

/**
 *  Maps user-friendly names to methods for setting properties on a profile object usign data from a datatable
 *  in a cucumber test
 */
export class ProfileProps extends ModelMap {

  private static props: ModelProperty[] = [
    new KeyModelProperty('First Name', 'preferredFirstName'),
    new KeyModelProperty('Last Name',  'preferredLastName'),
    new KeyModelProperty('Email',      'preferredEmail'),
    new KeyModelProperty('Office',     'commonProfile.officeAddress.name'),
    new FunctionModelProperty('Primary Phone', setPrimaryPhone)
  ];

  constructor() {
    super('profile', ProfileProps.props);
  }
}

function setPrimaryPhone(profile: any, phone: string): void {

  profile.phoneNumbers[0].number = phone;
}
