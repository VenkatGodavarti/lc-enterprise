import { FormControl } from '@angular/forms';
import { BaseForm } from './base.form';
import { AddressForm } from './address.form';
import { PhoneNumber } from '../models/phone-number.model';
import { Address } from '../models/address.model';
import { PhoneNumberForm } from './phone-number.form';

export class AddressInformationForm extends BaseForm {
   public get fullName(): string { return this.get('fullName').value; };
   public get address(): Address { return this.get('address').value; };
   public get company(): string { return this.get('company').value; };
   public get phoneNumber(): PhoneNumber { return this.get('phoneNumber').value; };
   public get email(): string { return this.get('email').value; };
   public get type(): string { return this.get('type').value; };

  constructor(value?: any) {
    super({
      fullName: new FormControl(),
      company: new FormControl(),
      email: new FormControl(),
      address: new AddressForm(value ? value.address : null),
      phoneNumber: new PhoneNumberForm(value ? value.phoneNumber : null),
      type: new FormControl()
    });

    if(value) {
      this.patchValue(value);
    }
  }
}
