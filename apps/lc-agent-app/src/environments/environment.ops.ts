export const environment = {
  production: false,
  envName: 'ops',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'https://lc-agent.ops.concierge.realogydev.com/',
  apiBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-persist/api/v1/',
  photosBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-image/api/v1/',
  notifyBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-notify/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzup50uTH2Rg7j0h7',
  oktaRedirectUri: 'https://lc-agent.ops.concierge.realogydev.com/implicit/callback',
};
