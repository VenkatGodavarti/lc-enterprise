import { Deserializable } from './deserializable.model';
import { PhoneNumber } from './phone-number.model';
import { Address } from './address.model';
import { AddressType } from './address.type';

export class StateTransition implements Deserializable<StateTransition> {

  transitionedTo: string;
  transitionedAt: Date;

  constructor(input?: any) {

    this.deserialize(input || {});
  }

  deserialize(input: any): StateTransition {
    Object.assign(this, input);

    return this;
  }
}
