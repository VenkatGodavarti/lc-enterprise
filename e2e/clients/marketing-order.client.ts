import { http } from './http';
import { Configuration as config } from '../configuration';

export class MarketingOrdersClient {
  static async post(marketingOrder: any) {
    return http.post(`${config.apiBaseURL}/marketing-orders`, marketingOrder);
  }

  static async put(marketingOrder: any) {
    return http.put(`${config.apiBaseURL}/marketing-orders/${marketingOrder._id}`, marketingOrder);
  }

  static async submit(marketingOrder: any) {
    return http.put(`${config.apiBaseURL}/marketing-orders/${marketingOrder._id}/submit`, marketingOrder);
  }

  static async delete(marketingOrderId: string) {
    return http.delete(`${config.apiBaseURL}/marketing-orders/${marketingOrderId}`);
  }

  static async updateOrderState(orderId, orderState) {
    return http.put(`${config.apiBaseURL}/marketing-orders/${orderId}/orderstate`, orderState);
  }
}
