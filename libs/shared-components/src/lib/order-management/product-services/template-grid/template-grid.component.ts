import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { TemplateInstance, ProductInstance } from '@lc/core';

@Component({
  selector: 'lc-template-grid',
  templateUrl: './template-grid.component.html',
  styleUrls: ['./template-grid.component.scss']
})
export class TemplateGridComponent implements OnInit {

  @Input()
  product: ProductInstance;

  @Input()
  templates: TemplateInstance[];

  @Output()
  onSelect = new EventEmitter<TemplateInstance>();

  constructor() { }

  ngOnInit() {
  }

  selectTemplate(template: TemplateInstance) : void {
    this.onSelect.emit(template);
  }
}
