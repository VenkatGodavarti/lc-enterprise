export const environment = {
  // App configuration
  production: false,
  envName: 'local',
  applicationName: 'Listing Concierge Photo App LocalHost',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://localhost:4700/',
  apiBaseURL: 'https://lc.dev.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.dev.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.dev.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajcwwe6zYnqyhEA0h7',
  oktaRedirectUri: 'http://localhost:4700/implicit/callback',
};
