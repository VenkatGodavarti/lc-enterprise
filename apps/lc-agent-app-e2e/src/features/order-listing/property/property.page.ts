import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const orderListingPropertyElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Property Type', field: "#property-type-input" }),
    new Element({ name: 'Listing Status', field: "#status-input" }),
    new Element({ name: 'Bedroom Decrement', field: "#bedroom-decrement" }),
    new Element({ name: 'Bedroom Increment', field: "#bedroom-increment" }),
    new Element({ name: 'Bedroom Count', field: "#bedroom-count" }),
    new Element({ name: 'Bathroom Decrement', field: "#bathroom-decrement" }),
    new Element({ name: 'Bathroom Increment', field: "#bathroom-increment" }),
    new Element({ name: 'Bathroom Count', field: "#bathroom-count" }),
    new Element({ name: 'List Price', field: "#list-price-input" }),
    new Element({ name: 'Square Feet', field: "#sqFt" }),
    new Element({ name: 'Lot Size', field: "#lotSqFt" }),
    new Element({ name: 'List Date', field: "#listDate" }),
    new Element({ name: 'Close Date', field: "#closedDate" }),
    new Element({ name: 'Next', field: "#nextBtn" }),
  ]
);


export class OrderListingPropertyPage extends LCPage {
  constructor() {
    super('Order Listing Property', [orderListingPropertyElements]);
  }
}
