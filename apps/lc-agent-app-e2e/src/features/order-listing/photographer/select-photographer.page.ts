import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const selectPhotographerElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Cutting Edge', field: "#card-option-0" }),
    new Element({ name: 'Fabulous Photo', field: "#card-option-1" }),
    new Element({ name: 'Quick Photo', field: "#card-option-2" }),
    new Element({ name: 'Next', field: "#nextBtn" }),
  ]
);


export class SelectPhotographerPage extends LCPage {
  constructor() {
    super('Select Photographer', [selectPhotographerElements]);
  }
}
