import { Then } from "cucumber";
import { Context, ContextKeys, Configuration as config, LCPage  } from '../../../../../../e2e';
import { browser } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^the "Agent Name" is set to the current user$/, {timeout: config.timeout }, async () => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const input = page.finder("Agent Name");
  const user = Context.get(ContextKeys.user);


  const text = await input.getAttribute('value');
  expect(text).to.be.equal(`${user.firstName} ${user.lastName}`);
});
