import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';

@Injectable()
export class PhotographerService {

  constructor(private http: HttpClient, private apiService: ApiService) { }

  /* istanbul ignore next */
  retrievePhotographers() {
    return this.apiService.get('photographers');
  }
}
