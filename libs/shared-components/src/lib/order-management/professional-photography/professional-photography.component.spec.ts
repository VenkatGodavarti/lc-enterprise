import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../core/src/lib/factories';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;
import { ProfessionalPhotographyComponent } from './professional-photography.component';
import { AuthenticationService, PhotographerAppointment, Photographer, Lockbox } from '@lc/core';

describe('ProfessionalPhotographyComponent', () => {

  let component: ProfessionalPhotographyComponent;
  let fixture: ComponentFixture<ProfessionalPhotographyComponent>;

  let store: SpyObj<Store<any>>;
  let authSvc: SpyObj<AuthenticationService>;

  let marketingOrder;

  beforeEach(async(() => {

    store = jasmine.createSpyObj('Store', ['pipe']);
    authSvc = jasmine.createSpyObj('AuthenticationService', ['getToken']);

    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);

    marketingOrder.availablePackages = PackageInstanceFactory.createAll();
    marketingOrder.selectedPackageCode = marketingOrder.availablePackages[1].code;
    const appt = new PhotographerAppointment();

    appt.photographer = new Photographer().deserialize({
      name: 'The Photo Guy',
      logoUri: '/logo-uri-here'
    });
    appt.scheduledAt = new Date();
    appt.lockboxAvailable = true;
    appt.lockbox = new Lockbox().deserialize({
      code: 'code',
      location: 'location'
    });
    appt.comments = 'A comment here';

    marketingOrder.getSelectedPackage().getPhotographyProduct().serviceAppointment = appt;

    store.pipe.and.returnValue(of(marketingOrder));
    authSvc.getToken.and.returnValue('test-token');

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ ProfessionalPhotographyComponent ],
      providers: [
        {provide: Store, useValue: store},
        {provide: AuthenticationService, useValue: authSvc}
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();


    fixture = TestBed.createComponent(ProfessionalPhotographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  })


  it('should show appointment data', () => {

    const img = fixture.debugElement.query(By.css('#photographer-logo-img'));
    expect(img).toBeTruthy();
    expect(img.nativeElement.src.includes(component.appointment.photographer.logoUri)).toBeTruthy();
  });
});
