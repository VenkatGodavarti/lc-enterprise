@login-before-all
@photography-orders
Feature: I want to upload photos as a photographer

Background:
  Given I am logged in to Listing Concierge

  Given the following orders exist
  | Name                       | Alamo                 | White House           |
  | Package                    | Gold                  | Platinum              |
  | Status                     | PENDING               | PENDING               |
  | Photo Order                | Quick Photos          | Quick Photos          |

  Given the photo orders have the following photos
  | orderName           | photos                              | status  |
  | Alamo               | kitchen, living room, dining room   |         |

Scenario: Verify that the page navigation works
  Then the Select Photographer page from Main is loaded

  When I wait for the Header element to load
  Then the "Header" contains "Please select your photographer"

  And the "Next" button is disabled

  When I click on the Quick Photos button
  Then the "Next" button is enabled

  When I click on the Next button
  Then the Appointment List page from Main is loaded
  And the "Header" contains "Photography Appointment Requests"
  And the "Appointments" table has the columns "Order Number, Order Date, Agent, Scheduled, Property Address, Status, Action"

  # Note: Current sort is based on record creation time?
  When the "Appointments" table records load
  Then the "Appointments" table has the following data
  | Property Address                                | Status   | Order Date | Scheduled        |
  | 300 Alamo Plaza, San Antonio, TX                | To Do    | 5/1/19     | 5/11/19, 8:00 AM |
  | 1600 Pennsylvania Ave NW, Washington, DC        | To Do    | 5/1/19     | 5/11/19, 8:00 AM |

  When I click on the "Start" button in row "1" of the "Appointments" table

  Then the "Prompt" dialog is displayed
  When I click on the "Yes" button inside the "Prompt Dialog"
  Then the "Prompt" dialog is not displayed

  # TODO: Write these as reusable steps for "Photo Upload" steps
  Then the Photos page from Manage Media is loaded
  Then there are 3 "Photo" elements in the "Unsubmitted Grid"

  # Test file uploader. TODO: This is working, except there is no cleanup on images. Commented out for now
  # When I drag and drop the "home" file onto the "Drop Zone" uploader
  # Then there is 1 "Photo" elements in the "Unsubmitted Grid"
