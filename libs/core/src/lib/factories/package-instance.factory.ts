import { PackageInstance } from '../models';

const silverPkg = {
  title: 'Silver',
  code: '001',
  description: 'Silver',
  position: 1,
  price: 99,
  products: [
    {
      title: 'Flyer',
      code: '100',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Just Listed Postcard',
      code: '103',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Brochure',
      code: '104',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    }
  ]
};

const goldPkg = {
  title: 'Gold',
  code: '002',
  description: 'Gold',
  position: 1,
  price: 299,
  products: [
    {
      title: 'Flyer',
      code: '100',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Professional Photography',
      code: '101',
      kind: 'service',
      category: 'multimedia',
      thumbnailUri: '123456',
      maxOptionsSelectable: 2,
      options: [
        {
          title: 'Flyover',
          description: 'Flyover Video',
          selected: false,
          assets: {selected: 's1', unselected: 's2'}
        },
        {
          title: 'Upgraded',
          description: 'Upgraded Photography',
          selected: false,
          assets: {selected: 's1', unselected: 's2'}
        },
        {
          title: 'Zillow',
          description: 'Showcase Video with Zillow Boost',
          selected: false,
          assets: {selected: 's1', unselected: 's2'}
        }
      ]
    },
    {
      title: 'Just Listed Postcard',
      code: '103',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Brochure',
      code: '104',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    }
  ]
};


const platinumPkg = {
  title: 'Platinum',
  code: '003',
  description: 'Platinum',
  position: 1,
  price: 499,
  products: [
    {
      title: 'Flyer',
      code: '100',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Professional Photography',
      code: '101',
      kind: 'service',
      category: 'multimedia',
      thumbnailUri: '123456'
    },
    {
      title: 'Just Listed Postcard',
      code: '103',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    },
    {
      title: 'Brochure',
      code: '104',
      kind: 'product',
      category: 'print',
      thumbnailUri: '123456'
    }
  ]
};

export class PackageInstanceFactory {
  public static silver = new PackageInstance(silverPkg);
  public static gold = new PackageInstance(goldPkg);
  public static platinum = new PackageInstance(platinumPkg);

  public static createAll(): PackageInstance[] {

    const packages = [];
    packages.push(new PackageInstance(silverPkg));
    packages.push(new PackageInstance(goldPkg));
    packages.push(new PackageInstance(platinumPkg));

    return packages;
  }
}
