/*
 * browser hooks
 */
import { browser } from 'protractor';

module.exports = function() {

// If there is a global before
//   this.before(function(scenario, callback) {
//     browser.manage().window().maximize();
//     browser.get(url);
//     callback();
//   });

// Take screenshot if scenario fails
  this.After(function (scenario, callback) {
    if (scenario.isFailed()) {
      browser.takeScreenshot().then(function (base64png) {
        const decodedImage = new Buffer(base64png, 'base64').toString('binary');
        scenario.attach(decodedImage, 'image/png');
        callback();
      }, function (err) {
        callback(err);
      });
    } else {
      callback();
    }
  });
};
