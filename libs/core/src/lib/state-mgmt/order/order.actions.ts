import { Action } from '@ngrx/store';
import { Listing, ListingPhoto, MarketingOrder, PhotoOrder, ProductDescription, UploadPhoto } from '@lc/core';
import { OrderState } from '../../models/listing.model';
import { MultiMediaItemRequest, MultiMediaItem } from '../../models/multi-media-item.model';


export enum OrderActionTypes {
  LoadOrder = '[Order] Load',

  GetOrder = '[Order] Get',
  GetOrderComplete = '[Order] Get Complete',

  AddOrder = '[Order] Add',
  AddOrderComplete = '[Order] Add Order Complete',

  UpdateOrder = '[Order] Update',
  UpdateOrderComplete = '[Order] Update Order Complete',
  UpdateOrderFailed = '[Order] Update Order Failed',

  SubmitOrder = '[Order] Submit',
  SubmitOrderComplete = '[Order] Submit Order Complete',
  SubmitOrderFailed = '[Order] Submit Order Failed',

  UpdateOrderProductDescription = '[Order] Update Product Description',

  UpdateOrderListing = '[Order] Update Listing',
  UpdateOrderListingComplete = '[Order] Update Listing Complete',
  UpdateOrderListingFailed = '[Order] Update Listing Failed',

  UpdateOrderPartial = '[Order] Partial Update',
  UpdateOrderPartialComplete = '[Order] Partial Update Complete',
  UpdateOrderPartialFailed = '[Order] Partial Update Failed',

  LoadOrderPhotos = '[Order] Load Order Photos',
  LoadOrderPhotosComplete = '[Order] Load Order Photos Complete',
  LoadOrderPhotosFailed = '[Order] Load Order Photos Failed',

  UpdateOrderPhotos = '[Order] Update Order Photos',
  UpdateOrderPhotosComplete = '[Order] Update Order Photos Complete',

  UpdateOrderPhoto = '[Order] Update Order Photo',

  UploadOrderPhotos = '[Order] Upload Order Photos',

  DeleteOrder = '[Order] Delete',
  DeleteOrderComplete = '[Order] Delete Order Complete',

  InitializeOrder = '[Order] Initialize',

  UpdateOrderState = '[Order] Update State',
  AddMarketingOrderMultimediaLink = '[MarketingOrder] Add Multimedia Link',
  AddMarketingOrderMultimediaLinkComplete = '[MarketingOrder] Add Multimedia Link Complete',
}

/**
 * Create ease of consumption wrapper actions
 * Eg.
 *
 * new LoadOrder(new MarketingOrder());
 */
export class LoadOrder implements Action {
  readonly type = OrderActionTypes.LoadOrder;
  constructor(public payload: MarketingOrder) {}
}

export class GetOrder implements Action {
  readonly type = OrderActionTypes.GetOrder;
  constructor(public payload: MarketingOrder, useCached?: boolean) {}
}

export class GetOrderComplete implements Action {
  readonly type = OrderActionTypes.GetOrderComplete;
  constructor(public payload: MarketingOrder) {}
}

export class AddOrder implements Action {
  readonly type = OrderActionTypes.AddOrder;
  constructor(public payload: MarketingOrder) {}
}

export class AddOrderComplete implements Action {
  readonly type = OrderActionTypes.AddOrderComplete;
  constructor(public payload: MarketingOrder) {}
}

export class UpdateOrder implements Action {
  readonly type = OrderActionTypes.UpdateOrder;
  constructor(public payload: MarketingOrder) {}
}

export class UpdateOrderComplete implements Action {
  readonly type = OrderActionTypes.UpdateOrderComplete;
  constructor(public payload: MarketingOrder) {}
}

export class SubmitOrder implements Action {
  readonly type = OrderActionTypes.SubmitOrder;
  constructor(public payload: MarketingOrder) {}
}

export class SubmitOrderComplete implements Action {
  readonly type = OrderActionTypes.SubmitOrderComplete;
  constructor(public payload: MarketingOrder) {}
}

export class UpdateOrderPartial implements Action {
  readonly type = OrderActionTypes.UpdateOrderPartial;
  constructor(public payload: MarketingOrder, public fields: (keyof(MarketingOrder))[] ) {}
}

export class UpdateOrderPartialComplete implements Action {
  readonly type = OrderActionTypes.UpdateOrderPartialComplete;
  constructor(public payload: MarketingOrder) {}
}

export class UpdateOrderPartialFailed implements Action {
  readonly type = OrderActionTypes.UpdateOrderPartialFailed;
  constructor(public payload: MarketingOrder, public fields: string[]) {}
}

export class UpdateOrderListing implements Action {
  readonly type = OrderActionTypes.UpdateOrderListing;

  /**
   * Update the marketing order listing
   * @param payload the marketing order including listing
   * @param persist save the update or not
   */
  constructor(public payload: MarketingOrder, public persist?: boolean) {
    if (persist === undefined) {
      this.persist = true;
    }
  }
}

export class UpdateOrderListingComplete implements Action {
  readonly type = OrderActionTypes.UpdateOrderListingComplete;
  constructor(public payload: Listing) {}
}

// TODO - maybe remove this action since we are throwing to a global error handler
export class UpdateOrderListingFailed implements Action {
  readonly type = OrderActionTypes.UpdateOrderListingFailed;
  constructor(public payload: any) {}
}

export class UpdateOrderPhoto implements Action {
  readonly type = OrderActionTypes.UpdateOrderPhoto;
  constructor(public payload: MarketingOrder, public photo: ListingPhoto, public productCode?: string) {}
}

export class UpdateOrderProductDescription implements Action {
  readonly type = OrderActionTypes.UpdateOrderProductDescription;
  constructor(public payload: ProductDescription ) {}
}

export class LoadOrderPhotos implements Action {
  readonly type = OrderActionTypes.LoadOrderPhotos;
  constructor(public productCode?: string) {}
}

export class UpdateOrderPhotos implements Action {
  readonly type = OrderActionTypes.UpdateOrderPhotos;
  constructor(public payload: MarketingOrder, public photos: ListingPhoto[], public photographerId?: string, public productCode?: string) {}
}

export class UploadOrderPhotos implements Action {
  readonly type = OrderActionTypes.UploadOrderPhotos;
  constructor(public payload: MarketingOrder, public photos: UploadPhoto[], public photographerId?: string, public productCode?: string) {}
}

export class UpdateOrderPhotosComplete implements Action {
  readonly type = OrderActionTypes.UpdateOrderPhotosComplete;
  constructor(public payload: MarketingOrder, public photos: ListingPhoto[], public productCode?: string) {}
}

export class DeleteOrder implements Action {
  readonly type = OrderActionTypes.DeleteOrder;
  constructor(public payload: MarketingOrder) {}
}

export class DeleteOrderComplete implements Action {
  readonly type = OrderActionTypes.DeleteOrderComplete;
  constructor() {}
}

export class InitializeOrder implements Action {
  readonly type = OrderActionTypes.InitializeOrder;
  constructor() {}
}

export class UpdateOrderState implements Action {
  readonly type = OrderActionTypes.UpdateOrderState;
  constructor(public marketingOrder: MarketingOrder, public payload: OrderState) {}
}

export class AddMarketingOrderMultimediaLink implements Action {
  readonly type = OrderActionTypes.AddMarketingOrderMultimediaLink;
  constructor(public payload: MultiMediaItemRequest) {}
}

export class AddMarketingOrderMultimediaLinkComplete implements Action {
  readonly type = OrderActionTypes.AddMarketingOrderMultimediaLinkComplete;
  constructor(public payload: MultiMediaItem) {}
}

export type OrderActions =
  LoadOrder
  | GetOrder
  | GetOrderComplete
  | AddOrder
  | AddOrderComplete
  | UpdateOrder
  | UpdateOrderComplete
  | SubmitOrder
  | SubmitOrderComplete
  | UpdateOrderListing
  | UpdateOrderListingComplete
  | UpdateOrderListingFailed
  | UpdateOrderPartial
  | UpdateOrderPartialComplete
  | UpdateOrderListingFailed
  | UpdateOrderPhoto
  | UpdateOrderPhotos
  | UploadOrderPhotos
  | UpdateOrderPhotosComplete
  | DeleteOrder
  | DeleteOrderComplete
  | UpdateOrderProductDescription
  | InitializeOrder
  | UpdateOrderState
  | AddMarketingOrderMultimediaLink
  | AddMarketingOrderMultimediaLinkComplete
  ;

export const SELECTEDORDER = 'selectedOrder';
