import { TestBed } from '@angular/core/testing';

import { NotesService } from './notes.service';
import { NotesClient } from '../clients/notes.client';
import { of } from 'rxjs';
import { Note } from '../models/note.model';
import { BaseForm, NoteForm } from '../forms';

describe('NotesService', () => {

  let mockedNotesClient: jasmine.SpyObj<NotesClient>;
  let expectedNotes = [
    new Note()
  ];

  beforeEach(() => {
    mockedNotesClient = jasmine.createSpyObj<NotesClient>(['get', 'post']);
    mockedNotesClient.get.and.callFake(() => of(expectedNotes));
    mockedNotesClient.post.and.callFake((form: BaseForm) => of(form.value));

    TestBed.configureTestingModule({
      providers: [
        {provide: NotesClient, useValue: mockedNotesClient}
      ]
    });
  });

  it('should be created', () => {
    const service: NotesService = TestBed.get(NotesService);
    expect(service).toBeTruthy();
  });

  it('getNotesFor should return an Observable', async () => {
    const service: NotesService = TestBed.get(NotesService);
    service.getNotesFor('123').subscribe(notes => {
      expect(notes).toBe(expectedNotes);
    });

  });

  it('create should return an Observable', async () => {
    const service: NotesService = TestBed.get(NotesService);
    const form = new NoteForm();
    form.getControl('ownerId').setValue('123');
    form.getControl('text').setValue('unit test');
    service.create(form).subscribe(note => {
      expect(note.ownerId).toBe('123');
      expect(note.text).toBe('unit test');
    });

  });
});
