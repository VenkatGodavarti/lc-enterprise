import { OrderFormsModule } from './order-forms.module';

describe('OrderFormsModule', () => {
  let orderFormsModule: OrderFormsModule;

  beforeEach(() => {
    orderFormsModule = new OrderFormsModule();
  });

  it('should create an instance', () => {
    expect(orderFormsModule).toBeTruthy();
  });
});
