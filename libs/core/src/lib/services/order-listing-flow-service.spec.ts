import { FlowState, PackageInstance, MarketingOrder } from '@lc/core';
import { OrderListingFlowStateService } from './order-listing-flow-service';
import { PackageInstanceFactory, MarketingOrderFactory } from '../factories';

describe('OrderListingFlowStateService', () => {

  let service: OrderListingFlowStateService;
  let flowState: FlowState;

  beforeEach(() => {
    service = new OrderListingFlowStateService();
    flowState = service.createNewFlowState();
  });

  it('should create service', () => {
    expect(service).toBeTruthy();
  });

  it('should create flow state', () => {
    expect(flowState).toBeTruthy();
  });

  it('start should return first step', () => {

    const step = flowState.start();
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/details');
  });


  it('should go through steps in order', () => {

    let step = flowState.start();
    expect(step).toBeTruthy();

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/details/property');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/details/agent');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/photography');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/photography/select-photographer');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/photography/photography-upgrades');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/photography/schedule-photographer');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/photography/photographer-comments');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/package-options');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/brochure-options');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/checkout');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/checkout/shipping');

    step = flowState.nextStep(step);
    expect(step).toBeTruthy();
    expect(step.id).toEqual('/order-listing/checkout/billing');

    step = flowState.nextStep(step);
    expect(step).toBeFalsy();

  });

  it('test previous step', () => {
    let currentStep = flowState.setStep('/order-listing/photography');
    currentStep = flowState.prevStep(currentStep);

    expect(currentStep.id === '/order-listing/details/agent').toBeTruthy();
  });

  it('set the current step on a workflow', () => {
    flowState.setStep('/order-listing/photography');
    expect(flowState.getCurrentStep().id === '/order-listing/photography').toBeTruthy();
  });

  it('set the last completed step of complete on a workflow on a parent step', () => {
    let currentStep = flowState.find('/order-listing/photography');
    expect(currentStep).not.toBeNull();
    flowState.setLastCompletedStep(currentStep);

    expect(currentStep.completed).toBeFalsy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
    expect(currentStep).toBeNull();
  });

  it('set the last completed step of complete on a workflow on a child step', () => {
    let currentStep = flowState.find('/order-listing/photography/select-photographer');
    expect(currentStep).not.toBeNull();
    flowState.setLastCompletedStep(currentStep);

    expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeFalsy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
    expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
        expect(currentStep.completed).toBeTruthy();
    expect(currentStep.active).toBeTruthy();

    currentStep = flowState.prevStep(currentStep);
    expect(currentStep).toBeNull();
  });

  it('test serialization to store state', () => {
    let currentStep = flowState.find('/order-listing/photography');
    flowState.setLastCompletedStep(currentStep);

    // set started to false like skipping step
    currentStep.started = false;
    currentStep.completed = false;

    currentStep = flowState.nextStep(currentStep);
    expect(currentStep).not.toBeNull();

    const savedState = flowState.serializeState();
    const step = savedState.state.find(aStep => aStep['id'] === '/order-listing/photography');
    expect(step['started']).toBeFalsy();
    expect(step['completed']).toBeFalsy();
      });

  it('test serialization to store state', () => {
    const currentStep = flowState.find('/order-listing/photography');
    flowState.setLastCompletedStep(currentStep);

    const savedState = flowState.serializeState();

    const step = savedState.state.find(aStep => aStep['id'] === '/order-listing/photography');
    expect(step['started']).toBeTruthy();
    // when setting a parent step to complete but sub steps are not complete, dont expect parent to be set to complete
    expect(step['completed']).toBeFalsy();

    flowState = service.createNewFlowState();
    flowState = flowState.deserializeState(savedState);
    const flowStep = flowState.find('/order-listing/photography');
    expect(flowStep.started).toBeTruthy();
    expect(flowStep.completed).toBeFalsy();

  });

  it('should remove photographyStep for listing with package without the photography product', () => {
    // Create a listing without the photography product
    const order = MarketingOrderFactory.createMarketingOrder();
    order.availablePackages = PackageInstanceFactory.createAll();
    order.selectedPackageCode = PackageInstanceFactory.silver.code;

    //Assert the test setup
    const photoProduct = order.getPhotographyProduct();
    expect(photoProduct).toBeFalsy();

    //Test the flowState
    flowState = service.createNewFlowState(order);
    const flowStep = flowState.find('/order-listing/photography');
    expect(flowStep).toBeFalsy();
  });

  it('should keep the photographyStep for listing with package with the photography product', () => {
    // Create a listing without the photography product
    const order = createOrder(PackageInstanceFactory.gold);

    //Assert the test setup
    const photoProduct = order.getPhotographyProduct();
    expect(photoProduct).toBeTruthy();

    //Test the flowState
    flowState = service.createNewFlowState(order);
    const flowStep = flowState.find('/order-listing/photography');
    expect(flowStep).toBeTruthy();
  });
});

function createOrder(selectedPackage: PackageInstance): MarketingOrder {
  const order = MarketingOrderFactory.createMarketingOrder();
  order.availablePackages = PackageInstanceFactory.createAll();
  order.selectedPackageCode = selectedPackage.code;
  return order;
}
