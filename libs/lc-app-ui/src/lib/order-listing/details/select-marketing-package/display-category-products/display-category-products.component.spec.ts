import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayCategoryProductsComponent } from './display-category-products.component';
import {Component, ViewChild} from "@angular/core";
import {samplePackages} from '../../../../../../../../sample-data/payloads/sample-package-instances-2';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


describe('DisplayCategoryProductsComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ DisplayCategoryProductsComponent, TestHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    component.displayCategoryProducts.category = 'print';
    component.displayCategoryProducts.packages = samplePackages;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should show available packages and those that are not', () => {

    let s = fixture.debugElement.queryAll(By.css(`#silver-flyer-qty`))
    expect(s).toBeTruthy();
    expect(s.length).toEqual(1);

    s = fixture.debugElement.queryAll(By.css(`#gold-flyer-qty`))
    expect(s).toBeTruthy();
    expect(s.length).toEqual(1);

    s = fixture.debugElement.queryAll(By.css(`#platinum-flyer-qty`))
    expect(s).toBeTruthy();
    expect(s.length).toEqual(1);

    s = fixture.debugElement.queryAll(By.css(`i.ion-md-checkmark`));
    expect(s).toBeTruthy();
    expect(s.length).toEqual(0);

    s = fixture.debugElement.queryAll(By.css(`i.ion-md-close`))
    expect(s).toBeTruthy();
    expect(s.length).toEqual(1);
  });
});


@Component({
  selector: 'lc-test-host',
  template: '<lc-display-category-products></lc-display-category-products>'
})
class TestHostComponent {
  @ViewChild(DisplayCategoryProductsComponent)
  public displayCategoryProducts: DisplayCategoryProductsComponent;

}
