import { MarketingOrder, OutputMimeType, ProductInstance } from '@lc/core';
import { Observable } from 'rxjs';

export interface ContentGenerator {

  mimeType: OutputMimeType;
  downloadable: boolean;

  generate(order: MarketingOrder, product: ProductInstance): Observable<any>;

  download(order: MarketingOrder, product: ProductInstance): Observable<any>;

}
