import { Observable } from 'rxjs/Observable';

export interface BaseAuthenticationProvider {
  redirectToLogin(): void;
  logout(): void;
  getToken(): string;
  getHeaderOptions(): any;
  isAuthenticated(): boolean;
  handle401Response(): void;
}
