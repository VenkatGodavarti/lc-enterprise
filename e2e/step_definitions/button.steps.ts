
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { by, ElementFinder, browser } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

/**
 * When: Button Click
 */
When(/^I click on the (.*) (link|button|icon)$/, {timeout: config.timeout},async (buttonName: string, elementType: string)=> {
  const button = findButton(buttonName);
  expect( await button.isEnabled()).to.be.equal(true); // TODO: is this necissary?
  await button.click();
});

/**
 * When: Button Click inside submap elements
 */
When(/^I click on the "(.*)" (link|button|icon) inside the "(.*)"$/, {timeout: config.timeout},async (buttonName: string, elementType: string, subMap: string)=> {
  const button = findButton(buttonName, subMap);
  await button.click();
});

function findButton(buttonName: string, subMapName?: string){
  const page = Context.get<LCPage>(ContextKeys.page);
  if(subMapName){
    return page.finder(subMapName, buttonName);
  }
  return page.finder(buttonName);
}


