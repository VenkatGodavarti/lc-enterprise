import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { PromptDialogService } from '../prompt-dialog/prompt-dialog.service';
import { DialogAction } from '../prompt-dialog/prompt-dialog.component';
import { UIConstants } from '@lc/core';

export class CropSize {
  constructor(public name: string, public height: number, public width: number, public isRound: boolean = false){
  }
}

export class ImageCropperResponse {
  constructor(public originalFile: File, public croppedFile: File, public isDelete: boolean = false, public aspectRatio?: string) {

  }
}

@Component({
  selector: 'lc-image-cropper-dialog',
  templateUrl: './image-cropper-dialog.component.html',
  styleUrls: ['./image-cropper-dialog.component.scss']
})
export class ImageCropperDialogComponent implements OnInit {

  file: File;
  cropSizes: CropSize[];
  croppedImage: string;
  croppedFile: File;
  uploadError: string;
  canDelete: boolean;
  aspectRatio: string;

  public readonly close = new Subject<ImageCropperResponse>();

  constructor(private promptDialog: PromptDialogService) { }

  ngOnInit() {
  }

  initialize(file: File, cropSizes: CropSize[], aspectRatio){
    this.canDelete = file != null;
    this.file = file;
    this.cropSizes = cropSizes;
    this.aspectRatio = aspectRatio;
  }

  onClose() {
    this.close.next();
  }

  onSubmit() {
    this.close.next(new ImageCropperResponse(this.file, this.croppedFile, false, this.aspectRatio));
  }

  onDelete() {
    const onYes = new DialogAction(UIConstants.YES);
    this.promptDialog.openPrompt(UIConstants.CONFIRM, 'Are you sure you want to delete this image?', onYes, [new DialogAction(UIConstants.NO)])
      .then(response => {
        if(response === onYes) {
          this.executeDelete();
        }
      })
  }

  onImageCropped(event) {
    this.croppedImage = event.base64;
    this.croppedFile = event.file;
  }

  loadImageFailed() {
    // show message
    this.uploadError = "Upload valid image with format : png/jpeg/jpg";
  }

  fileChangeEvent(event: any): void {
    if (event.target.files[0]) {
      this.file = event.target.files[0];
    }
  }

  private executeDelete() {
    this.close.next(new ImageCropperResponse(null, null, true));
  }

}
