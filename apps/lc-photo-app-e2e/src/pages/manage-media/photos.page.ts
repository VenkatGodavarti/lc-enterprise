import { ElementMap, Element, LCPage } from '../../../../../e2e';
import * as path from 'path';

export class PhotosPage extends LCPage {
  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Header', field: 'h1'}),
      new Element( { name: 'Drop Zone', field: '.lc-drop-zone'}),
      new Element( { name: 'Photo', field: '.photo'}),
      new Element( { name: 'Unsubmitted Grid', field: '#unsubmitted-grid .photo-container'})
    ]
  );

  constructor() {
    const photosPath = path.resolve(__dirname, `./photos`);
    super('Photos', [PhotosPage.elements], { 'home': `${photosPath}/home.jpg`});
  }
}
