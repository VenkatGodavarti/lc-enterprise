import { Component, OnInit, Input } from '@angular/core';
import { PropertyInfoForm } from '@lc/core';

@Component({
  selector: 'lc-mls-info-form',
  templateUrl: './mls-info-form.component.html',
  styleUrls: ['./mls-info-form.component.scss']
})
export class MlsInfoFormComponent implements OnInit {
  @Input()
  formGroup: PropertyInfoForm;

  constructor() { }

  ngOnInit() {
  }

}
