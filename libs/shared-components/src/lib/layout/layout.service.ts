import { Injectable, NgZone } from '@angular/core';

@Injectable()
export class LayoutService {
  constructor(private zone: NgZone) {}

  private get layoutHelpers() {
    return window['layoutHelpers'];
  }

  /* istanbul ignore next */
  private exec(fn) {
    return this.layoutHelpers && this.zone.runOutsideAngular(fn);
  }

  /* istanbul ignore next */
  public getLayoutSidenav() {
    return this.exec(() => this.layoutHelpers.getLayoutSidenav()) || null;
  }

  /* istanbul ignore next */
  public getSidenav() {
    return this.exec(() => this.layoutHelpers.getSidenav()) || null;
  }

  /* istanbul ignore next */
  public getLayoutNavbar() {
    return this.exec(() => this.layoutHelpers.getLayoutNavbar()) || null;
  }

  /* istanbul ignore next */
  public getLayoutFooter() {
    return this.exec(() => this.layoutHelpers.getLayoutFooter()) || null;
  }

  /* istanbul ignore next */
  public getLayoutContainer() {
    return this.exec(() => this.layoutHelpers.getLayoutContainer()) || null;
  }

  /* istanbul ignore next */
  public isSmallScreen() {
    return this.exec(() => this.layoutHelpers.isSmallScreen());
  }

  /* istanbul ignore next */
  public isLayout1() {
    return this.exec(() => this.layoutHelpers.isLayout1());
  }

  /* istanbul ignore next */
  public isCollapsed() {
    return this.exec(() => this.layoutHelpers.isCollapsed());
  }

  /* istanbul ignore next */
  public isFixed() {
    return this.exec(() => this.layoutHelpers.isFixed());
  }

  /* istanbul ignore next */
  public isOffcanvas() {
    return this.exec(() => this.layoutHelpers.isOffcanvas());
  }

  /* istanbul ignore next */
  public isNavbarFixed() {
    return this.exec(() => this.layoutHelpers.isNavbarFixed());
  }

  /* istanbul ignore next */
  public isFooterFixed() {
    return this.exec(() => this.layoutHelpers.isFooterFixed());
  }

  /* istanbul ignore next */
  public isReversed() {
    return this.exec(() => this.layoutHelpers.isReversed());
  }

  /* istanbul ignore next */
  public setCollapsed(collapsed, animate = true) {
    this.exec(() => this.layoutHelpers.setCollapsed(collapsed, animate));
  }

  /* istanbul ignore next */
  public toggleCollapsed(animate = true) {
    this.exec(() => this.layoutHelpers.toggleCollapsed(animate));
  }

  /* istanbul ignore next */
  public setPosition(fixed, offcanvas) {
    this.exec(() => this.layoutHelpers.setPosition(fixed, offcanvas));
  }

  /* istanbul ignore next */
  public setNavbarFixed(fixed) {
    this.exec(() => this.layoutHelpers.setNavbarFixed(fixed));
  }

  /* istanbul ignore next */
  public setFooterFixed(fixed) {
    this.exec(() => this.layoutHelpers.setFooterFixed(fixed));
  }

  /* istanbul ignore next */
  public setReversed(reversed) {
    this.exec(() => this.layoutHelpers.setReversed(reversed));
  }

  /* istanbul ignore next */
  public update() {
    this.exec(() => this.layoutHelpers.update());
  }

  /* istanbul ignore next */
  public setAutoUpdate(enable) {
    this.exec(() => this.layoutHelpers.setAutoUpdate(enable));
  }

  /* istanbul ignore next */
  public on(event, callback) {
    this.exec(() => this.layoutHelpers.on(event, callback));
  }

  /* istanbul ignore next */
  public off(event) {
    this.exec(() => this.layoutHelpers.off(event));
  }

  /* istanbul ignore next */
  public init() {
    this.exec(() => this.layoutHelpers.init());
  }

  /* istanbul ignore next */
  public destroy() {
    this.exec(() => this.layoutHelpers.destroy());
  }

  // Internal
  //

  /* istanbul ignore next */
  public _redrawLayoutSidenav() {
    this.exec(() => this.layoutHelpers._redrawLayoutSidenav());
  }

  /* istanbul ignore next */
  public _removeClass(cls) {
    this.exec(() => this.layoutHelpers._removeClass(cls));
  }
}
