import { TestBed } from '@angular/core/testing';

import { ImageUploaderService } from './image-uploader.service';
import { ImageLoaderService } from './image-loader.service';
import { PhotoService } from './photo.service';

describe('ImageUploaderService', () => {
  let service: ImageUploaderService;
  let imageLoader: ImageLoaderService;
  let photoService: PhotoService;

  beforeEach(() => {
    imageLoader = jasmine.createSpyObj<ImageLoaderService>(['loadFile']);
    photoService = jasmine.createSpyObj<PhotoService>(['upload']);
    TestBed.configureTestingModule({
      providers: [
        { provide: ImageLoaderService, useValue: imageLoader },
        { provide: PhotoService, useValue: photoService }
      ]
    })
  });

  beforeEach(() => service = TestBed.get(ImageUploaderService));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
