import { Component, OnInit, OnDestroy } from '@angular/core';
import { TeamProfile, PhotoService, TeamProfileForm, UpdateTeam, UserService, TeamMemberForm, TeamMember, TeamMemberRole, TeamMemberService, AgentInfo, UpdateTeamComplete, TeamActionTypes, SELECTEDTEAM, UploadStatus, ThumbnailSizes, ImageUploaderService, UploadPhoto } from '@lc/core';
import { Store, select } from '@ngrx/store';
import { FormArray, FormControl } from '@angular/forms';
import { Masks, UploadResultsDialogService, ImageCropperDialogService, CropSize, ImageCropperResponse } from '@lc/shared-components-ui';
import { switchMap, filter } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lc-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.scss']
})
export class TeamCreateComponent implements OnInit, OnDestroy {
  readonly phoneMask = Masks.phoneNumber;
  form: TeamProfileForm = new TeamProfileForm();
  memberSearch$: Observable<AgentInfo[]>;
  searchControl: FormControl;
  updateSubscription: any;
  alreadyExistsSubscription: any;
  status: UploadStatus;
  editPhoto: boolean;

  get showUpload(): boolean { return this.editPhoto || this.form.get('teamPhoto.photoUrl').value == null; }

  constructor(
    public store: Store<any>,
    private userService: UserService,
    private imageUploaderService: ImageUploaderService,
    private uploadResultsDialog: UploadResultsDialogService,
    private teamMemberService: TeamMemberService,
    private actions: Actions,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private photoService: PhotoService,
    private dialogService: ImageCropperDialogService) {
    this.initializeSearch();
    this.initializeForm();
    this.alreadyExistsSubscription = this.store.pipe(select(SELECTEDTEAM), filter(team => team != null))
      .subscribe(() => this.routeToTeamDisplay())
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if(this.updateSubscription) { this.updateSubscription.unsubscribe(); }
    if(this.alreadyExistsSubscription) { this.alreadyExistsSubscription.unsubscribe(); }
  }

  private initializeForm() {
    const teamMember = this.createTeamMember(this.userService.getUserId(), true);
    this.form.get('ownerId').setValue(teamMember.agentId);
    const teamMembers = this.form.get('members') as FormArray;
    teamMembers.push(new TeamMemberForm(teamMember));
  }

  private initializeSearch() {
    this.searchControl = new FormControl();
    this.memberSearch$ = this.searchControl.valueChanges.pipe(
      // debounce(() => timer(1000)), // NOTE: The debounce time on the chips input is already 250 ms
      switchMap(searchTerm => this.teamMemberService.searchAgents(searchTerm)),
    );
  }

  public onSubmit() {
    this.form.markAllAsDirty();
    if(!this.form.valid) { return; }

    this.updateSubscription = this.actions.pipe(ofType<UpdateTeamComplete>(TeamActionTypes.UpdateTeamComplete))
      .subscribe(() => this.routeToTeamDisplay());
    this.store.dispatch(new UpdateTeam(this.form.value));
  }

  private routeToTeamDisplay(){
    this.router.navigate(['..'], {relativeTo: this.activatedRoute});
  }

  private createTeamMember(userId: string, isOwner: boolean): TeamMember {
    const teamMember = new TeamMember;
    teamMember.agentId = userId;
    teamMember.teamRole = TeamMemberRole.salesAgent;
    teamMember.teamAdmin = isOwner;
    teamMember.position = 0;
    return teamMember;
  }

  onEditPhoto() {
    const photoUrl = this.form.get('teamPhoto.originalPhotoUrl').value;

    if(photoUrl) {
      // If there is an image, we need to load that as a default
      const metaData = this.form.get('teamPhoto.metaData').value;
      const aspectRatio = metaData && metaData.ratio ? metaData.ratio : "2";
      this.photoService.getImage(photoUrl)
        .subscribe(res => this.cropImage(res[0].body, aspectRatio));
    } else {
      // Otherwise, open up the cropper without an image
      this.cropImage();
    }
  }

  private cropImage(file?: any, aspectRatio = "2"){
    // Open up the image dialog with the crop size we want to preview it as
    this.dialogService.openImageCropper(file, [new CropSize('Avatar', 120, 120, false)], aspectRatio)
      .then(response => {
        if(!response) { return; }

        // If the user did an action, update appropriately
        if(response.isDelete) {
          this.deleteImage();
        } else if(response.croppedFile) {
          this.uploadImage(response);
        }
      });
  }

  private deleteImage() {
    this.updateImages(null, null, null);
  }

  private uploadImage(response: ImageCropperResponse) {
    if(!response){ return; }

    // Set the name on the cropped Blob or File
    (<any>response.croppedFile).name = "profilePhoto.png";

    // Upload images synchronously and then update the profile with both images
    combineLatest(this.photoService.upload(response.originalFile), this.photoService.upload(response.croppedFile))
      .subscribe(([originalPhotos, croppedPhotos]) => {
        // Find the original images from the response and then call update
        const original: UploadPhoto = originalPhotos.find((photo: any) => photo.original);
        const cropped: UploadPhoto = croppedPhotos.find((photo: any) => photo.original);
        this.updateImages(original.url, cropped.url, response.aspectRatio);
      });
  }

  private updateImages(originalUrl: string, croppedUrl: string, aspectRatio: string) {
    this.form.markAsTouched();
    this.form.get('teamPhoto.photoUrl').setValue(croppedUrl);
    this.form.get('teamPhoto.originalPhotoUrl').setValue(originalUrl);
    this.form.get('teamPhoto.metaData').setValue(aspectRatio ? { ratio: aspectRatio} : null);
  }
}
