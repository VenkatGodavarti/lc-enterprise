@login-before-all
@team-profile
@team-create
Feature: Create a Team Profile
Background:
  Given I am logged in to Listing Concierge
  When I go to the Main page in My Profile
  Then the Main page from My Profile is loaded
  When I click on the My Team button
  Then the Team page from My Profile is loaded

@empty-team-profile
Scenario: Create a new team
#Given I have no team profile created
  When I click on the "Create a new Team" button
  Then the Create Team page from My Profile is loaded
  When I enter data into the Team Profile form
  | Team Name        | Team Awesome               |
  | Phone            | 9495551212                 |
  | Website          | https://realogy.com        |
  | Email            | teamawesome@realogy.com    |
  | Slogan           | We sell houses             |
  # | Office Name      | CB north 12                |
  # | Members          | Lavonda Riel, Tracy Eagle  |

  And I click on the "Create Team" button
  Then the Team page from My Profile is loaded

@empty-team-profile
Scenario: Team form validation
  When I click on the "Create a new Team" button
  Then the Create Team page from My Profile is loaded

  When I click on the "Create Team" button
  Then the "Team Name" has an error containing "required"
  Then the "Phone" has an error containing "required"
  Then the "Website" has an error containing "required"
  Then the "Email" has an error containing "required"
