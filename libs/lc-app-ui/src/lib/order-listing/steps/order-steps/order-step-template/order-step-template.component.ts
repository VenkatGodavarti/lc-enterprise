import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActionTypes, FlowState, FlowStep, WorkflowAction } from '@lc/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'lc-order-step-template',
  templateUrl: './order-step-template.component.html',
  styleUrls: ['./order-step-template.component.scss']
})
export class OrderStepTemplateComponent implements OnInit, OnDestroy {

  @Input() index: number;
  @Input() orderState: FlowState;
  @Input() step: FlowStep;
  @Input() showChildren?: boolean;

  totalSteps: number;

  private orderStateSub: Subscription;
  private params: ParamMap;
  public get isActiveStep() { return this.step === this.orderState.currentStep.parent; }
  public get isNextStepStarted() {
    return (this.index < this.totalSteps) && this.orderState.steps[this.index].started;
  }

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {
  }

  ngOnInit() {
    this.totalSteps = this.orderState ? this.orderState.getActiveSteps().length : 0;
    this.activatedRoute.parent.paramMap.subscribe(params => {
      this.params = params;
    });
  }

  ngOnDestroy(): void {
    if (this.orderStateSub) {
      this.orderStateSub.unsubscribe();
    }
  }

  public canEditStep() {
    // Edit pencil indicator should not display on any steps if:
    // 1.) There are no substeps
    // 2.) there are previous incomplete tests
    if(!this.step.hasSubsteps() || this.orderState.hasPreviousUnfinishedSteps(this.step)) {
      return false;
    }

    //If we are on the current step, then the step needs to be started and not completed
    if(this.orderState.currentStep.id === this.step.id) {
      return this.step.started && !this.step.completed;
    }

    //Otherwise, display if the step has been completed
    return this.step.completed;
  }

  public nextStep() {
    const stepCompleted = !this.orderState.getCurrentStep().hasSubsteps();
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, {stepCompleted: stepCompleted},
      this.params));
  }

  // TODO - parentStep to next parent and skip children steps
  public nextParentStep() {
    // dispatch to the store to handle event
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, null,
      this.params));
  }

  /**
   * This will invoke the next substep. This will jump to the first substep that is not complete.
   */
  public nextSubStep(editStepSelected: FlowStep) {
    // dispatch to the store to handle event. Passing the id will set the stop to that workflow step id
    // and then call next
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionEdit,
      {id: editStepSelected.id},
      this.params));
  }

}
