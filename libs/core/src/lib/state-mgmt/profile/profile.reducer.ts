import { Profile } from '../../models/profile.model';
import { ProfileActions, ProfileActionTypes } from './profile.actions';

export const initialState: Profile = null;

export function profileReducer(state = initialState, action: ProfileActions): Profile {

  switch (action.type) {
    case ProfileActionTypes.LoadProfile: {
      return action.payload;
    }
    case ProfileActionTypes.UpdateProfile: {
      return action.payload;
    }
    case ProfileActionTypes.UpdateProfileComplete: {
      console.log('UpdateProfileComplete:', action);
      return action.payload;
    }
    default: {
      return state;
    }
  }
}
