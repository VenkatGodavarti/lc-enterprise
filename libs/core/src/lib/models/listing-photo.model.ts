import {Deserializable} from './deserializable.model';
import { PhotoThumbnails } from './photo-thumbnail.model';

/**
 * Client-side model of a Listing photograph.
 */
export class ListingPhoto implements Deserializable<ListingPhoto> {
  _id: string;
  uri: string;
  order: number;
  favorite = false;
  uploadedBy: string;
  photographerId: string;
  thumbnails: PhotoThumbnails = {};

  constructor(listingPhoto?) {
    if (listingPhoto) {
      Object.assign(this, listingPhoto);
    }
  }

  deserialize(input: any): ListingPhoto {
    Object.assign(this, input);

    return this;
  }

}
