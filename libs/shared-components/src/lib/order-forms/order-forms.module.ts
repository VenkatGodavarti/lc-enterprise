import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const angularModules = [ CommonModule, FormsModule, ReactiveFormsModule ];

// Form Components
import { AddressDetailsFormComponent} from './address-details-form/address-details-form.component';
import { PropertyInfoFormComponent} from './property-info-form/property-info-form.component';
import { MlsInfoFormComponent } from './mls-info-form/mls-info-form.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
const formComponents = [ AddressDetailsFormComponent, MlsInfoFormComponent, PropertyInfoFormComponent ];

@NgModule({
  imports: [
    ...angularModules,
    SharedComponentsUiModule,
    NgbDatepickerModule
  ],
  declarations: [...formComponents],
  exports: [...formComponents],
})
export class OrderFormsModule { }
