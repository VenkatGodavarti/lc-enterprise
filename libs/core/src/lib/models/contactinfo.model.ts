import { Deserializable } from './deserializable.model';
import { ContactType } from './contact.type';
import { AddressInformation } from './address-information.model';

export class CardContactInfo implements Deserializable<CardContactInfo> {

  type: ContactType;
  address: AddressInformation;

  constructor(input?: any) {
    this.deserialize(input || {});
  }

  deserialize(input: any): CardContactInfo {
    Object.assign(this, input);
    if (this.address) {
      this.address = new AddressInformation(this.address);
    }
    return this;
  }

}

export type ContactInfo = CardContactInfo;

export class ContactInfoFactory {

  public static createContactInfo(data: any) {
    if (!data || !data.type) {
      throw new Error('Must supply data and a contact info type');
    }
    switch (data.type) {
      case ContactType.card: {
        return new CardContactInfo(data);
      }
      default: {
        throw new Error('only card contacts currently supported');
      }

    }
  }
}
