export const environment = {
  production: false,
  envName: 'ft',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'https://lc.ft.listing-concierge.realogy.com/',
  apiBaseURL: 'https://lc.ft.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.ft.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.ft.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzupdjbvFKysGd0h7',
  oktaRedirectUri: 'https://lc.ft.listing-concierge.realogy.com/implicit/callback',
};
