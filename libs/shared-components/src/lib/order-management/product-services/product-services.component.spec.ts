import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductServicesComponent } from './product-services.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { MarketingOrderFactory } from '../../../../../core/src/lib/factories';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;

describe('ProductServicesComponent', () => {
  let component: ProductServicesComponent;
  let fixture: ComponentFixture<ProductServicesComponent>;
  let store: SpyObj<Store<any>>;
  let marketingOrder;

  beforeEach(async(() => {
    store = jasmine.createSpyObj('Store', ['pipe', 'dispatch']);

    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);

    store.pipe.and.returnValue(of(marketingOrder));


    TestBed.configureTestingModule({
      declarations: [ ProductServicesComponent ],
      providers: [
        { provide: Store, useValue: store },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
