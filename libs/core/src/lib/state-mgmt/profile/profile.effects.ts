import { Actions, Effect, ofType } from '@ngrx/effects';
import { Profile } from '../../models/profile.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { ProfileService } from '../../services/profile.service';
import { Store } from '@ngrx/store';
import { UpdateProfile, UpdateProfileComplete, ProfileActionTypes } from './profile.actions';
import { ErrorData, GlobalErrorHandler } from '../../errors';
import { NotificationEvent, NotificationEventService } from '../../notifications';
import { UserProfileUpdated } from '../user/user.actions';
import { BaseEffects } from '../base.effects';

@Injectable()
export class ProfileEffects extends BaseEffects {

  constructor(private actions$: Actions,
              private profileService: ProfileService,
              private notificationService: NotificationEventService,
              private store: Store<any>,
              errorHandler: GlobalErrorHandler) {
    super(errorHandler)
  }

  @Effect( { dispatch: false})
  updateProfile: Observable<Profile> = this.actions$.pipe(
    ofType<UpdateProfile>(ProfileActionTypes.UpdateProfile),
    switchMap (({ payload, fields}) => {
      return this.profileService.updateProfilePartial(payload, fields).pipe(
        catchError(err => this.processCatchError(ProfileActionTypes.UpdateProfile, {payload: payload, fields: fields}, err)),
      );
    }),
    filter( profile => !(profile instanceof ErrorData) ),
    tap((profile: Profile) => this.updateLoggedInUserProfile(profile)),
    tap((profile: Profile) => this.updateComplete(profile))
  );

  updateLoggedInUserProfile(profile: Profile): void {
    this.store.dispatch( new UserProfileUpdated(profile));
  }

  updateComplete(profile: Profile) {
    this.store.dispatch(new UpdateProfileComplete(profile));
    const event = new NotificationEvent(ProfileActionTypes.UpdateProfileComplete, 'Profile updated');
    this.notificationService.getEventEmitter().emit(event);
  }
}
