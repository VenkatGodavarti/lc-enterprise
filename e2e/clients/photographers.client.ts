import { http } from './http';
import { Configuration as config } from '../configuration';

export class PhotographersClient {
  static async get() {
    const res = await http.get(`${config.apiBaseURL}/photographers`);
    const photographers = res.data;
    return photographers;
  }
}
