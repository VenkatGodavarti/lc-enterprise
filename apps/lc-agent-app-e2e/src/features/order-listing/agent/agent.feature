## These are steps specifically for validating the Agent Step within the order process
@agent
@login-before-all
Feature: Agent types

Background:
  # Login to the app and create the Alamo order
  Given I am logged in to Listing Concierge
  Given the following orders exist
  | Name       | Alamo        |
  | Agent Type | Single Agent |
  | Status     | OPEN         |
  | Submitted  | false        |
  And the order is on the agent step
  # TODO: Set selected package

  When I go to the Main page in Listing Dashboard
  Then the Main page from Listing Dashboard is loaded

  # Navigate to the Order Details
  When I wait for the Add Listing button to load
  When I click the Complete Order button on the last order
  Then the Order Listing Details page from Order Listing is loaded

  # Navigate into Property Page
  And I wait for the Listing Details Continue button to load
  When I click on the Listing Details Continue button
  Then the Order Listing Property page from Order Listing is loaded

@empty-team-profile
Scenario: Agent Type disclaimers not part of a Team
  # Navigate to Agent Page
  When I wait for the Next button to load
  When I click on the Next button
  Then the Order Listing Agent page from Order Listing is loaded

  # Validate Agent Name
  Then the "Agent Name" is set to the current user
  And the "Agent Name" field is disabled

  # Validate all 3 buttons are displayed
  Then the Single Agent button is present
  And the "Single Agent" button has class "option-selected"
  Then the Two Agents button is present
  And the "Two Agents" button does not have class "option-selected"
  Then the Team button is present
  And the "Team" button does not have class "option-selected"
  Then the "Team" button has class "disabled"

  # Single Agent Info Text
  When I hover over the Single Agent button
  Then the "Info Text" text is "Select this option if you are listing this property by yourself"

  # Two Agents Info Text
  When I hover over the Two Agents button
  Then the "Info Text" text is "Select this option if you are listing this property with another agent and tell us their name"

  # Team Info Text when no team exists
  When I hover over the Team button
  Then the "Info Text" text is "Looks like you're not part a team yet. To select this option, please check your profile to create your team, or contact your team owner(s)"

Scenario: Agent Type disclaimers part of a Team
  Given the coldwell team profile exists

  # Navigate to Agent Page
  When I wait for the Next button to load
  When I click on the Next button
  Then the Order Listing Agent page from Order Listing is loaded

  # Validate all 3 buttons are displayed
  Then the Single Agent button is present
  Then the Two Agents button is present
  Then the Team button is present
  Then the "Team" button does not have class "disabled"

  # Single Agent Info Text
  When I hover over the Single Agent button
  Then the "Info Text" text is "Select this option if you are listing this property by yourself"

  # Two Agents Info Text
  When I hover over the Two Agents button
  Then the "Info Text" text is "Select this option if you are listing this property with another agent and tell us their name"

  # Team Info Text when team exists
  When I hover over the Team button
  Then the "Info Text" text is "Select this option if you are listing this property as a Team"

Scenario: Two Agents requires a colister
  # Navigate to Agent Page
  When I wait for the Next button to load
  When I click on the Next button
  Then the Order Listing Agent page from Order Listing is loaded

  # Validate all 3 buttons are displayed
  Then the Two Agents button is present

  When I click on the Two Agents button
  Then the "Co-Lister" field is displayed

  When I click on the Next button
  Then the "Co-Lister Error" text is "This field is required"

  When I enter "Cherri" into the "Co-Lister"
  When the chips menu is loaded
  Then the chips menu has 1 option

  When I click on option 1 in the chips menu
  Then the "Selected Co-Listers" chips contains "Cherri Lyda"

  When I click on the Next button
  Then the Overview Package Options page from Order Listing is loaded

## NOTE: I did not use these examples as it will run the scenario once for each run. (2 Outlines * 3 Rows = 6 Scenarios w/ setup and teardown)
## Rather for efficiency purposes, broke them apart into 2 seperate scenarios
# Given I am in my agent details page
# And I am already part of a Team
# When I hover over <agent_type>
# Then I should see <type_disclaimer>

# Examples:
# | agent_type | type_disclaimer                                                                                 |
# | Agent      | “Select this option if you are listing this property by yourself”                               |
# | Two Agents | “Select this option if you are listing this property with another agent and tell us their name” |
# | Team       | “Select this option if you are listing this property as a Team”                                 |

# Scenario Outline: Agent Type disclaimers not part of a Team
# Given I am in my agent details page
# And I am not part of a Team
# When I hover over <agent_type>
# Then I should see <type_disclaimer>

# Examples:
# | agent_type | type_disclaimer                                                                                                                              |
# | Agent      | “Select this option if you are listing this property by yourself”                                                                            |
# | Two Agents | “Select this option if you are listing this property with another agent and tell us their name”                                              |
# | Team       | “Looks like you're not part a team yet. To select this option, please check your profile to create your team, or contact your team owner(s)” |
