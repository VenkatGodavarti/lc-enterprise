
import { TestBed } from '@angular/core/testing';

import { TeamProfileService } from './team-profile.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from './user.service';
import { UserServiceMocks } from '../mocks/user-service.mocks';
import { environment } from '../../../../environments/environment';
import { TeamProfile } from '../models/team-profile.model';
import { TeamContactPreferencesForm } from '../forms';
import { PhotoInformation } from '@lc/core';

describe('TeamProfileService', () => {

  let testController: HttpTestingController;
  const teamProfileEndpoint = `${environment.apiBaseURL}team-profiles`;
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      HttpClientTestingModule
    ],
    providers: [
      { provide: UserService, useValue: UserServiceMocks.buildUserService()}
    ]
  }));

  beforeEach(() => {
    testController = TestBed.get(HttpTestingController);
  })

  afterEach(() => {
    testController.verify();
  });

  it('should be created', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    expect(service).toBeTruthy();
  });

  it('should call team-profiles with agentId as a query parameter', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    const agentId = '123';
    service.getAgentProfiles(agentId).subscribe();

    const expectedUrl = `${teamProfileEndpoint}?agentId=${agentId}`
    const request = testController.expectOne(expectedUrl);

    expect(request.request.method).toBe('GET');
  });

  it('should call get team-profiles with profileId in the route', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    const teamProfileId = '123';
    service.getTeamProfile(teamProfileId).subscribe();

    const expectedUrl = `${teamProfileEndpoint}/${teamProfileId}`
    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('GET');
  });

  it('should call put team-profiles with profileId in the route', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    const teamProfileId = '123';
    const teamProfile = new TeamProfile({_id: teamProfileId});
    service.put(teamProfile).subscribe();

    const expectedUrl = `${teamProfileEndpoint}/${teamProfileId}`
    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('PUT');
  });

  it('should call post team-profiles', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    const teamProfile = new TeamProfile();
    teamProfile.teamPhoto = new PhotoInformation();
    service.post(teamProfile).subscribe();

    const expectedUrl = `${teamProfileEndpoint}`;
    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('POST');
  });

  it('should call put with the contactPreferences in the route', () => {
    const service: TeamProfileService = TestBed.get(TeamProfileService);
    const teamProfile = new TeamProfile({_id: 123});
    const preferencesForm = new TeamContactPreferencesForm();
    service.updateContactPreferences(teamProfile._id, preferencesForm.value).subscribe();

    const expectedUrl = `${teamProfileEndpoint}/${teamProfile._id}/contactPreferences`
    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('PUT');
  });
});
