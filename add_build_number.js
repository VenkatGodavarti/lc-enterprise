const fs = require('fs');
const path = require('path');

const { version } = require('./package.json');
const envName = process.env.NODE_ENV;
const buildNumber = process.env.BUILD_NUMBER;

const BASE_PATH = path.join(__dirname);
const CONTENT_FOLDER = path.join(BASE_PATH, 'dist', 'apps', 'lc-agent-app');

if (envName && buildNumber && version) {
    const indexFilePath = path.join(CONTENT_FOLDER, 'index.html');
    if (fs.existsSync(indexFilePath)) {
        const fullVersion = `${version} -- ${buildNumber} -- ${envName}`
        const indexFileStr = fs.readFileSync(indexFilePath).toString();
        const newIndexFileStr = indexFileStr.replace('content="---BUILD-NUMBER---"', 'content="' + fullVersion + '"');
        fs.writeFileSync(indexFilePath, newIndexFileStr);
    } else {
        console.log('The index file was missing for env: ' + envName);
    }
} else {
    console.log('Missing version, buildNumber, or environment');
    console.log('Version = ' + version);
    console.log('Build Number = ' + buildNumber);
    console.log('Env Name = ' + envName);
}
