import { ErrorHandler, Injectable } from '@angular/core';
import { ErrorService } from './error.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NotificationEvent, NotificationEventService } from '../notifications';
import { ErrorData } from './error-data';


/**
 * GlobalErrorHandler to process errors
 *
 */
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private errorService: ErrorService,
              private notificationService: NotificationEventService) {
  }

  handleError(error: any) {
    // send error to service to process. i.e. log, save, send to API, etc..
    this.errorService.handleError(error);

    if (error instanceof HttpErrorResponse) {
      this.handleServerError(error)
    } else if (error instanceof ErrorData) {
      this.handleErrorData(error);
    } else {
      this.handleUnexpectedError(error)
    }

  }

  // we have different handlers based on different error objects. Final action to be determined
  handleServerError(error: any) {
    const event = new NotificationEvent('error', 'server error', error);
    this.notificationService.getEventEmitter().emit(event);
  }

  handleErrorData(error: ErrorData) {
    this.notificationService.getEventEmitter().emit(error.getNotificationEvent());
  }

  handleUnexpectedError(error: any) {
    const event = new NotificationEvent('error', 'unexpected', error);
    this.notificationService.getEventEmitter().emit(event);
  }
}
