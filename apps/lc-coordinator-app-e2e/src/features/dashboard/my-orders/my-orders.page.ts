import { ElementMap, Element, LCPage } from '../../../../../../e2e';

export class MyOrdersPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Orders', field: '.table'}),

      new Element( { name: 'Agent Avatar', field: '.agent-avatar'}),
      new Element( { name: 'Agent First Name', field: '.agent-firstName'}),
      new Element( { name: 'Agent Last Name', field: '.agent-lastName'}),
      new Element( { name: 'Agent Phone', field: '.agent-phone'}),
      new Element( { name: 'Agent Email', field: '.agent-email'}),
      new Element( { name: 'Agent Office', field: '.agent-office'}),


      // Navigation Tabs
      new Element( { name: 'My Orders Tab', field: '#my-orders'}),
      new Element( { name: 'All Orders Tab', field: '#all-orders'}),
      new Element( { name: 'Completed Orders Tab', field: '#completed-orders'}),

      // Table Column Headers
      new Element( { name: 'Package Header', field: '#package-header'}),

    ]
  );

  constructor() {
    super('My Orders', [MyOrdersPage.elements]);
  }

}
