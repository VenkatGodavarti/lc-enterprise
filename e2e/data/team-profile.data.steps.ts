import { Given } from 'cucumber';
import {
  Configuration,
  TeamProfilesClient,
  Context,
  Scheduler,
  Schedule,
  ContextKeys
} from '../../e2e';
import { BaseData } from './base-data';
import { verticalTableToRecords } from '../step_definitions';
import { TeamProfileProps } from './team-profile.props';

const teamProfileProps = new TeamProfileProps();

Given(/^the following team profile exists$/,  { timeout: Configuration.timeout },  async (rawTable) => {
  const profileToCreate = verticalTableToRecords(rawTable)[0];
  await buildTeamProfile(profileToCreate);

});

async function buildTeamProfile(profileToCreate) {

  const profileName = profileToCreate.Name;

  const teamProfile = BaseData.readFileToObject(`team-profiles/${profileName.toLowerCase()}.json`);
  await updateProfileProperties(teamProfile, profileToCreate);
  await createTeamProfile(teamProfile);
}

async function updateProfileProperties(profile, tableRecord) {
  teamProfileProps.setValues(profile, tableRecord, ['Name']);
}

async function createTeamProfile(teamProfile) {
  const user = Context.get(ContextKeys.user);
  teamProfile.members.push({agentId:user._id,teamRole:"sales-agent",teamAdmin:true,position:0});

  const team = await TeamProfilesClient.post(teamProfile);
  Context.set('profileId', team._id);

  // Schedule a cleanup of the order
  Scheduler.schedule(Schedule.afterScenario, async () => {
    await TeamProfilesClient.delete(team._id);
  })
}
