

const context = {};

/**
 * Pre-defined keys used in re-usable steps to store and access data.
 */
export enum ContextKeys {

  loggedIn = 'loggedIn',
  apiAuthToken = 'apiAuthToken',
  apiUserId = 'apiUserId',

  order = 'order',
  orderId = 'orderId',

  user = 'user',
  userId = 'userId',

  profile = 'profile',

  originalFormData = 'originalFormData',
  enteredFormData = 'enteredFormData',

  page = 'page',
  url = 'url'
}

export class Context {

  static context() { return context; }
  static set(key: string, value: any) {
    context[key] = value;
  }


  static get<TReturnType = any>(key: string): TReturnType {
    return context[key];
  }

  static clear(key: string): void {
    delete context[key];
  }

  static dump(): void {
    for ( const prop in context) {
      console.log('Context['+prop+']: ', context[prop]);
    }
  }

  static userKey(name: string): string {

    return `${name.toLowerCase()}-user`;
  }

  static setUser(key: string, user: any) {
    Context.set(Context.userKey(key), user);
  }

  static getUser(key: string): any {
    const user =  Context.get(Context.userKey(key));
    if (!user) {
      throw new Error('Cannot find user with key: ' + key + ' in Context');
    }
    return user;
  }


}
