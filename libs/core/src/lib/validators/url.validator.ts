import { ValidatorFn, AbstractControl } from '@angular/forms';

export function urlValidator(requireHttps?: boolean): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    if(!control.value) {
      return null;
    }
    const urlPattern =  /\b(?:https?):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
    const isAcceptedUrl = urlPattern.test(control.value);
    return isAcceptedUrl ? null : {'url': true};
  };
}
