@login-before-all
Feature: Select Marketing Page

Background:
  When I go to the Main page in Listing Dashboard
  And I wait for the Add Listing button to load
  Then the Add Listing button is present
  When I click on the Add Listing button

  # Enter in the form validation
  Then the Add New Listing page from Listing Dashboard is loaded
  When I enter data into the page form
  | Address         | 139 Park Drive  |
  | Unit Number     |                 |
  | City            | San Antonio     |
  | State           | TX              |
  | Zip Code        | 78212           |

  # Validate Navigation to the Select Marketing Packages Page
  And I click on the Continue button
  Then the Select Marketing Package page from Order Listing is loaded

@cleanup-order
Scenario: Opt Out Tests
  # Validate Select Marketing Package navigates to listing details
  And I wait for the Order Gold button to load
  When I click on the Order Gold button
  Then the Order Listing Details page from Order Listing is loaded

  # Validate the Photography step displays
  Then the "Professional Photography Header" element exists

  # Go back to Select Package and Opt-Out
  When I click on the Edit Package button
  Then the Select Marketing Package page from Order Listing is loaded
  And I wait for the Order Gold button to load

  # Check Opt Out
  When I check the "Use My Own Photography" checkbox
  Then the "Use My Own Photography" checkbox is checked

  # Continue order with opt out
  When I click on the Order Gold button
  Then the Order Listing Details page from Order Listing is loaded

  # Validate the Photography step displays
  Then the "Professional Photography Header" element does not exist

