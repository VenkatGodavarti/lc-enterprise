import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {
  OrderService,
  AllOrders,
  MarketingOrder,
  UpdateOrderPartial,
  SELECTEDPROFILE,
  Profile,
  UpdateOrderPartialComplete,
  OrderActionTypes,
  OrderFilterTypes,
  UIConstants, UserService
} from '@lc/core';
import { FormControl } from '@angular/forms';
import { debounceTime, map, tap } from 'rxjs/operators';
import { OrderTableColumns } from '../../components/order-table/order-table.component';
import { SortEvent } from '../../directives/sortable.directive';
import { PageRequest } from '../../components/paginator/paginator.component';
import { PromptDialogService, NotesDialogService } from '@lc/shared-components-ui';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lc-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.scss']
})
export class AllOrdersComponent implements OnInit, OnDestroy {

  private readonly preferencesPath = 'dashboard.allOrdersTable';

  private readonly defaultPageRequest = new PageRequest(1, 5);
  private readonly subscriptions: Subscription[] = [];
  private requestType: OrderFilterTypes;
  private sort: SortEvent;

  readonly profile$: Observable<Profile>;
  readonly search = new FormControl();

  orders$: Observable<any[]>;
  totalCount: number;

  columns: OrderTableColumns[] = ['address', 'agent', 'date', 'package', 'coordinator', 'actions'];
  showRequestButtons: boolean = false;

  public pageRequest: PageRequest = this.defaultPageRequest;

  preferences: any;

  constructor(private store: Store<any>,
              private actions: Actions,
              private noteDialog: NotesDialogService,
              private activatedRoute: ActivatedRoute,
              private orderService: OrderService,
              private promptDialog: PromptDialogService,
              private userService: UserService) {

    this.profile$ = store.pipe(select<Profile>(SELECTEDPROFILE));
    this.resetPageNumber();
    this.subscriptions.push(
      // Subscribe to the search box changes event and filter records after a 500ms debounce
      this.search.valueChanges
        .pipe(debounceTime(500))
        .subscribe(value$ => this.onFilter(value$), err => { })
    );
   }

  ngOnInit() {
    const filterType = this.activatedRoute.snapshot.data['type'];
    this.requestType = filterType;
    switch(filterType) {
      case OrderFilterTypes.mine:
        this.columns = ['address', 'agent', 'date', 'package', 'copy', 'photos'];
        this.showRequestButtons = true;
        break;
      case OrderFilterTypes.notMine:
        this.columns = ['address', 'agent', 'date', 'package', 'coordinator', 'copy', 'photos', 'actions'];
        break;
      case OrderFilterTypes.completed:
        this.columns = ['address', 'agent', 'date', 'package'];
        break;
    }
    this.getPreferences();
    this.getOrders();
  }

  ngOnDestroy() {
    this.subscriptions.filter(sub => sub != null).forEach(sub => sub.unsubscribe());
  }

  public onFilter(searchTerm) {
    // When the filter term changes, reset page number and re-query
    this.resetPageNumber();
    this.getOrders();
  }

  public onSort(sort: SortEvent) {
    // When the sort changes, reset page number and re-query
    this.sort = sort;
    this.resetPageNumber();
    this.getOrders();

    // Update preferences
    this.userService.setPreference(this.preferencesPath, 'sort',
      {"column": sort.column, "direction": sort.direction}
    );
  }

  public onPaginate(request: PageRequest) {
    // When paginating, set request and re-query
    this.pageRequest = request;
    this.getOrders();

    // Update preferences
    this.userService.setPreference(this.preferencesPath, 'paginate',
      {"pageSize": request.pageSize}
    );
  }

  /**
   * Opens the notes for a specific marketing order
   */
  public onOpenNotes(record: AllOrders) {
    this.noteDialog.openNotes(record._id, 'Add Note', record.listing.formatAddress())
  }

  public onAssignToMe(profile: Profile, record: AllOrders) {
    const description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
    this.promptDialog.openPrompt('Are you sure?', description, UIConstants.YES, [UIConstants.NO])
      .then(response => {
        if(response.text === UIConstants.YES){
          const partialOrder = new MarketingOrder({_id: record._id, coordinatorId: profile.userId});
          const subscription = this.actions.pipe(ofType<UpdateOrderPartialComplete>(OrderActionTypes.UpdateOrderPartialComplete))
            .subscribe(complete => {
              this.getOrders();
              subscription.unsubscribe();
            });
          this.store.dispatch(new UpdateOrderPartial(partialOrder, ['coordinatorId'] ));
        }
      });
  }

  private resetPageNumber() {
    // Set the page request back to default
    this.pageRequest.pageNumber = 1;
  }

  private getOrders(){
    // Build the search parameters and execute search
    const params: any = {};
    if(this.search.value) {
      params.search = this.search.value;
    }

    if(this.sort && this.sort.direction) {
      params.sortColumn = this.getSortColumn();
      params.sortDir =this.sort.direction;
    }
    if(this.pageRequest) {
      params.page = this.pageRequest.pageNumber;
      params.pageSize = this.pageRequest.pageSize;
    }
    this.orders$ = this.orderService.getOrders(this.requestType, params).pipe(
      tap(response => this.totalCount = response.totalCount),
      map(response => response.orders)
    );
  }

  private getPreferences() {
   const preferredSort = this.userService.getPreferenceValue(this.preferencesPath + ".sort");
   if (preferredSort) {
     this.sort = {column: preferredSort.column, direction: preferredSort.direction};
   }
   const preferredPaginate = this.userService.getPreferenceValue(this.preferencesPath + ".paginate");
   if (preferredPaginate) {
     this.pageRequest = new PageRequest(this.defaultPageRequest.pageNumber, preferredPaginate.pageSize);
   }
  }

  private getSortColumn() {
    // Set the actual column name that needs to be used upon sorting
    switch(this.sort.column) {
      case 'date':
        return this.nameof<AllOrders>('audit');
      case 'package':
        return this.nameof<AllOrders>('package');
    }
  }

  //Helper function to ensure type-safety
  nameof<T>(key: keyof T): keyof T {
    return key;
  }
}
