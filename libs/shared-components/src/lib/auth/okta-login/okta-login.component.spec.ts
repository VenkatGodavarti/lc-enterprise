import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OktaLoginComponent } from './okta-login.component';
import { OktaAuthenticationProvider } from '@lc/core';


describe('OktaLoginComponent', () => {
  let component: OktaLoginComponent;
  let fixture: ComponentFixture<OktaLoginComponent>;
  let oktaAuthenticationProviderSpy;


  beforeEach(async(() => {
    oktaAuthenticationProviderSpy = jasmine.createSpyObj('OktaAuthenticationProvider', ['getToken', 'login']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ OktaLoginComponent ],
      providers: [
        { provide: OktaAuthenticationProvider, useValue: oktaAuthenticationProviderSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OktaLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
