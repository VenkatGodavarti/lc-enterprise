
import {LogEntry} from './logentry';
import {Appender} from './appender';
import {ConsoleAppender} from "./console-appender";

/**
 *
 * A simple client-side Logger implementation with an interface similar to log4js.
 *
 * Usage:
 *
 *  Logger log = LoggerFactory.getLogger('my-logger');
 *
 *  log.debug('name: '+ name + ' is not valid');
 *
 */
export class Logger {

  /**
   * Logging levels ordered in decreasing exclusivity.
   *
   * For example if the Logger has NONE set as the level than no log output will be appended
   * for any levels. If the level is set to ALL then all all log level messages will be appended.
   */
  private static levels = [
    'NONE',
    'ERROR',
    'WARN',
    'INFO',
    'DEBUG',
    'TRACE',
    'ALL'
  ];

  /**
   * Internal logger for logging errors in the Logger.
   */
  private static errorLogger = new Logger('loggerErrors', {
    level : 'TRACE', appenders: [ new ConsoleAppender() ]});



  private levelIndex: number;
  private readonly category: string;
  private appenders: Appender[] = [];


  constructor(category: string, configuration: any) {

    const level = configuration.level || 'INFO';
    this.setLevel(level);
    this.category = category;
    if (configuration.appenders) {
      this.appenders.push(...configuration.appenders);
    }
  }

  /**
   * Alter the configured logging level
   *
   * @param level
   */
  setLevel(level: string) {
    const index = Logger.levels.indexOf(level);
    if (index !== undefined) {
      this.levelIndex = index;
    }
    else {
      Logger.errorLogger.error(level +' is not a valid logging level')
    }
  }

  /**
   * Add an appender to direct log output to another target
   *
   * @param appender
   */
  addAppender(appender: Appender): void {
    this.appenders.push(appender);
  }

  /**
   * Remove all appenders.
   */
  removeAllAppenders(): void {
    this.appenders = [];
  }

  /**
   * return true if the level is enabled
   *
   * @param level
   */
  isEnabled(level: string) {
    const index = Logger.levels.indexOf(level);

    const result =  (index !== undefined) ? index <= this.levelIndex : false;

    return result;
  }

  /**
   * append a log entry to all configured appenders if the level is allowed for the level configured
   * on this Logger.
   *
   * @param level
   * @param message
   */
  log(level: string, message: string) {
    this._log(Logger.levels.indexOf(level), message)
  }

  debug(message: string): void {
    this.log('DEBUG', message);
  }

  isDebugEnabled(): boolean {
    return this.isEnabled('DEBUG');
  }

  info(message: string): void {
    this.log('INFO', message);
  }

  isInfoEnabled(): boolean {
    return this.isEnabled('INFO');
  }

  warn(message: string): void {
    this.log('WARN', message);
  }

  isWarnEnabled(): boolean {
    return this.isEnabled('WARN');
  }


  error(message: string): void {
    this.log('ERROR', message);
  }

  isErrorEnabled(): boolean {
    return this.isEnabled('ERROR');
  }


  trace(message: string): void {
    this.log('TRACE', message);
  }

  isTraceEnabled(): boolean {
    return this.isEnabled('TRACE');
  }

  private append(entry: LogEntry) {
    this.appenders.forEach( (appender) => {

      appender.append(entry);
    })
  }

  private _log(level: number, message: string) {
    if (level <= this.levelIndex) {
      this.append(new LogEntry(Logger.levels[level], this.category, message))
    }
  }
}
