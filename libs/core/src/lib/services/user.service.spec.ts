import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AddressType, ApiService, CommonProfile, ContactInfo, ContactType, Profile, User, UserService } from '@lc/core';
import { TestBed } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { ContactInfoFactory } from '../models/contactinfo.model';

describe('UserService', () => {
  let httpMock: HttpTestingController;
  let service: UserService;

  let resource;
  let user: User;
  const userId = 'NEWUSERID';
  const firstName = 'Unit';
  const lastName = 'Tester';

  let profile: Profile;
  const preferredFirstName = 'Mr. Unit';
  const preferredLastName = 'Tester, III';

  beforeEach(() => {

    const storeSpy = jasmine.createSpyObj('Store', ['dispatch']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        ApiService,
        {provide: Store, useValue: storeSpy }
      ]
    });

    user = createUser();
    localStorage.setItem('currentUser', JSON.stringify(user));

    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(UserService);
  });

  afterEach(() => {
    localStorage.removeItem('currentUser');
    httpMock.verify();
  });

  describe('#getUserId', () => {
    it('returns the users ID', () => {
      const result = service.getUserId();
      expect(result).toEqual(userId);
    });

    it('returns null', () => {
      service.clearCurrentUser();
      const result = service.getUserId();
      expect(result).toBeNull();
    });
  });

  describe('#getUserFirstName', () => {
    it('returns the first name', () => {
      const result = service.getUserFirstName();
      expect(result).toEqual('Mr Unit');
    });
    it('returns an empty string', () => {
      service.clearCurrentUser();
      const result = service.getUserFirstName();
      expect(result).toEqual('');
    });
  });

  describe('#setCurrentUser', () => {
    it('does not save to local storage', () => {
      spyOn(localStorage, 'setItem');
      service.setCurrentUser(user, true);

      expect(localStorage.setItem).not.toHaveBeenCalled();
    });
    it('saves to local storage', () => {
      spyOn(localStorage, 'setItem');
      service.setCurrentUser(user, false);

      expect(localStorage.setItem).toHaveBeenCalledWith('currentUser', JSON.stringify(user));
    });
  });

  it('#clearCurrentUser nullifies any reference to a user', () => {
    service.clearCurrentUser();
    expect(service.getCurrentUser()).toBeNull();
  });

  describe('get/set preferences', () => {

    const preferences = {
      widget1: {
        color: "blue",
        size: { h: 10, w: 20 }
      },
      widget2: {
        color: 'green',
        size: { h: 10, w: 20 }
      }
    };

    beforeEach(() => {
      spyOn(service, 'getPreferences').and.returnValue(preferences);
      spyOn(service, 'savePreferences');
    });

    it('saves preferences on set', () => {
      service.setPreference('widget1.size', 'h', 25 );
      const got = service.getPreferenceValue('widget1.size.h');
      expect(got).toEqual(25);
      expect(service.savePreferences).toHaveBeenCalled();
    });

    it('changes correct preference on set', () => {
      // Update existing
      UserService.findAndSetPreference(preferences, 'widget1.size.h',25 );
      expect(preferences.widget1.size.h).toEqual(25);

      // Add new
      UserService.findAndSetPreference(preferences, 'widget2.shape','circle' );
      expect(preferences.widget2['shape']).toEqual('circle');
    });
  });

  describe('#getAddress', () => {
    it('returns null because there is no contactInfo data', () => {
      expect(service.getAddress(AddressType.BILLING)).toBeNull();
      expect(service.getAddress(AddressType.SHIPPING)).toBeNull();
    });

    it('returns shipping address info', () => {
      service.clearCurrentUser();
      const newUser = createUser([AddressType.SHIPPING, AddressType.BILLING]);
      service.setCurrentUser(newUser);
      localStorage.setItem('currentUser', JSON.stringify(newUser));

      const result = service.getAddress(AddressType.SHIPPING);
      expect(result).not.toBeNull();
      expect(result.type).toEqual(AddressType.SHIPPING);
    });

    it('returns billing address info', () => {
      service.clearCurrentUser();
      const newUser = createUser([AddressType.SHIPPING, AddressType.BILLING]);
      service.setCurrentUser(newUser);
      localStorage.setItem('currentUser', JSON.stringify(newUser));

      const result = service.getAddress(AddressType.BILLING);
      expect(result).not.toBeNull();
      expect(result.type).toEqual(AddressType.BILLING);
    });

  });

  function createUser(withContactInfo: AddressType[] = null) {
    user = new User();
    user._id = userId;
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = 'unit.test@lc.com';
    user.username = 'unit.tester';
    user.oktaId = 'ABCD-EFGH-IJKL-MNOP';
    user.preferences = [];
    user.userPermissions = ['any_permission', 'allow_impersonation'];
    user.schemaVersion = 2;
    user.profile = createProfile(withContactInfo);

    return user;
  }

  function createProfile(withContactInfo: AddressType[] = null) {
    profile = new Profile();
    profile._id ='NEWPROFILEID';
    profile.preferredFirstName = preferredFirstName;
    profile.preferredLastName = preferredLastName;
    profile.firstLogin = new Date();
    profile.commonProfile = new CommonProfile();
    if(withContactInfo) {
      profile.contactInfo = createContactInfoAddresses(withContactInfo)
    }

    return profile;
  }

  function createContactInfoAddresses(types: AddressType[]): ContactInfo[] {
    const contacts: ContactInfo[] = [];

    types.forEach((type) => {
      const contactInfo: ContactInfo = ContactInfoFactory.createContactInfo({
        type: ContactType.card,
        fullName: "Unit Tester",
        address: {
          type: type,
          streetAddress1: "street address 1",
          streetAddress2: "street address 2",
          city: "City",
          state: "CA",
          postalCode: "98765",
          country: "USA"
        },
        company: "Real Estate Company",
        phoneNumber: 4155551212,
        email: "unit.tester@realogy.com"
      });

      contacts.push(contactInfo);
    });

    return contacts;
  }
});
