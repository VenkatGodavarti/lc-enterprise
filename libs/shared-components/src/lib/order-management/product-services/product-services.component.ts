import { Component, OnDestroy, OnInit } from '@angular/core';
import { MarketingOrder, PackageInstance, SELECTEDORDER } from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'lc-product-services',
  templateUrl: './product-services.component.html',
  styleUrls: ['./product-services.component.scss']
})
export class ProductServicesComponent implements OnInit, OnDestroy {

  marketingOrder$: Observable<MarketingOrder>;
  marketingOrder: MarketingOrder;
  selectedPackage: PackageInstance;
  marketingOrderSub: Subscription;

  constructor(private store: Store<any>) {
    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));
  }

  ngOnInit() {
    this.marketingOrderSub = this.marketingOrder$.subscribe((marketingOrder) => {
      this.marketingOrder = marketingOrder;
      this.selectedPackage = marketingOrder.getSelectedPackage();
    });
  }

  ngOnDestroy(): void {
    if (this.marketingOrderSub) {
      this.marketingOrderSub.unsubscribe();
    }
  }

}
