export const environment = {
  // App configuration
  production: false,
  envName: 'ops',
  applicationName: 'Listing Concierge Photo App OPS',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://lc-photo.ops.concierge.realogydev.com/',
  apiBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-persist/api/v1/',
  photosBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-image/api/v1/',
  notifyBaseURL: 'https://api.ops.concierge.realogydev.com/lcms-notify/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzv68e4gqBdDNZ0h7',
  oktaRedirectUri: 'https://lc-photo.ops.concierge.realogydev.com/implicit/callback',
};
