import { BehaviorSubject } from "rxjs";
import { User } from "../models/user.model";
import { UserService } from "../services/user.service";

export class UserServiceMocks {
  static buildUserService(user?: User): UserService {
    user = user || new User({
      _id: '123',
      firstName: 'Unit',
      lastName: 'Test'
    });

    const currentUserSubject = new BehaviorSubject<User>(user);

    let service = jasmine.createSpyObj<UserService>(['currentUser', 'getCurrentUser', 'getUserId', 'getUserFirstName', 'setCurrentUser', 'clearCurrentUser']);
    service = {
      ...service,
      currentUser: currentUserSubject.asObservable()
    } as jasmine.SpyObj<UserService>;

    service.getCurrentUser.and.callFake(() => user);
    service.getUserId.and.callFake(() => user._id);
    service.getUserFirstName.and.callFake(() => user.firstName);
    service.setCurrentUser.and.callFake(u => {
      user = u;
      currentUserSubject.next(user);
    });

    service.clearCurrentUser.and.callFake(() => {
      user = null;
      currentUserSubject.next(user);
    });

    return service;
  };

}
