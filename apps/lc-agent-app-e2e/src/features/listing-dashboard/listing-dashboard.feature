@listing-dashboard
@login-before-all
Feature: Listing dashboard page

Background:
  Given I am logged in to Listing Concierge

# Note: This test assumes there are not already any marketing orders
@show-hide-orders
Scenario: Verify that hide/show toggle buttons work
  Given the following orders exist
    | Name       | Alamo        | White House  |
    | Agent Type | Single Agent | Single Agent |
    | Status     | PENDING      | PENDING      |

  When I go to the Main page in Listing Dashboard
  And I wait for the Add Listing button to load
  Then I should see 2 orders

  When I hover over the Listing Image of order 1
  And I click the hide order button on order 1
  Then I should see 1 orders

  When I hover over the Listing Image of order 1
  And I click the hide order button on order 1
  Then I should see 0 orders

  When I click on the Show Archived button
  Then I should see 2 orders

  When I hover over the Listing Image of order 1
  And I click the show order button on order 1
  Then I should see 1 orders

@delete-orders
Scenario: Delete order works
  Given the following orders exist
    | Name       | Alamo        | White House  | White House  |
    | Agent Type | Single Agent | Single Agent | Single Agent |
    | Status     | PENDING      | PENDING      | OPEN         |
    | Submitted  | true         | true         | false        |

  # Navigate to the listing dashboard page and wait for the orders to load
  When I go to the Main page in Listing Dashboard
  And I wait for the Add Listing button to load
  Then I should see 3 orders

  # Verify the delete button is not available
  When I hover over the Listing Image of order 1
  Then the Delete Order Button element of order 1 is not present

  # Verify the delete button is not available
  When I hover over the Listing Image of order 2
  Then the Delete Order Button element of order 2 is not present

  # Hover over and click the delete button to remove an order
  When I hover over the Listing Image of order 3
  And I hover over the Delete Order Button of order 3
  And I click the Delete Order Button of order 3
  Then the "Prompt" dialog is displayed

  # Click yes on the delete confirmation dialog
  When I click on the "Cancel" button inside the "Prompt Dialog"
  And I wait for the Add Listing button to load
  Then I should see 3 orders

  # Hover over and click the delete button to remove an order
  When I hover over the Listing Image of order 3
  And I hover over the Delete Order Button of order 3
  And I click the Delete Order Button of order 3
  Then the "Prompt" dialog is displayed

  # Click yes on the delete confirmation dialog
  When I click on the "Yes" button inside the "Prompt Dialog"
  And I wait for the Add Listing button to load
  Then I should see 2 orders

  # verify on page reload that there is the expected number of orders
  When I go to the Main page in Listing Dashboard
  And I wait for the Add Listing button to load
  Then I should see 2 orders

@order-mgmt-nav
Scenario: Navigate to Order Management
  Given the following orders exist
    | Name                 | Alamo                 |
    | Package              | Gold                  |
    | Street               | 123 Main Street       |
    | City                 | Madison               |
    | State                | NJ                    |
    | Zip Code             | 07040                 |
    | Status               | PENDING               |
    | Property Information | default               |

  When I go to the Main page in Listing Dashboard
  When I click the Manage Order button for the Alamo order
  Then the Summary page from Order Management is displayed
  And the "Street Address" text is "123 Main Street"

  Scenario: Displaying filtered orders
    Given the following orders exist
      | Name       | Alamo        | White House  | Alamo      | White House | Alamo   | Alamo | Alamo         |
      | Agent Type | Single Agent | Single Agent | Co-Listing | Co-Listing  | Team    | Team  | Single Agent  |
      | Status     | PENDING      | PENDING      | PENDING    | PENDING     | PENDING | OPEN  | OPEN          |
    And I go to the Main page in Listing Dashboard

    # Check the results of each of the filters on the listing dashboard
    When I select the "Single Agent" option from the Marketed As dropdown
    Then I should see 3 orders

    When I select the "Co-Listing" option from the Marketed As dropdown
    Then I should see 2 orders

    When I select the "Team" option from the Marketed As dropdown
    Then I should see 2 orders

    When I select the "All" option from the Marketed As dropdown
    Then I should see 7 orders

  Scenario: Hovering over orders to see marketed by info
    Given the coldwell team profile exists
    Given the following orders exist
      | Name       | Alamo        | White House | Alamo       |
      | Agent Type | Single Agent | Co-Listing  | Team        |
      | Status     | PENDING      | PENDING     | OPEN        |
      | List Date  | 06/03/2019   | 06/02/2019  | 06/01/2019  |

      And I go to the Main page in Listing Dashboard

    # checking the hover state over the listing image
    When I hover over the Listing Image of order 1
    Then the Agent Type element of order 1 contains Single Agent
    And the Archive Order element of order 1 is present

    When I hover over the Listing Image of order 2
    Then the Agent Type element of order 2 contains Co-Listing
    And the Archive Order element of order 2 is present

    When I hover over the Listing Image of order 3
    Then the Agent Type element of order 3 contains Team
    And the Archive Order element of order 3 is not present

    # checking the hover state of the listing popovers
    When I hover over the Listing Image of order 2
    And I hover over the Agent Type of order 2
    Then the Co-Listing Info element of order 2 is present

    When I hover over the Listing Image of order 3
    And I hover over the Agent Type of order 3
    Then the Team Info element of order 3 is present

  Scenario: Hovering over orders that do not have additional info
    Given the following orders exist
      | Name       | Alamo |
      | Status     | OPEN  |
    And I go to the Main page in Listing Dashboard

    When I hover over the Listing Image of order 1
    And the Archive Order element of order 1 is not present
