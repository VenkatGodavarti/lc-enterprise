import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NgbTabset, NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { filter } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Profile, SELECTEDPROFILE } from '@lc/core';

@Component({
  selector: 'lc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  selectedTab: any;
  readonly profile$: Observable<Profile>;
  readonly subscriptions: Subscription[] = [];
  readonly tabs = [
    {id: 'my-orders', isSelected: false, label: 'My Orders', path: '/orders/mine'},
    {id: 'all-orders', isSelected: true, label: 'All Orders', path: '/orders'},
    {id: 'completed-orders', isSelected: false, label: 'Completed', path: '/orders/completed'},
  ];

  @ViewChild(NgbTabset)
  tabSet: NgbTabset;

  constructor(store: Store<any>, private router: Router, private activatedRoute: ActivatedRoute) {
    this.profile$ = store.pipe(select<Profile>(SELECTEDPROFILE));

    this.subscriptions.push(
      // Listen to navigation events and set selected tab accordingly
      this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => this.setSelectedTab())
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // Unsubscribe to all subscriptions
    this.subscriptions.filter(sub => sub != null).forEach(sub => sub.unsubscribe());
  }

  tabChange($event: NgbTabChangeEvent) {
    // Find the corresponding tab by Id and navigate to the path
    const tab = this.tabs.find(tab => tab.id === $event.nextId);
    this.router.navigate([tab.path]);
  }

  private setSelectedTab(){
    // Find the tab related to the current activated route
    let path = this.activatedRoute.snapshot.firstChild.url.map(url => url.path).join('/');
    path = path.indexOf('/') === 0 ? path : '/' + path;

    // Mark the tab that matches the path as selected
    this.tabs.forEach(tab => tab.isSelected = tab.path === path);

    // Update the tabSet in the UI with the selected tab
    this.selectedTab = this.tabs.find(tab => tab.isSelected === true);
    if(this.selectedTab && this.tabSet && this.tabSet.tabs){
      this.tabSet.select(this.selectedTab.id);
    }
  }
}
