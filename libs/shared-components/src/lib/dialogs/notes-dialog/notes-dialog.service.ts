import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotesDialogComponent } from './notes-dialog.component';

/**
 * Handles the creation of the Notes Dialog component by requiring proper parameter construction
 * and eliminates the need to inject the NgbModal into the components directly. This allows
 * the ability to change implementation if we wanted to switch to a different dialog component (i.e. - Angular Material)
 */
@Injectable()
export class NotesDialogService {
  constructor(private modal: NgbModal) { }

  /**
   * Opens the notes dialog component with the required parameters
   * @param ownerId The ownerId we are requesting notes for
   * @param title The title of the dialog
   * @param subheader The subtitle of the dialog
   */
  openNotes(ownerId: string, title: string, subtitle: string) {
    const dialogRef = this.modal.open(NotesDialogComponent);
    const component: NotesDialogComponent = dialogRef.componentInstance;

    // Initialize the dialog with the properties
    component.initialize(ownerId, title, subtitle);

    component.close.subscribe(action => dialogRef.close(action));
    return dialogRef.result.then(() => null, () => null); // Handle dismiss and close
  }
}
