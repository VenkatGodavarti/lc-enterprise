import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderStepTemplateComponent } from './order-step-template.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { FlowState, FlowStep } from '@lc/core';
import SpyObj = jasmine.SpyObj;

describe('OrderStepTemplateComponent', () => {
  let component: OrderStepTemplateComponent;
  let fixture: ComponentFixture<OrderStepTemplateComponent>;
  let activatedRoute: ActivatedRoute;
  let store: SpyObj<Store<any>>;
  let state: SpyObj<FlowState>;

  beforeEach(async(() => {
    activatedRoute = new ActivatedRoute();
    activatedRoute.data = of({});
    spyOnProperty(activatedRoute, 'parent', 'get').and.returnValue(activatedRoute);
    spyOnProperty(activatedRoute, 'paramMap', 'get').and.returnValue(of(convertToParamMap({})));

    state = jasmine.createSpyObj('FlowState', ['setStep']);

    store = jasmine.createSpyObj('Store', ['pipe']);
    store.pipe.and.returnValue(of(state));

    TestBed.configureTestingModule({
      declarations: [ OrderStepTemplateComponent ],
      providers: [
        { provide: Store, useValue: store },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStepTemplateComponent);
    component = fixture.componentInstance;
    component.orderState = new FlowState({currentStep: new FlowStep('x')});
    component.step = new FlowStep('x');

    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
