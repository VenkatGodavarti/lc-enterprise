// NOTE: This file should not be getting used unless you are using "ng serve" or "ng build" without specifying a configuration.
// Instead of modifying this file for your environment specific variables, please use "ng serve -c [configuration]"

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const unknownConfigurationMessage = "Unknown Configuration - Please us -c flag";
export const environment =  {
  // App confiuration
  production: false,
  envName: unknownConfigurationMessage,
  applicationName: unknownConfigurationMessage,
  applicationKey: unknownConfigurationMessage,

  // API/UI Endpoints
  baseURL: 'http://localhost:4200/',
  apiBaseURL: 'https://lc.dev.lcms-persist.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.dev.lc-notify.realogy.com/api/v1/',
  //apiBaseURL: 'http://localhost:4100/api/v1/',
  photosBaseURL: 'https://lc.dev.lcms-image.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oaglhfi71bLgkLoe0h7',
  oktaRedirectUri: 'http://localhost:4200/implicit/callback',

   //Stripe Key 

   stripePublishKey:'pk_test_GzgXKTGsDAOYaX6jHsDzEFQ800jzeOk9BL',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
