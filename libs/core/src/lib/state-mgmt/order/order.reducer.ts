import { Listing, MarketingOrder, MultiMediaItem, Audit } from '../../models';
import { OrderActionTypes } from './order.actions';

export const initialState:MarketingOrder = null;

export function orderReducer(state = initialState, action): MarketingOrder {
  switch (action.type) {
    case OrderActionTypes.InitializeOrder: {
      return null;
    }
    // set state to loaded order
    case OrderActionTypes.LoadOrder:
    case OrderActionTypes.GetOrderComplete:
    case OrderActionTypes.UpdateOrderComplete: {
      return new MarketingOrder(action.payload);
    }
    // set state to new order
    case OrderActionTypes.UpdateOrderPartialComplete: {
      return new MarketingOrder(action.payload);
    }
    // overlay new payload on old listing
    case OrderActionTypes.UpdateOrderListing: {
      state.listing = new Listing(Object.assign(state.listing, action.payload.listing));
      return state;
    }
    case OrderActionTypes.UpdateOrderPhotosComplete: {
      state.setPhotos(action.photos, action.productCode);
      return state;
    }
    case OrderActionTypes.AddMarketingOrderMultimediaLinkComplete:
      const multiMediaItem = new MultiMediaItem(action.payload);
      const foundItem = state.media.multimedia.find(function(elt) {
        return elt._id === multiMediaItem._id;
      });
      if (foundItem) {
        Object.assign(foundItem, multiMediaItem);
        foundItem.audit = new Audit(multiMediaItem.audit);
      } else {
        state.media.multimedia.push(multiMediaItem);
      }
      // by returning a new marketing order the UI that is looking at this object will
      // trigger an update
      return new MarketingOrder(state);
    default:
      return state;
  }
}
