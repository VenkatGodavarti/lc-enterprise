import { Deserializable } from './deserializable.model';
import { PhoneNumber } from './phone-number.model';
import { CommonProfile } from './common-profile.model';
import { ContactInfo, ContactInfoFactory } from './contactinfo.model';

export class Profile implements Deserializable<Profile> {

  _id: string;
  userId: string;
  preferredFirstName: string;
  preferredLastName: string;
  preferredEmail: string;
  phoneNumbers: PhoneNumber[];
  commonProfile: CommonProfile;
  photoUrl: string;
  logoUrl: string;
  originalPhotoUrl : string;
  firstLogin: Date;
  website: string;
  contactInfo: ContactInfo[];


  constructor(input?: any) {
    this.deserialize(input);
  }

  deserialize(input?: any): Profile {
    if(!input) { return this; }
    Object.assign(this, input);
    if(typeof input.user === "string") {
      this.userId = input.user;
    }
    if (this.contactInfo && this.contactInfo.length) {
      this.contactInfo = this.contactInfo.map( ci => ContactInfoFactory.createContactInfo(ci));
    }

    if(input.commonProfile) {
      this.preferredFirstName = input.preferredFirstName || input.commonProfile.firstName;
      this.preferredLastName = input.preferredLastName || input.commonProfile.lastName;
      this.preferredEmail = input.preferredEmail || input.commonProfile.email;
    }

    return this;
  }
}
