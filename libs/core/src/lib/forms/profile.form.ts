import { FormArray, FormControl, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { PhoneNumberForm } from './phone-number.form';
import { PhoneNumber } from '../models';

export class ProfileForm extends BaseForm {
  public get phoneNumbers(): PhoneNumber[] { return this.get('phoneNumbers').value; }
  public get photoUrl(): string { return this.get('photoUrl').value; }
  public get logoUrl(): string { return this.get('logoUrl').value; }

  constructor(public readonly originalValue?: any, public options?: {validateType: boolean, phoneNotRequired: boolean }) {
    super({
      _id: new FormControl(),
      preferredFirstName: new FormControl(),
      preferredLastName: new FormControl(),
      preferredEmail: new FormControl(null, [Validators.email]),
      website: new FormControl(),
      photoUrl: new FormControl(),
      originalPhotoUrl: new FormControl(),
      presignedPhotoUrl: new FormControl(),
      logoUrl: new FormControl(),
      phoneNumbers: new FormArray([]),
      contactInfo: new FormArray([])
    });


    if(this.originalValue) {
      this.patchValue(this.originalValue);
    }
  }

  patchValue(value: any, options?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.patchValue(value, options);
    if(value.phoneNumbers) {
      const phoneNumbers = this.get('phoneNumbers') as FormArray;
      while (phoneNumbers.length !== 0) {
        phoneNumbers.removeAt(0);
      }
      value.phoneNumbers.forEach(phoneNumber => phoneNumbers.push(new PhoneNumberForm(phoneNumber, this.options)));
    }
  }
}
