import { Deserializable } from './deserializable.model';

/*
 * Phone number type used for dropdown selects and labeling
 */
export enum PhoneType {
  mobile = 'mobile',
  direct = 'direct',
  office = 'office'
}

export class PhoneNumber implements Deserializable<PhoneNumber> {
  type: PhoneType;
  number: string;
  extension: string;
  priority: number;
  prefix: string;
  primary = false;

  constructor(input?: any) {

    this.deserialize(input || {});
  }

  deserialize(input: any): PhoneNumber {
    Object.assign(this, input);

    return this;
  }
}
