import { Given, When } from 'cucumber';
import { Configuration, MarketingOrdersClient, Context, ContextKeys, Scheduler, Schedule } from '../../e2e';
import { verticalTableToRecords } from '../../e2e/step_definitions';
import { MarketingOrderProps } from './marketing-order.props';
import { BaseData } from './base-data';
import { PhotographersClient, PhotoOrdersClient, PackagesClient } from '../../e2e/clients';

const orderProps = new MarketingOrderProps();

// i.e. - Given the Alamo order exists
Given(/^the (.*) order exists$/,  { timeout: Configuration.timeout },  async (orderName: string) => {
  let order = readOrderFromFile(orderName);
  await createOrder(order, orderName);
});

/**
 * Sets the order state on the current order
 */
Given(/^the order is on the (.*) step$/,  { timeout: Configuration.timeout },  async (stepName: string) => {
  const currentOrder = Context.get(ContextKeys.order);
  setOrderStep(currentOrder, stepName);
  await MarketingOrdersClient.updateOrderState(currentOrder._id, currentOrder.orderState);
});


Given(/^the following orders exist$/,  { timeout: Configuration.timeout },  async (rawTable) => {
  const ordersToCreate = verticalTableToRecords(rawTable);
  await createOrders(ordersToCreate);
});

Given(/^the order has "(.*)" photos$/, {timeout: Configuration.timeout}, async (count: number) => {
  const order = Context.get(ContextKeys.order);
  const photos: any[] = BaseData.readFileToObject(`marketing-orders/photos/photos.json`);
  order.photos = photos.splice(0, count);
  await MarketingOrdersClient.put(order);
});

Given(/^the order has "(.*)" (multimedia links)$/, {timeout: Configuration.timeout}, async (count: number, type: string) => {
  const order = Context.get(ContextKeys.order);
  if(order.media == null || order.media.multimedia == null) {
    order.media = { multimedia: []};
  }
  const multimedia = order.media.multimedia;
  for (let i = multimedia.length; i < count; i++) {
    multimedia.push({
      title: 'Link-'+i,
      uri: 'https://realogy.com/link-'+i,
    })
  }

  await MarketingOrdersClient.put(order);
});

When(/^I select the (.*) order$/,  {timeout: Configuration.timeout}, (orderName: string) => {
  const order = Context.get(orderName+'-'+ContextKeys.order);
  Context.set(ContextKeys.order, order);
  Context.set(ContextKeys.orderId, order._id);
});

async function createOrders(ordersToCreate) {

  for (let i = 0; i < ordersToCreate.length; i++) {

    const orderToCreate = ordersToCreate[i];

    const orderName = orderToCreate.Name || orderToCreate.orderName;

    let order = readOrderFromFile(orderName);
    order.agentId = Context.get(ContextKeys.apiUserId);
    await updateOrderProperties(order, orderToCreate);
    await createOrder(order, orderName);

    if(orderToCreate['Photo Order']) {
      order.photoOrder = await createPhotoOrder(order, orderToCreate['Photo Order']);
    }
  }
}

function readOrderFromFile(orderName) {
  if(!orderName) {
    throw Error('orderName must be specified');
  }
  const fileName = orderName.replace(' ', '-').toLowerCase();
  let order = BaseData.readFileToObject(`marketing-orders/${fileName}.json`);
  order.agentId = Context.get(ContextKeys.apiUserId);
  return order;
}

async function updateOrderProperties(order, tableRecord) {
  order.availablePackages = await getPackages();

  orderProps.setValues(order, tableRecord, ['orderName', 'Name', 'Photo Order']);
}


function setOrderStep(order, stepName) {
  const orderState = BaseData.readFileToObject(`marketing-orders/order-states/${stepName.toLowerCase()}-step.json`);
  order.orderState = orderState;
}

async function createPhotoOrder(order, photoOrderName) {
  const photoOrder = await readPhotoOrder(photoOrderName);
  order.photoOrder = photoOrder;

  // Need to get the proper photographerId from database as it changes
  const photographers = await PhotographersClient.get();
  const photographer = photographers.find(p => p.name === photoOrder.appointment.photographer.name);
  photoOrder.appointment.photographer = photographer;
  photoOrder.photographerId = photographer._id;

  const user = Context.get(ContextKeys.user);
  photoOrder.propertyAddress = order.listing.address;
  photoOrder.agentName = `${user.firstName} ${user.lastName}`;

  const created = await MarketingOrdersClient.submit(order);
  // TODO: Cleanup photo Order
  return photoOrder;
}

async function readPhotoOrder(photographerName) {
  if(!photographerName) {
    throw Error('photoOrderName must be specified');
  }

  const fileName = photographerName.replace(' ', '-').toLowerCase();
  let order = BaseData.readFileToObject(`photo-orders/${fileName}.json`);
  return order;
}

async function createOrder(order, orderName?: string) {
  Context.set(ContextKeys.order, order);

  const response = await MarketingOrdersClient.post(order);

  order._id = response.data._id;
  Context.set(ContextKeys.orderId, order._id);

  if(orderName){
    Context.set(orderName+'-'+ContextKeys.order, order);
  }

  // Schedule a cleanup of the order
  Scheduler.schedule(Schedule.afterScenario, async () => {
    await deleteOrder(order);
  });
}

let packages = null
async function getPackages() {
  if(!packages) {
    packages = await PackagesClient.getMyPackages();
  }
  return packages;
}

async function deleteOrder(order) {
  await MarketingOrdersClient.delete(order._id);
  let photoOrders = await PhotoOrdersClient.get();
  photoOrders = photoOrders.filter(photoOrder => photoOrder.marketingOrderId === order._id);

  photoOrders.forEach(async (photoOrder) => {
    await PhotoOrdersClient.delete(photoOrder._id);
  });
}
