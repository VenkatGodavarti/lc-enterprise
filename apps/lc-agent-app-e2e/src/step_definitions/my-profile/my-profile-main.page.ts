import { LCPage, ElementMap, Element  } from '../../../../../e2e';

export class MyProfileMainPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Form Header',           field: '#form-header'}),
      new Element( { name: 'My Team',               field: '#my-team-link'}),
      new Element( { name: 'Upload Failed',         field: 'button#upload-failed-btn'}),
      new Element( { name: 'Cancel',                field: 'button#profile-cancel'}),
      new Element( { name: 'Update Profile',        field: 'button#profile-submit'}),
      new Element( { name: 'Edit Profile Photo',    field: 'i#editProfilePhoto'}),
      new Element( { name: 'Change File link',      field: 'a#change-profiile-image'}),
      new Element( { name: 'Apply Profile Image',   field: 'button#profile-image-update'}),
      new Element( { name: 'Profile Avatar',        field: 'img#avatar'}),
      new Element( { name: 'Logo',                  field: 'img#agent-logo'}),
      new Element( { name: 'Header Profile Avatar', field: 'img#header-profile-avatar'}),
      new Element( { name: 'Change File',           field: 'a#change-profile-image'}),
      new Element( { name: 'Upload File',           field: 'button#chooseFileButton'}),
      new Element( { name: 'Agent Logo',           field: 'img#agent-logo'}),
      new Element( { name: 'Edit Logo',           field: 'span#edit-logo-button'})
    ]
  );

  static formElements = new ElementMap( 'Profile',
    [
      new Element( { name: 'First Name',    model: 'preferredFirstName', field: 'input#first-name-input'}),
      new Element( { name: 'Last Name',     model: 'preferredLastName',  field: 'input#last-name-input'}),
      new Element( { name: 'Email',         model: 'preferredEmail',     field: 'input#email-input'}),
      new Element( { name: 'Website',       model: 'website',            field: 'input#website-input'}),
      new Element( { name: 'Image',         model: 'photoUrl',           field: 'input#fileInput'}),
      new Element( { name: 'Logo',          model: 'logoUrl',            field: 'input#uploadPhotos'}),
      new Element( { name: 'Primary Phone Number',   field: 'input#phone-0-input'}),
      new Element( { name: 'Primary Phone Type',     field: 'select#type-0-input'}),
      new Element( { name: 'Secondary Phone Number', field: 'input#phone-1-input'}),
      new Element( { name: 'Secondary Phone Type',   field: 'select#type-1-input'}),
      new Element( { name: 'Tertiary Phone Number',  field: 'input#phone-2-input'}),
      new Element( { name: 'Tertiary Phone Type',    field: 'select#type-2-input'}),
    ]);

  constructor(pageName: string = 'My Profile') {
    super(pageName, [MyProfileMainPage.elements, MyProfileMainPage.formElements]);
  }

}
