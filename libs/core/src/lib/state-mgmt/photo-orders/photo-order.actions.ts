import { Action } from '@ngrx/store';
import { PhotoOrder } from '../../models/photo-order.model';
import { ListingPhoto, MultiMediaItem, UploadPhoto } from '../../models/';
import { MultiMediaItemRequest } from '../../models/multi-media-item.model';


export enum PhotoOrderActionTypes {
  InitializePhotoOrder = '[PhotoOrder] Initialize',

  LoadPhotoOrder = '[PhotoOrder] Load',

  GetPhotoOrder = '[PhotoOrder] Get',
  GetPhotoOrderComplete = '[PhotoOrder] Get Complete',

  UpdatePhotoOrder = '[PhotoOrder] Update',
  UpdatePhotoOrderPartial = '[PhotoOrder] Update Partial',
  UpdatePhotoOrderComplete = '[PhotoOrder] Update Order Complete',
  UpdatePhotoOrderFailed = '[PhotoOrder] Update Order Failed',

  UpdatePhotoOrderPhotos = '[PhotoOrder] Update Photo Order Photos',
  UpdatePhotoOrderPhotosComplete = '[PhotoOrder] Update Photo Order Photos Complete',

  LoadPhotoOrderMedia = '[PhotoOrder] Load Order Media',
  LoadPhotoOrderMediaComplete = '[PhotoOrder] Load Order Media Complete',
  AddPhotoOrderMultimediaLink = '[PhotoOrder] Add Multimedia Link',
  AddPhotoOrderMultimediaLinkComplete = '[PhotoOrder] Add Multimedia Link Complete',

  LoadPhotoOrders = '[PhotoOrder] Get Photo Orders',
  LoadPhotoOrdersComplete = '[PhotoOrder] Get Photo Orders Complete',
  PhotoOrderSelected = '[PhotoOrder] Selected',

  UploadPhotoOrderPhotos = '[PhotoOrder] Upload Photo Order Photos',

  SubmitPhotoOrderPhotos = '[PhotoOrder] Submit Photo Order Photos',
  SubmitPhotoOrderPhotosComplete = '[PhotoOrder] Submit Photo Order Photos Complete',

  AddPhotosToOrder = '[PhotoOrder] Add photos'
}

/**
 * Create ease of consumption wrapper actions
 * Eg.
 *
 * new LoadOrder(new MarketingOrder());
 */

export class InitializePhotoOrder implements Action {
  readonly type = PhotoOrderActionTypes.InitializePhotoOrder;
  constructor() {}
}

export class LoadPhotoOrder implements Action {
  readonly type = PhotoOrderActionTypes.LoadPhotoOrder;
  constructor(public payload: PhotoOrder) {}
}

export class GetPhotoOrder implements Action {
  readonly type = PhotoOrderActionTypes.GetPhotoOrder;
  constructor(public payload: PhotoOrder) {}
}

export class GetPhotoOrderComplete implements Action {
  readonly type = PhotoOrderActionTypes.GetPhotoOrderComplete;
  constructor(public payload: PhotoOrder) {}
}

export class PhotoOrderSelected implements Action {
  readonly type = PhotoOrderActionTypes.PhotoOrderSelected;
  constructor(public payload) {}
}

export class UpdatePhotoOrder implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrder;
  constructor(public payload: PhotoOrder) {}
}

export class UpdatePhotoOrderPartial implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrderPartial;
  constructor(public payload: PhotoOrder, public fields: string[]) {}
}

export class UpdatePhotoOrderComplete implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrderComplete;
  constructor(public payload: PhotoOrder) {}
}

export class UpdatePhotoOrderFailed implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrderFailed;
  constructor(public payload: PhotoOrder) {}
}

export class LoadPhotoOrderMedia implements Action {
  readonly type = PhotoOrderActionTypes.LoadPhotoOrderMedia;
  constructor(public payload: PhotoOrder) {}
}

export class LoadPhotoOrderMediaComplete implements Action {
  readonly type = PhotoOrderActionTypes.LoadPhotoOrderMediaComplete;
  constructor(public payload: PhotoOrder) {}
}

export class AddPhotoOrderMultimediaLink implements Action {
  readonly type = PhotoOrderActionTypes.AddPhotoOrderMultimediaLink;
  constructor(public payload: MultiMediaItemRequest) {}
}

export class AddPhotoOrderMultimediaLinkComplete implements Action {
  readonly type = PhotoOrderActionTypes.AddPhotoOrderMultimediaLinkComplete;
  constructor(public payload: MultiMediaItem) {}
}

export class UpdatePhotoOrderPhotos implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrderPhotos;
  constructor(public payload: PhotoOrder, public photos: ListingPhoto[]) {}
}

export class UpdatePhotoOrderPhotosComplete implements Action {
  readonly type = PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete;
  constructor(public payload: PhotoOrder, public photos: UploadPhoto[]) {}
}

export class LoadPhotoOrders implements Action {
  readonly type = PhotoOrderActionTypes.LoadPhotoOrders;
  constructor(public payload: string) {}
}

export class LoadPhotoOrdersComplete implements Action {
  readonly type = PhotoOrderActionTypes.LoadPhotoOrdersComplete;
  constructor(public payload: PhotoOrder[]) {}
}

export class UploadPhotoOrderPhotos implements Action {
  readonly type = PhotoOrderActionTypes.UploadPhotoOrderPhotos;
  constructor(public payload: PhotoOrder, public newPhotos: UploadPhoto[]) {}
}

export class SubmitPhotoOrderPhotos implements Action {
  readonly type = PhotoOrderActionTypes.SubmitPhotoOrderPhotos;
  // Payload is photoOrderId
  constructor(public payload: string, public photoUrls: string[]) {}
}

export class SubmitPhotoOrderPhotosComplete implements Action {
  readonly type = PhotoOrderActionTypes.SubmitPhotoOrderPhotosComplete;
  // Payload is array of photoUrls
  constructor(public payload: string[]) {}
}

export type PhotoOrderActions =
  InitializePhotoOrder
  | LoadPhotoOrder
  | GetPhotoOrder
  | GetPhotoOrderComplete
  | PhotoOrderSelected
  | UpdatePhotoOrder
  | UpdatePhotoOrderPartial
  | UpdatePhotoOrderComplete
  | UpdatePhotoOrderFailed
  | LoadPhotoOrderMedia
  | LoadPhotoOrderMediaComplete
  | AddPhotoOrderMultimediaLink
  | AddPhotoOrderMultimediaLinkComplete
  | LoadPhotoOrders
  | LoadPhotoOrdersComplete
  | UploadPhotoOrderPhotos
  | UpdatePhotoOrderPhotos
  | UpdatePhotoOrderPhotosComplete
  | SubmitPhotoOrderPhotos
  | SubmitPhotoOrderPhotosComplete
  ;

