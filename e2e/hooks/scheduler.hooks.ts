import { After, AfterAll } from 'cucumber';
import { Scheduler, Schedule } from '../support';
import { Configuration as config } from '../configuration';

/**
 *  Execute functions scheduled to be executed after a scenario completes
 */
After(  { timeout: config.timeout }, async () => {
  await Scheduler.execute(Schedule.afterScenario);
});

/**
 *  Execute any scheduled functions after all scenarios are complete
 */
AfterAll(  { timeout: config.timeout }, async () => {
  await Scheduler.executeAll();
});
