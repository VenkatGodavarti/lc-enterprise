import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, MetaReducer } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ORDERSTATE } from './workflow/workflow.actions';
import { workflowReducer } from './workflow/workflow.reducer';
import { orderReducer } from './order/order.reducer';
import { SELECTEDORDER } from './order/order.actions';
import { WorkflowEffects } from './workflow/workflow.effects';
import { OrderEffects, OrderStateEffects } from './order/order.effects';
import { debugReducer } from './debug.reducer';
import { LOGGEDINUSER } from './user/user.actions';
import { userReducer } from './user/user.reducer';

import { SELECTEDPROFILE } from './profile/profile.actions';
import { profileReducer } from './profile/profile.reducer';
import { ProfileEffects } from './profile/profile.effects';
import { SELECTEDTEAM } from './team/team.actions';
import { teamReducer } from './team/team.reducer';
import { TeamEffects } from './team/team.effects';
import { NOTIFICATIONS } from './notifications/notification.actions';
import { notificationReducer } from './notifications/notification.reducer';
import { NotificationEffects } from './notifications/notification.effects';
import { photoOrderReducer, PHOTOORDERS } from './photo-orders/photo-order.reducer';
import { PhotoOrderEffects} from './photo-orders/photo-order.effects';
import { keysReducer } from './keys/key.reducer';
import { KEYS } from './keys/key.actions';
import { APP_OPTIONS } from './options/options.actions';
import { optionsReducer} from './options/options.reducer';
import { OptionsEffects} from './options/options.effects';

export const metaReducers: MetaReducer<any>[] = [debugReducer];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot({}, {metaReducers}),
    EffectsModule.forRoot([]),
    StoreModule.forFeature(ORDERSTATE, workflowReducer),
    StoreModule.forFeature(SELECTEDORDER, orderReducer),
    StoreModule.forFeature(LOGGEDINUSER, userReducer),
    StoreModule.forFeature(NOTIFICATIONS, notificationReducer),
    StoreModule.forFeature(SELECTEDPROFILE, profileReducer),
    StoreModule.forFeature(SELECTEDTEAM, teamReducer),
    StoreModule.forFeature(PHOTOORDERS, photoOrderReducer),
    StoreModule.forFeature(KEYS, keysReducer),
    StoreModule.forFeature(APP_OPTIONS, optionsReducer),
    EffectsModule.forFeature([WorkflowEffects, OrderEffects, OrderStateEffects,
      ProfileEffects, TeamEffects, PhotoOrderEffects, NotificationEffects, OptionsEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 10 }),
  ],
  declarations: []
})
export class StateModule { }
