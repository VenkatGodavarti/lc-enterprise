import { Deserializable } from './deserializable.model';
import { PhoneNumber } from './phone-number.model';
import { TeamMember } from './team-member.model';
import { TeamContactPreferences } from './team-contact-preferences.model';
import { PhotoInformation } from './photo-information.model';

export class TeamProfile implements Deserializable<TeamProfile> {
  _id: string;
  name: string;
  slogan: string;
  phone: PhoneNumber;
  email: string;
  websiteURL: string;
  ownerId: string;
  // teamPhotoURL: string;
  teamPhoto: PhotoInformation;
  teamLogoURL: string;
  members: TeamMember[];
  contactPreferences: TeamContactPreferences;

  constructor(input?: any) {
    this.deserialize(input || {});
  }

  deserialize(input: any): TeamProfile {
    Object.assign(this, input);
    return this;
  }
}
