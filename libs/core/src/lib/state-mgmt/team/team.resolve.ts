import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { TeamProfile } from '../../models/team-profile.model';
import { GetTeam } from './team.actions';

/**
 * The team resolve is used to simply request the store to get the pipe plumbed with the Team
 */
@Injectable({
  providedIn: 'root'
})
export class TeamResolve  implements Resolve<TeamProfile> {
  constructor(private store: Store<any>) {
  }

  resolve(route, state) {
    this.store.dispatch(new GetTeam());
    // we are no using resolve injection of objects so just return null
    return of(null);
  }

}
