import {Injectable} from "@angular/core";
import {Logger} from './logger';
import {ConsoleAppender} from "./console-appender";

/**
 * An injectable factory for components and services to create loggers
 */
@Injectable()
export class LoggerFactory {
  private configuration = {
    categories: {

      default: {
        level: 'WARN',
        appenders: [
          new ConsoleAppender()
        ]
      }
      /*
       example of configuration for the PhotoGridComponent logger
      'lc-photo-grid': {
        level: 'DEBUG',
        appenders: [
          new ConsoleAppender()
        ]
      }
      */
      //add your configuration here to set logging level for your own loggers
    }
  };


  constructor() {}

  public setConfiguration(override) {
    this.configuration.categories = Object.assign(this.configuration.categories, override);
  }

  /**
   * Return a Logger for the named category, if the category is not found in the
   * configuration the default configuration is used.
   *
   * At this time loggers are not shared, every time getLogger is called with the same
   * category argument a new Logger is created.
   *
   * @param category
   */
  public getLogger(category: string): Logger {

    let configCategory = category;

    if (!this.configuration.categories[configCategory]) {
      configCategory = 'default';
    }

    return new Logger(category, this.configuration.categories[configCategory]);
  }

}
