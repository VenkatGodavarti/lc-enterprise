import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lc-manage-media',
  templateUrl: './manage-media.component.html'
})
export class ManageMediaComponent {

  navLinks: any[];
  activeId: string;

  constructor(private activatedRoute: ActivatedRoute) {

    if (this.activatedRoute.snapshot.data && this.activatedRoute.snapshot.data['navLinks']) {
      this.navLinks = this.activatedRoute.snapshot.data['navLinks'];
      this.activeId = this.activatedRoute.snapshot.data['activeId'];
    }
  }


}
