import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ProductPreviewComponent } from './product-preview.component';
import { PrintPreviewComponent } from '../print-preview/print-preview.component';
import { Store } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../../core/src/lib/factories';
import { StoreMock, TestingModule } from '@lc/testing';
import { SELECTEDORDER } from '@lc/core';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';

describe('ProductPreviewComponent', () => {
  let component: ProductPreviewComponent;
  let fixture: ComponentFixture<ProductPreviewComponent>;
  let store: StoreMock<any>;
  const marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
  const packages = PackageInstanceFactory.createAll();
  marketingOrder.availablePackages = packages;
  marketingOrder.selectedPackageCode = packages[0].code;

  beforeEach(async(() => {
    store = new StoreMock<any>();
    store.setState(SELECTEDORDER, marketingOrder); // set default state

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule
      ],
      declarations: [ ProductPreviewComponent, PrintPreviewComponent ],
      providers: [
        {provide: Store, useValue: store},
        {provide: Router, useValue: {
          url: '/details'
          }},
        {
          provide: ActivatedRoute, useValue: {
            snapshot: {
              paramMap: convertToParamMap({ product_cod: '100' })
            }
          }
        }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  // beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
  //   store = mockStore;
  //   store.setState(SELECTEDORDER, marketingOrder); // set default state
  // }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ProductPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
