import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AddKeys, HeaderConstants } from '@lc/core';

@Component({
  selector: 'lc-agent-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lc-agent-app';

  constructor(store: Store<any>) {
    const initKeys = {};
    initKeys[HeaderConstants.APPNAME] = this.title;
    store.dispatch(new AddKeys(initKeys));
  }
 
}
