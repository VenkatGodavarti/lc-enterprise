
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const dropFile = require("../utils/drop-file.js");

When( /^I drag and drop the "(.*)" file onto the "(.*)" uploader$/, {timeout: config.timeout }, async (fileName, elementName) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const uploader = page.finder(elementName);

  const filePath = page.files[fileName];
  await dropFile(uploader, filePath);
});
