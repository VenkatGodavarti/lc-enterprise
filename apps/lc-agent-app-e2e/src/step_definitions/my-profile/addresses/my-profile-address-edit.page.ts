
import { LCPage, ElementMap, Element } from '../../../../../../e2e';

export class MyProfileAddressEditPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Form Header',       field: '#form-header'}),
      new Element( { name: 'Cancel',           field: 'button#address-cancel'}),
      new Element( { name: 'Save',             field: 'button#address-submit'})
    ]
  );

  static formElements = new ElementMap( 'Address',
  [

    new Element( { name: 'Full Name',        field: 'input#full-name-input'}),
    new Element( { name: 'Street Address 1', field: 'input#street-address-1-input'}),
    new Element( { name: 'Street Address 2', field: 'input#street-address-2-input'}),
    new Element( { name: 'City',             field: 'input#city-input'}),
    new Element( { name: 'State',            field: 'select#state-input'}),
    new Element( { name: 'Zip',              field: 'input#zip-input'}),
    new Element( { name: 'Company',          field: 'input#company-input'}),
    new Element( { name: 'Phone Number',     field: 'input#phone-input'}),
    new Element( { name: 'Email',            field: 'input#email-input'}),
  ]
  );

  constructor() {
    super('Profile Addresses', [MyProfileAddressEditPage.elements,MyProfileAddressEditPage.formElements]);
  }

}
