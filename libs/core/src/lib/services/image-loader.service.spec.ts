import { TestBed } from '@angular/core/testing';
import { ImageLoaderService } from './image-loader.service';

describe('ImageLoaderService', () => {
  let service: ImageLoaderService;
  const minFileSizeInMB = 0.3;
  const allowedExtensions = ["jpeg", "png", "jpg"];

  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => service = TestBed.get(ImageLoaderService));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should accept only valid extensions', () => {
    const goodSuffixes = allowedExtensions;
    goodSuffixes.forEach((s) => {
      let f = { name: 'name.' + s} as File;
      expect(service.validateFileExtension(f, allowedExtensions)).toBe(true);

      // Extra dots shouldn't matter
      f = { name: 'name.more-name.' + s} as File;
      expect(service.validateFileExtension(f, allowedExtensions)).toBe(true);
    });

    const badSuffixes = ['tiff', 'doc'];
    badSuffixes.forEach((s) => {
      const f = { name: 'name.more-name.' + s} as File;
      expect(service.validateFileExtension(f, allowedExtensions)).toBe(false);
    });

    const noSuffixFilename = { name: 'noSuffix' } as File;
    expect(service.validateFileExtension(noSuffixFilename, allowedExtensions)).toBe(false);
  });

  it('should enforce file size limits', () => {
    let f = { name: 'justRight.jpg', size: (1024 * 1024 * minFileSizeInMB)} as File;
    expect(service.validateFileSize(f, minFileSizeInMB)).toBeTruthy();

    f = { name: 'aLittleBigger.jpg', size: (1024 * 1024 * minFileSizeInMB) + 1 } as File;
    expect(service.validateFileSize(f, minFileSizeInMB)).toBeTruthy();

    f = { name: 'tooSmall.jpg', size: (1024 * 1024 * minFileSizeInMB) - 1 } as File;
    expect(service.validateFileSize(f, minFileSizeInMB)).toBeFalsy();
  });
});
