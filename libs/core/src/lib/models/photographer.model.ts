import { Deserializable } from './deserializable.model';

export class Photographer implements Deserializable<Photographer> {
  _id: string;
  name: string;
  logoUri: string;

  deserialize(input: any): Photographer {
    Object.assign(this, input);
    return this;
  }
}
