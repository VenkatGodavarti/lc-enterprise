import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MlsInfoFormComponent } from './mls-info-form.component';
import { PropertyInfoForm, Listing } from '@lc/core';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

describe('MlsInfoFormComponent', () => {
  let component: MlsInfoFormComponent;
  let fixture: ComponentFixture<MlsInfoFormComponent>;
  let listing: Listing;
  let form: PropertyInfoForm;

  beforeEach(async(() => {
    listing = {
      mlsId: 'MLS 123',
      globalLuxuryListing: true,
    } as Listing;
    form = new PropertyInfoForm(listing);

    TestBed.configureTestingModule({
      declarations: [ MlsInfoFormComponent ],
      imports: [FormsModule, ReactiveFormsModule, SharedComponentsUiModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlsInfoFormComponent);
    component = fixture.componentInstance;
    component.formGroup = form;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
