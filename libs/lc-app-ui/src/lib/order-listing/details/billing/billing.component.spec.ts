import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DetailStepsComponent } from '../detail-steps/detail-steps.component';
import { OrderSummaryComponent } from '../order-summary/order-summary.component';
import {
  ORDERSTATE,
  SELECTEDORDER,
  ProductInstance,
  OrderListingFlowStateService,
  Address,
  PhoneType,
  PhoneNumber,
  AddressType
} from '@lc/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AddressInformation,
  Listing,
  MarketingOrder,
  PackageInstance,
} from '@lc/core';
import { By } from '@angular/platform-browser';

import { BillingComponent } from './billing.component';
import { Store } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { StoreMock, TestingModule } from '@lc/testing';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

describe('BillingComponent', () => {
  let component: BillingComponent;
  let fixture: ComponentFixture<BillingComponent>;
  let store: StoreMock<any>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [
        DetailStepsComponent,
        OrderSummaryComponent,
        BillingComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    const flowState = new OrderListingFlowStateService().createNewFlowState();
    flowState.setStep('/order-listing/checkout/billing');
    store.setState(ORDERSTATE, flowState); // set default state
    store.setState(SELECTEDORDER, createMarketingOrder());
  }));

  // Create the component
  beforeEach(() => {
    fixture = TestBed.createComponent(BillingComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('initializes a new billing address', async(async() => {
    spyOn(component, 'patchForm');
    const element = fixture.debugElement.query(By.css('#populateShipping')).nativeElement;
    element.click();
    fixture.detectChanges();
    element.click();
    fixture.detectChanges();

    expect(component.patchForm).toHaveBeenCalled();
    expect(element.checked).toBeFalsy();
  }));

  it('populates billing address from the shipping address', async(async() => {
    const element = fixture.debugElement.query(By.css('#populateShipping')).nativeElement;

    // test if checkbox is not checked
    expect(element.checked).toBeFalsy();
    element.click();
    fixture.detectChanges();

    // test if checkbox is checked
    expect(element.checked).toBeTruthy();
  }));

  it('#onSubmit should save the order and navigate to a confirmation page', async(async() => {
    const dispatchMethod = spyOn(store, 'dispatch');
    const validAddress: AddressInformation = new AddressInformation({
      address: new Address({
        streetAddress1: '123 Easy Street',
        city: 'New York',
        state: 'NY',
        postalCode: '10001',
      }),
      phoneNumber: new PhoneNumber({
        number: '55555555555',
        type: PhoneType.direct,
      }),
      fullName: 'John Doe',
      email: 'unit-test@realogy.com',
      type: AddressType.BILLING
    });
    component.billingForm.patchValue(validAddress);
    component.onSubmit(createMarketingOrder());
    fixture.detectChanges();

    expect(dispatchMethod).toHaveBeenCalled();
  }));

  function createMarketingOrder() {
    const order = new MarketingOrder();
    order.listing =  new Listing();
    order.availablePackages = [ createPackage() ];
    order.selectedPackageCode = order.availablePackages[0].code;
    order.shippingInformation = createShippingInfo();
    // as the model for shipping and billing is same
    order.billingInformation = createShippingInfo();
    return order;
  }

  function createPackage() {
    const pkg = new PackageInstance();
    pkg.code = 'PKG_CODE';
    pkg.products.push(createProducts());
    return pkg;
  }

  function createProducts() {
    const product = new ProductInstance();
    product.title = 'Professional Photography';
    return product;
  }

  function createShippingInfo() {
    return new AddressInformation();
  }
});
