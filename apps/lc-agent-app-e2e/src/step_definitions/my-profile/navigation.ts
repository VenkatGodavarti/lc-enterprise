import { MyProfileAddressesPage } from './addresses/my-profile-addresses.page';
import { MyProfileAddressEditPage } from './addresses/my-profile-address-edit.page';
import { MyProfileMainPage } from './my-profile-main.page';
import { MyTeamProfileMainPage } from './team-profile-main.page';
import { MyTeamContactPreferencesPage } from './team-contact-preferences.page';
import { MyTeamCreatePage } from './team-create.page';

/***
 * Page mapping for the My Profile section of the app
 *
 */
export const pages = {

  'my profile': {

    'my profile': {
      url: '/my-profile/main',
      page: new MyProfileMainPage()
    },
    'main': {
      url: '/my-profile/main',
      page: new MyProfileMainPage('Main')
    },
    'team': {
      url: '/my-profile/team',
      page: new MyTeamProfileMainPage('Team')
    },
    'my team': {
      url: '/my-profile/team/edit',
      page: new MyTeamProfileMainPage()
    },
    'create team': {
      url: '/my-profile/team/create',
      page: new MyTeamCreatePage()
    },
    'team contact preferences': {
      url: '/my-profile/team/contacts',
      page: new MyTeamContactPreferencesPage()
    },

    'addresses': {
      url: '/my-profile/addresses',
      page: new MyProfileAddressesPage()
    },
    'shipping address': {
      url: '/my-profile/addresses/shipping',
      page: new MyProfileAddressEditPage()
    },

    'billing address': {
      url: '/my-profile/addresses/billing',
      page: new MyProfileAddressEditPage()
    },

    'mailing address': {
      url: '/my-profile/addresses/mailing',
      page: new MyProfileAddressEditPage()
    }

  }
};
