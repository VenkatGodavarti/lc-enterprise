import { environment } from '../../../../../environments/environment';
import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as OktaAuth from '@okta/okta-auth-js';
import { BaseAuthenticationProvider } from './base-authentication.provider';
import { UserService } from '../user.service';
import { User } from '../../models/user.model';
import { Store } from '@ngrx/store';

@Injectable()
export class OktaAuthenticationProvider implements BaseAuthenticationProvider, OnDestroy {

  oktaAuth = new OktaAuth({
    url: environment.oktaTenantUrl,
    clientId: environment.oktaClientId,
    issuer: environment.oktaIssuer,
    redirectUri: environment.oktaRedirectUri,
  });

  protected isAuthenticatedSubject = new BehaviorSubject<boolean>(false);

  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private userService: UserService,
    private store: Store<any>
  ) {
  }

  public isAuthenticated(): boolean {
    // Checks if there is a current accessToken in the TokenManger.
    return !!this.oktaAuth.tokenManager.get('accessToken') && this.isAuthenticatedSubject.value;
  }

  public handle401Response(): void {
    this.logout();
    this.redirectToInactivityLogout();
  }

  public redirectToInactivityLogout() {
    this.router.navigate(['inactivity-logout']);
  }

  public redirectToLogin() {
    this.router.navigate(['/okta/login']);
  }

  public login() {
    this.oktaAuth.token.getWithRedirect({
      responseType: ['id_token', 'token'],
      scopes: ['openid', 'email', 'profile']
    });
  }

  public async logout() {
    this.isAuthenticatedSubject.next(false);
    this.oktaAuth.tokenManager.clear();
    this.userService.clearCurrentUser();

    // logs the user out of their Okta session
    this.oktaAuth.signOut();
  }

  public getHeaderOptions(): any {
    return {
      Authorization: `Bearer ${this.getToken()}`
    };
  }

  public getToken(): string {
    const token = this.oktaAuth.tokenManager.get('accessToken');
    if (token) {
      return token.accessToken;
    }

    return null;
  }

  public async handleAuthentication() {
    let accessToken = null;
    let tokens;

    let fullAuth = false;

    if (!this.oktaAuth.tokenManager.get('accessToken')) {
      fullAuth = true;

      try {
        tokens = await this.oktaAuth.token.parseFromUrl();
      }
      catch (e) {

        try {
          tokens = await this.oktaAuth.token.parseFromUrl();
        }
        catch (e) {
          console.error(e.message);
          if (e.message.indexOf('not assigned') > -1 || e.message === 'Unable to parse a token from the url') {   // User is not assigned to the client application
            return Promise.reject(new Error('Not assigned the ' + environment.applicationName + ' application in Okta'));
          }

          return Promise.reject(new Error('No token provided.'));
        }
      }

      tokens.forEach(token => {
        if (token.idToken)
          this.oktaAuth.tokenManager.add('idToken', token);

        if (token.accessToken) {
          this.oktaAuth.tokenManager.add('accessToken', token);
          accessToken = token.accessToken;
        }
      });
    }

    await this.authenticateWithOktaToken(fullAuth);
  }

  private async authenticateWithOktaToken(fullAuth: boolean) {
    const headers = new HttpHeaders().set('Authorization', this.getHeaderOptions()['Authorization']);
    let response: any;
    try {
      response = await this.httpClient.get(environment.apiBaseURL + 'user/current-user/', { headers: headers }).toPromise();
    }
    catch (e) {
      return Promise.reject(new Error(e.error));
    }

    if (response) {
      // check for 401 error
      // render redirect to try again link
      // Error code (more: details) which indicates OktaID out of sync between desk and okta
      // try scenario where user does not exisat
      // backedn: if fail, attempt to sync user in okta (desk app assigned)
      // frontend: if here with 401 error, redirect pages/usernotfound with details okta account
      // not found or not assigned desk, 2nd level will attend to
      // todo

      const user = new User().deserialize(response);
      this.userService.setCurrentUser(user);
      this.isAuthenticatedSubject.next(true);

    }
  }

  ngOnDestroy() {
  }
}
