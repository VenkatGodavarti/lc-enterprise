import { Context } from './context';

/**
 * Defines the interface for classes used to set values on an individual property of a target object.
 */
export interface ModelProperty {

  name: string;

  set(order: any, value:any):void;
}

/**
 * Map a user-friendly property name to a function to set a property on a target object
 */
export class FunctionModelProperty implements ModelProperty {

  name: string;

  setter:(order: any, value: any)=>void;


  constructor(name: string, setter:(order: any, value: any)=>void) {
    this.name = name;
    this.setter = setter;
  }

  set(order: any, value:any):void {
    this.setter(order, value);
  }
}

/**
 * Map a user-friendly property name to a property path to set a property on the target
 * object directly
 */
export class KeyModelProperty implements ModelProperty {


  name: string;
  path: string;
  parts: string[];

  constructor(name: string, path: string) {
    this.name = name;
    this.path = path;
    this.parts = path.split('.');
  }


  set(model: any, value:any):void {

    let target = model;

    for (let i = 0; i < this.parts.length - 1; i++) {
      target = target[this.parts[i]];
      if (!target) {
        throw new Error(`Cannot evaluate property path: ${this.path} to set a value`);
      }
    }
    target[this.parts[this.parts.length-1]] = value;
  }
}

/**
 * A map of ModelProperty objects that can be used to set properties on a target object using user-friendly property
 * names. See ProfileProps or MarketingOrderProps for examples
 *
 */
export class ModelMap {

  name: string;

  readonly  _properties: ModelProperty[];

  private propertiesByName = {};

  constructor(name: string, properties: ModelProperty[]) {
    this.name = name;
    this._properties = properties
    for (let i = 0; i < properties.length; i++) {
      this.propertiesByName[properties[i].name] = properties[i];
    }
  }

  properties(): ModelProperty[] {
    return this._properties;
  }

  property(name: string): ModelProperty {
    return this.propertiesByName[name];
  }

  /**
   * Set a value of a property on the the target object
   *
   * @param target the target of the operation
   * @param propertyName the user-friendly name for the property
   * @param  the value to be set
   */
  set(target: any, propertyName: string, value: string) {

    value = parseValue(value);
    const prop = this.property(propertyName);
    if (prop) {
      prop.set(target, value);
    }
    else {
      target[propertyName] = value;
    }
  }

  /**
   * A convenience method to set all property values declared in a table for one object
   *
   * @param target
   * @param tableRecord
   * @param ignoredPropertyNames names of property keys in the tabelRecord that shoiuld be ignored.
   */
  setValues(target: any, tableRecord: any, ignoredPropertyNames: string[]) {

    Object.keys(tableRecord).forEach((key) => {

      if(ignoredPropertyNames.indexOf(key) === -1) {
        this.set(target, key, tableRecord[key]);
      }
    });

    return target;
  }
}

function parseValue(value): any {
  if(value && value.startsWith('{{') && value.endsWith('}}')) {
    // Replace {{}} values with values from the context, assuming they exist
    const prop = value.substr(2, value.length - 4);
    value = Context.get(prop);
  }
  return value;
}
