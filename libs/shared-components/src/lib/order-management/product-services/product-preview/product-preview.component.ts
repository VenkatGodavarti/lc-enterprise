import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {
  MarketingOrder,
  ProductInstance,
  SELECTEDORDER,
} from '@lc/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'lc-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent implements OnInit, OnDestroy {

  marketingOrder$: Observable<MarketingOrder>;
  marketingOrderSub: Subscription;

  product: ProductInstance;
  navLinks: any[];
  activeId: string;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private store: Store<any>) {

    this.marketingOrder$ = this.store.select(SELECTEDORDER);

    this.navLinks = [
      {
        label: 'Details',
        path: 'details',
        id: 'details',
        index: 0
      }, {
        label: 'Description',
        path: 'description',
        id: 'description',
        index: 1
      }, {
        label: 'Photos',
        path: 'photos',
        id: 'photos',
        index: 2
      },
    ];

    this.activeId = 'details';
  }

  ngOnInit() {
    this.marketingOrderSub = this.marketingOrder$.subscribe((marketingOrder) => {
      const productCode = this.activatedRoute.snapshot.paramMap.get('product_code');
      if(marketingOrder) {
        this.product = marketingOrder.getSelectedPackage().products.find((pr) => pr.code === productCode);
      }
    });
    // initial the active tab based on URL
    const navLink = this.navLinks.find(aNavLink => this.router.url.endsWith(aNavLink.path));
    this.activeId = navLink.id;
  }

  ngOnDestroy(): void {
    if (this.marketingOrderSub) {
      this.marketingOrderSub.unsubscribe();
    }
  }

  tabChange($event: NgbTabChangeEvent) {
    this.router.navigate([$event.nextId], {relativeTo: this.activatedRoute});
  }
}
