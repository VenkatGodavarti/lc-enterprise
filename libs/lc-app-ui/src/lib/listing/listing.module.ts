import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ListingAddressComponent } from './listing-address/listing-address.component';
import { AuthGuard } from '@lc/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ListingResolve } from './listing.resolve';
import { OrderFormsModule } from '@lc/shared-components-order-forms';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OrderFormsModule,
    SharedComponentsUiModule,
    RouterModule.forChild([
      {
        path: ':id/address',
        component: ListingAddressComponent,
        resolve: {
          listing: ListingResolve
        },
        children: []
      },
      { path: 'add-new-listing',
        component: ListingAddressComponent,
        resolve: {
          listing: ListingResolve
        },
        canActivate: [AuthGuard]
      }
    ])
  ],
  declarations: [
    ListingAddressComponent
  ],
  providers: [
    ListingAddressComponent,
    ListingResolve,
  ]
})
export class ListingModule {
}
