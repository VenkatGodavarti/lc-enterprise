// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const { join } = require('path');
const getBaseKarmaConfig = require('../../karma.conf');


module.exports = function(config) {
  const baseConfig = getBaseKarmaConfig();

  config.set({
    ...baseConfig,
    autoWatch: true,
    browsers: ['ChromeWithDevTools'],
    singleRun: false,
    reporters: ['kjhtml'],
    angularCli: {
      sourceMap: true,
    },
    coverageReporter: {
      ...baseConfig.coverageReporter,
      dir: join(__dirname, '../../reports/coverage/libs/lc-app-ui')
    },
    coverageIstanbulReporter: {
      ...baseConfig.coverageIstanbulReporter,
      dir: join(__dirname, '../../reports/coverage/libs/lc-app-ui')
    },
    remapCoverageReporter: {
      ...baseConfig.remapCoverageReporter,
      html: join(__dirname, '../../reports/coverage/libs/lc-app-ui/html'),
      cobertura: join(__dirname, '../../reports/coverage/libs/lc-app-ui/cobertura.xml')
    },
    junitReporter: {
      ...baseConfig.junitReporter,
      outputDir: join(__dirname, '../../reports/unit-tests/libs/lc-app-ui')
    }
  });
};
