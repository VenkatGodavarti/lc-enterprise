import { ElementMap, Element, LCPage } from '../../../../../../e2e';

export class CompletedOrdersPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Orders', field: '.table'}),
      new Element( { name: 'My Orders Tab', field: '#my-orders'}),
      new Element( { name: 'All Orders Tab', field: '#all-orders'}),
      new Element( { name: 'Completed Orders Tab', field: '#completed-orders'}),
    ]
  );

  constructor() {
    super('Completed Orders', [CompletedOrdersPage.elements]);
  }

}
