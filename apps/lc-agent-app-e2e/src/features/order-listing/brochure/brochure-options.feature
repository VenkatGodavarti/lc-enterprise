@order-listing
@brochure-options
@login-before-all

Feature: New Listing Brochure Options Page

  Background:
  # Login to the app and create the Alamo order
    Given I am logged in to Listing Concierge
    And the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Status               | OPEN                  |
      | Property Information | default               |
    And the order is on the Brochure step
    Then I select the Alamo order
    And I go to the Brochure Options page in Order Listing

  Scenario Outline: All page-size options are displayed
    Given the <paper-size> option is present
    Examples:
      | paper-size |
      | 8x8        |
      | 85x11      |
      | 11x17      |

  Scenario Outline: All paper-type options are displayed
    Given the <paper-type> option is present
    Examples:
      | paper-type  |
      | Glossy      |
      | Matte       |
      | Soft touch  |

  Scenario Outline: Select a paper-size
    Given the <paper-size> option is present
    When I click on the <paper-size> button
    Then the <paper-size> option is highlighted
    Examples:
      | paper-size |
      | 8x8        |
      | 85x11      |
      | 11x17      |
      | Other      |

  Scenario Outline: Select a paper-type
    Given the <paper-type> option is present
    When I click on the <paper-type> button
    Then the <paper-type> option is highlighted
    Examples:
      | paper-type  |
      | Glossy      |
      | Matte       |
      | Soft touch  |

  @only-one-page-size
  Scenario: Select only one paper-size
    When I click on the 11x17 button
    Then the 11x17 option is highlighted
    And  the 85x11 option is not highlighted
    When I click on the 85x11 button
    Then the 85x11 option is highlighted
    And  the 11x17 option is not highlighted

  @only-one-paper-type
  Scenario: Select only one paper-type
    When I click on the Glossy button
    Then the Glossy option is highlighted
    And  the Matte option is not highlighted
    When I click on the Matte button
    Then the Matte option is highlighted
    And  the Glossy option is not highlighted

  @must-select-paper-size-and-type
  Scenario: Click next with no selection
    When I click on the Next button
    Then the "Paper Size Error" contains "required"
    Then the "Paper Type Error" contains "required"

  @must-select-paper-size-and-type
  Scenario: Click next with no page-size selection
    When I click on the 11x17 button
    And I click on the Next button
    Then the "Paper Size Error" text is empty
    Then the "Paper Type Error" contains "required"

  @must-select-paper-size-and-type
  Scenario: Click next with no paper-type selection
    When I click on the Glossy button
    And I click on the Next button
    Then the "Paper Size Error" contains "required"
    Then the "Paper Type Error" text is empty

  @must-select-paper-size-and-type
  Scenario: Click next with paper-size and paper-type selected
    When I click on the 11x17 button
    Then I click on the Glossy button
    And I click on the Next button
    Then the Overview Checkout page from Order Listing is loaded

  @must-select-paper-size-and-type
  Scenario: Click Back with paper-size and paper-type selected and come back
    When I click on the 11x17 button
    Then I click on the Glossy button
    And I click on the Back button
    Then the Overview Package Options page from Order Listing is loaded
    When I click on the Package Options Edit button
    Then the Brochure Options page from Order Listing is loaded
    And the 11x17 option is not highlighted
    And the Glossy option is not highlighted

  @must-select-paper-size-and-type
  Scenario: Click Next with paper-size and paper-type selected and come back
    When I click on the 11x17 button
    Then I click on the Glossy button
    And I click on the Next button
    Then the Overview Checkout page from Order Listing is loaded
    When I click on the Package Options Edit button
    Then the Brochure Options page from Order Listing is loaded
    And the 11x17 option is highlighted
    And the Glossy option is highlighted
