import { Component, OnInit } from '@angular/core';
import { Profile, SELECTEDPROFILE, AddressInformation, CardContactInfo, UpdateProfile, UIConstants } from '@lc/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ContactType, AddressType } from '@lc/core';
import { map } from 'rxjs/operators';
import { PromptDialogService, DialogAction } from '@lc/shared-components-ui';

/**
 * This is the display model for this component. The display model handles any of the logic required to display a certain object to the UI
 * so that the component doesnt need to be responsible for handling the business logic and can just display the information.
 */
export class MyProfileAddressesViewModel {
  contactInfo: CardContactInfo;
  info: AddressInformation;
  description1: string;
  description2: string;

  constructor(public readonly type: AddressType, public readonly profile: Profile) {
    this.contactInfo = profile.contactInfo.find( ci => ci.type === ContactType.card && ci.address.type === type);
    this.info = this.contactInfo ? this.contactInfo.address : null;

    switch(type){
      case AddressType.SHIPPING:
        this.description1 = 'This shipping address is used as the default address for any marketing materials that are shipped to you.'
        break;
      case AddressType.BILLING:
        this.description1 = 'The billing address is used as the default payment address.';
        this.description2 = 'Note: Listing Concierge does not store your credit card information.';
        break;
      case AddressType.MAILING:
        this.description1 = 'The mailing address is used as the default address to mail you examples of any mailed '+
        'materials (i.e. just listed postcards).'
    }
  }
}

@Component({
  selector: 'lc-app-my-profile-addresses',
  templateUrl: './my-profile-addresses.component.html',
  styleUrls: ['./my-profile-addresses.component.scss']
})
export class MyProfileAddressesComponent implements OnInit {

  public contacts$: Observable<MyProfileAddressesViewModel[]>;

  constructor(private store: Store<any>,
              private dialogService: PromptDialogService) {
    this.contacts$ = store.select<Profile>(SELECTEDPROFILE).pipe(
      map(profile => this.buildContacts(profile))
    );
  }

  ngOnInit() {
  }

  onDelete(display: MyProfileAddressesViewModel) {
    const onYes = new DialogAction(UIConstants.YES, () => this.delete(display));
    const onNo = new DialogAction(UIConstants.NO);
    this.dialogService.openPrompt(UIConstants.CONFIRM, 'Are you sure you want to delete?', onYes, [onNo]);
  }

  private delete(display: MyProfileAddressesViewModel) {
    const index = display.profile.contactInfo.indexOf(display.contactInfo);
    if (index !== -1) {
      display.profile.contactInfo.splice(index, 1);
    }
    this.store.dispatch(new UpdateProfile(display.profile, ['contactInfo']));
  }

  private buildContacts(profile : Profile): MyProfileAddressesViewModel[] {
    return [
      new MyProfileAddressesViewModel(AddressType.SHIPPING, profile),
      new MyProfileAddressesViewModel(AddressType.BILLING, profile),
      new MyProfileAddressesViewModel(AddressType.MAILING, profile),
    ]
  }
}
