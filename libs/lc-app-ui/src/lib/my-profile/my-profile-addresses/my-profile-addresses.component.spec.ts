import { TestBed } from '@angular/core/testing';
import { MyProfileAddressesViewModel } from './my-profile-addresses.component';
import { AddressType } from '@lc/core';
import { ProfileFactory } from '../../../../../core/src/lib/factories';

/**
 * Unit tests against the ViewModel for the MyProfileAddresses Component
 */
describe('MyProfileAddressesViewModel', () => {

  beforeEach(() => TestBed.configureTestingModule({}));

  it('selects the proper address for each address type', () => {
    const profile = ProfileFactory.johnDoe;
    const shipping = new MyProfileAddressesViewModel(AddressType.SHIPPING, profile);
    const billing = new MyProfileAddressesViewModel(AddressType.BILLING, profile);
    const mailing = new MyProfileAddressesViewModel(AddressType.MAILING, profile);

    expect(shipping.info).toBeTruthy();
    expect(shipping.info.type).toBe(AddressType.SHIPPING);
    expect(billing.info).toBeTruthy();
    expect(billing.info.type).toBe(AddressType.BILLING);
    expect(mailing.info).toBeTruthy();
    expect(mailing.info.type).toBe(AddressType.MAILING);
  });

  it('handles non-existing addresses appropriately', () => {
    const profile = ProfileFactory.johnDoe;
    profile.contactInfo = [];
    const shipping = new MyProfileAddressesViewModel(AddressType.SHIPPING, profile);
    const billing = new MyProfileAddressesViewModel(AddressType.BILLING, profile);
    const mailing = new MyProfileAddressesViewModel(AddressType.MAILING, profile);

    expect(shipping.info).toBeFalsy();
    expect(billing.info).toBeFalsy();
    expect(mailing.info).toBeFalsy();
  });

});

