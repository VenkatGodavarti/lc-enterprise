import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
const angularModules = [ CommonModule, ReactiveFormsModule ];

import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { MyProfileRoutingModule } from './my-profile-routing.module';
const internalModules = [ SharedComponentsUiModule, MyProfileRoutingModule ];

import { ProfileComponent } from './profile/profile.component';
import { MyProfileComponent } from './my-profile.component';
const internalComponents = [ ProfileComponent, MyProfileComponent ];

@NgModule({
  declarations: [...internalComponents],
  imports: [
    ...angularModules,
    ...internalModules
  ]
})
export class MyProfileModule { }
