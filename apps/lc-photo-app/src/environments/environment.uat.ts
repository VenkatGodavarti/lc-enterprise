export const environment = {
  // App configuration
  production: false,
  envName: 'uat',
  applicationName: 'Listing Concierge Photo App UAT',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://lc.uat.lc-photo.realogy.com/',
  apiBaseURL: 'https://lc.uat.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.uat.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.uat.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajm2ys56Igwa2LQ0h7',
  oktaRedirectUri: 'https://lc.uat.lc-photo.realogy.com/implicit/callback',
};
