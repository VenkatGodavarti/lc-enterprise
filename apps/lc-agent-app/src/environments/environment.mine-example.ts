export const environment = {
  // App configuration
  production: false,
  envName: 'mine',
  applicationName: 'Listing Concierge',
  applicationKey: 'lc-agent-app',

  // API/UI Endpoints
  baseURL: 'http://localhost:4200/',
  apiBaseURL: 'http://localhost:4100/api/v1/',
  photosBaseURL: 'http://localhost:4300/api/v1/',
  notifyBaseURL: 'http://localhost:4800/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oaglhfi71bLgkLoe0h7',
  oktaRedirectUri: 'http://localhost:4200/implicit/callback',
};
