import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesDialogComponent } from './notes-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedComponentsUiModule } from '../../shared-components-ui.module';
import { NotesService } from '@lc/core';

describe('NotesDialogComponent', () => {
  let component: NotesDialogComponent;
  let fixture: ComponentFixture<NotesDialogComponent>;

  let mockedNotesService: jasmine.SpyObj<NotesService>;
  beforeEach(async(() => {
    mockedNotesService = jasmine.createSpyObj<NotesService>(['getNotesFor', 'create']);
    TestBed.configureTestingModule({
      imports: [ NgbModule, SharedComponentsUiModule ],
      providers: [ {provide: NotesService, useValue: mockedNotesService} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
