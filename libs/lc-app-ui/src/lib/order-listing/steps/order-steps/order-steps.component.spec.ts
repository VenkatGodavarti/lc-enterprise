import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderStepsComponent } from './order-steps.component';
import { RouterTestingModule } from '@angular/router/testing';
import { OrderStepTemplateComponent } from './order-step-template/order-step-template.component';
import { DetailStepsComponent } from '../../details/detail-steps/detail-steps.component';
import { DetailStepTemplateComponent } from '../../details/detail-steps/detail-step-template/detail-step-template.component';
import { Store, StoreModule } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FlowState } from '@lc/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';

describe('OrderStepsComponent', () => {
  let component: OrderStepsComponent;
  let fixture: ComponentFixture<OrderStepsComponent>;

  let activatedRoute: ActivatedRoute;

  beforeEach((() => {
    activatedRoute = new ActivatedRoute();
    activatedRoute.data = of({});
    spyOnProperty(activatedRoute, 'parent', 'get').and.returnValue(activatedRoute);
    spyOnProperty(activatedRoute, 'paramMap', 'get').and.returnValue(of(convertToParamMap({})));

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({}, {})
      ],
      declarations: [
        OrderStepsComponent,
        OrderStepTemplateComponent,
        DetailStepsComponent,
        DetailStepTemplateComponent
      ],
      providers: [
        Store,
        { provide: ActivatedRoute, useValue: activatedRoute },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStepsComponent);
    component = fixture.componentInstance;
    const currentStep = {id: 'x'};
    component.orderState = new FlowState({steps: [currentStep], currentStep: currentStep });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
