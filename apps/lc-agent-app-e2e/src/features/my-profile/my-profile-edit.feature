@my-profile
@login-before-all
Feature: I want to edit my profile

  Background:
    Given I am logged in to Listing Concierge

  @empty-profile
  Scenario: Edit profile data and cancel
    When I go to the My Profile page in My Profile
    Then the "Form Header" text is "My Profile"
    And  the "Update Profile" button is disabled
    When I enter data into the Profile form
      | First Name   | Marcus                      |
      | Last Name    | Aurelius                    |
      | Email        | marcus.aurelius@realogy.com |
      | Website      | http://www.realogy.com      |
    Then the "Update Profile" button is enabled
    When I click on the Cancel button
    Then the Profile form has been reset

  @empty-profile
  Scenario: Edit profile data and save reveals errors
    When I go to the My Profile page in My Profile
    Then the "Form Header" text is "My Profile"
    And  the "Update Profile" button is disabled
    When I enter data into the Profile form
      | First Name           | Marcus                      |
      | Last Name            | Aurelius                    |
      | Email                | marcus.aurelius@realogy.com |
      | Website              | http://www.realogy.com      |
      | Primary Phone Number | 4155551212                  |
    Then the "Update Profile" button is enabled
    When I click on the Update Profile button
    Then the "Primary Phone Type" has an error containing "required"

  @empty-profile
  Scenario: Edit profile data and save
    When I go to the My Profile page in My Profile
    Then the "Form Header" text is "My Profile"
    And  the "Update Profile" button is disabled
    When I enter data into the Profile form
      | First Name             | Marcus                      |
      | Last Name              | Aurelius                    |
      | Email                  | marcus.aurelius@realogy.com |
      | Website                | http://www.realogy.com      |
      | Primary Phone Number   | 4155551212                  |
      | Primary Phone Type     | Mobile                      |
      | Secondary Phone Number | 4155551234                  |
      | Secondary Phone Type   | Direct                      |
    Then the "Update Profile" button is enabled
    When I click on the Update Profile button
    And  the "Update Profile" button is disabled
    And  my profile has been updated

  @desktop @mobile @tablet @empty-profile
  Scenario: Replace Profile Photo
    When I go to the My Profile page in My Profile
    And I click on the Edit Profile Photo icon
    And I wait for the Change File element to load
    And I replace the Profile Avatar on the page
      | Profile | Image | profile-image.jpg |
    And I click on the Apply Profile Image button
    Then the Profile Avatar image has been updated on the page

  @empty-profile
  Scenario: Add Agent Logo
    When I go to the My Profile page in My Profile
    And I upload a "Logo" image
      | Profile | Logo | agent-logo.png |
    And I wait for the Agent Logo element to load
    Then the Agent Logo image is present
    And the Edit Logo button is present

  @empty-team-profile
  Scenario: Add Team Photo
    Given the coldwell team profile exists
    When I go to the My Team page in My Profile
    And I upload a "Photo" image
      | Team Profile | Photo | team-photo.jpg |
    And I wait for the Team Photo element to load
    Then the Team Photo image is present
    And the Edit Photo button is present
    And the Delete Photo button is present

    # reload the page to make sure the logo is still present
    When I go to the My Profile page in My Profile
    And I go to the My Team page in My Profile
    And I wait for the Team Photo element to load
    Then the Team Photo image is present

  @empty-team-profile
  Scenario: Delete Team Photo
    Given the coldwell team profile exists
    When I go to the My Team page in My Profile
    And I upload a "Photo" image
      | Team Profile | Photo | team-photo.jpg |
    And I wait for the Team Photo element to load
    And I click on the Delete Photo button
    Then the Upload Photo button is present

  @empty-team-profile
  Scenario: Add Team Logo
    Given the coldwell team profile exists
    When I go to the My Team page in My Profile
    And I upload a "Logo" image
      | Team Profile | Logo | agent-logo.png |
    And I wait for the Team Logo element to load
    Then the Team Logo image is present
    And the Edit Logo button is present
    And the Delete Logo button is present

    # reload the page to make sure the logo is still present
    When I go to the My Profile page in My Profile
    And I go to the My Team page in My Profile
    And I wait for the Team Logo element to load
    Then the Team Logo image is present

  @empty-team-profile
  Scenario: Delete Team Logo
    Given the coldwell team profile exists
    When I go to the My Team page in My Profile
    And I upload a "Logo" image
      | Team Profile | Logo | agent-logo.png |
    And I wait for the Team Logo element to load
    And I click on the Delete Logo button
    Then the Upload Logo button is present

    # reload the page to make sure the logo is absent
    When I go to the My Profile page in My Profile
    And I go to the My Team page in My Profile
    Then the Upload Logo button is present

  @empty-team-profile
  Scenario: upload a non conventional file format
    Given the coldwell team profile exists
    When I go to the My Team page in My Profile
    And I upload a "Logo" image
      | Team Profile | Logo | invalid-file-format.pdf |
    Then I see the Upload Failed dialog

# This scenario is not valid yet but will be once we have a real agent lookup web service
#  Scenario: view team logo by other members
#    Given I am logged in as "Lisa" as an "Agent"
#    And I am part of "awesome team" as a team "member"
#    And a team owner already uploaded our "team logo"
#    When I am in my my team profile page
#    Then I should see our "team logo"
#    And I should not see an edit pencil
#
