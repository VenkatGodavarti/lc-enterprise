import { Component, OnInit, ViewChild } from '@angular/core';
import { USStates, STATES_BY_ABBREVIATION, MarketingOrder, AddressInformationForm, UserService, SubmitOrderComplete, OrderActionTypes, SELECTEDORDER, PhoneType, AddressType, AddressInformation, UpdateOrder, WorkflowAction, ActionTypes, ServiceStatusType, SubmitOrder } from '@lc/core';
import { Masks } from '@lc/shared-components-ui';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionsSubject, Store } from '@ngrx/store';
import { ofType } from '@ngrx/effects';
import { filter, tap } from 'rxjs/operators';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ElementsOptions, StripeCardComponent, ElementOptions, StripeService } from 'ngx-stripe';

import { environment } from '../../../../../../environments/environment';


declare var stripe:any

@Component({
  selector: 'lc-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  @ViewChild(StripeCardComponent) card: StripeCardComponent;
 
  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '80px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };
 
  elementsOptions: ElementsOptions = {
    locale: 'en'
  };
 
  stripeTest: FormGroup;

  /*    */

  readonly states: USStates[] = STATES_BY_ABBREVIATION;
  readonly phoneMask = Masks.phoneNumber;
  currentOrder$: Observable<MarketingOrder>;

  /**
   * Making exception here because we need to initialize it for UI to work
   */
  currentOrder: MarketingOrder;

  // billing address form
  billingForm: AddressInformationForm;
  params: any;

  subjectSub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
              private router: Router,
              private actionSubject: ActionsSubject,
              private userService: UserService,
              private store: Store<any>,
              private fb: FormBuilder,
              private stripeService: StripeService

  ) { 

     // wait for an action change that matches complete
     this.subjectSub = this.actionSubject.pipe(
      ofType<SubmitOrderComplete>(OrderActionTypes.SubmitOrderComplete)
    ).subscribe((data: SubmitOrderComplete) => {
      // created listing or updated with no package selected
      const pageRoute = '/manage-order/' + data.payload._id + '/confirmation/';
     this.router.navigate([pageRoute]);
    });

    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  ngOnInit() {

   
    this.stripeService.setKey(environment.stripePublishKey);

    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    /*    */

    this.billingForm = new AddressInformationForm();
    this.billingForm.get('fullName').setValidators([Validators.required]);

    // initialize the form if the order is not null. The subscription is on html page
    this.currentOrder$ = this.store.select(SELECTEDORDER).pipe(
      filter(order => order),
      tap(order => this.patchForm(order.billingInformation))
    );

    // populate address type if it's not already
    if(!this.billingForm.get('type').value) {
      this.billingForm.controls['type'].setValue(AddressType.BILLING);
    }

    // populate required phoneNumber fields if necessary
    const phoneNumber = this.billingForm.get('phoneNumber') as FormGroup;
    if(!phoneNumber.get('type').value) {
      phoneNumber.get('type').setValue(PhoneType.direct);
    }



  }
  getFormControls(controls,key){
    let controlsValue = controls.get(key).value ? controls.get(key).value : ''
    return controlsValue
}
  buy(order) {
    const getCardname = this.stripeTest.get('name').value;
    const getAddress = this.billingForm.get('address')
    const options = {
      name: getCardname,
      address_line1:  this.getFormControls(getAddress,'streetAddress1'),
      address_line2:  this.getFormControls(getAddress,'streetAddress2'),
      address_city:  this.getFormControls(getAddress,'city'),
      address_state:  this.getFormControls(getAddress,'state'),
      address_zip:   this.getFormControls(getAddress,'postalCode'),
      address_country: "USA",
    };
    console.log('options==>',options)
    this.stripeService
      .createToken(this.card.getCard(), options)
      .subscribe(result => {
        if (result.token) {
          // Use the token to create a charge or a customer
          // https://stripe.com/docs/charges
          console.log(result.token.id);
          console.log(order);
          this. OrderComplete(order);

         // this.onSubmit(order);
        // this.store.dispatch(new SubmitOrder(order));
         //this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, {stepCompleted: true}, this.params));

        } else if (result.error) {
          // Error creating the token
          console.log(result.error.message);
        }
      });
  }

  ngOnDestroy(): void {
    if (this.subjectSub) {
      this.subjectSub.unsubscribe();
    }
  }

  patchForm(addressInfo) {
    if (addressInfo) {
      this.billingForm.patchValue(addressInfo);
      this.billingForm.get('type').setValue(AddressType.BILLING);
    } else {
      this.billingForm.patchValue(this.getBillingContactInfo());
    }
  }

  getShippingDetails(event, order) {
    if (event.target.checked && order) {
      this.patchForm(order.shippingInformation);
    } else {
      this.patchForm(null);
    }
    event.stopPropagation();
  }

  getBillingContactInfo() {
    let address = this.userService.getAddress(AddressType.BILLING);
    if (!address) {
      address = new AddressInformation();
    }
    return address;
  }

  goBack(order) {
    // dispatch and save the data state - Just a hack for LC-604
    if(this.billingForm.valid) {
      order.billingInformation = this.billingForm.value;
      this.store.dispatch(new UpdateOrder(order));
    }
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(order: MarketingOrder) {
    this.buy(order);
  }

  OrderComplete(order){
    this.billingForm.markAllAsDirty();
    if(!this.billingForm.valid) { return; }

    // remove any keys with null values before populating the model
    const formData = this.billingForm.value;
    Object.keys(formData).forEach((key) => (formData[key] == null) && delete formData[key]);
    Object.keys(formData.phoneNumber).forEach((key) => (formData.phoneNumber[key] == null) && delete formData.phoneNumber[key]);
    order.billingInformation = formData;

    // This is the last step of order entry, so we are complete
                  //order.submitted = true;

    // submit order with or without a photoorder
    const product = order.getSelectedPackage().getPhotographyProduct();
    if(product) {
      // TODO - we need to set the state of the product instance in the API on submit order api
      product.status = ServiceStatusType.PENDING;
      //TODO from LC-416: replace currentUser with store.select(LOGGEDINUSER)
      order.photoOrder = order.createPhotoOrder(this.userService.getCurrentUser());
    }
    // this.store.dispatch(new UpdateOrder(order));
    // // next step
    // this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, {stepCompleted: true}, this.params));
 
    this.store.dispatch(new SubmitOrder(order));
  }

}
