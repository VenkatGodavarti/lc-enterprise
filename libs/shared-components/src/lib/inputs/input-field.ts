import { FormControl, ControlValueAccessor, Validator, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

export abstract class InputField implements ControlValueAccessor, Validator {
  public static inputCount: number = 0;

  public errors = '';
  public hasErrors: boolean;
  protected subscription: any;
  public formControl: FormControl = new FormControl();

  public _value: any = null;

  @Input()
  idPrefix = `field-${InputField.inputCount++}`;

  @Input()
  public get value() { return this._value; }

  public set value(value) {
    if (this._value !== value) {
      this._value = value;
      this.executeOnChanged();
      this.executeOnTouched();
    }
  }

  @Input()
  public disabled: boolean;

  readonly errorsChanged: EventEmitter<string> = new EventEmitter<string>();

  public onChange = (_: any) => {};
  public onTouch = (_: any) => {};
  public onValidatorChange = (_: any) => {
    console.log('validator changed', _);
  };

  constructor(private sanitizer: DomSanitizer) { }


  public writeValue(value: any): void {
    this.value = value;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  public registerOnValidatorChange(fn: () => void) {
    this.onValidatorChange = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  protected executeOnTouched() {
    this.onTouch(this.value);

    this.setErrors(this.formControl);
  }

  protected executeOnChanged() {
    this.onChange(this.value);
  }

  public validate(formControl: FormControl) {
    this.initializeForm(formControl);
    return null;
  }

  protected initializeForm(formControl: FormControl) {
    if (!this.subscription) {
      this.formControl = formControl;
      this.subscription = formControl.valueChanges.subscribe(() => {
        this.setErrors(formControl);
        this.value = formControl.value;
      });
      formControl.statusChanges.subscribe(() => this.setErrors(formControl) );
      this.setErrors(formControl);
    }
  }

  protected setErrors(formControl: FormControl) {
    this.errors = this.getDisplayErrors(formControl);
    this.hasErrors = this.errors !== '';
  }

  protected getDisplayErrors(formControl: FormControl) {
    if (!formControl) { return ''; }

    if (formControl.dirty && formControl.hasError('required')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field is <strong>required</strong>');
    }
    if (formControl.touched && formControl.hasError('email')) {
     return this.sanitizer.bypassSecurityTrustHtml('This field must be a valid <strong>email</strong>');
    }
    if (formControl.hasError('pattern')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field is not <strong>valid</strong>');
     }
     if (formControl.hasError('whitespace')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field cannot contain any <strong>spaces</strong>');
     }
     if (formControl.hasError('maxlength')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field exceeds the <strong>maximum</strong> length');
     }
     if (formControl.dirty && formControl.hasError('regex')) {
      return formControl.errors.regex;
     }
     if (formControl.dirty && formControl.hasError('requireHttps')) {
      return this.sanitizer.bypassSecurityTrustHtml('URL Must start with <strong>https://</strong>');
     }
     if (formControl.dirty && formControl.hasError('url')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field must be a proper <strong>URL</strong>');
     }
      if (formControl.dirty && formControl.hasError('phone')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field must be a proper <strong>phone number</strong>');
     }
     if (formControl.dirty && formControl.hasError('wholeNumber')) {
      return this.sanitizer.bypassSecurityTrustHtml('This field only accepts <strong>whole numbers</strong>');
     }

     return '';
  }
}
