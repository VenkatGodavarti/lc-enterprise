import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard, CoreModule, UserService, TeamResolve } from '@lc/core';
import { TagInputModule } from 'ngx-chips';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { SharedComponentsMediaModule } from '@lc/shared-components-media';
import { NgbModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { MyProfileTeamComponent } from './my-profile-team/my-profile-team.component';
import { MyProfileAddressesComponent } from './my-profile-addresses/my-profile-addresses.component';
import { MyProfileNotificationsComponent } from './my-profile-notifications/my-profile-notifications.component';
import { MyProfileMainComponent } from './my-profile-main/my-profile-main.component';
import { MyProfileComponent } from './my-profile.component';
import { MyProfileNavigationComponent } from './my-profile-navigation/my-profile-navigation.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MyProfileAddressEditorComponent } from './my-profile-addresses/my-profile-address-editor/my-profile-address-editor.component';
import { AddressInformationFormComponent } from './my-profile-addresses/address-information-form/address-information-form.component';
import { TeamDisplayComponent } from './my-profile-team/team-display/team-display.component';
import { TeamCreateComponent } from './my-profile-team/team-create/team-create.component';
import { TeamEditComponent } from './my-profile-team/team-edit/team-edit.component';
import { TeamContactsComponent } from './my-profile-team/team-contacts/team-contacts.component';
import { NgxMaskModule } from 'ngx-mask';
import { DragulaModule } from 'ng2-dragula';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    NgxMaskModule.forRoot(),
    CommonModule,
    DragulaModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    ImageCropperModule,
    CoreModule,
    SharedComponentsUiModule,
    SharedComponentsMediaModule,
    ComponentsModule,
    NgbModule,
    NgbTabsetModule,
    RouterModule.forChild([
      {
        // by placing components under this parent component there will be a left side nav bar
        path: '',
        component: MyProfileComponent,
        children: [
          { path: '', redirectTo: 'main', pathMatch: 'full' },
          { path: 'main', component: MyProfileMainComponent, pathMatch: 'full', canActivate: [AuthGuard] },
          { path: 'team', component: MyProfileTeamComponent, canActivate: [AuthGuard], resolve: { team: TeamResolve } },
          { path: 'team/edit', component: TeamEditComponent, canActivate: [AuthGuard], resolve: { team: TeamResolve } },
          { path: 'team/create', component: TeamCreateComponent, canActivate: [AuthGuard], resolve: { team: TeamResolve } },
          { path: 'team/contacts', component: TeamContactsComponent, canActivate: [AuthGuard], resolve: { team: TeamResolve } },
          { path: 'addresses', component: MyProfileAddressesComponent, canActivate: [AuthGuard] },
          { path: 'addresses/:addressType', component: MyProfileAddressEditorComponent, canActivate: [AuthGuard] },
          { path: 'notifications', component: MyProfileNotificationsComponent, canActivate: [AuthGuard] }
        ]
      }
    ])
  ],
  providers: [
    UserService,
  ],
  declarations: [
    MyProfileComponent,
    MyProfileMainComponent,
    MyProfileTeamComponent,
    MyProfileAddressesComponent,
    MyProfileNotificationsComponent,
    MyProfileNavigationComponent,
    MyProfileAddressEditorComponent,
    AddressInformationFormComponent,
    TeamDisplayComponent,
    TeamCreateComponent,
    TeamEditComponent,
    TeamContactsComponent
  ]
})
export class MyProfileModule {
}
