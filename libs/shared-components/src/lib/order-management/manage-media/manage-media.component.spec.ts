import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMediaComponent } from './manage-media.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestingModule } from '@lc/testing';
import { ActivatedRoute } from '@angular/router';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { TabSetComponent } from '../../../../../shared-components/src/lib/tabset/tab-set.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ManageMediaComponent', () => {
  let component: ManageMediaComponent;
  let fixture: ComponentFixture<ManageMediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule,
        SharedComponentsUiModule,
      ],
      declarations: [ ManageMediaComponent ],
      providers: [
        TabSetComponent,
        { provide: ActivatedRoute, useValue: {
            snapshot: {
              data: {
                navLinks: [],
                activeId: null
              }
            }
          }
        },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
