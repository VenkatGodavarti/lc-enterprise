const { exec } = require('child_process');

let failedTestRunCount = 0;
let closedCount = 0;

function handleClose(code, signal){
  if (code !== 0) {
    failedTestRunCount++;
  }
  closedCount++;
}

function handleDisconnectAndError(err) {
  if (err){
    console.error(err);
  }
  failedTestRunCount++;
  closedCount++;
}

function handleChildCallback(error, stdout, stderr) {
  if (error) {
    console.error(`exec error: ${error}`);
    return;
  }
  console.log(stdout);
  console.log(stderr);
}

function checkForCompletion(expected) {
  if (closedCount >= expected) {
    if (failedTestRunCount > 0){
      console.error(`${failedTestRunCount} test runs failed.`)
    } else {
      console.log(`All ${closedCount} test runs passed.`)
    }
    process.exit(failedTestRunCount);
  } else {
    setTimeout(checkForCompletion.bind(this), 1000, expected);
  }
}
const { projects } = require('./angular');

const projectNames = Object.keys(projects);

const testRuns = [];

function runTests() {
  projectNames.forEach((n) => {
    if (projects[n]['architect'] && projects[n]['architect']['test']) {
      const childProcess = exec(`ng test --project ${n}`, handleChildCallback);
      childProcess.on('close', handleClose);
      childProcess.on('disconnect', handleDisconnectAndError);
      childProcess.on('error', handleDisconnectAndError);
      testRuns.push({ name: n, childProcess: childProcess });
    }
  });
  setTimeout(checkForCompletion.bind(this), 1000, testRuns.length);
}

runTests();
