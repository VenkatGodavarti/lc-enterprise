import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderManagementSummaryComponent } from './order-management/order-management-summary/order-management-summary.component';
import { OrderManagementNavigationComponent } from './order-management/order-management-navigation/order-management-navigation.component';
import { AuthGuard, CoreModule } from '@lc/core';

import { RouterModule } from '@angular/router';
import { OrderManagementComponent } from './order-management/order-management.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderFormsModule } from './order-forms/order-forms.module';
import { MarketingOrderResolve } from './order-management/marketing-order.resolve';
import { MarketingCopyComponent } from './order-management/property-description/marketing-copy.component';
import { SelectProductComponent } from './order-management/sections/select-product/select-product.component';
import { NotificationsComponent } from './order-management/sections/notifications/notifications.component';
import { PhotosComponent } from './order-management/sections/photos/photos.component';
import { ListingComponent } from './order-management/sections/listing/listing.component';
import { DescriptionComponent } from './order-management/sections/description/description.component';
import { ProductServicesComponent } from './order-management/product-services/product-services.component';
import { ListingDetailsComponent } from './order-management/listing-details/listing-details.component';
import { CoordinatorComponent } from './order-management/sections/coordinator/coordinator.component';
import { ProductNotImplementedComponent } from './order-management/product-services/product-not-implemented/product-not-implemented.component';
import { ProductGridComponent } from './order-management/product-services/product-grid/product-grid.component';
import { ProfessionalPhotographyComponent } from './order-management/professional-photography/professional-photography.component';
import { ConfirmationComponent } from './order-management/confirmation/confirmation.component';
import { TemplateGridComponent } from './order-management/product-services/template-grid/template-grid.component';
import { ProductPreviewComponent } from './order-management/product-services/product-preview/product-preview.component';
import { PrintPreviewComponent } from './order-management/product-services/print-preview/print-preview.component';
import { WebsiteComponent} from './order-management/product-services/website/website.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { ManagePhotosComponent } from './order-management/manage-media/manage-photos/manage-photos.component';
import { SharedComponentsMediaModule } from '@lc/shared-components-media';
import { ProductPreviewWrapperComponent } from './order-management/product-services/product-preview-wrapper/product-preview-wrapper.component';
import { ManageMultimediaComponent } from './order-management/manage-media/manage-multimedia/manage-multimedia.component';
import { ManageMediaComponent } from './order-management/manage-media/manage-media.component';
import { ProductDesignsComponent } from './order-management/product-services/product-designs/product-designs.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    SharedComponentsUiModule,
    SharedComponentsMediaModule,
    NgbTabsetModule,
    OrderFormsModule,
    RouterModule.forChild([
      {
        // by placing components under this parent component there will be a left side nav bar
        path: '',
        component: OrderManagementComponent,
        resolve: {
          marketingOrder: MarketingOrderResolve
        },
        children: [
          { path: '', redirectTo: 'summary', pathMatch: 'full' },
          { path: 'summary', component: OrderManagementSummaryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
          { path: 'listing-details', component: ListingDetailsComponent, canActivate: [AuthGuard] },
          { path: 'property-description', component: MarketingCopyComponent, canActivate: [AuthGuard] },
          { path: 'products-services',
            children: [
              { path: '',   component: ProductServicesComponent, canActivate: [AuthGuard] },
              { path: 'professional-photography/:product_code', component: ProfessionalPhotographyComponent, canActivate: [AuthGuard]},
              { path: ':product_code/preview', component: ProductPreviewComponent, canActivate: [AuthGuard],
                children: [
                  { path: '', redirectTo: 'description', pathMatch: 'full' },
                  { path: 'details', component: ListingDetailsComponent, canActivate: [AuthGuard] },
                  { path: 'description', component: MarketingCopyComponent, canActivate: [AuthGuard] },
                  { path: 'photos', component: ManagePhotosComponent, canActivate: [AuthGuard] },
                ]},
              { path: 'flyer/:product_code', component: ProductDesignsComponent, canActivate: [AuthGuard] },
              { path: 'flyer/:product_code/preview', redirectTo: ':product_code/preview', canActivate: [AuthGuard], pathMatch: 'full' },
              { path: 'website/:product_code', redirectTo: ':product_code/preview', pathMatch: 'full' },
              { path: 'brochure/:product_code', component: ProductDesignsComponent, canActivate: [AuthGuard] },
              { path: 'brochure/:product_code/preview', redirectTo: ':product_code/preview', canActivate: [AuthGuard], pathMatch: 'full' },
              { path: 'just-listed-postcard/:product_code', component: ProductDesignsComponent, canActivate: [AuthGuard] },
              { path: 'just-listed-postcard/:product_code/preview', redirectTo: ':product_code/preview', canActivate: [AuthGuard], pathMatch: 'full' },
              { path: '**', component: ProductNotImplementedComponent, canActivate: [AuthGuard]}
            ]
           },
          { path: 'media', component: ManageMediaComponent, canActivate: [AuthGuard],
            data: {navLinks: [
                {
                  label: 'Photos',
                  path: 'photos',
                  id: 'photos',
                  index: 0
                }, {
                  label: 'Multimedia',
                  path: 'multimedia',
                  id: 'multimedia',
                  index: 1
                },
              ],
              activeId: 'photos'},
            children: [
              { path: '', redirectTo: 'photos', pathMatch: 'full' },
              { path: 'photos', component: ManagePhotosComponent, canActivate: [AuthGuard] },
              { path: 'multimedia', component: ManageMultimediaComponent, canActivate: [AuthGuard] },
            ],
          },
        ]
      },
      {
        path: 'confirmation',
        component: ConfirmationComponent,
         resolve: {
          marketingOrder: MarketingOrderResolve,
        },
      },
    ])
  ],
  providers: [
    MarketingOrderResolve,
  ],
  declarations: [
    ConfirmationComponent,
    OrderManagementSummaryComponent,
    OrderManagementNavigationComponent,
    OrderManagementComponent,
    MarketingCopyComponent,
    SelectProductComponent,
    NotificationsComponent,
    PhotosComponent,
    ListingComponent,
    DescriptionComponent,
    ProductServicesComponent,
    ListingDetailsComponent,
    CoordinatorComponent,
    ProductNotImplementedComponent,
    ProductGridComponent,
    ProfessionalPhotographyComponent,
    TemplateGridComponent,
    ProductPreviewComponent,
    ProductPreviewWrapperComponent,
    PrintPreviewComponent,
    ProductDesignsComponent,
    ManagePhotosComponent,
    WebsiteComponent,
    ManageMultimediaComponent,
    ManageMediaComponent
  ]
})
export class OrderManagementModule { }
