import { MarketingOrder, ProductInstance } from '../models';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebsiteGenerationService } from './website-generation.service';
import { PdfGenerationService } from './pdf-generation.service';

@Injectable()
export class ProductGenerationService {

  private mimeTypeGenerators = {};


  constructor(private websiteGenerationService: WebsiteGenerationService,
              private pdfGenerationService: PdfGenerationService) {

    this.mimeTypeGenerators[websiteGenerationService.mimeType] = websiteGenerationService;
    this.mimeTypeGenerators[pdfGenerationService.mimeType] =pdfGenerationService;
  }

  isDownloadable(product: ProductInstance): boolean {

    const mimeType = product.selectedTemplate.mimeType;

    return this.mimeTypeGenerators[mimeType].downloadable;
  }

  download(order: MarketingOrder, product: ProductInstance): Observable<any> {

    const mimeType = product.selectedTemplate.mimeType;

    return this.isDownloadable(product) ? this.mimeTypeGenerators[mimeType].download(order, product): of(null);
  }

  generate(order: MarketingOrder, product: ProductInstance): Observable<any> {

    const mimeType = product.selectedTemplate.mimeType;

    return this.mimeTypeGenerators[mimeType].generate(order, product);

  }
}
