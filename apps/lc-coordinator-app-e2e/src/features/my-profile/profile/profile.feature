@login-before-all
Feature: I want to edit my profile

  Background:
    Given I am logged in to Listing Concierge

  @empty-profile
  Scenario: Edit profile data and cancel
    When I go to the My Profile page in My Profile
    Then the "Header" text is "My Profile"
    And  the "Update Profile" button is enabled
    When I enter data into the Profile form
      | First Name   | Marcus                      |
      | Last Name    | Aurelius                    |
      | Email        | marcus.aurelius@realogy.com |
      | Phone Number | 5551231234                  |
    Then the "Update Profile" button is enabled
    When I click on the Cancel button
    Then the Profile form has been reset

  @empty-profile
  Scenario: Edit profile data and save
    When I go to the My Profile page in My Profile
    Then the "Header" text is "My Profile"
    And  the "Update Profile" button is enabled
    When I enter data into the Profile form
      | First Name   | Marcus                      |
      | Last Name    | Aurelius                    |
      | Email        | marcus.aurelius@realogy.com |
      | Phone Number | 5551231234                  |
    Then the "Update Profile" button is enabled
    When I click on the Update Profile button
    And  the "Update Profile" button is enabled
    And  my profile has been updated
