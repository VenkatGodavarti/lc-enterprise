import { Component, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { InputField } from '../input-field';
import { DomSanitizer } from '@angular/platform-browser';
import { FileSystemFileEntry, UploadEvent } from 'ngx-file-drop';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'lc-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent extends InputField {

  @Input()
  inputId: string; // the id of the upload input field

  @Input()
  btnId: string; // the id of the upload button

  @Input()
  label: string; // text to display as a component label

  @Input()
  instructions: string; // text to display for the component

  @Input()
  allowedExtensions: any[];  // File validation constraints

  @Input()
  processing: boolean;  // determines if the component should accept files

  @Output()
  uploadResult = new EventEmitter<any>();

  constructor(sanitizer: DomSanitizer) {
    super(sanitizer);
  }

  // called for drag-n-drop
  async handleDroppedFiles(event: UploadEvent) {
    if (!this.processing) {
      this.uploadFiles(await this.getFilesFromUploadEvent(event));
    }
  }

  // called for file chooser input
  handleSelectedFiles(event) {
    this.uploadFiles(event.target.files);
  }

  async getFilesFromUploadEvent(event: UploadEvent) {
    const filePromises = [];
    event.files.forEach(droppedFile => {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        const filePromise = new Promise((resolve) => {
          fileEntry.file(resolve);
        });
        filePromises.push(filePromise);
      }
    });
    return Promise.all(filePromises);
  }

  uploadFiles(files: File[]) {
    this.uploadResult.emit(files);
  }

  public chooseFiles() {
    const element: HTMLElement = document.getElementById(this.inputId || 'uploadPhotos') as HTMLElement;
    element.click();
  }
}
