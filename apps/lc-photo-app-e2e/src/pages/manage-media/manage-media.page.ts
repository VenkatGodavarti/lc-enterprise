import { ElementMap, Element, LCPage } from '../../../../../e2e';
import { PhotosPage } from './photos.page';


export class ManageMediaPage extends LCPage {
  static elements = new ElementMap( 'page', []
  );

  constructor() {
    super('Manage Media', [ManageMediaPage.elements]);
  }
}


export const manageMediaPages = {
  'manage media': {
    'photos' : {
      url: '/manage-media/:photographerId/:photoOrderId/photos',
      page: new PhotosPage()

    },
  },
};
