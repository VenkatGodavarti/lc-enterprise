import { UriValidators } from '@lc/core';
import { FormControl } from '@angular/forms';

describe('UriValidators', () => {

  describe('validateSecureUri', () => {

    it('uri is valid', () => {

      const result = UriValidators.validateSecureUri(new FormControl('https://www.cnn.com'));
      expect(result).toBeFalsy();
    });


    it('uri is  invalid (http)', () => {

      const result = UriValidators.validateSecureUri(new FormControl('http://www.cnn.com' ));
      expect(result).toBeTruthy();
      expect(result.url).toBeTruthy();
    });

    it('uri is  invalid (simple text)', () => {

      const result = UriValidators.validateSecureUri(new FormControl('this is not a  uri' ));
      expect(result).toBeTruthy();
      expect(result.url).toBeTruthy();
    });
  });

});
