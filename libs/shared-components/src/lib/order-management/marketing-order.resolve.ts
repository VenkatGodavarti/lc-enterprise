import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GetOrder, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';

@Injectable()
export class MarketingOrderResolve implements Resolve<MarketingOrder> {

  readonly marketingOrder$: Observable<MarketingOrder>;

  constructor(private store: Store<any>) {
    this.marketingOrder$ = this.store.select<MarketingOrder>(SELECTEDORDER);
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const marketingOrderId = route.params['id'];
    // TODO - throw error to be handled by global error handler
    if (!marketingOrderId) {
      return of(null);
    }

    this.store.dispatch(new GetOrder(new MarketingOrder({_id: marketingOrderId})));
    return of(null);
  }
}
