import { Element, ElementMap, LCPage } from '../../../../../e2e/support';

export class MyTeamProfileMainPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Cancel',         field: 'button#team-profile-cancel'}),
      new Element( { name: 'Update Profile', field: 'button#team-profile-save'}),
      new Element( { name: 'Team Photo',     field: 'img#team-photo'}),
      new Element( { name: 'Team Logo',      field: 'img#team-logo'}),
      new Element( { name: 'Upload Photo',   field: 'button#teamPhotoSubmit'}),
      new Element( { name: 'Upload Logo',    field: 'button#teamLogoSubmit'}),
      new Element( { name: 'Edit Photo',     field: 'span#edit-image-button'}),
      new Element( { name: 'Edit Logo',      field: 'span#edit-logo-button'}),
      new Element( { name: 'Delete Photo',   field: 'span#delete-image-button'}),
      new Element( { name: 'Delete Logo',    field: 'span#delete-logo-button'}),
      new Element( { name: 'Add Members',    field: 'span#add-members-btn'}),
      new Element( { name: 'Create a new Team', field: '#create-team'})
    ]
  );

  static formElements = new ElementMap( 'Team Profile',
    [
      new Element( { name: 'Team Name', model: 'name',         field: 'input#teamName-input'}),
      new Element( { name: 'Phone',     model: 'number',       field: 'input#phone-input'}),
      new Element( { name: 'Website',   model: 'websiteURL',   field: 'input#website-input'}),
      new Element( { name: 'Email',     model: 'email',        field: 'input#email-input'}),
      new Element( { name: 'Slogan',    model: 'slogan',       field: 'textarea#slogan-input'}),
      new Element( { name: 'Photo',     model: 'teamPhotoURL', field: 'input#teamPhotoInput'}),
      new Element( { name: 'Logo',      model: 'teamLogoURL',  field: 'input#teamLogoInput'})
    ]
  );

  constructor(pageName: string = 'My Team') {
    super(pageName, [MyTeamProfileMainPage.elements, MyTeamProfileMainPage.formElements]);
  }

}
