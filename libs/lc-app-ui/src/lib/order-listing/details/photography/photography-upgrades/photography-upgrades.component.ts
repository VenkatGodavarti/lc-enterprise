import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  ActionTypes,
  WorkflowAction,
  SELECTEDORDER, MarketingOrder, UpdateOrder, ProductInstance, ProductOption
} from '@lc/core';

import { BehaviorSubject, Observable } from 'rxjs';
import {  Store } from '@ngrx/store';
import { filter, tap } from 'rxjs/operators';
import { CardOption } from '@lc/shared-components-ui';

@Component({
  selector: 'lc-app-photography-upgrades',
  templateUrl: './photography-upgrades.component.html',
  styleUrls: ['./photography-upgrades.component.scss']
})
export class PhotographyUpgradesComponent implements OnInit {

  marketingOrder$: Observable<MarketingOrder>;

  // Local copy of options that tracks user selections
  availableOptions: CardOption<ProductOption>[] = [];
  selectedOptions: CardOption<ProductOption>[] = [];

  maxOptionsSelectable;

  params: ParamMap;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {
    this.marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => order && order.listing != null),
      tap(order => this.setPhotographyOptions(order))
    );
    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  setPhotographyOptions(marketingOrder: MarketingOrder) {

    const photographyProduct: ProductInstance = marketingOrder.getPhotographyProduct();

    if (photographyProduct && photographyProduct.options) {
      // Get a local copy of available options and note the selection limit
      this.maxOptionsSelectable = photographyProduct.maxOptionsSelectable;
      const newOptions = photographyProduct.options.map(o => {
        const option = new CardOption(o, o.title, o.selected);
        if(o.assets) {
          option.imageSource = o.assets.unselected;
          option.selectedImageSource = o.assets.selected;
        }
        return option;
      });
      this.availableOptions = newOptions;
    }
  }

  ngOnInit() {
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(marketingOrder: MarketingOrder) {

    const photographyProduct: ProductInstance = marketingOrder.getPhotographyProduct();
    if (photographyProduct && photographyProduct.options) {
      // Update the marketingOrder with the user selections
      this.selectedOptions.forEach(userSelection => {
        photographyProduct.options.forEach(option => {
          if (option.description === userSelection.model.description) {
            option.selected = userSelection.isSelected;
          }
        });
      });

      this.store.dispatch(new UpdateOrder(marketingOrder));

      // TODO - this needs to be moved waiting for completed action
      this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true },
        this.params));
    }
  }

  updateSelection(options: CardOption<ProductOption>[]) {
    const canSelect = options.length < this.maxOptionsSelectable;
    this.availableOptions.filter(x=>!x.isSelected).forEach(option => option.isEnabled = canSelect);
    this.selectedOptions = options;
  }
}
