import { Before, After } from 'cucumber';
import { Context, ContextKeys, MarketingOrdersClient, Configuration as config  } from '../../../../../e2e';

After({tags: "@cleanup-order", timeout: config.timeout}, async () => {
  const orderId = Context.get(ContextKeys.orderId);
  await MarketingOrdersClient.delete(orderId);
});
