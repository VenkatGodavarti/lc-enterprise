
export enum ContactType {
  card = 'card',
  address = 'address',
  phone = 'phone',
  email = 'email'

}
