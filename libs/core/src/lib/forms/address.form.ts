import { FormControl, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { STATES_BY_ABBREVIATION } from '../models';

export class AddressForm extends BaseForm {
  states = STATES_BY_ABBREVIATION;
  constructor(address?: any) {
    super({
      streetAddress1: new FormControl(null, [Validators.required]),
      streetAddress2: new FormControl(null),
      unitNumber: new FormControl(null),
      city: new FormControl(null, [Validators.required]),
      state: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z]{2}$')]),
      postalCode: new FormControl(null, [Validators.required, Validators.pattern('^\\d{5}$')])
    });

    if(address){
      this.patchValue(address);
    }
  }
}
