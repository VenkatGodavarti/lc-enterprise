import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  ActionTypes,
  WorkflowAction,
  Photographer,
  PhotographerAppointment,
  PhotographerService, SELECTEDORDER, MarketingOrder, UpdateOrder, PhotographerForm
} from '@lc/core';

import { Observable } from 'rxjs';
import {  Store } from '@ngrx/store';
import { filter, map, tap } from 'rxjs/operators';
import { CardOption } from '@lc/shared-components-ui';

/**
 * the SelectPhotographerComponent is responsible for displaying professional
 * photography options and allowing the user to choose exactly one from the
 * list.  The output is stored in
 * the Listing.PhotographerAppointment.Photographer object.
 */
@Component({
  selector: 'lc-app-select-photographer',
  templateUrl: './select-photographer.component.html',
  styleUrls: ['./select-photographer.component.scss']
})
export class SelectPhotographerComponent implements OnInit {
  form: PhotographerForm;

  marketingOrder$: Observable<MarketingOrder>;
  cardOptions: Observable<CardOption<Photographer>[]>;

  params: ParamMap;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>,
              private photographerService: PhotographerService) {
    this.marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => order && order.listing != null),
      tap(order => this.initializeForm(order))
    );
    this.params = this.activatedRoute.parent.snapshot.paramMap;

    // retrieve all the available photographers from the server
    this.cardOptions = this.photographerService.retrievePhotographers().pipe(
      map(photographers => photographers.map(photographer => this.toDisplayOption(photographer)))
    );
  }

  ngOnInit() {
  }

  private initializeForm(marketingOrder: MarketingOrder) {
    const product = marketingOrder.getPhotographyProduct();
    const appointment = product ? product.serviceAppointment : undefined;
    this.form = new PhotographerForm(appointment ? appointment.photographer : undefined);
  }

  onSelected(options: CardOption<Photographer>[]) {
    const selected = options.length > 0 ? options[0].model : new Photographer();
    this.form.patchValue(selected);
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(marketingOrder: MarketingOrder) {
    // don't allow if user hasn't chosen a photographer
    this.form.markAllAsDirty();
    if(!this.form.valid) {
      return;
    }

    // create new PhotographyAppointment if one doesn't yet exist
    const photographyProduct = marketingOrder.getPhotographyProduct();
    if(!photographyProduct.serviceAppointment) {
      photographyProduct.serviceAppointment = new PhotographerAppointment();
    }

    photographyProduct.serviceAppointment.photographer = this.form.value;
    this.store.dispatch(new UpdateOrder(marketingOrder));

    // TODO - this needs to be moved waiting for completed action
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true },
      this.params));
  }

    /**
   * Converts the Options to a CardOption to be used in the display
   * @param option The photograhper to convert
   */
  private toDisplayOption(photographer: Photographer): CardOption<Photographer> {
    const cardOption = new CardOption(photographer, null, false, photographer.logoUri, photographer.logoUri);
    return cardOption;
  }
}
