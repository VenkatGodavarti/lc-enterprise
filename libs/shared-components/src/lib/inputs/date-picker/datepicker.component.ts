import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputField } from '../input-field';
import { Mask } from '../input-masks';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbCustomDateFormatter } from './ngb-custom-date-formatter';

@Component({
  selector: 'lc-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    // Use the appropriate Date adapter to convert form Date object
    {provide: NgbDateAdapter, useClass: NgbDateNativeAdapter},
    // Use the CustomDateFormatter to display date in MM/DD/YYYY format
    {provide: NgbDateParserFormatter, useClass: NgbCustomDateFormatter},
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true,
    }
  ]
})
export class DatepickerComponent extends InputField {
  @Input()
  label: string;

  @Input()
  placeholder = '';

  @Input()
  inputMask: Mask;

  @Input()
  hint: string;

  @Output()
  public readonly blur = new EventEmitter<any>();

  constructor(sanitizer: DomSanitizer) {
    super(sanitizer);
  }
}
