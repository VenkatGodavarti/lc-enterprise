import { Component, OnInit } from '@angular/core';
import { TeamProfile, SELECTEDTEAM } from '@lc/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'lc-my-profile-team',
  templateUrl: './my-profile-team.component.html',
  styleUrls: ['./my-profile-team.component.scss']
})
export class MyProfileTeamComponent implements OnInit {
  public readonly team$: Observable<TeamProfile>;

  constructor(store: Store<any>) {
    this.team$ = store.pipe(select<TeamProfile>(SELECTEDTEAM));
  }

  ngOnInit() {
  }
}
