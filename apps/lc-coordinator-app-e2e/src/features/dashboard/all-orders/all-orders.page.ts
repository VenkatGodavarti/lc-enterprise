import { ElementMap, Element, LCPage } from '../../../../../../e2e';

export class AllOrdersPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Orders', field: '.table'}),
      new Element( { name: 'My Orders Tab', field: '#my-orders'}),
      new Element( { name: 'All Orders Tab', field: '#all-orders'}),
      new Element( { name: 'Completed Orders Tab', field: '#completed-orders'}),

      new Element( { name: 'Package Header', field: '#package-header'}),
      new Element( { name: 'Assign to Me', field: '.assign-to-me'}),

      new Element( { name: 'View Notes', field: '.add-note'}),
      new Element( { name: 'Notes', field: '#notes-dialog'}),
      new Element( { name: 'Notes Title', field: '#notes-title'}),
      new Element( { name: 'Notes Subtitle', field: '#notes-subtitle'}),
      new Element( { name: 'Note Text', field: '.note-text'}),
      new Element( { name: 'Note Author', field: '.note-author'}),
      new Element( { name: 'Note', field: '.note'}),
      new Element( { name: 'Add a Note', field: '#note-input'}),

      new Element( { name: 'Prompt', field: '#prompt-dialog'}),
    ]
  );

  static dialogElement = new ElementMap( 'prompt dialog',
    [
      new Element( { name: 'Yes', field: '#yes-button'}),
      new Element( { name: 'No', field: '#no-button'}),
    ]
  );

  static notesDialogElement = new ElementMap( 'notes dialog',
    [
      new Element( { name: 'Save', field: '#save-notes-button'}),
      new Element( { name: 'Cancel', field: '#cancel-notes-button'}),
    ]
  );
  constructor() {
    super('All Orders', [AllOrdersPage.elements, AllOrdersPage.dialogElement, AllOrdersPage.notesDialogElement]);
  }

}
