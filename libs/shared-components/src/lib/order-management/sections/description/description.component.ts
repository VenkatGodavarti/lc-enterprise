import { Component, Input, OnInit } from '@angular/core';
import { Listing, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'lc-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  @Input() marketingOrder: MarketingOrder;

  marketingOrder$: Observable<MarketingOrder>;

  constructor(private store: Store<any>) {
    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));
  }

  ngOnInit(){
  }

}
