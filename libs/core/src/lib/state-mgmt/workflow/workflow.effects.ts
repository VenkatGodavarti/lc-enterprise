import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActionTypes, ORDERSTATE } from './workflow.actions';
import { tap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FlowState } from '../../models/flow-state.model';

@Injectable()
export class WorkflowEffects {

  constructor(private actions$: Actions,
              private router: Router,
              private store: Store<FlowState>) {

  }

  @Effect({ dispatch: false })
  navigate: Observable<any> = this.actions$.pipe(
    ofType(ActionTypes.WorkflowActionNext, ActionTypes.WorkflowActionPrev,
      ActionTypes.WorkflowActionNextIncomplete, ActionTypes.WorkflowActionEdit),
    withLatestFrom(this.store, (action, store) => {
      return {
        action: action,
        orderState: store[ORDERSTATE]
      };
    }),
    tap(payload => {
      let url = payload.orderState.currentStep.routerLinkUri;
      payload.action.params.keys.forEach(key => {
        url = url.replace(':'+key, payload.action.params.get(key));
      });
      this.router.navigate([url]);
    })
  );

  /**
   * This effect is used when the initialization of a state needs to navigate to a URL
   * on the flow state model. It is used to jump back to the last place in a workflow
   * on initialization
   */
  @Effect({ dispatch: false })
  initialize: Observable<any> = this.actions$.pipe(
    ofType(ActionTypes.WorkflowActionInitialize),
    withLatestFrom(this.store, (action, store) => {
      return {
        action: action,
        orderState: store[ORDERSTATE]
      };
    }),
    tap(payload => {
      if (payload.orderState.currentStep) {
        let url = payload.orderState.currentStep.routerLinkUri;
        payload.action.params.keys.forEach(key => {
          url = url.replace(':'+key, payload.action.params.get(key));
        });
        this.router.navigate([url]);
      }
    })
  );
}
