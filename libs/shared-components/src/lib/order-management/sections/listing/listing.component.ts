import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Listing, ListingPhoto, ThumbnailSizes, MarketingOrder } from '@lc/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'lc-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  readonly defaultImage = 'assets/images/no-photo2.png';

  @Input()
  marketingOrder: MarketingOrder;

  get listing(): Listing { return this.marketingOrder ? this.marketingOrder.listing : null; }

  backgroundImage = this.defaultImage;

  get address() { return this.listing ? this.listing.address : null };

  constructor(
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    if(!this.marketingOrder) { return; }
    this.backgroundImage = this.getBackgroundImage(this.marketingOrder.photos);
  }

  manageListingDetails() {
    this.router.navigate(['../listing-details'], { relativeTo: this.route});
  }

  // set backround image as 1st listing photo on listing card or default no-photo.png
  getBackgroundImage(photos : ListingPhoto[]) {
    if (photos && photos[0] && photos[0].thumbnails && photos[0].thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH]) {
      return photos[0].thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH].uri;
    }
    return this.defaultImage;
  }
}
