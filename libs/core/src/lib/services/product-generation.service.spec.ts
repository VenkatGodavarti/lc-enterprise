import {getTestBed, TestBed} from '@angular/core/testing';
import {
  OutputMimeType,
  ProductInstance,
  PdfGenerationService,
  WebsiteGenerationService,
  MarketingOrderService,
  ProductGenerationService
} from '@lc/core';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { MarketingOrderFactory } from '../factories';
import { Observable } from 'rxjs';

describe('ProductGenerationService', () => {

  let generated: boolean
  let downloaded: boolean;

  let marketingOrder;
  let product;
  let httpClient;
  const websiteGenerationService = {
    downloadable: false,
    mimeType: OutputMimeType.html,

    generate: (o, p) => {
      generated = true;
    },

    download: (o, p) => {
      downloaded = true;
    }
  };

  const pdfGenerationService = {
    downloadable: true,
    mimeType: OutputMimeType.pdf,

    generate: (o, p) => {
      generated = true;
    },

    download: (o, p) => {
      downloaded = true;
    }
  };

  let productGenerationService;

  let injector: TestBed;

  beforeEach(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    product = new ProductInstance({
      title: 'Flyer',
      kind: 'product',
      category: 'print',
      selectedTemplate: {
        mimeType: OutputMimeType.html
      }
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ProductGenerationService,
        { provide: PdfGenerationService, useValue: pdfGenerationService },
        { provide: WebsiteGenerationService, useValue: websiteGenerationService }
      ]
    });

    injector = getTestBed();
    httpClient = injector.get(HttpTestingController);
    productGenerationService = TestBed.get(ProductGenerationService);

    generated = false;
    downloaded = false;

  });

  describe('PDF ', () => {


    it('isDownloadable()', () => {

      expect(productGenerationService.isDownloadable({ selectedTemplate: { mimeType: OutputMimeType.pdf }})).toBeTruthy();
    });

    it('generate()', () => {

      productGenerationService.generate(marketingOrder, { selectedTemplate: { mimeType: OutputMimeType.pdf }});
      expect(generated).toBeTruthy();
    });

    it('download()', () => {

      productGenerationService.download(marketingOrder, { selectedTemplate: { mimeType: OutputMimeType.pdf }});
      expect(downloaded).toBeTruthy();
    });

  });


  describe('Website ', () => {


    it('isDownloadable()', () => {

      expect(productGenerationService.isDownloadable({ selectedTemplate: { mimeType: OutputMimeType.html }})).toBeFalsy();
    });

    it('generate()', () => {

      productGenerationService.generate(marketingOrder, { selectedTemplate: { mimeType: OutputMimeType.html }});
      expect(generated).toBeTruthy();
    });

    it('download()', () => {

      productGenerationService.download(marketingOrder, { selectedTemplate: { mimeType: OutputMimeType.html }});
      expect(downloaded).toBeFalsy();
    });

  });
});

