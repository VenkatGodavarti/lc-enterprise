import { http } from './http';
import { Configuration as config } from '../configuration';

/**
 *
 * Invoke testing endpoints to crerate and delete user and profiel records for e2e tests
 *
 * Note; the API calls will fail if the lcms-perssis service is not properly configured
 * to include the endpoints called by this class.
 */
export class UsersClient {
  static async post(user: any) {
    return http.post(`${config.apiBaseURL}e2e/users`, user);
  }

  static async delete(userId: string) {
    return http.delete(`${config.apiBaseURL}e2e/users/${userId}`);
  }

}
