@my-profile
@my-profile-addresses
@login-before-all
Feature: I want to add addresses to my profile

  Background:
    Given I am logged in to Listing Concierge
    And I have no profile addresses

  Scenario: Navigate to the addresses page
    When I go to the Addresses page in My Profile
    Then the Add Shipping Address button is present
    And the Delete Shipping Address icon is not present
    And the Edit Shipping Address icon is not present
    Then the Add Billing Address button is present
    And the Delete Billing Address icon is not present
    And the Edit Billing Address icon is not present
    Then the Add Mailing Address button is present
    And the Delete Mailing Address icon is not present
    And the Edit Mailing Address icon is not present

  Scenario: Add a shipping address then cancel
    When I go to the Addresses page in My Profile
    Then the Add Shipping Address button is present
    When I click on the Add Shipping Address button
    Then the Shipping Address page from My Profile is displayed
    And the "Form Header" text is "Shipping Address"
    And  the "Save" button is disabled
    When I enter data into the Address form
      | Full Name        | Marcus Aurelius |
      | Street Address 1 | 1234 1st Street |
      | City             | Santa Ana       |
      | State            | CA              |
      | Zip              | 90210           |
      | Phone Number     | 9495551212      |
    Then the "Save" button is enabled
    When I click on the Cancel button
    Then the Addresses page from My Profile is displayed
    Then the Add Shipping Address button is present
    And the Delete Shipping Address icon is not present
    And the Edit Shipping Address icon is not present

  Scenario: Add a shipping address then save
    When I go to the Addresses page in My Profile
    Then the Add Shipping Address button is present
    When I click on the Add Shipping Address button
    Then the Shipping Address page from My Profile is displayed
    And the "Form Header" text is "Shipping Address"
    And  the "Save" button is disabled
    When I enter data into the Address form
      | Full Name        | Marcus Shipping |
      | Street Address 1 | 1234 1st Street |
      | City             | Santa Ana       |
      | State            | CA              |
      | Zip              | 90210           |
      | Phone Number     | 9495551212      |
    Then the "Save" button is enabled
    When I click on the Save button
    Then the Addresses page from My Profile is displayed
    Then the Add Shipping Address button is not present
    And the Delete Shipping Address icon is present
    And the Edit Shipping Address icon is present
    And the Shipping Address contains the last entered Full Name

  Scenario: Add a billing address then save
    When I go to the Addresses page in My Profile
    Then the Add Billing Address button is present
    When I click on the Add Billing Address button
    Then the Billing Address page from My Profile is displayed
    And the "Form Header" text is "Billing Address"
    And  the "Save" button is disabled
    When I enter data into the Address form
      | Full Name        | Marcus Billing |
      | Street Address 1 | 1234 1st Street |
      | City             | Santa Ana       |
      | State            | CA              |
      | Zip              | 90210           |
      | Phone Number     | 9495551212      |
    Then the "Save" button is enabled
    When I click on the Save button
    Then the Addresses page from My Profile is displayed
    Then the Add Billing Address button is not present
    And the Delete Billing Address icon is present
    And the Edit Billing Address icon is present
    And the Billing Address contains the last entered Full Name


  Scenario: Add a mailing address then save
    When I go to the Addresses page in My Profile
    Then the Add Mailing Address button is present
    When I click on the Add Mailing Address button
    Then the Mailing Address page from My Profile is displayed
    And the "Form Header" text is "Mailing Address"
    And  the "Save" button is disabled
    When I enter data into the Address form
      | Full Name        | MarcusMailing |
      | Street Address 1 | 1234 1st Street |
      | City             | Santa Ana       |
      | State            | CA              |
      | Zip              | 90210           |
      | Phone Number     | 9495551212      |
    Then the "Save" button is enabled
    When I click on the Save button
    Then the Addresses page from My Profile is displayed
    Then the Add Mailing Address button is not present
    And the Delete Mailing Address icon is present
    And the Edit Mailing Address icon is present
    And the Mailing Address contains the last entered Full Name

