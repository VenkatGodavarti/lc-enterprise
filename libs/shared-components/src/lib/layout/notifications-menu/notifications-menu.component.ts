import {Component, OnInit} from '@angular/core';
import {Notification, NotificationService, NotificationType} from '@lc/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Router} from '@angular/router';


export class NotificationViewModel {
  icon: string;
  title: string;
  text: string;
  path: string;
  when: string;

  constructor(public readonly notification: Notification) {
    this.title = notification.subject;
    this.text = notification.text;
    this.path = this.getRouterLink();
    this.icon = this.getIcon();
    this.when = this.getWhenDisplay();
  }

  getRouterLink() {
    // TODO: these routes should be stored in a route service or something
    switch(this.notification.notificationType) {
      case NotificationType.ASSIGNED_TO_ORDER:
        return `manage-order/${this.notification.marketingOrderId}`;
      case NotificationType.REQUESTED_MARKETING_COPY:
        return `manage-order/${this.notification.marketingOrderId}/property-description`;
      case NotificationType.REQUESTED_PHOTOS:
      case NotificationType.PRO_PHOTOS_RECEIVED:
        return `manage-order/${this.notification.marketingOrderId}/media/photos`;
      case NotificationType.ASSIGNED_TO_TEAM:
        return `my-profile/team`;
    }
  }

  getIcon() {
    // TODO: Should we use a pipe for these?
    switch(this.notification.notificationType) {
      case NotificationType.ASSIGNED_TO_ORDER:
        return `ion-md-home`;
      case NotificationType.REQUESTED_MARKETING_COPY:
        return `ion-md-paper`;
      case NotificationType.REQUESTED_PHOTOS:
      case NotificationType.PRO_PHOTOS_RECEIVED:
        return `ion-md-photos`;
      case NotificationType.ASSIGNED_TO_TEAM:
        return `ion-md-people`;
    }
  }

  // TODO: Move to a pipe?
  getWhenDisplay(){
    if(!this.notification.audit) { return '12 Hours Ago';} // TODO: This is a fake placeholder until we have a createdAt
    const diff = Math.floor(new Date().getTime() - this.notification.audit.createdAt.getTime());
    let hours = 1000 * 60 * 60;
    const day = hours * 24;

    hours = Math.floor(diff/hours);
    const days = Math.floor(diff/day);
    const months = Math.floor(days/31);
    const years = Math.floor(months/12);
    if(hours < 24) {
      return `${hours} Hours Ago`;
    } else if(days < 31) {
      return `${days} Days Ago`;
    } else if(months < 12) {
      return `${months} Months Ago`;
    }else {
      return `${years} years Ago`;
    }

  }
}

@Component({
  selector: 'lc-notifications-menu',
  templateUrl: './notifications-menu.component.html',
  styleUrls: ['./notifications-menu.component.scss']
})
export class NotificationsMenuComponent implements OnInit {
  newNotificationCount$: BehaviorSubject<number> = new BehaviorSubject(0);
  notifications$: Observable<NotificationViewModel[]>;

  constructor(private service: NotificationService, private router: Router) {}

  ngOnInit(): void {
    this.notifications$ = this.service.get().pipe(
      map(notifications => notifications.filter(n => !n.isRead)),
      tap(notifications => this.newNotificationCount$.next(notifications.length)),
      map(notifications => notifications.map(notification => new NotificationViewModel(notification)))
    );
  }

  /**
   * Handles the click event of a notification and navigates to the proper route. Marks the notification as read
   */
  onNotificationClicked(notification: NotificationViewModel) {
    this.router.navigate([notification.path]);
    this.service.markAsRead(notification.notification);
  }

}
