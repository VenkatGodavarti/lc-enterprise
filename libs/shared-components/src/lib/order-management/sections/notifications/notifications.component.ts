import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MarketingOrder, SELECTEDORDER } from '@lc/core';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'lc-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  marketingOrder$: Observable<MarketingOrder>;

  constructor(private store: Store<any>) {
    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));
  }

  ngOnInit() {
  }

}
