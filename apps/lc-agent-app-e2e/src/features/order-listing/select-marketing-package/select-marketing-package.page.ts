import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const selectMarketingPackageElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h2"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Order Silver', field: "#order-Silver" }),
    new Element({ name: 'Order Gold', field: "#order-Gold" }),
    new Element({ name: 'Order Platinum', field: "#order-Platinum" }),
    new Element({ name: 'Silver Flyer Qty', field: "#silver-flyer-qty>b" }),
    new Element({ name: 'Gold Flyer Qty', field: "#gold-flyer-qty>b" }),
    new Element({ name: 'Platinum Flyer Qty', field: "#platinum-flyer-qty>b" }),
    new Element({ name: 'Silver Postcard Qty', field: "#silver-just-listed-postcard-qty>b" }),
    new Element({ name: 'Gold Postcard Qty', field: "#gold-just-listed-postcard-qty>b" }),
    new Element({ name: 'Platinum Postcard Qty', field: "#platinum-just-listed-postcard-qty>b" }),
    new Element({ name: 'Use My Own Photography', field: "#opt-out-photography" }),
  ]
);


export class SelectMarketingPackagePage extends LCPage {
  constructor() {
    super('Select Marketing Package', [selectMarketingPackageElements]);
  }
}
