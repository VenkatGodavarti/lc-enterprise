'rxjs-compat' dependent import locations are forbidden
Accessing the value property of a BehaviorSubject is forbidden
Calling subscribe without an error handler is forbidden
Calling subscribe without arguments is forbidden
Finnish notation required for
Nested subscribe calls are forbidden
outdated import path
prefer operator imports with no side-effects
prefer pipeable operators.
RxJS add import is unused: forkJoin
RxJS do/tap operator is forbidden
RxJS patched methods are forbidden: catch
RxJS patched methods are forbidden: filter
RxJS patched methods are forbidden: map
Subject 'availableOptions' must be private.
Subject 'close' must be private.
Subject 'isAuthenticatedSubject' must be private.
Subject 'progress' must be private.
The selector of the component "MyProfileNotificationsComponent" should have prefix "lc" (https://angular.io/styleguide#style-02-07)
The selector of the directive "SortableDirective" should have prefix "lc" (https://angular.io/styleguide#style-02-08)
Unsafe catch usage in effects and epics is forbidden
Unsafe scopes are forbidden
Unsafe switchMap usage in effects and epics is forbidden
