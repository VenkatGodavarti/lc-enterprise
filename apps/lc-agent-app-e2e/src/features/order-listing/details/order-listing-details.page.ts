import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const orderListingDetailElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Edit Package', field: "#edit-package"}),
    new Element({ name: 'Listing Details Header', field: "#listing-details-header" }),
    new Element({ name: 'Listing Details Continue', field: "#listing-details-continue" }),
    new Element({ name: 'Professional Photography Header', field: "#professional-photography-header" }),
    new Element({ name: 'Professional Photography Continue', field: "#professional-photography-continue" }),
    new Element({ name: 'Package Options Continue', field: "#package-options-continue" }),
    new Element({ name: 'Package Options Header', field: "#package-options-header" }),
    new Element({ name: 'Package Options Edit', field: "#package-options-edit" }),
    new Element({ name: 'Checkout Continue', field: "#checkout-continue" }),
  ]
);


export class OrderListingDetailsPage extends LCPage {
  constructor() {
    super('Order Listing Details', [orderListingDetailElements]);
  }
}
