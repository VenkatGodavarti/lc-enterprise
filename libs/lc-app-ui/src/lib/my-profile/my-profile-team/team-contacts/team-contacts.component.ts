import { Component, OnInit } from '@angular/core';
import {
  SELECTEDTEAM,
  TeamProfile,
  TeamContactPreferencesForm,
  UpdateTeamContactPreferences,
  UpdateTeamComplete,
  TeamActionTypes,
  TeamContactPreferences,
  TeamPhoneOptions,
  TeamNameOptions,
  OptionDisplay,
  TeamEmailOptions,
  TeamWebsiteOptions,
  TeamLogoOptions,
  TeamLicenseOptions,
  TeamPhotoOptions
} from '@lc/core';
import { Store, select } from '@ngrx/store';
import { filter, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'lc-team-contacts',
  templateUrl: './team-contacts.component.html',
  styleUrls: ['./team-contacts.component.scss']
})
export class TeamContactsComponent implements OnInit {
  photoOptions: OptionDisplay<TeamPhotoOptions>[];
  nameOptions: OptionDisplay<TeamNameOptions>[];
  emailOptions: OptionDisplay<TeamEmailOptions>[];
  phoneOptions: OptionDisplay<TeamPhoneOptions>[];
  websiteOptions: OptionDisplay<TeamWebsiteOptions>[];
  licenseOptions: OptionDisplay<TeamLicenseOptions>[];
  logoOptions: OptionDisplay<TeamLogoOptions>[];

  readonly form = new TeamContactPreferencesForm();

  team$: Observable<TeamProfile>;

  constructor(private readonly store: Store<any>, private router: Router, private route: ActivatedRoute, private actions: Actions) {
    this.team$ = store.pipe(
      select<TeamProfile>(SELECTEDTEAM),
      filter(team => team != null),
      tap((team: TeamProfile) => {
        this.form.patchValue(team.contactPreferences);
        this.getTeamContactPreferences(team);
      })
    );
  }

  ngOnInit() {
  }

  onCancel() {
    this.navigateToTeamDisplay();
  }

  getTeamContactPreferences(teamProfile: TeamProfile) {
    this.photoOptions = TeamContactPreferences.getTeamPhotoDisplayOptions(teamProfile);
    this.nameOptions = TeamContactPreferences.getTeamNameDisplayOptions(teamProfile);
    this.emailOptions = TeamContactPreferences.getTeamEmailDisplayOptions(teamProfile);
    this.phoneOptions = TeamContactPreferences.getTeamPhoneDisplayOptions(teamProfile);
    this.websiteOptions = TeamContactPreferences.getTeamWebsiteDisplayOptions(teamProfile);
    this.licenseOptions = TeamContactPreferences.getTeamLicenseDisplayOptions(teamProfile);
    this.logoOptions = TeamContactPreferences.getTeamLogoDisplayOptions(teamProfile);
  }

  onSubmit(team: TeamProfile) {
    if(!this.form.valid) {
      this.form.markAllAsDirty();
      return;
    }

    const subscription = this.actions.pipe(ofType<UpdateTeamComplete>(TeamActionTypes.UpdateTeamComplete)).subscribe(() => {
      this.navigateToTeamDisplay();
      subscription.unsubscribe();
    });
    this.store.dispatch(new UpdateTeamContactPreferences(team, this.form.value));
  }

  private navigateToTeamDisplay() {
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
