import { LCPage, Element, ElementMap } from '../../../../../../e2e';

const orderListingAgentElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Home', field: "#home-button"}),
    new Element({ name: 'Next', field: "#nextBtn" }),
    new Element({ name: 'Agent Name', field: "#agent-name-input" }),
    new Element({ name: 'Single Agent', field: "#card-option-single-agent" }),
    new Element({ name: 'Two Agents', field: "#card-option-co-listing" }),
    new Element({ name: 'Team', field: "#card-option-team" }),
    new Element({ name: 'Info Text', field: "#info-text" }),
    new Element({ name: 'Co-Lister', field: "#members-input input" }),
    new Element({ name: 'Selected Co-Listers', field: "#members-input" }),
    new Element({ name: 'Co-Lister Error', field: "#members-error" }),
  ]
);


export class OrderListingAgentPage extends LCPage {
  constructor() {
    super('Order Listing Agent', [orderListingAgentElements]);
  }
}
