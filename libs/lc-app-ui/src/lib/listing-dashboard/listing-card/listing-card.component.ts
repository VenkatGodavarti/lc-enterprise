import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { convertToParamMap, Router } from '@angular/router';

import {
  ActionTypes,
  AgentInfo,
  AgentTypes, DeleteOrder,
  ListingPhoto,
  MarketingOrder,
  OrderListingFlowStateService,
  OrderStatus,
  PackageInstance,
  TeamMemberService,
  TeamProfile,
  TeamProfileService,
  ThumbnailSizes, UIConstants,
  UpdateOrder,
  User,
  UserService,
  WorkflowAction
} from '@lc/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { DialogAction, PromptDialogService } from '@lc/shared-components-ui';

@Component({
  selector: 'lc-listing-card',
  templateUrl: './listing-card.component.html',
  styleUrls: ['./listing-card.component.scss']
})
export class ListingCardComponent implements OnInit, OnDestroy {

  readonly defaultImage = 'assets/images/no-photo2.png';

  @Input() order: MarketingOrder;   // TODO this is a slimmer version of the full MO -- should it be a different class?

  @Input() deleteMessage: string;

  @Output()
  readonly updated = new EventEmitter<MarketingOrder>();

  backgroundImage: string = this.defaultImage;
  selectedPackage: PackageInstance;
  orderStatus =  OrderStatus;

  user: User;
  agentTypes = AgentTypes;
  showHover = false;
  coListAgent: AgentInfo;
  teamProfileSub: Subscription;
  teamProfile: TeamProfile;

  get oaddress() { return this.order ? this.order.listing.address : null };

  constructor(private router: Router,
              private orderListingFlowService: OrderListingFlowStateService,
              private userService: UserService,
              private teamMemberService: TeamMemberService,
              private teamProfileService: TeamProfileService,
              private dialogService: PromptDialogService,
              private store: Store<any>) {

  }

  ngOnInit() {
    this.user = this.userService.getCurrentUser();
    this.backgroundImage = this.setBackgroudimage(this.order.photos);
    this.selectedPackage = this.order ? this.order.getSelectedPackage() : null;

    // TODO This should be refactored to retrieve the team from order.teamId once we assign a team to an order
    if(this.order.listing.agentType === AgentTypes.team) {
      this.teamProfileSub = this.teamProfileService.getAgentProfiles(this.user._id).subscribe(profiles => {
        this.teamProfile = profiles[0];
      });
    }

    if(this.order.listing.agentType === AgentTypes.twoAgents) {
      this.getAgentInfo();
    }
  }

  ngOnDestroy(): void {
    if(this.teamProfileSub) {
      this.teamProfileSub.unsubscribe();
    }
  }

  // Set background image as 1st listing photo on listing card or default no-photo.png
  setBackgroudimage(photos : ListingPhoto[]) {
    if (photos && photos[0] && photos[0].thumbnails && photos[0].thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH]) {
      return photos[0].thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH].uri;
    }
    return this.defaultImage;
  }

  /* istanbul ignore next */
  toggleArchiveOrder() {
    this.order.isArchived = !this.order.isArchived;
    this.store.dispatch(new UpdateOrder(this.order));
    this.updated.emit(this.order);
  }

  getAgentInfo() {
    if(this.coListAgent) { return; }

    // TODO This should be refactored to retrieve the co-list agent from order.coListAgentId once we assign a co-list agent to an order
    this.teamMemberService.searchAgents('Vincenzo').subscribe(agents => {
      this.coListAgent = agents[0];
    });
  }

  /**
   * Function evoked when user chooses to delete a marketing order.  Confirms with the user first then responds appropriately
   */
  deleteOrder() {
    const message = this.deleteMessage ? this.deleteMessage : 'Are you sure you wish to permanently delete this Order?  This action cannot be undone.';
    const okAction = new DialogAction(UIConstants.YES, () => {
      this.store.dispatch(new DeleteOrder(this.order));
    });
    this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)]);
    event.stopPropagation();
  }

  routeToOrder(order: MarketingOrder){
    if(!order || order._id !== this.order._id) { return; }

    // this.getOrderSubscription.unsubscribe();
    if(order.submitted) {
      this.router.navigate([`manage-order/${order._id}`]);
    } else {
      if (order.getOrderState()) {
        const newState = this.orderListingFlowService.createNewFlowState(order);
        const params = convertToParamMap({id: order._id, pid: order.selectedPackageCode});
        this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionInitialize, {state: newState}, params));
      } else {
        this.router.navigate([`order-listing/${order._id}/select-marketing-package`]);
      }
    }
  }
}
