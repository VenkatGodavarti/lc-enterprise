import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { ListingPhoto, UploadStatus, UploadPhoto } from '@lc/core';
import { FileSystemFileEntry, UploadEvent } from 'ngx-file-drop';
import { forkJoin } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThumbnailSizes, ImageUploaderService } from '@lc/core';

@Component({
  selector: 'lc-property-photo-upload',
  templateUrl: './property-photo-upload.component.html',
  styleUrls: ['./property-photo-upload.component.scss']
})
export class PropertyPhotoUploadComponent {
  @Output()
    public uploadResult = new EventEmitter<UploadPhoto[]>();

  @ViewChild('content') content: any;

  successfulUploads = [];
  failedUploads = [];
  isProcessing = false;

  statuses: UploadStatus[];

  // File validation constraints
  readonly allowedExtensionsWithDot;

  constructor(private imageUploaderService: ImageUploaderService, private modalService: NgbModal) {
    this.allowedExtensionsWithDot = imageUploaderService.allowedExtensions.map((e) => ('.' + e));
  }

  async getFilesFromUploadEvent(event: UploadEvent) {
    const filePromises = [];
    event.files.forEach(droppedFile => {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        const filePromise = new Promise((resolve) => {
          fileEntry.file(resolve);
        });
        filePromises.push(filePromise);
      }
    });
    return Promise.all(filePromises);
  }

  async handleDroppedFiles(event: UploadEvent) {
    this.successfulUploads = [];
    this.failedUploads = [];
    if (!this.isProcessing) {
      this.uploadFiles(await this.getFilesFromUploadEvent(event));
    }
  }

  handleSelectedFiles(event) {
    this.successfulUploads = [];
    this.failedUploads = [];
    this.uploadFiles(event.target.files);
  }

  uploadFiles(files: File[]) {
    this.statuses = [];
    for (const file of files) {
      this.statuses.push(new UploadStatus(file));
    }

    const uploads$ = this.statuses.map(status =>
      this.imageUploaderService.upload(status, ThumbnailSizes.STANDARD_THUMBNAIL_SPECS));

    // Join all uploads and subscribe to when they all emit
    forkJoin(uploads$)
      .subscribe(statuses => {
        console.log('Uploaded', statuses);
        this.failedUploads = statuses.filter(status => status.error).map(status => status.error);
        this.successfulUploads = statuses.filter(status => !status.error).map(status => status.fileName);

        if (this.failedUploads.length > 0) {
          this.open(this.content).then(() => this.clearUploadStatuses());
        } else {
          this.clearUploadStatuses();
        }
        // Combine all photos into one array and emit event
        const allPhotoUploads: UploadPhoto[] = [];
        statuses.forEach(status => allPhotoUploads.push(...status.photos));

        if(allPhotoUploads.length > 0) {
          this.uploadResult.emit(allPhotoUploads);
        }
      });
  }

  private clearUploadStatuses() {
    this.statuses = [];
    this.isProcessing = false;
  }

  public chooseFiles() {
    const element: HTMLElement = document.getElementById('uploadPhotos') as HTMLElement;
    element.click();
  }

  open(content): Promise<any> {
    const dialogRef = this.modalService.open(content, );
    return dialogRef.result.then(() => {}, () => {});
  }
}
