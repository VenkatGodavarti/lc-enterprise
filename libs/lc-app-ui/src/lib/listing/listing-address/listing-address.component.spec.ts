import { async, ComponentFixture, fakeAsync, flush, TestBed, inject } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import {
  OrderListingFlowStateService,
  Listing,
  AddressForm,
  SELECTEDORDER, MarketingOrder
} from '@lc/core';
import { ListingAddressComponent } from './listing-address.component';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { StoreMock, TestingModule } from '@lc/testing';
import { SharedComponentsUiModule, PromptDialogService, DialogAction } from '@lc/shared-components-ui';

describe('ListingAddressComponent', () => {
  let component: ListingAddressComponent;
  let fixture: ComponentFixture<ListingAddressComponent>;
  let router: Router;
  let store: StoreMock<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [ ListingAddressComponent ],
      providers: [
        OrderListingFlowStateService,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, new MarketingOrder({listing: new Listing()})); // set default state
  }));

  // Create the component
  beforeEach(fakeAsync(() => {
      fixture = TestBed.createComponent(ListingAddressComponent);
      component = fixture.componentInstance;

      fixture.detectChanges();
      flush();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.listingForm.valid).toBeFalsy();
  });

  it('form valid when required fields set', () => {
    const addressForm = component.listingForm.get('address') as AddressForm;
    expect(addressForm.valid).toBeFalsy();
    addressForm.controls['streetAddress1'].setValue("123 Main St.");
    addressForm.controls['city'].setValue("Anytown");
    addressForm.controls['state'].setValue("CA");
    addressForm.controls['postalCode'].setValue("01234");
    expect(addressForm.valid).toBeTruthy();
  });

  describe('#onSubmit', () => {
    it('exits early because form is not valid', () => {
      spyOn(component, 'onSubmit');
      fixture.debugElement.query(By.css('form')).triggerEventHandler('submit', null);

      expect(component.onSubmit).toHaveBeenCalled();
      expect(component.listingForm.valid).toBeFalsy();
    });

    it('updates an existing marketingOrder', async() => {
      const dialogService: PromptDialogService = TestBed.get(PromptDialogService);
      const listing: Listing = new Listing();
      Object.assign(listing, {
        address: {
          streetnAddress1: 'address',
          unitNumber: '#3',
          city: 'city',
          state: 'XX',
          postalCode: '99999',
        },
        mlsId: '55555'
      });
      store.setState(SELECTEDORDER, new MarketingOrder({ _id: "test", listing: listing}));

      component.marketingOrder$.subscribe(order => {
        const addressForm = component.listingForm.get('address') as AddressForm;
        addressForm.controls['streetAddress1'].setValue("123 Main St.");
        addressForm.controls['city'].setValue("Anytown");
        addressForm.controls['state'].setValue("CA");
        addressForm.controls['postalCode'].setValue("01234");
        addressForm.controls['streetAddress1'].markAsDirty();
        expect(component.listingForm.valid).toBeTruthy();

        // Initialize Spys
        const openDialog = spyOn(dialogService, 'openPrompt').and.callFake(()=> {});
        const dispatchMethod = spyOn(store, 'dispatch');

        // Submit order
        component.onSubmit(order);

        // Assert the dialog was called and click on the ok button
        expect(openDialog ).toHaveBeenCalled();
        const okayButton: DialogAction = openDialog.calls.argsFor(0).filter(arg => arg instanceof DialogAction)[0];
        okayButton.onClick();

        // Assert dispatch was called
        expect(dispatchMethod).toHaveBeenCalled();
      });
    });

    it('adds a new listing', () => {
      const listing: Listing = new Listing();
      Object.assign(listing, {
        address: {
          streetAddress1: 'address',
          unitNumber: '#3',
          city: 'city',
          state: 'XX',
          postalCode: '99999',
        },
        mlsId: '55555'
      });
      store.setState(SELECTEDORDER, new MarketingOrder({listing: listing}));

      component.marketingOrder$.subscribe(order => {
        const addressForm = component.listingForm.get('address') as AddressForm;
        addressForm.controls['streetAddress1'].setValue("123 Main St.");
        addressForm.controls['city'].setValue("Anytown");
        addressForm.controls['state'].setValue("CA");
        addressForm.controls['postalCode'].setValue("01234");

        expect(addressForm.valid).toBeTruthy();

        const dispatchMethod = spyOn(store, 'dispatch');
        component.onSubmit(order);

        expect(dispatchMethod).toHaveBeenCalled();
      });
    });


    it('adds a new listing as globalLuxuryListing', async() => {
      const listing: Listing = new Listing();
      Object.assign(listing, {
        address: {
          streetAddress1: 'address',
          unitNumber: '#3',
          city: 'city',
          state: 'XX',
          postalCode: '99999',
        },
        mlsId: '55555'
      });

      store.setState(SELECTEDORDER, new MarketingOrder({listing: listing}));

      const addressForm = component.listingForm.get('address') as AddressForm;
      addressForm.controls['streetAddress1'].setValue("123 Main St.");
      addressForm.controls['city'].setValue("Anytown");
      addressForm.controls['state'].setValue("CA");
      addressForm.controls['postalCode'].setValue("01234");
      component.listingForm.controls['globalLuxuryListing'].setValue(true);

      component.marketingOrder$.subscribe(order => {
        const dispatchMethod = spyOn(store, 'dispatch').and.callFake(()=>{});
        component.onSubmit(order);

        const addListingAction = dispatchMethod.calls.mostRecent().args[0];
        const updatedOrder = addListingAction.payload;
        expect(updatedOrder).toBeTruthy();
        expect(updatedOrder.listing.globalLuxuryListing).toBeTruthy();

        expect(dispatchMethod).toHaveBeenCalled();
      });
    });
  });

  it ('should display new listing header when listing is new', fakeAsync(() => {
      const listing: Listing = new Listing();

    store.setState(SELECTEDORDER, new MarketingOrder({listing: listing}));
      fixture.detectChanges();
      flush();

      const h1 = fixture.nativeElement.querySelector('h1');
      expect(h1.textContent).toEqual(component.newHeaderText);
    })
  );

  it ('should display update listing header and field values with existing listing', fakeAsync(() => {
      const listing: Listing = new Listing();
      Object.assign(listing, {
        _id: "test",
        address: {
          streetAddress1: 'address',
          unitNumber: '#3',
          city: 'city',
          state: 'XX',
          postalCode: '99999',
        },
        mlsId: '55555'
      });

      store.setState(SELECTEDORDER, new MarketingOrder({listing: listing}));
      fixture.detectChanges();
      flush();

      const h1 = fixture.nativeElement.querySelector('h1');
      expect(h1.textContent).toEqual(component.updateHeaderText);

      const addressForm = component.listingForm.get('address') as AddressForm;
      expect(addressForm.controls.streetAddress1.value).toEqual('address');
      expect(addressForm.controls.unitNumber.value).toEqual('#3');
      expect(addressForm.controls.city.value).toEqual('city');
      expect(addressForm.controls.state.value).toEqual('XX');
      expect(addressForm.controls.postalCode.value).toEqual('99999');
      expect(component.listingForm.controls.mlsId.value).toEqual('55555');
    })
  );

});
