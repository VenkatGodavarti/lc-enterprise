import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lc-product-not-implemented',
  templateUrl: './product-not-implemented.component.html',
  styleUrls: ['./product-not-implemented.component.scss']
})
export class ProductNotImplementedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
