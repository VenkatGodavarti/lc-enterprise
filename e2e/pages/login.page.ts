import { LCPage, ElementMap, Element } from '../support';

export class LoginPage extends LCPage {
  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Username', field: 'input#okta-signin-username'}),
      new Element({ name: 'Password', field: 'input#okta-signin-password'}),
      new Element({ name: 'Sign In', field: '#okta-signin-submit'}),
    ]
  );

  constructor() {
    super('Login', [LoginPage.elements])
  }
}
