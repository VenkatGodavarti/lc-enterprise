import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageLoaderService {

  constructor() { }

  /**
   * Takes in an image file, and performs validation. If validation passes, it will load the file
   * into an HtmlImage and return the DataURL
   * @param file
   * @param allowedExtensions
   * @param minFileSizeInMB
   */
  loadFile(file: File, allowedExtensions: string[], minFileSizeInMB: number): Observable<string> {
    return new Observable(observer => {

      if(!this.validateFileExtension(file, allowedExtensions)) {
        const failureMsg = `${file.name}: Invalid format (Should one of ${allowedExtensions.join(', ')})`;
        observer.error(failureMsg);
      }

      if(!this.validateFileSize(file, minFileSizeInMB)) {
        const failureMsg = `${file.name}: Image too small (Should be at least ${minFileSizeInMB}MB)`;
        observer.error(failureMsg);
      }

      this.loadImage(file, observer);
    })
  }

  public validateFileExtension(file: File, allowedExtensions: string[]): boolean {
    let filenameSuffix = file.name ? file.name.split(".").slice(-1)[0] : undefined;
    filenameSuffix = filenameSuffix ? filenameSuffix.toLowerCase() : undefined;
    return allowedExtensions.includes(filenameSuffix);
  }

  public validateFileSize(file: File, minimumFileSizeInMB: number) {
    const fileSizeInMB = file.size / (1024 * 1024);
    return fileSizeInMB >= minimumFileSizeInMB;
  }

  private loadImage(file: File, observer: Subscriber<string>) {
    // Scale here in code so we know when scaling is complete
    // Otherwise scaling can often take longer than the upload.
    // If upload/progress completes before thumbnail is visible, the display looks very odd
    const img = new Image;
    img.onload = () => this.onImageLoaded(img, observer);
    img.onerror = (e: ErrorEvent) => this.onImageError(e, file, observer);
    img.src = URL.createObjectURL(file);
  }

  private onImageError(e: ErrorEvent, file: File, observer: Subscriber<string>) {
    // If the Image errors, we need to set an appropriate error message and have the observable error out
    const msg = "Error loading thumbnail for " + file.name + ":" + e.message;
    console.warn(msg);
    observer.error(msg);
  }

  /**
   * Load the HTML Image into a file and complete the observable with the DataURL
   * @param image
   * @param observer
   */
  private onImageLoaded(image: HTMLImageElement, observer: Subscriber<string>) {
    // Dump the image into a canvas to retrieve the DataURL
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    const iw = image.width;
    const ih = image.height;
    const scale = Math.min((150 / iw), (150 / ih));
    const iwScaled = iw * scale;
    const ihScaled = ih * scale;
    canvas.width = iwScaled;
    canvas.height = ihScaled;
    ctx.drawImage(image, 0, 0, iwScaled, ihScaled);

    // NOT Sure what this is doing? Do we need this?
    const thumb = new Image();
    thumb.src = canvas.toDataURL();

    URL.revokeObjectURL(image.src);

    // Return the DataURL as the response to the observable
    observer.next(canvas.toDataURL());
    observer.complete();
  }
}
