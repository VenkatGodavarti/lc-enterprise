
import { Then, When } from 'cucumber';

import { Context, ContextKeys } from '../support';
import { protractor, browser, by, ExpectedConditions as EC } from 'protractor';
import { Element, ElementMap } from '../support/element-map';
import { LCPage } from '../support/lc.page';
import * as path from 'path';
import { fail } from 'assert';
import { Configuration as config } from '../configuration';
import { ElementFinder } from 'protractor'
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^I enter (.*) into the (.*) text field$/, {timeout: config.timeout }, async (value, fieldName) => {

  const page = Context.get<LCPage>(ContextKeys.page);
  const field = page.finder(fieldName);

  //TODO - generalizse th concept of page-ready in the page object, its different for each page (or is it step?)
  await browser.wait(EC.elementToBeClickable(field), 10000);
  await field.clear();
  await field.sendKeys(value);
  await field.sendKeys(protractor.Key.TAB);
});

Then(/^I enter (.*) into the (.*) text field on the (.*) form$/, {timeout: config.timeout }, async (value, fieldName, formName) => {

  const page = Context.get<LCPage>(ContextKeys.page);

  const field = page.finder(formName, fieldName);

  //TODO - generalizse th concept of page-ready in the page object, its different for each page (or is it step?)
  await browser.wait(EC.elementToBeClickable(field), 10000);
  await field.clear();
  await field.sendKeys(value);
  await field.sendKeys(protractor.Key.TAB);
});

When( /^I wait for the (.*) (link|button|icon|element) to load$/, {timeout: config.timeout},async (elementName: string, elementType: string)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.finder(elementName);
  await browser.wait(EC.elementToBeClickable(element));
});

//TODO find out why the to is in the step
When( /^I click on the (.*) (link|button|icon|element) to (.*) times$/, {timeout: config.timeout},async (fieldName: string, elementType: string, numberOfClicks: number)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const button = page.finder('page', fieldName);

  await browser.wait(EC.elementToBeClickable(button), 10000);

  for(var i = 0; i < numberOfClicks; i++) {
    await button.click();
  }
});

Then( /^the form fields have been cleared$/, {timeout: config.timeout},async ()=> {

  const page = Context.get<LCPage>(ContextKeys.page);

  const formData = Context.get(ContextKeys.enteredFormData);

  for (const fieldName in formData) {
    const element = page.finder(fieldName);
    const value = await element.getAttribute('value');

    expect(value).to.be.equal('');
  }
});


Then( /^the (.*) form has been cleared$/, {timeout: config.timeout},async (formName: string)=> {

  const page = Context.get(ContextKeys.page);

  const form = await page.findForm(formName).elements();

  for (let i = 0; i < form.length; i++) {
    expect(await form[i].getAttribute('value')).to.be.equal('');
  }

});


Then( /^the (.*) form has been reset$/, {timeout: config.timeout},async (formName: string)=> {

  const page = Context.get<LCPage>(ContextKeys.page);
  const form = await page.findMap(formName);

  await checkFormData(form, Context.get(ContextKeys.originalFormData));

});

Then( /^I enter data into the (.*) form$/, {timeout: config.timeout }, async (formName, table) => {

  const page = Context.get<LCPage>(ContextKeys.page);
  const form = page.findMap(formName);

  const originalData = await extractFormData(form, table);
  const enteredData = await enterFormData(form, table);

  Context.set(ContextKeys.originalFormData, originalData);
  Context.set(ContextKeys.enteredFormData, enteredData);
});

Then( /^I replace the (.*) on the page$/, {timeout: config.timeout}, async (imgName, table) => {

  const row = table.raw();

  const page = Context.get<LCPage>(ContextKeys.page);

  // get original value from the img element
  const imgElement: Element = page.element(imgName);
  const imgFinder = await imgElement.finder();
  const originalData = await imgFinder.getAttribute('src');
  Context.set(ContextKeys.originalFormData, originalData);

  // input data from feature file
  const formName = row[0][0];
  const inputName = row[0][1];
  const value = row[0][2];
  const absolutePath = path.resolve(__dirname, `${config.fileStorage}/${value}`);

  // populate the file input with new data
  const form = page.findMap(formName);
  const fileElement: Element = form.element(inputName);
  const fileFinder = await fileElement.finder();
  await fileFinder.sendKeys(absolutePath);
});

Then(/^the (.*) image has been updated on the page$/, {timeout: config.timeout}, async (imgName) => {

  const originalData = Context.get(ContextKeys.originalFormData);
  if(!originalData) {
    fail('Original image URL was not captured in previous step');
  }

  const page = Context.get<LCPage>(ContextKeys.page);
  const imgElement: Element = page.element(imgName);
  const imgFinder = await imgElement.finder();
  let data = await imgFinder.getAttribute('src');

  // wait for updated presignedUrl value to be loaded for the image.  If it doesn't load this test fill fail in 15 sec
  while (data === originalData) {
    await browser.sleep(250);
    data = await imgFinder.getAttribute('src');
  }

  // Original and new image URLs should be different
  expect(data).to.not.be.equal(originalData);
});

Then( /^the "(.*)" text on element (.*) is "(.*)"$/, {timeout: config.timeout}, async (elementName: string, index: number, value: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const elements = page.arrayFinder(elementName);

  await browser.wait(() => elements.count().then(records => records >= +index), 10000,
    `The element at index ${index} did not load in the time given.`);

  const element = elements.get(index - 1);
  await page.waitFor(element);
  const text = await getElementText(element);
  expect(text).to.be.equal(value);
});

Then( /^the "(.*)" text is "(.*)"$/, {timeout: config.timeout}, async (elementName: string, value: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.finder(elementName);
  await page.waitFor(element);
  const text = await getElementText(element);
  expect(text).to.be.equal(value);
});

Then( /^the "(.*)" contains "(.*)"$/, {timeout: config.timeout}, async (elementName: string, value: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.finder(elementName);
  browser.waitForAngularEnabled()
  const text = await element.getText();
  expect(text).to.include(value);
});

Then( /^the "(.*)" text is empty$/, {timeout: config.timeout}, async (elementName: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element = page.finder(elementName);
  const text = await element.getText();
  const isEmpty = text == null || text.trim() === '';
  expect(isEmpty).to.be.true;
});

Then( /^the (.*) contains the last entered (.*)$/, {timeout: config.timeout}, async (elementName: string, formDataName: string) => {

  const page = Context.get<LCPage>(ContextKeys.page);

  const element = page.finder(elementName);

  const formData = Context.get(ContextKeys.enteredFormData);

  const formValue = formData[formDataName];

  expect(await element.getText()).to.contain(formValue);
});


Then( /^the (.*) contains the original (.*)$/, {timeout: config.timeout}, async (elementName: string, formDataName: string) => {

  const page = Context.get<LCPage>(ContextKeys.page);

  const element = page.finder(elementName);

  const formData = Context.get(ContextKeys.originalFormData);

  const formValue = formData[formDataName];

  expect(await element.getText()).to.contain(formValue);

});

async function getElementText(element: ElementFinder) {
  const type = await element.getTagName();

  let text = '';
  if(type === "select"){
    // If the type of control is a select, we need to get the selected options text
    text = await element.$('option:checked').getText();
    text = text.trim();
  } else {
    // Otherwise, just get the text of the element
    text = await element.getText();;
  }
  return text;
}

async function extractFormData(form: ElementMap, table) {
  const extracted = {}
  const rows = table.raw()

  for (let i = 0; i < rows.length; i++) {
    const fieldName = rows[i][0];


    const element: Element = form.element(fieldName);
    const finder = element.finder();
    const value = await finder.getAttribute('value');

    extracted[fieldName] = value;
  }

  return extracted;
}

async function enterFormData(form: ElementMap, table) {

  const rows = table.raw();

  for (let i = 0; i < rows.length; i++) {
    const fieldName = rows[i][0];
    const value = rows[i][1];

    const element: Element = form.element(fieldName);
    await element.setValue(value);
  }
  return table.rowsHash();
}


async function checkFormData(form: ElementMap, data) {

  for (const key in data) {
    const fieldName = key;
    const value = data[key];

    const element: Element = form.element(fieldName);
    const finder = element.finder();
    const formFieldValue = await finder.getAttribute('value');

    expect(formFieldValue).to.be.equal(value);
  }
}

Then( /^the "(.*)" (link|button|icon|element) (has|does not have) class "(.*)"$/, {timeout: config.timeout},async (elementName: string, elementType: string, hasOrNotHas: string, className: string)=> {

  const page = Context.get<LCPage>(ContextKeys.page);

  const expectedResult = hasOrNotHas === "has";
  const element: ElementFinder = page.finder(elementName);
  const classString = await element.getAttribute('class');
  const classes = classString.split(' ');
  const found = classes.indexOf(className) > -1;
  expect(found).to.be.equal(expectedResult);

});

When( /^I hover over the (.*) (link|button|icon|element)$/, {timeout: config.timeout},async (elementName: string, elementType: string)=> {
  const page = Context.get<LCPage>(ContextKeys.page);
  const element: ElementFinder = page.finder(elementName);
  await page.mouseHover(element);
});
