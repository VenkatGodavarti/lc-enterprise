import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OktaCallbackComponent } from "./okta-callback.component";
import { AuthenticationService, OktaAuthenticationProvider } from '@lc/core';
import { UserActivityService } from '../user-activity/user-activity-service';

describe('OktaCallbackComponent', () => {
  let component: OktaCallbackComponent;
  let fixture: ComponentFixture<OktaCallbackComponent>;

  let authenticationServiceSpy;
  let oktaAuthenticationProviderSpy;
  let userActivityServiceSpy;

  beforeEach(async(() => {
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', ['redirectPostLogin']);
    oktaAuthenticationProviderSpy = jasmine.createSpyObj('OktaAuthenticationProvider', ['handleAuthentication', 'login', 'logout']);
    userActivityServiceSpy = jasmine.createSpyObj('UserActivityService', ['startWatching']);

    oktaAuthenticationProviderSpy.handleAuthentication.and.returnValue(Promise.resolve(null));
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ OktaCallbackComponent ],
      providers: [
        { provide: AuthenticationService, useValue: authenticationServiceSpy },
        { provide: OktaAuthenticationProvider, useValue: oktaAuthenticationProviderSpy},
        { provide: UserActivityService, userValue: userActivityServiceSpy}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OktaCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#retry logs out and back in', () => {
    component.retry();
    expect(oktaAuthenticationProviderSpy.logout).toHaveBeenCalled();
    expect(oktaAuthenticationProviderSpy.login).toHaveBeenCalled();
  });
});
