import { Component, Input, OnInit } from '@angular/core';
import { FlowState } from '@lc/core';

@Component({
  selector: 'lc-order-steps',
  templateUrl: './order-steps.component.html',
  styleUrls: ['./order-steps.component.scss']
})
export class OrderStepsComponent implements OnInit {

  @Input() orderState: FlowState;

  constructor() { }

  ngOnInit() { }
}
