@multimedia
@login-before-all
Feature: I want to add multi-media links to an order

Background:
  Given I am logged in to Listing Concierge
  Given the following orders exist
    | Name                 | Alamo                 |
    | Package              | Gold                  |
    | Status               | PENDING               |

Scenario Outline: Add multimedia link
  When I go to the Multimedia page in Order Management
  Then the "Multimedia" table has "0" records

  When I set the Name to "<name>"
  And I set the Link to "<link>"
  Then the "Add Link" button is <enabled>

  When I click on the Add Link button

  Then the "Multimedia" table has "1" record
  Then the "Multimedia" table has the following data
  | Name              | Link        | Agent                                  |
  | <name>            | <link>      | {{userFirstName}} {{userLastName}}     |

Examples:
    | name        | link                | enabled  |
    | Bedroom     | https://www.cnn.com | enabled  |


Scenario Outline: Multimedia Link Validation
  When I go to the Multimedia page in Order Management
  When I set the Name to "<name>"
  And I set the Link to "<link>"
  Then the "Add Link" button is <enabled>
Examples:
    | name                                          | link                          | enabled  |
    | This Name is too long for the multimedia      | https://www.cnn.com           | disabled |
    | Missing Protocal                              | www.cnn.com                   | disabled |
    | Missing Domain                                | https://cnn/article           | disabled |
    | Unsecure Protocol                             | http://cnn.com                | disabled |
    | Validate                                      | https://www.cnn.com           | enabled  |

Scenario: Delete Multimedia Link
  Given the order has "3" multimedia links

  When I go to the Multimedia page in Order Management
  Then the "Multimedia" table has "3" records
  And the "Delete Selected" button is disabled

  When I click on the "Select" button in row "1" of the "Multimedia" table
  Then the "Delete Selected" button is enabled

  # Click Delete and Validate records deleted
  When I click on the Delete Selected button
  Then the "Prompt" dialog is displayed
  When I click on the "Yes" button inside the "Prompt Dialog"
  Then the "Multimedia" table has "2" records

Scenario: Delete Multiple Link
  Given the order has "3" multimedia links

  When I go to the Multimedia page in Order Management
  Then the "Multimedia" table has "3" records
  And the "Delete Selected" button is disabled

  When I click on the "Select" button in row "1" of the "Multimedia" table
  When I click on the "Select" button in row "2" of the "Multimedia" table
  Then the "Delete Selected" button is enabled

  # Click Delete and Validate records deleted
  When I click on the Delete Selected button
  Then the "Prompt" dialog is displayed
  When I click on the "Yes" button inside the "Prompt Dialog"
  Then the "Multimedia" table has "1" records

Scenario Outline: Edit Multimedia Link
  Given the order has "1" multimedia links

  When I go to the Multimedia page in Order Management
  And I click on the "Edit" button in row "1" of the "Multimedia" table

  # Click Delete and Validate records deleted
  Then the "Edit Prompt" dialog is displayed

  When I set the Update Name to "<name>"
  And I set the Update Link to "<link>"
  When I click on the "Update" button inside the "Edit Dialog"
  Then the "Edit Prompt" dialog is not displayed

  Then the "Multimedia" table has "1" records
  And the "Multimedia" table has the following data
  | Name              | Link        |
  | <name>            | <link>      |

Examples:
    | name        | link                    |
    | dining room | https://dining-room.com |

Scenario Outline: Edit Multimedia Link Validation
  Given the order has "1" multimedia links

  When I go to the Multimedia page in Order Management
  And I click on the "Edit" button in row "1" of the "Multimedia" table

  # Click Delete and Validate records deleted
  Then the "Edit Prompt" dialog is displayed

  When I set the Update Name to "<name>"
  And I set the Update Link to "<link>"
  Then the "Update" button is <enabled>

Examples:
    | name                                          | link                          | enabled  |
    | This Name is too long for the multimedia      | https://www.cnn.com           | disabled |

#  Scenario: Add a multimedia link to an order
#    When I go to the Multimedia page in Order Management
#    And  I enter Living Room and https://www.cnn.com into the Add Multimedia Link form
#    And  I click on the Add Link button
#    Then I can see Living Room listed below the Add Multimedia Link form
