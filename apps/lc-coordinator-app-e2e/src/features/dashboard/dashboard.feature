@login-before-all
Feature: I want to verify the listing dashboard page

Background:
  Given I am logged in to Listing Concierge
  Given the following orders exist
  | Name                 | Alamo                 | White House           |
  | Package              | Gold                  | Platinum              |
  | Status               | PENDING               | PENDING               |

# Note: This test assumes there are not already any marketing orders
Scenario: Verify that hide/show toggle buttons work
  When I go to the Main page in Dashboard
  Then the "Header" contains "Hello"
  And the "My Orders Tab" text is "My Orders"
  And the "All Orders Tab" text is "All Orders"
  And the "Completed Orders Tab" text is "Completed"

  # Navigate to the All Orders tab
  When I click on the My Orders Tab button
  Then the My Orders page from Dashboard is loaded

  # Navigate to the All Orders tab
  When I click on the All Orders Tab button
  Then the All Orders page from Dashboard is loaded

  # Navigate to the My Orders tab
  When I click on the Completed Orders Tab button
  Then the Completed Orders page from Dashboard is loaded
