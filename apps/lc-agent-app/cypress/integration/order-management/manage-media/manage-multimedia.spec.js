
const order = require('../../../fixtures/marketing-order-with-photos.json');

const baseUrl = process.env.LC_AGENT_APP_BASE_URL || 'http://localhost:4200';

describe('Manage Multimedia', function() {

  it('Add Link button is enabled when form is valid', function() {
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/'+order._id, order).as('order');
    cy.route('OPTIONS', '/api/v1/marketing-orders/'+order._id);

    cy.visit(baseUrl+'/manage-order/'+order._id+'/tabset/multimedia');


    cy.get('button#multimediaitem-add-link').as('submit');

    cy.get('@submit').should('be.disabled');
    cy.get('input#add-title-input').type('Patio');
    cy.get('input#add-link-input').type('https://www.realogy.com/a-video');
    cy.get('@submit').should('not.be.disabled');

    cy.screenshot();
  });


  it('Add Link button is disabled when form is not valid', function() {
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/'+order._id, order).as('order');
    cy.route('OPTIONS', '/api/v1/marketing-orders/'+order._id);

    cy.visit(baseUrl+'/manage-order/'+order._id+'/tabset/multimedia');


    cy.get('button#multimediaitem-add-link').as('submit');

    cy.get('@submit').should('be.disabled');
    cy.get('input#add-title-input').type('Patio');
    cy.get('input#add-link-input').type('http://www.realogy.com/a-video');
    cy.get('@submit').should('be.disabled');

    cy.screenshot();
  });


  it('Add a Multimedia Link to the order', function() {
    const title = 'Patio';
    const link = 'https://www.realogy.com/patio';
    const updatedOrder = { media: { multimedia: [{ title: title, uri: link }]}, ...order };
    console.log('updatedOrder: ', updatedOrder);
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/*', order).as('order');

    cy.route('PUT', '/api/v1/marketing-orders/*', updatedOrder).as('addLink');

    cy.visit(baseUrl+'/manage-order/'+order._id+'/tabset/multimedia');


    cy.get('button#multimediaitem-add-link').as('submit');

    cy.get('input#add-title-input').type(title);
    cy.get('input#add-link-input').type(link);
    cy.get('@submit').should('not.be.disabled');


    cy.get('@submit').click();

    cy.wait('@addLink');

    cy.get('@submit').should('be.disabled');
    cy.get('input#add-title-input').should('be.empty');
    cy.get('input#add-link-input').should('be.empty');

    // Cypress can't seem to find the span if we reference it directly by id, but if we find the parent first it works
    cy.get('#select-multimedia-item-list-0 span').should('be.visible');
    cy.screenshot();
  });
});

