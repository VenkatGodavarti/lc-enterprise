import { ComponentFixture, fakeAsync, flush, TestBed, inject } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { SelectMarketingPackageComponent } from './select-marketing-package.component';
import { RouterTestingModule } from '@angular/router/testing';
import {
  FlowState,
  Listing,
  PackageInstance,
  PackageService,
  OrderListingFlowStateService,
  ORDERSTATE,
  MarketingOrder, SELECTEDORDER
} from '@lc/core';
import { Observable, of } from 'rxjs';
import { OrderListingComponent } from '../../order-listing.component';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ActionsSubject, Store } from '@ngrx/store';
import { StoreMock, TestingModule } from '@lc/testing';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

describe('SelectMarketingPackageComponent', () => {
  let component: SelectMarketingPackageComponent;
  let fixture: ComponentFixture<SelectMarketingPackageComponent>;
  let packageServiceSpy;
  let router: Router;

  let activatedRoute: ActivatedRoute;
  let store: StoreMock<any>;

  let order: MarketingOrder;

  let packages: PackageInstance[] = [];

  beforeEach(() => {

    activatedRoute = new ActivatedRoute();
    activatedRoute.data = of({});
    spyOnProperty(activatedRoute, 'parent', 'get').and.returnValue(activatedRoute);
    spyOnProperty(activatedRoute, 'paramMap', 'get').and.returnValue(of(convertToParamMap({})));

    const listing: Listing = new Listing();
    packages = [
      new PackageInstance({title: 'silver', code: '001'}),
      new PackageInstance({title: 'gold', code: '002'})
    ];

    order = new MarketingOrder();
    order.selectedPackageCode = packages[1].code;
    order.listing = listing;

    packageServiceSpy = jasmine.createSpyObj('PackageService', ['getAllAvailablePackages']);
    packageServiceSpy.getAllAvailablePackages.and.returnValue(of(packages));

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes([{path: 'order-listing/:id/:pid', component: OrderListingComponent}]),
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [ SelectMarketingPackageComponent, OrderListingComponent ],
      providers: [
        { provide: PackageService, useValue: packageServiceSpy },
        { provide: ActivatedRoute, useValue: activatedRoute },
        OrderListingFlowStateService,
        ActionsSubject,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    // MERGE Conflict: this was the local that is commented out
    // store.setState(SELECTEDORDER, new MarketingOrder(<any>{selectedPackageCode: packages[1].code})); // set default state
    store.setState(SELECTEDORDER, order); // set default state
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
  }));

  // Create the component
  beforeEach(fakeAsync(() => {
    router = TestBed.get(Router);

    fixture = TestBed.createComponent(SelectMarketingPackageComponent);
    component = fixture.componentInstance;

    component.ngOnInit();
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#marktingPackageSelected navigates to the marketing package', fakeAsync(() => {
    const listing: Listing = Object.assign(new Listing(), {
      _id: 'ABCDEFGHIJKLMN'
    });
    const marketingOrder = new MarketingOrder({_id: '123', listing: listing});
    store.setState(SELECTEDORDER, marketingOrder);


    const pkgId = 'PACKAGE-ID';
    const pkg: PackageInstance = new PackageInstance();
    pkg['code'] = pkgId;

    spyOn(store, 'dispatch');
    component.marketingPackageSelected(marketingOrder, pkg);

    expect(store.dispatch).toHaveBeenCalled();
  }));

  it('should display selected only on selected package', fakeAsync(() => {
    const s = fixture.nativeElement.querySelector(`#${packages[1].title}`);
    expect(s.textContent).toContain('Selected');

    const ns = fixture.nativeElement.querySelector(`#${packages[0].title}`);
    expect(ns).toBe(null);
    })
  );
});
