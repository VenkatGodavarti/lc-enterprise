import { Given } from 'cucumber';
import { Configuration } from '../../configuration';
import { verticalTableToRecords } from '../../step_definitions';

import * as alice from './alice-smith.json';
import * as john from './john-jones.json';

/**
 *  Cucumber steps to create test user and matching profiles in the database.
 *
 *  The tests use the ProfileProps class to set the profile preoperties decalred in the step's table of data and the
 *  UserClient class to invoke an API to create the records in Mongo
 *
 *  At this time there are 2 test users John and Alice but more can be created with test data Json files
 *
 *
 */
const userTemplates = {
  alice: alice,
  john: john
};

import { UsersClient } from '../../clients/users.client';
import { Context, Schedule, Scheduler } from '../../support';

import { ProfileProps } from '../profile/profile.props';

const props = new ProfileProps();

Given(/^the following users exist$/,  { timeout: Configuration.timeout },  async (rawTable) => {
  const users = verticalTableToRecords(rawTable);
  await createUsers(users);
});


async function createUsers(users) {

  for (let i = 0; i < users.length; i++) {

    const record = users[i];
    const name = record.Name;

    let data = userTemplates[name.toLowerCase()];
    if (!data) {
      throw new Error('Only users John and Alice can be created');
    }
    data = JSON.parse(JSON.stringify(data));
    data = editData(data, record);
    await createUser(name, data);

  }
}

function editData(data, record): any {

  let profile = data.profile;

  props.setValues(profile, record, ['Name'])

  return data;
}

async function createUser(name: string, user: any) {

  const response = await UsersClient.post(user);

  const createdUser = response.data;

  Context.setUser(name, createdUser);

  Scheduler.schedule(Schedule.afterScenario, async () => {

    await UsersClient.delete(createdUser._id);
  });
}
