import { ElementMap, Element, LCPage } from '../../../../../../e2e';

export class ProfilePage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Header',           field: '#header'}),
      new Element( { name: 'Cancel',                field: 'button#profile-cancel'}),
      new Element( { name: 'Update Profile',        field: 'button#profile-submit'}),
      new Element( { name: 'Edit Profile Photo',    field: 'i#editProfilePhoto'}),
      new Element( { name: 'Change File link',      field: 'a#change-profiile-image'}),
      new Element( { name: 'Apply Profile Image',   field: 'button#profile-image-update'}),
      new Element( { name: 'Profile Avatar',        field: 'img#avatar'}),
      new Element( { name: 'Header Profile Avatar', field: 'img#header-profile-avatar'}),
      new Element( { name: 'Change File',           field: 'a#change-profile-image'})
    ]
  );

  static formElements = new ElementMap( 'Profile',
    [
      new Element( { name: 'First Name',    model: 'preferredFirstName', field: 'input#firstName-input'}),
      new Element( { name: 'Last Name',     model: 'preferredLastName',  field: 'input#lastName-input'}),
      new Element( { name: 'Email',         model: 'preferredEmail',     field: 'input#email-input'}),
      new Element( { name: 'Phone Number',  field: 'input#phone-input'}),
    ]);

  constructor() {
    super('Profile', [ProfilePage.elements, ProfilePage.formElements]);
  }

}
