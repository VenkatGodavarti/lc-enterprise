import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { ApiService } from "./api.service";
import { UserService } from './user.service';
import { MarketingOrder } from '../models/marketing-order.model';
import * as Mustache from 'mustache';
import { ProductDescription } from '../models/product-description.model';
import { Observable } from 'rxjs';
import { Listing, OrderState } from '../models/listing.model';
import { ListingPhoto } from '../models/listing-photo.model';
import { UploadPhoto } from '../models/upload-photo.model';
import { PhotoService } from '../services/photo.service';
import { MultiMediaItem, MultiMediaItemRequest } from '../models/multi-media-item.model';

@Injectable()
export class MarketingOrderService {

  resource = 'marketing-orders';

  constructor(private apiService: ApiService,
              private userService: UserService) { }

  /**
   * Get all marketing orders owned by the currently logged in user. This will
   * also set the summary flag on the MarketingOrder object
   *
   * @return an Observable that results in an array of MarketingOrders
   */
  getAllMyOrders(): Observable<MarketingOrder[]> {
    return this.apiService.get('marketing-orders/summary').pipe(
        map((orders: [MarketingOrder]) => {
          const orderObjs = [];
          orders.forEach((o: MarketingOrder) => {
            o.summary = true;
            orderObjs.push(new MarketingOrder(o));
          });
          return orderObjs;
        })
    );
  }

  getOrder(id): Observable<MarketingOrder> {
    return this.apiService.get(`${this.resource}/${id}`).pipe(map( body => new MarketingOrder(body) ));
  }

  getOrdersForListing(listingId): Observable<MarketingOrder[]> {
    return this.apiService.get(this.resource, {'listingId': listingId});
  }

  /**
   * Queries for the marketingOrder photos. Includes the photographerId in the route parameters
   * if provided.
   */
  getOrderPhotos(marketingOrderId: string, photographerId?: string, productCode?: string): Observable<ListingPhoto []> {
    let route = `${this.resource}/${marketingOrderId}/photos`;
    let params: any = {};
    if(photographerId) {
      params.photographerId = photographerId;
    }
    if(productCode) {
      params.productCode = productCode;
    }
    return this.apiService.get(route, params).pipe(
      map(photos => photos.map(photo => new ListingPhoto(photo)))
    );
  }

  updateListing(id: string, listing: Listing): Observable<Listing> {
    return this.apiService.put(this.resource + '/' + id + '/listing', listing);
  }

  updateOrderPartial(order: MarketingOrder, fields: string[]) {
    const clonedOrder = this.cleanOrder(order);

    fields = fields || [];
    const partial = {};
    //order.orderState = order.listing.orderState;
    fields.forEach( field => {
      partial[field] = clonedOrder[field];
    });
    return this.apiService.put(this.resource + '/' + order._id, partial);
  }

  // Removes "read-only" properties that cannot be included in put or post operations
  private cleanOrder(order: MarketingOrder): MarketingOrder {
    // HACK: This is a temporary solution and should be removed.
    // We should be handling photos in a separate service instead of the model
    const clonedOrder = Object.assign({}, order);
    const clonedListing = Object.assign({}, order.listing);
    delete clonedListing.marketingOrder;
    delete clonedOrder.summary;
    clonedOrder.listing = clonedListing;

    delete clonedOrder.statusHistory;
    // delete clonedOrder.status;

    if (clonedOrder.availablePackages) {
      clonedOrder.availablePackages.forEach(availablePackage => {
        if (availablePackage.products) {
          availablePackage.products.forEach(product => {
            delete product.status;
            delete product.statusHistory;
          });
        }
      });
    }
    return clonedOrder;
  }

  saveOrder(order: MarketingOrder) {
    const clonedOrder = this.cleanOrder(order);

    if (order._id) {
      return this.updateOrder(clonedOrder);
    } else {
      return this.createOrder(clonedOrder);
    }
  }

  saveOrderProductDescription(description: ProductDescription) {

    const url = `${this.resource}/${description.orderId}?packageCode=${description.packageCode}&productCode=${description.productCode}`;

    return this.apiService.put(url, {
      marketingCopyHeadline: description.marketingCopyHeadline,
      marketingCopyBody: description.marketingCopyBody
    });
  }

  protected createOrder(order: MarketingOrder) {
    return this.apiService.post(this.resource, this.cleanOrder(order));
  }

  protected updateOrder(order: MarketingOrder) {
    return this.apiService.put(this.resource + '/' + order._id, this.cleanOrder(order))
      .pipe(map(response => new MarketingOrder(response)));
  }

  deleteOrder(order: MarketingOrder) {
    return this.apiService.delete(this.resource + '/' + order._id);
  }

  submitOrder(order: MarketingOrder) {
    return this.apiService.put(this.resource + '/' + order._id + '/submit', this.cleanOrder(order))
      .pipe(map(response => new MarketingOrder(response)));
  }

  fulfill(id, productId, template) {
    return this.apiService.get(`${this.resource}/${id}/dictionary/${productId}/1`).pipe(map(data => {
      return Mustache.render(template, data)
    }));
  }

  /**
   * Sends an update of the MarketingOrder for photos only.
   *
   * @param marketingOrder
   * @param productCode
   */
  setPhotos(marketingOrder: MarketingOrder, productCode?: string): Observable<any> {
    let url = `marketing-orders/${marketingOrder._id}/photos`;
    if(productCode) {
      url += `?packageCode=${marketingOrder.selectedPackageCode}&productCode=${productCode}`;
    }
    const photos = marketingOrder.getPhotosFor(productCode);
    return this.apiService.put(url, photos).pipe(
      tap(results => marketingOrder.setPhotos(results, productCode))
    );
  }

  /**
   * Adds a photo to a MarketingOrder.
   *
   * @param marketingOrder The MarketingOrder
   * @param listingPhotos: New photos to add
   * @return Observable<any> of the response
   * @param productCode is optional. When sent we are modifying the product instance photos
   */
  addPhoto(marketingOrder: MarketingOrder, listingPhotos: ListingPhoto[], productCode: string): Observable<any> {
    if (productCode) {
      return this.apiService.put(`marketing-orders/${marketingOrder._id}/photos?push=true&packageCode=${marketingOrder.selectedPackageCode}&productCode=${productCode}`, listingPhotos);
    }
    return this.apiService.put(`marketing-orders/${marketingOrder._id}/photos?push=true`, listingPhotos);
  }

  /**
   * Update a single photo associated with a MarketingOrder or a product instance
   *
   * @param id
   * @param photoId
   * @param values the photo array
   * @param packageCode is optional. When sent we are modifying the product instance photos
   * @param productCode is optional. When sent we are modifying the product instance photos
   */
  updatePhoto(id: string, photoId: string, values: any, packageCode?: string, productCode?: string): Observable<ListingPhoto> {
    if (packageCode && productCode) {
      return this.apiService.put(`marketing-orders/${id}/photos/${photoId}?packageCode=${packageCode}&productCode=${productCode}`, values);
    } else {
      return this.apiService.put(`marketing-orders/${id}/photos/${photoId}`, values);
    }
  }

  /**
   * Parses upload response into photo, adds to MarketingOrder, and updates the MarketingOrder on server
   */
  addPhotoFromUploadAndUpdatePhotos(marketingOrder: MarketingOrder, photos: UploadPhoto[], photographerId?: string, productCode?: string) : Observable<any> {
    const userId = this.userService.getUserId();
    const photoCount = marketingOrder.getPhotosFor(productCode).length; // TODO: This will not handle concurrency very well
    const listingPhotos: ListingPhoto[] = PhotoService.createFromRawPhotos(photos, userId, photoCount, photographerId);

    return this.addPhoto(marketingOrder, listingPhotos, productCode).pipe(
      tap(photos => marketingOrder.setPhotos(photos, productCode))
    );
  }


  /**
   * Update the orderstate. This is currently implemented on the marketing order collection
   *
   * @param id
   * @param state
   */
  updateOrderState(id: string, state: OrderState): Observable<any> {
    return this.apiService.put(this.resource + '/' + id + '/orderstate', state);
  }

  addMultimediaLink(marketingOrderId: string, linkRequest: MultiMediaItemRequest): Observable<MultiMediaItem> {
    return this.apiService.put(this.resource + '/' + marketingOrderId + '/addlink', linkRequest);
  }
}
