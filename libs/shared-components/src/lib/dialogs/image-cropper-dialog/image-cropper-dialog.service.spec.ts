import { TestBed } from '@angular/core/testing';

import { ImageCropperDialogService } from './image-cropper-dialog.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('ImageCropperDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ NgbModule ],
    providers: [ ImageCropperDialogService ]
  }));

  it('should be created', () => {
    const service: ImageCropperDialogService = TestBed.get(ImageCropperDialogService);
    expect(service).toBeTruthy();
  });
});
