import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { MarketingOrder, SELECTEDORDER } from '@lc/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'lc-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  @HostBinding('class') private hostClasses = 'w-100';

  marketingOrder$: Observable<MarketingOrder>;

  constructor(public router: Router,
              private store: Store<any>) {
    // TODO - we can  use the id in the route rather that the loaded order to navigate
    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));
  }

  ngOnInit() {
  }

  manageOrder(marketingOrder) {
    const manageOrderUrl = `/manage-order/${marketingOrder._id}`;
    this.router.navigate([manageOrderUrl]);
  }
}
