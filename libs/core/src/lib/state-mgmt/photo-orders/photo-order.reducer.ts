import { PhotoOrder } from '../../models';
import { PhotoOrderActionTypes } from './photo-order.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface PhotoOrdersState extends EntityState<PhotoOrder> {
  selectedOrderId: string | null;
}

export const adapter: EntityAdapter<PhotoOrder> = createEntityAdapter<PhotoOrder>({
  selectId: (aOrder) => aOrder._id
});
export const photoOrderInitialState: PhotoOrdersState = adapter.getInitialState({
  selectedOrderId: null
});

export function photoOrderReducer(state = photoOrderInitialState, action): EntityState<PhotoOrder> {
  switch (action.type) {
    case PhotoOrderActionTypes.PhotoOrderSelected:
      if (state.selectedOrderId === action.payload) {
        return state;
      }
      return Object.assign({}, state, { selectedOrderId: action.payload });
    // set state to loaded order
    case PhotoOrderActionTypes.InitializePhotoOrder:
    case PhotoOrderActionTypes.LoadPhotoOrder:
    case PhotoOrderActionTypes.GetPhotoOrderComplete:
      return Object.assign({},
        adapter.updateOne({id: action.payload._id, changes: action.payload}, state),
        { selectedOrderId: action.payload._id });
    case PhotoOrderActionTypes.UpdatePhotoOrderComplete:
      return adapter.updateOne({id: action.payload._id, changes: action.payload}, state);
    case PhotoOrderActionTypes.LoadPhotoOrdersComplete:
      return Object.assign({}, adapter.addAll(action.payload, state),{ selectedOrderId: null });
    case PhotoOrderActionTypes.LoadPhotoOrderMediaComplete:
      return Object.assign({},
        adapter.updateOne({id: action.payload._id, changes: action.payload}, state));
    case PhotoOrderActionTypes.AddPhotoOrderMultimediaLinkComplete:
      const order = getSelectedOrder(state);
      order.media.multimedia.push(action.payload);
      // update the current with a new photoOrder
      return Object.assign({},
        adapter.updateOne({id: order._id, changes: new PhotoOrder(order)}, state));
    default:
      return state;
  }
}


// Selectors

export const PHOTOORDERS = 'photoOrders';

export const getSelectedOrderId = (state: PhotoOrdersState) => state.selectedOrderId;
export const getSelectedOrder = (state: PhotoOrdersState) => state.selectedOrderId ? state.entities[state.selectedOrderId] : new PhotoOrder({_id: null});

const { selectIds, selectEntities, selectAll } = adapter.getSelectors();

export const selectOrderState = createFeatureSelector<PhotoOrdersState>(PHOTOORDERS);

export const selectAllOrders = createSelector(
  selectOrderState,
  selectAll
);

export const selectOrderEntities = createSelector(
  selectOrderState,
  selectEntities
);

export const selectCurrentOrderId = createSelector(
  selectOrderState,
  getSelectedOrderId
);

export const selectCurrentOrder = createSelector(
  selectOrderEntities,
  selectCurrentOrderId,
  (orderEntities, orderId) => {
    return orderId ? orderEntities[orderId] : new PhotoOrder({_id: null});
  }
);
