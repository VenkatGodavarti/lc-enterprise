const fs = require('fs');

export class BaseData {
  static readFileToObject(filePath: string) {
    const path = `${__dirname}/${filePath}`;
    let rawdata = fs.readFileSync(path);
    let object = JSON.parse(rawdata);
    return object
  }
}
