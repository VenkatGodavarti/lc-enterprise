import { Component, Input, OnInit, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';

import { AgentTypes, MarketingOrder } from '@lc/core';

@Component({
  selector: 'lc-listing-card-container',
  templateUrl: './listing-card-container.component.html',
  styleUrls: ['./listing-card-container.component.scss']
})
export class ListingCardContainerComponent implements OnInit, OnChanges {

  @Input() orders: MarketingOrder[];

  displayedOrders: MarketingOrder[];

  archived = false;

  agentTypeFilter = null;
  readonly marketedByOptions: {display: string, value?: string}[] = [{display: 'All', value: null}];

  constructor() {
    Object.keys(AgentTypes).forEach(key => {
      this.marketedByOptions.push({display: AgentTypes[key], value: AgentTypes[key]});
    });
   }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.orders) {
      this.filterOrders();
    }
  }
  /* istanbul ignore next */
  showArchived() {
    this.archived = !this.archived;
    this.filterOrders();
  }

  filterOrders() {
    this.agentTypeFilter = this.agentTypeFilter === 'null' ? null : this.agentTypeFilter;
    this.displayedOrders = this.orders.filter(order =>
      order.isArchived === this.archived
      && (!this.agentTypeFilter || order.listing.agentType === this.agentTypeFilter)
    );
  }

}
