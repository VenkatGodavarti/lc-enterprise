import { LCPage, ElementMap, Element } from '../../support';

export class SummaryPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Listing', field: '#listing-tile'}),
      new Element({ name: 'Property Description', field: '#property-description-tile'}),
      new Element({ name: 'Notifications', field: '#notifications-tile'}),
      new Element({ name: 'Products & Services', field: '#products-tile'}) ,
      new Element({ name: 'Photos', field: '#photos-tile'}),
      new Element({ name: 'Street Address', field: '#lc-listing-address-street-unit'}),
      new Element({ name: 'City, State, Zip', field: '#lc-listing-address-city-state-zip'}),
      new Element({ name: 'Listing Photo', field: '#lc-listing-photo'}),
      new Element({ name: 'Listing Price', field: '#lc-listing-list-price'}),
      new Element({ name: 'Manage Listing Details', field: '#lc-listing-manage-details'}),
      new Element({ name: 'Default Listing Photo', field: 'div[style^="background-image:"][style$="assets/images/no-photo2.png)"]'}),
    ]
  );
  constructor() {
    super('Summary', [SummaryPage.elements]);
  }

}
