#!/usr/bin/env node

const fs = require('fs');

if (fs.existsSync('./package-lock.json')) {
  const contents = fs.readFileSync('./package-lock.json').toString();

  const result = contents.indexOf('registry.npmjs.org');

  if (result > -1) {
    console.error('\n\nFailed: Package-lock.json is using public repo!  :-(\n\n');
    console.error('Please look at the link below to fix your package-lock.json');
    console.error('https://nrt-trial.atlassian.net/wiki/spaces/LC/pages/134905944/Get+NPM+to+Install+from+LC+Repo\n\n');
    process.exit(1);
  } else {
    console.log('Pass: Package-lock.json looks good.  :-)');
    process.exit(0);
  }
} else {
  console.error('\n\nFailed: Package-lock.json is missing!  :-(\n\n');
  console.error('Please look at the link below to fix your package-lock.json');
  console.error('https://nrt-trial.atlassian.net/wiki/spaces/LC/pages/134905944/Get+NPM+to+Install+from+LC+Repo\n\n');
  process.exit(2);
}

