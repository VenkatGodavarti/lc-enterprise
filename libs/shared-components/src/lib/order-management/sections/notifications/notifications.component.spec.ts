import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsComponent } from './notifications.component';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;

describe('NotificationsComponent', () => {
  let component: NotificationsComponent;
  let fixture: ComponentFixture<NotificationsComponent>;
  let store: SpyObj<Store<any>>;

  beforeEach(async(() => {
    store = jasmine.createSpyObj('Store', ['dispatch', 'pipe']);

    TestBed.configureTestingModule({
      declarations: [ NotificationsComponent ],
      providers: [
        { provide: Store, useValue: store },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
