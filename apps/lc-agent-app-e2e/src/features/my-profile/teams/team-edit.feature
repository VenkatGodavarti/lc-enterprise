@login-before-all
@team-profile
@team-edit
Feature: Create a Team Profile
Background:
  Given I am logged in to Listing Concierge
  And the following team profile exists
  | Name             | coldwell                   |
  When I go to the My Team page in My Profile
  Then the My Team page from My Profile is loaded

Scenario: Create a new team
  When I enter data into the Team Profile form
  | Team Name        | Team Awesome               |
  | Phone            | 9495551212                 |
  | Website          | https://realogy.com        |
  | Email            | teamawesome@realogy.com    |
  | Slogan           | We sell houses             |

  And I click on the "Update Profile" button
  # Bug: path is not changing
  # Then the Team page from My Profile is loaded

Scenario: Team form validation
  When I enter data into the Team Profile form
    | Team Name        | |
    | Phone            | |
    | Website          | |
    | Email            | |
    | Slogan           | |

  When I click on the "Update Profile" button
  Then the "Team Name" has an error containing "required"
  Then the "Phone" has an error containing "required"
  Then the "Website" has an error containing "required"
  Then the "Email" has an error containing "required"
