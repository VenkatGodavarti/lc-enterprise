import { FormControl, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { Note } from '../models/note.model';

export class NoteForm extends BaseForm<Note> {
  constructor(note?: Note) {
    super({
      ownerId: new FormControl(null, [Validators.required]),
      text: new FormControl(null, [Validators.required]),
    });

    if(note){
      this.patchValue(note);
    }
  }
}
