import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  WorkflowAction,
  ActionTypes,
  Listing,
  PhotographerAppointment,
  PhotographerAppointmentForm,
  SELECTEDORDER, MarketingOrder, UpdateOrderListing, UpdateOrder
} from '@lc/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { filter, map, tap } from 'rxjs/operators';

/**
 * PhotographerCommentsComponent is responsible for displaying options for using
 * lockboxes and general notes to the professional photographer.  The output is
 * stored in fields of the Listing.PhotographerAppointment and
 * Listing.PhotographerAppointment.Lockbox objects.
 */
@Component({
  selector: 'lc-app-photographer-comments',
  templateUrl: './photographer-comments.component.html',
  styleUrls: ['./photographer-comments.component.scss']
})
export class PhotographerCommentsComponent {
  marketingOrder$: Observable<MarketingOrder>;
  params: ParamMap;

  infoForm: PhotographerAppointmentForm;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {
    this.marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => order != null),
      tap(order => this.populateForm(order))
    );
    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  // populate FormGroup from existing data
  populateForm(marketingOrder: MarketingOrder) {
    const product = marketingOrder.getPhotographyProduct();
    const appointment = product ? product.serviceAppointment : undefined;
    this.infoForm = new PhotographerAppointmentForm(appointment)
  }

  handleLockboxInfo(appointment: PhotographerAppointment) {
    // if lockbox checkbox value is false make sure the object is empty, otherwise populate
    if(!this.infoForm.lockboxAvailable) {
      appointment.lockbox = null;
    } else {
      appointment.lockbox = this.infoForm.lockbox;
    }

    appointment.lockboxAvailable = this.infoForm.lockboxAvailable;
    appointment.comments = this.infoForm.comments;
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(marketingOrder: MarketingOrder) {
    if(!this.infoForm.valid) {
      return;
    }

    const photographyProduct = marketingOrder.getPhotographyProduct();

    // create new PhotographyAppointment if one doesn't yet exist
    if(!photographyProduct.serviceAppointment) {
      photographyProduct.serviceAppointment = new PhotographerAppointment();
    }

    // populate lockbox info
    const appointment = photographyProduct.serviceAppointment;
    this.handleLockboxInfo(appointment);

    this.store.dispatch(new UpdateOrder(marketingOrder));
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true, parentCompleted: true},
      this.params));
  }
}
