import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'lc-upload-results-dialog',
  templateUrl: './upload-results-dialog.component.html',
  styleUrls: ['./upload-results-dialog.component.scss']
})
export class UploadResultsDialogComponent implements OnInit {
  public readonly close = new Subject();

  public failedMessages: string[];

  constructor() { }

  ngOnInit() {
  }

  initialize(failedMessages: string[]) {
    this.failedMessages = failedMessages;
  }

  onClose() {
    this.close.next();
  }
}
