import {Deserializable} from './deserializable.model';

export class ProductType implements Deserializable<ProductType> {

  title: string;
  description: string;
  code: string;
  category: string;
  price: number;
  unitQuantity: number;
  imageId: string;
  status: string;

  deserialize(input: any): ProductType {
    Object.assign(this, input);
    return this;
  }
}
