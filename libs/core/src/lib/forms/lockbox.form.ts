import { FormControl  } from '@angular/forms';
import { Lockbox } from '@lc/core';
import { BaseForm } from './base.form';

export class LockboxForm extends BaseForm {
  constructor(value?: Lockbox){
    super({
      code: new FormControl(),
      location: new FormControl(),
    });

    if (value) {
      this.patchValue(value);
    }
  }
}
