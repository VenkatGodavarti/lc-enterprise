import {LogEntry} from "./logentry";
import {Appender} from "./appender";

/**
 * Simple appender that appends log entries to the console.
 */
export class ConsoleAppender extends Appender {

  name = 'console-appender';

  append(entry: LogEntry) {

    let str = entry.message;

    if (!str || !str.length) {
      str = '';
    }
    this.write(entry.date + ' [' + entry.category + '] ' + entry.level + ' - ' + str);
  }

  write(str: string) {
    console.log(str);
  }
}
