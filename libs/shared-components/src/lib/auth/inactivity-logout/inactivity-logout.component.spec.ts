import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import {  } from './inactivity-logout.component';
import { AuthenticationService } from '@lc/core';
import { InactivityLogoutComponent } from './inactivity-logout.component';


describe('InactivityLogoutComponent', () => {
  let component: InactivityLogoutComponent;
  let fixture: ComponentFixture<InactivityLogoutComponent>;
  let authServiceSpy;

  beforeEach(async(() => {

    authServiceSpy = jasmine.createSpyObj('AuthenticationService', ['logout', 'redirectToLogin']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [ InactivityLogoutComponent ],
      providers: [
        { provide: AuthenticationService, useValue: authServiceSpy},
      ]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(InactivityLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
