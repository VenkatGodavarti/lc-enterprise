
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { by, ElementFinder, browser } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^there are (.*) "(.*)" (buttons|elements)$/, {timeout: config.timeout }, async (count, elementName, elementType) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const elements = page.arrayFinder(elementName);
  await browser.wait(() => elements.count().then(records => records == +count), 10000,
    `The ${count} expected ${elementType} did not resolve in the time given.`);
});
