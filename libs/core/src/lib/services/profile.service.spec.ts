import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment';
import { ApiService } from './api.service';
import { ProfileService } from './profile.service';
import {
  AddressInformation,
  AddressType,
  CardContactInfo,
  ContactType,
  LoggerFactory,
  PhoneType,
  PhotoService,
  Profile
} from '@lc/core';
import { CommonProfile, PhoneNumber } from '../models';

describe('ProfileService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let service: ProfileService;
  let resource;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService, ProfileService, LoggerFactory, PhotoService]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    service = injector.get(ProfileService);
    resource = environment.apiBaseURL + service.resource;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("#updateProfile should call server to update a profile", () => {

    const profileId = '5bdc9c240e3858131e797123';
    const profile = new Profile();
    profile._id = profileId;
    profile.photoUrl = 'test/data.jpg';

    service.updateProfile(profileId, profile).subscribe((result) => {
      expect(result).toEqual(profile);
    });

    const req = httpMock.expectOne(`${resource}/${profileId}`);
    expect(req.request.method).toBe('PUT');
    req.flush(profile);
  });


  it("#updateProfilePartial should call server to update only the fields requested", () => {

    const profileId = '5bdc9c240e3858131e797123';
    const profile = new Profile();
    profile._id = profileId;
    profile.preferredEmail = 'test@realogy.com';
    profile.contactInfo = [
      new CardContactInfo({
          type: ContactType.card,
          card: new AddressInformation({
            type: AddressType.SHIPPING,
            address: {
              streetAddress1: '123 4th St.',
              city: 'Costa Mesa',
              state: 'CA',
              postalCode: '92626'
            }
          })
        })
    ];

    service.updateProfilePartial(profile, ['contactInfo']).subscribe((result) => {
      expect(result).toEqual(profile);
    });

    const req = httpMock.expectOne(`${resource}/${profileId}`);
    expect(req.request.method).toBe('PUT');
    const body = req.request.body;
    expect(body.contactInfo).toBe(profile.contactInfo);
    expect(body.preferredEmail).toBeFalsy();

    req.flush(profile);
  });


  it("Should return #preferredFirstName to get firstName/preferredFirstName", () => {
    // check for commonprofile firstName
    expect(ProfileWithCommonProfile().preferredFirstName).toBe("Jim");

    // check for PreferredFirstName in Profile
    expect(ProfileWithPreferredFields().preferredFirstName).toBe("Jimmy");

  });

  it("Should return #preferredLastName to get lastName/preferredLastName", () => {
    // check for commonprofile lastName
    expect(ProfileWithCommonProfile().preferredLastName).toBe("Spalding");

    // check for PreferredLastName in Profile
    expect(ProfileWithPreferredFields().preferredLastName).toBe("Spalde");

  });

  it("Should return #preferredEmail to get email/getPreferredEmail", () => {
    // check for commonprofile email
    expect(ProfileWithCommonProfile().preferredEmail).toBe("jim@spalding.com");

    // check for PreferredEmail in Profile
    expect(ProfileWithPreferredFields().preferredEmail).toBe("jim@spalding.io");

  });

  function ProfileWithCommonProfile() {
    const profileId = '5bdc9c240e3858131e797123';
    const profile = new Profile({
      _id: profileId,
      commonProfile: createCommonProfile()
    });
    return profile;
  }

  function ProfileWithPreferredFields() {
    const profile = new Profile({
      _id: '5bdc9c240e3858131e797123',
      commonProfile: createCommonProfile(),
      preferredFirstName: "Jimmy",
      preferredLastName: "Spalde",
      preferredEmail: "jim@spalding.io",
      phoneNumbers: [createPhoneNumber()]

    });

    return profile;
  }

  function createCommonProfile(){
    const commonProfile = new CommonProfile();
    commonProfile.firstName = "Jim";
    commonProfile.lastName = "Spalding";
    commonProfile.email = "jim@spalding.com";
    commonProfile.phoneNumbers= [createPhoneNumber()];

    return commonProfile;

  }

  function createPhoneNumber(){
    const phoneNumber = new PhoneNumber();
    phoneNumber.number = "1527368236";
    phoneNumber.priority = 0;
    phoneNumber.type = PhoneType.mobile;

    return phoneNumber;
  }
});
