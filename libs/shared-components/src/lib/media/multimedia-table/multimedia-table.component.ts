import {
  Component,
  EventEmitter,
  Output,
  Input,
  OnChanges,
  SimpleChanges,
  Attribute
} from '@angular/core';
import { MultiMediaItem } from '@lc/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MultiMediaItemForm } from '../multi-media-item-form/multi-media-item.form';

class MultimediaViewModel {
  constructor(public item: MultiMediaItem, public isSelected: boolean = false) {
  }
}

@Component({
  selector: 'lc-multimedia-table',
  templateUrl: './multimedia-table.component.html',
  styleUrls: ['./multimedia-table.component.scss']
})
export class MultimediaTableComponent implements OnChanges {

  @Input()
  items: MultiMediaItem[];

  @Output()
  onUpdate = new EventEmitter<MultiMediaItem>();

  @Output()
  onDelete = new EventEmitter<MultiMediaItem[]>();

  formGroup = new MultiMediaItemForm();

  editItem: MultiMediaItem;

  deleteEnabled: boolean = true;
  selectEnabled: boolean = true;
  editEnabled: boolean = true;

  viewModels: MultimediaViewModel[] = [];
  allSelected: boolean = false;
  noneSelected: boolean = false;

  constructor(@Attribute('deleteEnabled')  deleteEnabled : string,
              @Attribute('selectEnabled') selectEnabled : string,
              @Attribute('editEnabled') editEnabled : string,
              private modalService: NgbModal) {
    this.deleteEnabled = (deleteEnabled === null) || (deleteEnabled === "true");
    this.selectEnabled = (selectEnabled === null) || (selectEnabled === "true");
    this.editEnabled = (editEnabled === null) || (editEnabled === "true");
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.items) {
      // Obtain the previous selected items
      const selectedItems = this.viewModels.filter(vm => vm.isSelected).map(vm => vm.item);

      // Query the items, sort by date descending and map into an appropriate view model
      this.viewModels = this.items
        .sort((a,b) => b.audit.updatedAt.getTime() - a.audit.updatedAt.getTime())
        .map(item => new MultimediaViewModel(item, selectedItems.indexOf(item) > -1));

      this.onSelectionChanged();
    }
  }

  /**
   * Queries the selected ViewModels and sets the appropriate properties needed for the UI
   * to determine the status of the Check All checkbox and Delete Selected button
   */
  onSelectionChanged() {
    this.allSelected = this.viewModels.every(vm => vm.isSelected);
    this.noneSelected = this.viewModels.every(vm => !vm.isSelected);
  }

  /**
   * Toggles the status of all of the multimedia links. If any of the links are selected, toggle
   * will deselect all links. If none are selected, it will select all the links.
   */
  toggleAll() {
    const setAsSelected = this.viewModels.some(vm => !vm.isSelected);
    this.viewModels.forEach(vm => vm.isSelected = setAsSelected);
  }

  /**
   * Queries all selected ViewModels and will select the appropriate link. It will then emit the
   * onDelete event with the selected multimedia links
   */
  deleteSelected() {
    const selectedItems = this.viewModels.filter(vm => vm.isSelected).map(vm => vm.item);
    this.onDelete.emit(selectedItems);
  }

  open(modal, item) {
    this.editItem = item;
    this.formGroup.patchValue(item);
    this.modalService.open(modal, { centered: true, size: 'lg'});
  }

  doUpdate() {
    Object.assign(this.editItem, this.formGroup.value);

    this.onUpdate.emit(this.editItem);
    this.close();;
  }

  onCancel() {
    this.close();
  }

  close() {
    this.modalService.dismissAll();
    this.editItem = null;
    this.formGroup.reset();
  }
}
