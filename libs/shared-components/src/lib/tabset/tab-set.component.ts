import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'lc-tab-set',
  templateUrl: './tab-set.component.html',
  styleUrls: ['./tab-set.component.scss']
})
export class TabSetComponent implements OnInit {

  @Input()
  navLinks: any[];
  @Input()
  activeId: string;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {

    if (this.activatedRoute.snapshot.data && this.activatedRoute.snapshot.data['navLinks']) {
      this.navLinks = this.activatedRoute.snapshot.data['navLinks'];
      this.activeId = this.activatedRoute.snapshot.data['activeId'];
    }
  }

  ngOnInit() {
    // initial the active tab based on URL
    const navLink = this.navLinks.find(aNavLink => this.router.url.endsWith(aNavLink.path));
    this.activeId = navLink && navLink.id;
  }

  tabChange($event: NgbTabChangeEvent) {
    this.router.navigate([$event.nextId], {relativeTo: this.activatedRoute});
  }
}
