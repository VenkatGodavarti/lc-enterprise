import { LCPage, ElementMap, Element } from '../support';

export class LogoutPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Sign In', field: 'button#signin-button'}),
    ]
  );

  constructor() {
    super('Logout', [LogoutPage.elements]);
  }
}
