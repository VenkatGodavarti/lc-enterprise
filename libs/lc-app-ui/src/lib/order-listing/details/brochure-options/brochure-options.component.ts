import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ActionTypes,
  MarketingOrder,
  Option,
  OptionType,
  SELECTEDORDER,
  WorkflowAction,
  APP_OPTIONS,
  LoadOptions,
  UpdateOrderPartial,ProductInstanceForm
} from '@lc/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { CardOption } from '@lc/shared-components-ui';
import { map, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'lc-brochure-options',
  templateUrl: './brochure-options.component.html',
  styleUrls: ['./brochure-options.component.scss']
})
export class BrochureOptionsComponent implements OnInit {
  form: ProductInstanceForm;

  marketingOrder$: Observable<MarketingOrder>;
  paperSizeOptions$: Observable<CardOption<Option>[]>;
  paperTypeOptions$: Observable<CardOption<Option>[]>;

  params: ParamMap;

  constructor(private activatedRoute: ActivatedRoute,
              private store: Store<any>) {
    this.store.dispatch(new LoadOptions());
    this.marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => order != null),
      tap(order => this.initializeForm(order))
    );

    this.paperSizeOptions$ = this.getOptions(OptionType.paperSize);
    this.paperTypeOptions$ = this.getOptions(OptionType.paperType);
  }

  ngOnInit() {
    this.params = this.activatedRoute.parent.snapshot.paramMap;
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(marketingOrder: MarketingOrder) {
    this.form.markAllAsDirty();
    if(!this.form.valid) { return; }

    const brochureProduct = marketingOrder.getBrochureProduct();
    const formValue = this.form.value;
    Object.assign(brochureProduct, formValue);

    this.store.dispatch(new UpdateOrderPartial(marketingOrder, ['availablePackages']));

    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true },
      this.params));
  }

  /**
   * Queries the options for a specific OptionType
   * @param type the optionType to filter the options on
   */
  private getOptions(type: OptionType): Observable<CardOption<Option>[]> {
    return this.store.select<Option[]>(APP_OPTIONS).pipe(
      filter(options => options != null),
      map(options => options.filter(o => o.type === type)),
      map(options => options.sort((a, b) => (a.order - b.order))),
      map(options => options.map(option => this.toDisplayOption(option)))
    );
  }

  /**
   * Initializes the ProductInstance form from the Brochure product
   * @param order The marketing order being created
   */
  private initializeForm(order: MarketingOrder) {
    const brochure = order.getBrochureProduct();
    this.form = new ProductInstanceForm(brochure);
  }

  /**
   * Converts the Options to a CardOption to be used in the display
   * @param option The option to convert
   */
  private toDisplayOption(option: Option): CardOption<Option> {
    const cardOption = new CardOption(option, option.description, option.selected);
    if(option.assets){
      cardOption.imageSource = option.assets.unselected,
      cardOption.selectedImageSource = option.assets.selected;
    }
    return cardOption;
  }
}
