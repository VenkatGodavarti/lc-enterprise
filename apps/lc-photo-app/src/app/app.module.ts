import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { RouterModule } from '@angular/router';
import { AuthGuard, CoreModule, StateModule } from '@lc/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutWithoutSidenavComponent, SharedComponentsLayoutModule } from '@lc/shared-components-layout';
import { FileDropModule } from 'ngx-file-drop';
import { InactivityLogoutComponent, SharedComponentsAuthModule } from '@lc/shared-components-auth';
import { LogoutComponent } from '@lc/shared-components-auth';
import { OktaCallbackComponent } from '@lc/shared-components-auth';
import { OktaLoginComponent } from '@lc/shared-components-auth';
import { ImpersonatePhotographerComponent } from './impersonate-photographer/impersonate-photographer.component';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { ManagePhotosComponent } from './manage-photos/manage-photos.component';
import { SharedComponentsMediaModule } from '@lc/shared-components-media';
import { PhotoOrdersResolve } from './photo-orders.resolve';
import { ManageMediaComponent } from './manage-media/manage-media.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { ManageMultimediaComponent } from './manage-media/manage-multimedia/manage-multimedia.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserIdleModule } from 'angular-user-idle';

@NgModule({
  declarations: [
    AppComponent,
    ImpersonatePhotographerComponent,
    AppointmentListComponent,
    ManagePhotosComponent,
    ManageMediaComponent,
    ManageMultimediaComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    CoreModule.forRoot(),
    StateModule,
    FileDropModule,
    NgbModule,
    NxModule.forRoot(),
    SharedComponentsAuthModule,
    SharedComponentsLayoutModule,
    SharedComponentsMediaModule,
    SharedComponentsUiModule,
    RouterModule.forRoot([
        {
          path: '',
          component: LayoutWithoutSidenavComponent,
          children: [
            { path: '', component: ImpersonatePhotographerComponent, canActivate: [AuthGuard] },
            { path: 'appointment-list/:photographerId',
              component: AppointmentListComponent,
              resolve: {
                photoOrder: PhotoOrdersResolve
              },
              canActivate: [AuthGuard] },
            { path: 'manage-media/:photographerId/:photoOrderId',
              component: ManageMediaComponent,
              resolve: {
                photoOrder: PhotoOrdersResolve
              },
              canActivate: [AuthGuard],
              data: {navLinks: [
                  {
                    label: 'Photos',
                    path: 'photos',
                    id: 'photos',
                    index: 0
                  }, {
                    label: 'Multimedia',
                    path: 'multimedia',
                    id: 'multimedia',
                    index: 1
                  },
                ],
                activeId: 'photos'},
              children: [
                { path: '', redirectTo: 'photos', pathMatch: 'full' },
                { path: 'photos', component: ManagePhotosComponent, canActivate: [AuthGuard] },
                { path: 'multimedia', component: ManageMultimediaComponent, canActivate: [AuthGuard] },
              ],
            },
            // { path: 'manage-photos/:photographerId/:photoOrderId',
            //   component: ManagePhotosComponent,
            //   canActivate: [AuthGuard],
            //
            // }
          ]
        },
        {
          path: '',
          component: LayoutWithoutSidenavComponent,
          children: [
            // { path: 'upload-photos/:pid/:moid', canActivate: [AuthGuard] }
            { path: 'okta/login', component: OktaLoginComponent },
            { path: 'implicit/callback', component: OktaCallbackComponent },
            { path: 'inactivity-logout', component: InactivityLogoutComponent },
            { path: 'logout', component: LogoutComponent }
          ]
        }
      ],
      { initialNavigation: 'enabled' , enableTracing: false }
    ),

    // User activity timeouts (in seconds)
    // Ping value is not used
    UserIdleModule.forRoot({idle: 5 * 60, timeout: 30, ping: 10000})

  ],
  providers: [
    PhotoOrdersResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
