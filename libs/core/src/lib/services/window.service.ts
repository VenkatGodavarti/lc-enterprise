import { Injectable } from '@angular/core';

const windowRef = window;

@Injectable()
export class WindowService {

  constructor() {
  }

  public open ( url: string, target?: string ) {
    windowRef.open(url, target);
  }
}
