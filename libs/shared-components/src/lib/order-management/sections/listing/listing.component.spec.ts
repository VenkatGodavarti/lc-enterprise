import { ComponentFixture, fakeAsync, tick, TestBed } from '@angular/core/testing';
import { Component, ViewChild, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";

import { ListingFactory, MarketingOrderFactory }  from '../../../../../../core/src/lib/factories';
import { ListingComponent } from './listing.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ThumbnailSizes, Listing, MarketingOrder } from '@lc/core';
import { By } from '@angular/platform-browser';

describe('ListingDetailsCardComponent', () => {

  describe('All Data Present', () => {
    const factory = new ListingFactory();
    let listing: Listing;
    let marketingOrder: MarketingOrder;

    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;

    beforeEach(fakeAsync(() => {
      configureTestModule();

      fixture = TestBed.createComponent(TestComponent);
      component = fixture.componentInstance;

      listing = factory.createDefaultListing();
      marketingOrder = MarketingOrderFactory.createMarketingOrder("123", listing, null, true);

      listing.listPrice = 333333;

      component.listingDetailsCard.marketingOrder = marketingOrder;

      fixture.detectChanges();
      tick(200);
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have first listing photo as backgroundImage ', () => {
      // Subscribes but only receives first change
      expect(component.listingDetailsCard.backgroundImage).toEqual(marketingOrder.photos[0].thumbnails[ThumbnailSizes.MEDIUM_THUMBNAIL_WIDTH].uri);
    });

    it('should have listingPrice equal listing.listPrice', () => {
      const listPrice = fixture.debugElement.query(By.css('#lc-listing-list-price'));
      expect(listPrice.nativeElement.textContent).toEqual('$333,333');
    });
  });


  describe('Use Default Values', () => {
    const factory = new ListingFactory();
    let marketingOrder: MarketingOrder;

    let component: TestComponent;
    let fixture: ComponentFixture<TestComponent>;

    beforeEach(fakeAsync(() => {
      configureTestModule();

      fixture = TestBed.createComponent(TestComponent);
      component = fixture.componentInstance;

      marketingOrder = new MarketingOrder();
      marketingOrder.listing = factory.createDefaultListing();
      marketingOrder.listing.listPrice = null;
      marketingOrder.photos = [];

      component.listingDetailsCard.marketingOrder = marketingOrder;
      fixture.detectChanges();
      tick(200);
    }));

    it('should have default image as backgroundImage ', () => {
      expect(component.listingDetailsCard.backgroundImage).toBe(component.listingDetailsCard.defaultImage);
    });

    it('should have listingPrice equal Price Not Set', () => {
      const listPrice = fixture.debugElement.query(By.css('#lc-listing-list-price'));
      expect(listPrice.nativeElement.textContent).toEqual('Price Not Set');
    });
  });


  async function configureTestModule() {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        TestComponent,
        ListingComponent,
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();

  }
});



/**
 * A simple component used to host the ListingComponent during tests so that web can set its Input listing value
 * properly.
 *
 */
@Component({
  selector: 'lc-test',
  template: '<lc-listing></lc-listing>'
})
class TestComponent {
  @ViewChild(ListingComponent)
  public listingDetailsCard: ListingComponent;

}
