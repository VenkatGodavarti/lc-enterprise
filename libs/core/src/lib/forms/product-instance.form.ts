import { FormControl, Validators } from '@angular/forms';
import { BaseForm } from './base.form';
import { ProductInstance } from '../models/product-instance.model';

export class ProductInstanceForm extends BaseForm {
  constructor(productInstance?: ProductInstance) {
    super({
      pageSizeChoice: new FormControl(null, [Validators.required]),
      paperTypeChoice: new FormControl(null, [Validators.required])
    });

    if(productInstance){
      this.patchValue(productInstance);
    }
  }
}
