@photo-management
@login-before-all
Feature: Manage favorite photos

  Background:
    Given I am logged in to Listing Concierge
    Given the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |
      | Status               | PENDING               |

    And the order has "8" photos
    And I go to the Photos page in Order Management
    And The "Number of Favorites" is greater than "0"

  Scenario: When I click my favorites filter I see just my favorite images
    When I click on the Favorites button
    Then I should see only my favorite photos

  Scenario: When I choose my favorites filter and then I see just my favorite images
    When I click on the Favorites button
    And I click on the Favorites button
    Then I should see all photos

  Scenario: Toggle favorite photos and select all
    When I click on the Favorites button
    And I check the "Select All" checkbox
    Then I should see all favorite photos selected

  Scenario: Select all photos
    When I check the "Select All" checkbox
    Then I should see all photos selected

  Scenario: Switch from Favorite photos to All photos after Select All
    When I click on the Favorites button
    And I check the "Select All" checkbox
    And I click on the Favorites button
    Then There should be "0" photos selected

  Scenario: Change photos arrangement using drag-n-drop
    When I move photo from position "4" to "1"
    Then I see the "4" photo in the "1" place
    And I see the "1" photo in the "next" place

#  # Currently only works when 'original' value is greater than 'destination' because of using dragula
#  Scenario Outline: Change photos arrangement using drag-n-drop
#    When I move photo from position "<original>" to "<destination>"
#    Then I see the "<original>" photo in the "<destination>" place
#    And I see the "<destination>" photo in the "<shift>" place
#    Examples:
#      | original | destination | shift
#      | 4        | 1           | next
#      | 3        | 1           | next
#      | 2        | 1           | next
#
