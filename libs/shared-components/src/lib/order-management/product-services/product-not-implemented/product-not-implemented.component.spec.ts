import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductNotImplementedComponent } from './product-not-implemented.component';

describe('ProductNotImplementedComponent', () => {
  let component: ProductNotImplementedComponent;
  let fixture: ComponentFixture<ProductNotImplementedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductNotImplementedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNotImplementedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
