import { AbstractControl } from '@angular/forms';

export class UriValidators {
  static httpsRegex = new RegExp('^https://');
  static urlRegex = /^https:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;

  static validateSecureUri(control: AbstractControl) {

    const candidate = control.value;
    if (candidate === null) {
      return null;
    }

    if (!UriValidators.urlRegex.test(candidate)) {
      const result: any =  { url: true };
      if(!UriValidators.httpsRegex.test(candidate)) {
        result.requireHttps = true;
      }
      return result;
    }
    return null;
  }

}
