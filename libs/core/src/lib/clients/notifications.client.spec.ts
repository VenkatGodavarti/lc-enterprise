import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiClient } from './api.client';
import { FormGroup, FormControl } from '@angular/forms';
import { NotificationsClient } from './notifications.client';
import { Notification, NotificationType } from '../models/notification.model';
import { environment } from '../../../../environments/environment';
import { NotificationForm } from '../forms';
import { NotificationFactory } from '../factories/notification.factory';

describe('NotificationsClient', () => {

  let testController: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      HttpClientTestingModule
    ],
    providers: [
      ApiClient,
      NotificationsClient
    ]
  }));

  beforeEach(() => {
    testController = TestBed.get(HttpTestingController);
  })

  afterEach(() => {
    testController.verify();
  });

  it('should be created', () => {
    const service: NotificationsClient = TestBed.get(NotificationsClient);
    expect(service).toBeTruthy();
  });

  it('get should call GET method with proper url', () => {
    const expectedUrl = `${environment.notifyBaseURL}notifications`;
    const client: NotificationsClient = TestBed.get(NotificationsClient);
    client.get().subscribe(() => {});

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('GET');
  });

  it('getById should call GET method with proper url', () => {
    const id = '123';
    const expectedUrl = `${environment.notifyBaseURL}notifications/${id}`;
    const client: NotificationsClient = TestBed.get(NotificationsClient);
    client.getById(id).subscribe(() => {});

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('GET');
  });

  it('getById should call GET method with proper url', () => {
    const id = '123';
    const expectedUrl = `${environment.notifyBaseURL}notifications/${id}`;
    const client: NotificationsClient = TestBed.get(NotificationsClient);
    client.getById(id).subscribe(() => {});

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('GET');
  });

  it('post should call POST method with proper url and return the created notification', () => {
    const expectedUrl = `${environment.notifyBaseURL}notifications`;
    const client: NotificationsClient = TestBed.get(NotificationsClient);

    const validNotification = NotificationFactory.assignedToOrder;
    const form = new NotificationForm(validNotification);
    client.post(form).subscribe((created) => {
      expect(created.subject).toBe(validNotification.subject);
    });

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('POST');
    request.flush(validNotification);
  });

  it('put should call PUT method with proper url and return the created notification', () => {

    const validNotification: Notification = NotificationFactory.assignedToOrder;
    const id = validNotification._id;
    const expectedUrl = `${environment.notifyBaseURL}notifications/${id}`;
    const client: NotificationsClient = TestBed.get(NotificationsClient);

    const form = new NotificationForm(validNotification);
    client.put(form).subscribe((created) => {
      expect(created.subject).toBe(validNotification.subject);
    });

    const request = testController.expectOne(expectedUrl);
    expect(request.request.method).toBe('PUT');
    request.flush(validNotification);
  });
});
