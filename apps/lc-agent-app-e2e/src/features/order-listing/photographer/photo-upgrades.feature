@order-listing
@photography-upgrades
@login-before-all
Feature: Photo upgrades
  Background: 
    Given I am logged in to Listing Concierge
    Given the following orders exist
      | Name                 | Alamo                 |
      | Package              | Gold                  |

    Scenario: view unselected upgrades
      When I select the Alamo order
      And the order is on the photographer-upgrades step
      And I go to the Overview Photography page in Order Listing
      Then the Overview Photography page from Order Listing is loaded
      When I click on the Professional Photography Continue button
      And the Select Photographer page from Order Listing is loaded
      When I wait for the Cutting Edge button to load
      And I click on the Cutting Edge button
      Then the "Next" button is enabled
      When I click on the Next button
      Then the Select Photography Upgrades page from Order Listing is loaded
      When I wait for the Next button to load
      Then the "Header" text exists
      Then the "Upgrades Available" element exists
      And the "Upgrades Available" text is "You can pick 1 more upgrade(s)"

    Scenario: Select upgrades
      When I select the Alamo order
      And the order is on the photographer-upgrades step
      And I go to the Overview Photography page in Order Listing
      Then the Overview Photography page from Order Listing is loaded
      When I click on the Professional Photography Continue button
      And the Select Photographer page from Order Listing is loaded
      When I wait for the Cutting Edge button to load
      And I click on the Cutting Edge button
      Then the "Next" button is enabled
      When I click on the Next button
      Then the Select Photography Upgrades page from Order Listing is loaded
      When I wait for the Next button to load
      Then the "Header" text exists
      Then the "Upgrades Available" element exists
      And the "Upgrades Available" text is "You can pick 1 more upgrade(s)"
      And I click on the "Showcase Upgrade" button
      Then the "Upgrades Available" text is "You can pick 0 more upgrade(s)"
