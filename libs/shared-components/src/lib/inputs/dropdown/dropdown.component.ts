import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { InputField } from '../input-field';
import { DomSanitizer } from '@angular/platform-browser';
import { Mask } from '../input-masks';


@Component({
  selector: 'lc-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true,
    }
  ]
})
export class DropdownComponent extends InputField {

  @Input()
  label: string;

  @Input()
  placeholder = '';

  @Input()
  emptyOptionDisplay: string;

  @Input()
  emptyOptionValue: string;

  @Input()
  hint: string;

  @Input()
  displayPath: string;

  @Input()
  valuePath: string;

  @Input()
  options: any[] = [];

  @Output()
  public readonly blur = new EventEmitter<void>();

  constructor(sanitizer: DomSanitizer) {
    super(sanitizer);
  }

  public onBlur() {
    this.blur.emit();
    this.executeOnTouched();
  }
}
