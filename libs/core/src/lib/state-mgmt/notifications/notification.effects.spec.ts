import { TestBed, inject, fakeAsync } from '@angular/core/testing';
import { TestingModule, StoreMock } from '@lc/testing';
import { Store } from '@ngrx/store';
import { NotificationFactory } from '../../factories/notification.factory';
import { Notification } from '../../models';
import { Observable, throwError, of } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { NotificationEffects } from './notification.effects';
import { NotificationsClient } from '../../clients/notifications.client';
import { GlobalErrorHandler, NOTIFICATIONS, CreateNotification } from '@lc/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoadNotifications, UpdateNotification } from './notification.actions';
import { hot } from 'jasmine-marbles';
import { NotificationForm } from '../../forms/notification.form';
import { FormControl } from '@angular/forms';

describe('NotificationEffects', () => {
  let store: StoreMock<any>;
  let expectedNotifications: Notification[];
  let actions$: Observable<any>;
  let effects: NotificationEffects;

  let mockedErrorHandler: jasmine.SpyObj<GlobalErrorHandler>;
  let mockedNotificationClient: jasmine.SpyObj<NotificationsClient>;

  beforeEach(() => {

    mockedErrorHandler = jasmine.createSpyObj<GlobalErrorHandler>(['handleError']);
    mockedNotificationClient = jasmine.createSpyObj<NotificationsClient>(['get', 'post', 'put']);

    TestBed.configureTestingModule({
    imports: [TestingModule, HttpClientTestingModule],
    providers: [
      NotificationEffects,
      { provide: NotificationsClient, useValue: mockedNotificationClient },
      { provide: GlobalErrorHandler, useValue: mockedErrorHandler },
      provideMockActions(() => actions$),
    ]
    });
  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    expectedNotifications = NotificationFactory.createAll();
    store.setState(NOTIFICATIONS, expectedNotifications); // set default state
  }));


  it('should be created', () => {
    effects = TestBed.get(NotificationEffects);
    expect(effects).toBeTruthy();
  });

  describe('#LoadNotifications', () => {
    beforeEach(() => {
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new LoadNotifications() });
    });

    it('should return notifications', fakeAsync(() => {
      mockedNotificationClient.get.and.callFake(() => of(expectedNotifications));

      effects = TestBed.get(NotificationEffects);
      effects.loadNotifications.subscribe(response => {
        expect(response.length).toBe(expectedNotifications.length);
        expect(mockedErrorHandler.handleError).not.toHaveBeenCalled();
      });
    }));

    it('should handle errors', fakeAsync(() => {
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new LoadNotifications() });
      mockedNotificationClient.get.and.callFake(() => throwError('Testing errors'));

      effects = TestBed.get(NotificationEffects);
      effects.loadNotifications.subscribe(response => {
        expect(response.length).toBe(0);
        expect(mockedErrorHandler.handleError).toHaveBeenCalled();
      });
    }));
  });

  describe('#CreateNotifications', () => {
    beforeEach(() => {
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new CreateNotification(new NotificationForm(new Notification({_id: 123}))) });
    });

    it('should create notification and adds notification', fakeAsync(() => {
      mockedNotificationClient.post.and.callFake((form: FormControl) => of(form.value));

      effects = TestBed.get(NotificationEffects);

      const expectedCount = expectedNotifications.length + 1;
      effects.createNotification.subscribe(response => {
        expect(response.length).toBe(expectedCount);
        expect(mockedErrorHandler.handleError).not.toHaveBeenCalled();
      });
    }));

    it('should handle errors', fakeAsync(() => {
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new LoadNotifications() });
      mockedNotificationClient.post.and.callFake(() => throwError('Testing errors'));

      effects = TestBed.get(NotificationEffects);
      effects.createNotification.subscribe(response => {
        expect(response.length).toBe(0);
        expect(mockedErrorHandler.handleError).toHaveBeenCalled();
      });
    }));
  });

  describe('#UpdateNotifications', () => {
    let notificationToUpdate: Notification;
    beforeEach(() => {
      notificationToUpdate = expectedNotifications[0];
      notificationToUpdate.subject = 'Testing';
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new UpdateNotification(new NotificationForm(expectedNotifications[0])) });
    });

    it('should update notification and update value', fakeAsync(() => {
      mockedNotificationClient.put.and.callFake((form: FormControl) => of(notificationToUpdate));

      effects = TestBed.get(NotificationEffects);

      const expectedCount = expectedNotifications.length;
      effects.updateNotification.subscribe(response => {
        expect(response.length).toBe(expectedCount);
        const updatedNotification = response.find(not => not._id === notificationToUpdate._id);
        expect(updatedNotification.subject).toBe(notificationToUpdate.subject);
        expect(mockedErrorHandler.handleError).not.toHaveBeenCalled();
      });
    }));

    it('should handle errors', fakeAsync(() => {
      // Refer here for mocking actions: https://github.com/ngrx/platform/blob/master/docs/effects/testing.md
      actions$ = hot('--a-', { a: new LoadNotifications() });
      mockedNotificationClient.put.and.callFake(() => throwError('Testing errors'));

      effects = TestBed.get(NotificationEffects);
      effects.updateNotification.subscribe(response => {
        expect(response.length).toBe(0);
        expect(mockedErrorHandler.handleError).toHaveBeenCalled();
      });
    }));
  });
});
