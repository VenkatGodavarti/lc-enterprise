(function ($) {
$(document).ready(function() {
  if (Drupal.ajax) {
    // Add a new ajax command that will change the location of the current page.
	Drupal.ajax.prototype.commands.buttonFieldLocation = function(ajax, response, status) {
	  location.href = response.url;
	};
  }
});
})(jQuery);
;/*})'"*/
;/*})'"*/
/**
 * @file
 */

(function ($) {

  'use strict';

  Drupal.extlink = Drupal.extlink || {};

  Drupal.extlink.attach = function (context, settings) {
    if (!settings.hasOwnProperty('extlink')) {
      return;
    }

    // Strip the host name down, removing ports, subdomains, or www.
    var pattern = /^(([^\/:]+?\.)*)([^\.:]{1,})((\.[a-z0-9]{1,253})*)(:[0-9]{1,5})?$/;
    var host = window.location.host.replace(pattern, '$2$3');
    var subdomain = window.location.host.replace(host, '');

    // Determine what subdomains are considered internal.
    var subdomains;
    if (settings.extlink.extSubdomains) {
      subdomains = '([^/]*\\.)?';
    }
    else if (subdomain === 'www.' || subdomain === '') {
      subdomains = '(www\\.)?';
    }
    else {
      subdomains = subdomain.replace('.', '\\.');
    }

    // Build regular expressions that define an internal link.
    var internal_link = new RegExp('^https?://([^@]*@)?' + subdomains + host, 'i');

    // Extra internal link matching.
    var extInclude = false;
    if (settings.extlink.extInclude) {
      extInclude = new RegExp(settings.extlink.extInclude.replace(/\\/, '\\'), 'i');
    }

    // Extra external link matching.
    var extExclude = false;
    if (settings.extlink.extExclude) {
      extExclude = new RegExp(settings.extlink.extExclude.replace(/\\/, '\\'), 'i');
    }

    // Extra external link CSS selector exclusion.
    var extCssExclude = false;
    if (settings.extlink.extCssExclude) {
      extCssExclude = settings.extlink.extCssExclude;
    }

    // Extra external link CSS selector explicit.
    var extCssExplicit = false;
    if (settings.extlink.extCssExplicit) {
      extCssExplicit = settings.extlink.extCssExplicit;
    }

    // Define the jQuery method (either 'append' or 'prepend') of placing the icon, defaults to 'append'.
    var extIconPlacement = settings.extlink.extIconPlacement || 'append';

    // Find all links which are NOT internal and begin with http as opposed
    // to ftp://, javascript:, etc. other kinds of links.
    // When operating on the 'this' variable, the host has been appended to
    // all links by the browser, even local ones.
    // In jQuery 1.1 and higher, we'd use a filter method here, but it is not
    // available in jQuery 1.0 (Drupal 5 default).
    var external_links = [];
    var mailto_links = [];
    $('a:not(.' + settings.extlink.extClass + ', .' + settings.extlink.mailtoClass + '), area:not(.' + settings.extlink.extClass + ', .' + settings.extlink.mailtoClass + ')', context).each(function (el) {
      try {
        var url = '';
        if (typeof this.href == 'string') {
          url = this.href.toLowerCase();
        }
        // Handle SVG links (xlink:href).
        else if (typeof this.href == 'object') {
          url = this.href.baseVal;
        }
        if (url.indexOf('http') === 0
          && ((!url.match(internal_link) && !(extExclude && url.match(extExclude))) || (extInclude && url.match(extInclude)))
          && !(extCssExclude && $(this).is(extCssExclude))
          && !(extCssExclude && $(this).parents(extCssExclude).length > 0)
          && !(extCssExplicit && $(this).parents(extCssExplicit).length < 1)) {
          external_links.push(this);
        }
        // Do not include area tags with begin with mailto: (this prohibits
        // icons from being added to image-maps).
        else if (this.tagName !== 'AREA'
          && url.indexOf('mailto:') === 0
          && !(extCssExclude && $(this).parents(extCssExclude).length > 0)
          && !(extCssExplicit && $(this).parents(extCssExplicit).length < 1)) {
          mailto_links.push(this);
        }
      }
      // IE7 throws errors often when dealing with irregular links, such as:
      // <a href="node/10"></a> Empty tags.
      // <a href="http://user:pass@example.com">example</a> User:pass syntax.
      catch (error) {
        return false;
      }
    });

    if (settings.extlink.extClass) {
      Drupal.extlink.applyClassAndSpan(external_links, settings.extlink.extClass, extIconPlacement);
    }

    if (settings.extlink.mailtoClass) {
      Drupal.extlink.applyClassAndSpan(mailto_links, settings.extlink.mailtoClass, extIconPlacement);
    }

    if (settings.extlink.extTarget) {
      // Apply the target attribute to all links.
      $(external_links).attr('target', settings.extlink.extTarget);
      // Add rel attributes noopener and noreferrer.
      $(external_links).attr('rel', function (i, val) {
        // If no rel attribute is present, create one with the values noopener and noreferrer.
        if (val == null) {
          return 'noopener noreferrer';
        }
        // Check to see if rel contains noopener or noreferrer. Add what doesn't exist.
        if (val.indexOf('noopener') > -1 || val.indexOf('noreferrer') > -1) {
          if (val.indexOf('noopener') === -1) {
            return val + ' noopener';
          }
          if (val.indexOf('noreferrer') === -1) {
            return val + ' noreferrer';
          }
          // Both noopener and noreferrer exist. Nothing needs to be added.
          else {
            return val;
          }
        }
        // Else, append noopener and noreferrer to val.
        else {
          return val + ' noopener noreferrer';
        }
      });
    }

    Drupal.extlink = Drupal.extlink || {};

    // Set up default click function for the external links popup. This should be
    // overridden by modules wanting to alter the popup.
    Drupal.extlink.popupClickHandler = Drupal.extlink.popupClickHandler || function () {
      if (settings.extlink.extAlert) {
        return confirm(settings.extlink.extAlertText);
      }
    };

    $(external_links).click(function (e) {
      return Drupal.extlink.popupClickHandler(e, this);
    });
  };

  /**
   * Apply a class and a trailing <span> to all links not containing images.
   *
   * @param {object[]} links
   *   An array of DOM elements representing the links.
   * @param {string} class_name
   *   The class to apply to the links.
   * @param {string} icon_placement
   *   'append' or 'prepend' the icon to the link.
   */
  Drupal.extlink.applyClassAndSpan = function (links, class_name, icon_placement) {
    var $links_to_process;
    if (Drupal.settings.extlink.extImgClass) {
      $links_to_process = $(links);
    }
    else {
      var links_with_images = $(links).find('img').parents('a');
      $links_to_process = $(links).not(links_with_images);
    }
    $links_to_process.addClass(class_name);
    var i;
    var length = $links_to_process.length;
    for (i = 0; i < length; i++) {
      var $link = $($links_to_process[i]);
      if ($link.css('display') === 'inline' || $link.css('display') === 'inline-block') {
        if (class_name === Drupal.settings.extlink.mailtoClass) {
          $link[icon_placement]('<span class="' + class_name + '" aria-label="' + Drupal.settings.extlink.mailtoLabel + '"></span>');
        }
        else {
          $link[icon_placement]('<span class="' + class_name + '" aria-label="' + Drupal.settings.extlink.extLabel + '"></span>');
        }
      }
    }
  };

  Drupal.behaviors.extlink = Drupal.behaviors.extlink || {};
  Drupal.behaviors.extlink.attach = function (context, settings) {
    // Backwards compatibility, for the benefit of modules overriding extlink
    // functionality by defining an "extlinkAttach" global function.
    if (typeof extlinkAttach === 'function') {
      extlinkAttach(context);
    }
    else {
      Drupal.extlink.attach(context, settings);
    }
  };

})(jQuery);

;/*})'"*/
;/*})'"*/

/**
 * Adds the autocomplete widget behavior to specified elements.
 *
 * This uses the jQuery UI Autocomplete widget included with Drupal core.
 */
Drupal.apachesolr_autocomplete = {
  processOne: function(key, settings, context) {
    // Look for items with the data-apachesolr-autocomplete-id attribute.
    var jquery_version = jQuery.fn.jquery.split('.');
    var apachesolr_autocomplete_selector = ((jquery_version[0] == 1 && jquery_version[1] >= 7) || jquery_version[0] > 1) ? 'ui-autocomplete' : 'autocomplete';
    var apachesolr_autocomplete_search = jQuery(".apachesolr-autocomplete[data-apachesolr-autocomplete-id='"+ key +"']", context);
    apachesolr_autocomplete_search.autocomplete({
        // TODO: source should be a function, which should add any client-side filters to autocomplete request.
        source: settings.path,
        // TODO: autocomplete select event should handle more actions on select: filling/submitting the textfield, jumping to URL... others?
        select: function(event, ui) {
            console.log("autocomplete.select: " + ui.item.value); //TODO: Debug
            // ui.item.label, ui.item.value, ui.item.count, etc.
            // Handle selection of an element in the autocomplete widget.
            jQuery(this).get(0).value = ui.item.value;
            // We should submit the widget's parent form.
            jQuery(this).closest("form").submit()
        },
        search: function(event, ui) {
            jQuery(this).addClass('throbbing');
        },
        // The following doesn't fire--why??
        response: function( event, ui ) {
            console.log('autoccomplete.response fired');
            jQuery(this).removeClass('throbbing');
        },
        open: function(event,ui) {
            jQuery(this).removeClass('throbbing');
        },
        minLength: 2,
        delay: 400
    })
    .addClass('form-autocomplete');
    if (apachesolr_autocomplete_search.data(apachesolr_autocomplete_selector)) {
      apachesolr_autocomplete_search.data(apachesolr_autocomplete_selector)._renderItem = Drupal.apachesolr_autocomplete.renderItem;
    }
  },
    // TODO: USe JS-side theming. See http://engineeredweb.com/blog/11/5/javascript-theme-functions-drupal/
  renderItem: function (ul, item) {
      var theme_function = 'apachesolr_autocomplete_default';
      if (item.theme) {
          theme_function = item.theme;
      }
      theme_function = theme_function;
      var html = Drupal.theme(theme_function, item);
      return jQuery("<li>")
          .data("item.autocomplete", item)
          .append(html)
          .appendTo(ul);
  }
};

/**
 * Implements the Drupal behavior functions that trigger on page load.
 *
 * Uses the JS Drupal.settings.apachesolr_autocomplete object set by the
 * apachesolr_autocomplete module to change any relevant form elements currently
 * on the page to be autocomplete-enabled.
 */
Drupal.behaviors.apachesolr_autocomplete = {
  attach: function(context) {
    // Check that settings exist.
    if (!Drupal.settings.apachesolr_autocomplete || !Drupal.settings.apachesolr_autocomplete.forms) {
      return;
    }
    // Cycle thru the settings array which contains:
    //   key => settingsArray
    // where
    //   key: the data-apachesolr-autocomplete-id HTML attribute to look for
    //        (which was added on the form_alter)
    //   settings: and
    jQuery.each(Drupal.settings.apachesolr_autocomplete.forms, function(key, settings) {
      Drupal.apachesolr_autocomplete.processOne(key, settings, context);
    });
  }
};

/**
 * Default theming function.
 * @param item
 *   The item object.
 * @returns {string}
 */
Drupal.theme.prototype.apachesolr_autocomplete_default = function(item) {
    return item.label;
};

;/*})'"*/
;/*})'"*/
/** 
 * Forward port jQuery.live()
 * Wrapper for newer jQuery.on()
 * Uses optimized selector context 
 * Only add if live() not already existing.
*/
if (typeof jQuery.fn.live == 'undefined' || !(jQuery.isFunction(jQuery.fn.live))) {
  jQuery.fn.extend({
      live: function (event, callback) {
         if (this.selector) {
              jQuery(document).on(event, this.selector, callback);
          }
      }
  });
}
;/*})'"*/
;/*})'"*/
/** 
 * Copyright  2017 Escalet, Inc - All Rights Reserved.
 * This file is part of the CB|One Listing Concierge project which is released and subject to the license terms at:
 * "/sites/all/modules/custom/cbone_listing_concierge_license.txt".
 */
 
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.cbone_admin = {
		attach: function(context, settings) {
			//show hide search field in mobile screen
			jQuery('.cbone-mobile-search-icon').click(function(e){
				jQuery(".cbone-mobile-search-field").toggle();
			});
			jQuery(".title-background-image").one('load',function(){
				//calculate title line height
				if(jQuery( window ).width() >767 && jQuery( window ).width() < 1951){
					var sub_section_title= jQuery(".section-basics-title .sub-section-title").height();// for multisection page
					var basic_page_title_height= jQuery(".node-type-page .detail-page-title").height();// for basic page
					var detail_page_title_height= jQuery(".node-type-details .detail-page-title").height();// for basic page
					jQuery(".section-basics-title .sub-section-title h3").css("height",sub_section_title+'px');
					jQuery(".header-title h3").css("height",basic_page_title_height+'px');
					jQuery(".header-title h3").css("height",detail_page_title_height+'px');
					jQuery(".node-type-multi-section-page .cbone-multi-section .sub-section-title h3").css("height",sub_section_title+'px');
				}
			}).each(function() {
				if(this.complete) $(this).load();
			});
			jQuery(window).resize(function(){
				if(jQuery( window ).width() >767 && jQuery( window ).width() < 1951){
					var title_height= jQuery(".section-basics-title .sub-section-title").height(); // for multisection page
					var basic_page_title_height= jQuery(".node-type-page .detail-page-title").height(); // for basic page
					var detail_page_title_height= jQuery(".node-type-details .detail-page-title").height();// for basic page
					jQuery(".section-basics-title .sub-section-title h3").css("height",title_height+'px');					
					jQuery(".header-title h3").css("height",basic_page_title_height+'px');
					jQuery(".header-title h3").css("height",detail_page_title_height+'px');
					jQuery(".node-type-multi-section-page .cbone-multi-section .sub-section-title h3").css("height",title_height+'px');
				}
			});
      //redirect the user to region page after select region from front-page url
			jQuery("#user-region-list").change(function() {
				var region = jQuery(this).val();
				location.href = Drupal.settings.basePath + "front-page?region="+region;
			});
		}
	};
})(jQuery, Drupal, this, this.document);

;/*})'"*/
;/*})'"*/
