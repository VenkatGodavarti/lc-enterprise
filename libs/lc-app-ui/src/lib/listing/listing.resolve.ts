import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import {
  Listing,
  UserService,
  GetOrder, MarketingOrder, LoadOrder
} from '@lc/core';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

// TODO - to be removed
@Injectable()
export class ListingResolve implements Resolve<Listing> {

  constructor(private userService: UserService,
              private store: Store<any>) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const id = route.params['id'];
    if (id) {
      this.store.dispatch(new GetOrder(new MarketingOrder({_id: id})));
    } else {
      const listing = Listing.createListing();
      this.store.dispatch(new LoadOrder(new MarketingOrder({_id: id, agentId: this.userService.getUserId(), listing: listing})));
    }

    // we are no using resolve injection of objects so just return null
    return of(null);
  }
}
