
import {Given, When, Then} from 'cucumber';
import { Context, ContextKeys } from '../support';
import { Configuration } from '../configuration';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Given(/^I am logged in to Listing Concierge$/,  async () => {
  expect(Context.get<boolean>(ContextKeys.loggedIn)).to.be.equal(true);
});

/*
When(/^I login in to Listing Concierge$/,  async () => {
  login(Configuration.username, Configuration.password)
});
*/
