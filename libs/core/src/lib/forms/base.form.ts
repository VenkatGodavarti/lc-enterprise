import { FormGroup, FormArray, AbstractControl } from '@angular/forms';

export class BaseForm<TModel = any> extends FormGroup {
  getDirty(): any {
    const dirty = {};
    Object.keys(this.controls).forEach(key => {
      const control = this.controls[key];
      if (!control.dirty) {
        return;
      }
      if(control instanceof BaseForm) {
        dirty[key] = control.getDirty();
      } else {
        dirty[key] = control.value;
      }
    });
    return dirty;
  }

  markAllAsDirty() {
    this.markAsDirty();
    Object.keys(this.controls).forEach(key => {
      const control = this.controls[key];
      if (control instanceof FormArray) {
        for(let form of control.controls){
          this.markAllControlsAsDirty(form);
        }
      } else {
        this.markAllControlsAsDirty(control);
      }
    })
  }

  private markAllControlsAsDirty(control: BaseForm | AbstractControl) {
    if(control instanceof BaseForm){
      control.markAllAsDirty();
    } else {
      control.markAsDirty();
      control.updateValueAndValidity();
    }
  }

  getControl(propertyName: keyof TModel): AbstractControl {
    return this.get(propertyName.toString());
  }
}
