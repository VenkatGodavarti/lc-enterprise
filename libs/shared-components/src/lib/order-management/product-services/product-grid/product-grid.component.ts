import { Component, OnInit, Input } from '@angular/core';
import * as kebabCase from 'lodash.kebabcase';
import { ProductInstance } from '@lc/core';

@Component({
  selector: 'lc-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.scss']
})
export class ProductGridComponent implements OnInit {

  @Input()
  products: ProductInstance[];

  @Input()
  pathPrefix = '../';

  displayProducts: object[];

  constructor() { }

  ngOnInit() {
    this.init(this.products);
  }

  private init(products) {

    this.displayProducts = products.map( (pr) => {return {
      title: pr.title,
      status: pr.status,
      thumbnailUri: pr.thumbnailUri,
      code: pr.code,
      routerLink: this.pathPrefix + kebabCase(pr.title) + '/'+pr.code
    }});
  }
}

