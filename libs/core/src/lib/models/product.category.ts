
export enum  ProductCategory {

  PRINT = 'print',
  DIGITAL = 'digital',
  MULTIMEDIA = 'multimedia',
  OTHER = 'other'
}

