import { Injectable } from '@angular/core';

/**
 * This service class is responsible for managing the error. It is not used for notifications to client
 * consumers. @see NotificationEventService for that purpose.
 *
 * This service may just log the error or it may send information to a API or service like Rollbar for error tracking
 * and crash reporting
 *
 * We assume this is a async service and it is a fire and forget type of usage.
 */
@Injectable()
export class ErrorService {

  constructor() {
  }

  handleError(error) {
    // TODO - replace with final logger
    console.error(error);
  }
}
