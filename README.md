# Listing Concierge V2

## Primary Angular UI for Agents
This Angular project contains multiple client-side applications used for Listing Concierge.

### **Preparing your environment**

* Install dependencies: `npm install`

* Modify the appropriate `environment.<env>.ts` file within the `apps/<appName>/src/environments` directory as needed to specify the client-side configuration. See apps/lc-agent-app/src/environments/environment.development.ts for example.

### **Running the applications:**
In this angular project, we will are using the default angular cli command to run the applications: `ng serve <appName> -c <configuration>`. The default application is lc-agent-app
  * **Mine:** (environment.mine.ts)
    * `ng serve <appName> -c mine`
  * **Development:** (environment.development.ts)
    * `ng serve <appName> -c development`
  * **QA:** (environment.qa.ts)
    * `ng serve <appName> -c qa`
  * **UAT:** (environment.uat.ts):
    * `ng serve <appName> -c uat`
  * **Production:** (environment.production.ts):
    * `ng serve <appName> -c production`

Open your browser:
  * **lc-agent-app:** [http://localhost:4200](http://localhost:4200)
  * **lc-coordinator-app:** [http://localhost:4700](http://localhost:4600)
  * **lc-photo-app:** [http://localhost:4700](http://localhost:4700)

**IMPORTANT!**
Make sure you use the configuration flag other wise the base environment.ts file will be used which will not contain the proper configuration properties.

**_Additional Notes_**:
  * Running without the appName, the default app "lc-agent-app" will be run.
  * If you want to access the app from another machine, include `--host 0.0.0.0`


### Build application and package for distribution
* Build the Angular application (transforms contents of apps and libs folders into the final application in the dist folder). The default angular build command should be used: `ng build <appName> -c <configuration>`. The default application is lc-agent-app
  * **Mine:** (environment.mine.ts)
    * `ng build <appName> -c mine`
  * **Development:** (environment.development.ts)
    * `ng build <appName> -c development`
  * **QA:** (environment.qa.ts)
    * `ng build <appName> -c qa`
  * **UAT:** (environment.uat.ts)
    * `ng build <appName> -c uat`
  * **Production:** (environment.production.ts)
    * `ng build <appName> -c production`

* If you want angular to re-build on the fly when files are modified, include the watch flag:
  `ng build -<appName> --configuration=development --watch`

## Setup Local Debugging with Webstorm

To run the server with debug, setup the following

1. Open Edit Configurations
2. Create a new `npm` configuration
3. Name it `lc-agent-app` or `lc-photo-app` based on what you want to run
4. package.json = the file located in the root of the `lc-enterprise` folder
5. Command = `run`
6. Scripts = `serve:lc-agent-app` or `serve:lc-photo-app`
7. Environment = `NODE_ENV=mine`
8. Click Apply
9. Create a new `Javascript Debug` configuration
10. Name = `lc-agent-app debug` or `lc-photo-app debug`
11. URL = `http://localhost:4200` for lc-agent-app or `http://localhost:4700` for lc-photo-app
12. Choose desired browser
13. Click OK

## Unit Testing

`npm run test` is used by the PR analysis and CD pipeline.  In both cases, it is a must pass activity.  For these
 checks to be of use the unit tests need to be stable.  This section is a living document on advice to help write
 and debug unit tests.

*NOTE:* Lint is run separately, as it is not required to pass and is provided as information to the team.  Keeping the
two separate allows for checking status of through exit codes.

###  The "An error was thrown in afterAll" Error

The "An error was thrown in afterAll" error is a problematic error as it does not point to a failing test.  The message
the developer gets is:
```
Chrome 64.0.3282 (Windows 10.0.0) ERROR
  {
    "message": "An error was thrown in afterAll\n[object ErrorEvent]",
    "str": "An error was thrown in afterAll\n[object ErrorEvent]"
  }
```

On research, the count of this error increases, as the number of Karma reporters increases.  Therefore, it can be
misleading when one error happens and there are multiple reporters.  Disabling reporters removes the error all together.

On a related note, this error has been associated with different version of reporters and jasmine.  More details here:
https://github.com/jasmine/jasmine/issues/1523

After ruling out the possibility of bad dependencies, the next option was to debug the issue.  This guidance comes from
the [a recommended process for fixing "An error was thrown in afterAll"]( https://github.com/jasmine/jasmine/issues/1523#issuecomment-452114706).
They used the browser devtools and console find and solve their problem.  To help the team easily get to these tools,
unit testing dev mode was added to the project.

### Unit Testing Dev Mode

To help the developers with unit test development, scripts have been added to the package.json.  These will run
specific projects
in dev mode.  This mode has only one reporter and is focused on giving the developer access to the devtools and console,
especially the console.  Unit test errors do not always show up in the test runner.  To see the full picture look at the
console.  The console should be free of errors.  To access this mode look for the scripts in package.json.

The dev mode scripts start with `"test:dev-` followed by the project name.  To target a specific Angular project, just
run the specific script for your project.  This puts the test runner in watch mode and keep the browser open, so the
developer can actively work on their unit tests.

### Additional Unit Test Reading

[Complete Guide to Angular Testing](https://christianlydemann.com/complete-guide-to-angular-testing/)

[Testing Observables in Angular](https://netbasal.com/testing-observables-in-angular-a2dbbfaf5329)

## End-to-End testing

End-to-End testing is currently in its first round of implementation. To run e2e tests in your local environment:

1. set LC_USERNAME and LC_PASSWORD environment variables with username and password of a test-user to login to LC during tests
2. Use the command `ng e2e lc-agent-app-e2e -c mine` to run the tests.

NOTE: If you run into the following error, do the following steps:

`Error message: Could not find update-config.json. Run 'webdriver-manager update' to download binaries.`

  1. In lc-enterprise folder, navigate to: node_modules/protractor/node_modules/webdriver-manager/bin
  2. Run the node command:
  `node webdriver-manager update`

CAUTION: Use a local mongo database and lcms-persist, or one of the test accounts, if you don't want your profile to be
over-written by these tests.

## Webpack Bundle Analyzer

WBA shows an interactive treemap visualization.  This allows the
developer to see what resides with each project's Javascript bundle, so
the developer can application load time.  The developer achieves this
by understanding what is taking up the most within a bundle.  This
helps them focus their optimization efforts.  To understand this process
better, recommend you read up on WBA at their
[GitHub page](https://github.com/webpack-contrib/webpack-bundle-analyzer).

### Running Webpack Bundle Analyzer

To run the analyzer, the app needs to be built in a certain manner.  The build needs to have a JSON stats file from the
NG build process.  The analyzer consumes the JSON stats and presents the results in a treemap visualization within
your default browser.  First you need to run the build command:

`npm run build:analyzer` to run all the analyzer specific builds

or run the individual builds:

`npm run build:analyzer:agent-app`,
`npm run build:analyzer:photo-app` or
`npm run build:analyzer:coordinator-app`

Then you need to run one or more of the analyzer commands:

`npm run analyzer:photo-app`,
`npm run analyzer:agent-app` or
`npm run analyzer:coordinator-app`

These commands will display the treemap.

Using Chrome's Lighthouse will help show performance issues related to size of the build artifacts.
The analyzer allows you to target areas in the app for improvements, like lazy loading and better
dependency loading.


## Cypress testing

Cypress tests use an integration configuration:

1. Launch the agent application: `npm run serve:lc-agent-app:integration`
2. Run the tests: `npm run test:lc-agent-app:integration:open`

(Replace above with `lc-coordinator-app`, `lc-photo-app`, etc. to test other apps)

If running on a different port use:

`export LC_AGENT_APP_BASE_URL=http://localhost:4200`

The integration build replaces the authentication service so that login is not required for tests

[Cypress Testing in CI](https://docs.cypress.io/guides/guides/continuous-integration.html#Setting-up-CI)
