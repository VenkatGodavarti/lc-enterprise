import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { EditToolbarComponent } from './edit-toolbar.component';
import { Listing, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { RouterTestingModule } from '@angular/router/testing';
import { TestingModule, StoreMock } from '@lc/testing';
import { Store } from '@ngrx/store';

describe('EditToolbarComponent', () => {
  let component: EditToolbarComponent;
  let fixture: ComponentFixture<EditToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule
      ],
      declarations: [ EditToolbarComponent ],
      providers: [
      ]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (store: StoreMock<any>) => {
    store.setState(SELECTEDORDER, new MarketingOrder()); // set default state
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
