import { OrderListingModule } from './order-listing.module';

describe('OrderListingModuleModule', () => {
  let orderListingModule: OrderListingModule;

  beforeEach(() => {
    orderListingModule = new OrderListingModule();
  });

  it('should create an instance', () => {
    expect(orderListingModule).toBeTruthy();
  });
});
