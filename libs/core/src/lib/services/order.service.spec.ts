import { TestBed } from '@angular/core/testing';

import { OrderService, OrderFilterTypes } from './order.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment';
import { AllOrders } from '../models';

describe('OrderService', () => {

  let httpMock: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  })

  it('should be created', () => {
    const service: OrderService = TestBed.get(OrderService);
    expect(service).toBeTruthy();
  });

  it('should return the total count and records', async() => {
    const service: OrderService = TestBed.get(OrderService);

    const mockRecords = new Array<AllOrders>(new AllOrders(), new AllOrders(), new AllOrders());
    service.getOrders(OrderFilterTypes.notMine).subscribe(response => {
      expect(response.orders.length).toBe(mockRecords.length);
      expect(response.totalCount).toBe(mockRecords.length);
    });

    const request = httpMock.expectOne(req => req.method === 'GET');
    request.flush(mockRecords, { headers: { 'total-count': `${mockRecords.length}` } });
    expect(service).toBeTruthy();
  });
});
