import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';

// definition class of the options that can be passed in
// TODO do we want to set defaults
class TextFitOptions {
  id: string;
  fontFamily: string;
  fontPointSize: number;
  fontStyle: string;
  height: number;
  width: number;
}

@Component({
  selector: 'lc-text-checker',
  templateUrl: './text-checker.component.html',
  styleUrls: ['./text-checker.component.scss']
})
export class TextCheckerComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChild('textChecker')
  textElement: ElementRef;

  @Input()
  metaData: TextFitOptions; // requirements from the product template.  Used to build the textarea.

  @Input()
  text: string;   // text the user enters that needs size verification

  height: number; // calculated height of the textarea
  width: number;  // calculated width of the textarea

  hasError = false; // boolean that is set depending on if the user input will fit

  constructor() { }

  ngOnInit(): void {
    // TODO when we start testing on better than HD displays that report a higher DPI than 96 we need to add a scale to the below calc

    // sets the height and with in pixels for the textarea used to measure the user input
    this.height = Math.ceil(this.metaData.height * 96);
    this.width = Math.ceil(this.metaData.width * 96);
  }

  /**
   * When NgModel changes execute a new check
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['text']) {
      this.onTextChanged();
    }
  }

  /**
   * Runs a fit test on the initial text value when component created
   */
  ngAfterViewInit(): void {
    if(this.metaData) {
      // timeout needed so function is run after everything is finished rendering
      setTimeout(() => {
        this.onTextChanged();
      }, 0);
    }
  }

  /**
   * Determines if the new text changes will fit in the template
   */
  onTextChanged() {
    if(this.metaData && this.height) {
      const element = this.textElement.nativeElement;
      const lineHeight = this.getLineHeight(element);
      this.hasError = element.scrollHeight > (this.height + lineHeight);
    }
  }

  private getLineHeight(element) {
    const style = <string>getComputedStyle(element)['line-height'];

    if(style && style.indexOf('px') >= 0) {  // ex. 21.56px
      return parseFloat(style);
    }

    if(style && style.indexOf('pt') >= 0) {  // ex. 22pt
      // convert to pixels
      return (parseFloat(style) * 1.333);
    }

    // May need to determine other values we might get from other browsers down
    // the road.  So far every browser utilizes a string ending in 'px'.
    return 0;
  }
}
