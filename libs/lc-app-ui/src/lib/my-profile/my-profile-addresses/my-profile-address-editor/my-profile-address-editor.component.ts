import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {
  CardContactInfo,
  AddressInformation,
  AddressInformationForm,
  ContactInfo,
  ContactType,
  Profile, ProfileActionTypes,
  SELECTEDPROFILE,
  UpdateProfile, UpdateProfileComplete,
  AddressType
} from '@lc/core';
import { ActionsSubject, Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { ofType } from '@ngrx/effects';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'lc-my-profile-address-editor',
  templateUrl: './my-profile-address-editor.component.html',
  styleUrls: ['./my-profile-address-editor.component.scss']
})
export class MyProfileAddressEditorComponent implements OnInit, OnDestroy {

  private profile$: Observable<Profile>;
  private profileSub: Subscription;
  public profile: Profile;

  private updatedSub: Subscription;

  public addressType: AddressType;

  private contact: ContactInfo;
  private address: AddressInformation;

  addressForm: AddressInformationForm;

  submitted = false;

  constructor(private store: Store<any>,
              private activatedRoute: ActivatedRoute,
              private actionSubject: ActionsSubject,
              private router: Router) {
    this.profile$ = store.select(SELECTEDPROFILE);
  }

  ngOnInit() {
    const addressType = this.activatedRoute.snapshot.paramMap.get('addressType') || '';
    this.addressType = AddressType[addressType.toUpperCase()];

    this.profileSub = this.profile$.subscribe( (profile: Profile) => {


      this.profile = profile;

      const addresses = this.profile.contactInfo.filter( ci => ci.type === ContactType.card);
      if (addresses && addresses.length) {
        this.contact = addresses.find(ci => ci.address.type.toString() === this.addressType);

        this.address = (this.contact) ? this.contact.address :  null;
      }
      this.initForm(this.address);

    });


    this.updatedSub = this.actionSubject.pipe(
      ofType<UpdateProfileComplete>(ProfileActionTypes.UpdateProfileComplete),
      filter(data => this.submitted && this.profile && this.profile._id === data.payload._id) // Only trigger after profile has been set and the ids match
    ).subscribe(() => {

      this.router.navigate(['/my-profile/addresses']);
      this.submitted = false;
    });

  }

  ngOnDestroy() {
    this.profileSub.unsubscribe();
    this.updatedSub.unsubscribe();
  }

  onCancel() {
    return this.router.navigate(['/my-profile/addresses']);
  }

  onSave() {

    if (!this.address) {
      this.address = new AddressInformation();
      this.address.type = this.addressType;

      this.contact = new CardContactInfo();
      this.contact.address = this.address;
      this.contact.type = ContactType.card;

      this.profile.contactInfo.push(this.contact);
    }
    this.address.deserialize(this.addressForm.value);

    this.submitted = true;
    this.store.dispatch( new UpdateProfile(this.profile, ['contactInfo']));

  }

  private initForm(addressInformation: AddressInformation) {

    if (this.addressForm && addressInformation) {
      this.addressForm.patchValue(addressInformation);
    } else {
      this.addressForm = new AddressInformationForm(addressInformation);
    }

    this.addressForm.controls['type'].setValue(this.addressType);
  }
}
