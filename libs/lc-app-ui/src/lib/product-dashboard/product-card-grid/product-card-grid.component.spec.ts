import { ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Angulartics2Module} from 'angulartics2';
import {Angulartics2GoogleAnalytics} from 'angulartics2/ga';

import {ProductCardComponent} from '../product-card/product-card.component';
import {ProductCardGridComponent} from './product-card-grid.component';
import {ApiService, UserService} from '@lc/core';

describe('ListingCardGridComponent', () => {
  let component: ProductCardGridComponent;
  let fixture: ComponentFixture<ProductCardGridComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        Angulartics2Module.forRoot([Angulartics2GoogleAnalytics])
      ],
      declarations: [
        ProductCardComponent,
        ProductCardGridComponent
      ],
      providers: [
        ApiService,
        UserService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductCardGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
