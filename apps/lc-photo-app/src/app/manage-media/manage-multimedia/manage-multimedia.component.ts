import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import {
  AddPhotoOrderMultimediaLink, LOGGEDINUSER,
  MultiMediaItem, PhotoOrder, PhotoOrderSelected, selectCurrentOrder
} from '@lc/core';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { MultiMediaItemForm } from '../../../../../../libs/shared-components/src/lib/media/multi-media-item-form/multi-media-item.form';
import { ActivatedRoute } from '@angular/router';
import { MultiMediaItemRequest } from '../../../../../../libs/core/src/lib/models/multi-media-item.model';
import { FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'lc-manage-multimedia',
  templateUrl: './manage-multimedia.component.html'
})
export class ManageMultimediaComponent implements OnInit, OnDestroy {

  addFormGroup = new MultiMediaItemForm();

  // Angular FormDirective needs to be reset after submission so that the error validations are removed.
  // See: https://github.com/angular/components/issues/4190
  @ViewChild(FormGroupDirective) formDirective: FormGroupDirective;

  subscriptions: Subscription[] = [];

  photoOrder$: Observable<PhotoOrder>;

  userId$: Observable<string>;
  userId = null;

  userFullName$: Observable<string>;
  userFullName = 'First Last';

  constructor(private route: ActivatedRoute,
              private store: Store<any>) {

    this.photoOrder$ = this.store.pipe(select<PhotoOrder>(selectCurrentOrder));

    // TODO - abstract code to get user name
    this.userFullName$ = this.store.select(LOGGEDINUSER).pipe( map( user => {
      if (user.profile.preferredFirstName && user.profile.preferredLastName) {
        return `${user.profile.preferredFirstName}  ${user.profile.preferredLastName}`;
      }
      return `${user.firstName}  ${user.lastName}`;
    }));
    this.userId$ = this.store.select(LOGGEDINUSER).pipe( map( user => {
      // use id and not names. It must be traceable via audit to a user and profile
      return `${user._id}`;
    }));
  }

  ngOnInit() {
    const id = this.route.parent.snapshot.paramMap.get('photoOrderId');
    this.store.dispatch(new PhotoOrderSelected(id));

    this.subscriptions.push(this.userId$.subscribe( id => {
      this.userId = id;
    }));

    this.subscriptions.push(this.userFullName$.subscribe( fullName => {
      this.userFullName = fullName;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.subscriptions = [];
  }

  addLink(order: PhotoOrder) {
    // prepare add link request
    // @see photo-order.effects.ts and photo-order.reducer.ts
    const request = new MultiMediaItemRequest(this.addFormGroup.value);
    request.createdById = order.photographerId;
    this.dispatchUpdate(request);
    this.addFormGroup.reset();
    this.formDirective.resetForm();
  }

  updateItem(order: PhotoOrder, item: MultiMediaItem) {
    const request = new MultiMediaItemRequest(item);
    request.photoOrderId = order._id;
    request.orderId = order.marketingOrderId;
    this.dispatchUpdate(request);
  }

  dispatchUpdate(item: MultiMediaItemRequest) {
    this.store.dispatch(new AddPhotoOrderMultimediaLink(item));
  }

}

