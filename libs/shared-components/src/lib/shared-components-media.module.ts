import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FileDropModule } from 'ngx-file-drop';
import { DragulaModule, DragulaService } from 'ng2-dragula';

import { CoreModule } from "@lc/core";

import { PhotoGridComponent } from './photos/photo-grid/photo-grid.component';
import { PropertyPhotoUploadComponent } from './photos/photo-upload/property-photo-upload.component';
import { MultimediaTableComponent } from './media/multimedia-table/multimedia-table.component';
import { MultiMediaItemFormComponent } from './media/multi-media-item-form/multi-media-item-form.component';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { MultiMediaItemForm } from './media/multi-media-item-form/multi-media-item.form';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    FileDropModule,
    DragulaModule,
    NgbModule,
    CoreModule,
    SharedComponentsUiModule
  ],
  declarations: [
    PhotoGridComponent,
    PropertyPhotoUploadComponent,
    MultimediaTableComponent,
    MultiMediaItemFormComponent,
  ],
  exports: [
    PhotoGridComponent,
    PropertyPhotoUploadComponent,
    MultimediaTableComponent,
    MultiMediaItemFormComponent,
  ],
  providers: [
    DragulaService
  ]
})
export class SharedComponentsMediaModule {}
