/*
Used to set properties of a team profile with user-friendlyt names:

      | Owner                | ABCDEFGHIJ-123456      |
      | Photo URL            | http://localhost:4200/ |
      | Photo Metadata       | { ratio: 2 }           |
 */
import { FunctionModelProperty, ModelMap } from '../support/model.map';
import { PhotoInformation } from '../../libs/core/src/lib/models/photo-information.model';

export class TeamProfileProps extends ModelMap {

  private static props: FunctionModelProperty[] = [
    new FunctionModelProperty('Owner', setOwnerId),
    new FunctionModelProperty('Photo URL', setTeamPhotoUrl),
    new FunctionModelProperty('Photo Metadata', setTeamPhotoMetaData)
  ];

  constructor() {
    super('team-profile', TeamProfileProps.props);
  }
}

function setOwnerId(profile: any, value: any): void {
  profile.ownerId = value;
}

function setTeamPhotoUrl(profile: any, value: any): void {
  if(!profile.teamPhoto) {
    profile.teamPhoto = new PhotoInformation();
  }
  profile.teamPhoto.photoUrl = value;
}

function setTeamPhotoMetaData(profile: any, value: any): void {
  if(!profile.teamPhoto) {
    profile.teamPhoto = new PhotoInformation();
  }
  profile.teamPhoto.metaData = JSON.parse(value);
}
