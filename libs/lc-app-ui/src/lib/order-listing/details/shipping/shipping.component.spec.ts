import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ShippingComponent } from './shipping.component';
import { DetailStepsComponent } from '../detail-steps/detail-steps.component';
import { OrderSummaryComponent } from '../order-summary/order-summary.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ORDERSTATE,
  FlowState,
  SELECTEDORDER,
  MarketingOrder,
  AddressType,
  PhoneNumber,
  PhoneType,
  AddressInformation,
  Address,
  UserService
} from '@lc/core';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { StoreMock, TestingModule } from '@lc/testing';
import { Store } from '@ngrx/store';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';
import { MarketingOrderFactory } from '../../../../../../core/src/lib/factories';
import SpyObj = jasmine.SpyObj;

describe('ShippingComponent', () => {
  let component: ShippingComponent;
  let fixture: ComponentFixture<ShippingComponent>;
  let store: StoreMock<any>;
  let order: MarketingOrder;
  let userService: SpyObj<UserService>;

  beforeEach(async(() => {
    userService = jasmine.createSpyObj('UserService', ['getAddress']);

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [
        DetailStepsComponent,
        OrderSummaryComponent,
        ShippingComponent,
      ],
      providers: [
        {provide: UserService, useValue: userService},
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    userService = TestBed.get(UserService);
    userService.getAddress.and.returnValue({});
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    order = MarketingOrderFactory.createMarketingOrder();
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
    store.setState(SELECTEDORDER, order);
  }));

  // Create the component
  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('#onSubmit should save the order and navigate to billing page', async(async() => {
    const dispatchMethod = spyOn(store, 'dispatch');
    const validAddress: AddressInformation = new AddressInformation({
      address: new Address({
        streetAddress1: '123 Easy Street',
        city: 'New York',
        state: 'NY',
        postalCode: '10001',
      }),
      phoneNumber: new PhoneNumber({
        number: '55555555555',
        type: PhoneType.direct,
      }),
      fullName: 'John Doe',
      email: 'unit-test@realogy.com',
      type: AddressType.SHIPPING
    });
    component.shippingForm.patchValue(validAddress);
    component.onSubmit(order);
    fixture.detectChanges();

    expect(dispatchMethod).toHaveBeenCalled();
  }));
});
