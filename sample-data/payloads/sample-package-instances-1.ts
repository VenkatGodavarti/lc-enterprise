export const samplePackages = JSON.parse(JSON.stringify(
  [
  {
    'title': 'Silver',
    'description': 'Silver PackageInstance',
    'code': '001',
    'price': 295.00,
    'position': 100,
    'thumbnailUri': 'abcd1233',
    'products': [{
      'title': 'Flyer',
      'description': 'Professionally designed property sign box and/or open house flyers.',
      'code': '043',
      'kind': 'product',
      'category': 'print',
      'thumbnailUri': '1234456abcd',
      'optional': false,
      'optedOut': false,
      'unitQuantity': 100,
      'unitPrice': 10,
      'unitValue': 1,
      'unitMultiple': 1,
      'position': 100,
      'options': [
      ]
    },
      {
        'title': 'Professional Photography',
        'description': 'High Dynamic Range Photography',
        'code': '050',
        'kind': 'product',
        'category': 'multimedia',
        'thumbnailUri': '1234456abcd',
        'optional': false,
        'optedOut': false,
        'unitQuantity': 100,
        'unitPrice': 10,
        'unitValue': 1,
        'unitMultiple': 1,
        'position': 100,
        'options': [
          {
            'title': 'HDR',
            'selected': true
          },
          {
            'title': 'Upgraded',
            'selected': false
          }
        ]
      }
    ]
  },
  {
    'title': 'Gold',
    'description': 'Gold PackageInstance',
    'code': '002',
    'price': 495.00,
    'position': 200,
    'thumbnailUri': 'abcd1234',
    'products': [{
      'title': 'Flyer',
      'description': 'Professionally designed property sign box and/or open house flyers.',
      'code': '043',
      'kind': 'product',
      'category': 'print',
      'thumbnailUri': '1234456abcd',
      'optional': false,
      'optedOut': false,
      'unitQuantity': 100,
      'unitPrice': 10,
      'unitValue': 1,
      'unitMultiple': 1,
      'position': 100,
      'options': [
      ]
    },
      {
        'title': 'Professional Photography',
        'description': 'HDR Photography',
        'code': '050',
        'kind': 'product',
        'category': 'multimedia',
        'thumbnailUri': '1234456abcd',
        'optional': false,
        'optedOut': false,
        'unitQuantity': 100,
        'unitPrice': 10,
        'unitValue': 1,
        'unitMultiple': 1,
        'position': 100,
        'options': [
          {
            'title': 'HDR',
            'selected': true
          },
          {
            'title': 'Upgraded',
            'selected': false
          }
        ]
      }
    ]
  },
    {
      'title': 'Platinum',
      'description': 'Platinum PackageInstance',
      'code': '003',
      'price': 895.00,
      'position': 300,
      'thumbnailUri': 'abcd1235',
      'products': [{
        'title': 'Flyer',
        'description': 'Professionally designed property sign box and/or open house flyers.',
        'code': '043',
        'kind': 'product',
        'category': 'print',
        'thumbnailUri': '1234456abcd',
        'optional': false,
        'optedOut': false,
        'unitQuantity': 100,
        'unitPrice': 10,
        'unitValue': 1,
        'unitMultiple': 1,
        'position': 100,
        'options': [
        ]
      },
        {
        'title': 'Professional Photography',
        'description': 'Photography package with additional photos',
        'code': '050',
        'kind': 'product',
        'category': 'multimedia',
        'thumbnailUri': '1234456abcd',
        'optional': false,
        'optedOut': false,
        'unitQuantity': 100,
        'unitPrice': 10,
        'unitValue': 1,
        'unitMultiple': 1,
        'position': 100,
        'options': [
          {
            'title': 'HDR',
            'selected': true
          },
          {
            'title': 'Upgraded',
            'selected': true
          }
        ]
      }
      ]
    }
]));

