import { Injectable } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { tap, switchMap, catchError, map } from 'rxjs/operators';
import { ThumbnailSpec } from '../models/photo-thumbnail.model';
import { ImageLoaderService } from './image-loader.service';
import { PhotoService } from './photo.service';
import { UploadPhoto } from '@lc/core';

export class UploadStatus {
  fileName: string;
  thumbnailUrl?: string;
  complete: boolean; // True if complete or error
  error?: string;
  visible: boolean;
  photos: UploadPhoto[];

  /** Percent done (0-100) */
  public readonly progress: Subject<number> = new Subject();

  constructor(public readonly file: File) {
    this.fileName = file.name;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ImageUploaderService {

  public minFileSizeInMB = 0.3;
  public allowedExtensions = ["jpeg", "png", "jpg"];

  constructor(private readonly photoService: PhotoService, private readonly imageLoader: ImageLoaderService){
  }

  upload(status: UploadStatus, thumbnailSpecs: ThumbnailSpec[], ignoreMinSize: boolean = false): Observable<UploadStatus> {

    // If the file doesn't need to meet minimum requirements set the minimum size to 1 byte
    if(ignoreMinSize) {
      this.minFileSizeInMB = 0.000001;
    }

    // Load the file into a dataURL for the UI to display
    return this.imageLoader.loadFile(status.file, this.allowedExtensions, this.minFileSizeInMB).pipe(
      tap(dataUrl => status.thumbnailUrl = dataUrl),

      // Then upload the file to the photoService and store the photos that return
      switchMap(() => this.photoService.upload(status.file, status.progress, thumbnailSpecs)),
      tap(photos => status.photos = photos),

      //Catch any errors from the previous steps and store them into the statis. Fail gracefully
      catchError(error => {
        status.error = error;
        status.progress.complete();
        return of(status);
      }),

      //Return the status
      map(() => status),
    );
  }
}
