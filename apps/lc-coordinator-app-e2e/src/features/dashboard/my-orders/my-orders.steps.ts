import { When, Then } from 'cucumber';
import { LCPage, Context, ContextKeys, Element, Configuration as config } from '../../../../../../e2e';
import { browser, ExpectedConditions as EC } from 'protractor';

When(/^I hover over the (.*) on row number (.*)$/, {timeout: config.timeout}, async (elementName: string, row: number) => {

  const page = Context.get<LCPage>(ContextKeys.page);

  const arrayElement = page.element(elementName);
  const arrayFinder = arrayElement.arrayFinder();
  const agentAvatar = arrayFinder.get(row - 1);
  await browser.wait(EC.elementToBeClickable(agentAvatar));

  await page.mouseHover(agentAvatar);
});
