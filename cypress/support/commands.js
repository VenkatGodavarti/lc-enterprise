// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("getPhotoUrl", (photographerName) => {
  const urls = {
    cuttingEdgePhotos: 'api/v1/images/5ca27eeaf4085f1575bea0f6',
    fabPhotos: 'api/v1/images/5ca27eebf4085f1575bea0f8',
    quickPhotos: 'api/v1/images/5ca27eebf4085f1575bea0fa'
  };

  return urls[photographerName];
});