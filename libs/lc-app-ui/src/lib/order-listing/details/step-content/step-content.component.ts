import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'lc-step-content',
  templateUrl: './step-content.component.html',
  styleUrls: ['./step-content.component.scss']
})
export class StepContentComponent implements OnInit {
  @Input()
  public showSummary = false;

  @Input()
  public nextButtonText = 'Next';

  @Input()
  public canClickNext = true;

  @Output()
  public next = new EventEmitter();

  @Output()
  public back = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onBack() {
    this.back.emit();
  }

  onNext() {
    if(this.canClickNext) {
      this.next.emit();
    }
  }
}
