
import { LCPage, ElementMap, Element } from '../../../../../../e2e';

export class MyProfileAddressesPage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element({ name: 'Page Header', field: '#addresses-header'}),

      new Element( { name: 'Shipping Address', field: '#shipping-address-display'}),
      new Element( { name: 'Billing Address',  field: '#billing-address-display'}),
      new Element( { name: 'Mailing Address',  field: '#mailing-address-display'}),


      new Element( { name: 'Add Shipping Address', field: '#add-shipping-address'}),
      new Element( { name: 'Edit Shipping Address', field: '#edit-shipping-address'}),
      new Element( { name: 'Delete Shipping Address', field: '#delete-shipping-address'}),

      new Element( { name: 'Add Billing Address',  field: '#add-billing-address'}),
      new Element( { name: 'Edit Billing Address',  field: '#edit-billing-address'}),
      new Element( { name: 'Delete Billing Address',  field: '#delete-billing-address'}),

      new Element( { name: 'Add Mailing Address',  field: '#add-mailing-address'}),
      new Element( { name: 'Edit Mailing Address',  field: '#edit-mailing-address'}),
      new Element( { name: 'Delete Mailing Address',  field: '#delete-mailing-address'})
    ]
  );

  constructor() {
    super('My Profile', [MyProfileAddressesPage.elements]);
  }

}
