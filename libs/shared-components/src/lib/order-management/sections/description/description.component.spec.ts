import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionComponent } from './description.component';
import {RouterTestingModule} from "@angular/router/testing";

import { Listing, MarketingOrder, PropertyStatus, PropertyType } from '@lc/core';
import { Store } from '@ngrx/store';
import SpyObj = jasmine.SpyObj;
import { Observable, of } from 'rxjs';

describe('DescriptionComponent', () => {
  let component: DescriptionComponent;
  let fixture: ComponentFixture<DescriptionComponent>;
  let store: SpyObj<Store<any>>;
  let state: SpyObj<MarketingOrder>;
  let listing: Listing;
  let marketingOrder: MarketingOrder;

  beforeEach(async(() => {
    store = jasmine.createSpyObj('Store', ['dispatch', 'pipe']);
    state = jasmine.createSpyObj('MarketingOrder', ['setStep']);

    listing = createListing(false);
    marketingOrder = new MarketingOrder();
    marketingOrder.marketingCopyHeadline = ['Headline copy'];
    marketingOrder.marketingCopyBody = ['body copy'];

    marketingOrder.listing = listing;
    store.pipe.and.returnValue(of(marketingOrder));

    TestBed.configureTestingModule({
      declarations: [ DescriptionComponent ],
      imports: [RouterTestingModule.withRoutes([{path: '**', component: DescriptionComponent}])],
      providers: [
        { provide: Store, useValue: store },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionComponent);
    component = fixture.componentInstance;
    component.marketingOrder = marketingOrder;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

function createListing(hasId: boolean) {
  const newListing = new Listing();
  newListing._id = hasId ? '123456789ABCDEF' : null;
  newListing.propertyType = PropertyType.RESIDENTIAL;
  newListing.status = PropertyStatus.ACTIVE;
  newListing.listPrice = 400000;
  newListing.bedrooms = 4;
  newListing.bathrooms = 4;
  newListing.sqFt = 4000;
  newListing.lotSqFt = 14000;
  newListing.listDate = new Date();

  return newListing;
}
