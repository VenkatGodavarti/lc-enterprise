import { LCPage, Element, ElementMap } from '../../../../../../e2e';

export class ListingAddressPage extends LCPage {
  static elements = new ElementMap( 'page',  [
      new Element({ name: 'Header', field: "h1"}),
      new Element({ name: 'Address', field: "#address-input" }),
      new Element({ name: 'Address Error', field: "#address-error" }),
      new Element({ name: 'Unit Number', field: "#unit-number-input" }),
      new Element({ name: 'City', field: "#city-input" }),
      new Element({ name: 'City Error', field: "#city-error" }),
      new Element({ name: 'State', field: "#state-input" }),
      new Element({ name: 'State Error', field: "#state-error" }),
      new Element({ name: 'Zip Code', field: "#zip-input" }),
      new Element({ name: 'Zip Code Error', field: "#zip-error" }),
      new Element({ name: 'MLS ID', field: "#mls-id-input" }),
      new Element({ name: 'GlobalLuxuryListing', field: "input[formcontrolname='globalLuxuryListing']" }),
      new Element({ name: 'Continue', field: "#nextBtn" }),
    ]
  );

  constructor() {
    super('Listing Address', [ListingAddressPage.elements]);
  }
}
