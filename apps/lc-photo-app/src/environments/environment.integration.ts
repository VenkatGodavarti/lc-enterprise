export const environment = {
  // App configuration
  production: false,
  envName: 'integration',
  applicationName: 'Listing Concierge Photo App LocalHost',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://localhost:4700/',
  apiBaseURL: 'http://localhost:4100/api/v1/',
  photosBaseURL: 'http://localhost:4300/api/v1/',
  notifyBaseURL: 'http://100.25.65.5:4800/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajcwwe6zYnqyhEA0h7',
  oktaRedirectUri: 'http://localhost:4700/implicit/callback',
};
