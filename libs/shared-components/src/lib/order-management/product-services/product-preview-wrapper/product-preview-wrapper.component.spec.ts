import { async, ComponentFixture, fakeAsync, flush, inject, TestBed } from '@angular/core/testing';

import { ProductPreviewWrapperComponent } from './product-preview-wrapper.component';
import { ProductPreviewComponent } from '../product-preview/product-preview.component';
import { StoreMock, TestingModule } from '@lc/testing';
import { MarketingOrderFactory, PackageInstanceFactory } from '../../../../../../core/src/lib/factories';
import { RouterTestingModule } from '@angular/router/testing';
import { PrintPreviewComponent } from '../print-preview/print-preview.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { SELECTEDORDER } from '@lc/core';

describe('ProductPreviewWrapperComponent', () => {

  let component: ProductPreviewWrapperComponent;
  let fixture: ComponentFixture<ProductPreviewWrapperComponent>;
  let store: StoreMock<any>;
  const marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
  const packages = PackageInstanceFactory.createAll();
  marketingOrder.availablePackages = packages;
  marketingOrder.selectedPackageCode = packages[0].code;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TestingModule
      ],
      declarations: [ ProductPreviewWrapperComponent, ProductPreviewComponent, PrintPreviewComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(SELECTEDORDER, marketingOrder); // set default state
  }));


  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(ProductPreviewWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    flush();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
