import {getTestBed, TestBed} from '@angular/core/testing';
import {
  AuthenticationService, OutputMimeType, PdfGenerationService, ProductInstance, WindowService
} from '@lc/core';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { MarketingOrderFactory } from '../factories';

describe('PdfGenerationService', () => {

  let marketingOrder;
  let product;
  let httpClient;
  let authenticationServiceSpy;
  let windowServiceSpy;
  let pdfGenerationService;

  let injector: TestBed;

  beforeEach(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    product = new ProductInstance({
      title: 'Flyer',
      kind: 'product',
      category: 'print',
      selectedTemplate: {
        mimeType: OutputMimeType.pdf
      }
    });
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', ['getToken']);
    windowServiceSpy = jasmine.createSpyObj('WindowService', ['open']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PdfGenerationService,
        { provide: AuthenticationService, useValue: authenticationServiceSpy },
        { provide: WindowService, useValue: windowServiceSpy }]
    });

    injector = getTestBed();
    httpClient = injector.get(HttpTestingController);
    pdfGenerationService = TestBed.get(PdfGenerationService);
  });

  it('generate()', () => {

    pdfGenerationService.generate(marketingOrder, product).subscribe( content => {});

    httpClient.expectOne( (req) => {
      let expectedUrl = req.url.endsWith('/print-render');
      expect(expectedUrl).toBeTruthy();
      expect(req.body.orderId).toEqual(marketingOrder._id);
      expect(req.body.productCode).toEqual(product.code);

      return expectedUrl;
    });

  });

  it('download()', () => {

    let openUrl;

    authenticationServiceSpy.getToken.and.returnValue('a-token');

    windowServiceSpy.open.and.callFake( (url, target) => {
      openUrl = url;
    });

    pdfGenerationService.download(marketingOrder, product);

    expect(authenticationServiceSpy.getToken).toHaveBeenCalled();
    expect(openUrl).toBeTruthy();
    expect(openUrl.includes('/flyer_123_somewhere_st.pdf')).toBeTruthy();
    expect(openUrl.includes('x=a-token')).toBeTruthy();


  });

});

