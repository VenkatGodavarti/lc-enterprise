import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'lc-avatar-image',
  templateUrl: './avatar-image.component.html',
  styleUrls: ['./avatar-image.component.scss']
})
export class AvatarImageComponent implements OnInit {

  @Input() src: string;

  @Input() canEdit: boolean = true;

  @Output()
  readonly edit = new EventEmitter<void>()
  constructor() { }

  ngOnInit() {
  }

  onEdit() {
    this.edit.emit();
  }

}
