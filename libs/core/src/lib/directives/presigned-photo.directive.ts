import { Directive, Input, OnChanges, SimpleChanges, HostBinding } from '@angular/core';
import { ListingPhoto } from '../models/listing-photo.model';
import { PresignService, PresignedThumbnail, PresignedPhoto } from '../services/presign.service';

@Directive({
  selector: '[lcPresignedPhoto]'
})
export class PresignedPhotoDirective implements OnChanges {
  private _presignedPhoto: PresignedPhoto;
  public get presignedPhoto(): PresignedPhoto { return this._presignedPhoto; }

  @HostBinding('src')
  imageSource: string = '';

  // tslint:disable-next-line:no-input-rename
  @Input('lcPresignedPhoto')  // Keep the name of this input the same as the directive seleector name
  photo: ListingPhoto;

  @Input()
  size?: number;

  @Input()
  fallbackSize?: number;

  constructor(private service: PresignService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.photo || changes.size) {
      if(this._presignedPhoto) {
        this.setImageSource(this._presignedPhoto);
      } else {
        this.service.presignPhoto(this.photo).subscribe(presigned => this.setImageSource(presigned));
      }
    }
  }

  private setImageSource(presignedPhoto: PresignedPhoto){
    // Cache the photo for future use
    this._presignedPhoto = presignedPhoto;

    const requestedThumbnail = this.findRequestedThumbnail(presignedPhoto);
    if(!requestedThumbnail) {
      // The default image is the main photo. Choose the presigned first, otherwise use the original url
      this.imageSource = presignedPhoto.presignedUrl || presignedPhoto.originalUrl;
    } else {
      // We found the requested Thumbnail. Choose the presigned first
      this.imageSource = requestedThumbnail.presignedUrl || requestedThumbnail.originalUrl;
    }
  }

  private findRequestedThumbnail(presigned: PresignedPhoto): PresignedThumbnail {
    if (!this.size) {
      return null; // If size is not specified, return null
    }

    // If a size is specified, lets find the requested thumbnail. If not found, find the fallback
    const requestedThumbnail = presigned.locateThumbnail(this.size, this.fallbackSize);
    return requestedThumbnail;
  }
}
