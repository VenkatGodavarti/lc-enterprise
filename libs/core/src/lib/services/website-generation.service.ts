import { ContentGenerator } from './content-generator';
import { MarketingOrder, OutputMimeType, ProductInstance } from '../models';
import { MarketingOrderService } from '../services/marketing-order.service';
import { Observable, of} from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const websiteTemplateUrl = '/assets/listing-site/index.html';

@Injectable()
export class WebsiteGenerationService implements ContentGenerator {

  mimeType = OutputMimeType.html;

  downloadable = false;

  constructor(private http: HttpClient,
    private marketingOrderService: MarketingOrderService) {

  }

  generate(order: MarketingOrder, product: ProductInstance): Observable<any> {

    return this.get(websiteTemplateUrl).pipe(map( data => {
      return this.marketingOrderService.fulfill(order._id, product.code, data).pipe(map( html => html));
    }));
  }

  download(order: MarketingOrder, product: ProductInstance): Observable<any> {
    return of(null);
  }

  private get(url: string): Observable<any> {

    return this.http.get(url, { responseType: 'text'});;
  }
}
