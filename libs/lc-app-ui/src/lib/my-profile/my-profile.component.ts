import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { NotificationEventService } from '@lc/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'lc-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit, OnDestroy {
  // TODO - find out why this is needed and not just in HTML
  @HostBinding('class') private hostClasses = 'd-flex flex-grow-1 align-items-stretch h-100 pr-3';

  private notificationSub: Subscription;

  // TODO - we can display message in any form. We are choosing a snackbar type here
  constructor(private notificationService: NotificationEventService) {
    this.notificationSub = this.notificationService.getEventEmitter().subscribe(
      (event) => {
        console.log('UI has notification event');
        console.log(event);
      }
    )

  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.notificationSub.unsubscribe();
  }
}
