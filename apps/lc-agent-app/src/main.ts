import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from '../../../libs/environments/environment';
// KWC - removed on merge because there is no version.ts in directory
//
// import { VERSION } from '../../../libs/environments/version';
//
// console.log(VERSION);

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
