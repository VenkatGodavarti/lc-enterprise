import { Deserializable } from './deserializable.model'
import { Photographer } from './photographer.model'

export class Lockbox implements Deserializable<Lockbox> {
  code: string;
  location: string;

  deserialize(input: any): Lockbox {
    Object.assign(this, input);
    return this;
  }
}

export class PhotographerAppointment implements Deserializable<PhotographerAppointment> {
  photographer: Photographer;
  scheduledAt: Date;
  lockboxAvailable = false;
  lockbox: Lockbox;
  comments: string;

  constructor(data?: any) {
    this.deserialize(data || {});
  }

  deserialize(input: any): PhotographerAppointment {
    Object.assign(this, input);
    return this;
  }
}
