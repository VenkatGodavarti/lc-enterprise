@login-before-all
Feature: I want to verify the listing dashboard page

Background:
  Given I am logged in to Listing Concierge
  Given the following orders exist
  | Name                 | Alamo                 | White House           |
  | Package              | Gold                  | Platinum              |
  | Status               | PENDING               | PENDING               |

  # Navigate to the All Orders tab
  When I go to the Main page in Dashboard
  And I click on the All Orders Tab button
  Then the All Orders page from Dashboard is loaded

  When the "Orders" table records load

# Note: This test assumes there are not already any marketing orders
Scenario: Sortable columns sort ascending and descending

  Then the "Orders" table has the columns "Address, Agent, Date, Package, Coordinator"

  # Default Sort: Date Desc
  Then the "Orders" table has the following data
  | Address                                         | Package  | Coordinator | Copy  | Photos |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum |             | NO    | NO     |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold     |             | Ready | 2      |

  # Sort Package name ascending
  When I click on the Package Header button
  And the "Orders" table records load

  Then the "Orders" table has the following data
  | Address                                         | Package             | Coordinator |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold                |             |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum            |             |

  # Sort Package name descending
  When I click on the Package Header button
  And the "Orders" table records load
  Then the "Orders" table has the following data
  | Address                                         | Package             | Coordinator |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum            |             |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold                |             |

Scenario: Assign to me removes element from All Orders and adds it to My Orders

  Then the "Orders" table has the columns "Address, Agent, Date, Package, Coordinator"

  # Default Sort: Date Desc
  Then the "Orders" table has "2" records
  Then the "Orders" table has the following data
  | Address                                         | Package             | Coordinator |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum            |             |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold                |             |

  # Sort Package name ascending
  When I click on the "Assign to Me" button in row "1" of the "Orders" table

  Then the "Prompt" dialog is displayed
  When I click on the "Yes" button inside the "Prompt Dialog"
  Then the "Prompt" dialog is not displayed


  When the "Orders" table records load
  Then the "Orders" table has "1" record
  Then the "Orders" table has the following data
  | Address                                         | Package             | Coordinator |
  | 300 Alamo Plaza\nSan Antonio, TX                | Gold                |             |

  # Validat it is on the My Orders page
  When I click on the My Orders Tab button
  Then the My Orders page from Dashboard is loaded

  When the "Orders" table records load
  Then the "Orders" table has "1" record
  Then the "Orders" table has the following data
  | Address                                         | Package             |
  | 1600 Pennsylvania Ave NW\nWashington, DC        | Platinum            |
