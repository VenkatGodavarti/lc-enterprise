import { SharedComponentsUiModule } from './shared-components-ui.module';

describe('SharedComponentsUiModule', () => {
  let sharedComponentsUiModule: SharedComponentsUiModule;

  beforeEach(() => {
    sharedComponentsUiModule = new SharedComponentsUiModule();
  });

  it('should create an instance', () => {
    expect(sharedComponentsUiModule).toBeTruthy();
  });
});
