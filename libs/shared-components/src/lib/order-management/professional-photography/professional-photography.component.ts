import { Component, OnInit, OnDestroy } from '@angular/core';
import { MarketingOrder, PhotographerAppointment, SELECTEDORDER } from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { environment } from '../../../../../environments/environment';

/**
 * Provide a UI for an agent to view the photography details
 *
 */
@Component({
  selector: 'lc-professional-photography',
  templateUrl: './professional-photography.component.html',
  styleUrls: ['./professional-photography.component.scss']
})
export class ProfessionalPhotographyComponent implements OnInit, OnDestroy {

  marketingOrder$: Observable<MarketingOrder>;
  appointment: PhotographerAppointment;
  photoUrl = environment.apiBaseURL;

  marketingOrderSub: Subscription;

  constructor(private store: Store<any>) {

    this.marketingOrder$ = this.store.pipe(select(SELECTEDORDER));

  }

  ngOnInit() {
    this.marketingOrderSub = this.marketingOrder$.subscribe((marketingOrder) => {
      this.appointment = marketingOrder.getSelectedPackage().getPhotographyProduct().serviceAppointment;
    });
  }

  ngOnDestroy(): void {
    if (this.marketingOrderSub) {
      this.marketingOrderSub.unsubscribe();
    }
  }

}
