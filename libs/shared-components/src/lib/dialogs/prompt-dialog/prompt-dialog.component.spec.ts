import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PromptDialogComponent } from './prompt-dialog.component';
import { PromptDialogService } from './prompt-dialog.service';

describe('PromptDialogComponent', () => {
  let component: PromptDialogComponent;
  let fixture: ComponentFixture<PromptDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NgbModule ],
      declarations: [ PromptDialogComponent ],
      providers: [ PromptDialogService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromptDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
