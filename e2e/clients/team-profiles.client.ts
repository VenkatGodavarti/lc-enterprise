import { http } from './http';
import { Configuration as config } from '../configuration';
import { ContextKeys, Context } from '../support/context';

export class TeamProfilesClient {

  /**
   * Returns the team profiles that belong to the logged in user
   */
  static async get(): Promise<any[]> {
    const user = Context.get(ContextKeys.user);
    return http.get(`${config.apiBaseURL}/team-profiles?agentId=${user._id}`).then(response => response.data);
  }

  /**
   * Http Post for the team-profiles endpoint.
   * Returns the created profile
   * @param profile Profile to create
   */
  static async post(profile: any): Promise<any> {
    const prof = this.cleanProfile(profile);
    return http.post(`${config.apiBaseURL}/team-profiles`, prof).then(response => response.data);
  }

  /**
   * Http Put for the team-profiles endpoint. Uses the the given profile._id for the route.
   * Returns the created profile
   * @param profile Profile to create
   */
  static async put(profile: any): Promise<any> {
    const prof = this.cleanProfile(profile);
    return http.put(`${config.apiBaseURL}/team-profiles/${profile._id}`, prof).then(response => response.data);;
  }

  /**
   * Http Delete for the team-profile endpoint.
   * @param profileId The profile._id to delete
   */
  static async delete(profileId: string): Promise<any> {
    return http.delete(`${config.apiBaseURL}/team-profiles/${profileId}`);
  }

  /**
   * Clones the given profile and removes any properties that are unwanted by the API
   * Returns the cloned, cleaned profile
   * @param profile The profile to clean
   */
  private static cleanProfile(profile){
    const cleaned = Object.assign({}, profile);
    delete cleaned._id;
    delete cleaned.schemaVersion;
    return cleaned;
  }
}
