import { Deserializable } from './deserializable.model';
import { Address } from './address.model';
import { PhotographerAppointment } from './photographer-appointment.model';
import { ServiceStatusType } from './service-status.type';
import { Media } from './media.model';
import { ListingPhoto } from './listing-photo.model';

export class PhotoOrder implements Deserializable<PhotoOrder> {
  _id: string;
  photographerId: string;
  propertyAddress: Address;
  agentName: string;
  orderedAt: Date;
  contactDetails: string;
  appointment: PhotographerAppointment;
  marketingOrderId: string;
  photoOrderId: string;
  status: ServiceStatusType;
  media: Media;
  photos: ListingPhoto[] = [];

  constructor(input?: any) {
    if (input) {
      this.deserialize(input);
    }
  }

  deserialize(input: any): PhotoOrder {
    Object.assign(this, input);
    // always initialize media object
    if (this.media) {
      this.media = new Media(this.media);
    } else {
      this.media = new Media();
    }
    return this;
  }
}
