import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GetOrder, Listing, MarketingOrder, SELECTEDORDER } from '@lc/core';
import { Store } from '@ngrx/store';
import { Subscription, of } from 'rxjs';

@Injectable()
export class OrderListingResolve implements Resolve<Listing>, OnDestroy {

  storeSub: Subscription;
  marketingOrder: MarketingOrder;

	constructor(private store: Store<any>) {

    const marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER);
    this.storeSub = marketingOrder$.subscribe(marketingOrder => {
      this.marketingOrder = marketingOrder;
    });
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.params['id'];

    // only load the marketing order if not already in memory store.
    if (id && !this.marketingOrder) {
      this.store.dispatch(new GetOrder(new MarketingOrder({_id: id})));
    }
    // we are not using resolve injection of objects so just return null
    return of(null);

	}

	ngOnDestroy(): void {
	  if (this.storeSub) {
	    this.storeSub.unsubscribe();
    }
  }
}
