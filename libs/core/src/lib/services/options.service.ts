import { Injectable } from '@angular/core';
import { Option } from '../models/option.model';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';


@Injectable()
export class OptionsService {

  constructor(private apiService: ApiService) { }

  /**
   * Get all options.
   * @return an Observable that results in an array of Options
   */
  getOptions(): Observable<Option[]> {
    return this.apiService.get('options');
  }
}
