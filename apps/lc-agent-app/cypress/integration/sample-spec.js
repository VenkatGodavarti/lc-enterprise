
describe('Example Test', function() {

  it('should contain one market order in "My Listings"', function() {
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/summary', 'fixture:market-order-summaries');
    cy.visit('http://localhost:4200');

    cy.get('lc-listing-card')
      .should("have.length", 1);
  });

  it('should hide my market order in "My Listings"', function() {
    cy.server();
    cy.route('GET', '/api/v1/marketing-orders/summary', 'fixture:market-order-summaries').as('summaryList');
    cy.route('OPTIONS', '/api/v1/marketing-orders/summary');
    cy.fixture('market-order-summaries').then((summaries) => {
      summaries[0].status = 'ARCHIVED';
      cy.route('PUT', '/api/v1/marketing-orders/5cb784440a031231acac3300', summaries).as('hiddenSummary');
    });
    cy.visit('http://localhost:4200');

    cy.wait('@summaryList').then((xhr) => {
      cy.get('.row .ion-md-eye')
        .click();
    });
    cy.wait(('@hiddenSummary')).then((xhr) => {
      cy.get('lc-listing-card')
        .should("have.length", 0);
    });
  });

});
