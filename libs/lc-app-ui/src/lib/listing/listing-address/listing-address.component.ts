import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { convertToParamMap, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

import {
  ActionTypes, AddKeys,
  AddOrder,
  AddOrderComplete,
  AddressForm, HeaderConstants,
  Listing,
  MarketingOrder,
  OrderActionTypes,
  OrderListingFlowStateService,
  SELECTEDORDER, UIConstants,
  UpdateOrder,
  UpdateOrderComplete,
  UpdateOrderListing,
  WorkflowAction
} from '@lc/core';
import { Observable, Subscription } from 'rxjs';
import { ActionsSubject, Store } from '@ngrx/store';
import { DialogAction, PromptDialogService } from '@lc/shared-components-ui';
import { ofType } from '@ngrx/effects';
import * as uuid from 'uuid'

@Component({
  selector: 'lc-listing-address',
  templateUrl: './listing-address.component.html',
  styleUrls: ['./listing-address.component.scss']
})
export class ListingAddressComponent implements OnInit, OnDestroy {

  @HostBinding('class') private hostClasses = 'd-flex flex-grow-1 align-items-stretch';
  submitted = false;

  marketingOrder$: Observable<MarketingOrder>;

  marketingOrderSub: Subscription;
  subjectSub: Subscription;

  headerText: String;

  readonly newHeaderText: String = 'Great! Let\'s get started creating your order.';
  readonly updateHeaderText: String = 'Update your listing address';
  listingForm: FormGroup;

  constructor(private router: Router,
              private dialogService: PromptDialogService,
              private orderListingFlowService: OrderListingFlowStateService,
              private actionSubject: ActionsSubject,
              private store: Store<any>) {

    this.marketingOrder$ = store.select(SELECTEDORDER);
  }

  ngOnInit() {
    this.marketingOrderSub = this.marketingOrder$.subscribe(
      (marketingOrder) => {
        if (marketingOrder && marketingOrder.listing) {
          this.initListingForm(marketingOrder.listing);
        }
      }
    );

    this.subjectSub = this.actionSubject.pipe(
      ofType(OrderActionTypes.UpdateOrderComplete, OrderActionTypes.AddOrderComplete)
    ).subscribe((data: UpdateOrderComplete | AddOrderComplete) => {
      // created order or update listing with no package selected
      if (data.type === OrderActionTypes.AddOrderComplete) {
        // payload is a MarketingOrder
        const pageRoute = `/order-listing/${data.payload._id}/select-marketing-package`;
        this.router.navigate([pageRoute]);
      } else {
        this.routeToListingDetails(data.payload);
      }
    });
  }

  ngOnDestroy(): void {
    this.marketingOrderSub.unsubscribe();
    this.subjectSub.unsubscribe();
  }


  initListingForm(listing: Listing) {
    this.headerText = listing._id ? this.updateHeaderText : this.newHeaderText;

    this.listingForm = new FormGroup({
      mlsId: new FormControl(listing ? listing.mlsId : null),
      globalLuxuryListing: new FormControl(listing.globalLuxuryListing || false),
      address: new AddressForm(listing ? listing.address : null)
    });
  }

  onSubmit(marketingOrder: MarketingOrder) {

    this.submitted = true;
    (this.listingForm.get('address') as AddressForm).markAllAsDirty();
    if (!this.listingForm.valid) {
      return;
    }

    const formValues = this.listingForm.value;
    const address = formValues.address;
    address.unitNumber = address.unitNumber ? address.unitNumber.trim() : null;
    address.unitNumber = address.unitNumber === '' ?  null : address.unitNumber;

    const listing = marketingOrder.listing;
    listing.address = address;

    //assign new values
    listing.mlsId = (formValues.mlsId || '').trim() === '' ? null : formValues.mlsId.trim();
    listing.globalLuxuryListing = formValues.globalLuxuryListing;

    if(marketingOrder._id && this.listingForm.dirty) {
      // If updating existing address, prompt user that progress will be lost
      const message = 'You will lose all progress on your order, if you continue. Do you want to continue?';
      const okAction = new DialogAction(UIConstants.YES, () => this.updateListingAddress(marketingOrder));
      this.dialogService.openPrompt(UIConstants.CONFIRM, message, okAction, [new DialogAction(UIConstants.CANCEL)])
    } else {
      const method = marketingOrder._id ? new UpdateOrderListing(marketingOrder) : new AddOrder(marketingOrder);
      if (method.type === OrderActionTypes.AddOrder) {
        // create correlation id for the creation of the marketing order
        const addKeys = {};
        addKeys[HeaderConstants.CORRELATION_ID] = uuid.v4();
        this.store.dispatch(new AddKeys(addKeys));
      }
      this.store.dispatch(method);
    }

  }

  canClickBack(order: MarketingOrder) {
    return order._id != null;
  }

  onBack(order: MarketingOrder) {
    this.routeToListingDetails(order);
  }

  private updateListingAddress(marketingOrder: MarketingOrder) {
    // If we are updating, we need to clear ALL steps as well as package related product info (i.e. Photo Appointments)
    marketingOrder.setOrderState(null);
    marketingOrder.listing.clearAgentInfo();
    marketingOrder.listing.clearPropertyInfo();
    marketingOrder.clearPackageSpecificData();
    // Updates an existing listing's address
    this.store.dispatch(new UpdateOrder(marketingOrder));
  }

  private routeToListingDetails(order: MarketingOrder){
    // TODO: This is duplicated code from select-marketing-package
    if (!order.getOrderState()) {
      const pageRoute = `/order-listing/${order._id}/${order.selectedPackageCode}`;
      this.router.navigate([pageRoute]);
    } else {
      // Navigate to current order state
      const newState = this.orderListingFlowService.createNewFlowState(order);
      const params = convertToParamMap({ lid: order.listing._id, pid: order.selectedPackageCode });
      const action = new WorkflowAction(ActionTypes.WorkflowActionInitialize, { state: newState }, params);
      this.store.dispatch(action);
    }
  }
}
