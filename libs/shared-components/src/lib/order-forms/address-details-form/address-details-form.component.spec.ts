import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressDetailsFormComponent } from './address-details-form.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';

import { Listing, AddressForm } from '@lc/core';
import { ListingFactory } from '../../../../../core/src/lib/factories';

describe('AddressDetailsFormComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let listing: Listing;
  let listingFactory: ListingFactory;

  beforeEach(async(() => {
    listingFactory = new ListingFactory();

    TestBed.configureTestingModule({
      declarations: [ TestComponent, AddressDetailsFormComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestComponent);
    listing = listingFactory.createDefaultListing();
    component = fixture.componentInstance;
    component.addressComponent.formGroup = new AddressForm(listing.address);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component.addressComponent).toBeTruthy();
  });


  it('has address values', () => {

    const controls = component.addressComponent.formGroup.controls;
    expect(controls).toBeTruthy();
    expect(controls.streetAddress1.value).toEqual(listing.address.streetAddress1);
    expect(controls.unitNumber.value).toEqual(listing.address.unitNumber);
    expect(controls.city.value).toEqual(listing.address.city);
    expect(controls.state.value).toEqual(listing.address.state);
    expect(controls.postalCode.value).toEqual(listing.address.postalCode);

  });

});


/**
 * A simple component used to host the component during tests so that we can set its Input value
 * properly.
 *
 */
@Component({
  selector: 'lc-test',
  template: '<lc-address-details-form></lc-address-details-form>'
})
class TestComponent {
  @ViewChild(AddressDetailsFormComponent)
  public addressComponent: AddressDetailsFormComponent;

}
