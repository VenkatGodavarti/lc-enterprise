import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { OrderStepOverviewComponent } from './order-step-overview.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ORDERSTATE, FlowStep } from '@lc/core';
import { Store } from '@ngrx/store';
import { TestingModule, StoreMock } from '@lc/testing';

describe('OrderStepOverviewComponent', () => {
  let component: OrderStepOverviewComponent;
  let fixture: ComponentFixture<OrderStepOverviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderStepOverviewComponent ],
      imports: [
        TestingModule,
      ],
      providers: [
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    const state = jasmine.createSpyObj('FlowState', ['setStep', 'parentOfCurrentStep', 'getCurrentStep', 'getActiveSteps']);
    const fs = new FlowStep();
    fs.data = {
      header1: '',
      header2: ''
    };

    state.getCurrentStep.and.returnValue(fs);
    state.getActiveSteps.and.returnValue(0);
    mockStore.setState(ORDERSTATE, state); // set default state
  }));

  // Create the component
  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStepOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
/*
  it('should create', () => {
    expect(component).toBeTruthy();
  });
*/
});
