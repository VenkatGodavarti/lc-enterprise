
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { browser, ElementFinder } from 'protractor';
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

When(/^I (check|uncheck) the "(.*)" checkbox$/, {timeout: config.timeout }, async (checkOrUncheck, checkboxName) => {
  // Get checkbox value
  const checkbox = findCheckbox(checkboxName);
  let isChecked = await isCheckboxChecked(checkbox);

  const expected = checkOrUncheck === 'check';
  while(isChecked !== expected) {
    // We were using checkbox.click() but it fails with ChromeDriver when the checkbox
    // is surrounded with a label/span/div.  Browser.actions() gets around that
    // await checkbox.click();
    const webEl = checkbox.getWebElement();
    await browser.actions().mouseMove(webEl).perform();
    await browser.actions().click().perform();
    isChecked = await isCheckboxChecked(checkbox);
  }
});

Then(/^the "(.*)" checkbox is (checked|unchecked)$/, {timeout: config.timeout }, async (checkboxName, checkOrUncheck) => {
  // Get checkbox value
  const checkbox = findCheckbox(checkboxName);
  const isChecked = await isCheckboxChecked(checkbox);

  // Assert the condition
  const expected = checkOrUncheck === 'checked'
  expect(isChecked).to.be.equal(expected);
});

function findCheckbox(checkboxName: string) {
  const page = Context.get<LCPage>(ContextKeys.page);
  const checkbox = page.finder(checkboxName);
  return checkbox;
}

async function isCheckboxChecked(checkbox: ElementFinder): Promise<boolean> {
  const isSelected = await checkbox.isSelected();
  return isSelected;
}
