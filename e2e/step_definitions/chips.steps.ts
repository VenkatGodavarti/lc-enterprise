
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { browser, $, ElementFinder, ExpectedConditions as EC } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const assert = chai.assert;

When(/^the chips menu is loaded$/, {timeout: config.timeout }, async () => {
  const element = $('ng2-dropdown-menu');
  const items = element.$$('ng2-menu-item');

  await browser.wait(() => items.count().then(records => records > 0),
    10000, 'Menu items did not load within expected time frame');

    const first = items.first();
    await browser.wait(EC.elementToBeClickable(first), 10000);
});

When(/^I click on option (.*) in the chips menu$/, {timeout: config.timeout }, async (optionNumber) => {
  const element = $('ng2-dropdown-menu');
  const option: ElementFinder = await element.$$('ng2-menu-item').get(optionNumber - 1);

  await option.click();
});

Then(/^the chips menu has (.*) (option|options)$/, {timeout: config.timeout }, async (expectedCount, optionOrOptions) => {
  expectedCount = +expectedCount;
  const element = $('ng2-dropdown-menu');

  const items = await element.$$('ng2-menu-item');
  const actualCount = items.length;
  expect(actualCount).to.be.equal(expectedCount);
});


Then(/^the "(.*)" chips contains "(.*)"$/, {timeout: config.timeout }, async (elementName:string, expectedString:string) => {
  const expectedOptions = expectedString.split(',').map(option => option.trim());
  const page = Context.get<LCPage>(ContextKeys.page);
  const chipsField = page.finder(elementName);

  const actualOptions = await chipsField.$$('tag').map(async (tag) => {
    const text = await tag.getAttribute("textContent");
    return text;
  });

  expectedOptions.forEach(expected => {
    const found = actualOptions.find(actual => actual === expected);
    assert.isNotNull(found, `The ${expected} option was not found in the ${elementName}`)
  })
});
