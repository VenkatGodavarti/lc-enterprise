import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  PhotoOrder,
  PhotoOrderService,
  UploadPhoto,
  ServiceStatusType,
  PhotoOrderSelected,
  selectCurrentOrder,
  UpdatePhotoOrder,
  UpdatePhotoOrderComplete,
  PhotoOrderActionTypes,
  ListingPhoto,
  UploadPhotoOrderPhotos,
  UpdatePhotoOrderPhotos,
  UpdatePhotoOrderPhotosComplete,
  SubmitPhotoOrderPhotos,
  SubmitPhotoOrderPhotosComplete,
  MarketingOrderService,
} from '@lc/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionsSubject, select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'lc-app-manage-photos',
  templateUrl: './manage-photos.component.html',
  styleUrls: ['./manage-photos.component.scss']
})
export class ManagePhotosComponent implements OnInit, OnDestroy {
  private photographerName: string;

  orderStatus = ServiceStatusType;

  storeSub: Subscription;
  orderCompleteSubject: Subscription;
  updatePhotoOrderPhotosCompleteSub: Subscription;
  updatePhotoSubmitCompleteSub: Subscription;

  photoOrder$: Observable<PhotoOrder>;

  workingPhotos: ListingPhoto[] = [];
  submittedPhotos: ListingPhoto[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private actionSubject: ActionsSubject,
              private marketingOrderService: MarketingOrderService,
              private photoOrderService: PhotoOrderService,
              private store: Store<any>) { }


  ngOnInit() {
    const id = this.route.parent.snapshot.paramMap.get('photoOrderId');
    this.store.dispatch(new PhotoOrderSelected(id));
    this.photographerName = this.route.snapshot.queryParamMap.get('photographerName');
    this.photoOrder$ = this.store.pipe(select(selectCurrentOrder));

    this.photoOrder$.subscribe(photoOrder => {
      if (!photoOrder || photoOrder._id === null) {
        this.getPhotoOrder(id);
      }
    });
    this.setPhotos();

    // listen for the completed action of updated photos
    this.updatePhotoOrderPhotosCompleteSub = this.actionSubject.pipe(ofType<UpdatePhotoOrderPhotosComplete>(PhotoOrderActionTypes.UpdatePhotoOrderPhotosComplete))
      .subscribe(updatePhotosComplete => {
        this.workingPhotos = updatePhotosComplete.photos.map(aPhoto => new ListingPhoto(aPhoto));
      });

    // listen for the completed action of submit photos
    this.updatePhotoSubmitCompleteSub = this.actionSubject.pipe(ofType<SubmitPhotoOrderPhotosComplete>(PhotoOrderActionTypes.SubmitPhotoOrderPhotosComplete))
      .subscribe(submitPhotosComplete => {
        const toKeep : ListingPhoto[] = [];
        const toMove : ListingPhoto[] = [];
        // Move matching photos from working to submitted
        this.workingPhotos.forEach(workingPhoto => {
          const destination = (submitPhotosComplete.payload.includes(workingPhoto.uri)) ? toMove : toKeep;
          destination.push(workingPhoto);
        });
        this.workingPhotos = toKeep;
        this.submittedPhotos = this.submittedPhotos.concat(toMove);
      });
  }

  getPhotoOrder(photoOrderId) {
    // TODO - place in state
    if (photoOrderId) {
      this.photoOrder$ = this.photoOrderService.retrievePhotoOrder(photoOrderId);
    }
  }

  setPhotos() {
    this.storeSub = this.photoOrder$.subscribe(photoOrder => {
      if (photoOrder && photoOrder.marketingOrderId) {
        // TODO - dispatch event to load photos
        // TODO - this subscribe is not cleaned up. But should be eliminated with dispatch
        this.marketingOrderService.getOrderPhotos(photoOrder.marketingOrderId, photoOrder.photographerId).subscribe(photos => {
          if (photos) {
            this.submittedPhotos = photos;
          }
        });
        this.photoOrderService.retrievePhotoOrder(photoOrder._id).subscribe(photoOrder => {
          this.workingPhotos = photoOrder.photos;
        });
      }
    });
  }

  uploadResult(photoOrder: PhotoOrder, newPhotos: UploadPhoto[]): void {
    this.store.dispatch(new UploadPhotoOrderPhotos(photoOrder, newPhotos));
  }

  updateWorkingPhotos(photoOrder: PhotoOrder, photos: ListingPhoto[]) {
    this.store.dispatch(new UpdatePhotoOrderPhotos(photoOrder, photos));
  }

  submitPhotos(photoOrder: PhotoOrder, photos: ListingPhoto[]) {
    const photoUrls = photos.map(photo => photo.uri);
    this.store.dispatch(new SubmitPhotoOrderPhotos(photoOrder._id, photoUrls));
  }

  completeOrder(photoOrder: PhotoOrder) {
    // marketingOrder.getSelectedPackage().getPhotographyProduct().serviceStatus = "Completed";
    this.store.dispatch(new UpdatePhotoOrder(
      new PhotoOrder({_id: photoOrder._id, status: this.orderStatus.DONE})));

    // subscribe the the action completing
    this.orderCompleteSubject = this.actionSubject.pipe(
      ofType<UpdatePhotoOrderComplete>(PhotoOrderActionTypes.UpdatePhotoOrderComplete)
    ).subscribe((data: UpdatePhotoOrderComplete) => {
      this.router.navigate([`/appointment-list/${photoOrder.photographerId}`],
        { queryParams: { photographerName: this.photographerName } });
    });

  }

  ngOnDestroy(): void {
    if (this.storeSub) {
      this.storeSub.unsubscribe();
    }
    if (this.orderCompleteSubject) {
      this.orderCompleteSubject.unsubscribe();
    }
    if (this.updatePhotoOrderPhotosCompleteSub) {
      this.updatePhotoOrderPhotosCompleteSub.unsubscribe();
    }
    if (this.updatePhotoSubmitCompleteSub) {
      this.updatePhotoSubmitCompleteSub.unsubscribe();
    }
  }
}
