import { Element, ElementMap, LCPage } from '../../../../../e2e/support';

export class MyTeamCreatePage extends LCPage {

  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Create Team',         field: 'button#create-team'}),
    ]
  );

  static formElements = new ElementMap( 'Team Profile',
  [
    new Element( { name: 'Team Name', model: 'name',         field: 'input#team-name-input'}),
    new Element( { name: 'Office Name',     model: 'number',       field: 'input#phone-input'}),
    new Element( { name: 'Phone',     model: 'number',       field: 'input#phone-input'}),
    new Element( { name: 'Website',   model: 'websiteURL',   field: 'input#website-input'}),
    new Element( { name: 'Email',     model: 'email',        field: 'input#email-input'}),
    new Element( { name: 'Slogan',    model: 'slogan',       field: 'textarea#slogan-input'}),
    new Element( { name: 'Photo',     model: 'teamPhotoURL', field: 'input#teamPhotoInput'}),
    new Element( { name: 'Logo',      model: 'teamLogoURL',  field: 'input#teamLogoInput'})
  ]
  );

  constructor() {
    super('Create Team', [MyTeamCreatePage.elements, MyTeamCreatePage.formElements]);
  }

}
