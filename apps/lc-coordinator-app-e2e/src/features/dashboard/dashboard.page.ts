import { ElementMap, Element, LCPage } from '../../../../../e2e';
import { MyOrdersPage } from './my-orders/my-orders.page';
import { AllOrdersPage } from './all-orders/all-orders.page';
import { CompletedOrdersPage } from './completed-orders/completed-orders.page';

export class DashboardPage extends LCPage {
  static elements = new ElementMap( 'page',
    [
      new Element( { name: 'Header', field: 'h1'}),
      new Element( { name: 'My Orders Tab', field: '#my-orders'}),
      new Element( { name: 'All Orders Tab', field: '#all-orders'}),
      new Element( { name: 'Completed Orders Tab', field: '#completed-orders'}),
    ]
  );

  constructor() {
    super('Main', [DashboardPage.elements]);
  }
}

export const pages = {
  'dashboard': {
    'main' : {
      url: '/orders',
      page: new DashboardPage()
    },
    'my orders': {
      url: '/orders/mine',
      page: new MyOrdersPage()
    },
    'all orders': {
      url: '/orders',
      page: new AllOrdersPage()
    },
    'completed orders': {
      url: '/orders/completed',
      page: new CompletedOrdersPage()
    }
  },
};
