import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadResultsDialogComponent } from './upload-results-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UploadResultsDialogService } from './upload-results-dialog.service';

describe('UploadResultsDialogComponent', () => {
  let component: UploadResultsDialogComponent;
  let fixture: ComponentFixture<UploadResultsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ NgbModule ],
      declarations: [ UploadResultsDialogComponent ],
      providers: [ UploadResultsDialogService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadResultsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
