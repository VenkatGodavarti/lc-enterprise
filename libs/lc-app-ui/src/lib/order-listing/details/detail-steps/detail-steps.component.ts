import { Component, OnInit } from '@angular/core';
import { FlowState, ORDERSTATE } from '@lc/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

/**
 * This is the component used on the left side of the page when in substeps of the order flow
 */

@Component({
  selector: 'lc-detail-steps',
  templateUrl: './detail-steps.component.html',
  styleUrls: ['./detail-steps.component.scss']
})
export class DetailStepsComponent implements OnInit {
  public readonly orderState$: Observable<FlowState>;

  constructor(private store: Store<any>) {
    this.orderState$ = store.select(ORDERSTATE);
  }

  ngOnInit() {
  }
}
