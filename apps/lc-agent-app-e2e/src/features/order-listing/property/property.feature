@login-before-all
Feature: Listing Details Property Page

Background:
  # Login to the app and create the Alamo order
  Given I am logged in to Listing Concierge
  Given the following orders exist
  | Name                 | Alamo                 |
  | Package              | Gold                  |
  | Status               | OPEN                  |
  | Property Information | default               |
  | Shipping Address     |                       |
  | Billing Address      |                       |

  And the order is on the property step
  # TODO: Set selected package

  When I go to the Main page in Listing Dashboard
  Then the Main page from Listing Dashboard is loaded

  # Navigate to the Order Details
  When I wait for the Add Listing button to load
  When I click the Complete Order button on the last order
  Then the Order Listing Details page from Order Listing is loaded

  # Navigate into Property Page
  And I wait for the Listing Details Continue button to load
  When I click on the Listing Details Continue button
  Then the Order Listing Property page from Order Listing is loaded
  When I wait for the Next button to load

Scenario: Property Page Form Validation

  # Don't enter in any form data, however verify that the bedrooms/bathrooms cannot go less than 0
  Then the "Bedroom Count" text is "0"
  When I click the Bedroom Decrement button 2 times
  Then the "Bedroom Count" text is "0"

  Then the "Bathroom Count" text is "0"
  When I click the Bathroom Decrement button 2 times
  Then the "Bathroom Count" text is "0"

  # Validate List Price doesn't accept alphabetic characters
  When I enter "Abc&^%" into the "List Price"
  Then the "List Price" value is ""

  # Validate the list Price doesn't accept decimals
  When I enter "2999.99" into the "List Price"
  Then the "List Price" value is "299999"

  # Validate Sq Ft doesn't accept alphabetic characters
  When I enter "Abc&^%" into the "Square Feet"
  Then the "Square Feet" value is ""

  # Validate Sq Ft accepts numbers and decimals
  When I enter "2000.25" into the "Square Feet"
  Then the "Square Feet" value is "2000.25"

  # Validate Lot Size doesn't accept alphabetic characters
  When I enter "Abc&^%" into the "Lot Size"
  Then the "Lot Size" value is ""

  # Validate Lot Size accepts numbers and decimals
  When I enter "4000.25" into the "Lot Size"
  Then the "Lot Size" value is "4000.25"
