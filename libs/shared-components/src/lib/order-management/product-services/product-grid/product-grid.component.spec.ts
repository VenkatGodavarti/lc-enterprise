import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductGridComponent } from './product-grid.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, ViewChild } from '@angular/core';
import { ProductType } from '@lc/core';
import {By} from "@angular/platform-browser";

describe('ProductGridComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let grid;
  let products;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestHostComponent, ProductGridComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    grid = component.productGrid;
    products = component.createDefaultProducts();
    grid.products = products;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(grid).toBeTruthy();
  });

  it('should display all products', () => {
    expect(grid).toBeTruthy();

    const productCards = fixture.debugElement.queryAll(By.css('div.service-product-card'));
    expect(productCards.length).toEqual(products.length);
  });
});

/**
 * A simple component ysed to host the photo grid during tests so that web can set its Inpiut listing value
 * properly.
 *
 * TestHostComponent has some test methods for creating  a bare-bones listing with photos for testing.
 * the listing object will not be saved to mongo or sent to a rest api call with validation so they are minimal listings
 *
 * We ar enot testing image retrieval so image urls are not real image urls.
 *
 */
@Component({
  selector: 'lc-test-host',
  template: '<lc-product-grid></lc-product-grid>'
})
class TestHostComponent {
  @ViewChild(ProductGridComponent)
  public productGrid: ProductGridComponent;

  createDefaultProducts() {
    const products = [];

    let product = new ProductType();
    product.deserialize({ title: 'product-1', code: '001', category: 'print', status: 'Submitted'});
    products.push(product);

    product = new ProductType();
    product.deserialize({ title: 'product-2', code: '002', category: 'print', status: 'Submitted'});
    products.push(product);

    product = new ProductType();
    product.deserialize({ title: 'product-3', code: '003', category: 'digital', status: 'Completed'});
    products.push(product);

    return products;

  }
}
