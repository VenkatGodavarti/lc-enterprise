import { Injectable } from '@angular/core';
import { AuthenticationService, OktaAuthenticationProvider } from '@lc/core';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';


export interface ActivityTimeoutConfirmation {
  confirmContinue(continueConfirmationHandler:ContinueConfirmationHandler):void,
  clearRequest():void
}

export interface ContinueConfirmationHandler {
  confirmInactivityContinue():void,
  confirmInactivityLogout():void
}

/**
 * Responsible for monitoring user activity and timeout events such as inactivity logout, token refresh, etc.
 */
@Injectable()
export class UserActivityService implements ContinueConfirmationHandler {

  private activityTimeoutConfirmation: ActivityTimeoutConfirmation;

  // Indicates confirmation request is active (dialog is showing)
  private  confirming:boolean;

  constructor(
    private okta: OktaAuthenticationProvider,
    private router: Router,
    private userIdle: UserIdleService,
    private authenticationService: AuthenticationService) {
  }

  startWatching() {
    this.authenticationService.setUserActivityService(this);

    // Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      if (count && this.activityTimeoutConfirmation && !this.confirming) {
        this.confirming = true;
        this.activityTimeoutConfirmation.confirmContinue(this);
      }
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      this.confirmInactivityLogout();
    });
  }

  setActivityTimeoutConfirmation(activityTimeoutConfirmation: ActivityTimeoutConfirmation) {
    this.activityTimeoutConfirmation = activityTimeoutConfirmation;
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  confirmInactivityContinue(): void {
    this.confirming = false;
    if (this.activityTimeoutConfirmation) {
      this.activityTimeoutConfirmation.clearRequest();
    }
    if (this.userIdle) {
      this.userIdle.resetTimer();
    }
  }

  confirmInactivityLogout(): void {
    this.confirming = true;
    if (this.activityTimeoutConfirmation) {
      this.activityTimeoutConfirmation.clearRequest();
    }
    if (this.userIdle) {
      this.userIdle.stopWatching();
    }
    this.router.navigate(['logout']);
  }
}
