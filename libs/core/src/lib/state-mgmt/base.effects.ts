import { Observable, of } from "rxjs";
import { ErrorData } from '../errors/error-data';
import { GlobalErrorHandler } from '../errors/global-error-handler';

export abstract class BaseEffects {

  /**
   * Abstract constructor used to enfource the error handling of effects
   * @param errorHandler Error handler to use while processing errors
   */
  constructor(protected readonly errorHandler: GlobalErrorHandler){
  }

  processCatchError(type, payload, err): Observable<ErrorData> {
    const errorPayload = new ErrorData(type,
      {
        original: payload,
        error: err
      }
    );
    this.errorHandler.handleError(errorPayload);
    return of(errorPayload);
  }
}
