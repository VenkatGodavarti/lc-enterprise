
import { MultimediaPage } from './media/multimedia/multimedia.page';
import { ConfirmationPage } from '../../../../../e2e/pages/order-management/confirmation.page';
import { SummaryPage } from '../../../../../e2e/pages/order-management/summary.page';
import { ListingDetailsPage } from '../../../../../e2e/pages/order-management/listing-details.page';
import { PhotoManagementPage } from './media/photo/photo-management.page';

/**
 *
 *  Page mapping for Order Managememnt pages
 */
export const orderManagementPages = {

  'order management': {
    'summary': {
      url: '/manage-order/:orderId/summary',
      page: new SummaryPage()
    },
    'multimedia': {
      url: '/manage-order/:orderId/media/multimedia',
      page: new MultimediaPage()
    },
    'photos': {
      url: '/manage-order/:orderId/media/photos',
      page: new PhotoManagementPage()
    },
    'confirmation': {
      url: '/manage-order/:orderId/confirmation',
      page: new ConfirmationPage()
    },

    'listing details': {
      url: '/manage-order/:orderId/listing-details',
      page: new ListingDetailsPage()
    }
  }
};
