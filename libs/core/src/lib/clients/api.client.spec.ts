import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiClient } from './api.client';
import { FormGroup, FormControl } from '@angular/forms';

describe('ApiClient', () => {

  let testController: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      HttpClientTestingModule
    ],
    providers: [
      ApiClient
    ]
  }));

  beforeEach(() => {
    testController = TestBed.get(HttpTestingController);
  })

  afterEach(() => {
    testController.verify();
  });

  it('should be created', () => {
    const service: ApiClient = TestBed.get(ApiClient);
    expect(service).toBeTruthy();
  });

  it('should call GET method', () => {
    const testUrl = 'http://test.com/';
    const service: ApiClient = TestBed.get(ApiClient);
    service.get(testUrl).subscribe(() => {});

    const request = testController.expectOne(testUrl);
    expect(request.request.method).toBe('GET');
  });

  it('should call GET method with Id', async() => {
    const testUrl = 'http://test.com/123';
    const service: ApiClient = TestBed.get(ApiClient);
    service.get(testUrl).subscribe(() => {});

    const request = testController.expectOne(testUrl);
    expect(request.request.method).toBe('GET');
  });

  it('should call POST method', async() => {
    const service: ApiClient = TestBed.get(ApiClient);

    const testUrl = 'http://test.com/';
    const form = new FormGroup({ name: new FormControl()});
    const disableMethod = spyOn(form, 'disable');
    const enableMethod = spyOn(form, 'enable');

    service.post(testUrl, form).subscribe(() => {});

    const request = testController.expectOne(testUrl);
    expect(request.request.method).toBe('POST');
    request.flush({});

    expect(disableMethod).toHaveBeenCalled();
    expect(enableMethod).toHaveBeenCalled();
  });

  it('should call PUT method', async() => {
    const service: ApiClient = TestBed.get(ApiClient)
    const testUrl = 'http://test.com/';
    const form = new FormGroup({ name: new FormControl()});
    const disableMethod = spyOn(form, 'disable');
    const enableMethod = spyOn(form, 'enable');

    service.put(testUrl, form).subscribe(() => {});

    const request = testController.expectOne(testUrl);
    expect(request.request.method).toBe('PUT');
    request.flush({});

    expect(disableMethod).toHaveBeenCalled();
    expect(enableMethod).toHaveBeenCalled();
  });
});
