import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OktaAuthenticationProvider } from './auth-providers/okta-authentication.provider';
import { BaseAuthenticationProvider } from './auth-providers/base-authentication.provider';
import { UserService } from './user.service';
import { environment } from '../../../../environments/environment';
import { UserActivityService } from '../../../../shared-components/src/lib/auth/user-activity/user-activity-service';

@Injectable()
export class AuthenticationService {

  public activeAuthService: BaseAuthenticationProvider;

  public currentQueryParams: {};

  private userActivityService: UserActivityService;

  constructor(
    private userService: UserService,
    private oktaAuthService: OktaAuthenticationProvider,
    public router: Router,
    private route: ActivatedRoute,
  ) {
    this.activeAuthService = this.oktaAuthService;

    this.route.queryParams.subscribe(params => {
      this.currentQueryParams = params;
    });
  }

  redirectToLogin() {
    this.activeAuthService.redirectToLogin();
  }

  redirectPostLogin() {
    const postLoginRedirectTo: string = localStorage.getItem('post-login-redirect');
    if (postLoginRedirectTo && postLoginRedirectTo.length > 0)
      this.router.navigateByUrl(postLoginRedirectTo);
    else
      this.router.navigate(['']);

    localStorage.removeItem('post-login-redirect');
  }

  redirectToLogout() {
    this.router.navigate(['logout']);
  }

  logout(): void {
    this.activeAuthService.logout();
    if (localStorage.getItem('impersonatedUser'))
      localStorage.removeItem('impersonatedUser');

    // We are logged out so don't need to watch anymore
    if (this.userActivityService) {
      this.userActivityService.stopWatching();
    }
  }

  /* istanbul ignore next */
  getToken(): string {
    return this.activeAuthService.getToken();
  }

  /* istanbul ignore next */
  handle401Response() {
    this.activeAuthService.handle401Response();
  }

  /* istanbul ignore next */
  getHeaderOptions(): any {
    return this.activeAuthService.getHeaderOptions();
  }

  isAuthenticated(): boolean {
    if (environment.envName === 'integration') {
      return true;
    }
    return this.activeAuthService.isAuthenticated() && this.userService.getCurrentUser() !== null;
  }

  // Setter to avoid injection circular dependency
  setUserActivityService(userActivitService:UserActivityService) {
    this.userActivityService = userActivitService;
  }

}
