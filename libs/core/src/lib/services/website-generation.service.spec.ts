import {getTestBed, TestBed} from '@angular/core/testing';
import {
  OutputMimeType, WebsiteGenerationService, ProductInstance, MarketingOrderService
} from '@lc/core';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { MarketingOrderFactory } from '../factories';
import { of } from 'rxjs';

describe('WebsiteGenerationService', () => {

  let marketingOrder;
  let product;
  let httpClient;
  let marketingOrderServiceSpy;
  let websiteGenerationService;

  let injector: TestBed;

  beforeEach(() => {
    marketingOrder = MarketingOrderFactory.createMarketingOrder(null, null, null);
    product = new ProductInstance({
      title: 'Flyer',
      kind: 'product',
      category: 'print',
      selectedTemplate: {
        mimeType: OutputMimeType.html
      }
    });
    marketingOrderServiceSpy = jasmine.createSpyObj('MarketingOrderService', ['fulfill']);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        WebsiteGenerationService,
        { provide: MarketingOrderService, useValue: marketingOrderServiceSpy }
        ]
    });

    injector = getTestBed();
    httpClient = injector.get(HttpTestingController);
    websiteGenerationService = TestBed.get(WebsiteGenerationService);
  });

  it('generate()', () => {

    const data = 'some-data';
    marketingOrderServiceSpy.fulfill.and.returnValue(of(data));
    websiteGenerationService.generate(marketingOrder, product).subscribe( (content) => {

      expect(content).toEqual(data);

      httpClient.expectOne( (req) => {
        return req.url.includes('/assets/listing-site/index.html');
      });

    });

  });

});

