export const environment = {
  // App configuration
  production: false,
  envName: 'ft',
  applicationName: 'Listing Concierge Photo App FT',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://lc.ft.lc-photo.realogy.com/',
  apiBaseURL: 'https://lc.ft.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.ft.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.ft.lc-notify.realogy.com/api/v1/', // TODO: Update

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajzupdnl5atNLev0h7',
  oktaRedirectUri: 'https://lc.ft.lc-photo.realogy.com/implicit/callback',
};
