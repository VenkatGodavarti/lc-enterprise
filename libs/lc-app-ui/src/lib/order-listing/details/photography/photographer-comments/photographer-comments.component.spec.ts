import { async, ComponentFixture, fakeAsync, flush, TestBed, inject } from '@angular/core/testing';

import { PhotographerCommentsComponent } from './photographer-comments.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import {
  FlowState,
  Listing,
  Lockbox,
  PhotographerAppointment,
  ORDERSTATE,
  MarketingOrder, SELECTEDORDER
} from '@lc/core';
import { Store } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { OrderStepsComponent } from '../../../steps/order-steps/order-steps.component';
import { OrderStepTemplateComponent } from '../../../steps/order-steps/order-step-template/order-step-template.component';
import { DetailStepsComponent } from '../../detail-steps/detail-steps.component';
import { DetailStepTemplateComponent } from '../../detail-steps/detail-step-template/detail-step-template.component';
import { By } from '@angular/platform-browser';
import { StoreMock, TestingModule } from '@lc/testing';
import { PackageInstanceFactory} from '../../../../../../../core/src/lib/factories';
import { StepContentComponent } from '../../step-content/step-content.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedComponentsUiModule } from '@lc/shared-components-ui';

describe('PhotographerCommentsComponent', () => {
  let component: PhotographerCommentsComponent;
  let fixture: ComponentFixture<PhotographerCommentsComponent>;
  let order: MarketingOrder;
  let store: StoreMock<any>;

  const code = '1234';
  const location = 'on a door';
  const comments = 'these are comments';
  const available = true;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        NgbModule,
        TestingModule,
        SharedComponentsUiModule
      ],
      declarations: [
        PhotographerCommentsComponent,
        OrderStepsComponent,
        OrderStepTemplateComponent,
        DetailStepsComponent,
        DetailStepTemplateComponent,
        StepContentComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {
            parent: {
              snapshot: {
                paramMap: convertToParamMap({
                  id: '1'
                })
              }
            }
          }
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  // Configure the TestStore
  beforeEach(inject([Store], (mockStore: StoreMock<any>) => {
    store = mockStore;
    store.setState(ORDERSTATE, new FlowState(null)); // set default state
    order = createOrder(true);
    store.setState(SELECTEDORDER, order);
  }));

  // Create the component
  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(PhotographerCommentsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
    flush();
  }));

  it('should have fully populated FormGroup', async() => {
    expect(component).toBeTruthy();

    expect(component.infoForm).toBeTruthy();
    expect(component.infoForm.lockbox.code).toBe(code);
    expect(component.infoForm.lockbox.location).toBe(location);
    expect(component.infoForm.comments).toBe(comments);
    expect(component.infoForm.lockboxAvailable).toBe(available);
  });

  it('should have new FormGroup', async() => {
    expect(component).toBeTruthy();
    store.setState(SELECTEDORDER, createOrder(false));

    expect(component.infoForm).toBeTruthy();
    expect(component.infoForm.lockbox.code).toBe(null);
    expect(component.infoForm.lockbox.location).toBe(null);
    expect(component.infoForm.comments).toBe(null);
    expect(component.infoForm.lockboxAvailable).toBe(!available);
  });

  it('#lockboxAvailable should hide fields when false', fakeAsync(() => {
    const checkbox = fixture.debugElement.query(By.css('#lockbox-chkbox')).nativeElement;
    const div: HTMLDivElement = fixture.debugElement.query(By.css('#code-location-div')).nativeElement;

    expect(checkbox.checked).toBeTruthy();
    expect(div.hidden).toBeFalsy();

    checkbox.click();
    fixture.detectChanges();

    expect(checkbox.checked).toBeFalsy();
    expect(div.hidden).toBeTruthy();
  }));

  it('#onSubmit should populate a PhotographyAppointment with lockbox info and comments', fakeAsync(() => {
    store.setState(SELECTEDORDER, createOrder(false));
    component.marketingOrder$.subscribe(marketingOrder => {
      const product = marketingOrder.getPhotographyProduct();
      const appointment = product ? product.serviceAppointment : null;
      expect(appointment).toBeFalsy();
    });
    fixture.detectChanges();
    flush();

    spyOn(component, 'onSubmit');

    const checkbox = fixture.debugElement.query(By.css('#lockbox-chkbox')).nativeElement;
    expect(checkbox.checked).toBeFalsy('checkbox is checked and shouldnt be');
    checkbox.click();
    expect(checkbox.checked).toBeTruthy('checkbox is not checked');

    const codeInput = fixture.debugElement.query(By.css('#lockbox-code')).nativeElement;
    codeInput.value = code;
    codeInput.dispatchEvent(new Event('input'));

    const locationInput = fixture.debugElement.query(By.css('#lockbox-location')).nativeElement;
    locationInput.value = location;
    locationInput.dispatchEvent(new Event('input'));

    const commentsInput = fixture.debugElement.query(By.css('#comments-input')).nativeElement;
    commentsInput.value = comments;
    commentsInput.dispatchEvent(new Event('input'));

    fixture.detectChanges();
  }));


  function createOrder(hasAppt: boolean) {

    const newListing = new Listing();
    newListing._id = '123456789ABCDEF';
    newListing.listPrice = 400000;
    newListing.sqFt = 4000;
    newListing.lotSqFt = 14000;
    newListing.listDate = new Date();

    let newAppt = new PhotographerAppointment();
    newAppt.lockbox = new Lockbox();
    newAppt.lockbox.code = code;
    newAppt.lockbox.location = location;
    newAppt.comments = comments;
    newAppt.lockboxAvailable = available;

    const newOrder : MarketingOrder = new MarketingOrder();
    newOrder.listing = newListing;

    newOrder.availablePackages = PackageInstanceFactory.createAll();
    newOrder.selectedPackageCode = newOrder.availablePackages[1].code;

    if (hasAppt) {
      newAppt = new PhotographerAppointment();

      newAppt.lockbox = new Lockbox();
      newAppt.lockbox.code = code;
      newAppt.lockbox.location = location;
      newAppt.comments = comments;

      newAppt.lockboxAvailable = available;
      newOrder.getPhotographyProduct().serviceAppointment = newAppt;
    }
    return newOrder;
  }
});
