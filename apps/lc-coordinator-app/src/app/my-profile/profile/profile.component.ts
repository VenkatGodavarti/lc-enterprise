import { Component, OnInit } from '@angular/core';
import { Masks, ImageCropperDialogService, CropSize, ImageCropperResponse } from '@lc/shared-components-ui';
import { ProfileForm, Profile, PhoneNumberForm, SELECTEDPROFILE, UpdateProfile, PhotoService, PhoneType, ProfileService, PhoneNumber } from '@lc/core';
import { Observable, combineLatest } from 'rxjs';
import { FormArray, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'lc-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  readonly phoneMask = Masks.phoneNumber;

  readonly profile$: Observable<Profile>;
  public form: ProfileForm;
  public phoneNumber: PhoneNumberForm;

  constructor(private store: Store<any>, private dialogService: ImageCropperDialogService, private photoService: PhotoService, private profileService: ProfileService) {
    // Load the profile, presign the image and then initialize the form.
    this.profile$ = store.pipe(
      select<Profile>(SELECTEDPROFILE),
      filter(profile => profile != null),
      tap(profile => this.initializeForm(profile))
    );
  }

  ngOnInit() {
  }

  onCancel() {
    // Patch the form with the original profile data
    this.form.patchValue(this.form.originalValue);
    this.phoneNumber.reset(this.getPhoneNumber(this.form.originalValue));
  }

  private getPhoneNumber(profile: Profile) {
    if((profile.phoneNumbers || []).length > 0){
      return profile.phoneNumbers[0];
    }
    return new PhoneNumber({ type: PhoneType.direct, priority: 0 });
  }

  onSubmit(form: ProfileForm) {
    // When user performs a form action, we need to update controls as dirty to
    // reveal any form validation errors. This is to be a system-wide practice
    form.markAllAsDirty();
    if(form.invalid) { return; }

    this.store.dispatch(new UpdateProfile(form.value));
  }

  onEditPhoto(profile: Profile) {
    if(profile.originalPhotoUrl){
      // If there is an image, we need to load that as a default
      this.photoService.getImage(profile.originalPhotoUrl)
        .subscribe(res => this.cropImage(profile, res[0].body));
    } else {
      // Otherwise, open up the cropper without an image
      this.cropImage(profile);
    }
  }

  private cropImage(profile: Profile, file?: any){
    // Open up the image dialog with the crop size we want to preview it as
    this.dialogService.openImageCropper(file, [new CropSize('Avatar', 80, 80, true)], null)
      .then(response => {
        if(!response) { return; }

        // If the user did an action, update appropriately
        if(response.isDelete) {
          this.deleteImage(profile);
        } else if(response.croppedFile) {
          this.uploadImage(profile, response);
        }
      });
  }

  private deleteImage(profile: Profile) {
    this.updateImages(profile, null, null);
  }

  private uploadImage(profile: Profile, response: ImageCropperResponse) {
    if(!response){ return; }

    // Set the name on the cropped Blob or File
    (<any>response.croppedFile).name = "profilePhoto.png";

    // Upload images synchronously and then update the profile with both images
    combineLatest(this.photoService.upload(response.originalFile), this.photoService.upload(response.croppedFile))
      .subscribe(([originalPhotos, croppedPhotos]) => {
        // Find the original images from the response and then call update
        const original: any = originalPhotos.find((photo: any) => photo.original);
        const cropped: any = croppedPhotos.find((photo: any) => photo.original);
        this.updateImages(profile, original.url, cropped.url);
      });
  }

  private updateImages(profile: Profile, originalUrl: string, croppedUrl: string) {
    profile.photoUrl = croppedUrl;
    profile.originalPhotoUrl = originalUrl;

    // We need to first put it into a Profile Form,  which will only emit the values
    // that are available for the POST/PUT operations. Do not use the existing form on the component
    // as it could have pending changes.
    this.store.dispatch(new UpdateProfile(new ProfileForm(profile).value));
  }

  private initializeForm(profile?: Profile) {
    this.form = new ProfileForm(profile);

    // NOTE: I do not like setting validators like this. It should be done in the form, but requirements are different
    // between the agent App and the coordinator app...
    this.form.get('preferredFirstName').setValidators(Validators.required);
    this.form.get('preferredLastName').setValidators(Validators.required);
    this.form.get('preferredEmail').setValidators(Validators.required);

    const phoneNumbers = (this.form.get('phoneNumbers') as FormArray);
    if(phoneNumbers.length === 0) {
      phoneNumbers.push(new PhoneNumberForm(this.getPhoneNumber(profile)));
    }
    this.phoneNumber = phoneNumbers.at(0) as PhoneNumberForm;
  }

}
