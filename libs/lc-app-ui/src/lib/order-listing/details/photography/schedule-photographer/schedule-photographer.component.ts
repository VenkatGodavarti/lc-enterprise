import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import {
  WorkflowAction,
  ActionTypes,
  PhotographerAppointment,
  SELECTEDORDER, MarketingOrder, UpdateOrder
} from '@lc/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { filter, tap } from 'rxjs/operators';

/**
 * SchedulePhotographerComponent is responsible for displaying date and time
 * widgets so the user can choose a date and time for scheduling a photography
 * appointment.  The output is stored in fields of
 * the Listing.PhotographerAppointment object.
 */
@Component({
  selector: 'lc-app-schedule-photographer',
  templateUrl: './schedule-photographer.component.html',
  styleUrls: ['./schedule-photographer.component.scss']
})
export class SchedulePhotographerComponent {
  marketingOrder$: Observable<MarketingOrder>;

  selectedTime = {hour: 8, minute: 0};
  today:Date = new Date();
  tomorrow:Date = new Date();
  earliestDate:any;
  selectedDate:any;

  params: ParamMap;

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              private store: Store<any>) {
    this.marketingOrder$ = store.select<MarketingOrder>(SELECTEDORDER).pipe(
      filter(order => order != null),
      tap(order => this.setSelectedDate(order))
    );

    this.params = this.activatedRoute.parent.snapshot.paramMap;

    // Don't allow scheduling until tomorrow
    this.tomorrow.setDate(this.today.getDate() + 1);
    this.earliestDate = {
      day: this.tomorrow.getDate(),
      month: this.tomorrow.getMonth() + 1, // convert to ISO 8601 format for NgbDate (Jan=1, Feb=2,...)
      year:this.tomorrow.getFullYear()};
  }

  setSelectedDate(marketingOrder: MarketingOrder) {
    const product = marketingOrder ? marketingOrder.getPhotographyProduct() : undefined;
    const appointment = product ? product.serviceAppointment : undefined;
    if(appointment && appointment.scheduledAt) {
      const scheduled = new Date(appointment.scheduledAt);
      this.selectedDate = {
        year: scheduled.getFullYear(),
        month: scheduled.getMonth() + 1, // convert to ISO 8601 format for NgbDate (Jan=1, Feb=2,...)
        day: scheduled.getDate()
      };

      this.selectedTime = {hour: scheduled.getHours(), minute: scheduled.getMinutes()};
    }
  }
  // populate the selected date from the event triggered on the calendar datepicker
  onDateSelect($event: NgbDate) {
    this.selectedDate = $event;
  }

  goBack() {
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionPrev, { }, this.params));
  }

  onSubmit(marketingOrder: MarketingOrder) {
    const product = marketingOrder.getPhotographyProduct();

    // create new PhotographyAppointment if one doesn't yet exist
    product.serviceAppointment = product.serviceAppointment || new PhotographerAppointment();
    const appointment = product ? product.serviceAppointment : undefined;

    //combine date and time choices in to a single field
    appointment.scheduledAt =
      new Date(
        this.selectedDate.year,
        this.selectedDate.month - 1, // convert back from ISO 8601 format before saving
        this.selectedDate.day,
        this.selectedTime.hour,
        this.selectedTime.minute
      );

    this.store.dispatch(new UpdateOrder(marketingOrder));
    this.store.dispatch(new WorkflowAction(ActionTypes.WorkflowActionNext, { stepCompleted: true },
      this.params));
  }
}
