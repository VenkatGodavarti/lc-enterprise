
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { by, ElementFinder, browser } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^the "(.*)" table has the columns "(.*)"$/, {timeout: config.timeout }, async (elementName, columns) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const table = page.finder(elementName);
  const headers = await getTableHeaders(table);

  const columnNames = columns.split(',').map(column => column.trim());
  columnNames.forEach(column => {
    const header = headers.find(header => header === column);
    expect(header).to.be.equal(column);
  })
});

/**
 * Matches the table values with the corresponding table headers.
 * This uses header text instead of Element name
 */
Then(/^the "(.*)" table has the following data$/, {timeout: config.timeout }, async (elementName, dataTable) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const table = page.finder(elementName);
  const headers = await getTableHeaders(table);

  const records = tableToRecords(dataTable);
  const promises = records.map(async (record, rowIndex) => {
    for(const key in record) {
      // Assert all the columns exist
      const columnIndex = headers.indexOf(key);
      if(columnIndex < 0) {
        throw Error('Column not found! Please use column text in Gherkin');
      }

      // We have the index, we need to find the cell:
      const row: ElementFinder = await table.all(by.css('tbody tr')).get(rowIndex);
      const cell: ElementFinder = await row.all(by.css('td')).get(columnIndex);
      const text = await cell.getText();
      expect(text).to.be.equal(parseValue(record[key]));
    }
  });

  await Promise.all(promises);
});

Then(/^the "(.*)" table has "(.*)" (record|records)$/, {timeout: config.timeout }, async (elementName, recordCount, recordOrRecords) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const table = page.finder(elementName);

  const rows = table.all(by.css('tbody tr'));
  await browser.wait(() => rows.count().then(rowCount => rowCount === +recordCount), 5000, `Table did not load ${recordCount} in the appropriate time`);
});

When(/^I click on the "(.*)" button in row "(.*)" of the "(.*)" table$/, {timeout: config.timeout }, async (elementName, recordNumber, tableName) => {
  const button = await findElementInRow(tableName, elementName, +recordNumber);
  await button.click();
});

When(/^the "(.*)" table records load$/, {timeout: config.timeout }, async (elementName) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const table = page.finder(elementName);

  const rows = table.all(by.css('tbody tr'));
  await browser.wait(() => rows.count().then(rowCount => rowCount > 0), 5000);
});

async function getTableHeaders(table) {
  const headers = await table.all(by.css('th')).map(async (header) => {
    const text = await header.getAttribute("textContent");
    return text;
  });
  return headers;
}

function parseValue(value) {
  const user = Context.get(ContextKeys.user);
  return value
    .replace('{{userFirstName}}', user.firstName)
    .replace('{{userLastName}}', user.lastName);
}

export async function findElementInRow(tableName: string, elementName: string, rowNumber: number) {
  const page = Context.get<LCPage>(ContextKeys.page);
  const table = page.finder(tableName);

  const elementPath = page.anyElement(elementName).field;
  const row: ElementFinder = await table.all(by.css('tbody tr')).get(rowNumber - 1);
  const element: ElementFinder = row.$(elementPath);
  return element;
}

export function tableToRecords(dataTable: any): any[] {
  const data: Array<any> = dataTable.rawTable;

  const header = data.splice(0, 1)[0];
  const orders = data.map(record => ({}));
  Object.keys(header).forEach((key, propertyIndex) => {
    const propertyName = header[key];
    orders.forEach((record, recordIndex) => {
      record[propertyName] = data[recordIndex][propertyIndex];
    });
  });
  return orders;
}


export function verticalTableToRecords(dataTable: any): any[] {
  const rows = dataTable.raw();
  const records = [];

  // Column names come from the first column
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];

    Object.keys(row).forEach((key, propertyIndex) => {
      if(propertyIndex === 0) {
        return;
      }
      if(records.length < propertyIndex) { //length = 0, property index = 1 Needs new record
        records.push({});
      }

      const record = records[propertyIndex - 1];

      const propertyName = row[0];
      let value = row[propertyIndex];
      if(value.length === 0) {
        value = null;
      }
      record[propertyName] = value;
    });
  }
  return records;
}
