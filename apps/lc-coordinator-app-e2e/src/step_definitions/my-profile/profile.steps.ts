import { Then } from 'cucumber';
import { LCPage, Context, ContextKeys, Element, Configuration as config, ProfileClient } from '../../../../../e2e';

const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

Then(/^my profile has been updated$/, {timeout: config.timeout}, async () => {

  const updatedUser = await ProfileClient.getCurrentUser();
  const updatedProfile = updatedUser.profile;

  const page = Context.get<LCPage>(ContextKeys.page);

  const formData = Context.get(ContextKeys.enteredFormData);

  for (const fieldName in formData) {
    //console.log('fieldName: ', fieldName);
    const element: Element = page.element(fieldName);
    if (element.model) {
      expect(updatedProfile[element.model]).to.be.equal(formData[fieldName]);
    }
  }

});
