import { HttpClient, HttpEventType, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { PhotoThumbnails, ThumbnailSpec } from '../models/photo-thumbnail.model';
import { Observable, forkJoin, Subject, throwError } from 'rxjs';
import { map, tap, filter } from 'rxjs/operators';
import { ListingPhoto } from  '../models/listing-photo.model';
import { UploadPhoto } from '../models/upload-photo.model';

const apiBase = environment.photosBaseURL;

export type ResponseHandler = (responseBody: any) => void;

@Injectable()
export class PhotoService {
  constructor(private http: HttpClient) {}

  /**
   * @param file File to upload
   * @param progress Will emit progress values (0-100) as upload happens
   * @param thumbnailSpec Hints to photo storage service about thumbnail sizes that will be needed.  If provided,
   * any thumbnail URLs provided by the photo storage service will be saved in the listing.
   */
  public upload(file: File, progress?: Subject<number>, thumbnailSpec?: ThumbnailSpec[]): Observable<UploadPhoto[]> {

    // Emit zero progress for start
    if(progress) {
      progress.next(0);
    }

    const formData: FormData = new FormData();
    formData.append('thumbnails', JSON.stringify(thumbnailSpec));
    formData.append('images', file, file.name);
    // for some reason, busboy in lcms-image is sometimes getting the thumbnails field
    // after the images, which means that it misses the thumbnails specification.
    // by including this next field as a marker, lcms-image will wait until it has
    // all fields to process the images, ensuring that the thumbnails field is available.
    // yes, this is an ugly hack
    formData.append('finalField', 'finalField');

    const url = apiBase + "upload";
    const req = new HttpRequest('POST', url, formData, {
      reportProgress: true
      // TODO: Credentials?
    });

    return this.http.request(req).pipe(
      tap(event => {
        // On the UploadProgress event, update the progress status
        if(progress && event.type === HttpEventType.UploadProgress ) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          progress.next(percentDone);
        }
      }),
      // Filter to only the HttpResponse
      filter(event => event instanceof HttpResponse),
      map((event: HttpResponse<any>) => {
        if(progress) {
          progress.complete();
        }

        if (event.status === 200) {
          // Use provided response handler
          return event.body[0];
        } else {
          console.error("Upload failed with response:" + event.status);
          throwError("Upload failed with response:" + event.status);
        }
      })
    );
  }

  // service to download single image WIP
  getImage(imageUrl: string) {
    // TODO: change api call to be initiated via apiservice once we have only one image source for downloading

    const filenameUrl = apiBase + "extractFilename";
    const params = new HttpParams()
      .set('imgurl', imageUrl);

    return forkJoin(
      this.http.get(imageUrl, { observe: 'response', responseType: 'blob' }),             // download image
      this.http.get(filenameUrl, { params, observe: 'response', responseType: 'json' })   // get original filename
    );
  }

  static createFromRawPhotos(photos: UploadPhoto[], userId: string, count: number, photographerId?: string): ListingPhoto[] {
    // Create an array of the original Listing Photos with the thumbnails attached
    const listingPhotos: ListingPhoto[] = photos
      .filter(photo => photo.original)
      .map(original => {
        const originalPhoto: ListingPhoto = new ListingPhoto();
        originalPhoto.uri = original.url;
        originalPhoto.order = count + 1;
        originalPhoto.uploadedBy = userId;

        if (photographerId) {
          originalPhoto.photographerId = photographerId;
        }

        // Enumerate all child thumbnails and attach to the original
        photos.filter(photo => !photo.original && photo.file === original.file).forEach(thumbnail => {
          if (originalPhoto.thumbnails === undefined) {
            originalPhoto.thumbnails = new PhotoThumbnails();
          }
          originalPhoto.thumbnails[thumbnail.width] = { 'uri': thumbnail.url };
        });
        count = count + 1; // Need to increment the count
        return originalPhoto;
      });

    return listingPhotos;
  }
}
