import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperDialogComponent, CropSize, ImageCropperResponse } from './image-cropper-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ImageCropperDialogService {

  constructor(private modal: NgbModal) { }

  openImageCropper(file: File, cropSizes: CropSize[], aspectRatio): Promise<ImageCropperResponse> {
    const dialogRef = this.modal.open(ImageCropperDialogComponent);
    const component: ImageCropperDialogComponent = dialogRef.componentInstance;
    component.initialize(file, cropSizes, aspectRatio);
    component.close.subscribe(response => dialogRef.close(response));
    return dialogRef.result.then((response: ImageCropperResponse) => response, () => null); // Handle dismiss and close
  }
}
