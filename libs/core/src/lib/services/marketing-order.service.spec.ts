import {getTestBed, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {environment} from '../../../../environments/environment';
import {ApiService} from './api.service';
import {MarketingOrderService} from './marketing-order.service';
import {MarketingOrder} from '../models/marketing-order.model';
import { Listing, ListingPhoto, UserService } from '@lc/core';
import { Store } from '@ngrx/store';

describe('MarketingOrderService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let service: MarketingOrderService;
  let resource;

  beforeEach(() => {
    let storeSpy = jasmine.createSpyObj('Store', ['dispatch']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService, MarketingOrderService, UserService,
        { provide: Store, useValue: storeSpy}
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    service = injector.get(MarketingOrderService);

    resource = environment.apiBaseURL + service.resource;
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getOrder', () => {
    it('should return an Observable<MarketingOrder>', () => {
      const orderId = 'ABCD1234';
      const marketingOrder = new MarketingOrder();
      marketingOrder._id = orderId;

      service.getOrder(orderId).subscribe(result => {
        expect(result).toEqual(marketingOrder);
        expect(result._id).toEqual(orderId);
      });

      const req = httpMock.expectOne(`${resource}/${orderId}`);
      expect(req.request.method).toBe('GET');
      // req.flush(marketingOrder);
    });
  });

  describe('#deleteOrder', () => {
    it('should remove the given marketing order', () => {
      const orderId = 'ABCD1234';
      const marketingOrder = new MarketingOrder();
      marketingOrder._id = orderId;

      service.deleteOrder(marketingOrder).subscribe(result => {
        expect(result).toBeNull();
      });

      const req = httpMock.expectOne(`${resource}/${orderId}`);
      expect(req.request.method).toBe('DELETE');
      // req.flush(marketingOrder);
    });
  });

  describe('#saveOrder', () => {
    it('should create a new MarketingOrder and return an Observable<MarketingOrder>', () => {
      const marketingOrder = new MarketingOrder();
      marketingOrder.listing = new Listing();
      marketingOrder.listing.marketingOrder = new MarketingOrder();

      service.saveOrder(marketingOrder).subscribe(result => {
        expect(result).toEqual(marketingOrder);
      });

      const req = httpMock.expectOne(`${resource}`);
      expect(req.request.method).toBe('POST');
      req.flush(marketingOrder);
    });

    it('should update an existing MarketingOrder and return an Observable<MarketingOrder>', () => {
      const orderId = 'ABCD1234';
      const marketingOrder = new MarketingOrder();
      marketingOrder._id = orderId;
      marketingOrder.listing = new Listing();
      marketingOrder.listing.marketingOrder = new MarketingOrder();

      service.saveOrder(marketingOrder).subscribe(result => {
        expect(result).toEqual(marketingOrder);
        expect(result._id).toBe(orderId);
      });

      const req = httpMock.expectOne(`${resource}/${marketingOrder._id}`);
      expect(req.request.method).toBe('PUT');
      req.flush(marketingOrder);
    });
  });

  /*
  Changing the  call to return an Observable has completely changed the behavior of the test, unable to resolve

  describe('#fulfill', () => {
    it('should call service fulfill', () => {
      const orderId = 'ABCD1234';
      const productId = '100';
      const template = '{{ address }}';

      service.fulfill(orderId, productId, template);

      const req = httpMock.expectOne(`${resource}/${orderId}/fulfill/${productId}/1`);
      expect(req.request.method).toBe('GET');
    });
  });
  */
});
