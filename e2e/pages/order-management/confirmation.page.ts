import { LCPage, Element, ElementMap } from '../../support';

const confirmationPageElements = new ElementMap( 'page',  [
    new Element({ name: 'Header', field: "h1"}),
    new Element({ name: 'Subheader', field: "h2" }),
    new Element({ name: 'Manage Order', field: "#manage-order-button" }),
  ]
);


export class ConfirmationPage extends LCPage {
  constructor() {
    super('Confirmation', [confirmationPageElements]);
  }
}
