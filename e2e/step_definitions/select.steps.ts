
import { Then, When } from 'cucumber';
import { Context, ContextKeys } from '../support';
import { LCPage } from '../support/lc.page';
import { Configuration as config } from '../configuration';
import { ElementArrayFinder } from 'protractor';

const chai = require('chai').use(require('chai-as-promised'));
const assert = chai.assert;

Then(/^the "(.*)" options are "(.*)"$/, {timeout: config.timeout }, async (fieldName, optionsString: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const select = page.finder(fieldName);

  // Get all expected and available options
  const expectedOptions = optionsString.split(',').map(option => option.trim());
  const availableOptions: string[] = await select.$$('option').map(async (option) => {
    const text = await option.getAttribute('textContent');
    return text.trim();
  });

  // Compare each of the expected options.
  // TODO: Should we also compare the proper ordering?
  expectedOptions.forEach(expected => {
    const available = availableOptions.find(opt => opt === expected);
    assert.equal(available, expected);
  })
});

Then(/^the selectable "(.*)" options are "(.*)"$/, {timeout: config.timeout }, async (fieldName, optionsString: string) => {
  const page = Context.get<LCPage>(ContextKeys.page);
  const select = page.finder(fieldName);

  // Get all expected and available options
  const expectedOptions = optionsString.split(',').map(option => option.trim());
  const availableOptions: string[] = await select.$$('option').map(async (option) => {
    const disabled = await option.getWebElement().getAttribute('disabled');
    if(!disabled) {
      const text = await option.getAttribute('textContent');
      return text.trim();
    }
  });

  // Compare each of the expected options.
  // TODO: Should we also compare the proper ordering?
  expectedOptions.forEach(expected => {
    const available = availableOptions.find(opt => opt === expected);
    assert.equal(available, expected);
  })
});

When(/^I select the "(.*)" option from the (.*) dropdown$/, {timeout: config.timeout }, async (optionText, fieldName) => {

  const page = Context.get<LCPage>(ContextKeys.page);
  const field = page.finder(fieldName);
  const optionsToSelect: ElementArrayFinder = field.$$('option').filter(async option => {
    const text = await option.getAttribute('textContent');
    return (text || '').trim() === optionText;
  });

  const optionToSelect = await optionsToSelect.get(0);
  if(optionToSelect == null) {
    throw Error('Could not find option with text '+ optionText);
  }
  await optionToSelect.click();

});


