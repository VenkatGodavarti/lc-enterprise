import { Deserializable } from './deserializable.model';
import { AddressType } from './address.type';

export class Address implements Deserializable<Address> {
  id: string;
  type: AddressType;
  name: string;
  streetAddress1: string;
  streetAddress2: string;
  unitNumber: string;
  city: string;
  state: string;
  postalCode: string;
  country: string;
  formattedAddress: string;


  constructor(input?: any) {

    this.deserialize(input || {});
  }

  deserialize(input: any): Address {
    Object.assign(this, input);

    return this;
  }
}
