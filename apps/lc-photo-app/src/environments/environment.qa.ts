export const environment = {
  // App configuration
  production: false,
  envName: 'qa',
  applicationName: 'Listing Concierge Photo App QA',
  applicationKey: 'lc-photo-app',

  // API/UI Endpoints
  baseURL: 'http://lc.qa.lc-photo.realogy.com/',
  apiBaseURL: 'https://lc.qa.lcms-persist.realogy.com/api/v1/',
  photosBaseURL: 'https://lc.qa.lcms-image.realogy.com/api/v1/',
  notifyBaseURL: 'https://lc.qa.lc-notify.realogy.com/api/v1/',

  // OKTA configuration
  oktaAuthEnabled: true,
  oktaTenantUrl: 'https://realogy.oktapreview.com',
  oktaIssuer: 'https://realogy.oktapreview.com/oauth2/ausfby7lsqSlV4VZY0h7',
  oktaClientId: '0oajm3d7dml3RZ8bb0h7',
  oktaRedirectUri: 'https://lc.qa.lc-photo.realogy.com/implicit/callback',
};
