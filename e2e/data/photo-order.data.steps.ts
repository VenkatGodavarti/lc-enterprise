import { Given } from 'cucumber';
import { Configuration, Context, ContextKeys  } from '../../e2e';
import { tableToRecords } from '../../e2e/step_definitions';
import { BaseData } from './base-data';
import { PhotoOrdersClient } from '../../e2e/clients';

/**
 * Updates photo order associated with a given marketing order with the provided photos. i.e -
 * | orderName  | photos                                            |
 * | Alamo      | kitchen, dining room, living room                 |
 */
Given(/^the photo orders have the following photos$/, {timeout: Configuration.timeout}, async (rawTable: string) => {
  const orderPhotos = tableToRecords(rawTable)
  orderPhotos.forEach(async (order) => {
    const marketingOrder = Context.get(order.orderName+'-'+ContextKeys.order);
    if(marketingOrder == null) {
      throw Error('Marketing Order Name is not set!');
    }


    const photoNames = order.photos.split(',');
    const photos = photoNames.map(name => readPhotoFromFile(name));

    // Need to find the corresponding photo order. No other way.
    const photoOrder = await PhotoOrdersClient.findByMarketingOrderId(marketingOrder._id);
    photos.forEach(photo => photo.photographerId = photoOrder.photographerId);
    photoOrder.photos = photos;

    // Add the photos to the photo order
    await PhotoOrdersClient.addPhotos(photoOrder, photos);
  })
});


function readPhotoFromFile(photoName) {
  if(!photoName) {
    throw Error('photoName must be specified');
  }
  const fileName = photoName.trim().replace(' ', '-').toLowerCase();
  let photo = BaseData.readFileToObject(`photos/${fileName}.json`);
  return photo;
}

